<?php

/**
 * This is used for Document module in CRM.
 * @package    Document
 * @author     Icreon Tech -SR.
 */

namespace Passenger\Controller;

use Base\Controller\BaseController;
use Zend\Authentication\AuthenticationService;
use Zend\View\Model\ViewModel;
use Base\Model\UploadHandler;
use Zend\Http\Request;
use User\Model\Relationship;
use Passenger\Model\Familyhistory;
use Passenger\Form\FamilyHistoryForm;
use Passenger\Form\SearchCrmFamilyHistoryForm;
use Passenger\Form\SearchUserFamilyHistoryForm;
use Passenger\Form\AssociateExistingPassengerForm;
use Passenger\Form\AssociateManulPassengerForm;
use Passenger\Form\SaveSearchShiplineForm;
use Passenger\Form\UserFamilyHistoryBonus;

/**
 * This is Class used for Passenger/Family History.
 * @package    Document
 * @author     Icreon Tech -SK.
 */
class FamilyhistoryController extends BaseController {

    protected $_documentTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_authservice = null;
    public $auth = null;
    protected $_flashMessage = null;

    /**
     * This is used for dafault initialization of objects.
     * @package    Document
     * @author     Icreon Tech -SR.
     */
    public function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    /**
     * This function is used to get config variables
     * @return array 
     * @author Icreon Tech - SK
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This function is used to get table and config file object
     * @return table gateway
     */
    public function getTables() {
        if (!$this->_documentTable) {
            $sm = $this->getServiceLocator();
            $this->_documentTable = $sm->get('Passenger\Model\FamilyhistoryTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
        }

        return $this->_documentTable;
    }

    /**
     * This function is used to add Family History from back end.
     * @author Icreon Tech - SK
     */
    public function addCrmFamilyhistoryAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getConfig();
        $message = $this->_config['PassengerMessages']['config']['crm_family_history'];

        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getTables();

        $params = $this->params()->fromRoute();
        $contact_id = isset($params['contact_id']) ? $this->decrypt($params['contact_id']) : null;
        $full_name = "";
        $max_stories = "";
        $max_story_characters = "";
        $max_passengers = "";
        $max_saved_histories = "";
        if (!is_null($contact_id)) {
            $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $contact_id));
            
            if ($userDetail['first_name'] != '')
                $full_name.= $userDetail['first_name'];
            if ($userDetail['middle_name'] != '')
                $full_name.= " " . $userDetail['middle_name'];
            if ($userDetail['last_name'] != '')
                $full_name.= " " . $userDetail['last_name'];

            if ($full_name == "" && $userDetail['company_name'] != '')
                $full_name = $userDetail['company_name'];
            
            $membershipDetailArr = array();
            $membershipDetailArr[] = $userDetail['membership_id'];
            $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
            $userFamilyHistoryBonusData = $this->getServiceLocator()->get('Passenger\Model\FamilyhistoryTable')->getUserFamilyHistoryBonus(array('user_id' => $contact_id));
            if (!empty($userFamilyHistoryBonusData)) {
                $getMemberShip['max_stories'] = $getMemberShip['max_stories'] + $userFamilyHistoryBonusData[0]['max_stories'];
                $getMemberShip['max_story_characters'] = $getMemberShip['max_story_characters'] + $userFamilyHistoryBonusData[0]['max_story_characters'];
                $getMemberShip['max_passengers'] = $getMemberShip['max_passengers'] + $userFamilyHistoryBonusData[0]['max_passengers'];
                $getMemberShip['max_saved_histories'] = $getMemberShip['max_saved_histories'] + $userFamilyHistoryBonusData[0]['max_saved_histories'];
            }
            $max_stories = $getMemberShip['max_stories'];
            $max_story_characters = $getMemberShip['max_story_characters'];
            $max_passengers = $getMemberShip['max_passengers'];
            $max_saved_histories = $getMemberShip['max_saved_histories'];
        }

        $add_family_history_form = new FamilyHistoryForm();
        $associateExistingPassengerForm = new AssociateExistingPassengerForm();
        $associateManualPassengerForm = new AssociateManulPassengerForm();

        $add_family_history_form->get('contact_name')->setValue($full_name);
        $relationship = new Relationship($this->_adapter);
        $familyHistory = new Familyhistory($this->_adapter);

        $get_relations = $this->getServiceLocator()->get('Common\Model\CommonTable')->getFamilyRelationships('');
        $mst_relations_list = array();
        $mst_relations_list[''] = 'Please Select';
        foreach ($get_relations as $key => $val) {
            $mst_relations_list[$val['relationship_id']] = $val['relationship'];
        }
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $country_list = array();
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }

        $familyHistory = new Familyhistory($this->_adapter);
        if ($request->isPost()) {
            $form_data = $request->getPost();
            $add_family_history_form->setData($form_data);
            $add_family_history_form->setInputFilter($familyHistory->getInputFilter());

            if (!$add_family_history_form->isValid()) {
                $errors = $add_family_history_form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $type_error => $rower) {
                            $msg [$key] = $message[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
            $data_family_histories['user_id'] = $form_data->contact_name_id;
            $data_family_histories['title'] = $form_data->title;
            $data_family_histories['story'] = addslashes($form_data->story);
            $data_family_histories['is_sharing'] = $form_data->is_sharing;
            $data_family_histories['is_featured'] = $form_data->is_featured;
            $data_family_histories['is_email_on'] = $form_data->is_email_on;
            $data_family_histories['main_content'] = $form_data->main_content_id;
            if (isset($form_data->crm_user_family_history_submitbutton)) {
                $data_family_histories['is_status'] = 1;
                $data_family_histories['publish_date'] = null;
            } elseif (isset($form_data->crm_user_family_history_save_and_publishbutton)) {
                $data_family_histories['is_status'] = 2;
                $data_family_histories['publish_date'] = DATE_TIME_FORMAT;
            }
            $data_family_histories['is_deleted'] = 0;
            $data_family_histories['added_by'] = $this->auth->getIdentity()->crm_user_id;
            $data_family_histories['added_date'] = DATE_TIME_FORMAT;
            $data_family_histories['modified_by'] = $this->auth->getIdentity()->crm_user_id;
            $data_family_histories['modified_date'] = DATE_TIME_FORMAT;
            /* Get membership detail for contact */
            $msg = array();
            if (!empty($form_data->contact_name_id)) {
                $searchParam = array();
                $searchParam['user_id'] = $form_data->contact_name_id;
                $get_contact_arr = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($searchParam);
                $membershipId = (!empty($get_contact_arr[0]['membership_id'])) ? $get_contact_arr[0]['membership_id'] : 1;
                $membershipDetailArr = array();
                $membershipDetailArr[] = $membershipId;
                $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
                /* get number of user family history */

                $userFamilyHistories = $this->_documentTable->getAllFamilyHistories($searchParam);
                $numberOfFamilyHistory = count($userFamilyHistories);
                $totalPassengerCount = count($form_data->passenger_id_db);
                /* get number of user family history */

                /* Get user bonus family history */
                $searchParam = array();
                $searchParam['user_id'] = $form_data->contact_name_id;
                $userFamilyHistoryBonusData = $this->_documentTable->getUserFamilyHistoryBonus($searchParam);
                if (!empty($userFamilyHistoryBonusData)) {
                    $getMembershipDetail['max_stories'] = $getMembershipDetail['max_stories'] + $userFamilyHistoryBonusData[0]['max_stories'];
                    $getMembershipDetail['max_story_characters'] = $getMembershipDetail['max_story_characters'] + $userFamilyHistoryBonusData[0]['max_story_characters'];
                    $getMembershipDetail['max_passengers'] = $getMembershipDetail['max_passengers'] + $userFamilyHistoryBonusData[0]['max_passengers'];
                    $getMembershipDetail['max_saved_histories'] = $getMembershipDetail['max_saved_histories'] + $userFamilyHistoryBonusData[0]['max_saved_histories'];
                }
                /* End get user bonus family history */
                if ($numberOfFamilyHistory >= $getMembershipDetail['max_stories']) {
                    $msg ['contact_name'] = "The user already created " . $getMembershipDetail['max_stories'] . " stories.";
                }
                if ($totalPassengerCount > $getMembershipDetail['max_passengers']) {
                    $msg ['is_in_db'] = "The user maximum add passenger limit is " . $getMembershipDetail['max_passengers'];
                }

                if (strlen(strip_tags($form_data->story)) > $getMembershipDetail['max_story_characters']) {
                    $msg ['story'] = "User can write upto " . $getMembershipDetail['max_story_characters'] . " characters story.";
                }
            }
            if (empty($form_data->story)) {
                $msg ['story'] = $message['STORY_EMPTY'];
            }
            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
            /* End get membership detail for contact */
            $i = 0;
            if (isset($form_data->location_one_image_type)) {
                ++$i;
                $data_family_content1['location_no'] = 1;
                $data_family_content1['location_content'] = $form_data->location_one_image_id;
                $data_family_content1['content_type'] = '1';
                $data_family_content1['content_caption'] = $form_data->location_one_caption;
                $data_family_content1['is_deleted'] = 0;
                $data_family_content1['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content1['added_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_one_video_type)) {
                ++$i;
                $data_family_content1['location_no'] = 1;
                $data_family_content1['location_content'] = $form_data->location_one_video;
                $data_family_content1['content_type'] = '2';
                $data_family_content1['content_caption'] = $form_data->location_one_caption;
                $data_family_content1['is_deleted'] = 0;
                $data_family_content1['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content1['added_date'] = DATE_TIME_FORMAT;
            }

            if (isset($form_data->location_two_image_type)) {
                ++$i;
                $data_family_content2['location_no'] = 2;
                $data_family_content2['location_content'] = $form_data->location_two_image_id;
                $data_family_content2['content_type'] = '1';
                $data_family_content2['content_caption'] = $form_data->location_two_caption;
                $data_family_content2['is_deleted'] = 0;
                $data_family_content2['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content2['added_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_two_video_type)) {
                ++$i;
                $data_family_content2['location_no'] = 2;
                $data_family_content2['location_content'] = $form_data->location_two_video;
                $data_family_content2['content_type'] = '2';
                $data_family_content2['content_caption'] = $form_data->location_two_caption;
                $data_family_content2['is_deleted'] = 0;
                $data_family_content2['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content2['added_date'] = DATE_TIME_FORMAT;
            }

            if (isset($form_data->location_three_image_type)) {
                ++$i;
                $data_family_content3['location_no'] = 3;
                $data_family_content3['location_content'] = $form_data->location_three_image_id;
                $data_family_content3['content_type'] = '1';
                $data_family_content3['content_caption'] = $form_data->location_three_caption;
                $data_family_content3['is_deleted'] = 0;
                $data_family_content3['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content3['added_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_three_video_type)) {
                ++$i;
                $data_family_content3['location_no'] = 3;
                $data_family_content3['location_content'] = $form_data->location_three_video;
                $data_family_content3['content_type'] = '2';
                $data_family_content3['content_caption'] = $form_data->location_three_caption;
                $data_family_content3['is_deleted'] = 0;
                $data_family_content3['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content3['added_date'] = DATE_TIME_FORMAT;
            }

            if (isset($form_data->location_four_image_type)) {
                ++$i;
                $data_family_content4['location_no'] = 4;
                $data_family_content4['location_content'] = $form_data->location_four_image_id;
                $data_family_content4['content_type'] = '1';
                $data_family_content4['content_caption'] = $form_data->location_four_caption;
                $data_family_content4['is_deleted'] = 0;
                $data_family_content4['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content4['added_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_four_video_type)) {
                ++$i;
                $data_family_content4['location_no'] = 4;
                $data_family_content4['location_content'] = $form_data->location_four_video;
                $data_family_content4['content_type'] = '2';
                $data_family_content4['content_caption'] = $form_data->location_four_caption;
                $data_family_content4['is_deleted'] = 0;
                $data_family_content4['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content4['added_date'] = DATE_TIME_FORMAT;
            }

            if (isset($form_data->location_five_image_type)) {
                ++$i;
                $data_family_content5['location_no'] = 5;
                $data_family_content5['location_content'] = $form_data->location_five_image_id;
                $data_family_content5['content_type'] = '1';
                $data_family_content5['content_caption'] = $form_data->location_five_caption;
                $data_family_content5['is_deleted'] = 0;
                $data_family_content5['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content5['added_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_five_video_type)) {
                ++$i;
                $data_family_content5['location_no'] = 5;
                $data_family_content5['location_content'] = $form_data->location_five_video;
                $data_family_content5['content_type'] = '2';
                $data_family_content5['content_caption'] = $form_data->location_five_caption;
                $data_family_content5['is_deleted'] = 0;
                $data_family_content5['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content5['added_date'] = DATE_TIME_FORMAT;
            }
            $data_family_histories['num_content'] = $i;

            $family_history = $this->getTables()->insertCrmFamilyhistory($data_family_histories);
            if ($data_family_histories['is_status'] == 2 && $data_family_histories['user_id'] != '' && $data_family_histories['user_id'] != '0') {
                $preferenceData['user_id'] = $data_family_histories['user_id'];
                $preferenceData['family_history_privacy'] = $data_family_histories['is_email_on'];
                $this->getServiceLocator()->get('User\Model\UserTable')->updateCommunicationPreferences($preferenceData);
            }
            $family_history_id = $family_history[0]['family_history_id'];

            if (isset($data_family_content1)) {
                $data_family_content1['family_history_id'] = $family_history_id;
                $this->getTables()->insertCrmFamilyContent($data_family_content1);
            }

            if (isset($data_family_content2)) {
                $data_family_content2['family_history_id'] = $family_history_id;
                $this->getTables()->insertCrmFamilyContent($data_family_content2);
            }

            if (isset($data_family_content3)) {
                $data_family_content3['family_history_id'] = $family_history_id;
                $this->getTables()->insertCrmFamilyContent($data_family_content3);
            }

            if (isset($data_family_content4)) {
                $data_family_content4['family_history_id'] = $family_history_id;
                $this->getTables()->insertCrmFamilyContent($data_family_content4);
            }

            if (isset($data_family_content5)) {
                $data_family_content5['family_history_id'] = $family_history_id;
                $this->getTables()->insertCrmFamilyContent($data_family_content5);
            }

            if (isset($form_data->is_in_db) && $form_data->is_in_db == 1) {
                foreach ($form_data->passenger_id_db as $key => $value) {
                    if ($form_data->passenger_id_db[$key] != '') {
                        $data_family_person = array();
                        $data_family_person['family_history_id'] = $family_history_id;
                        $data_family_person['passenger_id'] = $value;
                        $data_family_person['first_name'] = null;
                        $data_family_person['last_name'] = null;
                        $data_family_person['year_of_birth'] = null;
                        $data_family_person['birth_country_id'] = null;
                        $data_family_person['panel_no'] = $form_data->panel_no_db[$key];
                        $data_family_person['relationship_id'] = $form_data->relationship_id_db[$key];
                        $data_family_person['is_deleted'] = 0;
                        $data_family_person['added_by'] = $this->auth->getIdentity()->crm_user_id;
                        $data_family_person['added_date'] = DATE_TIME_FORMAT;
                        $this->getTables()->insertCrmFamilyPerson($data_family_person);
                        unset($data_family_person);
                    }
                }
            }
            if (isset($form_data->is_manual) && $form_data->is_manual == 1) {
                foreach ($form_data->first_name_manual as $key => $value) {
                    $data_family_person = array();
                    $data_family_person['family_history_id'] = $family_history_id;
                    $data_family_person['passenger_id'] = null;
                    $data_family_person['first_name'] = $value;
                    $data_family_person['last_name'] = $form_data->last_name_manual[$key];
                    $data_family_person['year_of_birth'] = $form_data->year_of_birth_manual[$key];
                    $data_family_person['birth_country_id'] = $form_data->country_of_birth_manual_id[$key];
                    $data_family_person['panel_no'] = $form_data->panel_no_manual[$key];
                    $data_family_person['relationship_id'] = $form_data->relationship_id_manual[$key];
                    $data_family_person['is_deleted'] = 0;
                    $data_family_person['added_by'] = $this->auth->getIdentity()->crm_user_id;
                    $data_family_person['added_date'] = DATE_TIME_FORMAT;
                    $this->getTables()->insertCrmFamilyPerson($data_family_person);
                    unset($data_family_person);
                }
            }

            /** insert into the change log */
            $changeLogArray = array();
            $changeLogArray['table_name'] = 'tbl_usr_family_histories_change_logs';
            $changeLogArray['activity'] = '1';/** 2 == for update */
            $changeLogArray['id'] = $family_history_id;
            $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $changeLogArray['added_date'] = DATE_TIME_FORMAT;
            $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
            /** end insert into the change log */
            $this->flashMessenger()->addMessage($this->_config['PassengerMessages']['config']['crm_family_history']['FAMILY_CREATED']);
            $messages = array('status' => "success", 'message' => "Family history added successfully.");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        } else {
            $associateExistingPassengerForm->get('relationship_id_db[]')->setAttribute('options', $mst_relations_list);
            $add_family_history_form->add($associateExistingPassengerForm);
            $associateManualPassengerForm->get('relationship_id_manual[]')->setAttribute('options', $mst_relations_list);
            $add_family_history_form->add($associateManualPassengerForm);
        }


        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'form' => $add_family_history_form,
            'contact_id' => $contact_id,
            'country_list' => $country_list,
            'jsLangTranslate' => $message,
            'max_stories' => $max_stories,
            'max_story_characters' => $max_story_characters,
            'max_passengers' => $max_passengers,
            'max_saved_histories' => $max_saved_histories
        ));
        return $viewModel;
    }

    /**
     * This functio is used to display user family history
     * @author Icreon Tech - DG + SK
     */
    public function getUserFamilyhistoryAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('layout');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $message = $this->_config['PassengerMessages']['config']['crm_family_history'];
        $params = $this->params()->fromRoute();
        $savedFamilyHostory = $this->getServiceLocator()->get('Passenger\Model\FamilyhistoryTable')->getUserSavedFamilyHistory(array('user_id' => $this->decrypt($params['user_id'])));
        $familyHostories = $this->getServiceLocator()->get('Passenger\Model\FamilyhistoryTable')->getAllFamilyHistories(array('user_id' => $this->decrypt($params['user_id']), 'sort_field' => 'modified_date', 'sort_order' => 'desc', 'is_status' => '2'));
        $familyHostory = array();
        if (!empty($familyHostories)) {
            foreach ($familyHostories as $key => $value) {
                $familyHostory[$value['family_history_id']] = $value;
                $encryptFamilyHistoryId = $this->encrypt($value['family_history_id']);
                $familyHostory[$value['family_history_id']]['encryptFamilyHistoryId'] = $encryptFamilyHistoryId;
            }
        }
        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->decrypt($params['user_id'])));
        $userPreferencesInfo = $this->getServiceLocator()->get('User\Model\ContactTable')->getCommunicationPreferences(array('user_id' => $this->decrypt($params['user_id'])));
        $defaultFhEmailPermission = $userPreferencesInfo['family_history_privacy'];
        $membershipDetailArr = array();
        $membershipDetailArr[] = $userDetail['membership_id'];
        $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
        $maxSavedFH = 0;
        $maxStoryFH = 0;
        $isValidMembership = 1;
        if (strtotime($userDetail['membership_expire']) < strtotime(date('Y-m-d'))) {
            $isValidMembership = 0;
        }

        $maxSavedFH = $getMemberShip['max_saved_histories'];
        $maxStoryFH = $getMemberShip['max_stories'];
        $maxStoryCharacter = $getMemberShip['max_story_characters'];
        $maxPassengers = $getMemberShip['max_passengers'];
        if (isset($history['familyPerson']['db'])) {
            $numOfPassengerOnPage = (count($history['familyPerson']['db']) + 1);
        } else {
            $numOfPassengerOnPage = 1;
        }
        $add_family_history_form = new FamilyHistoryForm();
        $associateExistingPassengerForm = new AssociateExistingPassengerForm();
        $associateManualPassengerForm = new AssociateManulPassengerForm();

        $relationship = new Relationship($this->_adapter);

        $get_relations = $this->getServiceLocator()->get('Common\Model\CommonTable')->getFamilyRelationships('');
        $mst_relations_list = array();
        $mst_relations_list[''] = 'Please Select';
        foreach ($get_relations as $key => $val) {
            $mst_relations_list[$val['relationship_id']] = $val['relationship'];
        }
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $country_list = array();
        $country_list[''] = 'Please Select';
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }

        $associateExistingPassengerForm->get('relationship_id_db[]')->setAttribute('options', $mst_relations_list);
        $add_family_history_form->add($associateExistingPassengerForm);
        $associateManualPassengerForm->get('relationship_id_manual[]')->setAttribute('options', $mst_relations_list);
        $add_family_history_form->add($associateManualPassengerForm);
        $associateManualPassengerForm->get('birth_country_id_manual[]')->setAttribute('options', $country_list);
        $add_family_history_form->add($associateManualPassengerForm);

        $auth = new AuthenticationService();
        $max_stories = 0;
        if ($auth->hasIdentity() === true) {
            $data['user_id'] = $this->auth->getIdentity()->user_id;
            $data['search_type'] = 3;
            $saved_searches = $this->getTables()->getUserSearches($data);
            $userDataArr = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->auth->getIdentity()->user_id));
            $membershipDetailArr = array();
            $membershipDetailArr[] = $userDataArr['membership_id'];
            $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
            $userFamilyHistoryBonusData = $this->getServiceLocator()->get('Passenger\Model\FamilyhistoryTable')->getUserFamilyHistoryBonus(array('user_id' => $this->auth->getIdentity()->user_id));
            if (!empty($userFamilyHistoryBonusData)) {
                $getMemberShip['max_stories'] = $getMemberShip['max_stories'] + $userFamilyHistoryBonusData[0]['max_stories'];
                $getMemberShip['max_story_characters'] = $getMemberShip['max_story_characters'] + $userFamilyHistoryBonusData[0]['max_story_characters'];
                $getMemberShip['max_passengers'] = $getMemberShip['max_passengers'] + $userFamilyHistoryBonusData[0]['max_passengers'];
                $getMemberShip['max_saved_histories'] = $getMemberShip['max_saved_histories'] + $userFamilyHistoryBonusData[0]['max_saved_histories'];
            }
        }
        $maxStoryFH = $max_stories = $getMemberShip['max_stories'];
        $maxStoryCharacter = $getMemberShip['max_story_characters'];
        $maxPassengers = $getMemberShip['max_passengers'];
        $maxSavedFH = $getMemberShip['max_saved_histories'];
        $viewModel->setVariables(array(
            'savedFamilyHostory' => $savedFamilyHostory,
            'familyHostory' => $familyHostory,
            'maxSavedFH' => $maxSavedFH,
            'maxStoryFH' => $maxStoryFH,
            'maxStoryCharacter' => $maxStoryCharacter,
            'maxPassengers' => $maxPassengers,
            'membershipId' => $userDetail['membership_id'],
            'jsLangTranslate' => array_merge($this->_config['PassengerMessages']['config']['user_family_history'], $message),
            'form' => $add_family_history_form,
            'country_list' => $country_list,
            'defaultFhEmailPermission' => $defaultFhEmailPermission,
            'isValidMembership' => $isValidMembership,
            'max_stories' => $max_stories,
            'numOfPassengerOnPage' => $numOfPassengerOnPage,
            'facebookAppId' => $this->_config['facebook_app']['app_id'],
			'allowed_IP' => $this->_config['allowed_ip']['ip'],
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to delete search fh
     * @author Icreon Tech-DG
     */
    public function deleteSearchFamilyhistoryAction() {
        $this->checkFrontUserAuthentication();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $dataParam = array();
            $response = $this->getResponse();
            $messages = array();

            if ($request->getPost('search_id') != '') {
                $dataParam['search_id'] = $request->getPost('search_id');
                $dataParam['is_delete'] = 1;
                $this->getServiceLocator()->get('Passenger\Model\FamilyhistoryTable')->deleteSearchFamilyHistory($dataParam);
                $messages = array('status' => "success", "message" => "Saved family history deleted successfully");
                return $response->setContent(\Zend\Json\Json::encode($messages));
            }
        }
    }

    /**
     * This Action is used to delete fh
     * @author Icreon Tech-DG
     */
    public function deleteUserFamilyhistoryAction() {
        $this->checkFrontUserAuthentication();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $dataParam = array();
            $response = $this->getResponse();
            $messages = array();

            if ($request->getPost('family_history_id') != '') {
                $dataParam['family_history_id'] = $request->getPost('family_history_id');
                $dataParam['is_delete'] = 1;
                $this->getServiceLocator()->get('Passenger\Model\FamilyhistoryTable')->deleteFamilyHistory($dataParam);
                $messages = array('status' => "success","message"=> "Family history deleted successfully");
                return $response->setContent(\Zend\Json\Json::encode($messages));
            }
        }
    }

    /**
     * This functio is used to list Family History.
     * @author Icreon Tech - SK
     */
    public function getCrmFamilyhistoryAction() {
        if ($this->auth->hasIdentity() === false) {
            return $this->redirect()->toRoute('crm');
        }
        $this->getConfig();
        $this->getTables();
        $viewModel = new ViewModel();
        $params = $this->params()->fromRoute();
        $historySearchMessages = array_merge($this->_config['PassengerMessages']['config']['crm_family_history'], $this->_config['PassengerMessages']['config']['passenger_search']);
        $request = $this->getRequest();
        $searchFamilyHistoryForm = new SearchCrmFamilyHistoryForm();
        $saveSearchForm = new SaveSearchShiplineForm();
        /* Get date range */
        $historyDateRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
        $dateRange = array("" => "Select");
        foreach ($historyDateRange as $key => $val) {
            $dateRange[$val['range_id']] = $val['range'];
        }
        $searchFamilyHistoryForm->get('added_date_range')->setAttribute('options', $dateRange);
        /* end Get date range */
        $familyHistory = new Familyhistory($this->_adapter);
        if (($request->isXmlHttpRequest() && $request->isPost()) || $request->getPost('export') == 'excel') {
            $response = $this->getResponse();
            parse_str($request->getPost('searchString'), $searchParam);

            $dashlet = $request->getPost('crm_dashlet');
            $dashletSearchId = $request->getPost('searchId');
            $dashletId = $request->getPost('dashletId');
            $dashletSearchType = $request->getPost('searchType');

            $dashletSearchString = $request->getPost('dashletSearchString');
            if (isset($dashletSearchString) && $dashletSearchString != '') {
                $searchParam = unserialize($dashletSearchString);
            }

            $postArray = $request->getPost()->toArray();
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;

            $isExport = $request->getPost('export');
            if ($isExport != "" && $isExport == "excel") {
                $chunksize = $this->_config['export']['chunk_size'];
                $export_limit = $this->_config['export']['export_limit'];

                $searchParam['record_limit'] = $chunksize;
            }
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'tbl_usr_family_histories.modified_date' : $searchParam['sort_field'];
            
            if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] != 1 && $searchParam['added_date_range'] != '') {
                $dateRange = $this->getDateRange($searchParam['added_date_range']);
                $searchParam['added_date_from'] = $this->DateFormat($dateRange['from'], 'db_date_format_from');
                $searchParam['added_date_to'] = $this->DateFormat($dateRange['to'], 'db_date_format_to');
            } else if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] == '') {
                $searchParam['added_date_from'] = '';
                $searchParam['added_date_to'] = '';
            } else {
                if (isset($searchParam['added_date_from']) && $searchParam['added_date_from'] != '') {
                    $searchParam['added_date_from'] = $this->DateFormat($searchParam['added_date_from'], 'db_date_format_from');
                }
                if (isset($searchParam['added_date_to']) && $searchParam['added_date_to'] != '') {
                    $searchParam['added_date_to'] = $this->DateFormat($searchParam['added_date_to'], 'db_date_format_to');
                }
            }
            
            $page_counter = 1;
            $number_of_pages = 1;
            do {
                $seachResults = $this->getTables()->getAllFamilyHistories($searchParam);
                $seachResult = array();
                if (!empty($seachResults)) {
                    foreach ($seachResults as $key => $value) {
                        if(($value['user_id'] != '0' && isset($value['isDeletedUser']) && $value['isDeletedUser'] == '0') || ($value['user_id'] == '0') || $value['user_id'] == ''){
                            $seachResult[$value['family_history_id']] = $value;
                        }
                    }
                }

                $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
                $newExplodedColumnName = array();
                if (!empty($dashletResult)) {
                    $explodedColumnName = explode(",", $dashletResult[0]['table_column_names']);
                    foreach ($explodedColumnName as $val) {
                        if (strpos($val, ".")) {
                            $newExplodedColumnName[] = trim(strstr($val, ".", false), ".");
                        } else {
                            $newExplodedColumnName[] = trim($val);
                        }
                    }
                }

                $searchParam['start_index'] = '';
                $searchParam['record_limit'] = '';

                $seachResults2 = $this->getTables()->getAllFamilyHistories($searchParam);
                $seachResult2 = array();
                if (!empty($seachResults2)) {
                    foreach ($seachResults2 as $key2 => $value2) {
                        $seachResult2[$value2['family_history_id']] = $value2;
                    }
                }


                $countResult = array();

                if (!empty($seachResult)) {
                    $total = @$countResult[0]->RecordCount = count($seachResult2);
                    $jsonResult = array();
                    $arrCell = array();
                    $subArrCell = array();

                    if ($isExport == "excel") {
                        $filename = "family_history_export_" . EXPORT_DATE_TIME_FORMAT . ".csv";
                        if ($total > $chunksize) {
                            $number_of_pages = ceil($total / $chunksize);
                            $page_counter++;
                            $start = $chunksize * $page_counter - $chunksize;
                            $searchParam['startIndex'] = $start;
                            $searchParam['page'] = $page_counter;
                        } else {
                            $number_of_pages = 1;
                            $page_counter++;
                        }
                    } else {
                        $page_counter = $page;
                        $jsonResult['page'] = $request->getPost('page');
                        $jsonResult['records'] = $countResult[0]->RecordCount;
                        $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
                    }
                }
            } while ($page_counter <= $number_of_pages && $isExport == "excel");


            if (!empty($seachResult)) {
                $arrExport = array();
                foreach ($seachResult as $val) {
                    $dashletCell = array();
                    $arrCell['id'] = $val['family_history_id'];
                    $encryptId = $this->encrypt($val['family_history_id']);
                    $viewLink = '<a href="/get-crm-family-history-detail/' . $encryptId . '/' . $this->encrypt('view') . '" class="view-icon"><div class="tooltip">View<span></span></div></a>';
                    $editLink = '<a href="/get-crm-family-history-detail/' . $encryptId . '/' . $this->encrypt('edit') . '" class="edit-icon"><div class="tooltip">Edit<span></span></div></a>';
                    $deleteLink = '<a onclick="deleteFamilyhistory(\'' . $encryptId . '\')" href="#delete_familyhistory" class="delete_familyhistory delete-icon"><div class="tooltip">Delete<span></span></div></a>';
                    $actions = '<div class="action-col">' . $viewLink . $editLink . $deleteLink . '</div>';
                    $published_date = ($val['publish_date'] == 0) ? 'N/A' : $this->OutputDateFormat($val['publish_date'], 'dateformatampm');
                    $modified_date = ($val['modified_date'] == 0) ? 'N/A' : $this->OutputDateFormat($val['modified_date'], 'dateformatampm');
                    $shared = ($val['is_sharing'] == '0') ? 'No' : 'Yes';
                    $featured = ($val['is_featured'] == '0') ? 'No' : 'Yes';
                    $status = ($val['is_status'] == '2') ? 'Published' : 'Unpublished';
                    $contact_name = 'N/A';
                    if($val['user_type']==1){
                        $contact_name = '';
                        if($val['user_first_name']!='' && !is_null($val['user_first_name']))
                            $contact_name.= $val['user_first_name'];
                        if($contact_name!='' && $val['user_middle_name']!='' && !is_null($val['user_middle_name']))
                            $contact_name.= " ".$val['user_middle_name'];
                        if($val['user_last_name']!='' && !is_null($val['user_last_name'])){
                            if($contact_name!='')
                                $contact_name.= " ";
                            $contact_name.= $val['user_last_name'];
                        }
                    }elseif($val['user_type']!=1 && $val['user_type']!='' && !is_null($val['user_type'])){
                        $contact_name = $val['user_company_name'];
                    }
                    
                    if($contact_name!='N/A' && $val['user_type']!='' && !is_null($val['user_type'])){
                        $contact_name_export = $contact_name;
                        $contact_name = '<a class="txt-decoration-underline" href="/view-contact/'.$this->encrypt($val['user_id']).'/'.$this->encrypt('view').'">'.$contact_name.'</a>';
                    }else
                        $contact_name_export = $contact_name = 'N/A';
                    
                    $dashletPublishDt = array_search('publish_date', $newExplodedColumnName);
                    if ($dashletPublishDt !== false)
                        $dashletCell[$dashletPublishDt] = $published_date;
                    
                    $dashletTitle = array_search('title', $newExplodedColumnName);
                    if ($dashletTitle !== false){
                        $dashletCell[$dashletTitle] = "<a href='/get-crm-family-history-detail/".$encryptId."/".$this->encrypt('view')."' class='txt-decoration-underline'>".$val['title']."</a>";
                    }
                    
                    $dashletModifeidDt = array_search('modified_date', $newExplodedColumnName);
                    if ($dashletModifeidDt !== false)
                        $dashletCell[$dashletModifeidDt] = $modified_date;
                    
                    $dashletContact = array_search('first_name', $newExplodedColumnName);
                    if ($dashletContact !== false)
                        $dashletCell[$dashletContact] = $contact_name;
                    
                    $dashletIsSharing = array_search('is_sharing', $newExplodedColumnName);
                    if ($dashletIsSharing !== false)
                        $dashletCell[$dashletIsSharing] = $shared;
                    
                    $dashletFeatured = array_search('is_featured', $newExplodedColumnName);
                    if ($dashletFeatured !== false){
                        $dashletCell[$dashletFeatured] = $featured;
                    }
                    
                    $dashletStatus = array_search('is_status', $newExplodedColumnName);
                    if ($dashletStatus !== false)
                        $dashletCell[$dashletStatus] = $status;
                    
                    
                    if (isset($dashlet) && $dashlet == 'dashlet') {
                        $arrCell['cell'] = $dashletCell;
                    } else if ($isExport == "excel") {
                        $arrExport[] = array("Title" => $val['title'], "Contact Name" => $contact_name_export,"Published Date" => $published_date, "Last Modified Date" => $modified_date, "Shared" => $shared, "Featured" => $featured, "Status" => $status);
                    } else {
                        $arrCell['cell'] = array($val['family_history_id'],  $val['title'], $contact_name, $published_date, $modified_date, $shared, $featured, $status, $actions);
                    }
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
                if ($isExport == "excel") {
                    $this->arrayToCsv($arrExport, $filename, $page_counter);
                }
            } else {
                $jsonResult['rows'] = array();
            }

            if ($isExport == "excel") {
                $this->downloadSendHeaders($filename);
                die;
            } else {
                return $response->setContent(\Zend\Json\Json::encode($jsonResult));
            }
        } else {
            $this->layout('crm');
            $viewModel = new ViewModel();
            $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
            $country_list = array();
            foreach ($country as $key => $val) {
                $country_list[$val['country_id']] = $val['name'];
            }
            $getSearchArray = array();
            $getSearchArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $getSearchArray['isActive'] = '1';
            $getSearchArray['searchId'] = '';
            $crmSearchArray = $this->getTables()->getCrmFamilySavedSearch($getSearchArray);
            $crmSearchList = array();
            $crmSearchList[''] = 'Select';
            foreach ($crmSearchArray as $key => $val) {
                $crmSearchList[$val['family_histories_search_id']] = stripslashes($val['title']);
            }
            $searchFamilyHistoryForm->get('saved_searches')->setAttribute('options', $crmSearchList);
            $searchFamilyHistoryForm->get('country_id')->setAttribute('options', $country_list);
            $messages = array();
            if ($this->_flashMessage->hasMessages()) {
                $messages = $this->_flashMessage->getMessages();
            }
            $response = $this->getResponse();
            $viewModel->setVariables(array(
                'searchForm' => $searchFamilyHistoryForm,
                'saveSearchForm' => $saveSearchForm,
                'country_list' => $country_list,
                'jsLangTranslate' => $historySearchMessages,
                'messages' => $messages
            ));
            return $viewModel;
        }
    }

    /**
     * This Action is used to file upload form elements .
     * @author Icreon Tech-SK
     * @return upload file form
     */
    public function associateExistingPassengerAction() {
        #$form_upload_file = new ContactUploadForm();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $data['counter'] = $params['counter'];
        $data['container'] = $params['container'];
        $fieldName = 'publicdocs_upload' . $data['counter'];
        $fieldFileName = 'publicdocs_upload_file' . $data['counter'];

        if (isset($params['flag'])) {
            $data['flag'] = $params['flag'];
        }
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'form_upload_form' => $form_upload_file,
            'jsLangTranslate' => $this->_config['user_messages']['config']['create_contact'],
            'params' => $data,
                )
        );
        return $viewModel;
    }

    /**
     * This action is used to upload Location Main Content to temp folder
     * @return json
     * @author Icreon Tech - SK
     */
    public function uploadMainImageAction() {

        $fileExt = $this->GetFileExt($_FILES['main_content']['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
        $_FILES['main_content']['name'] = $filename;                 // assign name to file variable
        $this->getTables();
        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration

        $error_messages = array(
            'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini', // Set error messages
            'max_file_size' => 'File is too big',
            'min_file_size' => 'File is too small',
            'accept_file_types' => 'Filetype not allowed',
        );
        $options = array(
            'upload_dir' => $upload_file_path['temp_upload_dir'],
            'param_name' => 'main_content', //file input name                      // Set configuration
            'inline_file_types' => $upload_file_path['file_types_allowed'],
            'accept_file_types' => $upload_file_path['file_types_allowed'],
            'max_file_size' => $upload_file_path['max_allowed_file_size'],
            'min_file_size' => $upload_file_path['min_allowed_file_size'],
            'image_versions' => array(
                'medium' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_medium_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_medium_height']
                ),
                'large' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_large_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_large_height']
                ),
                'extralarge' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_extralarge_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_extralarge_height']
                ),
                'small' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_small_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_small_height']
                ),
                'thumbnail' => array(
                    'max_width' => $this->_config['file_upload_path']['user_thumb_width'],
                    'max_height' => $this->_config['file_upload_path']['user_thumb_height']
            ))
        );
        $upload_handler = new UploadHandler($options, true, $error_messages);
        $file_response = $upload_handler->jsonResponceData;
        if (isset($file_response['thumbnail'][0]->error)) {
            $messages = array('status' => "error", 'message' => $file_response['main_content'][0]->error);
        } else {
            $this->moveFileFromTempToFamilyhistory($file_response['main_content'][0]->name);
            $messages = array('status' => "success", 'message' => 'Image uploaded', 'filename' => $file_response['main_content'][0]->name);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This action is used to upload Image Location One to temp folder
     * @return json
     * @author Icreon Tech - SK
     */
    public function uploadImageOneAction() {

        $fileExt = $this->GetFileExt($_FILES['location_one_image']['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
        $_FILES['location_one_image']['name'] = $filename;                 // assign name to file variable
        $this->getTables();
        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration

        $error_messages = array(
            'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini', // Set error messages
            'max_file_size' => 'File is too big',
            'min_file_size' => 'File is too small',
            'accept_file_types' => 'Filetype not allowed',
        );
        $options = array(
            'upload_dir' => $upload_file_path['temp_upload_dir'],
            'param_name' => 'location_one_image', //file input name                      // Set configuration
            'inline_file_types' => $upload_file_path['file_types_allowed'],
            'accept_file_types' => $upload_file_path['file_types_allowed'],
            'max_file_size' => $upload_file_path['max_allowed_file_size'],
            'min_file_size' => $upload_file_path['min_allowed_file_size'],
            'image_versions' => array(
                'medium' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_medium_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_medium_height']
                ),
                'large' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_large_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_large_height']
                ),
                'extralarge' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_extralarge_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_extralarge_height']
                ),
                'small' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_small_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_small_height']
                ),
                'thumbnail' => array(
                    'max_width' => $this->_config['file_upload_path']['user_thumb_width'],
                    'max_height' => $this->_config['file_upload_path']['user_thumb_height']
            ))
        );
        $upload_handler = new UploadHandler($options, true, $error_messages);
        $file_response = $upload_handler->jsonResponceData;
        if (isset($file_response['thumbnail'][0]->error)) {
            $messages = array('status' => "error", 'message' => $file_response['location_one_image'][0]->error);
        } else {
            $this->moveFileFromTempToFamilyhistory($file_response['location_one_image'][0]->name);
            $messages = array('status' => "success", 'message' => 'Image uploaded', 'filename' => $file_response['location_one_image'][0]->name);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This action is used to upload Image Location Two to temp folder
     * @return json
     * @author Icreon Tech - SK
     */
    public function uploadImageTwoAction() {

        $fileExt = $this->GetFileExt($_FILES['location_two_image']['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
        $_FILES['location_two_image']['name'] = $filename;                 // assign name to file variable
        $this->getTables();
        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration

        $error_messages = array(
            'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini', // Set error messages
            'max_file_size' => 'File is too big',
            'min_file_size' => 'File is too small',
            'accept_file_types' => 'Filetype not allowed',
        );
        $options = array(
            'upload_dir' => $upload_file_path['temp_upload_dir'],
            'param_name' => 'location_two_image', //file input name                      // Set configuration
            'inline_file_types' => $upload_file_path['file_types_allowed'],
            'accept_file_types' => $upload_file_path['file_types_allowed'],
            'max_file_size' => $upload_file_path['max_allowed_file_size'],
            'min_file_size' => $upload_file_path['min_allowed_file_size'],
            'image_versions' => array(
                'medium' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_medium_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_medium_height']
                ),
                'large' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_large_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_large_height']
                ),
                'extralarge' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_extralarge_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_extralarge_height']
                ),
                'small' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_small_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_small_height']
                ),
                'thumbnail' => array(
                    'max_width' => $this->_config['file_upload_path']['user_thumb_width'],
                    'max_height' => $this->_config['file_upload_path']['user_thumb_height']
            ))
        );
        $upload_handler = new UploadHandler($options, true, $error_messages);
        $file_response = $upload_handler->jsonResponceData;
        if (isset($file_response['thumbnail'][0]->error)) {
            $messages = array('status' => "error", 'message' => $file_response['location_two_image'][0]->error);
        } else {
            $this->moveFileFromTempToFamilyhistory($file_response['location_two_image'][0]->name);
            $messages = array('status' => "success", 'message' => 'Image uploaded', 'filename' => $file_response['location_two_image'][0]->name);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This action is used to upload Image Location Three to temp folder
     * @return json
     * @author Icreon Tech - SK
     */
    public function uploadImageThreeAction() {

        $fileExt = $this->GetFileExt($_FILES['location_three_image']['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
        $_FILES['location_three_image']['name'] = $filename;                 // assign name to file variable
        $this->getTables();
        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration

        $error_messages = array(
            'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini', // Set error messages
            'max_file_size' => 'File is too big',
            'min_file_size' => 'File is too small',
            'accept_file_types' => 'Filetype not allowed',
        );
        $options = array(
            'upload_dir' => $upload_file_path['temp_upload_dir'],
            'param_name' => 'location_three_image', //file input name                      // Set configuration
            'inline_file_types' => $upload_file_path['file_types_allowed'],
            'accept_file_types' => $upload_file_path['file_types_allowed'],
            'max_file_size' => $upload_file_path['max_allowed_file_size'],
            'min_file_size' => $upload_file_path['min_allowed_file_size'],
            'image_versions' => array(
                'medium' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_medium_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_medium_height']
                ),
                'large' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_large_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_large_height']
                ),
                'extralarge' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_extralarge_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_extralarge_height']
                ),
                'small' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_small_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_small_height']
                ),
                'thumbnail' => array(
                    'max_width' => $this->_config['file_upload_path']['user_thumb_width'],
                    'max_height' => $this->_config['file_upload_path']['user_thumb_height']
            ))
        );
        $upload_handler = new UploadHandler($options, true, $error_messages);
        $file_response = $upload_handler->jsonResponceData;
        if (isset($file_response['thumbnail'][0]->error)) {
            $messages = array('status' => "error", 'message' => $file_response['location_three_image'][0]->error);
        } else {
            $this->moveFileFromTempToFamilyhistory($file_response['location_three_image'][0]->name);
            $messages = array('status' => "success", 'message' => 'Image uploaded', 'filename' => $file_response['location_three_image'][0]->name);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This action is used to upload Image Location Four to temp folder
     * @return json
     * @author Icreon Tech - SK
     */
    public function uploadImageFourAction() {

        $fileExt = $this->GetFileExt($_FILES['location_four_image']['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
        $_FILES['location_four_image']['name'] = $filename;                 // assign name to file variable
        $this->getTables();
        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration

        $error_messages = array(
            'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini', // Set error messages
            'max_file_size' => 'File is too big',
            'min_file_size' => 'File is too small',
            'accept_file_types' => 'Filetype not allowed',
        );
        $options = array(
            'upload_dir' => $upload_file_path['temp_upload_dir'],
            'param_name' => 'location_four_image', //file input name                      // Set configuration
            'inline_file_types' => $upload_file_path['file_types_allowed'],
            'accept_file_types' => $upload_file_path['file_types_allowed'],
            'max_file_size' => $upload_file_path['max_allowed_file_size'],
            'min_file_size' => $upload_file_path['min_allowed_file_size'],
            'image_versions' => array(
                'medium' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_medium_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_medium_height']
                ),
                'large' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_large_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_large_height']
                ),
                'extralarge' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_extralarge_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_extralarge_height']
                ),
                'small' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_small_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_small_height']
                ),
                'thumbnail' => array(
                    'max_width' => $this->_config['file_upload_path']['user_thumb_width'],
                    'max_height' => $this->_config['file_upload_path']['user_thumb_height']
            ))
        );
        $upload_handler = new UploadHandler($options, true, $error_messages);
        $file_response = $upload_handler->jsonResponceData;
        if (isset($file_response['thumbnail'][0]->error)) {
            $messages = array('status' => "error", 'message' => $file_response['location_four_image'][0]->error);
        } else {
            $this->moveFileFromTempToFamilyhistory($file_response['location_four_image'][0]->name);
            $messages = array('status' => "success", 'message' => 'Image uploaded', 'filename' => $file_response['location_four_image'][0]->name);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This action is used to upload Image Location Five to temp folder
     * @return json
     * @author Icreon Tech - SK
     */
    public function uploadImageFiveAction() {

        $fileExt = $this->GetFileExt($_FILES['location_five_image']['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
        $_FILES['location_five_image']['name'] = $filename;                 // assign name to file variable
        $this->getTables();
        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration

        $error_messages = array(
            'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini', // Set error messages
            'max_file_size' => 'File is too big',
            'min_file_size' => 'File is too small',
            'accept_file_types' => 'Filetype not allowed',
        );
        $options = array(
            'upload_dir' => $upload_file_path['temp_upload_dir'],
            'param_name' => 'location_five_image', //file input name                      // Set configuration
            'inline_file_types' => $upload_file_path['file_types_allowed'],
            'accept_file_types' => $upload_file_path['file_types_allowed'],
            'max_file_size' => $upload_file_path['max_allowed_file_size'],
            'min_file_size' => $upload_file_path['min_allowed_file_size'],
            'image_versions' => array(
                'medium' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_medium_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_medium_height']
                ),
                'large' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_large_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_large_height']
                ),
                'extralarge' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_extralarge_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_extralarge_height']
                ),
                'small' => array(
                    'max_width' => $this->_config['file_upload_path']['family_history_small_width'],
                    'max_height' => $this->_config['file_upload_path']['family_history_small_height']
                ),
                'thumbnail' => array(
                    'max_width' => $this->_config['file_upload_path']['user_thumb_width'],
                    'max_height' => $this->_config['file_upload_path']['user_thumb_height']
            ))
        );
        $upload_handler = new UploadHandler($options, true, $error_messages);
        $file_response = $upload_handler->jsonResponceData;
        if (isset($file_response['thumbnail'][0]->error)) {
            $messages = array('status' => "error", 'message' => $file_response['location_five_image'][0]->error);
        } else {
            $this->moveFileFromTempToFamilyhistory($file_response['location_five_image'][0]->name);
            $messages = array('status' => "success", 'message' => 'Image uploaded', 'filename' => $file_response['location_five_image'][0]->name);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This action is used to move file from temp to family history folder
     * @param 1 for add , 2 for edit
     * @return json
     * @author Icreon Tech - SK
     */
    public function moveFileFromTempToFamilyhistory($filename, $operation = 1, $oldfilename = '') {
        $this->getTables();
        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
        $temp_dir = $upload_file_path['temp_upload_dir'];
        $temp_thumbnail_dir = $upload_file_path['temp_upload_thumbnail_dir'];
        $temp_medium_dir = $this->_config['file_upload_path']['temp_upload_medium_dir'];
        $temp_large_dir = $this->_config['file_upload_path']['temp_upload_large_dir'];
        $temp_extralarge_dir = $this->_config['file_upload_path']['temp_upload_extralarge_dir'];
        $temp_small_dir = $this->_config['file_upload_path']['temp_upload_small_dir'];

        $cms_dir = $upload_file_path['assets_upload_dir'] . "familyhistory/";
        $cms_thumbnail_dir = $upload_file_path['assets_upload_dir'] . "familyhistory/thumbnail/";
        $cms_medium_dir = $upload_file_path['assets_upload_dir'] . "familyhistory/medium/";
        $cms_large_dir = $upload_file_path['assets_upload_dir'] . "familyhistory/large/";
        $cms_extralarge_dir = $upload_file_path['assets_upload_dir'] . "familyhistory/extralarge/";
        $cms_small_dir = $upload_file_path['assets_upload_dir'] . "familyhistory/small/";

        $temp_file = $temp_dir . $filename;
        $temp_thumbnail_file = $temp_thumbnail_dir . $filename;
        $temp_medium_file = $temp_medium_dir . $filename;
        $temp_large_file = $temp_large_dir . $filename;
        $temp_extralarge_file = $temp_extralarge_dir . $filename;
        $temp_small_file = $temp_small_dir . $filename;

        $cms_filename = $cms_dir . $filename;
        $cms_thumbnail_filename = $cms_thumbnail_dir . $filename;
        $cms_medium_filename = $cms_medium_dir . $filename;
        $cms_large_filename = $cms_large_dir . $filename;
        $cms_extralarge_filename = $cms_extralarge_dir . $filename;
        $cms_small_filename = $cms_small_dir . $filename;

        if (!is_dir($cms_dir)) {
            mkdir($cms_dir, 0777, TRUE);
        }
        if (!is_dir($cms_thumbnail_dir)) {
            mkdir($cms_thumbnail_dir, 0777, TRUE);
        }
        if (!is_dir($cms_medium_dir)) {
            mkdir($cms_medium_dir, 0777, TRUE);
        }
        if (!is_dir($cms_large_dir)) {
            mkdir($cms_large_dir, 0777, TRUE);
        }
        if (!is_dir($cms_extralarge_dir)) {
            mkdir($cms_extralarge_dir, 0777, TRUE);
        }
        if (!is_dir($cms_small_dir)) {
            mkdir($cms_small_dir, 0777, TRUE);
        }

        if (!empty($filename) && isset($filename)) {
            if (file_exists($temp_file)) {
                if (copy($temp_file, $cms_filename)) {
                    unlink($temp_file);
                }
            }
            if (file_exists($temp_thumbnail_file)) {
                if (copy($temp_thumbnail_file, $cms_thumbnail_filename)) {
                    unlink($temp_thumbnail_file);
                }
            }
            if (file_exists($temp_medium_file)) {
                if (copy($temp_medium_file, $cms_medium_filename)) {
                    unlink($temp_medium_file);
                }
            }
            if (file_exists($temp_large_file)) {
                if (copy($temp_large_file, $cms_large_filename)) {
                    unlink($temp_large_file);
                }
            }
            if (file_exists($temp_extralarge_file)) {
                if (copy($temp_extralarge_file, $cms_extralarge_filename)) {
                    unlink($temp_extralarge_file);
                }
            }
            if (file_exists($temp_small_file)) {
                if (copy($temp_small_file, $cms_small_filename)) {
                    unlink($temp_small_file);
                }
            }
            if ($operation == 2) {
                if ($oldfilename != $filename) {
                    $cms_old_filename = $cms_dir . $oldfilename;
                    $cms_old_thumbnail_filename = $cms_thumbnail_dir . $oldfilename;
                    if ($oldfilename != '') {
                        if (file_exists($cms_old_filename)) {
                            unlink($cms_old_filename);
                        }
                        if (file_exists($cms_old_thumbnail_filename)) {
                            unlink($cms_old_thumbnail_filename);
                        }
                    }
                }
            }
        }
    }

    /**
     * This action is used to view family history
     * @return json
     * @author Icreon Tech - AS
     */
    public function getCrmFamilyHistoryDetailAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getConfig();
        $message = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['crm_family_history']);
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $params = $this->params()->fromRoute();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $message,
            'encrypt_id' => $params['id'],
            'mode' => $this->decrypt($params['mode']),
            'moduleName' => $this->encrypt('family-history'),
                )
        );
        return $viewModel;
    }

    /**
     * This action is used to view family history
     * @return json
     * @author Icreon Tech - AS
     */
    public function getCrmSearchFamilyHistoryInfoAction() {
        $this->checkUserAuthentication();
        $this->getTables();
        $msg = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['crm_family_history']);
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $upload_file_path = $this->_config['file_upload_path'];
        $searchParam['id'] = $this->decrypt($params['id']);

        $response = $this->getResponse();
        $params['family_history_id'] = $this->decrypt($params['id']);
        $params['id'] = $params['family_history_id'];
        $familyHistory = $this->getTables()->getFamilyHistoryInfo($params);

        $familyContent = array();
        $familyContentData = $this->getTables()->getUserFamilyContent($params);
        foreach ($familyContentData as $key => $value) {
            $familyContent[$value['location_no']] = $value;
        }
        $familyPerson = $this->getTables()->getUserFamilyPerson($params);

        ksort($familyContent);

        $contact_id = isset($familyHistory[0]['user_id']) ? $familyHistory[0]['user_id'] : null;
        $full_name = "";
        if (!is_null($contact_id)) {
            $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $contact_id));

            if ($userDetail['first_name'] != '')
                $full_name.= $userDetail['first_name'];
            if ($userDetail['middle_name'] != '')
                $full_name.= " " . $userDetail['middle_name'];
            if ($userDetail['last_name'] != '')
                $full_name.= " " . $userDetail['last_name'];

            if ($full_name == "" && $userDetail['company_name'] != '')
                $full_name = $userDetail['company_name'];

            $familyHistory[0]['full_name'] = $full_name;
        }

        $history = array();
        $history['familyHistory'] = $familyHistory[0];
        $history['familyHistory']['mode'] = $this->encrypt("view");
        $history['familyContent'] = $familyContent;

        $history['familyPerson'] = $familyPerson;
        $i = 0;
        foreach ($history['familyPerson'] as $key1 => $value1) {
            if ($value1['passenger_id'] != '') {
                $passangerRecord = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getPassengerRecord(array('passenger_id' => $value1['passenger_id']));
                $passenger_name = (isset($passangerRecord[0])) ? ($passangerRecord[0]['PRIN_FIRST_NAME'] . " " . $passangerRecord[0]['PRIN_LAST_NAME']) : 'N/A';
                $history['familyPerson'][$key1]['passenger_name'] = $passenger_name;
                $history['familyPerson'][$key1]['year_of_birth'] = (isset($passangerRecord[0])) ? $passangerRecord[0]['BIRTH_YEAR'] : 'N/A';
                $history['familyPerson'][$key1]['encryptId'] = $this->encrypt($value1['passenger_id']);
            }
        }
        $contact_id = isset($history['familyHistory']['user_id']) ? $history['familyHistory']['user_id'] : null;
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $country_list = array();
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }
        $get_relations = $this->getServiceLocator()->get('Common\Model\CommonTable')->getFamilyRelationships('');
        $mst_relations_list = array();
        $mst_relations_list[''] = 'Please Select';
        foreach ($get_relations as $key => $val) {
            $mst_relations_list[$val['relationship_id']] = $val['relationship'];
        }
        $searchResult = $this->getTables()->getFamilyHistoryInfo($searchParam);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'searchResult' => $history,
            'country_list' => $country_list,
            'relations_list' => $mst_relations_list,
            'jsLangTranslate' => $msg,
            'filePath' => $upload_file_path
        ));
        return $viewModel;
    }

    /**
     * This action is used to edit family history
     * @return json
     * @author Icreon Tech - SK
     */
    public function editCrmFamilyHistoryAction() {
        $this->checkUserAuthentication();
        $this->getTables();

        $msg = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['crm_family_history']);
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $response = $this->getResponse();
        $params['family_history_id'] = $this->decrypt($params['id']);
        $params['id'] = $params['family_history_id'];
        $familyHistory = $this->getTables()->getFamilyHistoryInfo($params);
        $familyContent = $this->getTables()->getUserFamilyContent($params);

        $associatedPersons = $this->getTables()->getUserFamilyPerson($params);
        $history = array();
        $familyPerson = array();
        $history['familyHistory'] = $familyHistory[0];
        $history['familyContent'] = $familyContent;
        foreach ($associatedPersons as $key => $value) {
            if ($value['passenger_id'] != '') {
                $familyPerson['db'][$value['family_person_id']] = $value;
            } elseif ($value['first_name'] != '') {
                $familyPerson['manual'][$value['family_person_id']] = $value;
            }
        }
        $history['familyPerson'] = $familyPerson;

        $contact_id = (isset($history['familyHistory']['user_id']) && ($history['familyHistory']['user_id'] != 0) && (!empty($history['familyHistory']['user_id']))) ? $history['familyHistory']['user_id'] : '';
        $full_name = "";
        if (!is_null($contact_id)) {
            $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $contact_id));

            if ($userDetail['first_name'] != '')
                $full_name.= $userDetail['first_name'];
            if ($userDetail['middle_name'] != '')
                $full_name.= " " . $userDetail['middle_name'];
            if ($userDetail['last_name'] != '')
                $full_name.= " " . $userDetail['last_name'];

            if ($full_name == "" && $userDetail['company_name'] != '')
                $full_name = $userDetail['company_name'];
        }
        $add_family_history_form = new FamilyHistoryForm();
        $add_family_history_form->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'is_status',
            'options' => array(
                'value_options' => array(
                   // '1' => 'Unpublished',
                    '0'=> 'Unpublished',
                    '2' => 'Published'
                ),
            ),
            'attributes' => array(
                'id' => 'is_status',
                'value' => $history['familyHistory']['is_status'],
                'class' => 'e1'
            )
        ));

        $add_family_history_form->get('contact_name')->setValue($full_name);
        $associateExistingPassengerForm = new AssociateExistingPassengerForm();
        $add_family_history_form->add($associateExistingPassengerForm);

        $associateManualPassengerForm = new AssociateManulPassengerForm();
        //For main image location
        if ($history['familyHistory']['main_content'] != '') {
            $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
            $history['familyHistory']['main_content'] = file_exists($upload_file_path['assets_upload_dir'] . "familyhistory/thumbnail/" . $history['familyHistory']['main_content']) ? $upload_file_path['assets_url'] . "familyhistory/thumbnail/" . $history['familyHistory']['main_content'] : '';
        }

        //For image location 1,2,3,4
        //content_caption,location_one_caption
        foreach ($history['familyContent'] as $key => $value) {
            if ($value['location_content'] != '') {
                if ($value['content_type'] == 1) {
                    $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
                    $history['familyContent']['thumbnail'][$value['location_no']] = file_exists($upload_file_path['assets_upload_dir'] . "familyhistory/thumbnail/" . $history['familyContent'][$key]['location_content']) ? $upload_file_path['assets_url'] . "familyhistory/thumbnail/" . $history['familyContent'][$key]['location_content'] : '';
                    if ($value['location_no'] == 1) {
                        $add_family_history_form->get('location_one_image')->setValue($value['location_content']);
                        $add_family_history_form->get('location_one_caption')->setValue($value['content_caption']);
                    }
                    if ($value['location_no'] == 2) {
                        $add_family_history_form->get('location_two_image')->setValue($value['location_content']);
                        $add_family_history_form->get('location_two_caption')->setValue($value['content_caption']);
                    }
                    if ($value['location_no'] == 3) {
                        $add_family_history_form->get('location_three_image')->setValue($value['location_content']);
                        $add_family_history_form->get('location_three_caption')->setValue($value['content_caption']);
                    }
                    if ($value['location_no'] == 4) {
                        $add_family_history_form->get('location_four_image')->setValue($value['location_content']);
                        $add_family_history_form->get('location_four_caption')->setValue($value['content_caption']);
                    }
                    if ($value['location_no'] == 5) {
                        $add_family_history_form->get('location_five_image')->setValue($value['location_content']);
                        $add_family_history_form->get('location_five_caption')->setValue($value['content_caption']);
                    }
                } elseif ($value['content_type'] == 2) {
                    if ($value['location_no'] == 1) {
                        $add_family_history_form->get('location_one_video')->setValue($value['location_content']);
                        $add_family_history_form->get('location_one_caption')->setValue($value['content_caption']);
                    }
                    if ($value['location_no'] == 2) {
                        $add_family_history_form->get('location_two_video')->setValue($value['location_content']);
                        $add_family_history_form->get('location_two_caption')->setValue($value['content_caption']);
                    }
                    if ($value['location_no'] == 3) {
                        $add_family_history_form->get('location_three_video')->setValue($value['location_content']);
                        $add_family_history_form->get('location_three_caption')->setValue($value['content_caption']);
                    }
                    if ($value['location_no'] == 4) {
                        $add_family_history_form->get('location_four_video')->setValue($value['location_content']);
                        $add_family_history_form->get('location_four_caption')->setValue($value['content_caption']);
                    }
                    if ($value['location_no'] == 5) {
                        $add_family_history_form->get('location_five_video')->setValue($value['location_content']);
                        $add_family_history_form->get('location_five_caption')->setValue($value['content_caption']);
                    }
                }
                $history['familyContent']['family_content_id'][$value['location_no']] = $value['family_content_id'];
            }
        }
        //End For image

        $relationship = new Relationship($this->_adapter);
        $familyHistory = new Familyhistory($this->_adapter);

        $get_relations = $this->getServiceLocator()->get('Common\Model\CommonTable')->getFamilyRelationships('');
        $mst_relations_list = array();
        $mst_relations_list[''] = 'Please Select';
        foreach ($get_relations as $key => $val) {
            $mst_relations_list[$val['relationship_id']] = $val['relationship'];
        }
        $associateExistingPassengerForm->get('relationship_id_db[]')->setAttribute('options', $mst_relations_list);
        $add_family_history_form->add($associateExistingPassengerForm);
        $associateManualPassengerForm->get('relationship_id_manual[]')->setAttribute('options', $mst_relations_list);
        $add_family_history_form->get('relationship_id')->setAttribute('options', $mst_relations_list);
        $add_family_history_form->add($associateManualPassengerForm);
        $contactInfo = array();
        if (!empty($contact_id)) {
            $param['user_id'] = $contact_id;
            $contactInfo = $this->getServiceLocator()->get('Common\Model\CommonTable')->getUserCommonInfo($param);
        }

        /* Get membership detail for contact */
        if (!empty($history['familyHistory']['user_id']) && $history['familyHistory']['user_id'] != 0) {
            $searchParam = array();
            $searchParam['user_id'] = $history['familyHistory']['user_id'];
            $get_contact_arr = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($searchParam);
            $membershipId = (!empty($get_contact_arr[0]['membership_id'])) ? $get_contact_arr[0]['membership_id'] : 1;
            $membershipDetailArr = array();
            $membershipDetailArr[] = $membershipId;
            $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
            
            $userFamilyHistoryBonusData = $this->getServiceLocator()->get('Passenger\Model\FamilyhistoryTable')->getUserFamilyHistoryBonus($searchParam);
            if (!empty($userFamilyHistoryBonusData)) {
                $getMembershipDetail['max_stories'] = $getMembershipDetail['max_stories'] + $userFamilyHistoryBonusData[0]['max_stories'];
                $getMembershipDetail['max_story_characters'] = $getMembershipDetail['max_story_characters'] + $userFamilyHistoryBonusData[0]['max_story_characters'];
                $getMembershipDetail['max_passengers'] = $getMembershipDetail['max_passengers'] + $userFamilyHistoryBonusData[0]['max_passengers'];
                $getMembershipDetail['max_saved_histories'] = $getMembershipDetail['max_saved_histories'] + $userFamilyHistoryBonusData[0]['max_saved_histories'];
            }
            $max_stories = $getMembershipDetail['max_stories'];
            $maxStoryCharacter = $getMembershipDetail['max_story_characters'];
            $maxPassengers = $getMembershipDetail['max_passengers'];
        } else {
            $maxStoryCharacter = "";
            $maxPassengers = "";
        }
        if (isset($history['familyPerson']['db'])) {
            $numOfPassengerOnPage = (count($history['familyPerson']['db']) + 1);
        } else {
            $numOfPassengerOnPage = 1;
        }

        /* End get membership detail for contact */


        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $country_list = array();
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }

        if (($history['familyHistory']['is_status'] == 0 && $history['familyHistory']['user_id'] != '' && $history['familyHistory']['user_id'] != '0') || ($history['familyHistory']['user_id'] == '' || $history['familyHistory']['user_id'] == '0'))
            $defaultFhEmailPermission = $history['familyHistory']['is_email_on'];
        else {
            $userPreferencesInfo = $this->getServiceLocator()->get('User\Model\ContactTable')->getCommunicationPreferences(array('user_id' => $history['familyHistory']['user_id']));
            $defaultFhEmailPermission = $userPreferencesInfo['family_history_privacy'];
        }
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'form' => $add_family_history_form,
            'contactInfo' => $contactInfo,
            'contact_id' => $contact_id,
            'country_list' => $country_list,
            'history' => $history,
            'defaultFhEmailPermission' => $defaultFhEmailPermission,
            'mst_relations_list' => $mst_relations_list,
            'jsLangTranslate' => $msg,
            'maxStoryCharacter' => $maxStoryCharacter,
            'maxPassengers' => $maxPassengers,
            'numOfPassengerOnPage' => $numOfPassengerOnPage
        ));
        return $viewModel;
    }

    /**
     * This action is used to delete family history
     * @return json
     * @author Icreon Tech - AS
     */
    public function deleteCrmFamilyhistoryAction() {
        $this->checkUserAuthentication();
        $this->getTables();
        $msg = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['crm_family_history']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $dataParam['id'] = $this->decrypt($params['id']);
        $dataParam['modifiedBy'] = $this->auth->getIdentity()->crm_user_id;
        $dataParam['modifiendDate'] = DATE_TIME_FORMAT;
        $this->getTables()->deleteFamilyHistoryInfo($dataParam);
        $this->flashMessenger()->addMessage($msg['FAMILY_DELETED']);
        $messages = array('status' => "success");
        return $response->setContent(\Zend\Json\Json::encode($messages));
    }

    /**
     * This Action is used to save the family history search
     * @param this will pass $searchParam
     * @return this will be confirmation
     * @author Icreon Tech - AS
     */
    public function saveFamilyHistorySearchAction() {
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $familyHistory = new Familyhistory($this->_adapter);
        if ($request->isPost()) {
            $searchName = $request->getPost('search_name');
            parse_str($request->getPost('searchString'), $searchParam);
            $searchParam['search_title'] = $searchName;
            $searchParam = $familyHistory->getInputFilterCrmSearch($searchParam);
            $searchName = $familyHistory->getInputFilterCrmSearch($searchName);
            if (trim($searchName) != '') {
                $saveSearchParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $saveSearchParam['isActive'] = 1;
                $saveSearchParam['isDelete'] = 0;
                $saveSearchParam['search_name'] = $searchName;
                $saveSearchParam['search_query'] = serialize($searchParam);
                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modifiend_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['search_id'] = $request->getPost('search_id');
                    if ($this->getTables()->updateCrmFamilyHistorySearch($saveSearchParam) == true) {
                        $this->flashMessenger()->addMessage('Family history search is updated successfully');
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {
                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    if ($this->getTables()->saveCrmFamilyHistorySearch($saveSearchParam) == true) {
                        $this->flashMessenger()->addMessage('Family history search is saved successfully!');
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used to get the family history search
     * @param this will pass $searchParam
     * @return this will be confirmation
     * @author Icreon Tech - AS
     */
    public function getCrmSearchFamilySavedSelectAction() {
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $dataParam['isActive'] = '1';
                $dataParam['campaignSearchId'] = $request->getPost('search_id');
                $searchResult = $this->getTables()->getCrmFamilyHistorySavedSearch($dataParam);
                $searchResult = json_encode(unserialize($searchResult[0]['search_query']));
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This Action is used to delete family history search
     * @param this will pass $searchParam
     * @return this will be confirmation
     * @author Icreon Tech - AS
     */
    public function deleteCrmSearchFamilyhistoryAction() {
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['search_id'] = $request->getPost('search_id');
            $dataParam['modifiend_date'] = DATE_TIME_FORMAT;
            $this->getTables()->deleteCrmFamilyhistorySearch($dataParam);
            $this->flashMessenger()->addMessage('Family history search is deleted successfully!');
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This action is used to add more passenger
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function addMorePassengerAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $totalPassenger = $request->getPost('totalPassenger');
            $maxPassengerAllowed = $request->getPost('maxPassengerAllowed');
            $numOfPassengerOnPage = $request->getPost('numOfPassengerOnPage');
            $this->checkUserAuthenticationAjx();
            $this->getTables();
            $msgArr = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['crm_family_history']);
            $associateExistingPassengerForm = new AssociateExistingPassengerForm();
            $get_relations = $this->getServiceLocator()->get('Common\Model\CommonTable')->getFamilyRelationships('');
            $mst_relations_list = array();
            $mst_relations_list[''] = 'Please Select';
            foreach ($get_relations as $key => $val) {
                $mst_relations_list[$val['relationship_id']] = $val['relationship'];
            }
            $relationshipId = 'relationship_id_' . ($totalPassenger + 1);
            $associateExistingPassengerForm->get('relationship_id_db[]')->setAttributes(array('options' => $mst_relations_list, 'id' => $relationshipId));
            //echo $totalPassenger;
            if (!empty($maxPassengerAllowed))
                $isAddMore = (($numOfPassengerOnPage + 1) < $maxPassengerAllowed);
            else
                $isAddMore = 1;
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $msgArr,
                'associateExistingPassengerForm' => $associateExistingPassengerForm,
                'totalPassenger' => $totalPassenger,
                'isAddMore' => $isAddMore
            ));
            return $viewModel;
        }
    }

    /**
     *
     * This action is used to add more manual family person
     * @param void
     * @return void
     * @author Icreon Tech - SK
     */
    public function addMoreManualFamilyPersonAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $totalManualFamilyPerson = $request->getPost('totalManualFamilyPerson');
            $this->checkUserAuthenticationAjx();
            $this->getTables();
            $msgArr = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['crm_family_history']);
            $associateManualPassengerForm = new AssociateManulPassengerForm();
            $get_relations = $this->getServiceLocator()->get('Common\Model\CommonTable')->getFamilyRelationships('');
            $mst_relations_list = array();
            $mst_relations_list[''] = 'Please Select';
            foreach ($get_relations as $key => $val) {
                $mst_relations_list[$val['relationship_id']] = $val['relationship'];
            }
            $relationshipId = 'relationship_id_' . ($totalManualFamilyPerson + 1);
            $associateManualPassengerForm->get('relationship_id_manual[]')->setAttributes(array('options' => $mst_relations_list, 'id' => $relationshipId));
            $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
            $country_list = array();
            foreach ($country as $key => $val) {
                $country_list[$val['country_id']] = $val['name'];
            }
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $msgArr,
                'country_list' => $country_list,
                'associateManualPassengerForm' => $associateManualPassengerForm,
                'totalManualFamilyPerson' => $totalManualFamilyPerson
            ));
            return $viewModel;
        }
    }

    /**
     *
     * This action is used to update crm family history
     * @param void
     * @return void
     * @author Icreon Tech - SK
     */
    public function updateCrmFamilyhistoryAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $param = array();
        $familyHistory = new Familyhistory($this->_adapter);
        $message = $this->_config['PassengerMessages']['config']['crm_family_history'];

        if ($request->isPost()) {
            $add_family_history_form = new FamilyHistoryForm();
            $get_relations = $this->getServiceLocator()->get('Common\Model\CommonTable')->getFamilyRelationships('');
            $mst_relations_list = array();
            $mst_relations_list[''] = 'Please Select';
            foreach ($get_relations as $key => $val) {
                $mst_relations_list[$val['relationship_id']] = $val['relationship'];
            }
            $add_family_history_form->get('relationship_id')->setAttribute('options', $mst_relations_list);
            $form_data = $request->getPost();
            $add_family_history_form->setData($form_data);
            $add_family_history_form->setInputFilter($familyHistory->getInputFilter());

            if (!$add_family_history_form->isValid()) {
                $errors = $add_family_history_form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $type_error => $rower) {
                            $msg [$key] = $message[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }

            $param['family_history_id'] = $form_data->family_history_id;
            $family_history_data = $this->getTables()->getAllFamilyHistories($param);
            $family_content = $this->getTables()->getUserFamilyContent($param);

            $family_content_data = array();
            foreach ($family_content as $key => $value) {
                $family_content_data[$value['location_no']] = $value;
            }

            //... update family histiry..

            $data_family_histories['family_history_id'] = $form_data->family_history_id;
            $data_family_histories['user_id'] = $form_data->contact_name_id;
            $data_family_histories['title'] = $form_data->title;
            $data_family_histories['story'] = addslashes($form_data->story);
            $data_family_histories['is_sharing'] = $form_data->is_sharing;
            $data_family_histories['is_featured'] = $form_data->is_featured;
            $data_family_histories['is_email_on'] = $form_data->is_email_on;
            $data_family_histories['is_notify_contact'] = $form_data->is_notifiy;
            $data_family_histories['main_content'] = ($form_data->main_content_id != '') ? $form_data->main_content_id : '';
            $data_family_histories['num_content'] = '';
            $data_family_histories['is_status'] = $form_data->is_status;
            if ($form_data->is_status == '2')
                $data_family_histories['publish_date'] = DATE_TIME_FORMAT;
            else
                $data_family_histories['publish_date'] = null;
            $data_family_histories['is_deleted'] = '0';
            $data_family_histories['modified_by'] = $this->auth->getIdentity()->crm_user_id;
            $data_family_histories['modified_date'] = DATE_TIME_FORMAT;


            /* Get membership detail for contact */
            if (!empty($form_data->contact_name_id)) {

                $searchParam = array();
                $searchParam['user_id'] = $form_data->contact_name_id;
                $get_contact_arr = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($searchParam);
                $membershipId = (!empty($get_contact_arr[0]['membership_id'])) ? $get_contact_arr[0]['membership_id'] : 1;
                $membershipDetailArr = array();
                $membershipDetailArr[] = $membershipId;
                $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
                /* get number of user family history */

                $userFamilyHistories = $this->_documentTable->getAllFamilyHistories($searchParam);
                $numberOfFamilyHistory = count($userFamilyHistories);
                $totalPassengerCount = count($form_data->passenger_id_db);
                /* get number of user family history */

                /* Get user bonus family history */
                /* Get user bonus family history */
                $searchParam = array();
                $searchParam['user_id'] = $form_data->contact_name_id;
                $userFamilyHistoryBonusData = $this->_documentTable->getUserFamilyHistoryBonus($searchParam);
                if (!empty($userFamilyHistoryBonusData)) {
                    $getMembershipDetail['max_stories'] = $getMembershipDetail['max_stories'] + $userFamilyHistoryBonusData[0]['max_stories'];
                    $getMembershipDetail['max_story_characters'] = $getMembershipDetail['max_story_characters'] + $userFamilyHistoryBonusData[0]['max_story_characters'];
                    $getMembershipDetail['max_passengers'] = $getMembershipDetail['max_passengers'] + $userFamilyHistoryBonusData[0]['max_passengers'];
                    $getMembershipDetail['max_saved_histories'] = $getMembershipDetail['max_saved_histories'] + $userFamilyHistoryBonusData[0]['max_saved_histories'];
                }
                /* End get user bonus family history */


                /* End get user bonus family history */

                $msg = array();
                /*if ($numberOfFamilyHistory > $getMembershipDetail['max_stories']) {
                    $msg ['contact_name'] = "The user already created " . $getMembershipDetail['max_stories'] . " stories.";
                }*/
                if ($totalPassengerCount > $getMembershipDetail['max_passengers']) {
                    $msg ['is_in_db'] = "The user maximum add passenger limit is " . $getMembershipDetail['max_passengers'];
                }
                if (strlen(strip_tags($form_data->story)) > $getMembershipDetail['max_story_characters']) {
                    $msg ['story'] = "User can write upto " + $getMembershipDetail['max_story_characters'] + " characters story.";
                }
            }
            if (empty($form_data->story)) {
                $msg ['story'] = $message['STORY_EMPTY'];
            }
            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }


            /* End get membership detail for contact */


            $this->getTables()->updateCrmFamilyhistory($data_family_histories);

            if ($data_family_histories['is_status'] == 2 && $data_family_histories['user_id'] != '' && $data_family_histories['user_id'] != '0') {
                $preferenceData['user_id'] = $data_family_histories['user_id'];
                $preferenceData['family_history_privacy'] = $data_family_histories['is_email_on'];
                $this->getServiceLocator()->get('User\Model\UserTable')->updateCommunicationPreferences($preferenceData);
            }

            if (($family_history_data[0]['is_notify_contact'] == 1 || $form_data->is_notifiy == 1)) {
                if ($form_data->contact_name_id != '' && $form_data->contact_name_id != $this->auth->getIdentity()->crm_user_id) {
                    $email_id_template_int = 24;
                    $encryptId = $this->encrypt($family_history_data[0]['family_history_id']);
                    //$viewLink = '/get-crm-family-history-detail/' . $encryptId . '/' . $this->encrypt('view');
                    $viewLink = '/view-user-familyhistory/' . $encryptId;
                    $param['user_id'] = $form_data->contact_name_id;
                    $userDataArr = $this->getServiceLocator()->get('User\Model\UserTable')->getUser(array('user_id' => $form_data->contact_name_id));
                    if ($userDataArr['first_name'] == '' && $userDataArr['last_name'] == '' && $userDataArr['company_name'] != '')
                        $userDataArr['first_name'] = $userDataArr['company_name'];
                    $userDataArr['family_history_link'] = $viewLink;
                    $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                    $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                    $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $email_id_template_int, $this->_config['email_options']);
                }
            }

            $i = 0;

            //..location_one_image ...

            $data_family_content1['location_no'] = 1;
            if ($form_data->location_one_image_id != '') {
                $location_one_content = $form_data->location_one_image_id;
                $content_type_one = 1;
            } elseif (isset($form_data->location_one_video) && $form_data->location_one_video != '') {
                $content_type_one = 2;
                $location_one_content = $form_data->location_one_video;
            } else {
                $location_one_content = '';
                $content_type_one = '';
            }

            $data_family_content1['family_history_id'] = $family_history_data[0]['family_history_id'];
            $data_family_content1['location_content'] = $location_one_content;
            $data_family_content1['content_type'] = $content_type_one;
            $data_family_content1['content_caption'] = ($form_data->location_one_caption != '') ? $form_data->location_one_caption : '';
            if (!isset($form_data->location_one_image_type) && !isset($form_data->location_one_video_type)) {
                ++$i;
                $data_family_content1['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content1['modified_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_one_image_type) || isset($form_data->location_one_video_type)) {
                ++$i;
                $data_family_content1['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content1['added_date'] = DATE_TIME_FORMAT;
            }
            if (isset($family_content_data[1]['family_content_id'])) {
                $data_family_content1['family_content_id'] = $family_content_data[1]['family_content_id'];
                $this->getTables()->updateCrmFamilyContent($data_family_content1);
            } else {
                if ($data_family_content1['location_content'] != '' || $data_family_content1['content_caption'] != '') {
                    $data_family_content1['is_deleted'] = '0';
                    $this->getTables()->insertCrmFamilyContent($data_family_content1);
                }
            }

            //....end of location one image
            //..location_two_image ...

            $data_family_content2['location_no'] = 2;
            if ($form_data->location_two_image_id != '') {
                $location_two_content = $form_data->location_two_image_id;
                $content_type_two = 1;
            } elseif (isset($form_data->location_two_video) && $form_data->location_two_video != '') {
                $content_type_two = 2;
                $location_two_content = $form_data->location_two_video;
            } else {
                $location_two_content = '';
                $content_type_two = '';
            }

            $data_family_content2['family_history_id'] = $family_history_data[0]['family_history_id'];
            $data_family_content2['location_content'] = $location_two_content;
            $data_family_content2['content_type'] = $content_type_two;
            $data_family_content2['content_caption'] = ($form_data->location_two_caption != '') ? $form_data->location_two_caption : '';
            if (!isset($form_data->location_two_image_type) && !isset($form_data->location_two_video_type)) {
                ++$i;
                $data_family_content2['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content2['modified_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_two_image_type) || isset($form_data->location_two_video_type)) {
                ++$i;
                $data_family_content2['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content2['added_date'] = DATE_TIME_FORMAT;
            }
            if (isset($family_content_data[2]['family_content_id'])) {
                $data_family_content2['family_content_id'] = $family_content_data[2]['family_content_id'];
                $this->getTables()->updateCrmFamilyContent($data_family_content2);
            } else {
                if ($data_family_content2['location_content'] != '' || $data_family_content2['content_caption'] != '') {
                    $data_family_content2['is_deleted'] = '0';
                    $this->getTables()->insertCrmFamilyContent($data_family_content2);
                }
            }

            //....end of location two image
            //..location_three_image ...

            $data_family_content3['location_no'] = 3;
            if ($form_data->location_three_image_id != '') {
                $location_three_content = $form_data->location_three_image_id;
                $content_type_three = 1;
            } elseif (isset($form_data->location_three_video) && $form_data->location_three_video != '') {
                $content_type_three = 2;
                $location_three_content = $form_data->location_three_video;
            } else {
                $location_three_content = '';
                $content_type_three = '';
            }

            $data_family_content3['family_history_id'] = $family_history_data[0]['family_history_id'];
            $data_family_content3['location_content'] = $location_three_content;
            $data_family_content3['content_type'] = $content_type_three;
            $data_family_content3['content_caption'] = ($form_data->location_three_caption != '') ? $form_data->location_three_caption : '';
            if (!isset($form_data->location_three_image_type) && !isset($form_data->location_three_video_type)) {
                ++$i;
                $data_family_content3['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content3['modified_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_three_image_type) || isset($form_data->location_three_video_type)) {
                ++$i;
                $data_family_content3['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content3['added_date'] = DATE_TIME_FORMAT;
            }
            if (isset($family_content_data[3]['family_content_id'])) {
                $data_family_content3['family_content_id'] = $family_content_data[3]['family_content_id'];
                $this->getTables()->updateCrmFamilyContent($data_family_content3);
            } else {
                if ($data_family_content3['location_content'] != '' || $data_family_content3['content_caption'] != '') {
                    $data_family_content3['is_deleted'] = '0';
                    $this->getTables()->insertCrmFamilyContent($data_family_content3);
                }
            }

            //....end of location three image
            //..location_four_image ...

            $data_family_content4['location_no'] = 4;
            if ($form_data->location_four_image_id != '') {
                $location_four_content = $form_data->location_four_image_id;
                $content_type_four = 1;
            } elseif (isset($form_data->location_four_video) && $form_data->location_four_video != '') {
                $content_type_four = 2;
                $location_four_content = $form_data->location_four_video;
            } else {
                $location_four_content = '';
                $content_type_four = '';
            }

            $data_family_content4['family_history_id'] = $family_history_data[0]['family_history_id'];
            $data_family_content4['location_content'] = $location_four_content;
            $data_family_content4['content_type'] = $content_type_four;
            $data_family_content4['content_caption'] = ($form_data->location_four_caption != '') ? $form_data->location_four_caption : '';
            if (!isset($form_data->location_four_image_type) && !isset($form_data->location_four_video_type)) {
                ++$i;
                $data_family_content4['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content4['modified_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_four_image_type) || isset($form_data->location_four_video_type)) {
                ++$i;
                $data_family_content4['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content4['added_date'] = DATE_TIME_FORMAT;
            }
            if (isset($family_content_data[4]['family_content_id'])) {
                $data_family_content4['family_content_id'] = $family_content_data[4]['family_content_id'];
                ;
                $this->getTables()->updateCrmFamilyContent($data_family_content4);
            } else {
                if ($data_family_content4['location_content'] != '' || $data_family_content4['content_caption'] != '') {
                    $data_family_content4['is_deleted'] = '0';
                    $this->getTables()->insertCrmFamilyContent($data_family_content4);
                }
            }

            //....end of location four image
            //..location_five_image ...

            $data_family_content5['location_no'] = 5;
            if ($form_data->location_five_image_id != '') {
                $location_five_content = $form_data->location_five_image_id;
                $content_type_five = 1;
            } elseif (isset($form_data->location_five_video) && $form_data->location_five_video != '') {
                $content_type_five = 2;
                $location_five_content = $form_data->location_five_video;
            } else {
                $location_five_content = '';
                $content_type_five = '';
            }

            $data_family_content5['family_history_id'] = $family_history_data[0]['family_history_id'];
            $data_family_content5['location_content'] = $location_five_content;
            $data_family_content5['content_type'] = $content_type_five;
            $data_family_content5['content_caption'] = ($form_data->location_five_caption != '') ? $form_data->location_five_caption : '';
            if (!isset($form_data->location_five_image_type) && !isset($form_data->location_five_video_type)) {
                ++$i;
                $data_family_content5['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content5['modified_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_five_image_type) || isset($form_data->location_five_video_type)) {
                ++$i;
                $data_family_content5['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $data_family_content5['added_date'] = DATE_TIME_FORMAT;
            }
            if (isset($family_content_data[5]['family_content_id'])) {
                $data_family_content5['family_content_id'] = $family_content_data[5]['family_content_id'];

                $this->getTables()->updateCrmFamilyContent($data_family_content5);
            } else {
                if ($data_family_content5['location_content'] != '' || $data_family_content5['content_caption'] != '') {
                    $data_family_content5['is_deleted'] = '0';
                    $this->getTables()->insertCrmFamilyContent($data_family_content5);
                }
            }

            //....end of location five image

            if (isset($form_data->is_in_db) && $form_data->is_in_db == 1) {

                foreach ($form_data->passenger_id_db as $key => $value) {
                    if ($form_data->passenger_id_db[$key] != '') {
                        $data_family_person = array();
                        $data_family_person['family_history_id'] = $family_history_data[0]['family_history_id'];
                        $data_family_person['passenger_id'] = $value;
                        $data_family_person['first_name'] = null;
                        $data_family_person['last_name'] = null;
                        $data_family_person['year_of_birth'] = null;
                        $data_family_person['birth_country_id'] = null;
                        $data_family_person['panel_no'] = $form_data->panel_no_db[$key];
                        $data_family_person['relationship_id'] = $form_data->relationship_id_db[$key];
                        $data_family_person['is_deleted'] = 0;
                        $data_family_person['added_by'] = $this->auth->getIdentity()->crm_user_id;
                        $data_family_person['added_date'] = DATE_TIME_FORMAT;
                        $this->getTables()->insertCrmFamilyPerson($data_family_person);
                        unset($data_family_person);
                    }
                }
            }
            if (isset($form_data->is_manual) && $form_data->is_manual == 1) {

                foreach ($form_data->first_name_manual as $key => $value) {
                    $data_family_person = array();
                    $data_family_person['family_history_id'] = $family_history_data[0]['family_history_id'];
                    $data_family_person['passenger_id'] = null;
                    $data_family_person['first_name'] = $value;
                    $data_family_person['last_name'] = $form_data->last_name_manual[$key];
                    $data_family_person['year_of_birth'] = trim($form_data->year_of_birth_manual[$key]);
                    $data_family_person['birth_country_id'] = $form_data->country_of_birth_manual_id[$key];
                    $data_family_person['panel_no'] = $form_data->panel_no_manual[$key];
                    $data_family_person['relationship_id'] = $form_data->relationship_id_manual[$key];
                    $data_family_person['is_deleted'] = 0;
                    $data_family_person['added_by'] = $this->auth->getIdentity()->crm_user_id;
                    $data_family_person['added_date'] = DATE_TIME_FORMAT;
                    $this->getTables()->insertCrmFamilyPerson($data_family_person);
                    unset($data_family_person);
                }
            }

            /** insert into the change log */
            $changeLogArray = array();
            $changeLogArray['table_name'] = 'tbl_usr_family_histories_change_logs';
            $changeLogArray['activity'] = '2';/** 2 == for update */
            $changeLogArray['id'] = $family_history_data[0]['family_history_id'];
            $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $changeLogArray['added_date'] = DATE_TIME_FORMAT;
            $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
            /** end insert into the change log */
            $this->flashMessenger()->addMessage($this->_config['PassengerMessages']['config']['crm_family_history']['FAMILY_UPDATED']);
            $messages = array('status' => "success", 'message' => "Data updated");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    public function updateCrmFamilyPersonAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();

        if ($request->isPost()) {
            $form_data = $request->getPost();
            $family_person_data = $form_data->familyPersonData;
            $family_person_info = explode('&', $family_person_data);
            foreach ($family_person_info as $key => $value) {
                $data = explode('=', $value);
                $family_person[$data[0]] = $data[1];
            }
            $family_person['modified_by'] = $this->auth->getIdentity()->crm_user_id;
            $family_person['modified_date'] = DATE_TIME_FORMAT;
            $this->getTables()->updateCrmFamilyPerson($family_person);
        }
        $messages = array('status' => "success", 'message' => "Data updated");
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This action is used to get contact family history
     * @param void
     * @return void
     * @author Icreon Tech - AS
     */
    public function getContactFamilyHistoryAction() {
        if ($this->auth->hasIdentity() === false) {
            return $this->redirect()->toRoute('crm');
        }
        $this->getConfig();
        $this->getTables();
        $viewModel = new ViewModel();
        $viewModel->setTerminal('true');
        $params = $this->params()->fromRoute();
        $historySearchMessages = array_merge($this->_config['PassengerMessages']['config']['crm_family_history'], $this->_config['PassengerMessages']['config']['passenger_search']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $userFamilyHistoryBonus = new UserFamilyHistoryBonus();
        $userId = $this->decrypt($request->getPost('userid'));
        $userFamilyHistoryBonus->get('user_id')->setValue($userId);
        $searchParam = array();
        $searchParam['user_id'] = $userId;
        $userFamilyHistoryBonusData = $this->_documentTable->getUserFamilyHistoryBonus($searchParam);
        if (!empty($userFamilyHistoryBonusData)) {
            $userFamilyHistoryBonus->get('max_stories')->setValue($userFamilyHistoryBonusData[0]['max_stories']);
            $userFamilyHistoryBonus->get('max_story_characters')->setValue($userFamilyHistoryBonusData[0]['max_story_characters']);
            $userFamilyHistoryBonus->get('max_passengers')->setValue($userFamilyHistoryBonusData[0]['max_passengers']);
            $userFamilyHistoryBonus->get('max_saved_histories')->setValue($userFamilyHistoryBonusData[0]['max_saved_histories']);
        }
        /* Get membership detail for contact */
        $searchParam = array();
        $searchParam['user_id'] = $userId;
        $get_contact_arr = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($searchParam);
        $membershipId = (!empty($get_contact_arr[0]['membership_id'])) ? $get_contact_arr[0]['membership_id'] : 1;
        $membershipDetailArr = array();
        $membershipDetailArr[] = $membershipId;
        $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
        /* End Get membership detail for contact */
        $viewModel->setVariables(array(
            'jsLangTranslate' => $historySearchMessages,
            'userId' => $this->decrypt($request->getPost('userid')),
            'userFamilyHistoryBonus' => $userFamilyHistoryBonus,
            'membershipDetail' => $getMembershipDetail,
            'userFamilyHistoryBonusData' => $userFamilyHistoryBonusData
                )
        );
        return $viewModel;
    }

    /**
     * This action is used to get contact family history
     * @param void
     * @return void
     * @author Icreon Tech - AS
     */
    public function getContactFamilyHistoryListAction() {
        if ($this->auth->hasIdentity() === false) {
            return $this->redirect()->toRoute('crm');
        }
        $this->getConfig();
        $this->getTables();
        $viewModel = new ViewModel();
        $params = $this->params()->fromRoute();
        $historySearchMessages = array_merge($this->_config['PassengerMessages']['config']['crm_family_history'], $this->_config['PassengerMessages']['config']['passenger_search']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $postArray = $request->getPost();
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $searchParam['start_index'] = $start;
        $searchParam['record_limit'] = $limit;
        $searchParam['sort_field'] = $request->getPost('sidx');
        $searchParam['sort_order'] = $request->getPost('sord');
        $searchParam['user_id'] = $params['id'];
        $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'tbl_usr_family_histories.modified_date' : $searchParam['sort_field'];
        if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] != 1 && $searchParam['added_date_range'] != '') {
            $dateRange = $this->getDateRange($searchParam['added_date_range']);
            $searchParam['added_date_from'] = $this->DateFormat($dateRange['from'], 'db_date_format');
            $searchParam['added_date_to'] = $this->DateFormat($dateRange['to'], 'db_date_format');
        } else if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] == '') {
            $searchParam['added_date_from'] = '';
            $searchParam['added_date_to'] = '';
        } else {
            if (isset($searchParam['added_date_from']) && $searchParam['added_date_from'] != '') {
                $searchParam['added_date_from'] = $this->DateFormat($searchParam['added_date_from'], 'db_date_format');
            }
            if (isset($searchParam['added_date_to']) && $searchParam['added_date_to'] != '') {
                $searchParam['added_date_to'] = $this->DateFormat($searchParam['added_date_to'], 'db_date_format');
            }
        }
        $seachResult = $this->getTables()->getAllFamilyHistories($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $request->getPost('page');
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
        if (!empty($seachResult)) {

            foreach ($seachResult as $val) {
                $arrCell['id'] = $val['family_history_id'];
                $encryptId = $this->encrypt($val['family_history_id']);
                $viewLink = '<a href="/get-crm-family-history-detail/' . $encryptId . '/' . $this->encrypt('view') . '" class="view-icon"><div class="tooltip">View<span></span></div></a>';
                $editLink = '<a href="/get-crm-family-history-detail/' . $encryptId . '/' . $this->encrypt('edit') . '" class="edit-icon"><div class="tooltip">Edit<span></span></div></a>';
                $deleteLink = '<a onclick="deleteFamilyhistory(\'' . $encryptId . '\')" href="#delete_familyhistory" class="delete_familyhistory delete-icon"><div class="tooltip">Delete<span></span></div></a>';
                $actions = '<div class="action-col">' . $viewLink . $editLink . $deleteLink . '</div>';
                $published_date = ($val['publish_date'] == 0) ? 'N/A' : $this->OutputDateFormat($val['publish_date'], 'dateformatampm');
                $shared = ($val['is_sharing'] == '0') ? 'No' : 'Yes';
                $featured = ($val['is_featured'] == '0') ? 'No' : 'Yes';
				$status = ($val['is_status'] == '2') ? 'Published' : 'Unpublished';
                $arrCell['cell'] = array($val['family_history_id'], $val['title'], $published_date, $shared, $featured, $status, $actions);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This functio is used to edit bonus Family History.
     * @author Icreon Tech - AS
     */
    public function editContactFamilyHistoryBonusAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $message = $this->_config['PassengerMessages']['config']['crm_family_history'];
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getTables();
        if ($request->isPost()) {

            $form_data = $request->getPost();
            $params = $this->params()->fromRoute();
            $data_family_histories['user_id'] = $form_data->user_id;
            $data_family_histories['max_stories'] = $form_data->max_stories;
            $data_family_histories['max_story_characters'] = $form_data->max_story_characters;
            $data_family_histories['max_passengers'] = $form_data->max_passengers;
            $data_family_histories['max_saved_histories'] = $form_data->max_saved_histories;
            $data_family_histories['added_by'] = $this->auth->getIdentity()->crm_user_id;
            $data_family_histories['added_date'] = DATE_TIME_FORMAT;
            $data_family_histories['modified_by'] = $this->auth->getIdentity()->crm_user_id;
            $data_family_histories['modified_date'] = DATE_TIME_FORMAT;
            $this->getTables()->insertUserFamilyHistoryBonus($data_family_histories);
            $messages = array('status' => "success", 'message' => "Bonus Information Updated Successfully !");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to delete associated family persons (crm)
     * @param void
     * @return void
     * @author Icreon Tech - SK
     */
    public function deleteCrmFamilyPersonAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getTables();
        if ($request->isPost()) {

            $form_data = $request->getPost();
            $family_person_data = explode('=', $form_data['familyPersonId']);
            $family_person_id = $family_person_data[1];
            $param['family_person_id'] = $family_person_id;
            $param['modified_by'] = $this->auth->getIdentity()->crm_user_id;
            $this->getTables()->deleteCrmFamilyPerson($param);

            $messages = array('status' => "success", 'message' => "Family Person deleted Successfully !");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to list family histories (front)
     * @param void
     * @return void
     * @author Icreon Tech - SK
     */
    public function familyHistoriesAction() {
        $this->layout('layout');
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getTables();
        $params = $this->params()->fromRoute();
        $auth = new AuthenticationService();
        $searchMessages = $this->_config['PassengerMessages']['config']['user_family_history'];
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $country_list = array();
        $country_list[' '] = 'All';
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }

        
        $searchParam1['sort_field'] = 'modified_date';
        $searchParam1['sort_order'] = 'desc';
        
        if ($auth->hasIdentity() === true) {
            /** logged - in user **/        
            $searchParam1['is_featured'] = '1';
            $searchParam1['user_id'] = $this->auth->getIdentity()->user_id;
            /** end of logged - in user **/
        }else{
            /** guest user**/
            $searchParam1['is_featured'] = '1';
            $searchParam1['user_id'] = '';
            /** end of guest user **/
        }

        $page = 1;

        $numRecPerPage = 5;
        $searchParam1['record_limit'] = $numRecPerPage;
        $searchParam1['start_index'] = ($page - 1) * $numRecPerPage;
        $familyHistoryData = $this->getTables()->getUserFamilyHistories($searchParam1);
        $familyHistories = $familyPerson = array();
        if (!empty($familyHistoryData) && $familyHistoryData[0] != '') {
            foreach ($familyHistoryData as $index => $item) {
                $familyHistories[$item['family_history_id']] = $item;
            }            
        }
        unset($searchParam1['record_limit']);
        unset($searchParam1['start_index']);
        $allFeatured = $this->getTables()->getUserFamilyHistories($searchParam1);
        if (!empty($allFeatured) && $allFeatured[0] != '') {
            foreach ($allFeatured as $index1 => $item1) {
                $featuredFamilyHistories[$item1['family_history_id']] = $item1;
            }            
        }
        if (!empty($familyHistories)) {
            foreach ($familyHistories as $key => $value) {
                $associatedPerson = $this->getTables()->getUserFamilyPerson(array('family_history_id' => $value['family_history_id']));
                if(count($associatedPerson)>3){
                   $associatedPerson = array_slice($associatedPerson, 0, 3);
                }
                if (!empty($associatedPerson)) {
                    foreach ($associatedPerson as $key1 => $value1) {
                        if ($value1['passenger_id'] != '') {
                            $passangerRecord = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getPassengerRecord(array('passenger_id' => $value1['passenger_id']));

                            $passenger_name = (isset($passangerRecord[0])) ? ($passangerRecord[0]['PRIN_FIRST_NAME'] . " " . $passangerRecord[0]['PRIN_LAST_NAME']) : '';
                            $associatedPerson[$key1]['passenger_name'] = $passenger_name;
                        } else {
                            $associatedPerson[$key1]['passenger_name'] = '';
                        }
                    }
                }
                $familyPerson[$value['family_history_id']] = (empty($associatedPerson)) ? array() : $associatedPerson;
                if ($value['user_id'] != '' && $value['user_id'] != '0') {
                    $contactInfo = $this->getServiceLocator()->get('Common\Model\CommonTable')->getUserCommonInfo($value);
                    if ($contactInfo['full_name'] != '')
                        $familyHistories[$key]['author'] = $contactInfo['full_name'];
                    elseif ($contactInfo['full_name'] == '' && $contactInfo['company_name'] != '')
                        $familyHistories[$key]['author'] = $contactInfo['company_name'];
                }
                else
                    $familyHistories[$key]['author'] = '';

                $familyHistories[$key]['encryptFamilyHistoryId'] = $this->encrypt($value['family_history_id']);
            }
            ksort($familyPerson);
        }
        
        if(!empty($featuredFamilyHistories)){
            foreach($featuredFamilyHistories as $k=>$v){
                if ($v['user_id'] != '' && $v['user_id'] != '0') {
                    $contactInfo = $this->getServiceLocator()->get('Common\Model\CommonTable')->getUserCommonInfo($v);
                    if ($contactInfo['full_name'] != '')
                        $featuredFamilyHistories[$k]['author'] = $contactInfo['full_name'];
                    elseif ($contactInfo['full_name'] == '' && $contactInfo['company_name'] != '')
                        $featuredFamilyHistories[$k]['author'] = $contactInfo['company_name'];
                }
                else
                    $featuredFamilyHistories[$k]['author'] = '';

                $featuredFamilyHistories[$k]['encryptFamilyHistoryId'] = $this->encrypt($v['family_history_id']);
            }
        }
        $isLogin = 1;
        $saved_searches = array();
        $maxSearchCountPS = 0;
        if ($auth->hasIdentity() === true) {
            $data['user_id'] = $this->auth->getIdentity()->user_id;
            $data['search_type'] = 3;
            $saved_searches = $this->getTables()->getUserSearches($data);
            $userDataArr = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->auth->getIdentity()->user_id));
            $membershipDetailArr = array();
            $membershipDetailArr[] = $userDataArr['membership_id'];
            $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);

            
            $searchSaveType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSearchSaveTypes();
            foreach($searchSaveType as $index=>$data){
                $searchSaveTypes[$data['saved_search_type']] = $data['saved_search_type_id'];
            }
            $saveSearchTypePS = $searchSaveTypes['Family History']; //Family History
            $savedSearchInDays = explode(',', $getMemberShip['savedSearchInDays']);
            $savedSearchTypeIds = explode(',', $getMemberShip['savedSearchTypeIds']);
            $maxSearchInDays = explode(',', $getMemberShip['maxSavedSearchInDays']);

            foreach ($savedSearchTypeIds as $key => $val) {
                if ($val == $saveSearchTypePS) {
                    $maxSearchCountPS = ($maxSearchInDays[$key]>$savedSearchInDays[$key])?$maxSearchInDays[$key]:$savedSearchInDays[$key];
                }
            }
        } else {
            $isLogin = 0;
        }

        $history['familyHistories'] = $familyHistories;
        $history['familyPerson'] = $familyPerson;
        $search_user_family_history_form = new SearchUserFamilyHistoryForm();

        if (isset($this->auth->getIdentity()->user_id))
            $savedFamilyHostory = $this->getServiceLocator()->get('Passenger\Model\FamilyhistoryTable')->getUserSavedFamilyHistory(array('user_id' => $this->auth->getIdentity()->user_id));
        else
            $savedFamilyHostory = array();

        $uploadedFilePath = $this->_config['file_upload_path']['assets_url'];
        $assetsUploadDir = $this->_config['file_upload_path']['assets_upload_dir'];

        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'history' => $history,
            'isLogin' => $isLogin,
            'assetsUploadDir' => $assetsUploadDir,
            'uploadedFilePath' => $uploadedFilePath,
            'form' => $search_user_family_history_form,
            'saved_searches' => $saved_searches,
            'page' => $page,
            'save_search_limit' => $maxSearchCountPS,
            'jsLangTranslate' => $searchMessages,
            'facebookAppId' => $this->_config['facebook_app']['app_id'],
            'savedFamilyHostory' => $savedFamilyHostory,
            'featuredFamilyHistories' => $featuredFamilyHistories,
			'allowed_IP' => $this->_config['allowed_ip']['ip']
        ));
        return $viewModel;
    }

    /**
     * This action is used to search family histories (front)
     * @param void
     * @return void
     * @author Icreon Tech - SK
     */
    public function searchUserFamilyhistoryAction() {
        $auth = new AuthenticationService();
        $request = $this->getRequest();
        $response = $this->getResponse();
		$this->getConfig();
		$history = array();
		$uploadedFilePath = $this->_config['file_upload_path']['assets_url'];
        $assetsUploadDir = $this->_config['file_upload_path']['assets_upload_dir']; 
        if ($auth->hasIdentity() === true) {
            /** logged - in user **/        
            $searchParam1['user_id'] = $this->auth->getIdentity()->user_id;
            /** end of logged - in user **/
        }else{
            /** guest user**/
            $searchParam1['user_id'] = '';
            /** end of guest user **/
        }
        $searchParam1['sort_field'] = 'modified_date';
        $searchParam1['sort_order'] = 'desc';
        $page = 1;        
        $numRecPerPage = 5;
        $limit = $searchParam1['record_limit'] = $numRecPerPage;
        $searchParam1['start_index'] = ($page - 1) * $numRecPerPage;
		$contact_name = null;
		if ($request->isXmlHttpRequest() && $request->isPost()) {
            $searchParam1['is_featured'] = '';
            $searchParam = $request->getPost()->toArray();
            $searchArray = array_merge($searchParam1, $searchParam);
            $familyHistoryData = $this->getTables()->searchUserFamilyhistories($searchArray);
            $familyHistories = $familyPerson = array();
            if (!empty($familyHistoryData)) {
                foreach ($familyHistoryData as $index => $item) {
                    $familyHistories[$item['family_history_id']] = $item;
            }
            if (!empty($familyHistories)) {
                foreach ($familyHistories as $key => $value) {
                    $associatedPerson = $this->getTables()->getUserFamilyPerson(array('family_history_id' => $value['family_history_id']));
                     if(count($associatedPerson)>3){
                        $associatedPerson = array_slice($associatedPerson, 0, 3);
                    }
                    if (!empty($associatedPerson)) {
                        foreach ($associatedPerson as $key1 => $value1) {
                            if ($value1['passenger_id'] != '') {
                                $passangerRecord = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getPassengerRecord(array('passenger_id' => $value1['passenger_id']));

                                $passenger_name = (isset($passangerRecord[0])) ? ($passangerRecord[0]['PRIN_FIRST_NAME'] . " " . $passangerRecord[0]['PRIN_LAST_NAME']) : '';
                                $associatedPerson[$key1]['passenger_name'] = $passenger_name;
                            } else {
                                $associatedPerson[$key1]['passenger_name'] = '';
                            }
                        }
                    }
                    $familyPerson[$value['family_history_id']] = (empty($associatedPerson)) ? array() : $associatedPerson;
                    if ($value['user_id'] != '' && $value['user_id'] != '0') {
                        $contactInfo = $this->getServiceLocator()->get('Common\Model\CommonTable')->getUserCommonInfo($value);
                        $familyHistories[$key]['author'] = ($contactInfo['full_name'] != '') ? $contactInfo['full_name'] : $contactInfo['company_name'];
                    }
                    else
                        $familyHistories[$key]['author'] = '';

                    $familyHistories[$key]['encryptFamilyHistoryId'] = $this->encrypt($value['family_history_id']);
                }
                ksort($familyPerson);
            }

            $history['familyHistories'] = $familyHistories;
            $history['familyPerson'] = $familyPerson;
            if(isset($searchParam['contact_name']) && !is_null($searchParam['contact_name']) && $searchParam['contact_name']!='')
                $contact_name = $searchParam['contact_name'];
            else
                $contact_name = '';         
        }
		
		   $viewModel = new ViewModel();
			$viewModel->setTerminal(true);
			$viewModel->setVariables(array(
				'history' => $history,
				'assetsUploadDir' => $assetsUploadDir,
				'uploadedFilePath' => $uploadedFilePath,
				'page' => $page,
				'limit' => $limit,
				'facebookAppId' => $this->_config['facebook_app']['app_id'],
				'contact_name' => $contact_name,
				'allowed_IP' => $this->_config['allowed_ip']['ip']
			));
			return $viewModel;
       }
    }

    /**
     * This action is used to view family history detail (front)
     * @param void
     * @return void
     * @author Icreon Tech - SK
     */
    public function viewUserFamilyhistoryAction() {
        $auth = new AuthenticationService();
        $isLogin = 0;
        $this->getConfig();
        if ($auth->hasIdentity() === true) {
            $isLogin = 1;
        }
        $message = $this->_config['PassengerMessages']['config']['crm_family_history'];
        $params = $this->params()->fromRoute();
        $this->layout('layout');
        $login_user_detail = $this->auth->getIdentity();
        $family_history_id = $this->decrypt($params['id']);
        $params['family_history_id'] = $family_history_id;
        $familyHistory = $this->getTables()->getFamilyHistoryInfo($params);
        $history['familyHistory'] = $familyHistory[0];
        
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $country_list = array();
        $country_list[' '] = 'All';
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }

        $familyPerson = $this->getTables()->getUserFamilyPerson($params);
        $familyPersonData = array();
                
        if (!empty($familyPerson)) {
            foreach ($familyPerson as $key => $value) {
                if ($value['passenger_id'] != '') {
                    $passangerRecord = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getPassengerRecord(array('passenger_id' => $value['passenger_id']));
                    $passenger_name = (isset($passangerRecord[0])) ? ($passangerRecord[0]['PRIN_FIRST_NAME'] . " " . $passangerRecord[0]['PRIN_LAST_NAME']) : 'N/A';
                    if((isset($passangerRecord[0]))){
                        $passenger_name = $passangerRecord[0]['PRIN_FIRST_NAME'] . " " . $passangerRecord[0]['PRIN_LAST_NAME'];
                        $familyPerson[$key]['passenger_name'] = $passenger_name;
                        $familyPerson[$key]['year_of_birth'] = (isset($passangerRecord[0])) ? $passangerRecord[0]['BIRTH_YEAR'] : 'N/A';
                        $familyPerson[$key]['encryptId'] = $this->encrypt($value['passenger_id']);
                    }
                }
            }
            
            if(!empty($familyPerson)){
                foreach($familyPerson as $k=>$v){
                    if(isset($v['passenger_name']) && $v['passenger_name']!='' &&  $v['passenger_name']!='N/A' ){
                        $familyPersonData[$k] = $v;
                    }elseif(($v['first_name']!='' || $v['last_name']!='') && $v['passenger_id']=='')
                        $familyPersonData[$k] = $v;
                }
            }
        }

        $history['familyPerson'] = $familyPersonData;

        $familyContentData = $this->getTables()->getUserFamilyContent($params);
        $familyContent = array();
        foreach ($familyContentData as $key1 => $value1) {
            $familyContent[$value1['location_no']] = $value1;
        }
        if (!empty($familyContent))
            ksort($familyContent);
        $history['familyContent'] = $familyContent;
        $uploadedFilePath = $this->_config['file_upload_path']['assets_url'];
        $assetsUploadDir = $this->_config['file_upload_path']['assets_upload_dir'];
        $email_id_template_int = '27';
        $encryptId = $this->encrypt($familyHistory[0]['family_history_id']);
        $viewLink = '/view-user-familyhistory/' . $encryptId;
        if ($familyHistory[0]['user_id'] != '' && $familyHistory[0]['user_id'] != '0')
            $userDataArr = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $familyHistory[0]['user_id']));
        else
            $userDataArr = array();
        //$userDataArr = (array) $this->auth->getIdentity();
        $userDataArr['family_history_link'] = $viewLink;
        $userDataArr['story_title'] = $familyHistory[0]['title'];
        $emailBody = '';
        if ($familyHistory[0]['user_id'] != '' && $familyHistory[0]['user_id'] != '0') {
            if ($this->auth->hasIdentity() === true) {
                if ($userDataArr['first_name'] == '' && $userDataArr['first_name'] == '' && $userDataArr['company_name'] != '')
                    $userDataArr['first_name'] = $userDataArr['company_name'];
                $viewerDataArr = (array) $this->auth->getIdentity();
                $userDataArr['viewer_first_name'] = $viewerDataArr['first_name'];
                $userDataArr['viewer_last_name'] = $viewerDataArr['last_name'];
                $userDataArr['viewer_email_id'] = $viewerDataArr['email_id'];
                $emailBody = $this->getServiceLocator()->get('Common\Model\EmailTable')->getEmailContent($userDataArr, $email_id_template_int);
            }else {
                $userDataArr['viewer_first_name'] = '';
                $userDataArr['viewer_last_name'] = '';
                $userDataArr['viewer_email_id'] = '';
                $emailBody = $this->getServiceLocator()->get('Common\Model\EmailTable')->getEmailContent($userDataArr, $email_id_template_int);
            }
        }


        if (isset($this->auth->getIdentity()->user_id))
            $savedFamilyHostory = $this->getServiceLocator()->get('Passenger\Model\FamilyhistoryTable')->getUserSavedFamilyHistory(array('user_id' => $this->auth->getIdentity()->user_id));
        else
            $savedFamilyHostory = array();
        $request = $this->getRequest();

        $userPreferencesInfo = $this->getServiceLocator()->get('User\Model\ContactTable')->getCommunicationPreferences(array('user_id' => $history['familyHistory']['user_id']));
        $defaultFhEmailPermission = $userPreferencesInfo['family_history_privacy'];
        $max_saved_histories = 0;

        if ($familyHistory[0]['user_id'] != '' && $familyHistory[0]['user_id'] != '0')
            $author_data = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $familyHistory[0]['user_id']));
        else
            $author_data = array();

        if ($auth->hasIdentity() === true) {
            $data['user_id'] = $this->auth->getIdentity()->user_id;
            $data['search_type'] = 3;
            $saved_searches = $this->getTables()->getUserSearches($data);
            $userDataArr = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->auth->getIdentity()->user_id));
            $membershipDetailArr = array();
            $membershipDetailArr[] = $userDataArr['membership_id'];
            $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
            $max_saved_histories = $getMemberShip['max_saved_histories'];
        }
        $search_user_family_history_form = new SearchUserFamilyHistoryForm();
        if(isset($params['contact_name']) && !is_null($params['contact_name']) && $params['contact_name']!='')
            $search_user_family_history_form->get('contact_name')->setValue($this->Decrypt($params['contact_name']));
        
        
        $get_relations = $this->getServiceLocator()->get('Common\Model\CommonTable')->getFamilyRelationships('');
        $mst_relations_list = array();
        $mst_relations_list[''] = 'Please Select';
        foreach ($get_relations as $key => $val) {
            $mst_relations_list[$val['relationship_id']] = $val['relationship'];
        }
        
        $showrequest = '';
        
        if(isset($params['showrequest']) && !empty($params['showrequest']))
                $showrequest = $params['showrequest'];
				
		$searchParam11 = array();
		if ($auth->hasIdentity() === true) {
            /** logged - in user **/        
            $searchParam11['is_featured'] = '1';
            $searchParam11['user_id'] = $this->auth->getIdentity()->user_id;
            /** end of logged - in user **/
        }else{
            /** guest user**/
            $searchParam11['is_featured'] = '1';
            $searchParam11['user_id'] = '';
            /** end of guest user **/
        }
		$featuredFamilyHistories = array();
		$allFeatured = $this->getTables()->getUserFamilyHistories($searchParam11);
        if (!empty($allFeatured) && $allFeatured[0] != '') {
            foreach ($allFeatured as $index1 => $item1) {
                $featuredFamilyHistories[$item1['family_history_id']] = $item1;
            }            
        }
		if(!empty($featuredFamilyHistories)){
            foreach($featuredFamilyHistories as $k=>$v){
                if ($v['user_id'] != '' && $v['user_id'] != '0') {
                    $contactInfo = $this->getServiceLocator()->get('Common\Model\CommonTable')->getUserCommonInfo($v);
                    if ($contactInfo['full_name'] != '')
                        $featuredFamilyHistories[$k]['author'] = $contactInfo['full_name'];
                    elseif ($contactInfo['full_name'] == '' && $contactInfo['company_name'] != '')
                        $featuredFamilyHistories[$k]['author'] = $contactInfo['company_name'];
                }
                else
                    $featuredFamilyHistories[$k]['author'] = '';

                $featuredFamilyHistories[$k]['encryptFamilyHistoryId'] = $this->encrypt($v['family_history_id']);
            }
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'history' => $history,
            'assetsUploadDir' => $assetsUploadDir,
            'uploadedFilePath' => $uploadedFilePath,
            'emailBody' => $emailBody,
            'isLogin' => $isLogin,
            'savedFamilyHostory' => $savedFamilyHostory,
            'jsLangTranslate' => $message,
            'max_saved_histories' => $max_saved_histories,
            'defaultFhEmailPermission' => $defaultFhEmailPermission,
            'author_data' => $author_data,
            'facebookAppId' => $this->_config['facebook_app']['app_id'],
            'form' => $search_user_family_history_form,
            'relations_list' => $mst_relations_list,
            'showrequest' => $this->decrypt($showrequest),
			'featuredFamilyHistories' => $featuredFamilyHistories,
			'allowed_IP' => $this->_config['allowed_ip']['ip']
        ));
        return $viewModel;
    }

    /**
     * This action is used send email to author of family history, if edited by anyone.(crm)
     * @param void
     * @return void
     * @author Icreon Tech - SK
     */
    public function sendemailFamilyhistoryAction() {
        $this->checkFrontUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $postdata = $request->getPost()->toArray();
            $familyHistory = $this->getTables()->getFamilyHistoryInfo($postdata);
            $email_id_template_int = '27';
            $encryptId = $this->encrypt($familyHistory[0]['family_history_id']);
            $viewLink = '/view-user-familyhistory/' . $encryptId;

            if ($familyHistory[0]['user_id'] != '' && $familyHistory[0]['user_id'] != '0') {
                $userDataArr = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $familyHistory[0]['user_id']));
                if ($userDataArr['first_name'] == '' && $userDataArr['last_name'] == '' && $userDataArr['company_name'] != '')
                    $userDataArr['first_name'] = $userDataArr['company_name'];
            }
            else
                $userDataArr = array();

            $viewerDataArr = (array) $this->auth->getIdentity();
            $userDataArr['viewer_first_name'] = $viewerDataArr['first_name'];
            $userDataArr['viewer_last_name'] = $viewerDataArr['last_name'];
            $userDataArr['viewer_email_id'] = $viewerDataArr['email_id'];
            $userDataArr['family_history_link'] = $viewLink;
            $userDataArr['story_title'] = $familyHistory[0]['title'];
            $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
            $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
            $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $email_id_template_int, $this->_config['email_options']);
            $messages = array('status' => "success", 'message' => "Email Sent");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }else{ 
            $userId = isset($params['user_id'])?$this->decrypt($params['user_id']):'';
            $defaultPermission = $this->decrypt($params['defaultPermission']);
            $email_id_template_int = '27';
            $encryptId = $params['family_history_id'];
            $viewLink = '/view-user-familyhistory/' . $encryptId;
            $message = $this->_config['PassengerMessages']['config']['crm_family_history'];
            if ($userId != '' && $userId!= '0') { 
                $userDataArr = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $userId));
                if ($this->auth->hasIdentity() === true) {
                    if ($userDataArr['first_name'] == '' && $userDataArr['first_name'] == '' && $userDataArr['company_name'] != '')
                        $userDataArr['first_name'] = $userDataArr['company_name'];
                    $viewerDataArr = (array) $this->auth->getIdentity();
                    $userDataArr['viewer_first_name'] = $viewerDataArr['first_name'];
                    $userDataArr['viewer_last_name'] = $viewerDataArr['last_name'];
                    $userDataArr['viewer_email_id'] = $viewerDataArr['email_id'];

                    $userDataArr['family_history_link'] = $viewLink;
                    $userDataArr['story_title'] = $this->decrypt($params['story_title']);
                    $emailBody = $this->getServiceLocator()->get('Common\Model\EmailTable')->getEmailContent($userDataArr, $email_id_template_int);
                }else {
                    $userDataArr['viewer_first_name'] = '';
                    $userDataArr['viewer_last_name'] = '';
                    $userDataArr['viewer_email_id'] = '';
                    $emailBody = $this->getServiceLocator()->get('Common\Model\EmailTable')->getEmailContent($userDataArr, $email_id_template_int);
                }
            }
        }
        
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'emailBody' => $emailBody,
            'jsLangTranslate' => $message,
            'family_history_id' => $this->decrypt($params['family_history_id'])
            ));
        return $viewModel;
    }

    /**
     * This action is used to save search family history (front)
     * @param void
     * @return void
     * @author Icreon Tech - SK
     */
    public function saveUserFamilyHistorySearchAction() {
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $familyHistory = new Familyhistory($this->_adapter);
        if ($request->isPost()) {
            $searchName = $request->getPost('search_name');
            parse_str($request->getPost('searchString'), $searchParam);
            $searchParam['search_title'] = $searchName;
            $searchParam = $familyHistory->getInputFilterCrmSearch($searchParam);
            $searchName = $familyHistory->getInputFilterCrmSearch($searchName);
            if (trim($searchName) != '') {
                $saveSearchParam['search_type'] = 3;
                $saveSearchParam['user_id'] = $this->auth->getIdentity()->user_id;
                $saveSearchParam['is_active'] = 1;
                $saveSearchParam['is_deleted'] = 0;
                $saveSearchParam['title'] = $searchName;
                $saveSearchParam['search_query'] = serialize($searchParam);
                $saveSearchParam['save_date'] = DATE_TIME_FORMAT;
                if ($this->getTables()->saveUserFamilyHistorySearch($saveSearchParam) == true) {
                    $this->flashMessenger()->addMessage('Family history search is saved successfully!');
                    $messages = array('status' => "success");
                    return $response->setContent(\Zend\Json\Json::encode($messages));
                }
            }
        }
    }

    /**
     * This action is used to get saved family history data (front)
     * @param void
     * @return void
     * @author Icreon Tech - SK
     */
    public function getUserSearchFamilySavedSelectAction() {
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $search_id = $request->getPost('search_id');
                $dataParam['user_id'] = $this->auth->getIdentity()->user_id;
                $dataParam['search_type'] = 3;
                $dataParam['search_id'] = $search_id;
                $saved_searches = $this->getTables()->getUserSearches($dataParam);
                foreach ($saved_searches as $key => $value) {
                    $saved_searches_data[$value['search_id']] = $value;
                }
                $searchResult = $saved_searches_data[$search_id];
                $searchResult = json_encode(unserialize($searchResult['search_query']));
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This action is used to add more passanger (front)
     * @param void
     * @return void
     * @author Icreon Tech - SK
     */
    public function associateMorePassengerAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $totalPassenger = $request->getPost('totalPassenger');
            $maxPassengerAllowed = $request->getPost('maxPassengerAllowed');
            $numOfPassengerOnPage = $request->getPost('numOfPassengerOnPage');
            $this->getTables();
            $msgArr = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['crm_family_history']);
            $associateExistingPassengerForm = new AssociateExistingPassengerForm();
            $get_relations = $this->getServiceLocator()->get('Common\Model\CommonTable')->getFamilyRelationships('');
            $mst_relations_list = array();
            $mst_relations_list[''] = 'Please Select';
            foreach ($get_relations as $key => $val) {
                $mst_relations_list[$val['relationship_id']] = $val['relationship'];
            }
            $relationshipId = 'relationship_id_' . ($totalPassenger + 1);
            $associateExistingPassengerForm->get('relationship_id_db[]')->setAttributes(array('options' => $mst_relations_list, 'id' => $relationshipId));
            //echo $totalPassenger;
            if (!empty($maxPassengerAllowed))
                $isAddMore = (($numOfPassengerOnPage + 1) < $maxPassengerAllowed);
            else
                $isAddMore = 1;
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $msgArr,
                'associateExistingPassengerForm' => $associateExistingPassengerForm,
                'totalPassenger' => $totalPassenger,
                'isAddMore' => $isAddMore
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to add more manual person(front)
     * @param void
     * @return void
     * @author Icreon Tech - SK
     */
    public function associateMoreManualFamilyPersonAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $totalManualFamilyPerson = $request->getPost('totalManualFamilyPerson');
            $this->getTables();
            $msgArr = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['crm_family_history']);
            $associateManualPassengerForm = new AssociateManulPassengerForm();
            $get_relations = $this->getServiceLocator()->get('Common\Model\CommonTable')->getFamilyRelationships('');
            $mst_relations_list = array();
            $mst_relations_list[''] = 'Please Select';
            foreach ($get_relations as $key => $val) {
                $mst_relations_list[$val['relationship_id']] = $val['relationship'];
            }
            $relationshipId = 'relationship_id_' . ($totalManualFamilyPerson + 1);
            $associateManualPassengerForm->get('relationship_id_manual[]')->setAttributes(array('options' => $mst_relations_list, 'id' => $relationshipId));
            $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
            $country_list = array();
            $country_list[''] = 'Please Select';
            foreach ($country as $key => $val) {
                $country_list[$val['country_id']] = $val['name'];
            }
            $associateManualPassengerForm->get('birth_country_id_manual[]')->setAttribute('options', $country_list);
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $msgArr,
                'country_list' => $country_list,
                'associateManualPassengerForm' => $associateManualPassengerForm,
                'totalManualFamilyPerson' => $totalManualFamilyPerson
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to add family history (front)
     * @param void
     * @return void
     * @author Icreon Tech - SK
     */
    public function addUserFamilyHistoryAction() {
        $this->checkFrontUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getTables();
        if ($request->isPost()) {
            $familyHistory = new Familyhistory($this->_adapter);
            $add_family_history_form = new FamilyHistoryForm();
            $get_relations = $this->getServiceLocator()->get('Common\Model\CommonTable')->getFamilyRelationships('');
            $mst_relations_list = array();
            $mst_relations_list[''] = 'Please Select';
            foreach ($get_relations as $key => $val) {
                $mst_relations_list[$val['relationship_id']] = $val['relationship'];
            }
            $add_family_history_form->get('relationship_id')->setAttribute('options', $mst_relations_list);
            $form_data = $request->getPost();
            $add_family_history_form->setInputFilter($familyHistory->getInputFilter());
            $add_family_history_form->setData($form_data);
            $message = $this->_config['PassengerMessages']['config']['crm_family_history'];
            if (!$add_family_history_form->isValid()) {
                $errors = $add_family_history_form->getMessages();

                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $type_error => $rower) {
                            $msg [$key] = $message[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $message['NOT_MATCH']);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
            $data_family_histories['user_id'] = $this->auth->getIdentity()->user_id;
            $data_family_histories['title'] = $form_data->title;
            $data_family_histories['story'] = addslashes($form_data->story);
            $data_family_histories['is_sharing'] = $form_data->is_sharing;
            $data_family_histories['is_featured'] = $form_data->is_featured;
            $data_family_histories['is_email_on'] = $form_data->is_email_on;
            $data_family_histories['main_content'] = $form_data->main_content_id;
            if (isset($form_data->crm_user_family_history_save_and_publishbutton)) {
                $data_family_histories['is_status'] = 0;
                $data_family_histories['publish_date'] = null;
            } elseif (isset($form_data->crm_user_family_history_submitbutton)) {
                $data_family_histories['is_status'] = 2;
                $data_family_histories['publish_date'] = DATE_TIME_FORMAT;
            }
            $data_family_histories['is_deleted'] = 0;
            $data_family_histories['added_by'] = $this->auth->getIdentity()->user_id;
            $data_family_histories['added_date'] = DATE_TIME_FORMAT;
            $data_family_histories['modified_by'] = $this->auth->getIdentity()->user_id;
            $data_family_histories['modified_date'] = DATE_TIME_FORMAT;
            /* Get membership detail for contact */
            $msg = array();
            $searchParam = array();
            $searchParam['user_id'] = $this->auth->getIdentity()->user_id;
            $get_contact_arr = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($searchParam);
            $membershipId = (!empty($get_contact_arr[0]['membership_id'])) ? $get_contact_arr[0]['membership_id'] : 1;
            $membershipDetailArr = array();
            $membershipDetailArr[] = $membershipId;
            $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
            /* get number of user family history */

            $userFamilyHistories = $this->_documentTable->getAllFamilyHistories($searchParam);
            $numberOfFamilyHistory = count($userFamilyHistories); 
            $totalPassengerCount = count($form_data->passenger_id_db);
            /* get number of user family history */

            /* Get user bonus family history */
            $searchParam = array();
            $searchParam['user_id'] = $this->auth->getIdentity()->user_id;
            $userFamilyHistoryBonusData = $this->_documentTable->getUserFamilyHistoryBonus($searchParam);
            if (!empty($userFamilyHistoryBonusData)) {
                $getMembershipDetail['max_stories'] = $getMembershipDetail['max_stories'] + $userFamilyHistoryBonusData[0]['max_stories'];
                $getMembershipDetail['max_story_characters'] = $getMembershipDetail['max_story_characters'] + $userFamilyHistoryBonusData[0]['max_story_characters'];
                $getMembershipDetail['max_passengers'] = $getMembershipDetail['max_passengers'] + $userFamilyHistoryBonusData[0]['max_passengers'];
                $getMembershipDetail['max_saved_histories'] = $getMembershipDetail['max_saved_histories'] + $userFamilyHistoryBonusData[0]['max_saved_histories'];
            }
            /* End get user bonus family history */
            if ($numberOfFamilyHistory >= $getMembershipDetail['max_stories']) {
                $msg ['contact_name'] = "The user already created " . $getMembershipDetail['max_stories'] . " stories.";
            }
            if ($totalPassengerCount > $getMembershipDetail['max_passengers']) {
                $msg ['is_in_db'] = "The user maximum add passenger limit is " . $getMembershipDetail['max_passengers'];
            }
            if (strlen(strip_tags($form_data->story)) > $getMembershipDetail['max_story_characters'] and isset($getMembershipDetail['max_story_characters']) and trim($getMembershipDetail['max_story_characters']) != "") {
                $msg ['story'] = "User can write upto " . $getMembershipDetail['max_story_characters'] . " characters story.";
            }
            if (empty($form_data->story)) {
                $msg ['story'] = $message['STORY_EMPTY'];
            }
            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
            /* End get membership detail for contact */
            $i = 0;
            if (isset($form_data->location_one_image_type)) {
                ++$i;
                $data_family_content1['location_no'] = 1;
                $data_family_content1['location_content'] = $form_data->location_one_image_id;
                $data_family_content1['content_type'] = '1';
                $data_family_content1['content_caption'] = $form_data->location_one_caption;
                $data_family_content1['is_deleted'] = 0;
                $data_family_content1['added_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content1['added_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_one_video_type)) {
                ++$i;
                $data_family_content1['location_no'] = 1;
                $data_family_content1['location_content'] = $form_data->location_one_video;
                $data_family_content1['content_type'] = '2';
                $data_family_content1['content_caption'] = $form_data->location_one_caption;
                $data_family_content1['is_deleted'] = 0;
                $data_family_content1['added_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content1['added_date'] = DATE_TIME_FORMAT;
            }

            if (isset($form_data->location_two_image_type)) {
                ++$i;
                $data_family_content2['location_no'] = 2;
                $data_family_content2['location_content'] = $form_data->location_two_image_id;
                $data_family_content2['content_type'] = '1';
                $data_family_content2['content_caption'] = $form_data->location_two_caption;
                $data_family_content2['is_deleted'] = 0;
                $data_family_content2['added_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content2['added_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_two_video_type)) {
                ++$i;
                $data_family_content2['location_no'] = 2;
                $data_family_content2['location_content'] = $form_data->location_two_video;
                $data_family_content2['content_type'] = '2';
                $data_family_content2['content_caption'] = $form_data->location_two_caption;
                $data_family_content2['is_deleted'] = 0;
                $data_family_content2['added_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content2['added_date'] = DATE_TIME_FORMAT;
            }

            if (isset($form_data->location_three_image_type)) {
                ++$i;
                $data_family_content3['location_no'] = 3;
                $data_family_content3['location_content'] = $form_data->location_three_image_id;
                $data_family_content3['content_type'] = '1';
                $data_family_content3['content_caption'] = $form_data->location_three_caption;
                $data_family_content3['is_deleted'] = 0;
                $data_family_content3['added_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content3['added_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_three_video_type)) {
                ++$i;
                $data_family_content3['location_no'] = 3;
                $data_family_content3['location_content'] = $form_data->location_three_video;
                $data_family_content3['content_type'] = '2';
                $data_family_content3['content_caption'] = $form_data->location_three_caption;
                $data_family_content3['is_deleted'] = 0;
                $data_family_content3['added_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content3['added_date'] = DATE_TIME_FORMAT;
            }

            if (isset($form_data->location_four_image_type)) {
                ++$i;
                $data_family_content4['location_no'] = 4;
                $data_family_content4['location_content'] = $form_data->location_four_image_id;
                $data_family_content4['content_type'] = '1';
                $data_family_content4['content_caption'] = $form_data->location_four_caption;
                $data_family_content4['is_deleted'] = 0;
                $data_family_content4['added_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content4['added_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_four_video_type)) {
                ++$i;
                $data_family_content4['location_no'] = 4;
                $data_family_content4['location_content'] = $form_data->location_four_video;
                $data_family_content4['content_type'] = '2';
                $data_family_content4['content_caption'] = $form_data->location_four_caption;
                $data_family_content4['is_deleted'] = 0;
                $data_family_content4['added_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content4['added_date'] = DATE_TIME_FORMAT;
            }

            if (isset($form_data->location_five_image_type)) {
                ++$i;
                $data_family_content5['location_no'] = 5;
                $data_family_content5['location_content'] = $form_data->location_five_image_id;
                $data_family_content5['content_type'] = '1';
                $data_family_content5['content_caption'] = $form_data->location_five_caption;
                $data_family_content5['is_deleted'] = 0;
                $data_family_content5['added_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content5['added_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_five_video_type)) {
                ++$i;
                $data_family_content5['location_no'] = 5;
                $data_family_content5['location_content'] = $form_data->location_five_video;
                $data_family_content5['content_type'] = '2';
                $data_family_content5['content_caption'] = $form_data->location_five_caption;
                $data_family_content5['is_deleted'] = 0;
                $data_family_content5['added_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content5['added_date'] = DATE_TIME_FORMAT;
            }
            $data_family_histories['num_content'] = $i;

            $family_history = $this->getTables()->insertCrmFamilyhistory($data_family_histories);
            if ($data_family_histories['is_status'] == 2 && $data_family_histories['user_id'] != '' && $data_family_histories['user_id'] != '0') {
                $preferenceData['user_id'] = $data_family_histories['user_id'];
                $preferenceData['family_history_privacy'] = $data_family_histories['is_email_on'];
                $this->getServiceLocator()->get('User\Model\UserTable')->updateCommunicationPreferences($preferenceData);
            }
            $family_history_id = $family_history[0]['family_history_id'];

            if (isset($data_family_content1)) {
                $data_family_content1['family_history_id'] = $family_history_id;
                $this->getTables()->insertCrmFamilyContent($data_family_content1);
            }

            if (isset($data_family_content2)) {
                $data_family_content2['family_history_id'] = $family_history_id;
                $this->getTables()->insertCrmFamilyContent($data_family_content2);
            }

            if (isset($data_family_content3)) {
                $data_family_content3['family_history_id'] = $family_history_id;
                $this->getTables()->insertCrmFamilyContent($data_family_content3);
            }

            if (isset($data_family_content4)) {
                $data_family_content4['family_history_id'] = $family_history_id;
                $this->getTables()->insertCrmFamilyContent($data_family_content4);
            }

            if (isset($data_family_content5)) {
                $data_family_content5['family_history_id'] = $family_history_id;
                $this->getTables()->insertCrmFamilyContent($data_family_content5);
            }

            foreach ($form_data->passenger_id_db as $key => $value) {
                if ($form_data->passenger_id_db[$key] != '') {
                    $data_family_person = array();
                    $data_family_person['family_history_id'] = $family_history_id;
                    $data_family_person['passenger_id'] = $value;
                    $data_family_person['first_name'] = null;
                    $data_family_person['last_name'] = null;
                    $data_family_person['year_of_birth'] = null;
                    $data_family_person['birth_country_id'] = null;
                    $data_family_person['panel_no'] = $form_data->panel_no_db[$key];
                    $data_family_person['relationship_id'] = $form_data->relationship_id_db[$key];
                    $data_family_person['is_deleted'] = 0;
                    $data_family_person['added_by'] = $this->auth->getIdentity()->user_id;
                    $data_family_person['added_date'] = DATE_TIME_FORMAT;
                    $this->getTables()->insertCrmFamilyPerson($data_family_person);
                    unset($data_family_person);
                }
            }
            foreach ($form_data->first_name_manual as $key => $value) {
                if ($form_data->first_name_manual[$key] != '') {
                    $data_family_person = array();
                    $data_family_person['family_history_id'] = $family_history_id;
                    $data_family_person['passenger_id'] = null;
                    $data_family_person['first_name'] = $value;
                    $data_family_person['last_name'] = $form_data->last_name_manual[$key];
                    $data_family_person['year_of_birth'] = $form_data->year_of_birth_manual[$key];
                    $data_family_person['birth_country_id'] = $form_data->birth_country_id_manual[$key];
                    $data_family_person['panel_no'] = $form_data->panel_no_manual[$key];
                    $data_family_person['relationship_id'] = $form_data->relationship_id_manual[$key];
                    $data_family_person['is_deleted'] = 0;
                    $data_family_person['added_by'] = $this->auth->getIdentity()->user_id;
                    $data_family_person['added_date'] = DATE_TIME_FORMAT;
                    $this->getTables()->insertCrmFamilyPerson($data_family_person);
                    unset($data_family_person);
                }
            }


            $messages = array('status' => "success", 'message' => "Family history added successfully", 'family_history_id' => $family_history_id);
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to edit family history (front)
     * @param void
     * @return void
     * @author Icreon Tech - SK
     */
    public function editUserFamilyHistoryAction() {
        $this->getTables();
        $this->getConfig();
        $msg = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['crm_family_history']);
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        if ($request->isPost()) {
            $params = $request->getPost();

            $params['family_history_id'] = $params['id'];
            $params['id'] = $params['family_history_id'];
            $familyHistory = $this->getTables()->getFamilyHistoryInfo($params);

            $familyContent = $this->getTables()->getUserFamilyContent($params);


            $associatedPersons = $this->getTables()->getUserFamilyPerson($params);
            $history = array();
            $familyPerson = array();
            $history['familyHistory'] = $familyHistory[0];
            $history['familyContent'] = $familyContent;
            foreach ($associatedPersons as $key => $value) {
                if ($value['passenger_id'] != '') {
                    $passangerRecord = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getPassengerRecord(array('passenger_id' => $value['passenger_id']));
                    $passenger_name = (isset($passangerRecord[0])) ? ($passangerRecord[0]['PRIN_FIRST_NAME'] . " " . $passangerRecord[0]['PRIN_LAST_NAME']) : '';
                    $associatedPerson[$key]['passenger_name'] = $passenger_name;
                    $familyPerson[$value['family_person_id']] = $value;
                    $familyPerson[$value['family_person_id']]['passenger_name'] = $passenger_name;
                    $familyPerson[$value['family_person_id']]['encPassengerId'] = $this->encrypt($value['passenger_id']);
                    $familyPerson[$value['family_person_id']]['viewtab'] = $this->encrypt('passenger');
                    $familyPerson[$value['family_person_id']]['year_of_birth'] = (isset($passangerRecord[0])) ? $passangerRecord[0]['BIRTH_YEAR'] : 'N/A';
                } elseif ($value['first_name'] != '') {
                    $familyPerson[$value['family_person_id']] = $value;
                    $familyPerson[$value['family_person_id']]['passenger_name'] = '';
                }
            }
            $history['familyPerson'] = $familyPerson;

            $contact_id = (isset($history['familyHistory']['user_id']) && ($history['familyHistory']['user_id'] != 0) && (!empty($history['familyHistory']['user_id']))) ? $history['familyHistory']['user_id'] : '';

            $add_family_history_form = new FamilyHistoryForm();
            $add_family_history_form->add(array(
                'type' => 'Zend\Form\Element\Select',
                'name' => 'is_status',
                'options' => array(
                    'value_options' => array(
                        '1' => 'Unpublished',
                        '2' => 'Published'
                    ),
                ),
                'attributes' => array(
                    'id' => 'is_status',
                    'value' => $history['familyHistory']['is_status'],
                    'class' => 'e1'
                )
            ));
            $associateExistingPassengerForm = new AssociateExistingPassengerForm();
            $add_family_history_form->add($associateExistingPassengerForm);

            $associateManualPassengerForm = new AssociateManulPassengerForm();
            //For main image location
            if ($history['familyHistory']['main_content'] != '') {
                $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
                $history['familyHistory']['main_content'] = file_exists($upload_file_path['assets_upload_dir'] . "familyhistory/thumbnail/" . $history['familyHistory']['main_content']) ? $upload_file_path['assets_url'] . "familyhistory/thumbnail/" . $history['familyHistory']['main_content'] : '';
            }

            foreach ($history['familyContent'] as $key => $value) {
                if ($value['location_content'] != '') {
                    if ($value['content_type'] == 1) {
                        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
                        $history['familyContent']['thumbnail'][$value['location_no']] = file_exists($upload_file_path['assets_upload_dir'] . "familyhistory/thumbnail/" . $history['familyContent'][$key]['location_content']) ? $upload_file_path['assets_url'] . "familyhistory/thumbnail/" . $history['familyContent'][$key]['location_content'] : '';
                        if ($value['location_no'] == 1) {
                            $add_family_history_form->get('location_one_image')->setValue($value['location_content']);
                            $add_family_history_form->get('location_one_caption')->setValue($value['content_caption']);
                        }
                        if ($value['location_no'] == 2) {
                            $add_family_history_form->get('location_two_image')->setValue($value['location_content']);
                            $add_family_history_form->get('location_two_caption')->setValue($value['content_caption']);
                        }
                        if ($value['location_no'] == 3) {
                            $add_family_history_form->get('location_three_image')->setValue($value['location_content']);
                            $add_family_history_form->get('location_three_caption')->setValue($value['content_caption']);
                        }
                        if ($value['location_no'] == 4) {
                            $add_family_history_form->get('location_four_image')->setValue($value['location_content']);
                            $add_family_history_form->get('location_four_caption')->setValue($value['content_caption']);
                        }
                        if ($value['location_no'] == 5) {
                            $add_family_history_form->get('location_five_image')->setValue($value['location_content']);
                            $add_family_history_form->get('location_five_caption')->setValue($value['content_caption']);
                        }
                    } elseif ($value['content_type'] == 2) {
                        if ($value['location_no'] == 1) {
                            $add_family_history_form->get('location_one_video')->setValue($value['location_content']);
                            $add_family_history_form->get('location_one_caption')->setValue($value['content_caption']);
                        }
                        if ($value['location_no'] == 2) {
                            $add_family_history_form->get('location_two_video')->setValue($value['location_content']);
                            $add_family_history_form->get('location_two_caption')->setValue($value['content_caption']);
                        }
                        if ($value['location_no'] == 3) {
                            $add_family_history_form->get('location_three_video')->setValue($value['location_content']);
                            $add_family_history_form->get('location_three_caption')->setValue($value['content_caption']);
                        }
                        if ($value['location_no'] == 4) {
                            $add_family_history_form->get('location_four_video')->setValue($value['location_content']);
                            $add_family_history_form->get('location_four_caption')->setValue($value['content_caption']);
                        }
                        if ($value['location_no'] == 5) {
                            $add_family_history_form->get('location_five_video')->setValue($value['location_content']);
                            $add_family_history_form->get('location_five_caption')->setValue($value['content_caption']);
                        }
                    }
                    $history['familyContent']['family_content_id'][$value['location_no']] = $value['family_content_id'];
                }
            }
            //End For image

            $relationship = new Relationship($this->_adapter);
            $familyHistory = new Familyhistory($this->_adapter);

            $get_relations = $this->getServiceLocator()->get('Common\Model\CommonTable')->getFamilyRelationships('');
            $mst_relations_list = array();
            $mst_relations_list[''] = 'Please Select';
            foreach ($get_relations as $key => $val) {
                $mst_relations_list[$val['relationship_id']] = $val['relationship'];
            }
            $associateExistingPassengerForm->get('relationship_id_db[]')->setAttribute('options', $mst_relations_list);
            $add_family_history_form->add($associateExistingPassengerForm);
            $associateManualPassengerForm->get('relationship_id_manual[]')->setAttribute('options', $mst_relations_list);
            $add_family_history_form->get('relationship_id')->setAttribute('options', $mst_relations_list);
            $add_family_history_form->add($associateManualPassengerForm);

            $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
            $country_list = array();
            $country_list[''] = 'Please Select';
            foreach ($country as $key => $val) {
                $country_list[$val['country_id']] = $val['name'];
            }
            $associateManualPassengerForm->get('birth_country_id_manual[]')->setAttribute('options', $country_list);
            $contactInfo = array();
            if (!empty($contact_id)) {
                $param['user_id'] = $contact_id;
                $contactInfo = $this->getServiceLocator()->get('Common\Model\CommonTable')->getUserCommonInfo($param);
            }

            /* Get membership detail for contact */
            if (!empty($history['familyHistory']['user_id']) && $history['familyHistory']['user_id'] != 0) {
                $searchParam = array();
                $searchParam['user_id'] = $history['familyHistory']['user_id'];
                $get_contact_arr = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($searchParam);
                $membershipId = (!empty($get_contact_arr[0]['membership_id'])) ? $get_contact_arr[0]['membership_id'] : 1;
                $membershipDetailArr = array();
                $membershipDetailArr[] = $membershipId;
                $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
                
                $userFamilyHistoryBonusData = $this->_documentTable->getUserFamilyHistoryBonus($searchParam);
                if (!empty($userFamilyHistoryBonusData)) {
                    $getMembershipDetail['max_stories'] = $getMembershipDetail['max_stories'] + $userFamilyHistoryBonusData[0]['max_stories'];
                    $getMembershipDetail['max_story_characters'] = $getMembershipDetail['max_story_characters'] + $userFamilyHistoryBonusData[0]['max_story_characters'];
                    $getMembershipDetail['max_passengers'] = $getMembershipDetail['max_passengers'] + $userFamilyHistoryBonusData[0]['max_passengers'];
                    $getMembershipDetail['max_saved_histories'] = $getMembershipDetail['max_saved_histories'] + $userFamilyHistoryBonusData[0]['max_saved_histories'];
                }
                $maxStoryCharacter = $getMembershipDetail['max_story_characters'];
                $maxPassengers = $getMembershipDetail['max_passengers'];
            } else {
                $maxStoryCharacter = "";
                $maxPassengers = "";
            }
            if (isset($history['familyPerson']['db'])) {
                $numOfPassengerOnPage = (count($history['familyPerson']['db']) + 1);
            } else {
                $numOfPassengerOnPage = 1;
            }

            $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->auth->getIdentity()->user_id));
            if ($history['familyHistory']['is_status'] == 0)
                $defaultFhEmailPermission = $history['familyHistory']['is_email_on'];
            else {
                $userPreferencesInfo = $this->getServiceLocator()->get('User\Model\ContactTable')->getCommunicationPreferences(array('user_id' => $history['familyHistory']['user_id']));
                $defaultFhEmailPermission = $userPreferencesInfo['family_history_privacy'];
            }
            $membershipDetailArr = array();
            $membershipDetailArr[] = $userDetail['membership_id'];
            $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
            $maxSavedFH = 0;
            $maxStoryFH = 0;
            $isValidMembership = 1;
            if (strtotime($userDetail['membership_expire']) < strtotime(date('Y-m-d'))) {
                $isValidMembership = 0;
            }

            /* End get membership detail for contact */
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'form' => $add_family_history_form,
                'contactInfo' => $contactInfo,
                'contact_id' => $contact_id,
                'country_list' => $country_list,
                'history' => $history,
                'mst_relations_list' => $mst_relations_list,
                'jsLangTranslate' => $msg,
                'maxStoryCharacter' => $maxStoryCharacter,
                'maxPassengers' => $maxPassengers,
                'isValidMembership' => $isValidMembership,
                'defaultFhEmailPermission' => $defaultFhEmailPermission,
                'numOfPassengerOnPage' => $numOfPassengerOnPage
            ));
            return $viewModel;
        }
        $response = $this->getResponse();
    }

    /**
     * This action is used to delete family history (front)
     * @param void
     * @return void
     * @author Icreon Tech - SK
     */
    public function deleteUserFamilyPersonAction() {
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getTables();
        if ($request->isPost()) {

            $form_data = $request->getPost();
            $family_person_data = explode('=', $form_data['familyPersonId']);
            $family_person_id = $family_person_data[1];
            $param['family_person_id'] = $family_person_id;
            $param['modified_by'] = $this->auth->getIdentity()->user_id;
            $this->getTables()->deleteCrmFamilyPerson($param);

            $messages = array('status' => "success", 'message' => "Family Person deleted Successfully !");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to update family history (front)
     * @param void
     * @return void
     * @author Icreon Tech - SK
     */
    public function updateUserFamilyhistoryAction() {
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $param = array();
        $familyHistory = new Familyhistory($this->_adapter);
        $message = $this->_config['PassengerMessages']['config']['crm_family_history'];
        if ($request->isPost()) {
            $add_family_history_form = new FamilyHistoryForm();
            $get_relations = $this->getServiceLocator()->get('Common\Model\CommonTable')->getFamilyRelationships('');
            $mst_relations_list = array();
            $mst_relations_list[''] = 'Please Select';
            foreach ($get_relations as $key => $val) {
                $mst_relations_list[$val['relationship_id']] = $val['relationship'];
            }
            $add_family_history_form->get('relationship_id')->setAttribute('options', $mst_relations_list);
            $form_data = $request->getPost();
            $add_family_history_form->setData($form_data);
            $add_family_history_form->setInputFilter($familyHistory->getInputFilter());

            if (!$add_family_history_form->isValid()) {
                $errors = $add_family_history_form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $type_error => $rower) {
                            $msg [$key] = $message[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
            $param['family_history_id'] = $this->decrypt($form_data->family_history_id);
            $family_history_data = $this->getTables()->getAllFamilyHistories($param);

            $family_content = $this->getTables()->getUserFamilyContent($param);

            $family_content_data = array();
            foreach ($family_content as $key => $value) {
                $family_content_data[$value['location_no']] = $value;
            }

            //... update family histiry..
            $family_history_id = $this->decrypt($form_data->family_history_id);
            $data_family_histories['family_history_id'] = $this->decrypt($form_data->family_history_id);
            $data_family_histories['user_id'] = $this->auth->getIdentity()->user_id;
            $data_family_histories['title'] = $form_data->title;
            $data_family_histories['story'] = addslashes($form_data->story);
            $data_family_histories['is_sharing'] = $form_data->is_sharing;
            $data_family_histories['is_featured'] = $form_data->is_featured;
            $data_family_histories['is_email_on'] = $form_data->is_email_on;
            $data_family_histories['is_notify_contact'] = $form_data->is_notifiy;
            $data_family_histories['main_content'] = ($form_data->main_content_id != '') ? $form_data->main_content_id : '';
            $data_family_histories['num_content'] = '';
            $data_family_histories['is_status'] = $form_data->is_status;
            if (isset($form_data->crm_user_family_history_save_and_publishbutton)) {
                $data_family_histories['is_status'] = 0;
                $data_family_histories['publish_date'] = null;
            } elseif (isset($form_data->crm_user_family_history_submitbutton)) {
                $data_family_histories['is_status'] = 2;
                $data_family_histories['publish_date'] = DATE_TIME_FORMAT;
            }
            $data_family_histories['is_deleted'] = '0';
            $data_family_histories['modified_by'] = $this->auth->getIdentity()->user_id;
            $data_family_histories['modified_date'] = DATE_TIME_FORMAT;


            /* Get membership detail for contact */

            $searchParam = array();
            $searchParam['user_id'] = $this->auth->getIdentity()->user_id;
            $get_contact_arr = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($searchParam);
            $membershipId = (!empty($get_contact_arr[0]['membership_id'])) ? $get_contact_arr[0]['membership_id'] : 1;
            $membershipDetailArr = array();
            $membershipDetailArr[] = $membershipId;
            $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
            /* get number of user family history */

            $userFamilyHistories = $this->_documentTable->getAllFamilyHistories($searchParam);
            $numberOfFamilyHistory = count($userFamilyHistories);
            $totalPassengerCount = count($form_data->passenger_id_db);
            /* get number of user family history */

            /* Get user bonus family history */
            /* Get user bonus family history */
            $searchParam = array();
            $searchParam['user_id'] = $this->auth->getIdentity()->user_id;
            $userFamilyHistoryBonusData = $this->_documentTable->getUserFamilyHistoryBonus($searchParam);
            if (!empty($userFamilyHistoryBonusData)) {
                $getMembershipDetail['max_stories'] = $getMembershipDetail['max_stories'] + $userFamilyHistoryBonusData[0]['max_stories'];
                $getMembershipDetail['max_story_characters'] = $getMembershipDetail['max_story_characters'] + $userFamilyHistoryBonusData[0]['max_story_characters'];
                $getMembershipDetail['max_passengers'] = $getMembershipDetail['max_passengers'] + $userFamilyHistoryBonusData[0]['max_passengers'];
                $getMembershipDetail['max_saved_histories'] = $getMembershipDetail['max_saved_histories'] + $userFamilyHistoryBonusData[0]['max_saved_histories'];
            }
            /* End get user bonus family history */


            /* End get user bonus family history */

            $msg = array();
            /*if ($numberOfFamilyHistory > $getMembershipDetail['max_stories']) {
                $msg ['contact_name'] = "The user already created " . $getMembershipDetail['max_stories'] . " stories.";
            }*/
            if ($totalPassengerCount > $getMembershipDetail['max_passengers']) {
                $msg ['is_in_db'] = "The user maximum add passenger limit is " . $getMembershipDetail['max_passengers'];
            }
            if (strlen(strip_tags($form_data->story)) > $getMembershipDetail['max_story_characters']) {
                $msg ['story'] = "User can write upto ".$getMembershipDetail['max_story_characters']." characters story.";
            }
            if (empty($form_data->story)) {
                $msg ['story'] = $message['STORY_EMPTY'];
            }
            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg, 'family_history_id' => $family_history_id);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }


            /* End get membership detail for contact */


            $this->getTables()->updateCrmFamilyhistory($data_family_histories);
            if ($data_family_histories['is_status'] == 2 && $data_family_histories['user_id'] != '' && $data_family_histories['user_id'] != '0') {
                $preferenceData['user_id'] = $data_family_histories['user_id'];
                $preferenceData['family_history_privacy'] = $data_family_histories['is_email_on'];
                $this->getServiceLocator()->get('User\Model\UserTable')->updateCommunicationPreferences($preferenceData);
            }

            $i = 0;

            //..location_one_image ...

            $data_family_content1['location_no'] = 1;
            if ($form_data->location_one_image_id != '') {
                $location_one_content = $form_data->location_one_image_id;
                $content_type_one = 1;
            } elseif (isset($form_data->location_one_video) && $form_data->location_one_video != '') {
                $content_type_one = 2;
                $location_one_content = $form_data->location_one_video;
            } else {
                $location_one_content = '';
                $content_type_one = '';
            }

            $data_family_content1['family_history_id'] = $family_history_data[0]['family_history_id'];
            $data_family_content1['location_content'] = $location_one_content;
            $data_family_content1['content_type'] = $content_type_one;
            $data_family_content1['content_caption'] = ($form_data->location_one_caption != '') ? $form_data->location_one_caption : '';
            if (!isset($form_data->location_one_image_type) && !isset($form_data->location_one_video_type)) {
                ++$i;
                $data_family_content1['modified_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content1['modified_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_one_image_type) || isset($form_data->location_one_video_type)) {
                ++$i;
                $data_family_content1['added_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content1['added_date'] = DATE_TIME_FORMAT;
            }
            if (isset($family_content_data[1]['family_content_id'])) {
                $data_family_content1['family_content_id'] = $family_content_data[1]['family_content_id'];
                $this->getTables()->updateCrmFamilyContent($data_family_content1);
            } else {
                if ($data_family_content1['location_content'] != '' || $data_family_content1['content_caption'] != '') {
                    $data_family_content1['is_deleted'] = '0';
                    $this->getTables()->insertCrmFamilyContent($data_family_content1);
                }
            }

            //....end of location one image
            //..location_two_image ...

            $data_family_content2['location_no'] = 2;
            if ($form_data->location_two_image_id != '') {
                $location_two_content = $form_data->location_two_image_id;
                $content_type_two = 1;
            } elseif (isset($form_data->location_two_video) && $form_data->location_two_video != '') {
                $content_type_two = 2;
                $location_two_content = $form_data->location_two_video;
            } else {
                $location_two_content = '';
                $content_type_two = '';
            }

            $data_family_content2['family_history_id'] = $family_history_data[0]['family_history_id'];
            $data_family_content2['location_content'] = $location_two_content;
            $data_family_content2['content_type'] = $content_type_two;
            $data_family_content2['content_caption'] = ($form_data->location_two_caption != '') ? $form_data->location_two_caption : '';
            if (!isset($form_data->location_two_image_type) && !isset($form_data->location_two_video_type)) {
                ++$i;
                $data_family_content2['modified_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content2['modified_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_two_image_type) || isset($form_data->location_two_video_type)) {
                ++$i;
                $data_family_content2['added_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content2['added_date'] = DATE_TIME_FORMAT;
            }
            if (isset($family_content_data[2]['family_content_id'])) {
                $data_family_content2['family_content_id'] = $family_content_data[2]['family_content_id'];
                $this->getTables()->updateCrmFamilyContent($data_family_content2);
            } else {
                if ($data_family_content2['location_content'] != '' || $data_family_content2['content_caption'] != '') {
                    $data_family_content2['is_deleted'] = '0';
                    $this->getTables()->insertCrmFamilyContent($data_family_content2);
                }
            }

            //....end of location two image
            //..location_three_image ...

            $data_family_content3['location_no'] = 3;
            if ($form_data->location_three_image_id != '') {
                $location_three_content = $form_data->location_three_image_id;
                $content_type_three = 1;
            } elseif (isset($form_data->location_three_video) && $form_data->location_three_video != '') {
                $content_type_three = 2;
                $location_three_content = $form_data->location_three_video;
            } else {
                $location_three_content = '';
                $content_type_three = '';
            }

            $data_family_content3['family_history_id'] = $family_history_data[0]['family_history_id'];
            $data_family_content3['location_content'] = $location_three_content;
            $data_family_content3['content_type'] = $content_type_three;
            $data_family_content3['content_caption'] = ($form_data->location_three_caption != '') ? $form_data->location_three_caption : '';
            if (!isset($form_data->location_three_image_type) && !isset($form_data->location_three_video_type)) {
                ++$i;
                $data_family_content3['modified_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content3['modified_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_three_image_type) || isset($form_data->location_three_video_type)) {
                ++$i;
                $data_family_content3['added_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content3['added_date'] = DATE_TIME_FORMAT;
            }
            if (isset($family_content_data[3]['family_content_id'])) {
                $data_family_content3['family_content_id'] = $family_content_data[3]['family_content_id'];
                $this->getTables()->updateCrmFamilyContent($data_family_content3);
            } else {
                if ($data_family_content3['location_content'] != '' || $data_family_content3['content_caption'] != '') {
                    $data_family_content3['is_deleted'] = '0';
                    $this->getTables()->insertCrmFamilyContent($data_family_content3);
                }
            }

            //....end of location three image
            //..location_four_image ...

            $data_family_content4['location_no'] = 4;
            if ($form_data->location_four_image_id != '') {
                $location_four_content = $form_data->location_four_image_id;
                $content_type_four = 1;
            } elseif (isset($form_data->location_four_video) && $form_data->location_four_video != '') {
                $content_type_four = 2;
                $location_four_content = $form_data->location_four_video;
            } else {
                $location_four_content = '';
                $content_type_four = '';
            }

            $data_family_content4['family_history_id'] = $family_history_data[0]['family_history_id'];
            $data_family_content4['location_content'] = $location_four_content;
            $data_family_content4['content_type'] = $content_type_four;
            $data_family_content4['content_caption'] = ($form_data->location_four_caption != '') ? $form_data->location_four_caption : '';
            if (!isset($form_data->location_four_image_type) && !isset($form_data->location_four_video_type)) {
                ++$i;
                $data_family_content4['modified_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content4['modified_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_four_image_type) || isset($form_data->location_four_video_type)) {
                ++$i;
                $data_family_content4['added_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content4['added_date'] = DATE_TIME_FORMAT;
            }

            if (isset($family_content_data[4]['family_content_id'])) {
                $data_family_content4['family_content_id'] = $family_content_data[4]['family_content_id'];
                ;
                $this->getTables()->updateCrmFamilyContent($data_family_content4);
            } else {
                if ($data_family_content4['location_content'] != '' || $data_family_content4['content_caption'] != '') {
                    $data_family_content4['is_deleted'] = '0';
                    $this->getTables()->insertCrmFamilyContent($data_family_content4);
                }
            }

            //....end of location four image
            //..location_five_image ...

            $data_family_content5['location_no'] = 5;
            if ($form_data->location_five_image_id != '') {
                $location_five_content = $form_data->location_five_image_id;
                $content_type_five = 1;
            } elseif (isset($form_data->location_five_video) && $form_data->location_five_video != '') {
                $content_type_five = 2;
                $location_five_content = $form_data->location_five_video;
            } else {
                $location_five_content = '';
                $content_type_five = '';
            }

            $data_family_content5['family_history_id'] = $family_history_data[0]['family_history_id'];
            $data_family_content5['location_content'] = $location_five_content;
            $data_family_content5['content_type'] = $content_type_five;
            $data_family_content5['content_caption'] = ($form_data->location_five_caption != '') ? $form_data->location_five_caption : '';
            if (!isset($form_data->location_five_image_type) && !isset($form_data->location_five_video_type)) {
                ++$i;
                $data_family_content5['modified_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content5['modified_date'] = DATE_TIME_FORMAT;
            } elseif (isset($form_data->location_five_image_type) || isset($form_data->location_five_video_type)) {
                ++$i;
                $data_family_content5['added_by'] = $this->auth->getIdentity()->user_id;
                $data_family_content5['added_date'] = DATE_TIME_FORMAT;
            }

            if (isset($family_content_data[5]['family_content_id'])) {
                $data_family_content5['family_content_id'] = $family_content_data[5]['family_content_id'];

                $this->getTables()->updateCrmFamilyContent($data_family_content5);
            } else {
                if ($data_family_content5['location_content'] != '' || $data_family_content5['content_caption'] != '') {
                    $data_family_content5['is_deleted'] = '0';
                    $this->getTables()->insertCrmFamilyContent($data_family_content5);
                }
            }

            //....end of location five image
            if (isset($form_data->passenger_id_db) && !empty($form_data->passenger_id_db[0])) {

                foreach ($form_data->passenger_id_db as $key => $value) {
                    if ($form_data->passenger_id_db[$key] != '') {
                        $data_family_person = array();
                        $data_family_person['family_history_id'] = $family_history_data[0]['family_history_id'];
                        $data_family_person['passenger_id'] = $value;
                        $data_family_person['first_name'] = null;
                        $data_family_person['last_name'] = null;
                        $data_family_person['year_of_birth'] = null;
                        $data_family_person['birth_country_id'] = null;
                        $data_family_person['panel_no'] = $form_data->panel_no_db[$key];
                        $data_family_person['relationship_id'] = $form_data->relationship_id_db[$key];
                        $data_family_person['is_deleted'] = 0;
                        $data_family_person['added_by'] = $this->auth->getIdentity()->user_id;
                        $data_family_person['added_date'] = DATE_TIME_FORMAT;
                        $this->getTables()->insertCrmFamilyPerson($data_family_person);
                        unset($data_family_person);
                    }
                }
            }

            if (isset($form_data->first_name_manual) && !empty($form_data->first_name_manual[0])) {

                foreach ($form_data->first_name_manual as $key => $value) {
                    if ($form_data->first_name_manual[$key] != '') {
                        $data_family_person = array();
                        $data_family_person['family_history_id'] = $family_history_data[0]['family_history_id'];
                        $data_family_person['passenger_id'] = null;
                        $data_family_person['first_name'] = $value;
                        $data_family_person['last_name'] = $form_data->last_name_manual[$key];
                        $data_family_person['year_of_birth'] = trim($form_data->year_of_birth_manual[$key]);
                        $data_family_person['birth_country_id'] = $form_data->birth_country_id_manual[$key];
                        $data_family_person['panel_no'] = $form_data->panel_no_manual[$key];
                        $data_family_person['relationship_id'] = $form_data->relationship_id_manual[$key];
                        $data_family_person['is_deleted'] = 0;
                        $data_family_person['added_by'] = $this->auth->getIdentity()->user_id;
                        $data_family_person['added_date'] = DATE_TIME_FORMAT;
                        $this->getTables()->insertCrmFamilyPerson($data_family_person);
                        unset($data_family_person);
                    }
                }
            }
            $messages = array('status' => "success", 'message' => "Family history updated successfully", 'family_history_id' => $family_history_id);
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to view family history (front -> My File)
     * @param void
     * @return void
     * @author Icreon Tech - SK
     */
    public function viewUserFamilyHistoryDetailAction() {
        $params = $this->params()->fromRoute();
        $this->getConfig();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $params = $request->getPost()->toArray();
            $family_history_id = $params['id'];
            $params['family_history_id'] = $family_history_id;
            $familyHistory = $this->getTables()->getFamilyHistoryInfo($params);
            $history['familyHistory'] = $familyHistory[0];

            $familyPersonData = $this->getTables()->getUserFamilyPerson($params);
            $familyPerson = array();
            if (!empty($familyPersonData)) {
                foreach ($familyPersonData as $key => $value) {
                    if ($value['passenger_id'] != '') {
                        $passangerRecord = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getPassengerRecord(array('passenger_id' => $value['passenger_id']));

                        $passenger_name = (isset($passangerRecord[0])) ? ($passangerRecord[0]['PRIN_FIRST_NAME'] . " " . $passangerRecord[0]['PRIN_LAST_NAME']) : '';
                        $familyPerson[$key] = $value;
                        $familyPerson[$key]['passenger_name'] = $passenger_name;
                        $familyPerson[$key]['encPassengerId'] = $this->encrypt($value['passenger_id']);
                        $familyPerson[$key]['viewtab'] = $this->encrypt('passenger');
                        $familyPerson[$key]['year_of_birth'] = (isset($passangerRecord[0])) ? $passangerRecord[0]['BIRTH_YEAR'] : 'N/A';
                    } elseif ($value['first_name'] != '') {
                        $familyPerson[$key] = $value;
                        $familyPerson[$key]['passenger_name'] = 'N/A';
                    }
                }
            }
            $history['familyPerson'] = $familyPerson;
            $familyContent = array();
            $familyContentData = $this->getTables()->getUserFamilyContent($params);
            foreach ($familyContentData as $key1 => $value1) {
                $familyContent[$value1['location_no']] = $value1;
            }
            if(!empty($familyContent) && isset($familyContent[0]) && $familyContent[0]!='')
                ksort($familyContent);
            $history['familyContent'] = $familyContent;

            if ($history['familyHistory']['is_status'] == 0)
                $defaultFhEmailPermission = $history['familyHistory']['is_email_on'];
            else {
                $userPreferencesInfo = $this->getServiceLocator()->get('User\Model\ContactTable')->getCommunicationPreferences(array('user_id' => $history['familyHistory']['user_id']));
                $defaultFhEmailPermission = $userPreferencesInfo['family_history_privacy'];
            }

            $uploadedFilePath = $this->_config['file_upload_path']['assets_url'];
            $assetsUploadDir = $this->_config['file_upload_path']['assets_upload_dir'];
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'history' => $history,
                'assetsUploadDir' => $assetsUploadDir,
                'uploadedFilePath' => $uploadedFilePath,
                'defaultFhEmailPermission' => $defaultFhEmailPermission
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to delete saved search (front)
     * @param void
     * @return void
     * @author Icreon Tech - SK
     */
    public function deleteUserSearchAction() {
        $response = $this->getResponse();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $this->getTables()->deleteUserSearch($data);
            $messages = array('status' => "success", 'message' => "Data updated");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to save favorite histories (front)
     * @param void
     * @return void
     * @author Icreon Tech - SK
     */
    public function saveUserHistoriesAction() {
        $response = $this->getResponse();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $data['user_id'] = $this->auth->getIdentity()->user_id;
            $data['family_history_id'] = $this->decrypt($data['family_history_id']);
            $data['is_active'] = 1;
            $data['is_deleted'] = 0;
            $data['save_date'] = DATE_TIME_FORMAT;
            $data['modified_date'] = DATE_TIME_FORMAT;
            $this->getTables()->saveUserFamilyHistoryDetailSearch($data);
            $messages = array('status' => "success", 'message' => "Data saved");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to get more family histories (front)
     * @param void
     * @return void
     * @author Icreon Tech - SK
     */
    public function moreUserFamilyhistoryAction() {
        $auth = new AuthenticationService();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $searchParam1['sort_field'] = 'modified_date';
        $searchParam1['sort_order'] = 'desc';
        if ($auth->hasIdentity() === true) {
            /** logged - in user **/        
            $searchParam1['user_id'] = $this->auth->getIdentity()->user_id;
            /** end of logged - in user **/
        }else{
            /** guest user**/
            $searchParam1['user_id'] = '';
            /** end of guest user **/
        }

        if ($request->isXmlHttpRequest() && $request->isPost()) {
            
            parse_str($request->getPost('searchString'), $searchParam);
            if (isset($searchParam['contact_name']) && $searchParam['contact_name'] != '') {
                $page = $searchParam['page'];

                $numRecPerPage = 5;
                $limit = $searchParam1['record_limit'] = $numRecPerPage;
                $searchParam1['start_index'] = (($page- 1) * $numRecPerPage);
                $searchArray = array_merge($searchParam1, $searchParam);
                $familyHistoriesData = $this->getTables()->searchUserFamilyhistories($searchArray);
                
            } else {

                $page = $request->getPost('page');

                $numRecPerPage = 4;
                $limit = $searchParam1['record_limit'] = $numRecPerPage;
                $searchParam1['start_index'] = (($page - 1 ) * $numRecPerPage);
                
                if( isset($searchParam['is_featured']) && $searchParam['is_featured'] == '')
                        $searchParam1['is_featured'] = '';
                else
                        $searchParam1['is_featured'] = '1';
                $searchArray = array_merge($searchParam1, $searchParam);
                
                $familyHistoriesData = $this->getTables()->getUserFamilyhistories($searchArray);
            }
            $familyHistories = array();
            $familyPerson = array();
            if (!empty($familyHistoriesData)) {
                
                foreach ($familyHistoriesData as $index => $item) {
                    $familyHistories[$item['family_history_id']] = $item;
                }
                foreach ($familyHistories as $key => $value) {
                    $associatedPerson = $this->getTables()->getUserFamilyPerson(array('family_history_id' => $value['family_history_id']));
                    if (!empty($associatedPerson)) {
                        foreach ($associatedPerson as $key1 => $value1) {
                            if ($value1['passenger_id'] != '') {
                                $passangerRecord = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getPassengerRecord(array('passenger_id' => $value1['passenger_id']));

                                $passenger_name = (isset($passangerRecord[0])) ? ($passangerRecord[0]['PRIN_FIRST_NAME'] . " " . $passangerRecord[0]['PRIN_LAST_NAME']) : '';
                                $associatedPerson[$key1]['passenger_name'] = $passenger_name;
                            } else {
                                $associatedPerson[$key1]['passenger_name'] = '';
                            }
                        }
                    }
                    $familyPerson[$value['family_history_id']] = (empty($associatedPerson)) ? array() : $associatedPerson;
                    if ($value['user_id'] != '' && $value['user_id'] != '0') {
                        $contactInfo = $this->getServiceLocator()->get('Common\Model\CommonTable')->getUserCommonInfo($value);
                        $familyHistories[$key]['author'] = $contactInfo['full_name'];
                    }
                    else
                        $familyHistories[$key]['author'] = '';

                    $familyHistories[$key]['encryptFamilyHistoryId'] = $this->encrypt($value['family_history_id']);
                }
            }
            if (!empty($familyPerson))
                ksort($familyPerson);
            $history['familyHistories'] = $familyHistories;
            $history['familyPerson'] = $familyPerson;
            $uploadedFilePath = $this->_config['file_upload_path']['assets_url'];
            $assetsUploadDir = $this->_config['file_upload_path']['assets_upload_dir'];
            
            if(isset($searchParam['contact_name']) && !is_null($searchParam['contact_name']) && $searchParam['contact_name']!='')
                $contact_name = $searchParam['contact_name'];
            else
                $contact_name = '';
            if(isset($searchParam['birth_country_id']) && !is_null($searchParam['birth_country_id']) && $searchParam['birth_country_id']!='')
                $birth_country_id = $searchParam['birth_country_id'];
            else
                $birth_country_id = '';
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'history' => $history,
                'assetsUploadDir' => $assetsUploadDir,
                'uploadedFilePath' => $uploadedFilePath,
                'page' => $page,
                'limit' => $limit,
                'facebookAppId' => $this->_config['facebook_app']['app_id'],
                'contact_name' => $contact_name,
                'birth_country_id' => $birth_country_id,
                'jsLangTranslate' => $this->_config['PassengerMessages']['config']['user_family_history'],
				'allowed_IP' => $this->_config['allowed_ip']['ip']
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to search authorized or associated persons of Family Histories
     * @return json
     * @author Icreon Tech - SK
     */
    public function getAuthorisedAssociatedFamilyPersonAction() {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $post_arr = $request->getPost()->toArray();
        $contacts = array();
        if (!empty($post_arr) && $post_arr['contact_name'] != '') {
            $contactName = $post_arr['contact_name'];
            $associatedPersons = $this->getTables()->searchAuthorisedAssociatedPersons($post_arr);
            $i = 0;
            $full_name = '';
            foreach ($associatedPersons as $key => $value) {
                if ($value['first_name'] != '')
                    $full_name.= $value['first_name'];
                if ($value['middle_name'] != '')
                    $full_name.= " " . $value['middle_name'];
                if ($value['last_name'] != '')
                    $full_name.= ($full_name != '') ? " " . $value['last_name'] : $value['last_name'];

                if ($full_name == '' && $value['company_name'] != '')
                    $full_name = $value['company_name'];

                $contacts[$i]['full_name'] = $full_name;
                $contacts[$i]['email_id'] = ($value['email_id'] != '' && $value['email_id'] != 'null' && !is_null($value['email_id'])) ? $value['email_id'] : '';
                $full_name = '';
                $i++;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($contacts));
        return $response;
    }

}