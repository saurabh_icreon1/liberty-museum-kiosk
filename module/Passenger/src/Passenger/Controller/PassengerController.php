<?php

/**
 * This controller is used for Passenger module.
 * @package    Passenger
 * @author     Icreon Tech -SR.
 */

namespace Passenger\Controller;

use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Passenger\Model\Passenger;
use Passenger\Form\PassengerForm;
use Passenger\Form\PassengerCorrectionForm;
use Passenger\Form\PassengerAnnotationForm;
use Passenger\Form\ShipForm;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Authentication\AuthenticationService;
use Base\Model\SphinxComponent;
use Base\Model\SphinxClient;
use Zend\Session\Container as SessionContainer;

/**
 * This controller is used for Passenger module.
 * @author     Icreon Tech -SR.
 */
class PassengerController extends BaseController {

    protected $_passengerTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_authservice = null;

    /**
     * This is used for dafault initialization of objects.
     * @author     Icreon Tech -SR.
     */
    public function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->auth = $auth;
        //error_reporting(E_ALL);
    }

    /**
     * This Action is used for the index page of passenger having the listing 
     * @param this will pass an array getSearchArray user_id int, isActive enum.
     * @return this will be array $saveSearchArray having the saved search listing 
     * @author Icreon Tech -SR
     */
    public function indexAction() {
       
         $this->checkFrontUserAuthentication();
        $form = new PassengerForm();
        $this->getPassengerTable();
        $params = $this->params()->fromRoute();

        $searchId = '';
        if (isset($params['searchId']) && !empty($params['searchId'])) {
            $searchId = $params['searchId'];
        }
        $sessionPassSearch = new SessionContainer('passengerSearch');
        unset($sessionPassSearch->passenger_search);

        $getSearchArray = array();
        $ethnicityArray = $this->getPassengerTable()->getEthnicity($getSearchArray);
        $ethnicity_list = array();
        foreach ($ethnicityArray as $key => $val) {
            if (!empty($val['ethnicity'])) {
                $eth = trim(trim($val['ethnicity'], '-'), '.');
                $ethnicity_list[$eth] = $eth;
            }
        }
        ksort($ethnicity_list);
        array_unique($ethnicity_list);
        $form->get('ethnicity')->setAttribute('options', $ethnicity_list);

        $request = $this->getRequest();
        $isPosted = false;
        if ($request->isPost()) {
            $form->get('last_name')->setAttribute('value', $request->getPost('last_name'));
            $isPosted = true;
        }

        $gender = '';
        $maritalstatus = '';
        $searchTypeArray = array();

        $getSearchArray = array();
        $portArray = $this->getPassengerTable()->getArrivalPort($getSearchArray);
        $portList = array();
        $portList[' '] = 'Arrival Port';
        /* foreach ($portArray as $key => $val) {
          $portList[$val['ship_arrival_port']] = $val['ship_arrival_port'];
          } */
        $portList['New York'] = 'New York';
        $form->get('arrival_port')->setAttribute('options', $portList);
        $getSearchArray = array();
        $saveSearchArray = array();
        $membershipConfig = array();
        $userId = '';
        if (!empty($this->auth->getIdentity()->user_id)) {
            $userId = $this->auth->getIdentity()->user_id;
            $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->auth->getIdentity()->user_id));
            $membershipDetailArr = array();
            $savedSearchInDays = '';
            $savedSearchTypeIds = '';
            if ($userDetail['membership_id'] != '') {
                $membershipDetailArr[] = $userDetail['membership_id'];
                $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
                $savedSearchInDays = explode(',', $getMembershipDetail['savedSearchInDays']);
                $savedSearchTypeIds = explode(',', $getMembershipDetail['savedSearchTypeIds']);
                foreach ($savedSearchTypeIds as $key => $val) {
                    $membershipConfig[$val] = $savedSearchInDays[$key];
                }
            }
            $getSearchArray['user_id'] = $this->auth->getIdentity()->user_id;
            $getSearchArray['isActive'] = 1;
            $getSearchArray['search_type'] = 1;/**  passenger search */
            $saveSearchArray = $this->getPassengerTable()->getPassengerSearch($getSearchArray);
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array('form' => $form, 'searchArray' => $saveSearchArray,
            'membershipConfig' => $membershipConfig,
            'isPosted' => $isPosted,
            'loggedInUserId' => $userId,
            'searchId' => $this->decrypt($searchId),
            'maritalstatus' => $maritalstatus,
            'gender' => $gender,
            'searchTypeArray' => $searchTypeArray,
            'jsLangTranslate' => array_merge($this->_config['PassengerMessages']['config']['passenger_search'], $this->_config['PassengerMessages']['config']['passenger_details'], $this->_config['PassengerMessages']['config']['common'])));
        return $viewModel;
    }

    /**
     * This Action is used for the listing for search passengers
     * @return this will be array of passengers
     * @author Icreon Tech -SR
     */
    public function passengerResultAction() {
        $this->checkFrontUserAuthentication();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $this->getPassengerTable();
        $searchParam = '';
        $form = '';
        $viewModel = new ViewModel();
        $gender = '';
        $maritalstatus = '';
        $saveSearchArray = '';
        $searchTypeArray = array();
        $form = new PassengerForm();
        $isPosted = false;
        $searchParam = $request->getPost();
        $urlParams = $this->params()->fromRoute();
        $searchParam = $request->getPost()->toArray();
        $sessionPassSearch = new SessionContainer('passengerSearch');
        $sessionPassSearch->passenger_search = $searchParam;

        $searchId = '';
        if (isset($urlParams['search_id']) && !empty($urlParams['search_id'])) {
            $searchId = $urlParams['search_id'];
        }

        /*
          $sessionPassSearch = new SessionContainer();
          if (!empty($this->auth->getIdentity()->user_id)) {
          if (!isset($searchParam['submit'])) {
          $searchParam = unserialize($sessionPassSearch->passenger_search);
          if(isset($searchParam['searchString'])) {
          parse_str($searchParam['searchString'], $searchParam);
          }
          } else {
          $searchParam = $request->getPost()->toArray();
          }
          } else { echo "aaaa";
          $sessionPassSearch->passenger_search = serialize($searchParam);
          $searchParam = unserialize($sessionPassSearch->passenger_search);
          }
         */


//        asd($searchParam);
        $form->get('last_name')->setAttribute('value', isset($searchParam['last_name']) ? $searchParam['last_name'] : '');
        if ($request->isPost()) {
            $form->get('last_name')->setAttribute('value', $request->getPost('last_name'));
            $isPosted = true;
        }

        $getSearchArray = array();
        $ethnicityArray = $this->getPassengerTable()->getEthnicity($getSearchArray);
        $ethnicity_list = array();
        foreach ($ethnicityArray as $key => $val) {
            if (!empty($val['ethnicity'])) {
                $eth = trim(trim($val['ethnicity'], '-'), '.');
                $ethnicity_list[$eth] = $eth;
            }
        }
        ksort($ethnicity_list);
        array_unique($ethnicity_list);
        $form->get('ethnicity')->setAttribute('options', $ethnicity_list);
        $getSearchArray = array();
        $portArray = $this->getPassengerTable()->getArrivalPort($getSearchArray);
        $portList = array();
        $portList[' '] = 'Arrival Port';
        /* foreach ($portArray as $key => $val) {
          $portList[$val['ship_arrival_port']] = $val['ship_arrival_port'];
          } */
        $portList['New York'] = 'New York';
        $form->get('arrival_port')->setAttribute('options', $portList);

        if (isset($searchParam['search_type1']) && $searchParam['search_type1'] == 1) {
            $searchTypeArray[] = 'search_type1';
        }
        if (isset($searchParam['search_type2']) && $searchParam['search_type2'] == 1) {
            $searchTypeArray[] = 'search_type2';
        }
        if (isset($searchParam['search_type3']) && $searchParam['search_type3'] == 1) {
            $searchTypeArray[] = 'search_type3';
        }
        if (isset($searchParam['search_type4']) && $searchParam['search_type4'] == 1) {
            $searchTypeArray[] = 'search_type4';
        }
        if (isset($searchParam['search_type5']) && $searchParam['search_type5'] == 1) {
            $searchTypeArray[] = 'search_type5';
        }
        if (isset($searchParam['search_type6']) && $searchParam['search_type6'] == 1) {
            $searchTypeArray[] = 'search_type6';
        }
		if (isset($searchParam['search_type_town']) && $searchParam['search_type_town']) {
            $searchTypeTown[] = $searchParam['search_type_town'];
        }

        if (isset($searchParam['birth_year_from']))
            $searchParam['birth_year'] = $searchParam['birth_year_from'] . ' - ' . $searchParam['birth_year_to'];

        if (isset($searchParam['current_age_from']))
            $searchParam['current_age'] = $searchParam['current_age_from'] . ' - ' . $searchParam['current_age_to'];

        if (isset($searchParam['arrival_age_from']))
            $searchParam['arrival_age'] = $searchParam['arrival_age_from'] . ' - ' . $searchParam['arrival_age_to'];

        if (isset($searchParam['year_of_arrival_from']))
            $searchParam['year_of_arrival'] = $searchParam['year_of_arrival_from'] . ' - ' . $searchParam['year_of_arrival_to'];
        if (isset($searchParam['month_of_arrival_from']))
            $searchParam['month_of_arrival'] = $searchParam['month_of_arrival_from'] . ' - ' . $searchParam['month_of_arrival_to'];

        if (isset($searchParam['day_of_arrival_from']))
            $searchParam['day_of_arrival'] = $searchParam['day_of_arrival_from'] . ' - ' . $searchParam['day_of_arrival_to'];


        $form->get('birth_year_from')->setAttribute('value', isset($searchParam['birth_year_from']) ? $searchParam['birth_year_from'] : '');

        $form->get('birth_year_to')->setAttribute('value', isset($searchParam['birth_year_to']) ? $searchParam['birth_year_to'] : '');
        $form->get('current_age_from')->setAttribute('value', isset($searchParam['current_age_from']) ? $searchParam['current_age_from'] : '');
        $form->get('current_age_to')->setAttribute('value', isset($searchParam['current_age_to']) ? $searchParam['current_age_to'] : '');
        $form->get('arrival_age_from')->setAttribute('value', isset($searchParam['arrival_age_from']) ? $searchParam['arrival_age_from'] : '');
        $form->get('arrival_age_to')->setAttribute('value', isset($searchParam['arrival_age_to']) ? $searchParam['arrival_age_to'] : '');
        $form->get('year_of_arrival_from')->setAttribute('value', isset($searchParam['year_of_arrival_from']) ? $searchParam['year_of_arrival_from'] : '');
        $form->get('year_of_arrival_to')->setAttribute('value', isset($searchParam['year_of_arrival_to']) ? $searchParam['year_of_arrival_to'] : '');
        $form->get('month_of_arrival_from')->setAttribute('value', isset($searchParam['month_of_arrival_from']) ? $searchParam['month_of_arrival_from'] : '');
        $form->get('month_of_arrival_to')->setAttribute('value', isset($searchParam['month_of_arrival_to']) ? $searchParam['month_of_arrival_to'] : '');
        $form->get('day_of_arrival_from')->setAttribute('value', isset($searchParam['day_of_arrival_from']) ? $searchParam['day_of_arrival_from'] : '');
        $form->get('day_of_arrival_to')->setAttribute('value', isset($searchParam['day_of_arrival_to']) ? $searchParam['day_of_arrival_to'] : '');
        $form->get('place_of_birth')->setAttribute('value', isset($searchParam['place_of_birth']) ? $searchParam['place_of_birth'] : '');

        $gender = array();
        if (isset($searchParam['gender']))
            $gender = $searchParam['gender'];

        $maritalstatus = array();
        if (isset($searchParam['marital_status']))
            $maritalstatus = $searchParam['marital_status'];
        $form->get('initital_name')->setAttribute('value', isset($searchParam['initital_name']) ? $searchParam['initital_name'] : '');

        $form->get('town')->setAttribute('value', isset($searchParam['town']) ? trim($searchParam['town']) : '');
		$form->get('search_type_town')->setAttribute('value', isset($searchParam['search_type_town']) ? trim($searchParam['search_type_town']) : '');
		
        $form->get('ship_name')->setAttribute('value', isset($searchParam['ship_name']) ? trim($searchParam['ship_name']) : '');
        $form->get('port_of_departure')->setAttribute('value', isset($searchParam['port_of_departure']) ? trim($searchParam['port_of_departure']) : '');
        $form->get('arrival_port')->setAttribute('value', isset($searchParam['arrival_port']) ? trim($searchParam['arrival_port']) : '');
        $form->get('passenger_id')->setAttribute('value', isset($searchParam['passenger_id']) ? trim($searchParam['passenger_id']) : '');
        $form->get('first_name')->setAttribute('value', isset($searchParam['first_name']) ? trim($searchParam['first_name']) : '');
        $form->get('companion_name')->setAttribute('value', isset($searchParam['companion_name']) ? trim($searchParam['companion_name']) : '');
        $form->get('ethnicity_search')->setAttribute('value', isset($searchParam['ethnicity_search']) ? $searchParam['ethnicity_search'] : '');


        $ethnicity = array();
        if (isset($searchParam['ethnicity']))
            $ethnicity = $searchParam['ethnicity'];
        $form->get('ethnicity')->setAttribute('value', $ethnicity);

        $userId = '';
        $maxSearchCountPS = '';
        $membershipConfig = array();
        if (!empty($this->auth->getIdentity()->user_id)) {
            $userId = $this->auth->getIdentity()->user_id;
            $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->auth->getIdentity()->user_id));
            $membershipDetailArr = array();
            $savedSearchInDays = '';
            $savedSearchTypeIds = '';
            if ($userDetail['membership_id'] != '') {
                $membershipDetailArr[] = $userDetail['membership_id'];
                $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
                $savedSearchInDays = explode(',', $getMembershipDetail['savedSearchInDays']);
                $savedSearchTypeIds = explode(',', $getMembershipDetail['savedSearchTypeIds']);
                $maxSearchInDays = explode(',', $getMembershipDetail['maxSavedSearchInDays']);
                $searchSaveType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSearchSaveTypes();
                foreach ($searchSaveType as $index => $data) {
                    $searchSaveTypes[$data['saved_search_type']] = $data['saved_search_type_id'];
                }
                $saveSearchTypePS = $searchSaveTypes['Passenger Search'];
                foreach ($savedSearchTypeIds as $key => $val) {
                    if ($val == $saveSearchTypePS) {
                        $maxSearchCountPS = ($maxSearchInDays[$key] > $savedSearchInDays[$key]) ? $maxSearchInDays[$key] : $savedSearchInDays[$key];
                    }
                    $membershipConfig[$val] = $savedSearchInDays[$key];
                }
            }

            $getSearchArray['user_id'] = $this->auth->getIdentity()->user_id;
            $getSearchArray['isActive'] = 1;
            $getSearchArray['search_type'] = 1;/**  passenger search */
            $saveSearchArray = $this->getPassengerTable()->getPassengerSearch($getSearchArray);
        }
        $sort_field = '';
        $sort_order = '';
        if (isset($searchParam['sort_field'])) {
            if ($searchParam['sort_field'] != '') {
                $sort_field = $searchParam['sort_field'];
                $sort_order = $searchParam['sort_order'];
            }
        } else {
            $sort_field = 'field3';
            $sort_order = 'asc';
        }
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $getSearchArray['user_id'] = $this->auth->getIdentity()->user_id;
                $getSearchArray['isActive'] = 1;
                $getSearchArray['search_type'] = 1;
                $getSearchArray['search_id'] = $request->getPost('search_id');
                $seachResult = $this->getPassengerTable()->getPassengerSearch($getSearchArray);

                if ($request->getPost('returnSearchParam') == 'yes') {
                    $seachResult = json_encode(unserialize($seachResult[0]->search_query));
                    return $response->setContent($seachResult);
                    // return $seachResult;
                }
            }
        }
        //asd($request->getPost());
        if ($request->getPost('listing_type') != '')
            $listingType = $request->getPost('listing_type');


        $searchTypeFlag = 'no';
        $matchType = '';
        $msgArray = array_merge($this->_config['PassengerMessages']['config']['passenger_search'], $this->_config['PassengerMessages']['config']['passenger_details']);
        $userId = '';
        if (!empty($this->auth->getIdentity()->user_id))
            $userId = $this->auth->getIdentity()->user_id;
			
        $viewModel->setVariables(array(
            'membershipConfig' => $membershipConfig,
            'maxSearchCountPS' => $maxSearchCountPS,
            'jsLangTranslate' => $msgArray,
            'searchParam' => $searchParam,
            'searchTypeFlag' => $searchTypeFlag,
            'loggedInUserId' => $userId,
            'maritalStatusArray' => $this->maritalStatusArray(),
            'genderArray' => $this->genderArray(),
            'limit' => $this->_config['records_per_page']['numRecPerPage'],
            'sort_field' => $sort_field,
            'sort_order' => $sort_order,
            'page' => '1',
            'matchType' => $matchType,
            'form' => $form,
            'maritalstatus' => $maritalstatus,
            'gender' => $gender,
            'searchTypeArray' => $searchTypeArray,
            'searchTypeTown' => $searchTypeTown,
            'searchArray' => $saveSearchArray,
            'searchEthnicity' => $ethnicity,
            'is_logged_in' => $userId,
            'searchId' => $this->decrypt($searchId),
            'listingType' => $listingType
        ));
        return $viewModel;
    }

    /**
     * This action is used to search the passengers on the bases of search filter / on the bases of search title
     * @param this will be user_id (int) for existing search, 
     * @return this return all list of passengers
     * @author Icreon Tech -SR
     */
    public function getSearchPassengerAction() {

        $response = $this->getResponse();
        $request = $this->getRequest();
        $this->getPassengerTable();
        $searchParam = '';
        $viewModel = new ViewModel();
        $saveSearchArray = '';
        $searchTypeArray = array();
        parse_str($request->getPost('searchString'), $searchParam);
        $sessionPassSearch = new SessionContainer('passengerSearch');
        $sessionPassSearch->passenger_search = $searchParam;
        //$sessionPassSearch = new SessionContainer('passengerSearch');
        //asd($sessionPassSearch->passenger_search);
        //$sessionPassSearch->passenger_search = $searchParam;
        $viewModel->setTerminal(true);
        $sort_field = '';
        $sort_order = '';
        if (isset($searchParam['sort_field'])) {
            if ($searchParam['sort_field'] != '') {
                $sort_field = $searchParam['sort_field'];
                $sort_order = $searchParam['sort_order'];
            }
        }
        if ($request->getPost('search_id') == '') {
            $searchParam['birth_year'] = $searchParam['birth_year_from'] . ' - ' . $searchParam['birth_year_to'];
            $searchParam['current_age'] = $searchParam['current_age_from'] . ' - ' . $searchParam['current_age_to'];
            $searchParam['arrival_age'] = $searchParam['arrival_age_from'] . ' - ' . $searchParam['arrival_age_to'];
            $searchParam['year_of_arrival'] = $searchParam['year_of_arrival_from'] . ' - ' . $searchParam['year_of_arrival_to'];
            $searchParam['month_of_arrival'] = $searchParam['month_of_arrival_from'] . ' - ' . $searchParam['month_of_arrival_to'];
            $searchParam['day_of_arrival'] = $searchParam['day_of_arrival_from'] . ' - ' . $searchParam['day_of_arrival_to'];
        }
        $searchResult1 = array();
        $searchResult2 = array();
        $searchResult3 = array();
        $searchResult4 = array();
        $searchResult5 = array();
        $searchResult6 = array();
        $config = array();
        $limit = $this->_config['records_per_page']['numRecPerPageGridView'];
        if (@$searchParam['result_format'] == 'grid_view') {
            $config['pageSource'] = 'grid_view';
            $limit = $this->_config['records_per_page']['numRecPerPageGridView'];
        } else if (@$searchParam['result_format'] == 'list_view') {
            $config['pageSource'] = 'list_view';
            $limit = $this->_config['records_per_page']['numRecPerPageListView'];
        }

        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $getSearchArray['user_id'] = $this->auth->getIdentity()->user_id;
                $getSearchArray['isActive'] = 1;
                $getSearchArray['search_type'] = 1;
                $getSearchArray['search_id'] = $request->getPost('search_id');
                $seachResult = $this->getPassengerTable()->getPassengerSearch($getSearchArray);

                if ($request->getPost('returnSearchParam') == 'yes') {
                    $seachResult = json_encode(unserialize($seachResult[0]->search_query));
                    return $response->setContent($seachResult);
                    // return $seachResult;
                }
            }
        }
        $searchTypeFlag = 'no';
        $matchType = '';
        if (isset($searchParam['search_type1']) && $searchParam['search_type1'] == 1) {
            $searchResult1 = $this->getsphinxSearchPassenger($request, 'search_type1', $page = 1, $config);
            $searchTypeFlag = 'yes';
            $searchTypeArray[] = 'search_type1';
        }
        if (isset($searchParam['search_type2']) && $searchParam['search_type2'] == 1) {
            $searchResult2 = $this->getsphinxSearchPassenger($request, 'search_type2', $page = 1, $config);
            $searchTypeFlag = 'yes';
            $searchTypeArray[] = 'search_type2';
        }
        if (isset($searchParam['search_type3']) && $searchParam['search_type3'] == 1) {
            $searchResult3 = $this->getsphinxSearchPassenger($request, 'search_type3', $page = 1, $config);
            $searchTypeFlag = 'yes';
            $searchTypeArray[] = 'search_type3';
        }
        if (isset($searchParam['search_type4']) && $searchParam['search_type4'] == 1) {
            $searchResult4 = $this->getsphinxSearchPassenger($request, 'search_type4', $page = 1, $config);
            $searchTypeFlag = 'yes';
            $searchTypeArray[] = 'search_type4';
        }
        if (isset($searchParam['search_type5']) && $searchParam['search_type5'] == 1) {
            $searchResult5 = $this->getsphinxSearchPassenger($request, 'search_type5', $page = 1, $config);
            $searchTypeFlag = 'yes';
            $searchTypeArray[] = 'search_type5';
        }
        if (isset($searchParam['search_type6']) && $searchParam['search_type6'] == 1) {
            $searchResult6 = $this->getsphinxSearchPassenger($request, 'search_type6', $page = 1, $config);
            $searchTypeFlag = 'yes';
            $searchTypeArray[] = 'search_type6';
        }

        if ($searchTypeFlag == 'no') {
            $searchResult5 = array();
            $searchResult1 = $this->getsphinxSearchPassenger($request, 'search_type1', $page = 1, $config);

            if ($searchResult1['total'] == 0) {
                $searchResult5 = $this->getsphinxSearchPassenger($request, 'search_type5', $page = 1, $config);
                $matchType = 5;
                $searchTypeArray[] = 'search_type5';
            } else {
                $searchTypeFlag = 'yes';
                $searchParam['search_type1'] = 1;
                $searchTypeArray[] = 'search_type1';
                $matchType = 1;
            }
        }

        $msgArray = array_merge($this->_config['PassengerMessages']['config']['passenger_search'], $this->_config['PassengerMessages']['config']['passenger_details']);
        $userId = '';
        if (!empty($this->auth->getIdentity()->user_id))
            $userId = $this->auth->getIdentity()->user_id;
        $viewModel->setVariables(array(
            'jsLangTranslate' => $msgArray,
            'searchResult' => $searchResult1,
            'searchResult1' => $searchResult1,
            'searchResult2' => $searchResult2,
            'searchResult3' => $searchResult3,
            'searchResult4' => $searchResult4,
            'searchResult5' => $searchResult5,
            'searchResult6' => $searchResult6,
            'searchParam' => $searchParam,
            'searchTypeFlag' => $searchTypeFlag,
            'loggedInUserId' => $userId,
            'maritalStatusArray' => $this->maritalStatusArray(),
            'genderArray' => $this->genderArray(),
            'limit' => $limit,
            'sort_field' => $sort_field,
            'sort_order' => $sort_order,
            'page' => '1',
            'matchType' => $matchType,
            'searchTypeArray' => $searchTypeArray,
            'searchArray' => $saveSearchArray,
            'passengerRecordsView' => $searchParam['result_format']
        ));
        return $viewModel;
    }

    /**
     * This action is used to search the passengers on the bases of search filter / on the bases of search title
     * @param this will be user_id (int) for existing search, 
     * @return this return all list of passengers
     * @author Icreon Tech -SR
     */
    public function getSearchPassengerListAction() {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $this->getPassengerTable();
        $searchParam = '';
        parse_str($request->getPost('searchString'), $searchParam);

        if ($searchParam['result_format'] == 'grid_view') {
            $config['pageSource'] = 'grid_view';
        } else if ($searchParam['result_format'] == 'list_view') {
            $config['pageSource'] = 'list_view';
        }
        if (isset($config['pageSource']) && $config['pageSource'] == 'grid_view') {
            $limit = $this->_config['records_per_page']['numRecPerPageGridView'];
        } else if (isset($config['pageSource']) && $config['pageSource'] == 'list_view') {
            $limit = $this->_config['records_per_page']['numRecPerPageListView'];
        }

        $searchResult = $this->getsphinxSearchPassenger($request, 'search_type' . $searchParam['match_type'], $request->getPost('page'), $config);
        $userId = '';
        if (!empty($this->auth->getIdentity()->user_id))
            $userId = $this->auth->getIdentity()->user_id;
        $div_name = $searchParam['div_name'];
        $msgArray = array_merge($this->_config['PassengerMessages']['config']['passenger_search'], $this->_config['PassengerMessages']['config']['passenger_details']);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $msgArray,
            'searchResult' => $searchResult,
            'searchParam' => $searchParam,
            'loggedInUserId' => $userId,
            'type' => $searchParam['match_type'],
            'maritalStatusArray' => $this->maritalStatusArray(),
            'genderArray' => $this->genderArray(),
            'page' => $request->getPost('page'),
            // 'limit' => $this->_config['records_per_page']['numRecPerPage'],
            'limit' => $limit,
            'div_name' => $div_name,
            'passengerRecordsView' => $searchParam['result_format']
        ));
        return $viewModel;
    }

    /**
     * This action is used to get the listing of all saved search titles for search passenger
     * @param this will pass an array searchParam having the search parameters.
     * @return this will return array of saved pasenger search.
     * @author Icreon Tech -SR
     */
    public function getSavePassengerSearchAction() {
        $form = new PassengerForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $pageact = $request->getPost('page_act');
            $getSearchArray['user_id'] = $this->auth->getIdentity()->user_id;
            $getSearchArray['isActive'] = 1;
            $getSearchArray['search_type'] = 1;/**  passenger search */
            $searchArray = $this->getPassengerTable()->getPassengerSearch($getSearchArray);

            $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->auth->getIdentity()->user_id));
            $membershipDetailArr = array();
            $savedSearchInDays = '';
            $savedSearchTypeIds = '';
            if ($userDetail['membership_id'] != '') {
                $membershipDetailArr[] = $userDetail['membership_id'];
                $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
                $savedSearchInDays = explode(',', $getMembershipDetail['savedSearchInDays']);
                $savedSearchTypeIds = explode(',', $getMembershipDetail['savedSearchTypeIds']);
                $maxSearchInDays = explode(',', $getMembershipDetail['maxSavedSearchInDays']);
                $searchSaveType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSearchSaveTypes();
                foreach ($searchSaveType as $index => $data) {
                    $searchSaveTypes[$data['saved_search_type']] = $data['saved_search_type_id'];
                }
                $saveSearchTypePS = $searchSaveTypes['Passenger Search'];

                foreach ($savedSearchTypeIds as $key => $val) {
                    if ($val == $saveSearchTypePS) {
                        $maxSearchCountPS = ($maxSearchInDays[$key] > $savedSearchInDays[$key]) ? $maxSearchInDays[$key] : $savedSearchInDays[$key];
                    }
                    $membershipConfig[$val] = $savedSearchInDays[$key];
                }
                foreach ($savedSearchTypeIds as $key => $val) {
                    $membershipConfig[$val] = $savedSearchInDays[$key];
                }
            }

            return array('form' => $form, 'searchArray' => $searchArray, 'membershipConfig' => $membershipConfig, 'pageact' => $pageact, 'maxSearchCountPS' => $maxSearchCountPS);
        }
    }

    /**
     * This action is save to the search parameters into the table, 
     * @param this will pass an array searchParam having the search parameters.
     * @return this will confirmation message.
     * @author Icreon Tech -SR
     */
    public function saveSearchPassengerAction() {
        //$this->checkFrontUserAuthentication();
        $form = new PassengerForm();
        $this->getPassengerTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        if ($request->isPost()) {
            $response = $this->getResponse();
            $messages = array();
            parse_str($request->getPost('searchString'), $searchParam);
            if (trim($searchParam['search_name']) != '') {
                $searchParam2 = array(
                    'user_id' => $this->auth->getIdentity()->user_id,
                    'type' => 1,
                    'sort_field' => 'save_date',
                    'sort_order' => 'desc'
                );
                $passengerSearch = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserSearches($searchParam2);
                $searchCountResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $searchCount = $searchCountResult[0]->RecordCount;

                $maxallowedSearch = $this->getMembershipId($this->auth->getIdentity()->user_id, 'Passenger Search');
                if ($maxallowedSearch <= $searchCount) {
                    $messages = array('status' => "exceed");
                    return $response->setContent(\Zend\Json\Json::encode($messages));
                } else {
                    $searchParam['user_id'] = $this->auth->getIdentity()->user_id;
                    $searchParam['isActive'] = 1;
                    $searchParam['isDelete'] = 0;
                    $searchParam['search_type'] = 1;
                    if ($this->getPassengerTable()->savePassengerSearch($searchParam) == 1) {
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This action is used to delete the passenger search
     * @param this will pass an array dataParam search_id int(), user_id int, is_delete enum.
     * @return this will return the confirmation.
     * @author Icreon Tech -SR
     */
    function deleteSearchPassengerAction() {
        //$this->checkFrontUserAuthenticationAjx();
        $this->getPassengerTable();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $dataParam = array();
            $response = $this->getResponse();
            $messages = array();

            if ($request->getPost('search_id') != '') {
                $dataParam['search_id'] = $request->getPost('search_id');
                $auth = new AuthenticationService();
                $this->auth = $auth;
                $userDetail = (array) $this->auth->getIdentity();
                $dataParam['user_id'] = $userDetail['user_id'];
                $dataParam['is_delete'] = 1;
                $this->getPassengerTable()->deleteSavedPassengerSearch($dataParam);
                $messages = array('status' => "success");
                return $response->setContent(\Zend\Json\Json::encode($messages));
            }
        }
    }

    /**
     * This function is used to get table and config file object
     * @param array $form
     * @return     array 
     */
    public function getPassengerTable() {
        if (!$this->_passengerTable) {
            $sm = $this->getServiceLocator();
            $this->_passengerTable = $sm->get('Passenger\Model\PassengerTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
        }
        return $this->_passengerTable;
    }

    /**
     * This action is used to for search for ship
     * @param this will pass an array searchParam having the search parameters.
     * @return this will array array
     * @author Icreon Tech -SR
     */
    public function getSearchShipAction() {
        $this->getPassengerTable();
        //$request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $searchId = '';
        if (isset($params['searchId']) && !empty($params['searchId'])) {
            $searchId = $params['searchId'];
        }
        $userId = '';
        //$response = $this->getResponse();
        $saveSearchArray = array();
        $searchTypeArray = array();
        $membershipConfig = array();
        if (!empty($this->auth->getIdentity()->user_id)) {
            $userId = $this->auth->getIdentity()->user_id;
            $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->auth->getIdentity()->user_id));
            $membershipDetailArr = array();
            $savedSearchInDays = '';
            $savedSearchTypeIds = '';
            if ($userDetail['membership_id'] != '') {
                $membershipDetailArr[] = $userDetail['membership_id'];
                $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);

                $savedSearchInDays = explode(',', $getMembershipDetail['savedSearchInDays']);
                $savedSearchTypeIds = explode(',', $getMembershipDetail['savedSearchTypeIds']);
                foreach ($savedSearchTypeIds as $key => $val) {
                    $membershipConfig[$val] = $savedSearchInDays[$key];
                }
            }
            $getSearchArray['user_id'] = $this->auth->getIdentity()->user_id;
            $getSearchArray['isActive'] = 1;
            $getSearchArray['search_type'] = 2;/**  ship search */
            $saveSearchArray = $this->getPassengerTable()->getPassengerSearch($getSearchArray);
        }

        $form = new ShipForm();
        $isPosted = false;
        if (isset($params['ship_name']) && $params['ship_name'] != '') {
            $form->get('ship_name')->setAttribute('value', $this->decrypt($params['ship_name']));
            $isPosted = true;
        }
        $getSearchArray = array();
        $portArray = $this->getPassengerTable()->getArrivalPort($getSearchArray);
        $port_list = array();
        $port_list[' '] = 'Arrival Port';
        /* foreach ($portArray as $key => $val) {
          $port_list[$val['ship_arrival_port']] = $val['ship_arrival_port'];
          } */
        $port_list['New York'] = 'New York';
        $form->get('arrival_port')->setAttribute('options', $port_list);
        $viewModel = new ViewModel();
        $viewModel->setVariables(
                array('form' => $form,
                    'searchArray' => $saveSearchArray,
                    'membershipConfig' => $membershipConfig,
                    'loggedInUserId' => $userId,
                    'isPosted' => $isPosted,
                    'searchId' => $this->decrypt($searchId),
                    'searchTypeArray' => $searchTypeArray,
                    'jsLangTranslate' => array_merge($this->_config['PassengerMessages']['config']['ship_search'], $this->_config['PassengerMessages']['config']['passenger_details'], $this->_config['PassengerMessages']['config']['common'])));
        return $viewModel;
    }

    /**
     * This Action is used for the listing for search ships
     * @return this will be array of ships
     * @author Icreon Tech -SR
     */
    public function shipResultAction() {

        $response = $this->getResponse();
        $request = $this->getRequest();
        $this->getPassengerTable();
        $searchParam = '';
        $form = '';
        $viewModel = new ViewModel();
        $gender = '';
        $maritalstatus = '';
        $saveSearchArray = '';
        $searchTypeArray = array();
        $form = new ShipForm();
        $isPosted = false;


        if ($request->isPost()) {
            $searchParam = $request->getPost();
            $searchParam = $request->getPost()->toArray();
        } else {
            $searchParam = $this->params()->fromRoute();
            $searchParam['ship_name'] = isset($searchParam['ship_name']) ? $this->decrypt($searchParam['ship_name']) : '';
        }

        $sessionShipSearch = new SessionContainer();
        $sessionShipSearch->ship_search = $searchParam;



        $urlParams = $this->params()->fromRoute();
        $searchId = '';
        if (isset($urlParams['searchId']) && !empty($urlParams['searchId'])) {
            $searchId = $urlParams['searchId'];
        }

        /*
          $sessionShipSearch = new SessionContainer();
          if (!empty($this->auth->getIdentity()->user_id)) {
          if (!isset($searchParam['submit'])) {
          $searchParam = unserialize($sessionShipSearch->ship_search);

          parse_str($searchParam['searchString'], $searchParam);
          } else {
          $searchParam = $request->getPost()->toArray();
          }
          } else {
          $sessionShipSearch->ship_search = serialize($searchParam);
          $searchParam = unserialize($sessionShipSearch->ship_search);
          } */


        $form->get('ship_name')->setAttribute('value', $searchParam['ship_name']);
        if ($request->isPost()) {
            $form->get('ship_name')->setAttribute('value', $request->getPost('ship_name'));
            $isPosted = true;
        }


        $getSearchArray = array();

        $portArray = $this->getPassengerTable()->getArrivalPort($getSearchArray);
        $portList = array();
        $portList[' '] = 'Arrival Port';
        /* foreach ($portArray as $key => $val) {
          $portList[$val['ship_arrival_port']] = $val['ship_arrival_port'];
          } */
        $portList['New York'] = 'New York';
        $form->get('arrival_port')->setAttribute('options', $portList);
        if (isset($searchParam['search_type1']) && $searchParam['search_type1'] == 1) {
            $searchTypeArray[] = 'search_type1';
        }
        if (isset($searchParam['search_type2']) && $searchParam['search_type2'] == 1) {
            $searchTypeArray[] = 'search_type2';
        }
        if (isset($searchParam['search_type3']) && $searchParam['search_type3'] == 1) {
            $searchTypeArray[] = 'search_type3';
        }
        if (isset($searchParam['search_type4']) && $searchParam['search_type4'] == 1) {
            $searchTypeArray[] = 'search_type4';
        }
        if (isset($searchParam['search_type5']) && $searchParam['search_type5'] == 1) {
            $searchTypeArray[] = 'search_type5';
        }
        if (isset($searchParam['search_type6']) && $searchParam['search_type6'] == 1) {
            $searchTypeArray[] = 'search_type6';
        }

        if (isset($searchParam['year_of_arrival_from']))
            $searchParam['year_of_arrival'] = $searchParam['year_of_arrival_from'] . ' - ' . $searchParam['year_of_arrival_to'];
        if (isset($searchParam['month_of_arrival_from']))
            $searchParam['month_of_arrival'] = $searchParam['month_of_arrival_from'] . ' - ' . $searchParam['month_of_arrival_to'];
        if (isset($searchParam['day_of_arrival_from']))
            $searchParam['day_of_arrival'] = $searchParam['day_of_arrival_from'] . ' - ' . $searchParam['day_of_arrival_to'];
        $form->get('year_of_arrival_from')->setAttribute('value', isset($searchParam['year_of_arrival_from']) ? $searchParam['year_of_arrival_from'] : '');
        $form->get('year_of_arrival_to')->setAttribute('value', isset($searchParam['year_of_arrival_to']) ? $searchParam['year_of_arrival_to'] : '');
        $form->get('month_of_arrival_from')->setAttribute('value', isset($searchParam['month_of_arrival_from']) ? $searchParam['month_of_arrival_from'] : '');
        $form->get('month_of_arrival_to')->setAttribute('value', isset($searchParam['month_of_arrival_to']) ? $searchParam['month_of_arrival_to'] : '');
        $form->get('day_of_arrival_from')->setAttribute('value', isset($searchParam['day_of_arrival_from']) ? $searchParam['day_of_arrival_from'] : '');
        $form->get('day_of_arrival_to')->setAttribute('value', isset($searchParam['day_of_arrival_to']) ? $searchParam['day_of_arrival_to'] : '');
        $form->get('port_of_departure')->setAttribute('value', isset($searchParam['port_of_departure']) ? $searchParam['port_of_departure'] : '');
        $form->get('arrival_port')->setAttribute('value', isset($searchParam['arrival_port']) ? $searchParam['arrival_port'] : '');
        $form->get('ship_line')->setAttribute('value', isset($searchParam['ship_line']) ? $searchParam['ship_line'] : '');
        $form->get('ship_builder')->setAttribute('value', isset($searchParam['ship_builder']) ? $searchParam['ship_builder'] : '');
        $userId = '';
        $membershipConfig = array();
        if (!empty($this->auth->getIdentity()->user_id)) {
            /* $userId = $this->auth->getIdentity()->user_id;
              $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->auth->getIdentity()->user_id));
              $membershipDetailArr = array();
              $savedSearchInDays = '';
              $savedSearchTypeIds = '';
              if ($userDetail['membership_id'] != '') {
              $membershipDetailArr[] = $userDetail['membership_id'];
              $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
              $savedSearchInDays = explode(',', $getMembershipDetail['savedSearchInDays']);
              $savedSearchTypeIds = explode(',', $getMembershipDetail['savedSearchTypeIds']);
              $maxSearchInDays = explode(',', $getMembershipDetail['maxSavedSearchInDays']);
              $searchSaveType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSearchSaveTypes();
              foreach($searchSaveType as $index=>$data){
              $searchSaveTypes[$data['saved_search_type']] = $data['saved_search_type_id'];
              }
              $saveSearchTypePS = $searchSaveTypes['Ship Search'];
              foreach ($savedSearchTypeIds as $key => $val) {
              if ($val == $saveSearchTypePS) {
              $maxSearchCountPS = ($maxSearchInDays[$key]>$savedSearchInDays[$key])?$maxSearchInDays[$key]:$savedSearchInDays[$key];

              }
              $membershipConfig[$val] = $savedSearchInDays[$key];
              }
              } */
            $getSearchArray['user_id'] = $this->auth->getIdentity()->user_id;
            $getSearchArray['isActive'] = 1;
            $getSearchArray['search_type'] = 2;/**  ship search */
            $saveSearchArray = $this->getPassengerTable()->getPassengerSearch($getSearchArray);
        }
        $sort_field = '';
        $sort_order = '';
        if (isset($searchParam['sort_field'])) {
            if ($searchParam['sort_field'] != '') {
                $sort_field = $searchParam['sort_field'];
                $sort_order = $searchParam['sort_order'];
            }
        }

        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $getSearchArray['user_id'] = $this->auth->getIdentity()->user_id;
                $getSearchArray['isActive'] = 1;
                $getSearchArray['search_type'] = 2;
                $getSearchArray['search_id'] = $request->getPost('search_id');
                $seachResult = $this->getPassengerTable()->getPassengerSearch($getSearchArray);

                if ($request->getPost('returnSearchParam') == 'yes') {
                    $seachResult = json_encode(unserialize($seachResult[0]->search_query));
                    return $response->setContent($seachResult);
                    // return $seachResult;
                }
            }
        }
        $searchTypeFlag = 'no';
        $matchType = '';

        $msgArray = array_merge($this->_config['PassengerMessages']['config']['ship_search'], $this->_config['PassengerMessages']['config']['common']);
        $userId = '';
        if (!empty($this->auth->getIdentity()->user_id))
            $userId = $this->auth->getIdentity()->user_id;
        $viewModel->setVariables(array(
            #'membershipConfig' => $membershipConfig,
            #'maxSearchCountPS' => $maxSearchCountPS,
            'jsLangTranslate' => $msgArray,
            'searchParam' => $searchParam,
            'searchTypeFlag' => $searchTypeFlag,
            'loggedInUserId' => $userId,
            'limit' => $this->_config['records_per_page']['numRecPerPage'],
            'sort_field' => $sort_field,
            'sort_order' => $sort_order,
            'page' => '1',
            'matchType' => $matchType,
            'form' => $form,
            'searchTypeArray' => $searchTypeArray,
            'searchArray' => $saveSearchArray,
            'is_logged_in' => $userId,
            'searchId' => $this->decrypt($searchId)
        ));
        return $viewModel;
    }

    /**
     * This action is used to for search for ship
     * @param this will pass an array searchParam having the search parameters.
     * @return this will array search listing 
     * @author Icreon Tech -SR
     */
    public function shipListAction() {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $this->getPassengerTable();
        $searchParam = '';
        parse_str($request->getPost('searchString'), $searchParam);
        $searchResult1 = array();
        $searchResult2 = array();
        $searchResult3 = array();
        $searchResult4 = array();
        $searchResult5 = array();
        $searchResult6 = array();

        $sessionShipSearch = new SessionContainer();
        $sessionShipSearch->ship_search = $searchParam;

        $sort_field = '';
        $sort_order = '';

        if (isset($searchParam['sort_field'])) {
            if ($searchParam['sort_field'] != '') {
                $sort_field = $searchParam['sort_field'];
                $sort_order = $searchParam['sort_order'];
            }
        }
        $userId = '';
        if (!empty($this->auth->getIdentity()->user_id))
            $userId = $this->auth->getIdentity()->user_id;

        $config = array();
        $limit = $this->_config['records_per_page']['numRecPerPageGridView'];
        if (@$searchParam['result_format'] == 'grid_view') {
            $config['pageSource'] = 'grid_view';
            $limit = $this->_config['records_per_page']['numRecPerPageGridView'];
        } else if (@$searchParam['result_format'] == 'list_view') {
            $config['pageSource'] = 'list_view';
            $limit = $this->_config['records_per_page']['numRecPerPageListView'];
        }

        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $getSearchArray['user_id'] = $this->auth->getIdentity()->user_id;
                $getSearchArray['isActive'] = 1;
                $getSearchArray['search_type'] = 2;
                $getSearchArray['search_id'] = $request->getPost('search_id');
                $seachResult = $this->getPassengerTable()->getPassengerSearch($getSearchArray);

                if ($request->getPost('returnSearchParam') == 'yes') {
                    $seachResult = json_encode(unserialize($seachResult[0]->search_query));
                    return $response->setContent($seachResult);
                    // return $seachResult;
                }
            }
        }
        $searchTypeFlag = 'no';
        if (isset($searchParam['search_type1']) && $searchParam['search_type1'] == 1) {
            $searchResult1 = $this->getsphinxSearchShip($request, 'search_type1', $page = 1, $config);
            $searchTypeFlag = 'yes';
        }
        if (isset($searchParam['search_type2']) && $searchParam['search_type2'] == 1) {
            $searchResult2 = $this->getsphinxSearchShip($request, 'search_type2', $page = 1, $config);
            $searchTypeFlag = 'yes';
        }
        if (isset($searchParam['search_type3']) && $searchParam['search_type3'] == 1) {
            $searchResult3 = $this->getsphinxSearchShip($request, 'search_type3', $page = 1, $config);
            $searchTypeFlag = 'yes';
        }
        if (isset($searchParam['search_type4']) && $searchParam['search_type4'] == 1) {
            $searchResult4 = $this->getsphinxSearchShip($request, 'search_type4', $page = 1, $config);
            $searchTypeFlag = 'yes';
        }
        if (isset($searchParam['search_type5']) && $searchParam['search_type5'] == 1) {
            $searchResult5 = $this->getsphinxSearchShip($request, 'search_type5', $page = 1, $config);
            $searchTypeFlag = 'yes';
        }
        if (isset($searchParam['search_type6']) && $searchParam['search_type6'] == 1) {
            $searchResult6 = $this->getsphinxSearchShip($request, 'search_type6', $page = 1, $config);
            $searchTypeFlag = 'yes';
        }
        $matchType = '';
        if ($searchTypeFlag == 'no') {
            $searchResult5 = array();
            $searchResult1 = $this->getsphinxSearchShip($request, 'search_type1', $page = 1, $config);

            if ($searchResult1['total'] == 0) {
                $searchResult5 = $this->getsphinxSearchShip($request, 'search_type5', $page = 1, $config);
                $matchType = 5;
            } else {
                $searchTypeFlag = 'yes';
                $searchParam['search_type1'] = 1;
                $matchType = 1;
            }
        }

        $msgArray = array_merge($this->_config['PassengerMessages']['config']['ship_search'], $this->_config['PassengerMessages']['config']['common']);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);

        $viewModel->setVariables(array(
            'jsLangTranslate' => $msgArray,
            'searchResult' => $searchResult1,
            'searchResult1' => $searchResult1,
            'searchResult2' => $searchResult2,
            'searchResult3' => $searchResult3,
            'searchResult4' => $searchResult4,
            'searchResult5' => $searchResult5,
            'searchResult6' => $searchResult6,
            'searchParam' => $searchParam,
            'searchTypeFlag' => $searchTypeFlag,
            'loggedInUserId' => $userId,
            'sort_field' => $sort_field,
            'sort_order' => $sort_order,
            'page' => 1,
            'limit' => $limit,
            'matchType' => $matchType,
            'shipPath' => $this->_config['file_upload_path']['assets_url'].'ship_image/',
            'shipRecordsView' => isset($searchParam['result_format']) ? $searchParam['result_format'] : ""
        ));
        return $viewModel;
    }

    /**
     * This action is used to save the search parameters into the table, 
     * @param this will pass an array searchParam having the search parameters.
     * @return this will confirmation message.
     * @author Icreon Tech -SR
     */
    public function saveSearchShipAction() {
        $form = new PassengerForm();
        $this->getPassengerTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        if ($request->isPost()) {
            parse_str($request->getPost('searchString'), $searchParam);
            if (trim($searchParam['search_name']) != '') {
                $searchParam['user_id'] = $this->auth->getIdentity()->user_id;
                $searchParam['isActive'] = 1;
                $searchParam['isDelete'] = 0;
                $searchParam['search_type'] = 2;/**  ship search */
                $getSearchArray = array();
                $getSearchArray['user_id'] = $this->auth->getIdentity()->user_id;
                $getSearchArray['isActive'] = 1;
                $getSearchArray['search_type'] = 2;/**  ship search */
                $saveSearchArray = $this->getPassengerTable()->getPassengerSearch($getSearchArray);

                $userId = $this->auth->getIdentity()->user_id;
                $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->auth->getIdentity()->user_id));
                $membershipDetailArr = array();
                $savedSearchInDays = '';
                $savedSearchTypeIds = '';
                $maxSearchCountPS = null;
                if ($userDetail['membership_id'] != '') {
                    $membershipDetailArr[] = $userDetail['membership_id'];
                    $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
                    $savedSearchInDays = explode(',', $getMembershipDetail['savedSearchInDays']);
                    $savedSearchTypeIds = explode(',', $getMembershipDetail['savedSearchTypeIds']);
                    $maxSearchInDays = explode(',', $getMembershipDetail['maxSavedSearchInDays']);
                    $searchSaveType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSearchSaveTypes();
                    foreach ($searchSaveType as $index => $data) {
                        $searchSaveTypes[$data['saved_search_type']] = $data['saved_search_type_id'];
                    }
                    $saveSearchTypePS = $searchSaveTypes['Ship Search'];
                    foreach ($savedSearchTypeIds as $key => $val) {
                        if ($val == $saveSearchTypePS) {
                            $maxSearchCountPS = ($maxSearchInDays[$key] > $savedSearchInDays[$key]) ? $maxSearchInDays[$key] : $savedSearchInDays[$key];
                        }
                    }
                }
                if ($maxSearchCountPS <= count($saveSearchArray)) {
                    $messages = array('status' => "exceed");
                    return $response->setContent(\Zend\Json\Json::encode($messages));
                } elseif ($this->getPassengerTable()->savePassengerSearch($searchParam) == true) {
                    $messages = array('status' => "success");
                    return $response->setContent(\Zend\Json\Json::encode($messages));
                }
            }
        }
    }

    /**
     * This action is used to get the listing of all saved search titles for ship search 
     * @param this will pass an array searchParam having the search parameters.
     * @return this will return array of saved ship search.
     * @author Icreon Tech -SR
     */
    public function getSaveShipSearchAction() {
        $form = new PassengerForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $pageact = $request->getPost('page_act');
            $getSearchArray['user_id'] = $this->auth->getIdentity()->user_id;
            $getSearchArray['isActive'] = 1;
            $getSearchArray['search_type'] = 2;/**  ship search */
            $searchArray = $this->getPassengerTable()->getPassengerSearch($getSearchArray);

            return array('form' => $form, 'searchArray' => $searchArray, 'pageact' => $pageact);
        }
    }

    /**
     * This action is used to delete the ship search
     * @param this will pass an array dataParam search_id int(), user_id int, is_delete enum.
     * @return this will return the confirmation.
     * @author Icreon Tech -SR
     */
    function deleteSearchShipAction() {
        $this->getPassengerTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        if ($request->isPost()) {
            $dataParam = array();
            if ($request->getPost('search_id') != '') {
                $dataParam['search_id'] = $request->getPost('search_id');
                $dataParam['user_id'] = $this->auth->getIdentity()->user_id;
                $dataParam['is_delete'] = 1;
                $this->getPassengerTable()->deleteSavedPassengerSearch($dataParam);
                $messages = array('status' => "success");
                return $response->setContent(\Zend\Json\Json::encode($messages));
            }
        }
    }

    /**
     * This Action is used get the ship listing
     * @param this will pass an array getSearchArray user_id int, isActive enum.
     * @return this will be array of ship names
     * @author Icreon Tech -SR
     */
    public function getShipAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {

            $getSearchArray['ship_name'] = $request->getPost('ship_name');
            $shipArray = $this->getPassengerTable()->getShip($getSearchArray);
            $ship_list = array();

            foreach ($shipArray as $key => $val) {
                $ship_list[$val['ship_name']] = $val['ship_name'];
            }
            return $response->setContent(\Zend\Json\Json::encode($ship_list));
        }
    }

    /**
     * This Action is used get the depart port listing
     * @return this will be array of ship names
     * @author Icreon Tech -SR
     */
    public function getDeparturePortAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $getSearchArray['port_of_departure'] = $request->getPost('port_of_departure');
            $getSearchArray['limit'] = 10;
            $portArray = $this->getPassengerTable()->getDeparturePort($getSearchArray);
            $port_list = array();
            $allRecords = array();
            $i = 0;
            if ($portArray !== false) {
                foreach ($portArray as $resultarray) {
                    $allRecords[$i] = array();
                    $allRecords[$i]['port_of_departure'] = $resultarray['departure_port'];
                    $i++;
                }
            }
            // foreach ($portArray as $key => $val) {
            //     $port_list[$val['departure_port']] = $val['departure_port'];
            // }
            return $response->setContent(\Zend\Json\Json::encode($allRecords));
        }
    }

    /**
     * This Action is used get the arrival port listing 
     * @param this will pass an array getSearchArray user_id int, isActive enum.
     * @return this will be array of ship arrival posrt listing
     * @author Icreon Tech -SR
     */
    public function getShipArrivalPortAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $getSearchArray['ship_name'] = $request->getPost('ship_name');
            $shipArray = $this->getServiceLocator()->get('Common\Model\ArrivalportTable')->getShipArrivalPort($getSearchArray);
            $ship_list = array();

            foreach ($shipArray as $key => $val) {
                $ship_list[$val['ship_name']] = $val['ship_name'];
            }
            return $response->setContent(\Zend\Json\Json::encode($ship_list));
        }
    }

    /**
     * This Action is used search the passenger
     * @param this will pass an array getSearchArray user_id int, isActive enum.
     * @return this will be array of ship arrival posrt listing
     * @author Icreon Tech -SR
     */
    public function searchPassengerAction() {
        $this->layout('crm');
        $this->getPassengerTable();
        $request = $this->getRequest();
        parse_str($request->getPost('searchString'), $searchParam);
        $seachResult = $this->getPassengerTable()->fetchAll($searchParam);
    }

    /**
     * This Action is used get the passenger details
     * @param will be the passenger id
     * @return this will be array of passenger details
     * @author Icreon Tech -SR
     */
    public function passengerDetailsAction() {
        $this->checkFrontUserAuthentication();
        // $this->checkFrontUserAuthentication();
        //  $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $form = new PassengerForm();
        $manifestFile = '';
        $manifestFileEnc = '';
        $gender = '';
        $maritalstatus = '';
        $saveSearchArray = '';
        $searchTypeArray = array();

        $sessionPassSearch = new SessionContainer('passengerSearch');
        //  asd($sessionPassSearch->passenger_search);
        $searchParam = $sessionPassSearch->passenger_search;
        // asd($searchParam);

        $form->get('last_name')->setAttribute('value', $searchParam['last_name']);


        if (isset($searchParam['birth_year_from']))
            $searchParam['birth_year'] = $searchParam['birth_year_from'] . ' - ' . $searchParam['birth_year_to'];

        if (isset($searchParam['current_age_from']))
            $searchParam['current_age'] = $searchParam['current_age_from'] . ' - ' . $searchParam['current_age_to'];

        if (isset($searchParam['arrival_age_from']))
            $searchParam['arrival_age'] = $searchParam['arrival_age_from'] . ' - ' . $searchParam['arrival_age_to'];

        if (isset($searchParam['year_of_arrival_from']))
            $searchParam['year_of_arrival'] = $searchParam['year_of_arrival_from'] . ' - ' . $searchParam['year_of_arrival_to'];
        if (isset($searchParam['month_of_arrival_from']))
            $searchParam['month_of_arrival'] = $searchParam['month_of_arrival_from'] . ' - ' . $searchParam['month_of_arrival_to'];

        if (isset($searchParam['day_of_arrival_from']))
            $searchParam['day_of_arrival'] = $searchParam['day_of_arrival_from'] . ' - ' . $searchParam['day_of_arrival_to'];



        $form->get('birth_year_from')->setAttribute('value', isset($searchParam['birth_year_from']) ? $searchParam['birth_year_from'] : '');
        $form->get('birth_year_to')->setAttribute('value', isset($searchParam['birth_year_to']) ? $searchParam['birth_year_to'] : '');
        $form->get('current_age_from')->setAttribute('value', isset($searchParam['current_age_from']) ? $searchParam['current_age_from'] : '');
        $form->get('current_age_to')->setAttribute('value', isset($searchParam['current_age_to']) ? $searchParam['current_age_to'] : '');
        $form->get('arrival_age_from')->setAttribute('value', isset($searchParam['arrival_age_from']) ? $searchParam['arrival_age_from'] : '');
        $form->get('arrival_age_to')->setAttribute('value', isset($searchParam['arrival_age_to']) ? $searchParam['arrival_age_to'] : '');
        $form->get('year_of_arrival_from')->setAttribute('value', isset($searchParam['year_of_arrival_from']) ? $searchParam['year_of_arrival_from'] : '');
        $form->get('year_of_arrival_to')->setAttribute('value', isset($searchParam['year_of_arrival_to']) ? $searchParam['year_of_arrival_to'] : '');
        $form->get('month_of_arrival_from')->setAttribute('value', isset($searchParam['month_of_arrival_from']) ? $searchParam['month_of_arrival_from'] : '');
        $form->get('month_of_arrival_to')->setAttribute('value', isset($searchParam['month_of_arrival_to']) ? $searchParam['month_of_arrival_to'] : '');
        $form->get('day_of_arrival_from')->setAttribute('value', isset($searchParam['day_of_arrival_from']) ? $searchParam['day_of_arrival_from'] : '');
        $form->get('day_of_arrival_to')->setAttribute('value', isset($searchParam['day_of_arrival_to']) ? $searchParam['day_of_arrival_to'] : '');
        $form->get('place_of_birth')->setAttribute('value', isset($searchParam['place_of_birth']) ? $searchParam['place_of_birth'] : '');

        $form->get('initital_name')->setAttribute('value', isset($searchParam['initital_name']) ? $searchParam['initital_name'] : '');

        $form->get('town')->setAttribute('value', isset($searchParam['town']) ? trim($searchParam['town']) : '');
        $form->get('ship_name')->setAttribute('value', isset($searchParam['ship_name']) ? trim($searchParam['ship_name']) : '');
        $form->get('port_of_departure')->setAttribute('value', isset($searchParam['port_of_departure']) ? trim($searchParam['port_of_departure']) : '');
        $form->get('arrival_port')->setAttribute('value', isset($searchParam['arrival_port']) ? trim($searchParam['arrival_port']) : '');
        $form->get('passenger_id')->setAttribute('value', isset($searchParam['passenger_id']) ? trim($searchParam['passenger_id']) : '');
        $form->get('first_name')->setAttribute('value', isset($searchParam['first_name']) ? trim($searchParam['first_name']) : '');
        $form->get('companion_name')->setAttribute('value', isset($searchParam['companion_name']) ? trim($searchParam['companion_name']) : '');
        $form->get('ethnicity_search')->setAttribute('value', isset($searchParam['ethnicity_search']) ? $searchParam['ethnicity_search'] : '');
        $ethnicity = array();
        if (isset($searchParam['ethnicity']))
            $ethnicity = $searchParam['ethnicity'];
        $form->get('ethnicity')->setAttribute('value', $ethnicity);



        if (isset($params['manifest_file']) && $params['manifest_file'] != '') {
            $manifestFile = $this->decrypt($params['manifest_file']);
            $manifestFileEnc = $params['manifest_file'];
        }

        if (isset($searchParam['search_type1']) && $searchParam['search_type1'] == 1) {
            $searchTypeArray[] = 'search_type1';
        }
        if (isset($searchParam['search_type2']) && $searchParam['search_type2'] == 1) {
            $searchTypeArray[] = 'search_type2';
        }
        if (isset($searchParam['search_type3']) && $searchParam['search_type3'] == 1) {
            $searchTypeArray[] = 'search_type3';
        }
        if (isset($searchParam['search_type4']) && $searchParam['search_type4'] == 1) {
            $searchTypeArray[] = 'search_type4';
        }
        if (isset($searchParam['search_type5']) && $searchParam['search_type5'] == 1) {
            $searchTypeArray[] = 'search_type5';
        }
        if (isset($searchParam['search_type6']) && $searchParam['search_type6'] == 1) {
            $searchTypeArray[] = 'search_type6';
        }

        $ethnicity = array();

        $gender = array();
        if (isset($searchParam['gender']))
            $gender = $searchParam['gender'];

        $maritalstatus = array();
        if (isset($searchParam['marital_status']))
            $maritalstatus = $searchParam['marital_status'];


        //$getSearchArray = array();
        $ethnicityArray = $this->getPassengerTable()->getEthnicity($getSearchArray);
        $ethnicity_list = array();
        foreach ($ethnicityArray as $key => $val) {
            if (!empty($val['ethnicity'])) {
                $eth = trim(trim($val['ethnicity'], '-'), '.');
                $ethnicity_list[$eth] = $eth;
            }
        }
        ksort($ethnicity_list);
        array_unique($ethnicity_list);
        $form->get('ethnicity')->setAttribute('options', $ethnicity_list);




        $getSearchArray = array();
        $portArray = $this->getPassengerTable()->getArrivalPort($getSearchArray);
        $portList = array();
        $portList[' '] = 'Arrival Port';
        /* foreach ($portArray as $key => $val) {
          $portList[$val['ship_arrival_port']] = $val['ship_arrival_port'];
          } */
        $portList['New York'] = 'New York';
        $form->get('arrival_port')->setAttribute('options', $portList);
        $this->getPassengerTable();
        $passengerId = $this->decrypt($params['id']);
        $msgArray = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['passenger_details'], $this->_config['PassengerMessages']['config']['passenger_search']);
        $searchParam['passenger_id'] = $passengerId;
        $searchResult = $this->getPassengerTable()->getPassengerRecord($searchParam);
        $passengerManifestResult = $this->getPassengerTable()->getPassengerManifest($searchParam);
        $shipResult = array();
        $searchShipParam = array();
        $shipPath = '';

        if (!empty($searchResult[0]['SHIP_NAME'])) {
            $searchShipParam['ship_name'] = $searchResult[0]['SHIP_NAME'];
            $searchShipParam['asset_for'] = '1';
            $shipResult = $this->getPassengerTable()->getShipDetails($searchShipParam);
            if (count($shipResult) > 0) {
                $shipResult = $shipResult[0];
                if (!empty($shipResult['ASSETVALUE']))
                    $shipPath = $this->_config['file_upload_path']['assets_url'].'ship_image/'. $shipResult['ASSETVALUE'];
					$shipfilePath =  $this->_config['file_upload_path']['assets_upload_dir'].'ship_image/' . $shipResult['ASSETVALUE'];
            }
        }

        $correctionArray = array();
        $searchParam['user_id'] = (isset($this->auth->getIdentity()->user_id) and trim($this->auth->getIdentity()->user_id) != "") ? $this->auth->getIdentity()->user_id : "";
        $searchParam['passenger_id'] = $passengerId;
        $searchParam['activity_type'] = 1; // annotation
        //$searchParam['status'] = 0; // Pending status
        $annotationResult = $this->_passengerTable->getPassengerCorrectionDetails($searchParam);
        $annotationCorrectionId = 0;
        if (count($annotationResult) > 0) {
            $annotationCorrectionId = $annotationResult[0]['annotation_correction_id'];
        }

        $ethnicity = array();
        if (isset($searchParam['ethnicity']))
            $ethnicity = $searchParam['ethnicity'];
        $form->get('ethnicity')->setAttribute('value', $ethnicity);


        //$msgArray = array_merge($this->_config['PassengerMessages']['config']['ship_search'], $this->_config['PassengerMessages']['config']['common']);

        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'passengerId' => $params['id'],
            'ship_name' => '',
            'date_arrive' => '',
            'searchResult' => $searchResult[0],
            'jsLangTranslate' => $msgArray,
            'shipPath' => $shipPath,
			'shipFilePath' => $shipfilePath,
            'user_id' => isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '',
            'mode' => $this->decrypt($params['mode']),
            'passengerManifestResult' => $passengerManifestResult,
            'manifestFile' => $manifestFileEnc,
            'annotationCorrectionId' => $annotationCorrectionId,
            'assets_url' => $this->_config['file_upload_path']['assets_url'],
            'form' => $form,
            'gender' => $gender,
            'maritalstatus' => $maritalstatus,
            'searchTypeArray' => $searchTypeArray,
            'searchEthnicity' => $ethnicity,
            'translator' => $msgArray
                //'annotationResult' => $annotationResult,
        ));
        return $viewModel;
    }

    function find_index_position($array, $value) {
        foreach ($array as $key => $val) {
            if ($val['annotation_correction_id'] == $value)
                return $key;
        }
        return FALSE;
    }

    /**
     * This Action is used for passenger annotation display
     * @param passenger id.
     * @return annotation values for passenger
     * @author Icreon Tech -SR
     */
    public function passengerAnnotationAction() {
        $this->checkFrontUserAuthenticationAjx();
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        $this->getPassengerTable();
        $searchResult = array();
        $msgArray = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['passenger_details']);
        $params = $this->params()->fromRoute();
        $passengerId = $this->decrypt($params['id']);
        $searchParam['passenger_id'] = $passengerId;
        $passengerResult = $this->getPassengerTable()->getPassengerRecord($searchParam);
        $msgArray = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['passenger_details']);
        $page = 1;
        $limit = '';
        $offset = '';
        $userDetail = array();
        $passParam = array();

        $passParam['passengerId'] = $this->decrypt($params['id']);
        $passengerEthnicity = $this->getPassengerTable()->getPassengerEthnicity($passParam);
        $memberShipConfig = array();
        if (!empty($this->auth->getIdentity()->user_id)) {
            $memberShipConfig = $this->getMembershipId($this->auth->getIdentity()->user_id, 'annotation');
            $memberShipConfigCorrection = $this->getMembershipId($this->auth->getIdentity()->user_id, 'correction');
        }

        $searchResult = array();
        $annotationCount = '';
        $annotationResult = array();
        $searchParam['passenger_id'] = $passengerId;
        $searchResult = $this->getPassengerTable()->getPassengerRecord($searchParam);
        $searchParam['status'] = '1'; // completed
        $searchResult['activity_type'] = 1; // added on 21 nov 2014
        $allAnnotationResult = $this->getPassengerTable()->getPassengeAnnotationCorrection($searchParam);

        $userAnnotationId = '';
        if (!empty($params['annotation_id']))
            $userAnnotationId = $this->decrypt($params['annotation_id']);


        if ($request->getPost('page') != '') {
            $page = $request->getPost('page');
        } else {
            $page = $this->find_index_position($allAnnotationResult, $userAnnotationId) + 1;
        }

        if ($page != '') {
            $limit = 1;
            $offset = ($page - 1) * $limit;
        }
        $searchResult['passenger_id'] = $this->decrypt($params['id']);
        $searchResult['recOffset'] = $offset;
        $searchResult['recLimit'] = 1;
        $searchResult['status'] = '1'; // completed
        $searchResult['activity_type'] = 1; // added on 21 nov 2014
        $annotationResult = $this->getPassengerTable()->getPassengeAnnotationCorrection($searchResult);
        // To get the total annotation count. 
        $annotationCountResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $annotationCount = 0;
        $annotationCount = $annotationCountResult[0]->RecordCount;

        if (count($annotationResult) > 0)
            $annotationResult = $annotationResult[0];

        if ($page != '') {
            if (count($annotationResult) > 0) {
                $addedBy = $annotationResult['added_by'];
                $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $addedBy));
            }
        }


        $userAnnotationSearchParam['user_id'] = $this->auth->getIdentity()->user_id;
        $userAnnotationSearchParam['passenger_id'] = $passengerId;
        $userAnnotationSearchParam['activity_type'] = 1; // annotation
        //$searchParam['status'] = 0; // Pending status
        $userAnnotationResult = array();
        $userAnnotationResult = $this->_passengerTable->getPassengerCorrectionDetails($userAnnotationSearchParam);

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'passengerResult' => $passengerResult[0],
            'jsLangTranslate' => $msgArray,
            'passengerId' => $params['id'],
            'passengerEthnicity' => isset($passengerEthnicity[0]) ? $passengerEthnicity[0] : '',
            'annotationResult' => $annotationResult,
            'memberShipConfig' => $memberShipConfig,
            'memberShipConfigCorrection' => $memberShipConfigCorrection,
            'page' => $page,
            'annotationCount' => $annotationCount,
            'userDetail' => $userDetail,
            'user_name' => $this->auth->getIdentity()->first_name,
            'user_email' => $this->auth->getIdentity()->email_id,
            'userId' => $this->encrypt($this->auth->getIdentity()->user_id),
            'userAnnotationResult' => $userAnnotationResult
        ));
        return $viewModel;
    }

    /**
     * This Action is used for passenger record
     * @param passenger id.
     * @return annotation value for passenger
     * @author Icreon Tech -SR
     */
    public function passengerRecordAction() {
        $this->getPassengerTable();
        $msgArray = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['passenger_details']);
        $params = $this->params()->fromRoute();
        $passengerId = $this->decrypt($params['id']);
        $searchParam['passenger_id'] = $passengerId;
        $searchResult = array();
        // asd($searchParam);
       
        $searchResult = $this->getPassengerTable()->getPassengerRecord($searchParam);
        // asd($searchResult);
        // die;


        $searchParam['status'] = '1'; // completed
        $searchParam['activity_type'] = 1; // added on 21 nov 2014
		$annotationResult = $this->getPassengerTable()->getPassengeAnnotationCorrection($searchParam);
        $productParam = array();
        $productParam['productType'] = '5';/** product */
        $productParam['isOptions'] = 'yes';/** product */
        $productResult = array();
        $productResult = $this->getPassengerTable()->getProductAttributeDetails($productParam);

        // membership discount on product
        $discount = 0;
        /*
       */
       
        if (defined('MEMBERSHIP_DISCOUNT') && $productResult[0]['is_membership_discount'] == 1) {
		$discount = MEMBERSHIP_DISCOUNT;
		}

        $memberShipConfig = array();
        $memberShipConfigAll = array();
        $memberShipConfigCorrection = array();
        if (!empty($this->auth->getIdentity()->user_id)) {
            $memberShipConfig = $this->getMembershipId($this->auth->getIdentity()->user_id, 'annotation');
            $memberShipConfigCorrection = $this->getMembershipId($this->auth->getIdentity()->user_id, 'correction');
            $memberShipConfigAll = $this->getMembershipId($this->auth->getIdentity()->user_id, 'Passenger Record');
        }
        // asd($memberShipConfig);
        $savedPassengerCount = 0;
        $passengerSavedSearch = $this->getPassengerTable()->getUserPassengerSearch(array('user_id' => (isset($this->auth->getIdentity()->user_id) and trim($this->auth->getIdentity()->user_id) != "") ? $this->auth->getIdentity()->user_id : ""));
        $savedPassengerCount = count($passengerSavedSearch);
        $passParam = array();
        $passParam['passengerId'] = $this->decrypt($params['id']);
        $passengerEthnicity = $this->getPassengerTable()->getPassengerEthnicity($passParam);

        $userAnnotationSearchParam['user_id'] = (isset($this->auth->getIdentity()->user_id) and trim($this->auth->getIdentity()->user_id) != "") ? $this->auth->getIdentity()->user_id : "";
        $userAnnotationSearchParam['passenger_id'] = $passengerId;
        $userAnnotationSearchParam['activity_type'] = 1; // annotation
        //$searchParam['status'] = 0; // Pending status
        $userAnnotationResult = array();
        $userAnnotationResult = $this->_passengerTable->getPassengerCorrectionDetails($userAnnotationSearchParam);

        /* Ads section starts */

        $url = explode('/', $_SERVER['REQUEST_URI']);
        $adParam['page_url'] = $url[1];
        $adParam['ad_zone_id'] = '3';   // 3 for right section
        $adParam['curr_date'] = DATE_TIME_FORMAT;
        $adParam['start_Index'] = '';
        $adParam['record_limit'] = '';
        $adParam['rand_flag'] = '1';
        $adsContent = $this->getServiceLocator()->get('Cms\Model\AdTable')->getCrmFrontAd($adParam);

        /* ads section ends */
        if (count($userAnnotationResult) > 0)
            $userAnnotationResult = $userAnnotationResult[0];

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'searchResult' => $searchResult[0],
            'jsLangTranslate' => $msgArray,
            'annotationResult' => $annotationResult,
            'productResult' => $productResult,
            'passengerEthnicity' => isset($passengerEthnicity[0]) ? $passengerEthnicity[0] : '',
            'maritalStatusArray' => $this->maritalStatusArray(),
            'genderArray' => $this->genderArray(),
            'passengerId' => $params['id'],
            'passenger_id' => $passengerId,
            'memberShipConfig' => $memberShipConfig,
            'memberShipConfigCorrection' => $memberShipConfigCorrection,
            'memberShipConfigAll' => $memberShipConfigAll,
            'savedPassengerCount' => $savedPassengerCount,
            'userAnnotationResult' => $userAnnotationResult,
            'discount' => $discount,
            'adsContent' => $adsContent,
            'facebookAppId' => $this->_config['facebook_app']['app_id'],
            'allowed_IP' => $this->_config['allowed_ip']['ip'],
            'request_print_certificate_email_id'=>$this->_config['request_print_certificate_email_id']
        ));
        return $viewModel;
    }

    /**
     * This Action is used for passenger ship information
     * @param passenger id.
     * @return annotation values for passenger ship 
     * @author Icreon Tech -SR
     */
    public function passengerShipInformationAction() {
        $this->checkFrontUserAuthenticationAjx();
        $params = $this->params()->fromRoute();
        $this->getPassengerTable();
        $msgArray = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['passenger_details'], $this->_config['PassengerMessages']['config']['ship_search']);
        $params = $this->params()->fromRoute();
        $passengerId = '';
        $passengerDetailsPage = '';
        $shipNameShipInfo = "";
        $shipDateArrivalShipInfo = "";
        $shipLineId = "";
        if (!empty($params['id'])) {
            $passengerId = $this->decrypt($params['id']);
            $searchParam['passenger_id'] = $passengerId;
            $passengerDetailsPage = 'yes';
        }

        if (!empty($params['ship_name'])) {
            $shipNameShipInfo = $params['ship_name'];
            $searchParam['ship_name'] = $this->decrypt($params['ship_name']);
        }
        if (!empty($params['line_id'])) {
            $shipLineId = $params['line_id'];
           // $searchParam['line_id'] = $this->decrypt($params['line_id']);
        }
        
        $dateArrival = '';
        if (!empty($params['date_arrive'])) {
            $shipDateArrivalShipInfo = $params['date_arrive'];
            $searchParam['date_arrive'] = $this->decrypt($params['date_arrive']);
        }
        $passengerResult = $this->getPassengerTable()->getPassengerRecord($searchParam);

        if (!empty($params['date_arrive'])) {
            $dateArrival = $params['date_arrive'];
        } else {
            $dateArrival = $this->encrypt(substr($passengerResult[0]['DATE_ARRIVE'], 0, 10));
        }

        $shipResult = array();
        $searchShipLineParam = array();
        $shipID = '';
        if (!empty($passengerResult[0]['SHIP_NAME'])) {
            $searchShipParam['ship_name'] = $passengerResult[0]['SHIP_NAME'];
            $searchShipParam['asset_for'] = '1';
            $searchShipParam['ship_line_id'] = $this->decrypt($shipLineId);
            $shipResult = $this->getPassengerTable()->getShipDetails($searchShipParam);
            if (count($shipResult) > 0) {
                $shipResult = $shipResult[0];
                $shipID = $shipResult['SHIPID'];
                $searchShipLineParam['ship_id'] = $shipID;
                $shipLineResult = $this->getPassengerTable()->getShipLineDetails($searchShipLineParam);
            }
        }


        //$shipPath = $this->_config['Image_path']['shipPath'];
		$shipPath =  $this->_config['file_upload_path']['assets_url'].'ship_image/';
		$shipfilePath =  $this->_config['file_upload_path']['assets_upload_dir'].'ship_image/';
        $productParam['productType'] = 8;/** Ship Image */
        $productParam['isOptions'] = 'yes';/** product */
        $productOptionResult = array();
        $productOptionResult = $this->getPassengerTable()->getProductAttributeDetails($productParam);
        // membership discount on product
        $discount = 0;
        /*if ($productOptionResult[0]['is_membership_discount'] == 1) {
            if (isset($this->auth->getIdentity()->user_id)) {
                $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserMemberShipDetail(array('user_id' => $this->auth->getIdentity()->user_id));
                if (count($userDetail) > 0) {
                    if ($userDetail['membership_id'] != '1') {
                        $discount = $userDetail['membership_discount'];
                    }
                }
            }
        }*/

        if (isset($this->auth->getIdentity()->user_id)) {
            if (defined('MEMBERSHIP_DISCOUNT') && $productOptionResult[0]['is_membership_discount'] == 1) {
                        $discount = MEMBERSHIP_DISCOUNT;
            }
        }

				
        $searchShipDetailParam = array();
        $searchShipDetailParam['ship_name'] = $passengerResult[0]['SHIP_NAME'];
        $searchShipDetailParam['DATE_ARRIVE'] = substr($passengerResult[0]['DATE_ARRIVE'], 0, 10);
        $passCountResult = $this->getServiceLocator()->get('Passenger\Model\DocumentTable')->getShipPassengerListCount($searchShipDetailParam);
        $passTotalCount = 0;
        
        /* Ads section starts */

        $url = explode('/', $_SERVER['REQUEST_URI']);
        $adParam['page_url'] = $url[1];
        $adParam['ad_zone_id'] = '3';   // 3 for right section
        $adParam['curr_date'] = DATE_TIME_FORMAT;
        $adParam['start_Index'] = '0';
        $adParam['record_limit'] = '2';
        $adParam['rand_flag'] = '1';
        $adsContent = $this->getServiceLocator()->get('Cms\Model\AdTable')->getCrmFrontAd($adParam);

        /* ads section ends */
        
        if (count($passCountResult) > 0)
            $passTotalCount = $passCountResult[0]['cnt'];

        $viewModel = new ViewModel();
        if ($this->getRequest()->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }

		$arrival_year = date('Y',strtotime($passengerResult[0]['DATE_ARRIVE']));
        
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $msgArray,
            'shipResult' => $shipResult,
            'shipPath' => $shipPath,
            'productOptionResult' => $productOptionResult,
            'passengerId' => $params['id'],
            'passenger_id' => $passengerId,
            'shipId' => $this->encrypt($shipID),
            'shipidPlain' => $shipID,
            'passTotalCount' => $passTotalCount,
            #'memberShipConfigAll' => $memberShipConfigAll,
            #'savedShipCount' => $savedShipCount,
            'discount' => $discount,
            'facebookAppId' => $this->_config['facebook_app']['app_id'],
            'dateArrival' => $dateArrival,
            'dateArrivalDateFormat' => $this->decrypt($dateArrival),
            'shipLineResult' => $shipLineResult,
            'transTypeArray' => $this->transTypeArray(),
            'howNameChangeArray' => $this->howNameChangeArray(),
            'lineLessTransTypeArray' => $this->lineLessTransTypeArray(),
            'screwNameArray' => $this->screwNameArray(),
            'engineTypeArray' => $this->engineTypeArray(),
            'passengerDetailsPage' => $passengerDetailsPage,
            'shipNameShipInfo' => $shipNameShipInfo,
            'adsContent' => $adsContent,
            'shipDateArrivalShipInfo' => $shipDateArrivalShipInfo,
            'allowed_IP' => $this->_config['allowed_ip']['ip'],
			'shipfilePath'=> $shipfilePath,
			'arrival_year' => $arrival_year
        ));
        return $viewModel;
    }

    /**
     * This Action is used for passenger listing
     * @param passenger id.
     * @return annotation values for passenger listing
     * @author Icreon Tech -SR
     */
    public function textPassengerListAction() { 
        $this->checkFrontUserAuthenticationAjx();
        $params = $this->params()->fromRoute();

        $passengerDetailsPage = '';
        $passengerId = '';
        $manifestFile = '';
        $shipManifestArray = array();


        $shipManifestFilenameArray = array();
        if (!empty($params['id'])) {
            $passengerId = $this->decrypt($params['id']);
            $searchParam['passenger_id'] = $passengerId;
            $passengerDetailsPage = 'yes';
        }
        if (!empty($params['ship_name'])) {
            $searchParam['ship_name'] = $this->decrypt($params['ship_name']);
        }
        if (!empty($params['date_arrive'])) {
            $searchParam['date_arrive'] = $this->decrypt($params['date_arrive']);
        }
        if (!empty($params['manifest_file'])) {
            $manifestFile = $this->decrypt($params['manifest_file']);
            //$params['id'] = $this->decrypt($params['id']);
            //$searchResult = $this->getTransactionTable()->getManifestImage($params);            
        }
        //$searchParam['date_arrive'] = '1926-01-01';

        $passengerResult = array();
        $passengerResult = $this->getPassengerTable()->getPassengerRecord($searchParam);
        $manifestPatternFileName = '';
        foreach($passengerResult as $passKey=>$passVal)
        {
            if($passVal['FILENAME']!='' && $manifestPatternFileName == '') {
                    $manifestPatternFileName = $passVal['FILENAME'];
            }
        }
        $fileArray = explode('.', $manifestPatternFileName);
		$fileExt = $fileArray[1];
		if (strtolower($fileExt) == 'tif') {
			$manifestPatternFileName = substr($manifestPatternFileName,0,-4);
			$manifestPatternFileName = strtolower($manifestPatternFileName.'.jpg');    
		}
		else {
			$manifestPatternFileName = strtolower($manifestPatternFileName);
		}
        $assets_url = $this->_config['file_upload_path']['assets_url'];
        $shipID = '';
        if (!empty($passengerResult[0]['SHIP_NAME'])) {
            $searchShipParam['ship_name'] = $passengerResult[0]['SHIP_NAME'];
            $searchShipParam['asset_for'] = '1';
            $shipResult = $this->getPassengerTable()->getShipDetails($searchShipParam);
            if (count($shipResult) > 0) {
                $shipResult = $shipResult[0];
                $shipID = $shipResult['SHIPID'];
            }
        }
        $manifestPath = $config['file_upload_path']['assets_url'].'manifest-low/';
        $manifestParam['shipName'] = $passengerResult[0]['SHIP_NAME'];
        $manifestParam['dateArrive'] = substr($passengerResult[0]['DATE_ARRIVE'], 0, 10);

        if (!empty($manifestParam['shipName'])) {
            $searchParam['ship_name'] = $manifestParam['shipName'];
            // for ship name having (1980) in name
            $shipNameString = explode('(', substr($searchParam['ship_name'], -6));
            $shipNameString = substr(@$shipNameString[1], 0, 4);
            if (is_numeric($shipNameString) == true)
                $manifestParam['shipName'] = trim(substr($searchParam['ship_name'], 0, -6));
            // end // for ship name having (1980) in name
        }
        if (!empty($params['ship_name'])) {
            $searchParam['ship_name'] = $this->decrypt($params['ship_name']);
            // for ship name having (1980) in name
            $shipNameString = explode('(', substr($searchParam['ship_name'], -6));
            $shipNameString = substr(@$shipNameString[1], 0, 4);
            if (is_numeric($shipNameString) == true)
                $manifestParam['shipName'] = trim(substr($searchParam['ship_name'], 0, -6));
            // end // for ship name having (1980) in name
        }        

         if ($passengerResult[0]['data_source'] == 1) {
            $shipManifestFrameResult = $this->getPassengerTable()->getShipManifestFrame($manifestParam);
            $shipManifestArray = array();
            foreach($shipManifestFrameResult as $mKey=>$mVal) {
                $manifestParam['frame'] = $mVal['ROLL_NUMBER'];
                $shipManifestResult = $this->getPassengerTable()->getShipManifestRecords($manifestParam);
                    if (count($shipManifestResult) > 0 && !isset($shipManifestResult[0]['Message'])) {
                        if ($passengerResult[0]['data_source'] == 1) {

                            $startFrame = $shipManifestResult[0]['START_RANGE'];
                            $endFrame = $shipManifestResult[0]['END_RANGE'];
                            
                            $rollNbr = $shipManifestResult[0]['ROLL_NBR'];
                            for ($k = $startFrame; $k <= $endFrame; $k++) {

                                if ($shipManifestResult[0]['data_source'] == 2) {
                                    $shipManifestFilenameArray[] = $shipManifestResult[0]['FILENAME'];
                                    $fileName = $shipManifestResult[0]['FILENAME'];
                                    $shipManifestArray[] = $shipManifestResult[0]['FILENAME'];
                                } else {
                                    $fileName = $rollNbr . sprintf('%04d', $k) . '.jpg';
                                    $folderName = substr($fileName, 0, 9);
                                    $shipManifestArray[] = $fileName;
                                    $shipManifestFilenameArray[] = strtolower($fileName);
                                }
                            }
                        } 
                    }            
            }      
        } else if ($passengerResult[0]['data_source'] == 2) {
            $manifestParam['dataSource'] = 2;
            $manifestFileNameArray = explode('_', $manifestPatternFileName);
            $manifestParam['fileNamePattern'] = $manifestFileNameArray[0];
            
            $dateArray = date_parse_from_format("Y-m-d", $passengerResult[0]['DATE_ARRIVE']);
            $manifestParam['dateArriveFrom'] = $dateArray['year'].'-01-01';
            $manifestParam['dateArriveTo'] = $dateArray['year'].'-12-31';
            
            if($passengerDetailsPage == 'yes') {
                    $shipManifestResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getFamilyShipManifest($manifestParam);
            }
            else {
                    $manifestResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getFamilyShipManifest($manifestParam);
                    $manifestSearchParam['start_range'] = $manifestResult[0]['START_RANGE'];
                    $manifestSearchParam['end_range'] = $manifestResult[0]['END_RANGE'];
                    $manifestSearchParam['data_source'] = 2;
                    $manifestSearchParam['fileNamePattern'] = $manifestFileNameArray[0];
                    $shipManifestResult = $this->getPassengerTable()->getFamilyShipManifestListing($manifestSearchParam);
                    foreach($shipManifestResult as $mKey=>$mVal)
                    {
                        $shipManifestResult[$mKey]['START_RANGE'] = $manifestSearchParam['start_range'];
                        $shipManifestResult[$mKey]['END_RANGE'] = $manifestSearchParam['end_range'];
                    }
            }
            
                    if (count($shipManifestResult) > 0 && !isset($shipManifestResult[0]['Message'])) {
                               if ($passengerResult[0]['data_source'] == 2) {
                                    if (count($shipManifestResult) > 0 && !isset($shipManifestResult[0]['Message'])) {

                                            $startFrame = $shipManifestResult[0]['START_RANGE'];
                                            $endFrame = $shipManifestResult[0]['END_RANGE'];
                                            $shipManifestArray = array();
                                            $rollNbr = $shipManifestResult[0]['ROLL_NBR'];
                                            for ($k = $startFrame; $k <= $endFrame; $k++) {
                                                    $fileName = substr($rollNbr,5) . '_'.sprintf('%05d', $k) . '.jpg';
                                                    $folderName = substr($fileName, 0, 9);
                                                    $shipManifestArray[] = $fileName;
                                                    $shipManifestFilenameArray[] = $fileName;                            
                                            }                    
                                    }
                                }
                            }            
        }

        
        $defaultImageIndex = 0;
        $manifestResult = array();
        if ($passengerDetailsPage == 'yes') {    // passenger details condition
            $manifestResult = $this->getPassengerTable()->getPassengerManifest($searchParam);

            if (count($manifestResult) > 0) {

                if ($passengerResult[0]['data_source'] == 2) {
                    $manifest_file_name = $manifestResult[0]['FILENAME'];
                    $fileName = $manifest_file_name;
                    $shipManifestArray[] = $fileName;
                    $shipManifestFilenameArray[] = $fileName;
                } else {
                    $manifest_file_name = strtolower($manifestResult[0]['FILENAME']);
                    $manifest_file_name = $this->convertTifToJpg($manifest_file_name);
                    $fileName = $manifest_file_name;
                    $folderName = substr($fileName, 0, 9);
                    $shipManifestArray[] = $fileName;
                    $shipManifestFilenameArray[] = $fileName;
                }
                $shipManifestArray = array_unique($shipManifestArray);
                $shipManifestFilenameArray = array_unique($shipManifestFilenameArray);
            }
            $defaultImageIndex = array_search($manifest_file_name, $shipManifestFilenameArray);
        } else {
            if (!empty($manifestFile)) {

                if ($passengerResult[0]['data_source'] == 2) {
                    $manifestFile = $manifestResult[0]['FILENAME'];
                    $shipManifestResult[0]['FILENAME'] = $manifestFile;
                    $shipManifestResult[0]['FRAME'] = $manifestFile[0]['FRAME'];
                    $shipManifestResult[0]['ROLL_NBR'] = $manifestFile[0]['ROLL_NBR'];
                    $shipManifestResult[0]['data_source'] = 2;
                    $shipManifestFilenameArray[] = $manifestFile;
                    $shipManifestArray[] = $assets_url . 'manifest' . $manifestFile;
                    $shipManifestArray = array_unique($shipManifestArray);
                    $shipManifestFilenameArray = array_unique($shipManifestFilenameArray);
                    // $shipManifestFilenameArray[] = $manifestResult[0]['FILENAME'];
                    // $shipManifestFilenameArray[] = $fileName;
                }
            } else {
				$shipManifestResult[0]['FILENAME'] = $this->convertTifToJpg($shipManifestResult[0]['FILENAME']);
                $defaultImageIndex = array_search($shipManifestResult[0]['FILENAME'], $shipManifestFilenameArray);
            }
            $manifestResult = $shipManifestResult;
        }

        $productParam = array();
        $productParam['productType'] = '7';/** manifest */
        $productParam['isOptions'] = 'yes';
        $productResult = array();
        $productResult = $this->getPassengerTable()->getProductAttributeDetails($productParam);
        // membership discount on product
        $discount = 0;
        /*if ($productResult[0]['is_membership_discount'] == 1) {
            if (isset($this->auth->getIdentity()->user_id)) {
                $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserMemberShipDetail(array('user_id' => $this->auth->getIdentity()->user_id));
                if (count($userDetail) > 0) {
                    if ($userDetail['membership_id'] != '1') {
                        $discount = $userDetail['membership_discount'];
                    }
                }
            }
        }*/
        if (isset($this->auth->getIdentity()->user_id)) {
            if (defined('MEMBERSHIP_DISCOUNT') && $productResult[0]['is_membership_discount'] == 1) {
                            $discount = MEMBERSHIP_DISCOUNT;
            }
        }
        $this->getPassengerTable();
        $msgArray = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['passenger_details']);
        $viewModel = new ViewModel();
        if ($this->getRequest()->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        
        $yearArrive = substr($passengerResult[0]['DATE_ARRIVE'], 0, 4);
        
        //if (count($shipManifestArray) == 1) {
            if ($yearArrive > 1907 && empty($passengerResult[0]['PRIN_CITIZEN_US_IND']) && empty($passengerResult[0]['PRIN_CREW_SHIP_IND']) && $passengerDetailsPage == 'yes') {
                if ($manifestResult[0]['data_source'] == 2) {

                    $sFileName = substr($manifestResult[0]['ROLL_NBR'], 5) . '_' . sprintf('%05d', $manifestResult[0]['FRAME'] + 1) . '.jpg';
                    if(!in_array($sFileName, $shipManifestArray)) {
                        $shipManifestFilenameArray[] = $sFileName;
                        $shipManifestArray[] = $sFileName;
                    }
                } else {
                    $sFileName = $manifestResult[0]['ROLL_NBR'] . sprintf('%04d', $manifestResult[0]['FRAME'] + 1). '.TIF';;
                    if(!in_array($sFileName, $shipManifestArray)) {
                        $shipManifestArray[] = $manifestResult[0]['ROLL_NBR'] . sprintf('%04d', $manifestResult[0]['FRAME'] + 1) . ".jpg";
                        $shipManifestFilenameArray[] = $manifestResult[0]['ROLL_NBR'] . sprintf('%04d', $manifestResult[0]['FRAME'] + 1) . '.jpg';
                    }
                }
            }
        //}
        if (!empty($manifestFile)) {
            $manifest_file_name = $manifestFile;
            $defaultImageIndex = array_search($manifest_file_name, $shipManifestFilenameArray);
            if ($passengerDetailsPage == 'yes') {
                $manifestFile = '';
            }
        }

        $typeShipText = $this->getRequest()->getPost('typeShipText');
        $typeShipText = (isset($typeShipText) and trim($typeShipText) != "") ? $typeShipText : "";
       

        /* Ads section starts */

        $url = explode('/', $_SERVER['REQUEST_URI']);
        $adParam['page_url'] = $url[1];
        $adParam['ad_zone_id'] = '3';   // 3 for right section
        $adParam['curr_date'] = DATE_TIME_FORMAT;
        $adParam['start_Index'] = '0';
        $adParam['record_limit'] = '2';
        $adParam['rand_flag'] = '1';
        $adsContent = $this->getServiceLocator()->get('Cms\Model\AdTable')->getCrmFrontAd($adParam);

        /* ads section ends */
//asd($manifestResult);
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $msgArray,
            'manifestResult' => $manifestResult,
            'passengerResult' => $passengerResult[0],
            'passenger_id' => $passengerId,
            'passengerId' => $params['id'],
            'manifestPath' => $manifestPath,
            'productResult' => $productResult,
            'shipId' => $this->encrypt($shipID),
            'shipidPlain' => $shipID,
            'shipManifestArray' => $shipManifestArray,
            'shipManifestFilenameArray' => $shipManifestFilenameArray,
            'defaultImageIndex' => $defaultImageIndex,
            'passengerDetailsPage' => $passengerDetailsPage,
            'discount' => $discount,
            'facebookAppId' => $this->_config['facebook_app']['app_id'],
            'manifestFile' => $manifestFile,
            's_ship_name' => $this->decrypt($params['ship_name']),
            'assets_url' => $this->_config['file_upload_path']['assets_url'],
            'typeShipText' => $typeShipText,
            'adsContent' => $adsContent,
            'uploadFilePath' => $this->_config['file_upload_path']['assets_upload_dir'],
            'allowed_IP' => $this->_config['allowed_ip']['ip']
        ));
        return $viewModel;
    }

    
   /* function array_iunique($arr)  
{ 
    foreach ($arr as $key => $value) { 
        $arr[$key] = strtolower($value); // Or uc_first(), etc. 
    } 
    return array_unique($arr); 
}*/
    /**
     * This Action is used for passenger to add annotation
     * @param passenger id.
     * @return confirmation
     * @author Icreon Tech -SR
     */
    public function passengerAddAnnotationAction() {
        $this->getPassengerTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $passenger = new Passenger($this->_adapter);
        $passengerId = $this->decrypt($params['id']);
        $searchParam['passenger_id'] = $passengerId;

        $passengerResult = $this->_passengerTable->getPassengerRecord($searchParam);
        $passengerForm = new PassengerAnnotationForm();

        $msgArray = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['passenger_details']);
        $viewModel = new ViewModel();
        if ($this->getRequest()->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $passParam = array();
        $passParam['passengerId'] = $this->decrypt($params['id']);
        $passengerEthnicity = $this->getPassengerTable()->getPassengerEthnicity($passParam);

        $maritalStatusArray = $this->maritalStatusArray();
        unset($maritalStatusArray['U']);
        $genderArray = $this->genderArray();
        unset($genderArray['U']);
        $passengerForm->get('marital_status')->setAttribute('options', $maritalStatusArray);
        $passengerForm->get('gender')->setAttribute('options', $genderArray);
        $firstTimeInUsArray = array('1' => 'Yes', '0' => 'No');
        $passengerForm->get('first_time_in_us')->setAttribute('options', $firstTimeInUsArray);

        $searchParam = array();
        $recordArray = array();
        $searchParam['user_id'] = $this->auth->getIdentity()->user_id;
        $searchParam['passenger_id'] = $passengerId;
        $searchParam['activity_type'] = 1; // annotation
        //$searchParam['status'] = 0; // Shown status
        $recordArray = $this->_passengerTable->getPassengerCorrectionDetails($searchParam);

        $email_send = 1;
        if (count($recordArray)) {

            $passengerForm->get('annotation_correction_id')->setAttribute('value', $recordArray[0]['annotation_correction_id']);
            $passengerForm->get('first_name')->setAttribute('value', $recordArray[0]['first_name']);
            $passengerForm->get('last_name')->setAttribute('value', $recordArray[0]['last_name']);
            $passengerForm->get('nationality')->setAttribute('value', $recordArray[0]['nationality']);
            $passengerForm->get('last_residence')->setAttribute('value', $recordArray[0]['last_residence']);
            $passengerForm->get('arrival_age')->setAttribute('value', $recordArray[0]['age_at_arrival']);
            $passengerForm->get('gender')->setAttribute('value', $recordArray[0]['gender']);
            $passengerForm->get('marital_status')->setAttribute('value', $recordArray[0]['maritial_status']);
            $passengerForm->get('arrival_date')->setAttribute('value', $this->DateFormat($recordArray[0]['arrival_date'], 'calender'));
            $passengerForm->get('ship')->setAttribute('value', $recordArray[0]['ship_name']);
            $passengerForm->get('port_departure')->setAttribute('value', $recordArray[0]['departure_port_name']);
            $passengerForm->get('line_number')->setAttribute('value', $recordArray[0]['line_no']);
            $passengerForm->get('birth_date')->setAttribute('value', $this->DateFormat($recordArray[0]['date_of_birth'], 'calender'));
            $passengerForm->get('death_date')->setAttribute('value', $this->DateFormat($recordArray[0]['date_of_death'], 'calender'));
            $passengerForm->get('occupation')->setAttribute('value', $recordArray[0]['occupation']);
            $passengerForm->get('spouse')->setAttribute('value', $recordArray[0]['spouse']);
            $passengerForm->get('children')->setAttribute('value', $recordArray[0]['children']);
            $passengerForm->get('us_residence')->setAttribute('value', $recordArray[0]['setteled_in_us']);
            $passengerForm->get('us_relatives')->setAttribute('value', $recordArray[0]['relatives_in_us']);
            $passengerForm->get('military_experience')->setAttribute('value', $recordArray[0]['military_experience']);
            $passengerForm->get('religious_community')->setAttribute('value', $recordArray[0]['religious_community']);
            $passengerForm->get('birth_place')->setAttribute('value', $recordArray[0]['birth_place']);
            if (isset($recordArray[0]['email_send']) && $recordArray[0]['email_send'] != '')
                $email_send = '1';
            else
                $email_send = '0';
            $passengerForm->get('annotation_email')->setAttribute('value', $recordArray[0]['email_send']);
            if (!empty($recordArray[0]['date_of_marriage']) && $recordArray[0]['date_of_marriage'] != '0000-00-00')
                $passengerForm->get('date_of_marriage')->setAttribute('value', $this->DateFormat($recordArray[0]['date_of_marriage'], 'calender'));
            $passengerForm->get('parent_names')->setAttribute('value', $recordArray[0]['parent_names']);
            $passengerForm->get('first_time_in_us')->setAttribute('value', $recordArray[0]['first_time_in_us']);
        }
        if ($request->isPost()) {
            $passengerForm->setData($request->getPost());
            $passengerForm->setInputFilter($passenger->getInputFilterAnnotation());

            if (!$passengerForm->isValid()) {
                $msg = array();
                $errors = $passengerForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['PassengerMessages']['config']['passenger_details'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $dataArray = $passengerForm->getData();

                $validityFlag = 1;
                foreach ($dataArray as $key => $val) {
                    if ($key != 'save' && $key != 'cancel' && $key != 'agreement' && $key != 'terms' && $key != 'annotation_correction_id' && $key != 'annotation_email') {
                        if ($val != '')
                            $validityFlag = 0;
                    }
                }
                if ($validityFlag == 0) {
                    $passenger->exchangeAnnotationArray($passengerForm->getData());
                    $postData = array();
                    $postData['passenger_id'] = $passengerId;
                    $postData['first_name'] = $passenger->first_name;
                    $postData['last_name'] = $passenger->last_name;
                    $postData['nationality'] = $passenger->nationality;
                    $postData['last_residence'] = $passenger->last_residence;
                    $postData['arrival_age'] = $passenger->arrival_age;
                    $postData['gender'] = $passenger->gender;
                    $postData['marital_status'] = $passenger->marital_status;
                    $postData['arrival_date'] = $this->DateFormat($passenger->arrival_date, 'db_date_format');
                    $postData['ship'] = $passenger->ship;
                    $postData['port_departure'] = $passenger->port_departure;
                    $postData['line_number'] = $passenger->line_number;
                    $postData['user_id'] = $this->auth->getIdentity()->user_id;
                    $postData['added_by'] = $this->auth->getIdentity()->user_id;
                    $postData['added_on'] = DATE_TIME_FORMAT;
                    $postData['modified_by'] = $this->auth->getIdentity()->user_id;
                    $postData['modified_on'] = DATE_TIME_FORMAT;
                    $postData['activity_type'] = 1; // for annotation
                    $postData['birth_date'] = $this->DateFormat($passenger->birth_date, 'db_date_format');
                    $postData['death_date'] = $this->DateFormat($passenger->death_date, 'db_date_format');
                    $postData['occupation'] = $passenger->occupation;
                    $postData['spouse'] = $passenger->spouse;
                    $postData['children'] = $passenger->children;
                    $postData['us_residence'] = $passenger->us_residence;
                    $postData['us_relatives'] = $passenger->us_relatives;
                    $postData['military_experience'] = $passenger->military_experience;
                    $postData['religious_community'] = $passenger->religious_community;
                    $postData['annotation_email'] = $passenger->annotation_email;
                    $postData['date_of_marriage'] = $this->DateFormat($passenger->date_of_marriage, 'db_date_format');
                    $postData['parent_names'] = $passenger->parent_names;
                    $postData['birth_place'] = $passenger->birth_place;
                    if (empty($passenger->first_time_in_us))
                        $postData['first_time_in_us'] = '';
                    else
                        $postData['first_time_in_us'] = $passenger->first_time_in_us;

                    if ($request->getPost('annotation_correction_id') == '') { /** insert annotation */
                        $postData['status'] = 1;
                        $flag = $this->_passengerTable->insertPassengerAnnotation($postData);
                        if ($flag == 1) {
                            $messages = array('status' => "success");
                            $response->setContent(\Zend\Json\Json::encode($messages));
                            return $response;
                        }
                    } else /** update annotation */ {
                        // 
                        if ($recordArray[0]['status'] == 0) {
                            $postData['status'] = 0;
                            $postData['is_reauthorization'] = 1; // pending status
                        }
                        $postData['annotation_correction_id'] = $request->getPost('annotation_correction_id');

                        //asd($postData);
                        $flag = $this->_passengerTable->updatePassengerAnnotation($postData);
                        if ($flag == 1) {

                            $messages = array('status' => "success");
                            $response->setContent(\Zend\Json\Json::encode($messages));
                            return $response;
                        }
                    }
                } else {
                    $msg['parent_names'] = $msgArray['ATLEAST_ONE_ABOVE_VALUE_ERROR'];
                    $messages = array('status' => "error", 'message' => $msg);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        }
        if ($this->getRequest()->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $viewModel->setVariables(array(
            'jsLangTranslate' => $msgArray,
            'passengerResult' => $passengerResult[0],
            'passengerId' => $params['id'],
            'form' => $passengerForm,
            'email_send' => $email_send,
            'passengerEthnicity' => isset($passengerEthnicity[0]) ? $passengerEthnicity[0] : ''
        ));
        return $viewModel;
    }

    /**
     * This Action is used for passenger to add correction
     * @param passenger id.
     * @return confirmation
     * @author Icreon Tech -SR
     */
    public function passengerAddCorrectionAction() {
        //  $this->checkFrontUserAuthenticationAjx();
        $this->getPassengerTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $passenger = new Passenger($this->_adapter);
        $passengerId = $this->decrypt($params['id']);
        $searchParam['passenger_id'] = $passengerId;

        $passengerResult = $this->_passengerTable->getPassengerRecord($searchParam);
        $passengerForm = new PassengerCorrectionForm();
        $maritalStatusArray = $this->maritalStatusArray();
        unset($maritalStatusArray['U']);
        $genderArray = $this->genderArray();
        unset($genderArray['U']);
        $passengerForm->get('marital_status')->setAttribute('options', $maritalStatusArray);
        $passengerForm->get('gender')->setAttribute('options', $genderArray);
        $msgArray = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['passenger_details']);
        $viewModel = new ViewModel();
        if ($this->getRequest()->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $passParam = array();
        $passParam['passengerId'] = $this->decrypt($params['id']);
        $passengerEthnicity = $this->getPassengerTable()->getPassengerEthnicity($passParam);
        $searchParam = array();
        $correctionArray = array();
        $searchParam['user_id'] = $this->auth->getIdentity()->user_id;
        $searchParam['passenger_id'] = $passengerId;
        $searchParam['activity_type'] = 2; // corrections
        $searchParam['status'] = 0; // Pending status
        $correctionArray = $this->_passengerTable->getPassengerCorrectionDetails($searchParam);

        if (count($correctionArray)) {
            $passengerForm->get('annotation_correction_id')->setAttribute('value', $correctionArray[0]['annotation_correction_id']);
            $passengerForm->get('first_name')->setAttribute('value', $correctionArray[0]['first_name']);
            $passengerForm->get('last_name')->setAttribute('value', $correctionArray[0]['last_name']);
            $passengerForm->get('nationality')->setAttribute('value', $correctionArray[0]['nationality']);
            $passengerForm->get('last_residence')->setAttribute('value', $correctionArray[0]['last_residence']);
            $passengerForm->get('arrival_age')->setAttribute('value', $correctionArray[0]['age_at_arrival']);
            $passengerForm->get('gender')->setAttribute('value', $correctionArray[0]['gender']);
            $passengerForm->get('marital_status')->setAttribute('value', $correctionArray[0]['maritial_status']);
            $passengerForm->get('arrival_date')->setAttribute('value', $this->DateFormat($correctionArray[0]['arrival_date'], 'calender'));
            $passengerForm->get('ship')->setAttribute('value', $correctionArray[0]['ship_name']);
            $passengerForm->get('port_departure')->setAttribute('value', $correctionArray[0]['departure_port_name']);
            $passengerForm->get('line_number')->setAttribute('value', $correctionArray[0]['line_no']);
            $passengerForm->get('birth_place')->setAttribute('value', $correctionArray[0]['birth_place']);
        }
        if ($request->isPost()) {
            $passengerForm->setData($request->getPost());
            $passengerForm->setInputFilter($passenger->getInputFilterCorrection());
            if (!$passengerForm->isValid()) {
                $msg = array();
                $errors = $annotationForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['PassengerMessages']['config']['passenger_details'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $dataArray = $passengerForm->getData();
                $validityFlag = 1;
                foreach ($dataArray as $key => $val) {
                    if ($key != 'save' && $key != 'cancel' && $key != 'agreement' && $key != 'annotation_correction_id') {
                        if ($val != '')
                            $validityFlag = 0;
                    }
                }
                if ($validityFlag == 0) {
                    $passenger->exchangeCorrectionArray($passengerForm->getData());
                    $postData = array();
                    $postData['passenger_id'] = $passengerId;
                    $postData['first_name'] = $passenger->first_name;
                    $postData['last_name'] = $passenger->last_name;
                    $postData['nationality'] = $passenger->nationality;
                    $postData['last_residence'] = $passenger->last_residence;
                    $postData['arrival_age'] = $passenger->arrival_age;
                    $postData['gender'] = $passenger->gender;
                    $postData['marital_status'] = $passenger->marital_status;
                    $postData['arrival_date'] = $this->DateFormat($passenger->arrival_date, 'db_date_format');
                    $postData['ship'] = $passenger->ship;
                    $postData['port_departure'] = $passenger->port_departure;
                    $postData['line_number'] = $passenger->line_number;
                    $postData['user_id'] = $this->auth->getIdentity()->user_id;
                    $postData['added_by'] = $this->auth->getIdentity()->user_id;
                    $postData['added_on'] = DATE_TIME_FORMAT;
                    $postData['modified_by'] = $this->auth->getIdentity()->user_id;
                    $postData['modified_on'] = DATE_TIME_FORMAT;
                    $postData['activity_type'] = 2; // for correction
                    $postData['birth_place'] = $passenger->birth_place;

                    if ($request->getPost('annotation_correction_id') == '') { /** insert correction */

                        $flag = $this->_passengerTable->insertPassengerCorrection($postData);
                        if ($flag == 1) {
                            $messages = array('status' => "success", 'message' => $msgArray['CORRECTION_SUCCESS_MESSAGE']);
                            $response->setContent(\Zend\Json\Json::encode($messages));
                            return $response;
                        }
                    } else /** update correction */ {
                        $postData['annotation_correction_id'] = $request->getPost('annotation_correction_id');
                        $flag = $this->_passengerTable->updatePassengerCorrection($postData);
                        if ($flag == 1) {
                            $messages = array('status' => "success", 'message' => $msgArray['CORRECTION_SUCCESS_MESSAGE']);
                            $response->setContent(\Zend\Json\Json::encode($messages));
                            return $response;
                        }
                    }
                } else {
                    $msg['line_number'] = $msgArray['ATLEAST_ONE_ABOVE_VALUE_ERROR'];
                    $messages = array('status' => "error", 'message' => $msg);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        }
        if ($this->getRequest()->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $viewModel->setVariables(array(
            'jsLangTranslate' => $msgArray,
            'passengerResult' => $passengerResult[0],
            'passengerId' => $params['id'],
            'form' => $passengerForm,
            'passengerEthnicity' => isset($passengerEthnicity[0]) ? $passengerEthnicity[0] : ''
        ));
        return $viewModel;
    }

    /**
     * This action is used to delete the saved passenger 
     * @param this will pass an array dataParam search_id int(), user_id int, is_delete enum.
     * @return this will return the confirmation.
     * @author Icreon Tech -DG
     */
    function deleteSavedPassengerAction() {
        $this->checkFrontUserAuthenticationAjx();
        $this->getPassengerTable();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $dataParam = array();
            $response = $this->getResponse();
            $messages = array();
            if ($request->getPost('search_id') != '') {
                $dataParam['search_id'] = $request->getPost('search_id');
                $auth = new AuthenticationService();
                $this->auth = $auth;
                $userDetail = (array) $this->auth->getIdentity();
                $dataParam['user_id'] = $userDetail['user_id'];
                $dataParam['is_delete'] = 1;
                $this->getPassengerTable()->deleteSavedPassenger($dataParam);
                $messages = array('status' => "success");
                return $response->setContent(\Zend\Json\Json::encode($messages));
            }
        }
    }

    /**
     * This action is used to delete the saved manifest search 
     * @param this will pass an array dataParam search_id int(), user_id int, is_delete enum.
     * @return this will return the confirmation.
     * @author Icreon Tech -DG
     */
    function deleteManifestSearchAction() {
        $this->checkFrontUserAuthenticationAjx();
        $this->getPassengerTable();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $dataParam = array();
            $response = $this->getResponse();
            $messages = array();
            if ($request->getPost('search_id') != '') {
                $dataParam['search_id'] = $request->getPost('search_id');
                $auth = new AuthenticationService();
                $this->auth = $auth;
                $userDetail = (array) $this->auth->getIdentity();
                $dataParam['user_id'] = $userDetail['user_id'];
                $dataParam['is_delete'] = 1;
                $this->getPassengerTable()->deleteManifestSearch($dataParam);
                $messages = array('status' => "success");
                return $response->setContent(\Zend\Json\Json::encode($messages));
            }
        }
    }

    /**
     * This action is used to delete the saved ship search 
     * @param this will pass an array dataParam search_id int(), user_id int, is_delete enum.
     * @return this will return the confirmation.
     * @author Icreon Tech -DG
     */
    function deleteSavedShipAction() {
        $this->checkFrontUserAuthenticationAjx();
        $this->getPassengerTable();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $dataParam = array();
            $response = $this->getResponse();
            $messages = array();
            if ($request->getPost('search_id') != '') {
                $dataParam['search_id'] = $request->getPost('search_id');
                $auth = new AuthenticationService();
                $this->auth = $auth;
                $userDetail = (array) $this->auth->getIdentity();
                $dataParam['user_id'] = $userDetail['user_id'];
                $dataParam['is_delete'] = 1;
                $this->getPassengerTable()->deleteSavedShip($dataParam);
                $messages = array('status' => "success");
                return $response->setContent(\Zend\Json\Json::encode($messages));
            }
        }
    }

    /**
     * This action is used to get the passenger list for the ship
     * @param passenger id
     * @return this will passenger list
     * @author Icreon Tech - SR
     */
    public function getTextPassengerListAction() {
        $params = $this->params()->fromRoute();
        $searchPassengerParam = array();
        if (!empty($params['id'])) {
            $passengerId = $this->decrypt($params['id']);
            $searchParam['passenger_id'] = $passengerId;
            $passengerResult = array();
            $passengerResult = $this->getPassengerTable()->getPassengerRecord($searchParam);
            $searchPassengerParam['ship_name'] = $passengerResult[0]['SHIP_NAME'];
            $searchPassengerParam['date_arrive'] = $passengerResult[0]['DATE_ARRIVE'];
        }


        if (!empty($params['ship_name'])) {
            $searchPassengerParam['ship_name'] = $this->decrypt($params['ship_name']);
        }
        if (!empty($params['date_arrive'])) {
            $searchPassengerParam['date_arrive'] = $this->decrypt($params['date_arrive']);
        }

        $request = $this->getRequest();
        $response = $this->getResponse();
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $searchPassengerParam['startIndex'] = $start;
        $searchPassengerParam['recordLimit'] = $limit;
        $searchPassengerParam['sortField'] = $request->getPost('sidx');
        $searchPassengerParam['sortOrder'] = $request->getPost('sord');
        $manifestIDString = '';
		$dataSource = $request->getPost('dataSource');
        if ($request->getPost('manifestImage') != '') {
            $searchPassengerParam['manifestImage'] = $request->getPost('manifestImage');

            if (strtolower(substr($searchPassengerParam['manifestImage'], -3, 3)) == 'jpg' && substr($searchPassengerParam['manifestImage'], 0, 1) != '/') {
                $searchPassengerParam['manifestImage'] = $searchPassengerParam['manifestImage'];
            }
            if($dataSource == 1){
				$fileName = substr($searchPassengerParam['manifestImage'],0,-4);
				$searchPassengerParam['manifestImage'] = $fileName.'.tif';
			} 
            $manifestResult = $this->getPassengerTable()->getManifestPassengerRecord($searchPassengerParam);
            $manifestResultArray = array();
            $searchResult = array();
            $searchResult = $manifestResult;
            $manifestFileName['file_name'] = $searchPassengerParam['manifestImage'];
            $countResultArray = $this->getServiceLocator()->get('Passenger\Model\ManifestTable')->getManifestPassengerCount($manifestFileName);

            $countResult = $countResultArray[0]['passenger_count'];
            $totalRecordCount = $countResult;
            /*
              if (count($manifestResult) > 0) {
              foreach ($manifestResult as $key => $val) {
              $manifestResultArray[] = $val['ID'];
              }
              $manifestIDString = implode(',', $manifestResultArray);
              $searchPassengerParam['passenger_id'] = $manifestIDString;
              $searchResult = $this->getPassengerTable()->getPassengerRecord($searchPassengerParam);
              } */
        } else {

            $searchResult = $this->getPassengerTable()->getPassengerRecord($searchPassengerParam);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $totalRecordCount = $countResult[0]->RecordCount;
        }


        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $totalRecordCount;
        $jsonResult['total'] = ceil($totalRecordCount / $limit);
        $no = $start + 1;
        if (count($searchResult) > 0) {
            foreach ($searchResult as $val) {
                $arrCell['id'] = $val['ID'];
                $passengerId = $this->encrypt($val['ID']);
                $viewPassenger = "<a href='/passenger-details/" . $passengerId . "/" . $this->encrypt('passenger') . "'><img src='" . $this->_config['img_file_path']['path'] . "/images/search-icon3.png' alt='View Passenger Details' title='View Passenger Details'></a>";
                $icon = "<a href='Javascript: void(0)' onClick=showPassengerDetailsSummary('" . $this->encrypt($val['ID']) . "');><img src='" . $this->_config['img_file_path']['path'] . "/img/info-icon.png' width='20' alt='View Passenger Summary' title='View Passenger Summary'></a>";
                $arrCell['cell'] = array(LTRIM($val['REF_PAGE_LINE_NBR'], '0') . '&nbsp;&nbsp;' . $icon, $val['PRIN_LAST_NAME'], $val['PRIN_FIRST_NAME'], $val['PRIN_PLACE_RESI'], $val['ethnicity'], $viewPassenger);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
                $no++;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This action is used to save the passenger to his file
     * @param passenger id
     * @return this will return the confirmation.
     * @author Icreon Tech - SR
     */
    function passengerSaveToFileAction() {
        // $this->checkFrontUserAuthenticationAjx();
        $this->getPassengerTable();
        $request = $this->getRequest();
        $dataParam = array();
        $response = $this->getResponse();
        $messages = array();
        $memberShipConfigAll = $this->getMembershipId($this->auth->getIdentity()->user_id, 'Passenger Record');
        $params = $this->params()->fromRoute();
        $passengerId = $this->decrypt($params['id']);
        $postData['passenger_id'] = $passengerId;
        $postData['user_id'] = $this->auth->getIdentity()->user_id;
        $postData['save_date'] = DATE_TIME_FORMAT;
        $postData['modified_date'] = DATE_TIME_FORMAT;
        /** Check if already into the passenger file */
        $count = 0;
        $searchParam['passenger_id'] = $passengerId;
        $searchParam['user_id'] = $this->auth->getIdentity()->user_id;
        $resultArray = $this->getPassengerTable()->getUserPassengerSearch($searchParam);
        $count = count($resultArray);
        unset($searchParam['passenger_id']);
        $resultArray2 = $this->getPassengerTable()->getUserPassengerSearch($searchParam);
        $countTotal = count($resultArray2);
        if ($memberShipConfigAll <= $countTotal) {
            $messages = array('status' => "exceed");
        } elseif ($count == 0) {
            $flag = $this->getPassengerTable()->addToFile($postData);
            if ($flag)
                $messages = array('status' => "success");
            else
                $messages = array('status' => "error");
        }
        else
            $messages = array('status' => "already");
        return $response->setContent(\Zend\Json\Json::encode($messages));
    }

    /**
     * This action is used to save the ship to his file
     * @param ship id
     * @return this will return the confirmation.
     * @author Icreon Tech - SR
     */
    function shipSaveToFileAction() {
        // $this->checkFrontUserAuthenticationAjx();
        $this->getPassengerTable();
        $response = $this->getResponse();
        $messages = array();
        $params = $this->params()->fromRoute();
        $shipId = $this->decrypt($params['id']);
        $arrivalDate = $this->decrypt($params['arrival_date']);
        $postData['line_ship_id'] = $shipId;
        $postData['ship_arrival_date'] = $arrivalDate;
        $postData['user_id'] = $this->auth->getIdentity()->user_id;
        $postData['save_date'] = DATE_TIME_FORMAT;
        $postData['modified_date'] = DATE_TIME_FORMAT;
        $postData['title'] = $this->decrypt($params['ship_name']);
        /** Check if already into the ship file */
        $count = 0;
        $searchParam['line_ship_id'] = $shipId;
        $searchParam['ship_arrival_date'] = $arrivalDate;
        $searchParam['user_id'] = $this->auth->getIdentity()->user_id;
        $resultArray = $this->getPassengerTable()->getUserShipSearch($searchParam);
        $count = count($resultArray);
        $max_allowed_search = $this->getMembershipId($this->auth->getIdentity()->user_id, 'Ship Record');
        if ($max_allowed_search <= $count)
            $messages = array('status' => "exceed");
        elseif ($count == 0) {
            $flag = $this->getPassengerTable()->addToFileShip($postData);
            if ($flag)
                $messages = array('status' => "success");
            else
                $messages = array('status' => "error");
        }
        else
            $messages = array('status' => "already");
        return $response->setContent(\Zend\Json\Json::encode($messages));
    }

    /**
     * This action is used to save the additional image
     * @param ship id
     * @return this will return the confirmation.
     * @author Icreon Tech - SR
     */
    public function addAdditionalImageAction() {
        $this->checkFrontUserAuthenticationAjx();
        $this->getPassengerTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('act') == 'save') {
                $postData = array();
                $shipManifestArray = array();
                $postData['passenger_id'] = $this->decrypt($request->getPost('passengerId'));
                $postData['file_name'] = $this->decrypt($request->getPost('fileName'));
                $postData['user_id'] = $this->auth->getIdentity()->user_id;
                $postData['file_path'] = $request->getPost('filePath');

                $searchParam['passenger_id'] = $this->decrypt($request->getPost('passengerId'));
                $searchParam['user_id'] = $this->auth->getIdentity()->user_id;
                $searchParam['filename'] = $this->decrypt($request->getPost('fileName'));
                $shipManifestArray = $this->getPassengerTable()->getPassengerManifestAdditionalImage($searchParam);
                if (count($shipManifestArray) == 0)
                    $this->getPassengerTable()->savePassengerManifestAdditionalImage($postData);
            }
        }
        $searchParam = array();
        $shipManifestArray = array();
        $searchParam['passenger_id'] = $this->decrypt($request->getPost('passengerId'));
        $searchParam['user_id'] = $this->auth->getIdentity()->user_id;
        $shipManifestArray = $this->getPassengerTable()->getPassengerManifestAdditionalImage($searchParam);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        if ($this->getRequest()->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $viewModel->setVariables(array(
            'shipManifestArray' => $shipManifestArray,
            'firstImagePrice' => $request->getPost('firstImagePrice')
        ));
        return $viewModel;
    }

    /**
     * This action is used to remove the additional image
     * @param image id
     * @return this will return the confirmation.
     * @author Icreon Tech - SR
     */
    public function deleteAdditionalImageAction() {
        //$this->checkFrontUserAuthenticationAjx();
        $this->getPassengerTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $param['image_id'] = $request->getPost('image_id');
            $param['user_id'] = $this->auth->getIdentity()->user_id;
            $status = $this->getPassengerTable()->removePassengerManifestAdditionalImage($param);

            if ($status)
                $messages = array('status' => "success");
            else
                $messages = array('status' => "error");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This action is used to show the passenger details
     * @param id
     * @return this will return the passenger details.
     * @author Icreon Tech - SR
     */
    public function showPassengerDetailsSummaryAction() {
        $this->getPassengerTable();
        $params = $this->params()->fromRoute();
        $searchParam['passenger_id'] = $this->decrypt($params['id']);
        $passengerDetail = $this->getServiceLocator()->get('Passenger\Model\DocumentTable')->searchCrmPassenger($searchParam);
        $msgArray = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['passenger_details']);
        $this->layout('web-popup');
        $viewModel = new ViewModel();
        if ($this->getRequest()->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $passParam = array();
        $passParam['passengerId'] = $this->decrypt($params['id']);
        $passengerEthnicity = $this->getPassengerTable()->getPassengerEthnicity($passParam);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $msgArray,
            'searchResult' => $passengerDetail[0],
            'passengerEthnicity' => isset($passengerEthnicity[0]) ? $passengerEthnicity[0] : '',
            'maritalStatusArray' => $this->maritalStatusArray(),
            'genderArray' => $this->genderArray()
        ));
        return $viewModel;
    }

    /**
     * This action is used to save the ship to his file
     * @param ship id
     * @return this will return the confirmation.
     * @author Icreon Tech - SR
     */
    function manifestSaveToFileAction() {
        $this->checkFrontUserAuthenticationAjx();
        $this->getPassengerTable();
        $response = $this->getResponse();
        $messages = array();
        $params = $this->params()->fromRoute();
        $imageName = $this->decrypt($params['id']);
        // asd($params);

        if ($params['data_source'] == 2) {
            $postData['frame'] = substr($imageName, -9, 5);
            $postData['roll_nbr'] = 'T715-' . substr($imageName, 0, 9);


            // $searchParam['frame'] = $postData['frame'];            
            // $searchParam['roll_nbr'] = $postData['roll_nbr'];
        } else {

            // echo $imageName;die;
            $postData['roll_nbr'] = substr($imageName, 0, 9);
            $postData['frame'] = substr($imageName, 9, 4);

            // $searchParam['file_name'] = $imageName;
            //$searchParam['frame'] = $postData['frame'];            
            //$searchParam['roll_nbr'] = $postData['roll_nbr'];
        }

        $searchParam['file_name'] = $imageName;
        $postData['user_id'] = $this->auth->getIdentity()->user_id;
        $postData['save_date'] = DATE_TIME_FORMAT;
        $postData['modified_date'] = DATE_TIME_FORMAT;
        $postData['ship_arrival_date'] = $this->decrypt($params['date_arrival']);
        $postData['ship_id'] = $this->decrypt($params['ship_id']);
        $postData['ship_name'] = $this->decrypt($params['ship_name']);
        $postData['passenger_id'] = $this->decrypt($params['passenger_id']);
        $postData['file_name'] = $imageName;

        /** Check if already into the ship file */
        $count = 0;


        $searchParam['user_id'] = $this->auth->getIdentity()->user_id;
        $resultArray = $this->getPassengerTable()->getUserManifestSearch($searchParam);
        $manifestSavedSearch = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getUserManifestSearch(array('user_id' => $this->auth->getIdentity()->user_id));
        $max_allowed_search = $this->getMembershipId($this->auth->getIdentity()->user_id, 'Ship Manifest');
        if ($max_allowed_search <= count($manifestSavedSearch)) {
            $messages = array('status' => "exceed");
        } else {
            if (empty($resultArray)) {

                $flag = $this->getPassengerTable()->addToFileManifest($postData);
                if ($flag)
                    $messages = array('status' => "success");
                else
                    $messages = array('status' => "error");
            }else
                $messages = array('status' => "already");
        }
        return $response->setContent(\Zend\Json\Json::encode($messages));
    }

    /**
     * This action is used for sphinx search for passenger
     * @param $request, $searchType, $page
     * @return this will return the array of records.
     * @author Icreon Tech - SR
     */
    function getsphinxSearchPassenger($request, $searchType, $page, $config = '', $passIdsArr = '', $isSavePassengerRecord = 0) {

        $response = $this->getResponse();
        //asd($config);
        /** Sphinx Setting section */
        if (isset($config) && !empty($config)) {
            if (isset($config['pageSource']) && ($config['pageSource'] == 'grid_view' || $config['pageSource'] == 'list_view')) {
                $sphinxServer = $this->_config['SphinxServer'];
            } else {
                $sphinxServer = $config['SphinxServer'];
            }
        } else {
            $sphinxServer = $this->_config['SphinxServer'];
        }
        $sphinxComponentObj = new SphinxComponent($sphinxServer['serverName']);
        $sphinxComponentObj->initialize();
        $arrSphinxIndexes = $sphinxServer['passengerIndexes'];
        if ($request->getPost('searchString') != '') {
            parse_str($request->getPost('searchString'), $searchParam);
        } else {
            $searchParam = $request->getPost();
        }
        if (@isset($searchParam['birth_year_from']) && $searchParam['birth_year_from'] != '') {
            $searchParam['birth_year'] = $searchParam['birth_year_from'] . ' - ' . $searchParam['birth_year_to'];
            $searchParam['current_age'] = $searchParam['current_age_from'] . ' - ' . $searchParam['current_age_to'];
            $searchParam['arrival_age'] = $searchParam['arrival_age_from'] . ' - ' . $searchParam['arrival_age_to'];
            $searchParam['year_of_arrival'] = $searchParam['year_of_arrival_from'] . ' - ' . $searchParam['year_of_arrival_to'];
            $searchParam['month_of_arrival'] = $searchParam['month_of_arrival_from'] . ' - ' . $searchParam['month_of_arrival_to'];
            $searchParam['day_of_arrival'] = $searchParam['day_of_arrival_from'] . ' - ' . $searchParam['day_of_arrival_to'];
        }

        $manifestArray = array();
        if (isset($searchParam['companion_name']) && $searchParam['companion_name'] != '') {
            $searchParam['companion_name'] = $searchParam['companion_name'];
            $manifestArray = $this->getPassengerTable()->getPassengeManifestImage($searchParam);
        }

        if ($request->getPost('sidx')) {
            $searchParam['sort_field'] = $request->getPost('sidx');
        }
        if ($request->getPost('sord')) {
            $searchParam['sort_order'] = $request->getPost('sord');
        }
//asd($config);
        $arrSphinxSettingsStr = '';
        if (isset($config['pageSource']) && $config['pageSource'] == 'transaction') {
            $limit = ($request->getPost('rows') != '') ? $request->getPost('rows') : $this->_config['records_per_page']['numRecPerPage'];
        } else if (isset($config['pageSource']) && $config['pageSource'] == 'grid_view') {
            $limit = $this->_config['records_per_page']['numRecPerPageGridView'];
        } else if (isset($config['pageSource']) && $config['pageSource'] == 'list_view') {
            $limit = $this->_config['records_per_page']['numRecPerPageListView'];
        } else {
            $limit = isset($config['records_per_page']['numRecPerPage']) ? $config['records_per_page']['numRecPerPage'] : $this->_config['records_per_page']['numRecPerPage'];
        }

        $arrSphinxSettings = array('limit' => (int) $limit,
            'index' => $arrSphinxIndexes,
            'page' => (int) $page,
            // 'groupby'        =>  array(array("sphinx_group_by" , '@weight DESC')),
            'rankingMode' => SPH_RANK_PROXIMITY_BM25, // SPH_RANK_PROXIMITY_BM25,
            'matchMode' => SPH_MATCH_EXTENDED2, //SPH_MATCH_EXTENDED2, //SPH_MATCH_BOOLEAN, //SPH_MATCH_EXTENDED2,
            'filter' => array()
        );

        $sphinxComponentObj->SetFieldWeights(array('field0' => 10000,
            'field3' => 5000,
            'field2' => 100,
            'field18' => 100,
            'field7' => 100,
            'field9' => 60,
            'field11' => 50,
            'field13' => 100,
            'field16' => 5000,
            'field25' => 200,
            'field26' => 100,
            'field27' => 1110,
            'field28' => 50,
            'field29' => 50,
            'field30' => 50,
            'field31' => 50,
            'field32' => 50,
            'field21' => 50,
            'field20' => 50
        ));
        $sphinxComponentObj->SetIndexWeights($sphinxServer['passengerIndexesIndexWeights']);
        $passengerId = (isset($searchParam['passenger_id']) && $searchParam['passenger_id'] != '') ? trim($searchParam['passenger_id']) : '';
        
        //$ship = (isset($searchParam['ship_name']) && $searchParam['ship_name'] != '') ? trim($searchParam['ship_name']) : '';
        
        $ship = '';
        if(isset($searchParam['ship']) && $searchParam['ship'] != '') {
            $ship = trim($searchParam['ship']);
        }
        if(isset($searchParam['ship_name']) && $searchParam['ship_name'] != '') {
            $ship = trim($searchParam['ship_name']);
        } 
        
        
        $inititalName = (isset($searchParam['initital_name']) && $searchParam['initital_name'] != '') ? trim($searchParam['initital_name']) : '';
        $firstName = (isset($searchParam['first_name']) && $searchParam['first_name'] != '') ? trim($searchParam['first_name']) : '';
        $lastName = (isset($searchParam['last_name']) && $searchParam['last_name'] != '') ? trim($searchParam['last_name']) : '';
        if (isset($searchParam['year_of_arrival']) && $searchParam['year_of_arrival'] != '') {
            $arrYearArr = explode(' - ', $searchParam['year_of_arrival']);
            $arrivalYearFrom = $arrYearArr[0];
            $arrivalYearTo = $arrYearArr[1];
        } else {
            $arrivalYearFrom = '';
            $arrivalYearTo = '';
        }
        $dayArrival = (isset($searchParam['day_of_arrival']) && $searchParam['day_of_arrival'] != '') ? trim($searchParam['day_of_arrival']) : '';
        $monthArrival = (isset($searchParam['month_of_arrival']) && $searchParam['month_of_arrival'] != '') ? trim($searchParam['month_of_arrival']) : '';
        if (isset($monthArrival) && $monthArrival != '') {
            $arrMonthArr = explode(' - ', $monthArrival);
            $monthArrivalFrom = $arrMonthArr[0];
            $monthArrivalTo = isset($arrMonthArr[1]) ? $arrMonthArr[1] : $arrMonthArr[0];
        } else {
            $monthArrivalFrom = '';
            $monthArrivalTo = '';
        }

        if (isset($dayArrival) && $dayArrival != '') {
            $arrDayArr = explode(' - ', $dayArrival);
            $dayArrivalFrom = $arrDayArr[0];
            $dayArrivalTo = isset($arrDayArr[1]) ? $arrDayArr[1] : $arrDayArr[0];
        } else {
            $dayArrivalFrom = '';
            $dayArrivalTo = '';
        }
        if (isset($searchParam['birth_year']) && $searchParam['birth_year'] != '') {
            $birthYearArr = explode(' - ', $searchParam['birth_year']);
            $birthYearFrom = $birthYearArr[0];
            $birthYearTo = $birthYearArr[1];
        } else {
            $birthYearFrom = '';
            $birthYearTo = '';
        }
        if (isset($searchParam['current_age']) && $searchParam['current_age'] != '') {
            $ageArr = explode(' - ', $searchParam['current_age']);
            $currentAgeFrom = $ageArr[0];
            $currentAgeTo = $ageArr[1];
        } else {
            $currentAgeFrom = '';
            $currentAgeTo = '';
        }


        if (isset($searchParam['arrival_age']) && $searchParam['arrival_age'] != '') {
            $arrivalAge = explode(' - ', $searchParam['arrival_age']);
            $arrivalAgeFrom = $arrivalAge[0];
            $arrivalAgeTo = $arrivalAge[1];
        } else {
            $arrivalAgeFrom = '';
            $arrivalAgeTo = '';
        }

        $gender = (isset($searchParam['gender']) && $searchParam['gender'] != '') ? $searchParam['gender'] : '';
        $maritalStatus = (isset($searchParam['marital_status']) && $searchParam['marital_status'] != '') ? $searchParam['marital_status'] : '';
        $deptPort = (isset($searchParam['port_of_departure']) && $searchParam['port_of_departure'] != '') ? trim($searchParam['port_of_departure']) : '';
        $arrivalPort = (isset($searchParam['arrival_port']) && $searchParam['arrival_port'] != '') ? trim($searchParam['arrival_port']) : '';
        $ethnicity = (isset($searchParam['ethnicity']) && $searchParam['ethnicity'] != '') ? $searchParam['ethnicity'] : '';
        $town = (isset($searchParam['town']) && $searchParam['town'] != '') ? trim($searchParam['town']) : '';
        $searchTypeTown = (isset($searchParam['search_type_town']) && $searchParam['search_type_town'] != '') ? trim($searchParam['search_type_town']) : '';
        $lastResidence = (isset($searchParam['last_residence']) && $searchParam['last_residence'] != '') ? trim($searchParam['last_residence']) : '';

        $placeOfBirth = (isset($searchParam['place_of_birth']) && $searchParam['place_of_birth'] != '') ? $searchParam['place_of_birth'] : '';

        $arrSphinxSettingsStr = '';
        if (isset($arrivalYearFrom) && $arrivalYearFrom != '')
            $sphinxComponentObj->SetFilterRange('field19', $arrivalYearFrom, $arrivalYearTo);
        if (isset($arrivalAgeFrom) && $arrivalAgeFrom != '')
            $sphinxComponentObj->SetFilterRange('field5', $arrivalAgeFrom, $arrivalAgeTo);
        if (isset($monthArrivalFrom) && $monthArrivalFrom != '')
            $sphinxComponentObj->SetFilterRange('field20', (int) $monthArrivalFrom, (int) $monthArrivalTo);
        if (isset($dayArrivalFrom) && $dayArrivalFrom != '')
            $sphinxComponentObj->SetFilterRange('field21', (int) $dayArrivalFrom, (int) $dayArrivalTo);
        if (isset($passengerId) && $passengerId != '')
            $arrSphinxSettingsStr.= ' & (@(field0) ("*' . trim($passengerId) . '*"))';

        if ($isSavePassengerRecord) {
            if (!empty($passIdsArr)) {
                $passIdsArray = array();
                foreach ($passIdsArr as $key => $val) {
                    $passIdsArray[] = ' ("*' . $val . '*") ';
                }
                $passengerIdStr = implode('|', $passIdsArray);
                $arrSphinxSettingsStr.= ' & (@(field0)' . $passengerIdStr . ')';
            } else {
                $passengerIdStr = '34343434343434343434343434';
                $arrSphinxSettingsStr.= ' & (@(field0)' . $passengerIdStr . ')';
            }
        }
        if (isset($ship) && $ship != '')
            $arrSphinxSettingsStr.= ' & (@(field7) ("*' . trim($ship) . '*"))';
        if (isset($inititalName) && $inititalName != '')
            $arrSphinxSettingsStr.= ' & (@(field2) ("' . trim($inititalName) . '*"))';

        if (isset($config['pageSource']) && $config['pageSource'] == 'transaction') {
            if (isset($firstName) && $firstName != '')
                $arrSphinxSettingsStr.= ' & (@(field2) ("^' . trim($firstName) . '*"))';
        }
        else {
            if (isset($firstName) && $firstName != '')
                $arrSphinxSettingsStr.= ' & (@(field2) ("*' . trim($firstName) . '*"))';
        }


        if (isset($birthYearFrom) && $birthYearFrom != '') {
            if ($birthYearFrom == 1680 && $birthYearTo == 1974) {
                // No condition for BIRTH_YEAR
            } else {
                $sphinxComponentObj->SetFilterRange('field12', $birthYearFrom, $birthYearTo);
            }
        }
        if (isset($currentAgeFrom) && $currentAgeFrom != '') {
            if ($currentAgeFrom == 40 && $currentAgeTo == 230) {
                // No condition for Current Age Range
            } else {
                $sphinxComponentObj->SetFilterRange('field33', $currentAgeFrom, $currentAgeTo);
            }
        }
        if (!empty($gender)) {
            $genderArray = array();
            $genderStr = '';
            if (is_array($gender) && !empty($gender)) {
                foreach ($gender as $key => $val) {
                    $genderArray[] = ' ("*' . $val . '*") ';
                }
                //$genderArray[] = ' ("U") ';
                //$genderArray[] = ' ("") ';
                $genderStr = implode('|', $genderArray);
            } else {
                $genderStr = $gender;
            }
            $arrSphinxSettingsStr.= ' & (@(field10)' . $genderStr . ')';
        }

        if (!empty($placeOfBirth))
            $arrSphinxSettingsStr.= ' & (@(field26) ("*' . trim($placeOfBirth) . '*"))';

        if (!empty($maritalStatus)) {
            $maritalArray = array();
            $maritalStr = '';
            if (is_array($maritalStatus) && !empty($maritalStatus)) {
                foreach ($maritalStatus as $key => $val) {
                    $maritalArray[] = ' ("*' . $val . '*") ';
                }
                $maritalStr = implode('|', $maritalArray);
            } else {
                $maritalStr = $maritalStatus;
            }
            $arrSphinxSettingsStr.= ' & (@(field11)' . $maritalStr . ')';
        }

        if (!empty($ethnicity)) {
            $ethnicityArray = array();
            $ethnicityStr = '';
            if (is_array($ethnicity) && !empty($ethnicity)) {
                foreach ($ethnicity as $key => $val) {
                    $ethnicityArray[] = ' ("*' . trim($val) . '*") ';
                }
                $ethnicityStr = implode('|', $ethnicityArray);
            } else {
                $ethnicityStr = $ethnicity;
            }
            $arrSphinxSettingsStr.= ' & (@(field36)' . $ethnicityStr . ')';
        }

        if (isset($deptPort) && $deptPort != '')
            $arrSphinxSettingsStr.= ' & (@(field24) ("*' . trim($deptPort) . '*"))';
        if (isset($arrivalPort) && $arrivalPort != '')
            $arrSphinxSettingsStr.= ' & (@(field25) ("*' . trim($arrivalPort) . '*"))';

		//commented this code due to ticket 56
        /*if (isset($town) && $town != '')
            $arrSphinxSettingsStr.= ' & (@(field4) ("*' . trim($town) . '*"))';
		*/
		//ticket 56
		 if (isset($town) && $town != '') {
            if ($searchTypeTown == 'exact') { //Exact Match
                $arrSphinxSettingsStr.= ' & (@(field4) (' . $town . '))';
            } else if ($searchTypeTown == 'startwith') { //Start with
                $arrSphinxSettingsStr.= ' & (@(field4) ("^' . $town . '*"))';
            } else if ($searchTypeTown == 'soundlike') { // sound like
                $arrSphinxSettingsStr.= ' & (@(field40) ("*' . soundex($town) . '*"))';
            }
            else
                $arrSphinxSettingsStr.= ' & (@(field4) ("*' . $town . '*"))';
        }
        if (isset($lastResidence) && $lastResidence != '')
            $arrSphinxSettingsStr.= ' & (@(field4) ("*' . $lastResidence . '*"))';
        
        if ($searchParam['last_name'] != '') {

            if ($searchType == 'search_type1') { // Exact Matches
                // $arrSphinxSettingsStr.= ' & (@(field3) ("^' . $searchParam['last_name'] . '$"))';
                $arrSphinxSettingsStr.= ' & (@(field1) ("_START_' . trim($searchParam['last_name']) . '_END_"))';
            }
            if ($searchType == 'search_type2') // Close Matches
                $arrSphinxSettingsStr.= ' & (@(field3) ("^' . trim($searchParam['last_name']) . '*"))';

            if ($searchType == 'search_type3') // Sounds Like
                $arrSphinxSettingsStr.= ' & (@(field35) ("*' . soundex(trim($searchParam['last_name'])) . '*"))';

            if ($searchType == 'search_type4') { // Alternate Spellings
                if ($_SERVER['HTTP_HOST'] == 'local.soleif_hot') {
                    $arrSphinxSettingsStr.= ' & (@(field35) ("*' . soundex(trim($searchParam['last_name'])) . '*"))';
                } else {
                    $wordsArray = $this->AlternateSpelling(trim($searchParam['last_name']));
                    $wordsArray = array_slice($wordsArray, 0, 5);
                    $newwordsArray = array();
                    $newwordsArray[] = $searchParam['last_name'];
                    foreach ($wordsArray as $wordVal) {
                        $newwordsArray[] = str_replace("'", "", $wordVal);
                    }

                    foreach ($newwordsArray as $key => $val) {
                        $newwordsArray[] = ' ("*' . $val . '*") ';
                    }
                    $wordsStr = implode('|', $newwordsArray);
                    $arrSphinxSettingsStr.= ' & (@(field3)' . $wordsStr . ')';
                }
                //$arrSphinxSettingsStr.= ' & (@(field35) ("*' . soundex($searchParam['last_name']) . '*"))';
            }
            if ($searchType == 'search_type5') // Contains
                $arrSphinxSettingsStr.= ' & (@(field3) ("*' . trim($searchParam['last_name']) . '*"))';

            if ($searchType == 'search_type6') // Where Last Name is First 
                $arrSphinxSettingsStr.= ' & (@(field2) ("^' . trim($searchParam['last_name']) . '$"))';
        }
        // used for companion search
        if (count($manifestArray) > 0) {
            $newManifestArray = array();
            foreach ($manifestArray as $fileVal) {
                $newManifestArray[] = $fileVal['FILENAME'];
            }
            //$newManifestArray[] = $manifestArray[0]['FILENAME'];
            foreach ($newManifestArray as $key => $val) {
                $newArray[] = ' ("*' . $val . '*") ';
            }
            $wordsStr2 = implode('|', $newArray);
            $arrSphinxSettingsStr.= ' & (@(field32)' . $wordsStr2 . ')';
        }
        $arrSphinxSettings['search'] = $arrSphinxSettingsStr;
        // PRIN_LAST_NAME,PRIN_FIRST_NAME,PRIN_PLACE_RESI,AGE_ARRIVE,DATE_ARRIVE

        if (isset($searchParam['sort_order']) && $searchParam['sort_order'] != '')
            $searchParam['sort_order'] = $searchParam['sort_order'];
        else
            $searchParam['sort_order'] = 'asc';

        if (isset($searchParam['sort_field']) && trim($searchParam['sort_field']) != '') {
            $searchParam['sort_field'] = trim($searchParam['sort_field']);
            $sphinxComponentObj->setSortMode(($searchParam['sort_order'] == 'desc') ? SPH_SORT_ATTR_DESC : SPH_SORT_ATTR_ASC, $searchParam['sort_field']);
        } else {
            $searchParam['sort_field'] = 'field3';
            $sphinxComponentObj->SetSortMode(SPH_SORT_EXTENDED, "field3 ASC, field2 asc"); // Last name, First Name
            //$sphinxComponentObj->SetSortMode(SPH_SORT_EXTENDED, "field3 ASC, field6 ASC, field18 ASC"); // Last name, Ship Name, Year of Arrivals 
        }
        // $sphinxComponentObj->setSortMode(($request->getPost('sord') == 'desc') ? SPH_SORT_ATTR_DESC : SPH_SORT_ATTR_ASC, 'field0');

        if ($request->isPost()) {

            if ($request->getPost('search_id') != '') {
                $getSearchArray['user_id'] = $this->auth->getIdentity()->user_id;
                $getSearchArray['isActive'] = 1;
                $getSearchArray['search_type'] = 1;
                $getSearchArray['search_id'] = $request->getPost('search_id');
                $seachResult = $this->getPassengerTable()->getPassengerSearch($getSearchArray);

                if ($request->getPost('returnSearchParam') == 'yes') {
                    $seachResult = json_encode(unserialize($seachResult[0]->search_query));
                    return $response->setContent($seachResult);
                }
            }
            if ($request->getPost('search_id') == '') {
                //parse_str($request->getPost('searchString'), $searchParam);
                return $searchResult = $sphinxComponentObj->search($arrSphinxSettings);
            }
        }
    }

    /**
     * This action is used to get the ship line
     * @param builder name
     * @return this will return all ship line list
     * @author Icreon Tech - SR
     */
    public function getShipLineAction() {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $postArray = $request->getPost()->toArray();
        $postArray['ship_line'] = $postArray['ship_line'];
        $postArray['limit'] = 10;
        $resultarray = $this->getPassengerTable()->getShipLine($postArray);
        $allRecords = array();
        $i = 0;
        if ($resultarray !== false) {
            foreach ($resultarray as $resultarray) {
                $allRecords[$i] = array();
                $allRecords[$i]['line_name'] = $resultarray['line_name'];
                $i++;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($allRecords));
        return $response;
        /* End Contacts */
    }

    /**
     * This action is used to get the ship builder lsiting
     * @param builder name
     * @return this will return all builder list
     * @author Icreon Tech - SR
     */
    public function getShipBuilderAction() {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $postArray = $request->getPost()->toArray();
        $postArray['ship_builder'] = $postArray['ship_builder'];
        $postArray['limit'] = 10;
        $resultarray = $this->getPassengerTable()->getShipBuilder($postArray);

        $allRecords = array();
        $i = 0;
        if ($resultarray !== false) {
            foreach ($resultarray as $resultarray) {
                $allRecords[$i] = array();
                $allRecords[$i]['builder_name'] = $resultarray['builder_name'];
                $i++;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($allRecords));
        return $response;
        /* End Contacts */
    }

    /**
     * This action is used for sphinx search for ship
     * @param $request, $searchType, $page
     * @return this will return the array of records.
     * @author Icreon Tech - SR
     */
    function getsphinxSearchShip($request, $searchType, $page, $config = '') {
        $response = $this->getResponse();
        /** Sphinx Setting section */
        $sphinxServer = $this->_config['SphinxServer'];
        $sphinxComponentObj = new SphinxComponent($sphinxServer['serverName']);
        $sphinxComponentObj->initialize();
        //$arrSphinxIndexes = array('ship'); 
        $arrSphinxIndexes = $this->_config['SphinxServer']['shipIndexes'];
        parse_str($request->getPost('searchString'), $searchParam);
        //asd($searchParam);
        if (isset($config['pageSource']) && $config['pageSource'] == 'grid_view') {
            $limit = $this->_config['records_per_page']['numRecPerPageGridView'];
        } else if (isset($config['pageSource']) && $config['pageSource'] == 'list_view') {
            $limit = $this->_config['records_per_page']['numRecPerPageListView'];
        } else {
            $limit = isset($config['records_per_page']['numRecPerPage']) ? $config['records_per_page']['numRecPerPage'] : $this->_config['records_per_page']['numRecPerPage'];
        }

        $arrSphinxSettingsStr = '';
        // $limit = $this->_config['records_per_page']['numRecPerPage'];
        $arrSphinxSettings = array('limit' => (int) $limit,
            'index' => $arrSphinxIndexes,
            'page' => (int) $page,
            // 'groupby'        =>  array(array("sphinx_group_by" , '@weight DESC')),
            'rankingMode' => SPH_RANK_PROXIMITY_BM25, // SPH_RANK_PROXIMITY_BM25,
            'matchMode' => SPH_MATCH_EXTENDED2, //SPH_MATCH_EXTENDED2, //SPH_MATCH_BOOLEAN, //SPH_MATCH_EXTENDED2,
            'filter' => array()
        );
        $sphinxComponentObj->SetFieldWeights(array('field0' => 10000,
            'field3' => 5000,
            'field2' => 100,
            'field18' => 100,
            'field7' => 100,
            'field9' => 60,
            'field11' => 50,
            'field13' => 100,
            'field16' => 5000,
            'field25' => 200,
            'field26' => 100,
            'field27' => 1110,
            'field28' => 50,
            'field29' => 50,
            'field30' => 50,
            'field31' => 50,
            'field32' => 50,
            'field21' => 50,
            'field20' => 50
        ));
        $sphinxComponentObj->SetIndexWeights($this->_config['SphinxServer']['shipIndexesIndexWeights']);
        $ship = (isset($searchParam['ship_name']) && $searchParam['ship_name'] != '') ? trim($searchParam['ship_name']) : '';
        if (@isset($searchParam['year_of_arrival_from']) && $searchParam['year_of_arrival_from'] != '') {
            $searchParam['year_of_arrival'] = $searchParam['year_of_arrival_from'] . ' - ' . $searchParam['year_of_arrival_to'];
            $searchParam['month_of_arrival'] = $searchParam['month_of_arrival_from'] . ' - ' . $searchParam['month_of_arrival_to'];
            $searchParam['day_of_arrival'] = $searchParam['day_of_arrival_from'] . ' - ' . $searchParam['day_of_arrival_to'];
        }

        if (isset($searchParam['year_of_arrival']) && $searchParam['year_of_arrival'] != '') {
            $arrYearArr = explode(' - ', $searchParam['year_of_arrival']);
            $arrivalYearFrom = $arrYearArr[0];
            $arrivalYearTo = $arrYearArr[1];
        } else {
            $arrivalYearFrom = '';
            $arrivalYearTo = '';
        }

        $dayArrival = (isset($searchParam['day_of_arrival']) && $searchParam['day_of_arrival'] != '') ? trim($searchParam['day_of_arrival']) : '';
        $monthArrival = (isset($searchParam['month_of_arrival']) && $searchParam['month_of_arrival'] != '') ? trim($searchParam['month_of_arrival']) : '';

        if (isset($monthArrival) && $monthArrival != '') {
            $arrMonthArr = explode(' - ', $monthArrival);
            $monthArrivalFrom = $arrMonthArr[0];
            $monthArrivalTo = $arrMonthArr[1];
        } else {
            $monthArrivalFrom = '';
            $monthArrivalTo = '';
        }

        if (isset($dayArrival) && $dayArrival != '') {
            $arrDayArr = explode(' - ', $dayArrival);
            $dayArrivalFrom = $arrDayArr[0];
            $dayArrivalTo = $arrDayArr[1];
        } else {
            $dayArrivalFrom = '';
            $dayArrivalTo = '';
        }

        $deptPort = (isset($searchParam['port_of_departure']) && $searchParam['port_of_departure'] != '') ? trim($searchParam['port_of_departure']) : '';
        $arrivalPort = (isset($searchParam['arrival_port']) && $searchParam['arrival_port'] != '') ? trim($searchParam['arrival_port']) : '';
        $arrSphinxSettingsStr = '';

        if (isset($arrivalYearFrom) && $arrivalYearFrom != '')
            $sphinxComponentObj->SetFilterRange('field19', $arrivalYearFrom, $arrivalYearTo);
        if (isset($monthArrivalFrom) && $monthArrivalFrom != '')
            $sphinxComponentObj->SetFilterRange('field20', (int) $monthArrivalFrom, (int) $monthArrivalTo);
        if (isset($dayArrivalFrom) && $dayArrivalFrom != '')
            $sphinxComponentObj->SetFilterRange('field21', (int) $dayArrivalFrom, (int) $dayArrivalTo);


        if (isset($deptPort) && $deptPort != '')
            $arrSphinxSettingsStr.= ' & (@(field35) ("*' . trim($deptPort) . '*"))';

        if (isset($arrivalPort) && $arrivalPort != '')
            $arrSphinxSettingsStr.= ' & (@(field36) ("*' . trim($arrivalPort) . '*"))';

        if ($searchType == 'search_type1') { // Exact Matches
            //$arrSphinxSettingsStr.= ' & (@(field6) ("^' . trim($searchParam['ship_name']) . '$"))';
            if (isset($searchParam['ship_name']) and trim($searchParam['ship_name']) != "") {
                $arrSphinxSettingsStr.= ' & (@(field1) ("_START_' . trim($searchParam['ship_name']) . '_END_"))';
            }
        }

        if ($searchType == 'search_type2') // Close Matches
            $arrSphinxSettingsStr.= ' & (@(field6) ("^' . trim($searchParam['ship_name']) . '*"))';

        if ($searchType == 'search_type3') // Sounds Like
            $arrSphinxSettingsStr.= ' & (@(field28) ("^' . soundex(trim($searchParam['ship_name'])) . '$"))';

        if ($searchType == 'search_type4') { // Alternate Spellings
            if ($_SERVER['HTTP_HOST'] == 'local.soleif') {
                $arrSphinxSettingsStr.= ' & (@(field28) ("*' . soundex(trim($searchParam['ship_name'])) . '*"))';
                // do nothing
            } else {
                $wordsArray = $this->AlternateSpelling(trim($searchParam['ship_name']));
                $wordsArray = array_slice($wordsArray, 0, 5);
                $newwordsArray = array();
                $newwordsArray[] = trim($searchParam['ship_name']);
                foreach ($wordsArray as $wordVal) {
                    $newwordsArray[] = str_replace("'", "", $wordVal);
                }

                foreach ($newwordsArray as $key => $val) {
                    $newwordsArray[] = ' ("*' . $val . '*") ';
                }

                $wordsStr = implode('|', $newwordsArray);
                $arrSphinxSettingsStr.= ' & (@(field6)' . $wordsStr . ')';
            }
            //$arrSphinxSettingsStr.= ' & (@(field6) ("*' . soundex($searchParam['ship_name']) . '*"))';
        }
        if ($searchType == 'search_type5') // Contains
            $arrSphinxSettingsStr.= ' & (@(field6) ("*' . trim($searchParam['ship_name']) . '*"))';

        if ($searchType == 'search_type6') // Name when Built  
            $arrSphinxSettingsStr.= ' & (@(field2) ("*' . trim($searchParam['ship_name']) . '*"))';

        $shipLine = (isset($searchParam['ship_line']) && $searchParam['ship_line'] != '') ? trim($searchParam['ship_line']) : '';
        $shipBuilder = (isset($searchParam['ship_builder']) && $searchParam['ship_builder'] != '') ? trim($searchParam['ship_builder']) : '';
        if ($shipLine != '')
            $arrSphinxSettingsStr.= ' & (@(field8) ("*' . $shipLine . '*"))';

        if ($shipBuilder != '')
            $arrSphinxSettingsStr.= ' & (@(field3) ("*' . $shipBuilder . '*"))';

        $arrSphinxSettings['search'] = $arrSphinxSettingsStr;
        // PRIN_LAST_NAME,PRIN_FIRST_NAME,PRIN_PLACE_RESI,AGE_ARRIVE,DATE_ARRIVE

        if (isset($searchParam['sort_field']) && $searchParam['sort_field'] != '')
            $searchParam['sort_field'] = $searchParam['sort_field'];
        else
            $searchParam['sort_field'] = 'field18';

        if (isset($searchParam['sort_order']) && $searchParam['sort_order'] != '')
            $searchParam['sort_order'] = $searchParam['sort_order'];
        else
            $searchParam['sort_order'] = 'desc';

        $sphinxComponentObj->setSortMode(($searchParam['sort_order'] == 'desc') ? SPH_SORT_ATTR_DESC : SPH_SORT_ATTR_ASC, $searchParam['sort_field']);
        // $sphinxComponentObj->setSortMode(($request->getPost('sord') == 'desc') ? SPH_SORT_ATTR_DESC : SPH_SORT_ATTR_ASC, 'field0');
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $getSearchArray['user_id'] = $this->auth->getIdentity()->user_id;
                $getSearchArray['isActive'] = 1;
                $getSearchArray['search_type'] = 1;
                $getSearchArray['search_id'] = $request->getPost('search_id');
                $seachResult = $this->getPassengerTable()->getPassengerSearch($getSearchArray);

                if ($request->getPost('returnSearchParam') == 'yes') {
                    $seachResult = json_encode(unserialize($seachResult[0]->search_query));
                    return $response->setContent($seachResult);
                }
            }
            if ($request->getPost('search_id') == '') {
                //parse_str($request->getPost('searchString'), $searchParam);
                return $searchResult = $sphinxComponentObj->search($arrSphinxSettings);
            }
        }
    }

    /**
     * This action is used to search the ship on the bases of search filter / on the bases of search title
     * @param this will search parameters
     * @return this return all list of ships
     * @author Icreon Tech - SR
     */
    public function getSearchShipListAction() {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $this->getPassengerTable();
        $searchParam = '';
        parse_str($request->getPost('searchString'), $searchParam);

        if ($searchParam['result_format'] == 'grid_view') {
            $config['pageSource'] = 'grid_view';
        } else if ($searchParam['result_format'] == 'list_view') {
            $config['pageSource'] = 'list_view';
        }
        if (isset($config['pageSource']) && $config['pageSource'] == 'grid_view') {
            $limit = $this->_config['records_per_page']['numRecPerPageGridView'];
        } else if (isset($config['pageSource']) && $config['pageSource'] == 'list_view') {
            $limit = $this->_config['records_per_page']['numRecPerPageListView'];
        }

        $searchResult = $this->getsphinxSearchShip($request, 'search_type' . $searchParam['match_type'], $request->getPost('page'), $config);
        $div_name = $searchParam['div_name'];
        $userId = '';
        if (!empty($this->auth->getIdentity()->user_id))
            $userId = $this->auth->getIdentity()->user_id;
        $msgArray = array_merge($this->_config['PassengerMessages']['config']['ship_search'], $this->_config['PassengerMessages']['config']['common']);
        //$msgArray = array_merge($this->_config['PassengerMessages']['config']['passenger_search'], $this->_config['PassengerMessages']['config']['passenger_details']);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'translator' => $msgArray,
            'searchResult' => $searchResult,
            'searchParam' => $searchParam,
            'loggedInUserId' => $userId,
            'type' => $searchParam['match_type'],
            'maritalStatusArray' => $this->maritalStatusArray(),
            'genderArray' => $this->genderArray(),
            'page' => $request->getPost('page'),
            'limit' => $limit,
            'shipPath' => $this->_config['file_upload_path']['assets_url'].'ship_image/',
            'div_name' => $div_name,
            'shipRecordsView' => $searchParam['result_format']
        ));
        return $viewModel;
    }

    /**
     * This Action is used get the ship details
     * @param will be the ship name
     * @return this will be array of ship details
     * @author Icreon Tech -SR
     */
    public function shipDetailsAction() {
        $this->checkFrontUserAuthentication();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();

        $sessionShipSearch = new SessionContainer();
        $searchParam = $sessionShipSearch->ship_search;


        $form = new ShipForm();
        $manifestFile = '';
        $manifestFileEnc = '';
        if (isset($params['manifest_file']) && $params['manifest_file'] != '') {
            $manifestFile = $this->decrypt($params['manifest_file']);
            $manifestFileEnc = $params['manifest_file'];
        }
        $shipLineId = '';
        if (isset($params['line_id']) && $params['line_id'] != '') {
           $shipLineId = $params['line_id'];
        }        

        $this->getPassengerTable();
        $shipName = $this->decrypt($params['ship_name']);
        $dateArrive = $this->decrypt($params['date_arrive']);

        $portList = array();
        $portList[' '] = 'Arrival Port';
        $portList['New York'] = 'New York';
        $form->get('arrival_port')->setAttribute('options', $portList);
        if (isset($searchParam['search_type1']) && $searchParam['search_type1'] == 1) {
            $searchTypeArray[] = 'search_type1';
        }
        if (isset($searchParam['search_type2']) && $searchParam['search_type2'] == 1) {
            $searchTypeArray[] = 'search_type2';
        }
        if (isset($searchParam['search_type3']) && $searchParam['search_type3'] == 1) {
            $searchTypeArray[] = 'search_type3';
        }
        if (isset($searchParam['search_type4']) && $searchParam['search_type4'] == 1) {
            $searchTypeArray[] = 'search_type4';
        }
        if (isset($searchParam['search_type5']) && $searchParam['search_type5'] == 1) {
            $searchTypeArray[] = 'search_type5';
        }
        if (isset($searchParam['search_type6']) && $searchParam['search_type6'] == 1) {
            $searchTypeArray[] = 'search_type6';
        }


        if (isset($searchParam['year_of_arrival_from']))
            $searchParam['year_of_arrival'] = $searchParam['year_of_arrival_from'] . ' - ' . $searchParam['year_of_arrival_to'];
        if (isset($searchParam['month_of_arrival_from']))
            $searchParam['month_of_arrival'] = $searchParam['month_of_arrival_from'] . ' - ' . $searchParam['month_of_arrival_to'];
        if (isset($searchParam['day_of_arrival_from']))
            $searchParam['day_of_arrival'] = $searchParam['day_of_arrival_from'] . ' - ' . $searchParam['day_of_arrival_to'];
        $form->get('year_of_arrival_from')->setAttribute('value', isset($searchParam['year_of_arrival_from']) ? $searchParam['year_of_arrival_from'] : '');
        $form->get('year_of_arrival_to')->setAttribute('value', isset($searchParam['year_of_arrival_to']) ? $searchParam['year_of_arrival_to'] : '');
        $form->get('month_of_arrival_from')->setAttribute('value', isset($searchParam['month_of_arrival_from']) ? $searchParam['month_of_arrival_from'] : '');
        $form->get('month_of_arrival_to')->setAttribute('value', isset($searchParam['month_of_arrival_to']) ? $searchParam['month_of_arrival_to'] : '');
        $form->get('day_of_arrival_from')->setAttribute('value', isset($searchParam['day_of_arrival_from']) ? $searchParam['day_of_arrival_from'] : '');
        $form->get('day_of_arrival_to')->setAttribute('value', isset($searchParam['day_of_arrival_to']) ? $searchParam['day_of_arrival_to'] : '');
        $form->get('port_of_departure')->setAttribute('value', isset($searchParam['port_of_departure']) ? $searchParam['port_of_departure'] : '');
        $form->get('arrival_port')->setAttribute('value', isset($searchParam['arrival_port']) ? $searchParam['arrival_port'] : '');
        $form->get('ship_line')->setAttribute('value', isset($searchParam['ship_line']) ? $searchParam['ship_line'] : '');
        $form->get('ship_builder')->setAttribute('value', isset($searchParam['ship_builder']) ? $searchParam['ship_builder'] : '');
        $form->get('ship_name')->setAttribute('value', isset($searchParam['ship_name']) ? $searchParam['ship_name'] : '');


        $msgArray = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['passenger_details'], $this->_config['PassengerMessages']['config']['ship_search']);
        $searchParam['ship_name'] = $shipName;
        $searchParam['date_arrive'] = $dateArrive . " 00:00:00";
        $searchParam['startIndex'] = 0;
        $searchParam['recordLimit'] = 1;
        $searchResult = $this->getPassengerTable()->getPassengerRecord($searchParam);
        $portDepartSearchArray = array();
        $portDepartSearchArray['ship_name'] = $searchResult[0]['SHIP_NAME'];
        $portDepartSearchArray['date_arrive'] = $searchResult[0]['DATE_ARRIVE'];
        $shipDeparturePortResult = $this->getPassengerTable()->getShipDeparturePort($portDepartSearchArray);

        $shipDeparturePortArray = array();
        $shipDeparturePortName = '';
        $shipDeparturePortPoint = '';
        if (count($shipDeparturePortResult) > 0) {
            foreach ($shipDeparturePortResult as $key => $val) {
                $shipDeparturePortArray[] = $val['DEP_PORT_NAME'];
            }
            $shipDeparturePortArray = array_unique($shipDeparturePortArray);
            $shipDeparturePortName = implode(', ', $shipDeparturePortArray);
            $shipDeparturePortPoint = implode('~~', $shipDeparturePortArray);
        }
        $shipResult = array();
        $searchShipParam = array();
        $shipPath = '';
        
        if (!empty($searchResult[0]['SHIP_NAME'])) {
            $searchShipParam['ship_name'] = $searchResult[0]['SHIP_NAME'];
            $searchShipParam['asset_for'] = '1';
            $searchShipParam['ship_line_id'] = $this->decrypt($shipLineId);
            $shipResult = $this->getPassengerTable()->getShipDetails($searchShipParam);
            if (count($shipResult) > 0) {
                $shipResult = $shipResult[0];
                if ($shipResult['ASSETVALUE'] != '')
                    $shipPath = $this->_config['file_upload_path']['assets_url'].'ship_image/' . $shipResult['ASSETVALUE'];
					$shipfilePath =  $this->_config['file_upload_path']['assets_upload_dir'].'ship_image/' . $shipResult['ASSETVALUE'];
            }
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $msgArray,
            'ship_name' => $params['ship_name'],
            'date_arrive' => $params['date_arrive'],
            'passengerId' => '',
            'searchResult' => $searchResult[0],
            'jsLangTranslate' => $msgArray,
            'shipPath' => $shipPath,
			'shipFilePath' => $shipfilePath,
            'user_id' => isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '',
            'mode' => $this->decrypt($params['mode']),
            'shipDeparturePortName' => $shipDeparturePortName,
            'shipDeparturePortPoint' => $shipDeparturePortPoint,
            'shipDeparturePortPointJson' => json_encode($shipDeparturePortArray),
            'manifestFile' => $manifestFileEnc,
            'form' => $form,
            'searchTypeArray' => $searchTypeArray,
            'shipLineId'=>$shipLineId
        ));
        return $viewModel;
    }

    /**
     * This Action is used to get the ship line.
     * @return this will be ship line
     * @author Icreon Tech - SR
     */
    public function shiplineAction() {
        $this->checkFrontUserAuthentication();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $this->getPassengerTable();
        $msgArray = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['passenger_details']);
        $searchParam = array();
        if ($request->getPost('sort_field') != '')
            $searchParam['sort_field'] = $request->getPost('sort_field');
        else
            $searchParam['sort_field'] = 'lineshipTab.SHIPNAME';

        if ($request->getPost('sort_order') != '')
            $searchParam['sort_order'] = $request->getPost('sort_order');
        else
            $searchParam['sort_order'] = 'asc';
        $page = 1;

        $numRecPerPage = $this->_config['records_per_page']['numRecPerPage'] = 10;
        $searchParam['limit'] = $numRecPerPage;
        $searchParam['offset'] = ($page - 1) * $numRecPerPage;
        $line_id = $this->decrypt($params['line_id']);
        $line_name = $this->decrypt($params['line_name']);
        $shipLineParam = array();
		$shipLineParam['lineId'] = $line_id;

		$shipLineResultArray = $this->getPassengerTable()->getShiplineDetailsByLineId($shipLineParam);
       
		//$line_name = $this->decrypt($params['line_name']);
		$line_name = $shipLineResultArray[0]['NAME'];
		$searchParam['line_id'] = $shipLineResultArray[0]['LINEID'];
		$searchParam['data_source'] = $shipLineResultArray[0]['data_source'];
		$searchResult = $this->getPassengerTable()->getShiplineShipListing($searchParam);
        $viewModel = new ViewModel();
        if ($this->getRequest()->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $getProduct = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProduct(array('is_available' => '1', 'featured' => '1', 'offset' => '1'));
        $discount = '';
        /*if (!empty($this->auth->getIdentity()->user_id)) {

            $user_id = $this->auth->getIdentity()->user_id;
            $userArr = array('user_id' => $user_id);
            $contactDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($userArr);
            if ($contactDetail[0]['membership_id'] != '1') {
                $discount = $contactDetail[0]['discount'];
            }
        }*/
        if (!empty($this->auth->getIdentity()->user_id)) {
            if (defined('MEMBERSHIP_DISCOUNT') && $getProduct[0]['is_membership_discount'] == 1) {
                            $discount = MEMBERSHIP_DISCOUNT;
            }
        }         
        $assetsUploadDirProduct = $this->_config['file_upload_path']['assets_upload_dir'] . 'product/thumbnail';
        $uploadedFilePathProduct = $this->_config['file_upload_path']['assets_url'] . 'product/thumbnail';

        $viewModel->setVariables(array(
            'getProduct' => $getProduct,
            'discount' => $discount,
            'uploadedFilePathProduct' => $uploadedFilePathProduct,
			'assetsUploadDirProduct' => $assetsUploadDirProduct,
            // 'line_name' => $params['line_name'],
            'line_name' => $line_name,
            'display_line_name' => $line_name,
            'line_id' => $this->encrypt($searchParam['line_id']),
            'data_source' => $this->encrypt($searchParam['data_source']),
            'page' => $page,
            'sort_order' => $searchParam['sort_order'],
            'searchResult' => $searchResult,
            'jsLangTranslate' => $msgArray,
            'limit' => $numRecPerPage
        ));
        return $viewModel;
    }

    /**
     * This Action is used to get ship line listing more
     * @author Icreon Tech -SR
     */
    public function shiplineListMoreAction() {
        $this->checkFrontUserAuthentication();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        //asd($request->getPost());
        $this->getPassengerTable();
        $msgArray = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['passenger_details']);

        parse_str($request->getPost('ship'), $searchParamData);
        // asd($searchParam);

        if ($request->getPost('sort_field') != '')
            $searchParam['sort_field'] = $request->getPost('sort_field');
        else
            $searchParam['sort_field'] = 'lineshipTab.SHIPNAME';

        if ($request->getPost('sort_order') != '')
            $searchParam['sort_order'] = $request->getPost('sort_order');
        else {
            if ($searchParamData['sort_order'] != '')
                $searchParam['sort_order'] = $searchParamData['sort_order'];
            else
                $searchParam['sort_order'] = 'asc';
        }

        if ($request->getPost('page') != '')
            $page = $request->getPost('page');
        else
            $page = 1;





        $numRecPerPage = $this->_config['records_per_page']['numRecPerPage'] = 10;
        $searchParam['limit'] = $numRecPerPage;
        $searchParam['offset'] = ($page - 1) * $numRecPerPage;

        $line_id = $this->decrypt($params['line_id']);
        $line_name = $this->decrypt($params['line_name']);
        $searchParam['line_id'] = $line_id;
		
		$shipLineParam = array();
        $shipLineResultArray = array();
        $searchParam['data_source'] = $this->decrypt($params['data_source']);

        //asd($searchParam,2);
        $searchResult = $this->getPassengerTable()->getShiplineShipListing($searchParam);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'line_name' => $params['line_name'],
            'line_id' => $params['line_id'],
            'page' => $page,
            'searchResult' => $searchResult,
            'jsLangTranslate' => $msgArray,
            'limit' => $numRecPerPage,
            'sort_order' => $searchParam['sort_order'],
        ));
        return $viewModel;
    }

    /**
     * This Action is used to get the membership Id.
     * @param $userId, $param
     * @return this will be membership configration
     * @author Icreon Tech -SR
     */
    function getMembershipId($userId, $param) {
        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $userId));
        $membershipDetailArr = array();

        $membershipConfig = array();
        $membershipDetailArr = array();
        $savedSearchInDays = '';
        $savedSearchTypeIds = '';
        if ($userDetail['membership_id'] != '') {
            $membershipDetailArr[] = $userDetail['membership_id'];
            $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
            if ($param == 'annotation') {
                $maxSearchCountPS['is_annotation'] = $getMembershipDetail['is_annotation'];
            } else if ($param == 'correction') {
                $maxSearchCountPS['is_correction'] = $getMembershipDetail['is_correction'];
            } else {
                $savedSearchInDays = explode(',', $getMembershipDetail['savedSearchInDays']);
                $savedSearchTypeIds = explode(',', $getMembershipDetail['savedSearchTypeIds']);
                $maxSearchInDays = explode(',', $getMembershipDetail['maxSavedSearchInDays']);

                $searchSaveType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSearchSaveTypes();
                foreach ($searchSaveType as $index => $data) {
                    $searchSaveTypes[$data['saved_search_type']] = $data['saved_search_type_id'];
                }
                @$saveSearchTypePS = $searchSaveTypes[$param];
                foreach ($savedSearchTypeIds as $key => $val) {
                    if ($val == $saveSearchTypePS) {
                        @$maxSearchCountPS = ($maxSearchInDays[$key] > $savedSearchInDays[$key]) ? $maxSearchInDays[$key] : $savedSearchInDays[$key];
                    }
                    $membershipConfig[$val] = $savedSearchInDays[$key];
                }
            }
        }
        return @$maxSearchCountPS;
    }

    /**
     * This Action is used to get the passenger annotation listing
     * @return this will array
     * @author Icreon Tech -SR
     */
    public function getPassengerAnnotationListingAction() {
        $this->checkFrontUserAuthentication();
        //  $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $this->getPassengerTable();
        $passengerId = $this->decrypt($params['id']);
        $searchParam['passenger_id'] = $passengerId;
        $searchResult = $this->getPassengerTable()->getPassengerRecord($searchParam);
// bbbbbbbbbbbbbbbbbbb
        $searchParam['status'] = '1'; // completed
        $searchResult['activity_type'] = 1; // added on 21 nov
        $annotationResult = $this->getPassengerTable()->getPassengeAnnotationCorrection($searchParam);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'annotationResult' => $annotationResult,
            'jsLangTranslate' => array_merge($this->_config['PassengerMessages']['config']['passenger_search'], $this->_config['PassengerMessages']['config']['passenger_details'])));
        return $viewModel;
    }

    /**
     * This Action is used to send the contact email for annotation
     * @return this will be confirmation of email
     * @author Icreon Tech -SR
     */
    public function emailAnnotationContactAction() {
        $this->getPassengerTable();
        $request = $this->getRequest();
        $flag = 0;
        $emailArray = array();
        $userDataArr = $this->getServiceLocator()->get('User\Model\UserTable')->getUser(array('user_id' => $this->auth->getIdentity()->user_id));
        $emailArray['admin_email'] = $userDataArr['email_id'];
        $emailArray['admin_name'] = $userDataArr['first_name'];
        $emailArray['first_name'] = stripslashes($request->getPost('to_first_name'));
        $emailArray['passenger_name'] = stripslashes($request->getPost('passenger_name'));
        $emailArray['last_name'] = '';
        $emailArray['sender_first_name'] = $userDataArr['first_name'];
        $emailArray['sender_email'] = $userDataArr['email_id'];
        $emailArray['email_id'] = $request->getPost('to_email_id');
        $template_id = 28;
        $response = $this->getResponse();
        $flag = $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($emailArray, $template_id, $this->_config['email_options']);
        $messages = array('status' => "success", 'message' => 'Email is sent successfully');
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This Action is used to get the related products
     * @return this will e an array
     * @author Icreon Tech -SR
     */
    public function getRelatedProductsAction() {
        $this->getPassengerTable();
        $msgArray = array_merge($this->_config['PassengerMessages']['config']['product'], $this->_config['PassengerMessages']['config']['common']);
        $request = $this->getRequest();
        $discount = 0;
        /*
        if (isset($this->auth->getIdentity()->user_id)) {
            $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserMemberShipDetail(array('user_id' => $this->auth->getIdentity()->user_id));
            if (count($userDetail) > 0) {
                if ($userDetail['membership_id'] != '1') {
                    $discount = $userDetail['membership_discount'];
                }
            }
        }
        */
        if (isset($this->auth->getIdentity()->user_id)) {
            if (defined('MEMBERSHIP_DISCOUNT')) {
			$discount = MEMBERSHIP_DISCOUNT;
		}
        }
        
        /*set minimum amount for membership discount starts*/
        $minimumMemberShipDiscount = 0;
        $memberShipDiscount = $this->getServiceLocator()->get('User\Model\ContactTable')->getMinimumDiscountAmount();
        if(isset($memberShipDiscount['discount']))
        {
          $minimumMemberShipDiscount = $memberShipDiscount['discount'];
        }

        /*set minimum amount for membership discount ends*/
        $params = $this->params()->fromRoute();
        $productTypeId = $params['product_type_id'];
        $productParam['product_type'] = $productTypeId;
        $productResult = $this->getServiceLocator()->get('Product\Model\ProductTable')->getCrmProducts($productParam);

        $searchParams = array();
        $searchParams['id'] = $productResult[0]['product_id'];
        $searchParams['in_stock_flag'] = "1";
        $relatedProduct = $this->getServiceLocator()->get('Product\Model\ProductTable')->getRelatedProduct($searchParams);

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'relatedProduct' => $relatedProduct,
            'file_upload_path_assets_url' => $this->_config['file_upload_path']['assets_url'] . 'product/thumbnail/',
            'file_upload_path_assets_upload_dir' => $this->_config['file_upload_path']['assets_upload_dir'] . 'product/thumbnail/',
            'file_upload_path_assets_url_main' => $this->_config['file_upload_path']['assets_url'] . 'product/',
            'file_upload_path_assets_upload_dir_main' => $this->_config['file_upload_path']['assets_upload_dir'] . 'product/',
            'discounts' => $discount,
            'minimumMemberShipDiscount' => $minimumMemberShipDiscount,
            'productTypeId' => $productTypeId,
            'product' => (isset($params['product']) and trim($params['product']) != "" and trim($params['product']) != "null" and trim($params['product']) != "0") ? $params['product'] : ""
        ));
        return $viewModel;
    }

    public function viewPassengerAction() {
        $params = $this->params()->fromRoute();
        header('Location: /passenger-details/' . $this->encrypt($params['id']) . '/' . $this->encrypt($params['target']));

        // http://local.soleifhot/view-passenger/604923010232/passenger
        // http://local.soleifhot/view-passenger/604923010232/ship
        // http://local.soleifhot/view-passenger/604923010232/manifest
        // http://local.soleifhot/view-passenger/604923010232/annotation
        // http://local.soleifhot/view-passenger/604923010232/textpassenger
        die;
    }

    public function fullPassengerSearchAction() {
        // $this->checkFrontUserAuthentication();
        $sessionPassSearch = new SessionContainer('passengerSearch');
        // print_r($sessionPassSearch->passenger_search);
        $searchParam = $sessionPassSearch->passenger_search;

        $form = new PassengerForm();
        $this->getPassengerTable();
        $params = $this->params()->fromRoute();
        $searchId = '';
        if (isset($params['searchId']) && !empty($params['searchId'])) {
            $searchId = $params['searchId'];
        }

        if (isset($searchParam['search_type1']) && $searchParam['search_type1'] == 1) {
            $searchTypeArray[] = 'search_type1';
        }
        if (isset($searchParam['search_type2']) && $searchParam['search_type2'] == 1) {
            $searchTypeArray[] = 'search_type2';
        }
        if (isset($searchParam['search_type3']) && $searchParam['search_type3'] == 1) {
            $searchTypeArray[] = 'search_type3';
        }
        if (isset($searchParam['search_type4']) && $searchParam['search_type4'] == 1) {
            $searchTypeArray[] = 'search_type4';
        }
        if (isset($searchParam['search_type5']) && $searchParam['search_type5'] == 1) {
            $searchTypeArray[] = 'search_type5';
        }
        if (isset($searchParam['search_type6']) && $searchParam['search_type6'] == 1) {
            $searchTypeArray[] = 'search_type6';
        }


        $form->get('last_name')->setAttribute('value', isset($searchParam['last_name']) ? $searchParam['last_name'] : '');
        $form->get('birth_year_from')->setAttribute('value', isset($searchParam['birth_year_from']) ? $searchParam['birth_year_from'] : '');

        $form->get('birth_year_to')->setAttribute('value', isset($searchParam['birth_year_to']) ? $searchParam['birth_year_to'] : '');
        $form->get('current_age_from')->setAttribute('value', isset($searchParam['current_age_from']) ? $searchParam['current_age_from'] : '');
        $form->get('current_age_to')->setAttribute('value', isset($searchParam['current_age_to']) ? $searchParam['current_age_to'] : '');
        $form->get('arrival_age_from')->setAttribute('value', isset($searchParam['arrival_age_from']) ? $searchParam['arrival_age_from'] : '');
        $form->get('arrival_age_to')->setAttribute('value', isset($searchParam['arrival_age_to']) ? $searchParam['arrival_age_to'] : '');
        $form->get('year_of_arrival_from')->setAttribute('value', isset($searchParam['year_of_arrival_from']) ? $searchParam['year_of_arrival_from'] : '');
        $form->get('year_of_arrival_to')->setAttribute('value', isset($searchParam['year_of_arrival_to']) ? $searchParam['year_of_arrival_to'] : '');
        $form->get('month_of_arrival_from')->setAttribute('value', isset($searchParam['month_of_arrival_from']) ? $searchParam['month_of_arrival_from'] : '');
        $form->get('month_of_arrival_to')->setAttribute('value', isset($searchParam['month_of_arrival_to']) ? $searchParam['month_of_arrival_to'] : '');
        $form->get('day_of_arrival_from')->setAttribute('value', isset($searchParam['day_of_arrival_from']) ? $searchParam['day_of_arrival_from'] : '');
        $form->get('day_of_arrival_to')->setAttribute('value', isset($searchParam['day_of_arrival_to']) ? $searchParam['day_of_arrival_to'] : '');
        $form->get('place_of_birth')->setAttribute('value', isset($searchParam['place_of_birth']) ? $searchParam['place_of_birth'] : '');

        $gender = array();
        if (isset($searchParam['gender']))
            $gender = $searchParam['gender'];

        $maritalstatus = array();
        if (isset($searchParam['marital_status']))
            $maritalstatus = $searchParam['marital_status'];

        $form->get('initital_name')->setAttribute('value', isset($searchParam['initital_name']) ? $searchParam['initital_name'] : '');

        $form->get('town')->setAttribute('value', isset($searchParam['town']) ? trim($searchParam['town']) : '');
		$form->get('search_type_town')->setAttribute('value', isset($searchParam['search_type_town']) ? trim($searchParam['search_type_town']) : '');
        $form->get('ship_name')->setAttribute('value', isset($searchParam['ship_name']) ? trim($searchParam['ship_name']) : '');
        $form->get('port_of_departure')->setAttribute('value', isset($searchParam['port_of_departure']) ? trim($searchParam['port_of_departure']) : '');
        $form->get('arrival_port')->setAttribute('value', isset($searchParam['arrival_port']) ? trim($searchParam['arrival_port']) : '');
        $form->get('passenger_id')->setAttribute('value', isset($searchParam['passenger_id']) ? trim($searchParam['passenger_id']) : '');
        $form->get('first_name')->setAttribute('value', isset($searchParam['first_name']) ? trim($searchParam['first_name']) : '');
        $form->get('companion_name')->setAttribute('value', isset($searchParam['companion_name']) ? trim($searchParam['companion_name']) : '');
        $form->get('ethnicity_search')->setAttribute('value', isset($searchParam['ethnicity_search']) ? $searchParam['ethnicity_search'] : '');


        $ethnicity = array();
        if (isset($searchParam['ethnicity']))
            $ethnicity = $searchParam['ethnicity'];
        $form->get('ethnicity')->setAttribute('value', $ethnicity);

        $getSearchArray = array();
        $ethnicityArray = $this->getPassengerTable()->getEthnicity($getSearchArray);
        $ethnicity_list = array();
        foreach ($ethnicityArray as $key => $val) {
            if (!empty($val['ethnicity'])) {
                $eth = trim(trim($val['ethnicity'], '-'), '.');
                $ethnicity_list[$eth] = $eth;
            }
        }
        ksort($ethnicity_list);
        array_unique($ethnicity_list);
        $form->get('ethnicity')->setAttribute('options', $ethnicity_list);

        $request = $this->getRequest();
        $isPosted = false;
        if ($request->isPost()) {
            $form->get('last_name')->setAttribute('value', $request->getPost('last_name'));
            $isPosted = true;
        }

        $ethnicity = array();
        if (isset($searchParam['ethnicity']))
            $ethnicity = $searchParam['ethnicity'];
        $form->get('ethnicity')->setAttribute('value', $ethnicity);

        $getSearchArray = array();
        $portArray = $this->getPassengerTable()->getArrivalPort($getSearchArray);
        $portList = array();
        $portList[' '] = 'Arrival Port';
        /* foreach ($portArray as $key => $val) {
          $portList[$val['ship_arrival_port']] = $val['ship_arrival_port'];
          } */
        $portList['New York'] = 'New York';
        $form->get('arrival_port')->setAttribute('options', $portList);
        $getSearchArray = array();
        $saveSearchArray = array();
        $membershipConfig = array();
        $userId = '';
        if (!empty($this->auth->getIdentity()->user_id)) {
            $userId = $this->auth->getIdentity()->user_id;
            $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->auth->getIdentity()->user_id));
            $membershipDetailArr = array();
            $savedSearchInDays = '';
            $savedSearchTypeIds = '';
            if ($userDetail['membership_id'] != '') {
                $membershipDetailArr[] = $userDetail['membership_id'];
                $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
                $savedSearchInDays = explode(',', $getMembershipDetail['savedSearchInDays']);
                $savedSearchTypeIds = explode(',', $getMembershipDetail['savedSearchTypeIds']);
                foreach ($savedSearchTypeIds as $key => $val) {
                    $membershipConfig[$val] = $savedSearchInDays[$key];
                }
            }
            $getSearchArray['user_id'] = $this->auth->getIdentity()->user_id;
            $getSearchArray['isActive'] = 1;
            $getSearchArray['search_type'] = 1;/**  passenger search */
            $saveSearchArray = $this->getPassengerTable()->getPassengerSearch($getSearchArray);
        }
        // asd($gender);
        //$this->layout('web-popup');
        $viewModel = new ViewModel();
        //$viewModel->setTerminal(true);
        $viewModel->setVariables(array('form' => $form, 'searchArray' => $saveSearchArray,
            'membershipConfig' => $membershipConfig,
            'isPosted' => $isPosted,
            'loggedInUserId' => $userId,
            'searchId' => $this->decrypt($searchId),
            'maritalstatus' => $maritalstatus,
            'gender' => $gender,
            'genderArray' => $this->genderArray(),
            'searchTypeArray' => $searchTypeArray,
            'searchParam' => $searchParam,
            'searchEthnicity' => $ethnicity,
            'translator' => array_merge($this->_config['PassengerMessages']['config']['passenger_search'], $this->_config['PassengerMessages']['config']['passenger_details'], $this->_config['PassengerMessages']['config']['common'])));
        return $viewModel;
    }

    /**
     * This Action is used to show the center manifest image in large
     * @return void
     * @author Icreon Tech - SR
     */
    public function showManifestBigImageAction() {
        $this->getPassengerTable();
        $params = $this->params()->fromRoute();
        $fileName = $this->decrypt($params['filename']);
        $dataSource = $params['data_source'];
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'fileName' => $fileName,
            'dataSource' => $dataSource));
        return $viewModel;
    }
    /**
     * This Action is used clear session data of passenger search
     * @return boolean
     */
    public function clearSearchPassengerSessionAction() {
        $sessionPassSearch = new SessionContainer('passengerSearch');
        unset($sessionPassSearch->passenger_search);
        return false;
    }
    
    /**
     * This Action is used to send the passenger records to the user email id
     * @return void
     * @author Icreon Tech - SR
     */    
    public function sendPassengerRecordAction(){
        $this->getPassengerTable();
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        $emailId = trim($request->getPost('emailId'));
        $passengerID = $this->decrypt($request->getPost('passenger_id'));
      
        $pdfPath = $this->_config['file_upload_path']['assets_upload_dir'] . "temp_passenger";
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $searchParam['passenger_id'] = $passengerID;
        //$searchResult = $this->getServiceLocator()->get('Passenger\Model\DocumentTable')->searchCrmPassenger($searchParam);
        $searchResult = $this->getPassengerTable()->getPassengerRecord($searchParam);

		
        $maritalStatusArray = $this->maritalStatusArray();
        $searchResult[0]['PRIN_MARITAL_STAT'] = (isset($searchResult[0]['PRIN_MARITAL_STAT']) ? $maritalStatusArray[$searchResult[0]['PRIN_MARITAL_STAT']] : "Unknown");
        $genderArray = $this->genderArray();
        $searchResult[0]['PRIN_GENDER_CODE'] = (isset($searchResult[0]['PRIN_GENDER_CODE']) ? $genderArray[$searchResult[0]['PRIN_GENDER_CODE']] : "Unknown");
       // $searchResult = $searchResult[0];
		$messages['LAST_RESIDENCE'] = (($searchResult[0]['data_source'] == '1') ? $messages['LAST_RESIDENCE'] : $messages['PLACE_OF_BIRTH']);
        $viewModel = new ViewModel();
		$prin_place = (($searchResult[0]['data_source'] == '1') ? $searchResult[0]['PRIN_PLACE_RESI'] : $searchResult[0]['PLACE_OF_BIRTH']);

        $dateArray = date_parse_from_format("Y-m-d", $searchResult[0]['DATE_ARRIVE']);
        if ($searchResult[0]['data_source'] == 2 && ($dateArray['month'] == '1' or $dateArray['month'] == '01') && ($dateArray['day'] == 1 or $dateArray['day'] == '01')) {
            $dateArr = $dateArray['year'];
        } else {
            if ($searchResult[0]['DATE_ARRIVE'] != '' && $searchResult[0]['DATE_ARRIVE'] != '0000-00-00 00:00:00') {
                $dateArr = $this->OutputDateFormat($searchResult[0]['DATE_ARRIVE'], 'displaymonthformat2');
            }
        }
		//  $pdf->AddPage();
	
		$kioskParam = array();	
		$kioskParam['kiosk_id'] = $this->auth->getIdentity()->kiosk_id;
		$kioskDetails = $this->getServiceLocator()->get('Kiosk\Model\KioskManagementTable')->getkioskDetails($kioskParam);	

		$str='<page><table style="width:100%; padding-top:262px" align="center" cellspacing="5"><tr><td style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left;font-size:16px;" width="2%">First Name: </td><td style="font-size:16px;" width="5%">'.ucfirst($this->auth->getIdentity()->first_name).'</td></tr>
		<tr><td style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left;font-size:16px;" width="2%">Last Name: </td><td style="font-size:16px;" width="5%">'.ucfirst($this->auth->getIdentity()->last_name).'</td></tr>
		<tr><td style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left;font-size:16px;" width="4%">Station ID: </td><td style="text-align:left;font-size:16px;" width="5%">'.$kioskDetails[0]['kiosk_name'].'</td>';
		
		$str.='</tr></table></page>';
        $str.= '
        <page><table style="width:100%; padding-top:262px" align="center" cellspacing="5">';
        if (!empty($searchResult[0]['PRIN_FIRST_NAME'])) {
            $str.='<tr>
            <td style="font-size:16px;"> ' . $messages['FIRST_NAME'] . ' : </td>
            <td style="font-size:16px;"> ' . $searchResult[0]['PRIN_FIRST_NAME'] . ' </td>
        </tr>';
        }
        $str.='<tr>
            <td style="font-size:16px;"> ' . $messages['LAST_NAME'] . ' : </td>
            <td style="font-size:16px;"> ' . $searchResult[0]['PRIN_LAST_NAME'] . ' </td>
        </tr>';
        if (!empty($searchResult[0]['PRIN_NATIONALITY'])) {
            $str.='<tr>
            <td style="font-size:16px;"> ' . $messages['NATIONALITY'] . ' : </td>
            <td style="font-size:16px;"> ' . $searchResult[0]['PRIN_NATIONALITY'] . ' </td>
        </tr>';
        }

        if (!empty($prin_place)) {
            $str.='<tr>
            <td style="font-size:16px;"> ' . $messages['LAST_RESIDENCE'] . ' : </td>
            <td style="font-size:16px;"> ' . $prin_place . ' </td>
        </tr>';
        }

        if (!empty($searchResult[0]['DATE_ARRIVE'])) {
            $str.='<tr>
            <td style="font-size:16px;"> ' . $messages['DATE_ARRIVAL'] . ' : </td>
            <td style="font-size:16px;"> ' . $dateArr . ' </td>
        </tr>';
        }
        if (!empty($searchResult[0]['PRIN_AGE_ARRIVAL'])) {
            $str.='<tr>
            <td style="font-size:16px;"> ' . $messages['AGE_ARRIVAL'] . ' : </td>
            <td style="font-size:16px;"> ' . $searchResult[0]['PRIN_AGE_ARRIVAL'] . ' </td>
        </tr>';
        }

        $str.='<tr>
            <td style="font-size:16px;"> ' . $messages['GENDER'] . ' : </td>
            <td style="font-size:16px;"> ' . $searchResult[0]['PRIN_GENDER_CODE'] . ' </td>
        </tr>';

        $str.='<tr>
            <td style="font-size:16px;"> ' . $messages['MARITAL_STATUS'] . ' : </td>
            <td style="font-size:16px;"> ' . $searchResult[0]['PRIN_MARITAL_STAT'] . ' </td>
        </tr>';
        if (!empty($searchResult[0]['SHIP_NAME'])) {
            $str.='<tr>
                <td style="font-size:16px;"> ' . $messages['SHIP_TRAVEL'] . ' : </td>
                <td style="font-size:16px;"> ' . $searchResult[0]['SHIP_NAME'] . ' </td>
            </tr>';
        }
        if (!empty($searchResult[0]['DEP_PORT_NAME'])) {
            $str.='<tr>
                    <td style="font-size:16px;"> ' . $messages['PORT_DEPARTURE'] . ' : </td>
                    <td style="font-size:16px;"> ' . $searchResult[0]['DEP_PORT_NAME'] . ' </td>
            </tr>';
        }
        if (!empty($searchResult[0]['REF_PAGE_LINE_NBR'])) {
            $str.='<tr>
            <td style="font-size:16px;"> ' . $messages['MANIFEST_LINE_NUMBER'] . ' : </td>
            <td style="font-size:16px;"> ' . $searchResult[0]['REF_PAGE_LINE_NBR'] . ' </td>
        </tr>';
        }
        $str.='</table>
    </page>';

        $html2pdf = new\ HTML2PDF('L', 'A4', 'en');
        $html2pdf->setDefaultFont('Times');
        $html2pdf->writeHTML($str, false);
        $pdfFileName = time() . "_" . $request->getPost('passenger_id');
        $html2pdf->Output($pdfPath . "/" . $pdfFileName.".pdf", F);

        $userDataArr['pdf_path'] = $pdfPath . "/" . $pdfFileName . ".pdf";
        $signup_email_id_template_int = 40;
        $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
        $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];       
        $userDataArr['email_id'] = $emailId;
        if($request->getPost('mailTo') == 'admin') {
           $userDataArr['first_name'] = 'Admin';
        }
        else {
                $userDataArr['first_name'] = $this->auth->getIdentity()->first_name;
                $userDataArr['last_name'] = $this->auth->getIdentity()->last_name;
        }
       
        $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $signup_email_id_template_int, $this->_config['email_options']);
        $response = $this->getResponse();
        $messages = array('status' => "success", 'message' => 'Email sent successfully');
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;                
    }
    
    /**
     * This Action is used to send the passenger records to the user email id
     * @return void
     * @author Icreon Tech - SR
     */    
    
    public function sendShipRecordAction(){
      $this->getPassengerTable();
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        $emailId = trim($request->getPost('emailId'));
        $assetValue = $this->decrypt($request->getPost('ship_assetvalue'));
      
        $pdfPath = $this->_config['file_upload_path']['assets_upload_dir'] . "temp_passenger";
        
        $shipImageUrl = $this->_config['Image_path']['shipPath'].$assetValue;
//  $pdf->AddPage();

        $str = '<page>
                    <table style="width:100%; padding-top:266px" align="center" cellspacing="5">
                        <tr>
                            <td style="font-size:14px;"> <img src=' . $shipImageUrl . ' border="0"> : </td>
                        </tr>

                    </table>
            </page>';

        $html2pdf = new\ HTML2PDF('L', 'A4', 'en');
        $html2pdf->setDefaultFont('Times');
        $html2pdf->writeHTML($str, false);
        $pdfFileName = time() . "_" . $request->getPost('ship_assetvalue');
        $html2pdf->Output($pdfPath . "/" . $pdfFileName.".pdf", F);

        $userDataArr['pdf_path'] = $pdfPath . "/" . $pdfFileName . ".pdf";
        $signup_email_id_template_int = 40;
        $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
        $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];       
        $userDataArr['email_id'] = $emailId;
        $userDataArr['first_name'] = $this->auth->getIdentity()->first_name;
        $userDataArr['last_name'] = $this->auth->getIdentity()->last_name;
        $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $signup_email_id_template_int, $this->_config['email_options']);
        $response = $this->getResponse();
        $messages = array('status' => "success", 'message' => 'Email is send successfully.');
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;  
    }

	/**
     * This Action is used to send the passenger records to the user email id (Admin)
     * @return void
     * @author Icreon Tech - SK
     */    
    public function printCertificateSendAction(){ 
        $this->getPassengerTable();
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        $emailId = trim($request->getPost('emailId'));
        $passengerID = $this->decrypt($request->getPost('passenger_id'));
      
        $pdfPath = $this->_config['file_upload_path']['assets_upload_dir'] . "temp_passenger";
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $searchParam['passenger_id'] = $passengerID;
        //$searchResult = $this->getServiceLocator()->get('Passenger\Model\DocumentTable')->searchCrmPassenger($searchParam);
        $searchResult = $this->getPassengerTable()->getPassengerRecord($searchParam);
        
        $maritalStatusArray = $this->maritalStatusArray();
        $searchResult[0]['PRIN_MARITAL_STAT'] = (isset($searchResult[0]['PRIN_MARITAL_STAT']) ? $maritalStatusArray[$searchResult[0]['PRIN_MARITAL_STAT']] : "Unknown");
        $genderArray = $this->genderArray();
        $searchResult[0]['PRIN_GENDER_CODE'] = (isset($searchResult[0]['PRIN_GENDER_CODE']) ? $genderArray[$searchResult[0]['PRIN_GENDER_CODE']] : "Unknown");
        //$searchResult = $searchResult[0];
		$messages['LAST_RESIDENCE'] = (($searchResult[0]['data_source'] == '1') ? $messages['LAST_RESIDENCE'] : $messages['PLACE_OF_BIRTH']);
        $viewModel = new ViewModel();
		$prin_place = (($searchResult[0]['data_source'] == '1') ? $searchResult[0]['PRIN_PLACE_RESI'] : $searchResult[0]['PLACE_OF_BIRTH']);
        $dateArray = date_parse_from_format("Y-m-d", $searchResult[0]['DATE_ARRIVE']);
        if ($searchResult[0]['data_source'] == 2 && ($dateArray['month'] == '1' or $dateArray['month'] == '01') && ($dateArray['day'] == 1 or $dateArray['day'] == '01')) {
            $dateArr = $dateArray['year'];
        } else {
            if ($searchResult[0]['DATE_ARRIVE'] != '' && $searchResult[0]['DATE_ARRIVE'] != '0000-00-00 00:00:00') {
                $dateArr = $this->OutputDateFormat($searchResult[0]['DATE_ARRIVE'], 'displaymonthformat2');
            }
        }
//  $pdf->AddPage();

        $str = '<page>
        <table style="width:100%; padding-top:266px" align="center" cellspacing="5">';
        if (!empty($searchResult[0]['PRIN_FIRST_NAME'])) {
            $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['FIRST_NAME'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['PRIN_FIRST_NAME'] . ' </td>
        </tr>';
        }
        $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['LAST_NAME'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['PRIN_LAST_NAME'] . ' </td>
        </tr>';
        if (!empty($searchResult[0]['PRIN_NATIONALITY'])) {
            $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['NATIONALITY'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['PRIN_NATIONALITY'] . ' </td>
        </tr>';
        }

        if (!empty($prin_place)) {
            $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['LAST_RESIDENCE'] . ' : </td>
            <td style="font-size:14px;"> ' . $prin_place . ' </td>
        </tr>';
        }

        if (!empty($searchResult[0]['DATE_ARRIVE'])) {
            $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['DATE_ARRIVAL'] . ' : </td>
            <td style="font-size:14px;"> ' . $dateArr . ' </td>
        </tr>';
        }
        if (!empty($searchResult[0]['PRIN_AGE_ARRIVAL'])) {
            $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['AGE_ARRIVAL'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['PRIN_AGE_ARRIVAL'] . ' </td>
        </tr>';
        }

        $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['GENDER'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['PRIN_GENDER_CODE'] . ' </td>
        </tr>';

        $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['MARITAL_STATUS'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['PRIN_MARITAL_STAT'] . ' </td>
        </tr>';
        if (!empty($searchResult[0]['SHIP_NAME'])) {
            $str.='<tr>
                <td style="font-size:14px;"> ' . $messages['SHIP_TRAVEL'] . ' : </td>
                <td style="font-size:14px;"> ' . $searchResult[0]['SHIP_NAME'] . ' </td>
            </tr>';
        }
        if (!empty($searchResult[0]['DEP_PORT_NAME'])) {
            $str.='<tr>
                    <td style="font-size:14px;"> ' . $messages['PORT_DEPARTURE'] . ' : </td>
                    <td style="font-size:14px;"> ' . $searchResult[0]['DEP_PORT_NAME'] . ' </td>
            </tr>';
        }
        if (!empty($searchResult[0]['REF_PAGE_LINE_NBR'])) {
            $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['MANIFEST_LINE_NUMBER'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['REF_PAGE_LINE_NBR'] . ' </td>
        </tr>';
        }
        $str.='</table>
    </page>';

        $html2pdf = new\ HTML2PDF('L', 'A4', 'en');
        $html2pdf->setDefaultFont('Times');
        $html2pdf->writeHTML($str, false);
        $pdfFileName = time() . "_" . $request->getPost('passenger_id');
        $html2pdf->Output($pdfPath . "/" . $pdfFileName.".pdf", F);

       $userDataArr['pdf_path'] = $pdfPath . "/" . $pdfFileName . ".pdf";
       $signup_email_id_template_int = 43;
       $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
       $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];       
       $userDataArr['email_id'] = $emailId;
       if($request->getPost('mailTo') == 'admin') {
           $userDataArr['first_name'] = 'Admin';
       }
       else {
        $userDataArr['first_name'] = $this->auth->getIdentity()->first_name;
        $userDataArr['last_name'] = $this->auth->getIdentity()->last_name;
       }
       
        $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $signup_email_id_template_int, $this->_config['email_options']);
        $response = $this->getResponse();
        $messages = array('status' => "success", 'message' => 'Email sent successfully');
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;                
    }

	public function getAnnotationLogAction(){
		$this->getPassengerTable();
        $params = $this->params()->fromRoute();
		$passengerId = $this->decrypt($params['passengerId']);
        $searchParam['passenger_id'] = $passengerId;
              
        $searchParam['status'] = '1'; // completed
        $searchParam['activity_type'] = 1; // added on 21 nov 2014
        $annotationResult = $this->getPassengerTable()->getPassengeAnnotationCorrection($searchParam);
		
		$postData = array();
        $postData[0] = '';
        $postData[1] = '';
        $postData[3] = 'asc';
        $postData[2] = 'tbl_membership.minimun_donation_amount';
        $postData['currentYear'] = date("Y", strtotime(DATE_TIME_FORMAT));
        $postData['isActive'] = "1";
        $membershipData = $this->getServiceLocator()->get('Kiosk\Model\PosTable')->getMasterMembership($postData);		
		$i = 0;
        foreach ($membershipData as $key) {
            if ($key['membership_id'] != '1' && $key['minimun_donation_amount'] > 0 && $key['is_ellis_default']!='1') {
                if ($i == 0) {
                    $min_donation = $key['minimun_donation_amount'];
                    $min_donation_title = $key['membership_title'];
                } else {
                    if ($min_donation > $key['minimun_donation_amount']) {
                        $min_donation = $key['minimun_donation_amount'];
                        $min_donation_title = $key['membership_title'];
                    }
                }
                $i++;
            }
        }		

		
		$viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'annotationResult' => $annotationResult,
            'min_donation_title'=>$min_donation_title,
            'min_donation'=>$min_donation,
            'jsLangTranslate' => array_merge($this->_config['PassengerMessages']['config']['passenger_search'], $this->_config['PassengerMessages']['config']['passenger_details'])));
			return $viewModel;
	}
	//
	public function getPassengerRecordDetailsAction(){
            $request = $this->getRequest();
            $searchParam['passenger_id'] = $request->getPost('passengerID');
	        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
            $searchResult = array();
	            
            //$searchResult = $this->getPassengerTable()->getPassengerRecord($searchParam);
           // $searchResult = $this->getServiceLocator()->get('Passenger\Model\DocumentTable')->searchCrmPassenger($searchParam);            
            $searchResult = $this->getPassengerTable()->getPassengerRecord($searchParam);
            //$searchResult = $searchResult[0];
            
	        $maritalStatusArray = $this->maritalStatusArray();
	        $searchResult[0]['PRIN_MARITAL_STAT'] = (isset($searchResult[0]['PRIN_MARITAL_STAT']) ? $maritalStatusArray[$searchResult[0]['PRIN_MARITAL_STAT']] : "Unknown");
	        $genderArray = $this->genderArray();
	        $searchResult[0]['PRIN_GENDER_CODE'] = (isset($searchResult[0]['PRIN_GENDER_CODE']) ? $genderArray[$searchResult[0]['PRIN_GENDER_CODE']] : "Unknown");
	        $messages['LAST_RESIDENCE'] = (($searchResult[0]['data_source'] == '1') ? $messages['LAST_RESIDENCE'] : $messages['PLACE_OF_BIRTH']);
	        $prin_place = (($searchResult[0]['data_source'] == '1') ? $searchResult[0]['PRIN_PLACE_RESI'] : $searchResult[0]['PLACE_OF_BIRTH']);
		        $dateArray = date_parse_from_format("Y-m-d", $searchResult[0]['DATE_ARRIVE']);
		        if ($searchResult[0]['data_source'] == 2 && ($dateArray['month'] == '1' or $dateArray['month'] == '01') && ($dateArray['day'] == 1 or $dateArray['day'] == '01')) {
		            $dateArr = $dateArray['year'];
		        } else {
		            if ($searchResult[0]['DATE_ARRIVE'] != '' && $searchResult[0]['DATE_ARRIVE'] != '0000-00-00 00:00:00') {
		                $dateArr = $this->OutputDateFormat($searchResult[0]['DATE_ARRIVE'], 'displaymonthformat2');
		            }
		        }         
            
			       $str = '<page>
			    <table style="width:60%; padding-top:262px" align="center" cellspacing="5">';
			        if (!empty($searchResult[0]['PRIN_FIRST_NAME'])) {
			            $str.='<tr>
			            <td style="font-size:16px;"> ' . $messages['FIRST_NAME'] . ' : </td>
			            <td style="font-size:16px;"> ' . $searchResult[0]['PRIN_FIRST_NAME'] . ' </td>
			        </tr>';
			        }
			        $str.='<tr>
			            <td style="font-size:16px;"> ' . $messages['LAST_NAME'] . ' : </td>
			            <td style="font-size:16px;"> ' . $searchResult[0]['PRIN_LAST_NAME'] . ' </td>
			        </tr>';
			        if (!empty($searchResult[0]['PRIN_NATIONALITY'])) {
			            $str.='<tr>
			            <td style="font-size:16px;"> ' . $messages['NATIONALITY'] . ' : </td>
			            <td style="font-size:16px;"> ' . $searchResult[0]['PRIN_NATIONALITY'] . ' </td>
			        </tr>';
			        }
			
			        if (!empty($prin_place)) {
			            $str.='<tr>
			            <td style="font-size:16px;"> ' . $messages['LAST_RESIDENCE'] . ' : </td>
			            <td style="font-size:16px;"> ' . $prin_place . ' </td>
			        </tr>';
			        }
			
			        if (!empty($searchResult[0]['DATE_ARRIVE'])) {
			            $str.='<tr>
			            <td style="font-size:16px;"> ' . $messages['DATE_ARRIVAL'] . ' : </td>
			            <td style="font-size:16px;"> ' . $dateArr . ' </td>
			        </tr>';
			        }
			        if (!empty($searchResult[0]['PRIN_AGE_ARRIVAL'])) {
			            $str.='<tr>
			            <td style="font-size:16px;"> ' . $messages['AGE_ARRIVAL'] . ' : </td>
			            <td style="font-size:16px;"> ' . $searchResult[0]['PRIN_AGE_ARRIVAL'] . ' </td>
			        </tr>';
			        }
			
			        $str.='<tr>
			            <td style="font-size:16px;"> ' . $messages['GENDER'] . ' : </td>
			            <td style="font-size:16px;"> ' . $searchResult[0]['PRIN_GENDER_CODE'] . ' </td>
			        </tr>';
			
			        $str.='<tr>
			            <td style="font-size:16px;"> ' . $messages['MARITAL_STATUS'] . ' : </td>
			            <td style="font-size:16px;"> ' . $searchResult[0]['PRIN_MARITAL_STAT'] . ' </td>
			        </tr>';
			        if (!empty($searchResult[0]['SHIP_NAME'])) {
			            $str.='<tr>
			                <td style="font-size:16px;"> ' . $messages['SHIP_TRAVEL'] . ' : </td>
			                <td style="font-size:16px;"> ' . $searchResult[0]['SHIP_NAME'] . ' </td>
			            </tr>';
			        }
			        if (!empty($searchResult[0]['DEP_PORT_NAME'])) {
			            $str.='<tr>
			                    <td style="font-size:16px;"> ' . $messages['PORT_DEPARTURE'] . ' : </td>
			                    <td style="font-size:16px;"> ' . $searchResult[0]['DEP_PORT_NAME'] . ' </td>
			            </tr>';
			        }
			        if (!empty($searchResult[0]['REF_PAGE_LINE_NBR'])) {
			            $str.='<tr>
			            <td style="font-size:16px;"> ' . $messages['MANIFEST_LINE_NUMBER'] . ' : </td>
			            <td style="font-size:16px;"> ' . $searchResult[0]['REF_PAGE_LINE_NBR'] . ' </td>
			        </tr>';
			        }
			      echo  $str.='</table>
			    </page>';
	
       die;
	}

    /**
     * This Action is used to send the passenger records to the user email id
     * @return void
     * @author Icreon Tech - SR
     */    
    public function samplePassengerRecordAction(){
        $this->getPassengerTable();
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();

        $passengerID = $this->decrypt($params['passenger_id']);
      
        //$pdfPath = $this->_config['file_upload_path']['assets_upload_dir'] . "temp_passenger";
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $searchParam['passenger_id'] = $passengerID;
        //$searchResult = $this->getServiceLocator()->get('Passenger\Model\DocumentTable')->searchCrmPassenger($searchParam);
        $searchResult = $this->getPassengerTable()->getPassengerRecord($searchParam);

		
        $maritalStatusArray = $this->maritalStatusArray();
        $searchResult[0]['PRIN_MARITAL_STAT'] = (isset($searchResult[0]['PRIN_MARITAL_STAT']) ? $maritalStatusArray[$searchResult[0]['PRIN_MARITAL_STAT']] : "Unknown");
        $genderArray = $this->genderArray();
        $searchResult[0]['PRIN_GENDER_CODE'] = (isset($searchResult[0]['PRIN_GENDER_CODE']) ? $genderArray[$searchResult[0]['PRIN_GENDER_CODE']] : "Unknown");
       // $searchResult = $searchResult[0];
		$messages['LAST_RESIDENCE'] = (($searchResult[0]['data_source'] == '1') ? $messages['LAST_RESIDENCE'] : $messages['PLACE_OF_BIRTH']);
        $viewModel = new ViewModel();
		$prin_place = (($searchResult[0]['data_source'] == '1') ? $searchResult[0]['PRIN_PLACE_RESI'] : $searchResult[0]['PLACE_OF_BIRTH']);

        $dateArray = date_parse_from_format("Y-m-d", $searchResult[0]['DATE_ARRIVE']);
        if ($searchResult[0]['data_source'] == 2 && ($dateArray['month'] == '1' or $dateArray['month'] == '01') && ($dateArray['day'] == 1 or $dateArray['day'] == '01')) {
            $dateArr = $dateArray['year'];
        } else {
            if ($searchResult[0]['DATE_ARRIVE'] != '' && $searchResult[0]['DATE_ARRIVE'] != '0000-00-00 00:00:00') {
                $dateArr = $this->OutputDateFormat($searchResult[0]['DATE_ARRIVE'], 'displaymonthformat2');
            }
        }
		//  $pdf->AddPage();
        $kioskParam = array();	
        $kioskParam['kiosk_id'] = $this->auth->getIdentity()->kiosk_id;
        $kioskDetails = $this->getServiceLocator()->get('Kiosk\Model\KioskManagementTable')->getkioskDetails($kioskParam);	

        $str= '
        <page><table style="width:70%; padding-top:200px" align="center" cellspacing="5">';
        if (!empty($searchResult[0]['PRIN_FIRST_NAME'])) {
            $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['FIRST_NAME'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['PRIN_FIRST_NAME'] . ' </td>
        </tr>';
        }
        $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['LAST_NAME'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['PRIN_LAST_NAME'] . ' </td>
        </tr>';
        if (!empty($searchResult[0]['PRIN_NATIONALITY'])) {
            $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['NATIONALITY'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['PRIN_NATIONALITY'] . ' </td>
        </tr>';
        }

        if (!empty($prin_place)) {
            $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['LAST_RESIDENCE'] . ' : </td>
            <td style="font-size:14px;"> ' . $prin_place . ' </td>
        </tr>';
        }

        if (!empty($searchResult[0]['DATE_ARRIVE'])) {
            $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['DATE_ARRIVAL'] . ' : </td>
            <td style="font-size:14px;"> ' . $dateArr . ' </td>
        </tr>';
        }
        if (!empty($searchResult[0]['PRIN_AGE_ARRIVAL'])) {
            $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['AGE_ARRIVAL'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['PRIN_AGE_ARRIVAL'] . ' </td>
        </tr>';
        }

        $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['GENDER'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['PRIN_GENDER_CODE'] . ' </td>
        </tr>';

        $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['MARITAL_STATUS'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['PRIN_MARITAL_STAT'] . ' </td>
        </tr>';
        if (!empty($searchResult[0]['SHIP_NAME'])) {
            $str.='<tr>
                <td style="font-size:14px;"> ' . $messages['SHIP_TRAVEL'] . ' : </td>
                <td style="font-size:14px;"> ' . $searchResult[0]['SHIP_NAME'] . ' </td>
            </tr>';
        }
        if (!empty($searchResult[0]['DEP_PORT_NAME'])) {
            $str.='<tr>
                    <td style="font-size:14px;"> ' . $messages['PORT_DEPARTURE'] . ' : </td>
                    <td style="font-size:14px;"> ' . $searchResult[0]['DEP_PORT_NAME'] . ' </td>
            </tr>';
        }
        if (!empty($searchResult[0]['REF_PAGE_LINE_NBR'])) {
            $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['MANIFEST_LINE_NUMBER'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['REF_PAGE_LINE_NBR'] . ' </td>
        </tr>';
        }
        echo $str.='</table>
    </page>';

        die;
    }
}