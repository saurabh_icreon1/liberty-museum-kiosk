<?php

/**
 * This is used for Document module in CRM.
 * @package    Document
 * @author     Icreon Tech -SR.
 */

namespace Passenger\Controller;

use Base\Controller\BaseController;
use Zend\Authentication\AuthenticationService;
use Zend\View\Model\ViewModel;
use Zend\Http\Request;
use Passenger\Model\Oralhistory;
use Passenger\Form\OralHistoryForm;

/**
 * This is Class used for Passenger/Document actions.
 * @package    Document
 * @author     Icreon Tech -SR.
 */
class OralhistoryController extends BaseController {

    protected $_documentTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_authservice = null;
    public $auth = null;
    protected $_flashMessage = null;

    /**
     * This is used for dafault initialization of objects.
     * @author     Icreon Tech - SK.
     */
    public function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    /**
     * This function is used to get config variables
     * @return array 
     * @author Icreon Tech - SK
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This function is used to get table and config file object
     * @return table gateway
     */
    public function getTables() {
        if (!$this->_documentTable) {
            $sm = $this->getServiceLocator();
            $this->_documentTable = $sm->get('Passenger\Model\OralhistoryTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
        }

        return $this->_documentTable;
    }

    /**
     * This function is used know about introduction and description of Oral Histories.
     * @author Icreon Tech - SK
     */
    public function oralHistoriesAction() {
        //$this->checkFrontUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $viewModel = new ViewModel();
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        $oral_history_message = $this->_config['PassengerMessages']['config']['oral_history'];
        $viewModel->setVariables(array(
            'jsLangTranslate' => $historySearchMessages,
            'messages' => $messages
        ));
        return $viewModel;
    }

    /**
     * This function is used for oral history libraray.
     * @author Icreon Tech - SK
     */
    public function oralHistoryLibraryAction() {
        $this->layout('layout');
        $this->getConfig();
        $this->getTables();
        $form = new OralHistoryForm();
        $page = 1;
        $sort_field = $param['sort_field'] = 'names.lastname';
        $sort_order = $param['sort_order'] = 'asc';
        $numRecPerPage = 6;
        $limit = $param['record_limit'] = $numRecPerPage;
        $start_index = ($page - 1) * $numRecPerPage;
        $countryList = $this->getTables()->getCountryList();
        $countries = array();
        $countries[' '] = 'Country';
        foreach ($countryList as $key => $value) {
            $countries[$value['countryid']] = $value['countryname'];
        }
        $form->get('country')->setAttribute('options', $countries);

        $typeList = $this->getTables()->getTypeList();
        $types = array();
        $types[' '] = 'Both';
        foreach ($typeList as $key => $value) {
            $types[$value['typeid']] = $value['typename'];
        }
        $form->get('type')->setAttribute('options', $types);

        $topicList = $this->getTables()->getTopicList();
        $topics = array();
        $topics[' '] = 'Topic';
        foreach ($topicList as $key => $value) {
            $topics[$value['topicid']] = $value['topicname'];
        }
        $form->get('topic')->setAttribute('options', $topics);
        $saved_searches = array();
        $maxSearchCountPS = 0;
        
        $oralHistoryData = $this->getTables()->getOralHistories($param);
        
        $param['start_index'] = '';
        $param['record_limit'] = '';

        $all_oral_histories = $this->getTables()->getOralHistories($param);
        $total_oral_histories = 0;

        if (!empty($all_oral_histories)) {
            $toaldata  = array();
            foreach($all_oral_histories  as $k=>$v){
                $toaldata[$v['namesid']] = $v;
            }
            $total_oral_histories = count($toaldata);
        }
            
        $oralHistories = array();
        if (!empty($oralHistoryData)) {
            foreach ($oralHistoryData as $key1 => $value1) {
                $oralHistories[$value1['namesid']] = $value1;
            }
            foreach ($oralHistories as $key => $value) {
                if (!is_null($value['passenger_id']) && $value['passenger_id'] != '') {
                    $ethnicityData = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getPassengerEthnicity(array('passengerId' => $value['passenger_id']));
                    $ethnicity = array();
                    if (!empty($ethnicityData) && $ethnicityData[0] != '') {
                        foreach ($ethnicityData as $k => $v) {
                            $ethnicity[] = $v['ethnicity'];
                        }
                    }
                    if (is_array($ethnicity))
                        $oralHistories[$key]['ethnicity'] = implode(",", $ethnicity);
                    else
                        $oralHistories[$key]['ethnicity'] = $ethnicity;
                }
                else
                    $oralHistories[$key]['ethnicity'] = 'N/A';

                //$oralHistories[$value['namesid']]['countries'][] = $value['countryname']; 
            }
        }
        
        if ($this->auth->hasIdentity() == true) {
            $data['user_id'] = $this->auth->getIdentity()->user_id;
            $data['search_type'] = 4;
            $saved_searches = $this->getServiceLocator()->get('Passenger\Model\FamilyhistoryTable')->getUserSearches($data);

            $userDataArr = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->auth->getIdentity()->user_id));
            $membershipDetailArr = array();
            $membershipDetailArr[] = $userDataArr['membership_id'];
            $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);

            
            $savedSearchInDays = explode(',', $getMemberShip['savedSearchInDays']);
            $savedSearchTypeIds = explode(',', $getMemberShip['savedSearchTypeIds']);
            $maxSearchInDays = explode(',', $getMemberShip['maxSavedSearchInDays']);
            
            $searchSaveType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSearchSaveTypes();
            foreach($searchSaveType as $index=>$data){
                $searchSaveTypes[$data['saved_search_type']] = $data['saved_search_type_id'];
            }
            $saveSearchTypePS = $searchSaveTypes['Oral History']; //Oral History
            
            foreach ($savedSearchTypeIds as $key => $val) {
                if ($val == $saveSearchTypePS) {
                    $maxSearchCountPS = ($maxSearchInDays[$key]>$savedSearchInDays[$key])?$maxSearchInDays[$key]:$savedSearchInDays[$key];
                }
            }
        }
        
        if ($this->auth->hasIdentity() == true)
            $loggedInUserId = $this->auth->getIdentity()->user_id;
        else
            $loggedInUserId = '';

        $viewModel = new ViewModel();
        $params = $this->params()->fromRoute();
        $saved_search_id = (isset($params['saved_search_id']) && $params['saved_search_id'] != '' && $params['saved_search_id'] != '0') ? $this->decrypt($params['saved_search_id']) : '';
        $request = $this->getRequest();
        $oral_history_message = $this->_config['PassengerMessages']['config']['oral_history_library'];
        $viewModel->setVariables(array(
            'jsLangTranslate' => $oral_history_message,
            'oralHistories' => $oralHistories,
            'form' => $form,
            'page' => $page,
            'sort_field' => $sort_field,
            'sort_order' => $sort_order,
            'limit' => $limit,
            'start_index' => $start_index,
            'loggedInUserId' => $loggedInUserId,
            'maxSearchCountPS' => $maxSearchCountPS,
            'saved_searches' => $saved_searches,
            'saved_search_id' => $saved_search_id,
            'types' => $types,
            'total_oral_histories' => $total_oral_histories,
            'oral_history_assets' => $this->_config['file_upload_path']['oral_history_assets'],
            'oral_history_assets_upload_dir' => $this->_config['file_upload_path']['oral_history_assets_upload_dir']
        ));
        return $viewModel;
    }

    /**
     * This function is used to search oral history.
     * @author Icreon Tech - SK
     */
    public function oralHistoryListAction() {
        $this->getConfig();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $oral_history_message = $this->_config['PassengerMessages']['config']['oral_history_library'];
        $page = 1;
        $sort_field = $param['sort_field'] = 'names.lastname';
        $sort_order = $param['sort_order'] = 'asc';
        $numRecPerPage = 6;
        $limit = $param['record_limit'] = $numRecPerPage;
        $start_index = ($page - 1) * $numRecPerPage;
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $data = $request->getPost()->toArray();
            parse_str($data['searchString']);

            $param['first_name'] = $first_name;
            $param['last_name'] = $last_name;
            if (isset($type))
                $param['type_id'] = $type;
            $param['country_id'] = $country;
            $param['topic'] = $topic;
            $param['sort_field'] = $sort_field;
            $param['sort_order'] = $sort_order;

            $numRecPerPage = 6;
            $param['record_limit'] = $limit;

            $start_index = $param['start_index'] = ($page - 1) * $limit;
        
            $oralHistoryData = $this->getTables()->getOralHistories($param);
            
            $param['start_index'] = '';
            $param['record_limit'] = '';
            
            $all_oral_histories = $this->getTables()->getOralHistories($param);
            $total_oral_histories = 0;

            if (!empty($all_oral_histories)) {
                $toaldata  = array();
                foreach($all_oral_histories  as $k=>$v){
                    $toaldata[$v['namesid']] = $v;
                }
                $total_oral_histories = count($toaldata);
            }

            $oralHistories = array();
            if (!empty($oralHistoryData)) {
                foreach ($oralHistoryData as $key1 => $value1) {
                    $oralHistories[$value1['namesid']] = $value1;
                }
                foreach ($oralHistories as $key => $value) {
                    if (!is_null($value['passenger_id']) && $value['passenger_id'] != '') {
                        $ethnicityData = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getPassengerEthnicity(array('passengerId' => $value['passenger_id']));
                        $ethnicity = array();
                        if (!empty($ethnicityData) && $ethnicityData[0] != '') {
                            foreach ($ethnicityData as $k => $v) {
                                $ethnicity[] = $v['ethnicity'];
                            }
                        }
                        if (is_array($ethnicity))
                            $oralHistories[$key]['ethnicity'] = implode(",", $ethnicity);
                        else
                            $oralHistories[$key]['ethnicity'] = $ethnicity;
                    }
                    else
                        $oralHistories[$key]['ethnicity'] = 'N/A';

                    //$oralHistories[$value['namesid']]['countries'][] = $value['countryname']; 
                }
            }

            if ($this->auth->hasIdentity() == true)
                $loggedInUserId = $this->auth->getIdentity()->user_id;
            else
                $loggedInUserId = '';

            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $oral_history_message,
                'oralHistories' => $oralHistories,
                'sort_field' => $sort_field,
                'sort_order' => $sort_order,
                'page' => $page,
                'start_index' => $start_index,
                'limit' => $limit,
                'loggedInUserId' => $loggedInUserId,
				'oral_history_assets' => $this->_config['file_upload_path']['oral_history_assets'],
				'oral_history_assets_upload_dir' => $this->_config['file_upload_path']['oral_history_assets_upload_dir'],
                'total_oral_histories' => $total_oral_histories
            ));
            return $viewModel;
        }
    }

    /**
     * This function is used to save oral history serach.
     * @author Icreon Tech - SK
     */
    public function saveOralHistorySearchAction() {
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        if ($request->isPost()) {
            $searchName = $request->getPost('search_name');
            parse_str($request->getPost('searchString'), $searchParam);
            if (trim($searchName) != '') {
                $saveSearchParam['search_type'] = 4;
                $saveSearchParam['user_id'] = $this->auth->getIdentity()->user_id;
                $saveSearchParam['is_active'] = 1;
                $saveSearchParam['is_deleted'] = 0;
                $saveSearchParam['title'] = $searchName;
                $saveSearchParam['search_query'] = serialize($searchParam);
                $saveSearchParam['save_date'] = DATE_TIME_FORMAT;
                if ($this->getServiceLocator()->get('Passenger\Model\FamilyhistoryTable')->saveUserFamilyHistorySearch($saveSearchParam) == true) {
                    $this->flashMessenger()->addMessage('Family history search is saved successfully!');
                    $messages = array('status' => "success");
                    return $response->setContent(\Zend\Json\Json::encode($messages));
                }
            }
        }
    }

    public function getUserSavedSearchSelectAction() {
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $search_id = $request->getPost('search_id');
                $dataParam['user_id'] = $this->auth->getIdentity()->user_id;
                $dataParam['search_type'] = 4;
                $dataParam['search_id'] = $search_id;
                $saved_searches = $this->getServiceLocator()->get('Passenger\Model\FamilyhistoryTable')->getUserSearches($dataParam);
                foreach ($saved_searches as $key => $value) {
                    $saved_searches_data[$value['search_id']] = $value;
                }
                $searchResult = $saved_searches_data[$search_id];
                $searchResult = json_encode(unserialize($searchResult['search_query']));
                return $response->setContent($searchResult);
            }
        }
    }

    public function moreOralHistoriesAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getConfig();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $oral_history_message = $this->_config['PassengerMessages']['config']['oral_history_library'];
        $sort_field = $param['sort_field'] = 'names.lastname';
        $sort_order = $param['sort_order'] = 'asc';

        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $data = $request->getPost()->toArray();
            parse_str($data['searchString']);
            $param['first_name'] = $first_name;
            $param['last_name'] = $last_name;
            if (isset($type))
                $param['type_id'] = $type;
            $param['country_id'] = $country;
            $param['topic'] = $topic;
            $param['sort_field'] = $sort_field;
            $param['sort_order'] = $sort_order;

            $numRecPerPage = 6;
            $limit = $param['record_limit'] = $numRecPerPage;
            $start_index = $param['start_index'] = (($page - 1) * $numRecPerPage);
            
            $oralHistoryData = $this->getTables()->getOralHistories($param);

            $oralHistories = array();

            if (!empty($oralHistoryData)) {
                foreach ($oralHistoryData as $key1 => $value1) {
                    $oralHistories[$value1['namesid']] = $value1;
                }
                foreach ($oralHistories as $key => $value) {
                    if (!is_null($value['passenger_id']) && $value['passenger_id'] != '') {
                        $ethnicityData = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getPassengerEthnicity(array('passengerId' => $value['passenger_id']));
                        $ethnicity = array();
                        if (!empty($ethnicityData) && $ethnicityData[0] != '') {
                            foreach ($ethnicityData as $k => $v) {
                                $ethnicity[] = $v['ethnicity'];
                            }
                        }
                        if (is_array($ethnicity))
                            $oralHistories[$key]['ethnicity'] = implode(",", $ethnicity);
                        else
                            $oralHistories[$key]['ethnicity'] = $ethnicity;
                    }
                    else
                        $oralHistories[$key]['ethnicity'] = 'N/A';
                }
            }
            if ($this->auth->hasIdentity() == true) {
                $loggedInUserId = $this->auth->getIdentity()->user_id;
            } else {
                $loggedInUserId = '';
            }

            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $oral_history_message,
                'oralHistories' => $oralHistories,
                'sort_field' => $sort_field,
                'sort_order' => $sort_order,
                'page' => $page,
                'limit' => $limit,
                'start_index' => $start_index,
				'oral_history_assets' => $this->_config['file_upload_path']['oral_history_assets'],
				'oral_history_assets_upload_dir' => $this->_config['file_upload_path']['oral_history_assets_upload_dir'],
                'loggedInUserId' => $loggedInUserId
            ));
            return $viewModel;
        }
    }

	public function oralHistoryAudioAction(){
		$this->checkFrontUserAuthentication();
		$this->layout('web-popup');
		$request = $this->getRequest();
		$viewModel = new ViewModel();
		if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
		$params = $this->params()->fromRoute();
		$filePath = $this->decrypt($params['filePath']);
		//$viewModel->setTerminal(true);
		$viewModel->setVariables(array(
			'filePath' => $filePath
		));
		
        return $viewModel;
	}

	public function oralHistoryTextAction(){
		$this->checkFrontUserAuthentication();
		$this->layout('web-popup');
		$request = $this->getRequest();
		$viewModel = new ViewModel();
		if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
		$params = $this->params()->fromRoute();
		$filePath = $this->decrypt($params['filePath']);
		$lines = file($filePath);
		$contents = implode("",$lines);
		
		//$viewModel->setTerminal(true);
		$viewModel->setVariables(array(
			'contents' => $contents
		));
		
        return $viewModel;
	}

}

