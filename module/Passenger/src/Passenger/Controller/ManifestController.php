<?php

/**
 * This controller is used for Document module in CRM.
 * @package    Document
 * @author     Icreon Tech -SR.
 */

namespace Passenger\Controller;

use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Passenger\Model\Manifest;
use Passenger\Form\EditManifestForm;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Authentication\AuthenticationService;
use Passenger\Form\SearchManifestForm;
use Passenger\Form\SaveSearchManifestForm;

/**
 * This class is used for Document module in CRM.
 * @author     Icreon Tech -SR.
 */
class ManifestController extends BaseController {

    protected $_manifestTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_authservice = null;
    protected $_flashMessage = null;

    /**
     * This is used for dafault initialization of objects.
     * @author     Icreon Tech -SR.
     */
    public function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    /**
     * This function is used to get table and config file object
     * @return table gateway
     */
    public function getManifestTable() {
        if (!$this->_manifestTable) {
            $sm = $this->getServiceLocator();
            $this->_manifestTable = $sm->get('Passenger\Model\ManifestTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
        }

        return $this->_manifestTable;
    }

    /**
     * This Action is used to show the default search form and save search action
     * @return this will the manifest search form values
     * @author Icreon Tech -SR
     */
    public function getManifestAction() {

        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getManifestTable();
        $manifestMsg = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['manifest_search'], $this->_config['PassengerMessages']['config']['edit_manifest']);
        $request = $this->getRequest();

        $search_manifest_form = new SearchManifestForm();
        $save_search_manifest_form = new SaveSearchManifestForm();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $response = $this->getResponse();

        $getSearchArray = array();
        $getSearchArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
        $getSearchArray['isActive'] = '1';
        $getSearchArray['searchId'] = '';
        $crmSearchArray = $this->_manifestTable->getManifestSavedSearch($getSearchArray);
        $crmSearch_list = array();
        $crmSearch_list[''] = 'Select';
        foreach ($crmSearchArray as $key => $val) {
            $crmSearch_list[$val['manifest_search_id']] = stripslashes($val['title']);
        }
        $save_search_manifest_form->get('saved_search')->setAttribute('options', $crmSearch_list);

        /** Series */
        $passengerSeries = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPassengerSeries();

        $seriesList = array();
        $seriesList[''] = 'Select';
        foreach ($passengerSeries as $key => $val) {
            $seriesList[$val['series']] = $val['series'];
        }

        $search_manifest_form->get('series')->setAttribute('options', $seriesList);
        /** End Series */
        //      die;

        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $viewModel->setVariables(array(
            'search_manifest_form' => $search_manifest_form,
            'save_search_manifest_form' => $save_search_manifest_form,
            'mode' => '',
            'messages' => $messages,
            'jsLangTranslate' => $manifestMsg
        ));
        return $viewModel;
    }

    /**
     * This Action is used to save the crm manifest search
     * @return this will be a confirmation
     * @author Icreon Tech - SR
     */
    public function saveManifestSearchAction() {
        $this->checkUserAuthentication();
        $this->getManifestTable();
        $manifestMsg = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['manifest_search'], $this->_config['PassengerMessages']['config']['edit_manifest']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $manifest = new Manifest($this->_adapter);
        if ($request->isPost()) {
            parse_str($request->getPost('searchString'), $searchParam);
            $search_name = $request->getPost('search_name');
            $messages = array();
            $searchParam['search_title'] = $search_name;
            $searchParam = $manifest->getInputFilterCrmSearch($searchParam);
            $search_name = $manifest->getInputFilterCrmSearch($search_name);

            if (trim($search_name) != '') {
                $saveSearchParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $saveSearchParam['isActive'] = 1;
                $saveSearchParam['isDelete'] = 0;
                $saveSearchParam['search_name'] = $search_name;
                $saveSearchParam['search_query'] = serialize($searchParam);

                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modifiend_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['search_id'] = $request->getPost('search_id');
                    if ($this->getManifestTable()->updateCrmManifestSearch($saveSearchParam) == true) {
                        $this->flashMessenger()->addMessage($manifestMsg['SEARCH_UPDATE_CONFIRM']);
                        $messages = array('status' => "success", 'message' => $manifestMsg['SEARCH_UPDATE_CONFIRM']);
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {
                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    if ($this->getManifestTable()->saveManifestSearch($saveSearchParam) == true) {
                        $this->flashMessenger()->addMessage($manifestMsg['SEARCH_SAVE_CONFIRM']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used to view the manifest details
     * @return void
     * @author Icreon Tech - SR
     */
    public function viewManifestAction() {
        $this->layout('crm');
        $this->getManifestTable();
        $manifestMsg = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['manifest_search']);
        $request = $this->getRequest();

        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'mode' => '',
            'jsLangTranslate' => $manifestMsg
        ));
        return $viewModel;
    }

    /**
     * This Action is used to get the saved crm ship search
     * @param this will be search_id
     * @return this will be search result array
     * @author Icreon Tech - SR
     */
    public function getCrmSearchManifestInfoAction() {
        $this->checkUserAuthentication();
        $this->getManifestTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $dataParam['isActive'] = '1';
                $dataParam['searchId'] = $request->getPost('search_id');
                $searchResult = $this->_manifestTable->getCrmManifestSavedSearch($dataParam);
                $searchResult = json_encode(unserialize($searchResult[0]['search_query']));
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This Action is used to generte the select option after saving the new search title
     * @return this will be a string having the all options
     * @author Icreon Tech - SR
     */
    public function getSavedCrmManifestSearchSelectAction() {

        $this->getManifestTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam = array();
            $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $dataParam['isActive'] = '1';
            $dataParam['shipSearchId'] = '';

            $crmSearchArray = $this->_manifestTable->getCrmManifestSavedSearch($dataParam);
            $options = '';
            $options .="<option value=''>" . $this->_config['PassengerMessages']['config']['common']['SELECT'] . "</option>";
            foreach ($crmSearchArray as $key => $val) {
                $options .="<option value='" . $val['manifest_search_id'] . "'>" . $val['title'] . "</option>";
            }
            return $response->setContent($options);
        }
    }

    /**
     * This Action is used for the search page of Document crm search passenger
     * @return this will be result array all manifest result.
     * @author Icreon Tech -SR
     */
    public function getSearchManifestAction() {

        $this->checkUserAuthentication();

        $this->getManifestTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $manifest = new Manifest($this->_adapter);
        parse_str($request->getPost('searchString'), $searchParam);
        $searchParam['frame'] = ltrim($searchParam['frame'], 0);
        $dashlet = $request->getPost('crm_dashlet');
        $dashletSearchId = $request->getPost('searchId');
        $dashletId = $request->getPost('dashletId');
        $dashletSearchType = $request->getPost('searchType');
        $dashletSearchString = $request->getPost('dashletSearchString');
        if (isset($dashletSearchString) && $dashletSearchString != '') {
            $searchParam = unserialize($dashletSearchString);
        }

        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $ArrivalDateArray = array();
        if (isset($searchParam['arrival_from_date']) && $searchParam['arrival_from_date'] != '') {
            $ArrivalDateArray = explode('/', $searchParam['arrival_from_date']);
            $searchParam['arrival_from_date'] = $ArrivalDateArray[2] . '-' . $ArrivalDateArray[0] . '-' . $ArrivalDateArray[1];
        }
        if (isset($searchParam['arrival_to_date']) && $searchParam['arrival_to_date'] != '') {
            $ArrivalDateArray = explode('/', $searchParam['arrival_to_date']);
            $searchParam['arrival_to_date'] = $ArrivalDateArray[2] . '-' . $ArrivalDateArray[0] . '-' . $ArrivalDateArray[1];
        }
        $searchManifestParam = $manifest->assignManifestArray($searchParam);

        $searchManifestParam['getRecordsCount'] = '';
        $isExport = $request->getPost('export');

        $searchManifestParam['startIndex'] = '';
        $searchManifestParam['recordLimit'] = '';
        // $searchManifestParam['sortField'] = '';
        //$searchManifestParam['sortOrder'] = '';
        $searchManifestParam['getRecordsCount'] = 'yes';
        $countResult = array();
        if ($searchManifestParam['roll_number'] != '')
            $searchManifestParam['roll_number'] = $searchManifestParam['series'] . '-' . $searchManifestParam['roll_number'];

        $countResult = $this->_manifestTable->searchCrmManifest($searchManifestParam);
        $searchManifestParam['getRecordsCount'] = '';
        $searchManifestParam['startIndex'] = $start;
        $searchManifestParam['recordLimit'] = $limit;
        $searchManifestParam['sortField'] = $request->getPost('sidx');
        $searchManifestParam['sortOrder'] = $request->getPost('sord');
        if ($isExport != "" && $isExport == "excel") {
            $chunksize = $this->_config['export']['chunk_size'];
            $export_limit = $this->_config['export']['export_limit'];

            $searchManifestParam['recordLimit'] = $chunksize;
        }
        $page_counter = 1;
        do {
            $searchResult = $this->_manifestTable->searchCrmManifest($searchManifestParam);
            $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
            $dashletColumnName = array();
            if (!empty($dashletResult)) {
                $dashletColumn = explode(",", $dashletResult[0]['table_column_names']);
                foreach ($dashletColumn as $val) {
                    if (strpos($val, ".")) {
                        $dashletColumnName[] = trim(strstr($val, ".", false), ".");
                    } else {
                        $dashletColumnName[] = trim($val);
                    }
                }
            }
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $total = $countResult[0]['RecordCount'];
            if ($isExport == "excel") {
                /* if ($total > $export_limit) {
                  $totalExportedFiles = ceil($total / $export_limit);
                  $exportzip = 1;
                  }
                 * */

                if ($total > $chunksize) {
                    //asd($searchManifestParam,true);
                    $number_of_pages = ceil($total / $chunksize);
                    //echo $number_of_pages .'--'.$page_counter;
                    //echo "<br>";
                    $page_counter++;
                    $start = $chunksize * $page_counter - $chunksize;
                    $searchManifestParam['recordLimit'] = $chunksize;
                    $searchManifestParam['startIndex'] = $start;
                    $searchManifestParam['page'] = $page_counter;
                } else {
                    $number_of_pages = 1;
                    $page_counter++;
                }
            } else {
                $page_counter = $page;
                $number_of_pages = ceil($total / $limit);
                $jsonResult['records'] = $countResult[0]['RecordCount'];
                $jsonResult['page'] = $page;
                $jsonResult['total'] = ceil($countResult[0]['RecordCount'] / $limit);
            }
            if (count($searchResult) > 0) {
                $arrExport = array();
                foreach ($searchResult as $val) {

                    $dashletCell = array();

                    if (false !== $dashletColumnKey = array_search('ROLL_NBR', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = '<a href="/view-crm-manifest/' . $this->encrypt($val['FRAME']) . '/' . $this->encrypt($val['ROLL_NBR']) . '/' . $this->encrypt('view') . '" class="txt-decoration-underline">' . $val['ROLL_NBR'] . '</a>';

                    if (false !== $dashletColumnKey = array_search('FRAME', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $val['FRAME'];

                    // $arrCell['id'] = $val['ID'];

                    if ($val['is_edited_image'] == 0)
                        $isEdited = 'No';
                    else if ($val['is_edited_image'] == 1)
                        $isEdited = 'Yes';

                    $viewLabel = $this->_config['PassengerMessages']['config']['common']['VIEW'];
                    $editLabel = $this->_config['PassengerMessages']['config']['common']['EDIT'];
                    $view = "<a class='view-icon' href='/view-crm-manifest/" . $this->encrypt($val['FRAME']) . "/" . $this->encrypt($val['ROLL_NBR']) . "/" . $this->encrypt('view') . "'><div class='tooltip'>" . $viewLabel . "<span></span></div></a>";
                    $edit = "<a class='edit-icon' href='/edit-crm-manifest/" . $this->encrypt($val['FRAME']) . "/" . $this->encrypt($val['ROLL_NBR']) . "/" . $this->encrypt('edit') . "'><div class='tooltip'>" . $editLabel . "<span></span></div></a>";
                    $series_number = '';
                    $arrival_date = '';
                    $date = $val['ARRIVAL_DATE'];
                    if (strpos($date, '-') != false) {
                        if (!empty($date)) {
                            $dateDataArray = explode('-', substr($date, 0, 10));
                            $arrival_date = $dateDataArray[1] . '-' . $dateDataArray[2] . '-' . $dateDataArray[0];
                        }
                    } else if (strpos($date, '/') != false) {
                        if (!empty($date)) {
                            $dateDataArray = explode('/', $date);
                            $arrival_date = $dateDataArray[0] . '-' . $dateDataArray[1] . '-' . $dateDataArray[2];
                        }
                    } else {
                        if (!empty($date)) {
                            $dateDataArray = explode(' ', $date);
                            $arrival_date = date("m", strtotime($dateDataArray[0])) . '-' . $dateDataArray[1] . '-' . $dateDataArray[2];
                        }
                    }

                    if (false !== $dashletColumnKey = array_search('ARRIVAL_DATE', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $arrival_date;
                    if (false !== $dashletColumnKey = array_search('SHIP_NAME', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = stripslashes($val['SHIP_NAME']);

                    if (false !== $dashletColumnKey = array_search('UDE_BATCH_NUMBER', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = stripslashes($val['UDE_BATCH_NUMBER']);

                    if (false !== $dashletColumnKey = array_search('is_edited_image', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $isEdited;

                    $passengerParam = array();
                    //     $frameParam = array();
                    $passResult = array();
                    if ($val['FILENAME'] != '') {
                        $passengerParam['file_name'] = $val['FILENAME'];
                        $passResult = $this->_manifestTable->getManifestPassengerCount($passengerParam);
                    }

                    if (!empty($val['ROLL_NBR']))
                        $series_number = substr($val['ROLL_NBR'], 0, 4);

                    $noOfPassenger = "<a href='javascript:void(0)' style='text-decoration: underline;' onClick=showPassengerListing('" . $this->encrypt($val['FILENAME']) . "')>" . $passResult[0]['passenger_count'] . "</a>";
                    if (false !== $dashletColumnKey = array_search('passenger_count', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $passResult[0]['passenger_count'];

                    if (isset($dashlet) && $dashlet == 'dashlet') {
                        $arrCell['cell'] = $dashletCell;
                    } else if ($isExport == "excel") {
                        //'Series' => $series_number, 
                        $arrExport[] = array('Roll Number' => $val['ROLL_NBR'], 'No of Passengers' => $passResult[0]['passenger_count'], 'Frame Number' => $val['FRAME'], 'Arrival Date' => $arrival_date, 'UDE Batch Number' => $val['UDE_BATCH_NUMBER'], 'Ship Name' => stripslashes($val['SHIP_NAME']), 'Is Image Edited' => $isEdited);
                    } else {
                        //  $series_number, 
                        $arrCell['cell'] = array($val['ROLL_NBR'], $noOfPassenger, $val['FRAME'], $arrival_date, $val['UDE_BATCH_NUMBER'], stripslashes($val['SHIP_NAME']), $isEdited, $view . $edit);
                    }
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
                if ($isExport == "excel") {
                    $filename = "manifest_export_" . date("Y-m-d") . ".csv";
                    $this->arrayToCsv($arrExport, $filename, $page_counter);
                }
            }
        } while ($page_counter <= $number_of_pages && $isExport == "excel");
        if ($isExport == "excel") {
            $this->downloadSendHeaders("manifest_export_" . date("Y-m-d") . ".csv");
            die;
        } else {
            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
        }
        //return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This Action is used to to view the manifest details
     * @return this will view page
     * @author Icreon Tech - SR
     */
    public function viewCrmManifestAction() {
        $this->checkUserAuthentication();

        $this->layout('crm');
        $this->getManifestTable();
        $params = $this->params()->fromRoute();
        $manifestMsg = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['manifest_search'], $this->_config['PassengerMessages']['config']['edit_manifest']);

        //$searchManifestParam['frame'] = $this->decrypt($params['frame']);
        //$searchManifestParam['roll_number'] = $this->decrypt($params['roll_no']);

        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'frame' => $params['frame'],
            'roll_number' => $params['roll_no'],
            'mode' => $this->decrypt($params['mode']),
            'moduleName' => $this->encrypt('manifest'),
            'jsLangTranslate' => $manifestMsg,
        ));
        return $viewModel;
    }

    /**
     * This Action is used to to view the manifest details in tabbing
     * @return this will return searchResult having the all search result details
     * @author Icreon Tech - SR
     */
    public function viewCrmManifestDetailsAction() {
        $this->checkUserAuthentication();
        $this->getManifestTable();
        $manifestMsg = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['manifest_search'], $this->_config['PassengerMessages']['config']['edit_manifest']);
        $params = $this->params()->fromRoute();
        $searchManifestParam = array();
        $searchResult = array();
        $manifest = new Manifest($this->_adapter);
        $searchManifestParam['frame'] = $this->decrypt($params['frame']);
        $searchManifestParam['roll_number'] = $this->decrypt($params['roll_no']);
        $searchManifestParam = $manifest->assignManifestArray($searchManifestParam);
        $searchManifestParam['getRecordsCount'] = '';
        $searchResult = $this->_manifestTable->searchCrmManifest($searchManifestParam);

        //$manifestPath = $this->_config['Image_path']['manifestPath'];
		$manifestPath = $config['file_upload_path']['assets_url'].'manifest/';
        $manifestParam['dateArrive'] = substr($searchResult[0]['ARRIVAL_DATE'], 0, 10);
        $manifestParam['shipName'] = $searchResult[0]['SHIP_NAME'];
        $shipManifestResult = array();


        if ($searchResult[0]['data_source'] == 1) {
            $shipManifestResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getShipManifest($manifestParam);
        } else if ($searchResult[0]['data_source'] == 2) {
            $manifestParam['dataSource'] = 2;
            
            $manifestFileNameArray = explode('_', $searchResult[0]['FILENAME']);
            $manifestParam['fileNamePattern'] = $manifestFileNameArray[0];              
            $dateArray = date_parse_from_format("Y-m-d", $searchResult[0]['ARRIVAL_DATE']);
            $manifestParam['dateArriveFrom'] = $dateArray['year'].'-01-01';
            $manifestParam['dateArriveTo'] = $dateArray['year'].'-12-31';            
            
            $shipManifestResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getFamilyShipManifest($manifestParam);
        }

        //$shipManifestResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getShipManifest($manifestParam);
        // asd($shipManifestResult);

        $shipManifestArray = array();
        //ini_set('max_execution_time', 300);
        if (count($shipManifestResult) > 0) {
            $startFrame = $shipManifestResult[0]['START_RANGE'];
            $endFrame = $shipManifestResult[0]['END_RANGE'];

            $rollNbr = $shipManifestResult[0]['ROLL_NBR'];
            for ($k = $startFrame; $k <= $endFrame; $k++) {

                if ($searchResult[0]['data_source'] == 2) {
                    //$shipManifestFilenameArray[] = $shipManifestResult[0]['FILENAME'];
                    //$fileName = $shipManifestResult[0]['FILENAME'];
                    //$shipManifestArray[] = $shipManifestResult[0]['FILENAME'];
                    $fileName = substr($rollNbr, 5) . '_' . sprintf('%05d', $k) . '.jpg';
                    $folderName = substr($fileName, 0, 9);
                    $shipManifestArray[] = $fileName;
                    $shipManifestFilenameArray[] = $fileName;
                    
                } else {
                    $fileName = $rollNbr . sprintf('%04d', $k) . '.TIF';
                    $folderName = substr($fileName, 0, 9);
                    $shipManifestArray[] = $fileName;
                    $shipManifestFilenameArray[] = strtoupper($fileName);
                }
            }
        }
        $defaultImageIndex = 0;

        if ($searchResult[0]['data_source'] == 2) {
            $manifest_file_name = $searchResult[0]['FILENAME'];
            $defaultImageIndex = array_search($manifest_file_name, $shipManifestFilenameArray);
        } else {
            $manifest_file_name = strtoupper($searchResult[0]['FILENAME']);
            $defaultImageIndex = array_search($manifest_file_name, $shipManifestFilenameArray);            
        }

        $passengerParam = array();
        $passResult = array();
        $passengerCountInManifest = 0;
        if ($searchResult[0]['FILENAME'] != '') {
            $passengerParam['file_name'] = $searchResult[0]['FILENAME'];
            $passResult = $this->_manifestTable->getManifestPassengerCount($passengerParam);
            if (count($passResult) > 0)
                $passengerCountInManifest = $passResult[0]['passenger_count'];
        }

        if (count($shipManifestArray) == 1) {
            if ($searchResult[0]['data_source'] == 2) {
                $imagePartArray = explode('_', $shipManifestArray[0]);
                $sFileName = $imagePartArray[0] . '_' . sprintf('%05d', (substr($imagePartArray[1], 0, 5) + 1)) . '.jpg';
                $shipManifestFilenameArray[] = $sFileName;
                $shipManifestArray[] = $sFileName;
            }
        }

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'frame' => $params['frame'],
            'roll_number' => $params['roll_no'],
            'moduleName' => $this->encrypt('manifest'),
            'searchResult' => $searchResult[0],
            'jsLangTranslate' => $manifestMsg,
            'manifestPath' => $config['file_upload_path']['assets_url'].'manifest/',
            'defaultImageIndex' => $defaultImageIndex,
            'shipManifestArray' => $shipManifestArray,
            'shipManifestFilenameArray' => $shipManifestFilenameArray,
            'passengerCountInManifest' => $passengerCountInManifest,
            'dataSource' => $searchResult[0]['data_source'],
            'uploadFilePath' => $this->_config['file_upload_path']['assets_upload_dir']
        ));
        return $viewModel;
    }

    /**
     * This Action is used to get the edit crm ship details
     * @param this will pass $params
     * @return this will be $viewModel
     * @author Icreon Tech - SR
     */
    public function editCrmManifestDetailsAction() {
        $this->checkUserAuthentication();

        $this->getManifestTable();
        $manifestMsg = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['manifest_search'], $this->_config['PassengerMessages']['config']['edit_manifest']);
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $response = $this->getResponse();
        $edit_form = new EditManifestForm();

        $manifest = new Manifest($this->_adapter);
        $searchManifestParam['frame'] = $this->decrypt($params['frame']);
        $searchManifestParam['roll_number'] = $this->decrypt($params['roll_no']);
        $searchManifestParam = $manifest->assignManifestArray($searchManifestParam);
        $searchManifestParam['getRecordsCount'] = '';
        $manifestResult = $this->_manifestTable->searchCrmManifest($searchManifestParam);


        $edit_form->get('frame')->setAttribute('value', $manifestResult[0]['FRAME']);
        $edit_form->get('roll_number')->setAttribute('value', $manifestResult[0]['ROLL_NBR']);
        $edit_form->get('frame_old')->setAttribute('value', $this->encrypt($manifestResult[0]['FRAME']));
        $edit_form->get('roll_number_old')->setAttribute('value', $this->encrypt($manifestResult[0]['ROLL_NBR']));
        $edit_form->get('series')->setAttribute('value', substr($manifestResult[0]['ROLL_NBR'], 0, 4));

        $date = $manifestResult[0]['ARRIVAL_DATE'];

        if (strpos($date, '-') != false) {
            if (!empty($date)) {
                $dateDataArray = explode('-', substr($date, 0, 10));
                $arrival_date = $dateDataArray[0] . '-' . $dateDataArray[1] . '-' . $dateDataArray[2];
            }
        } else if (strpos($date, '/') != false) {
            if (!empty($date)) {
                $dateDataArray = explode('/', $date);
                $arrival_date = $dateDataArray[0] . '-' . $dateDataArray[1] . '-' . $dateDataArray[2];
            }
        } else {
            if (!empty($date)) {
                $dateDataArray = explode(' ', $date);
                $arrival_date = $dateDataArray[2] . '-' . date("m", strtotime($dateDataArray[0])) . '-' . $dateDataArray[1];
            }
        }

        $this->DateFormat($arrival_date, 'calender');
        $edit_form->get('arrival_date')->setAttribute('value', $this->DateFormat($arrival_date, 'calender'));


        $edit_form->get('ude_batch_number')->setAttribute('value', $manifestResult[0]['UDE_BATCH_NUMBER']);
        $edit_form->get('file_name')->setAttribute('value', $manifestResult[0]['FILENAME']);
        $edit_form->get('ship')->setAttribute('value', $manifestResult[0]['SHIP_NAME']);
        $edit_form->get('is_edited_image')->setAttribute('value', $manifestResult[0]['is_edited_image']);
        $manifestPassParam['manifestImage'] = $manifestResult[0]['FILENAME'];
        $passImageArray = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getManifestPassengerRecord($manifestPassParam);
        $passengerIdArray = array();
        if (count($passImageArray) > 0) {
            foreach ($passImageArray as $val)
                $passengerIdArray[] = $val['ID'];
        }
        $passengerIdStr = implode(',', $passengerIdArray);

        if ($request->isPost()) {
            $edit_form->setInputFilter($manifest->getManifestInputFilter());
            $edit_form->setData($request->getPost());

            if (!$edit_form->isValid()) {
                $msg = array();
                $errors = $edit_form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $manifestMsg[$rower];
                        }
                    }
                }

                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                //die("validated");
                $manifest->exchangeManifestArray($edit_form->getData());
                $postData = array();
                $frame_old = $this->decrypt($request->getPost('frame_old'));
                $roll_number_old = $this->decrypt($request->getPost('roll_number_old'));
                $postData['frame'] = $manifest->frame;
                $postData['roll_number'] = $manifest->roll_number;
                $postData['arrival_date'] = $this->DateFormat($manifest->arrival_date, 'db_date_format') . ' 00:00:00';
                $postData['ude_batch_number'] = $manifest->ude_batch_number;
                $postData['file_name'] = $manifest->file_name;
                $postData['ship'] = $manifest->ship;
                $postData['frame_old'] = $frame_old;
                $postData['roll_number_old'] = $roll_number_old;
                $postData['filename_old'] = $request->getPost('filename_old');
                $postData['ude_batch_no_old'] = $request->getPost('ude_batch_no_old');
                $postData['modified_time'] = DATE_TIME_FORMAT;

                $postData['passenger_id'] = $request->getPost('passenger_id');
                $postData['is_edited_image'] = $manifest->is_edited_image;

                $flag = $this->_manifestTable->updateCrmManifest($postData);


                // $LINEID = $request->getPost('');

                /** insert into the change log */
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_manifest_change_logs';
                $changeLogArray['activity'] = '2';
                $changeLogArray['id'] = $frame_old . $roll_number_old;
                //$changeLogArray['roll'] = $roll_number_old;
                $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);

                /** end insert into the change log */
                if ($flag == true) {
                    $this->flashMessenger()->addMessage($manifestMsg['MANIFEST_UPDATE_MSG']);
                    $messages = array('status' => "success");
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        }

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'frame' => $manifestResult[0]['FRAME'],
            'roll_number' => $manifestResult[0]['ROLL_NBR'],
            'edit_form' => $edit_form,
            'moduleName' => $this->encrypt('manifest'),
            'jsLangTranslate' => $manifestMsg,
            'passengerIdStr' => $passengerIdStr,
            'filename_old' => $manifestResult[0]['FILENAME'],
            'ude_batch_no_old' => $manifestResult[0]['UDE_BATCH_NUMBER'],
            'manifestPath' => $config['file_upload_path']['assets_url'].'manifest/',
        ));

        return $viewModel;
    }

    /**
     * This Action is used to delete the the saved crm manifest search
     * @return this will be a confirmation message
     * @author Icreon Tech - SR
     */
    public function deleteCrmSearchManifestAction() {
        $this->checkUserAuthentication();
        $this->getManifestTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['search_id'] = $request->getPost('search_id');
            $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $dataParam['isDelete'] = 1;
            $dataParam['modifiend_date'] = DATE_TIME_FORMAT;
            $this->_manifestTable->deleteCrmManifestSearch($dataParam);
            $this->flashMessenger()->addMessage('Search is deleted successfully!');
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This Action is used to get the manifest passenger listing
     * @param void
     * @return void
     * @author Icreon Tech - SR
     */
    public function manifestPassengerAction() {
        $this->checkUserAuthentication();
        $this->getManifestTable();
        $this->layout('popup');
        $params = $this->params()->fromRoute();
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'fileName' => $params['fileName']
        ));
        return $viewModel;
    }

    /**
     * This Action is used to get the manifest passenger list
     * @return this will be a list of passenger
     * @author Icreon Tech - SR
     */
    public function getManifestPassengerListAction() {
        $this->checkUserAuthentication();
        // $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $fileName = $this->decrypt($request->getPost('fileName'));
        $this->getManifestTable();

        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $searchParam['fileName'] = $fileName;
        $searchResult = $this->_manifestTable->getManifestPassengers($searchParam);

        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();

        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
        $maritalStatusArray = $this->maritalStatusArray();
        $genderArray = $this->genderArray();
        if (count($searchResult) > 0) {
            foreach ($searchResult as $val) {
                $gender = 'Unknown';
                $maritalStatus = 'Unknown';
                if ($val['PRIN_GENDER_CODE'] != '')
                    $gender = $genderArray[$val['PRIN_GENDER_CODE']];

                if ($val['PRIN_MARITAL_STAT'] != '')
                    $maritalStatus = $maritalStatusArray[$val['PRIN_MARITAL_STAT']];

                $arrCell['cell'] = array($val['REF_PAGE_LINE_NBR'], $val['ID'], $val['PRIN_LAST_NAME'], $val['PRIN_FIRST_NAME'], $gender, $val['PRIN_PLACE_RESI'], $maritalStatus, $val['PRIN_AGE_ARRIVAL'], $val['ethnicity']);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

}