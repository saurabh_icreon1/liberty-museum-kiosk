<?php

/**
 * This is used for Document module in CRM.
 * @package    Document
 * @author     Icreon Tech -SR.
 */

namespace Passenger\Controller;

use Base\Controller\BaseController;
use Base\Model\ZipStream;
use Zend\View\Model\ViewModel;
use Passenger\Model\Document;
use Passenger\Form\DocumentForm;
use Passenger\Form\ShipForm;
use Passenger\Form\CrmSearchPassengerForm;
use Passenger\Form\SaveSearchForm;
use Passenger\Form\EditPassengerForm;
use Passenger\Form\SearchShipForm;
use Passenger\Form\SaveSearchShipForm;
use Passenger\Form\EditShipForm;
use Passenger\Form\SearchShiplineForm;
use Passenger\Form\SaveSearchShiplineForm;
use Passenger\Form\EditShiplineForm;
use Zend\Mvc\Controller\Plugin\Params;
use Passenger\Form\AnnotationForm;
use Passenger\Form\SearchAnnotationForm;
use Passenger\Form\SearchAnnotationListForm;
use Passenger\Form\SaveSearchAnnotationForm;
use Passenger\Form\CrmSearchBulkPassengerForm;
use Passenger\Form\CrmBulkPassengerUpdateForm;
use Zend\Authentication\AuthenticationService;
use Passenger\Form\UpdateShipnameForm;
use Passenger\Form\CrmShipManifestForm;
use Base\Model\SphinxComponent;
use Base\Model\SphinxClient;
use Zend\Session\Container;

/**
 * This is Class used for Passenger/Document actions.
 * @package    Document
 * @author     Icreon Tech -SR.
 */
class DocumentController extends BaseController {

    protected $_documentTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_authservice = null;
    protected $_flashMessage = null;

    /**
     * This is used for dafault initialization of objects.
     * @author     Icreon Tech -SR.
     */
    public function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    /**
     * This function is used to get table and config file object
     * @return table gateway
     */
    public function getDocumentTable() {
        if (!$this->_documentTable) {
            $sm = $this->getServiceLocator();
            $this->_documentTable = $sm->get('Passenger\Model\DocumentTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
        }

        return $this->_documentTable;
    }

    /**getCrmSearchPassenger
     * This Action is used to show the default search form and save search action
     * @return this will return the crm search passengers form parameters 
     * @author Icreon Tech -SR
     */
    public function crmPassengerAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getDocumentTable();
        $request = $this->getRequest();
        $search_passenger_form = new CrmSearchPassengerForm();
        $save_search_form = new SaveSearchForm();
        /** Series */
        $passengerSeries = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPassengerSeries();
        $seriesList = array();
        $seriesList[''] = 'Select';
        foreach ($passengerSeries as $key => $val) {
            $seriesList[$val['series']] = $val['series'];
        }
        $search_passenger_form->get('series')->setAttribute('options', $seriesList);
        /** End Series */
        $getSearchArray = array();
        $ethnicityArray = $this->getDocumentTable()->getEthnicity($getSearchArray);
        $ethnicityList = array();
        $ethnicityList[''] = 'Select';
        foreach ($ethnicityArray as $key => $val) {
            if (!empty($val['ethnicity']))
                $ethnicityList[$val['ethnicity']] = trim(trim($val['ethnicity'], '-'), '.');
        }

        $search_passenger_form->get('ethnicity')->setAttribute('options', $ethnicityList);

        $getSearchArray = array();
        $getSearchArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
        $getSearchArray['isActive'] = '1';
        $getSearchArray['passengerSearchId'] = '';

        $crmSearchArray = $this->getDocumentTable()->getCrmSavedSearch($getSearchArray);
        $crmSearchList = array();
        $crmSearchList[''] = 'Select';
        foreach ($crmSearchArray as $key => $val) {
            $crmSearchList[$val['passenger_search_id']] = stripslashes($val['title']);
        }
        $search_passenger_form->get('saved_search')->setAttribute('options', $crmSearchList);


        $arrivalYear = array();
        $arrivalYear[''] = 'Select';
        $arrivalYear['1800 - 1804'] = '1800 - 1804';
        for ($k = 1805; $k < 1984;) {
            $p = $k + 4;
            $arrivalYear[$k . ' - ' . $p] = $k . ' - ' . $p;
            $k = $k + 5;
        }
        $search_passenger_form->get('arrival_year')->setAttribute('options', $arrivalYear);
		$currentYear = array();
		$currentYear[''] = 'Select';
		$currentYear['0 - 10'] = '0 - 10';
		for($l=11;$l<231;){
			$m = $l+10;
			$currentYear[$l . ' - ' . $m] = $l . ' - ' . $m;
			$l = $l + 11;
		}
		$search_passenger_form->get('current_age_range')->setAttribute('options', $currentYear);
        $day = array();
        $day[''] = 'All';
        for ($i = 1; $i <= 31; $i++) {
            $day[$i] = $i;
        }
        $search_passenger_form->get('day_arrival')->setAttribute('options', $day);

        $getSearchArray = array();
        $portArray = $this->getDocumentTable()->getArrivalPort($getSearchArray);
        $portList = array();
        $portList[''] = 'Select';
        foreach ($portArray as $key => $val) {
            $portList[$val['ship_arrival_port']] = $val['ship_arrival_port'];
        }
        $search_passenger_form->get('arrival_port')->setAttribute('options', $portList);
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $viewModel->setVariables(array(
            'search_passeneger_form' => $search_passenger_form,
            'mode' => '',
            'search_form' => $save_search_form,
            'messages' => $messages,
            'jsLangTranslate' => array_merge($this->_config['PassengerMessages']['config']['passenger_search'], $this->_config['PassengerMessages']['config']['edit_passenger'], $this->_config['PassengerMessages']['config']['common'])
        ));
        return $viewModel;
    }

    /**
     * This Action is used for the search of Document crm search passenger
     * @return this will be searched passengers data
     * @author Icreon Tech -SR
     */
    public function getCrmSearchPassengerAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        /** Sphinx Setting section */
        $filePath = 'assets/export/';
        //$filePath = $this->_config['file_upload_path']['assets_upload_dir'] . 'export/';
        $sphinxServer = $this->_config['SphinxServer'];
        $sphinxComponentObj = new SphinxComponent($sphinxServer['serverName']);
        $sphinxComponentObj->initialize();
        $arrSphinxIndexes = $this->_config['SphinxServer']['passengerIndexes'];
        $request = $this->getRequest();
        $response = $this->getResponse();
        parse_str($request->getPost('searchString'), $searchParam);
        $dashlet = $request->getPost('crm_dashlet');
        $dashletSearchId = $request->getPost('searchId');
        $dashletId = $request->getPost('dashletId');
        $dashletSearchType = $request->getPost('searchType');
        $dashletSearchString = $request->getPost('dashletSearchString');
        if (isset($dashletSearchString) && $dashletSearchString != '') {
            $searchParam = unserialize($dashletSearchString);
        }
        $limit = ($request->getPost('rows') != 0) ? $request->getPost('rows') : '1';
        $page = $request->getPost('page');
        $isExport = $request->getPost('export');
        if ($isExport == "" || $isExport != "excel") {
            $arrSphinxSettings = array('limit' => (int) $limit,
                'index' => $arrSphinxIndexes,
                'page' => (int) $request->getPost('page'),
                // 'groupby'        =>  array(array("sphinx_group_by" , '@weight DESC')),
                'rankingMode' => SPH_RANK_PROXIMITY_BM25, // SPH_RANK_PROXIMITY_BM25,
                'matchMode' => SPH_MATCH_EXTENDED2, //SPH_MATCH_EXTENDED2, //SPH_MATCH_BOOLEAN, //SPH_MATCH_EXTENDED2,
                'filter' => array()
            );
        } else {
            $chunksize = $this->_config['export']['chunk_size'];
            $export_limit = $this->_config['export']['export_limit'];

            $arrSphinxSettings = array('limit' => (int) $chunksize,
                'index' => $arrSphinxIndexes,
                'page' => (int) 1,
                // 'groupby'        =>  array(array("sphinx_group_by" , '@weight DESC')),
                'rankingMode' => SPH_RANK_PROXIMITY_BM25, // SPH_RANK_PROXIMITY_BM25,
                'matchMode' => SPH_MATCH_EXTENDED2, //SPH_MATCH_EXTENDED2, //SPH_MATCH_BOOLEAN, //SPH_MATCH_EXTENDED2,
                'filter' => array()
            );
        }
        $sphinxComponentObj->SetFieldWeights(array('field0' => 10000,
            'field3' => 5000,
            'field2' => 100,
            'field18' => 100,
            'field7' => 100,
            'field9' => 60,
            'field11' => 50,
            'field13' => 100,
            'field16' => 5000,
            'field25' => 200,
            'field26' => 100,
            'field27' => 1110,
            'field28' => 50,
            'field29' => 50,
            'field30' => 50,
            'field31' => 50,
            'field32' => 50,
            'field21' => 50,
            'field20' => 50
        ));

        $sphinxComponentObj->SetIndexWeights($this->_config['SphinxServer']['passengerIndexesIndexWeights']);
        $passenger_id = (isset($searchParam['passenger_id']) && $searchParam['passenger_id'] != '') ? trim($searchParam['passenger_id']) : '';
        $ship = (isset($searchParam['ship']) && $searchParam['ship'] != '') ? trim($searchParam['ship']) : '';
        $first_name = (isset($searchParam['first_name']) && $searchParam['first_name'] != '') ? trim($searchParam['first_name']) : '';
        $last_name = (isset($searchParam['last_name']) && $searchParam['last_name'] != '') ? trim($searchParam['last_name']) : '';
        $series = (isset($searchParam['series']) && $searchParam['series'] != '') ? trim($searchParam['series']) : '';
        $frame = (isset($searchParam['frame']) && $searchParam['frame'] != '') ? trim($searchParam['frame']) : '';
        $frame = ltrim($frame, '0');
        $roll_number = (isset($searchParam['roll_number']) && $searchParam['roll_number'] != '') ? trim($searchParam['roll_number']) : '';
        if (isset($searchParam['arrival_year']) && $searchParam['arrival_year'] != '') {
            $arr_year_arr = explode(' - ', $searchParam['arrival_year']);
            $arrival_year_from = $arr_year_arr[0];
            $arrival_year_to = $arr_year_arr[1];
        } else {
            $arrival_year_from = '';
            $arrival_year_to = '';
        }
        $day_arrival = (isset($searchParam['day_arrival']) && $searchParam['day_arrival'] != '') ? trim($searchParam['day_arrival']) : '';
        $month_arrival = (isset($searchParam['month_arrival']) && $searchParam['month_arrival'] != '') ? trim($searchParam['month_arrival']) : '';

        if (isset($searchParam['birth_year']) && $searchParam['birth_year'] != '') {
            $birth_year_arr = explode(' - ', $searchParam['birth_year']);
            $birth_year_from = $birth_year_arr[0];
            $birth_year_to = $birth_year_arr[1];
        } else {
            $birth_year_from = '';
            $birth_year_to = '';
        }

        if (isset($searchParam['current_age_range']) && $searchParam['current_age_range'] != '') {
            $age_arr = explode(' - ', $searchParam['current_age_range']);
            $current_age_from = $age_arr[0];
            $current_age_to = $age_arr[1];
        } else {
            $current_age_from = '';
            $current_age_to = '';
        }

        if (isset($searchParam['age_at_arrival']) && $searchParam['age_at_arrival'] != '') {
            $age_at_arrival_arr = explode(' - ', $searchParam['age_at_arrival']);
            $arrival_age_from = $age_at_arrival_arr[0];
            $arrival_age_to = $age_at_arrival_arr[1];
        } else {
            $arrival_age_from = '';
            $arrival_age_to = '';
        }


        $gender = (isset($searchParam['gender']) && $searchParam['gender'] != '') ? trim($searchParam['gender']) : '';
        $maritial_status = (isset($searchParam['maritial_status']) && $searchParam['maritial_status'] != '') ? trim($searchParam['maritial_status']) : '';
        $dept_port = (isset($searchParam['dept_port']) && $searchParam['dept_port'] != '') ? trim($searchParam['dept_port']) : '';
        $arrival_port = (isset($searchParam['arrival_port']) && $searchParam['arrival_port'] != '') ? trim($searchParam['arrival_port']) : '';
        $ethnicity = (isset($searchParam['ethnicity']) && $searchParam['ethnicity'] != '') ? trim($searchParam['ethnicity']) : '';
        $last_residence = (isset($searchParam['last_residence']) && $searchParam['last_residence'] != '') ? trim($searchParam['last_residence']) : '';
        $search_type = (isset($searchParam['search_type']) && $searchParam['search_type'] != '') ? trim($searchParam['search_type']) : '';
        $placeOfBirth = (isset($searchParam['place_of_birth']) && $searchParam['place_of_birth'] != '') ? trim($searchParam['place_of_birth']) : '';
        // $passenger_id = '100001010001';
                
        $town = (isset($searchParam['town']) && $searchParam['town'] != '') ? trim($searchParam['town']) : '';
        $searchTypeTown = (isset($searchParam['search_type_town']) && $searchParam['search_type_town'] != '') ? trim($searchParam['search_type_town']) : '';
        $arrSphinxSettingsStr = '';

        if (isset($arrival_year_from) && $arrival_year_from != '')
            $sphinxComponentObj->SetFilterRange('field19', $arrival_year_from, $arrival_year_to);

        if (isset($passenger_id) && $passenger_id != '')
            $arrSphinxSettingsStr.= ' & (@(field0) ("*' . $passenger_id . '*"))';
        if (isset($ship) && $ship != '')
            $arrSphinxSettingsStr.= ' & (@(field7) ("*' . $ship . '*"))';
        if (isset($first_name) && $first_name != '')
            $arrSphinxSettingsStr.= ' & (@(field2) ("*' . $first_name . '*"))';
        if (isset($last_name) && $last_name != '') {
            if ($search_type == 'exact') { //Exact Match
                $arrSphinxSettingsStr.= ' & (@(field1) ("_START_' . $last_name . '_END_"))';
            } else if ($search_type == 'startwith') { //Start with
                $arrSphinxSettingsStr.= ' & (@(field3) ("^' . $last_name . '*"))';
            } else if ($search_type == 'soundlike') { // sound like
                $arrSphinxSettingsStr.= ' & (@(field35) ("*' . soundex($last_name) . '*"))';
            }
            else
                $arrSphinxSettingsStr.= ' & (@(field3) ("*' . $last_name . '*"))';
        }
        if (isset($series) && $series != '')
            $arrSphinxSettingsStr.= ' & (@(field29) ("*' . $series . '*"))';
        if (isset($frame) && $frame != '')
            $arrSphinxSettingsStr.= ' & (@(field30) ("*' . $frame . '*"))';

        if (isset($roll_number) && $roll_number != '' && isset($series) && $series != '')
            $arrSphinxSettingsStr.= ' & (@(field31) ("*' . $series . '-' . $roll_number . '*"))';

        if (isset($roll_number) && $roll_number != '')
            $arrSphinxSettingsStr.= ' & (@(field31) ("*' . $roll_number . '*"))';

        if (!empty($placeOfBirth))
            $arrSphinxSettingsStr.= ' & (@(field26) ("*' . trim($placeOfBirth) . '*"))';


        /* 	
          if (isset($month_arrival) && $month_arrival != '')
          $arrSphinxSettingsStr.= ' & (@(field20) (*"' . $month_arrival . '"*))';
         */
        if (isset($month_arrival) && $month_arrival != '')
            $sphinxComponentObj->setFilter('field20', array($month_arrival));

        if (isset($day_arrival) && $day_arrival != '')
            $sphinxComponentObj->setFilter('field21', array($day_arrival));

        if (isset($birth_year_from) && $birth_year_from != '')
            $sphinxComponentObj->SetFilterRange('field12', $birth_year_from, $birth_year_to);
        if (isset($current_age_from) && $current_age_from != '')
            $sphinxComponentObj->SetFilterRange('field33', $current_age_from, $current_age_to);
        if (isset($arrival_age_from) && $arrival_age_from != '')
            $sphinxComponentObj->SetFilterRange('field5', $arrival_age_from, $arrival_age_to);

        if (isset($gender) && $gender != '')
            $arrSphinxSettingsStr.= ' & (@(field10) ("^' . $gender . '$"))';
        if (isset($maritial_status) && $maritial_status != '') {
            if ($maritial_status == 'U') {
                $maritalArray = array();
                $maritalArray[] = ' ("U") ';
                $maritalArray[] = ' (" ") ';
                $maritalStr = implode('|', $maritalArray);
                $arrSphinxSettingsStr.= ' & (@(field11)' . $maritalStr . ')';
            } else {
                $arrSphinxSettingsStr.= ' & (@(field11) ("^' . $maritial_status . '$"))';
            }
        }
        if (isset($dept_port) && $dept_port != '')
            $arrSphinxSettingsStr.= ' & (@(field24) ("*' . $dept_port . '*"))';
        if (isset($arrival_port) && $arrival_port != '')
            $arrSphinxSettingsStr.= ' & (@(field25) ("*' . $arrival_port . '*"))';
        if (isset($ethnicity) && $ethnicity != '') {
            $arrSphinxSettingsStr.= ' & (@(field36) ("^' . $ethnicity . '$"))';
        }
		//commented this code due to ticket 56
        /*if (isset($last_residence) && $last_residence != '')
            $arrSphinxSettingsStr.= ' & (@(field4) ("*' . $last_residence . '$"))';
			*/
			
        //ticket 56
        if (isset($last_residence) && $last_residence != '') {
            //$arrSphinxSettingsStr.= ' & (@(field4) ("*' . trim($last_residence) . '*"))';
            if ($searchTypeTown == 'exact') { //Exact Match
                $arrSphinxSettingsStr.= ' & (@(field4) (' . $last_residence . '))';
            } else if ($searchTypeTown == 'startwith') { //Start with
                $arrSphinxSettingsStr.= ' & (@(field4) ("^' . $last_residence . '*"))';
            } else if ($searchTypeTown == 'soundlike') { // sound like
                $arrSphinxSettingsStr.= ' & (@(field40) ("*' . soundex($last_residence) . '*"))';
            }
            else
                $arrSphinxSettingsStr.= ' & (@(field4) ("*' . $last_residence . '*"))';
        }
 
        $arrSphinxSettings['search'] = $arrSphinxSettingsStr;
        $exportzip = 0;

        if ($request->getPost('sidx') == '') {
            $sphinxComponentObj->SetSortMode(SPH_SORT_EXTENDED, "field3 ASC, field2 asc"); // Last name, First Name
        } else {
            $sphinxComponentObj->setSortMode(($request->getPost('sord') == 'desc') ? SPH_SORT_ATTR_DESC : SPH_SORT_ATTR_ASC, $request->getPost('sidx'));
        }
        // $sphinxComponentObj->setSortMode(SPH_SORT_ATTR_DESC, $request->getPost('sidx'));
        $page_counter = 1;

        do {
            $searchResult = $sphinxComponentObj->search($arrSphinxSettings);

            $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
            $dashletColumnName = array();
            if (!empty($dashletResult)) {
                $dashletColumn = explode(",", $dashletResult[0]['table_column_names']);
                foreach ($dashletColumn as $val) {
                    if (strpos($val, ".")) {
                        $dashletColumnName[] = trim(strstr($val, ".", false), ".");
                    } else {
                        $dashletColumnName[] = trim($val);
                    }
                }
            }

            if ($isExport == "excel") {
                $total = $searchResult['total'];
                if ($total > $export_limit) {
                    $totalExportedFiles = ceil($total / $export_limit);
                    $exportzip = 1;
                }
                if ($total > $chunksize) {
                    $number_of_pages = ceil($total / $chunksize);
                    $page_counter++;

                    $arrSphinxSettings['page'] = $page_counter;
                } else {
                    $number_of_pages = 1;
                    $page_counter++;
                }
            } else {
                $page_counter = $page;
                $number_of_pages = ceil($searchResult['total'] / $limit);
            }
            $jsonResult['page'] = $page;
            $jsonResult['records'] = $searchResult['total'];
            $jsonResult['total'] = ceil($searchResult['total'] / $limit);
            $valueArray = array();
            if ($searchResult['total'] > 0) {
                $arrExport = array();
                foreach ($searchResult['matches'] as $key => $val) {
                    $dashletCell = array();
                    $valueArray = $val['attrs'];
                    $arrCell['id'] = $valueArray['field0'];

                    if (false !== $dashletColumnKey = array_search('field0', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = '<a href="/view-crm-passenger/' . $this->encrypt($valueArray['field0']) . '/' . $this->encrypt('view') . '" class="txt-decoration-underline">' . $valueArray['field0'] . '</a>';

                    if (false !== $dashletColumnKey = array_search('field31', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $valueArray['field31'];

                    if (false !== $dashletColumnKey = array_search('field34', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = stripslashes($valueArray['field34']);

                    if (false !== $dashletColumnKey = array_search('field8', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = stripslashes($valueArray['field8']);
                    if (false !== $dashletColumnKey = array_search('field18', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $this->Dateformat($valueArray['field18'], 'displayformat');

                    if (false !== $dashletColumnKey = array_search('field16', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = stripslashes($valueArray['field16']);
                    if (false !== $dashletColumnKey = array_search('field30', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = stripslashes($valueArray['field30']);
                    if (false !== $dashletColumnKey = array_search('field32', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = stripslashes($valueArray['field32']);
                    if (false !== $dashletColumnKey = array_search('field10', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = stripslashes($valueArray['field10']);
                    if (false !== $dashletColumnKey = array_search('field5', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = stripslashes($valueArray['field5']);

                    $viewLabel = $this->_config['PassengerMessages']['config']['common']['VIEW'];
                    $editLabel = $this->_config['PassengerMessages']['config']['common']['EDIT'];
                    $addAnnotationLabel = $this->_config['PassengerMessages']['config']['passenger_search']['ADD_ANNOTATION_CORRECTION_FOR_PASSENGER'];
                    $view = "<a class='view-icon' href='/view-crm-passenger/" . $this->encrypt($valueArray['field0']) . "/" . $this->encrypt('view') . "'><div class='tooltip'>" . $viewLabel . "</div></a>";
                    $edit = "<a class='edit-icon' href='/edit-crm-passenger/" . $this->encrypt($valueArray['field0']) . "/" . $this->encrypt('edit') . "'><div class='tooltip'>" . $editLabel . "</div></a>";
                    $addAnnotation = "<a class='add-icon' href='/create-crm-annotation-correction/" . $this->encrypt($valueArray['field0']) . "/" . $this->encrypt('addNewAnnotationCorrection') . "'><div class='tooltip'>" . $addAnnotationLabel . "</div></a>";
                    $gender = '';
                    if ($valueArray['field10'] == 'M')
                        $gender = 'Male';
                    else if ($valueArray['field10'] == 'F')
                        $gender = 'Female';
                    else if ($valueArray['field10'] == 'U' || empty($valueArray['field10']))
                        $gender = 'Unknown';


                    $refPageLineNbr = '';
                    if (!empty($valueArray['field16']))
                        $refPageLineNbr = $valueArray['field16'];
                    if (isset($dashlet) && $dashlet == 'dashlet') {
                        $arrCell['cell'] = $dashletCell;
                        //$arrCell['cell'] = array($valueArray['field0'], $valueArray['field31'], $valueArray['field30'], $valueArray['field34'], $gender, $valueArray['field14'], $valueArray['field8'], $this->Dateformat($valueArray['field18'], 'displayformat'));
                    } else if ($isExport == "excel") {
                        $arrExport[] = array('Ref Line' => $refPageLineNbr, 'Passenger ID' => $valueArray['field0'], 'Series-Roll Number' => strtoupper($valueArray['field31']), 'Frame' => $valueArray['field30'], 'LastName' => $valueArray['field3'], 'FirstName' => $valueArray['field2'], 'Gender' => $gender, 'Age' => $valueArray['field14'], 'Ship' => $valueArray['field7'], 'Date of Arrival' => $this->Dateformat($valueArray['field18'], 'displayformat'));
                    } else {
                        $manifestImage = "<a href='javascript:void(0)' style='text-decoration:underline' onClick=showPassengerMoreManifestImage('" . trim($this->encrypt($valueArray['field31'])) . "','" . trim($this->encrypt($valueArray['field30'])) . "','" . trim($this->encrypt($valueArray['field0'])) . "');>" . strtoupper($valueArray['field32']) . "</a>";

                        $arrCell['cell'] = array($addAnnotation . ' ' . $refPageLineNbr, $valueArray['field0'], strtoupper($valueArray['field31']), $valueArray['field30'], $valueArray['field3'], $valueArray['field2'], $manifestImage, $gender, $valueArray['field14'], $valueArray['field7'], $this->Dateformat($valueArray['field18'], 'displayformat'), $view . $edit);
                    }

                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
                if ($isExport == "excel") {
                    $filename = "passenger_export_" . date("Y-m-d") . ".csv";
                    $this->arrayToCsv($arrExport, $filename, $page_counter);
                }
            }

            //if ($isExport == "excel") {
            // $filename = "passenger_export_" . date("Y-m-d") . ".csv";
            //$this->arrayToCsv($arrExport, $filename, $page_counter);
            //}
        } while ($page_counter <= $number_of_pages && $isExport == "excel");
        if ($isExport == "excel") {
            $this->downloadSendHeaders("passenger_export_" . date("Y-m-d") . ".csv");


            die();
        } else {
            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
        }

        //return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This Action is used to save/update the crm search
     * @param this will search_id to edit the saved search passenger
     * @return this will be comfirmation for the save/edit
     * @author Icreon Tech -SR
     */
    public function saveCrmSearchPassengerAction() {
        $this->checkUserAuthentication();

        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $document = new Document($this->_adapter);
        $messageArray = $this->_config['PassengerMessages']['config']['passenger_search'];
        if ($request->isPost()) {
            parse_str($request->getPost('searchString'), $searchParam);
            $searchName = $request->getPost('search_name');
            $messages = array();
            //parse_str($request->getPost('searchString'), $searchParam);
            $searchParam['search_title'] = $searchName;
            $searchParam = $document->getInputFilterCrmSearch($searchParam);

            $searchName = $document->getInputFilterCrmSearch($searchName);

            if (trim($searchName) != '') {
                $saveSearchParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $saveSearchParam['isActive'] = 1;
                $saveSearchParam['isDelete'] = 0;
                $saveSearchParam['search_name'] = $searchName;
                $saveSearchParam['search_query'] = serialize($searchParam);

                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modifiend_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['search_id'] = $request->getPost('search_id');

                    if ($this->getDocumentTable()->updateCrmPassengerSearch($saveSearchParam) == true) {
                        $this->flashMessenger()->addMessage($messageArray['PASSENGER_UPDATE_SEARCH_SUCCESS_MSG']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {
                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    $this->flashMessenger()->addMessage($messageArray['PASSENGER_SAVE_SEARCH_SUCCESS_MSG']);
                    if ($this->getDocumentTable()->saveCrmPassengerSearch($saveSearchParam) == true) {
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used to get the saved crm passenger search
     * @param search_id will be used to get the search parameters
     * @return this will be the saved search parameters
     * @author Icreon Tech - SR
     */
    public function getCrmSearchPassengerSavedParamAction() {
        $this->checkUserAuthentication();

        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $dataParam['isActive'] = '1';
                $dataParam['passengerSearchId'] = $request->getPost('search_id');
                $searchResult = $this->getDocumentTable()->getCrmSavedSearch($dataParam);
                $searchResult = json_encode(unserialize($searchResult[0]['search_query']));
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This Action is used to generte the select option after saving the new search title
     * @return this will be the listing of the saved search
     * @author Icreon Tech - SR
     */
    public function getSavedCrmSearchSelectAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();

        if ($request->isPost()) {
            $dataParam = array();
            $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $dataParam['isActive'] = '1';
            $dataParam['passengerSearchId'] = '';

            $crmSearchArray = $this->getDocumentTable()->getCrmSavedSearch($dataParam);
            $options = '';
            $options .="<option value=''>" . $this->_config['PassengerMessages']['config']['common']['SELECT'] . "</option>";
            foreach ($crmSearchArray as $key => $val) {
                $options .="<option value='" . $val['passenger_search_id'] . "'>" . $val['title'] . "</option>";
            }
            return $response->setContent($options);
        }
    }

    /**
     * This Action is used to delete the saved crm passenger search
     * @param search_Id which is used to delete the saved search 
     * @return this will be a confirmation of deletion
     * @author Icreon Tech - SR
     */
    public function deleteCrmSearchPassengerAction() {
        $this->checkUserAuthentication();

        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['search_id'] = $request->getPost('search_id');
            $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $dataParam['isDelete'] = 1;
            $dataParam['modifiend_date'] = DATE_TIME_FORMAT;
            $this->getDocumentTable()->deleteCrmPassengerSearch($dataParam);
            $this->flashMessenger()->addMessage($this->_config['PassengerMessages']['config']['common']['SAVED_SEARCH_DELETE_MSG']);
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This Action is used to to view the passenger details
     * @param this will id of the passenger
     * @return this will be the passenger details
     * @author Icreon Tech - SR
     */
    public function viewCrmPassengerAction() {
        $this->checkUserAuthentication();

        $this->layout('crm');
        $this->getDocumentTable();
        $params = $this->params()->fromRoute();
        $searchParam['passenger_id'] = $this->decrypt($params['id']);
        $searchResult = $this->getDocumentTable()->searchCrmPassenger($searchParam);
        $this->getDocumentTable()->searchCrmAnnotationCorrection($searchParam);
        $annotationCountResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $annotationCount = 0;
        $annotationCount = $annotationCountResult[0]->RecordCount;

        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'passenger_id' => $params['id'],
            'mode' => $this->decrypt($params['mode']),
            'moduleName' => $this->encrypt('passenger'),
            'searchResult' => $searchResult[0],
            'FILENAME' => $this->encrypt($searchResult[0]['FILENAME']),
            'jsLangTranslate' => array_merge($this->_config['PassengerMessages']['config']['edit_passenger'], $this->_config['PassengerMessages']['config']['common']),
            'annotationCount' => $annotationCount
        ));
        return $viewModel;
    }

    /**
     * This Action is used to to view the passenger details in tabbing
     * @param this will id of the passenger
     * @return this will be the passenger details
     * @author Icreon Tech - SR
     */
    public function viewCrmPassengerDetailsAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $params = $this->params()->fromRoute();
        $searchParam['passenger_id'] = $this->decrypt($params['id']);

        $searchResult = $this->getDocumentTable()->searchCrmPassenger($searchParam);

        $passParam = array();
        $passengerEthnicity = array();
        $passParam['passengerId'] = $this->decrypt($params['id']);
        $passengerEthnicity = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getPassengerEthnicity($passParam);
        $document = new Document($this->_adapter);
        $searchShipParam['ship_name'] = addslashes($searchResult[0]['SHIP_NAME']);
        $searchShipParam = $document->assignData($searchShipParam);
        $searchShipParam['startIndex'] = '';
        $searchShipParam['recordLimit'] = '';
        $searchShipParam['groupby'] = '';
        $searchShipParam['sortField'] = '';
        $searchShipParam['sortOrder'] = '';
        $searchShipParam['getRecordsCount'] = '';
//echo "aaaa";
        $searchShipResult = $this->getDocumentTable()->searchCrmShip($searchShipParam);
        //asd($searchShipResult);

        $shipId = $this->encrypt($searchShipResult[0]['SHIPID']);

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'passenger_id' => $params['id'],
            'moduleName' => $this->encrypt('passenger'),
            'searchResult' => $searchResult[0],
            'FILENAME' => $this->encrypt($searchResult[0]['FILENAME']),
            'ship_id' => $shipId,
            'mode' => $this->encrypt('view'),
            'jsLangTranslate' => $this->_config['PassengerMessages']['config']['edit_passenger'],
            'passengerEthnicity' => $passengerEthnicity,
            'arrival_date' => $this->encrypt($searchResult[0]['DATE_ARRIVE']),
            'ship_name' => $this->encrypt($searchResult[0]['SHIP_NAME'])
        ));
        return $viewModel;
    }

    /**
     * This Action is used to to edit the passenger details
     * @param this will pass passenger_id
     * @return this will be the confirmation
     * @author Icreon Tech - SR
     */
    public function editPassengerAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getDocumentTable();
        $edit_passenger_form = new EditPassengerForm();
        $request = $this->getRequest();
        $document = new Document($this->_adapter);
        $params = $this->params()->fromRoute();
        $response = $this->getResponse();

        if ($request->isPost()) {
            //sssssssssssssssssssssss
            // $edit_passenger_form->get('ship_arrival_date')->setAttribute('value', $this->DateFormat($request->getPost('ship_arrival_date'),'db_date_format'));

            $edit_passenger_form->setInputFilter($document->getInputFilter());
            $edit_passenger_form->setData($request->getPost());

            if (!$edit_passenger_form->isValid()) {

                $msg = array();
                $errors = $edit_passenger_form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit' && $key != 'ship_arrival_date') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['PassengerMessages']['config']['edit_passenger'][$rower];
                        }
                    }
                }
                if (!empty($msg))
                    $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                //asd($edit_passenger_form->getData());
                $document->exchangeArray($edit_passenger_form->getData());
                $postData = array();
                $postData['batch'] = $document->batch;
                $postData['ref_line'] = sprintf('%04d', $document->ref_line);
                $postData['ref_page'] = $document->ref_page;
                //$postData['arrival_date'] = date("Y-m-d H:i:s", strtotime($document->arrival_date));
                if ($document->arrival_date != '' && $document->arrival_date != 'Select')
                    $postData['arrival_date'] = $this->DateFormat($document->arrival_date, 'db_date_format');
                else
                    $postData['arrival_date'] = '';

                $postData['last_name'] = $document->last_name;
                $postData['first_name'] = $document->first_name;
                $postData['birth_year'] = $document->birth_year;
                $postData['gender'] = $document->gender;
                $postData['maritial_status'] = $document->maritial_status;
                $postData['residence'] = $document->residence;
                $postData['ship'] = $document->ship;
                $postData['ship_name_iss'] = $document->ship_name_iss;

                if ($document->ship_arrival_date != '' and $document->ship_arrival_date != 'Select')
                    $postData['ship_arrival_date'] = $this->DateFormat($document->ship_arrival_date, 'datetomonthformat');
                else
                    $postData['ship_arrival_date'] = '';

                $postData['passenger_id'] = $document->passenger_id;
                $postData['nationality'] = $document->nationality;
                if ($document->us_citizen == 1)
                    $postData['us_citizen'] = 'X';
                else
                    $postData['us_citizen'] = '';
                if ($document->crew_member == 1)
                    $postData['crew_member'] = 'X';
                else
                    $postData['crew_member'] = '';

                $postData['print_age'] = $document->print_age;
                $postData['age_arrival'] = $document->age_arrival;
                $postData['soundex_lname'] = soundex($postData['last_name']);
                $postData['place_of_birth'] = $document->place_of_birth;
                $postData['port_of_departure'] = $document->port_of_departure;

                if (!empty($document->file_name)) { // update file name
                    $extn = substr($document->file_name, strrpos($document->file_name, '.') + 1);
                    if (strtolower($extn) == 'jpg' || strtolower($extn) == 'jpeg') {
                        $postData['FILENAME'] = substr($document->roll_number, 5) . '_' . sprintf('%05d', $document->frame) . '.jpg';
                    } else {
                        $postData['FILENAME'] = $document->roll_number . sprintf('%04d', $document->frame) . '.TIF';
                    }
                } else { // insert file name
                    if ($request->getPost('data_source') == 1)
                        $postData['FILENAME'] = $document->roll_number . sprintf('%04d', $document->frame) . '.TIF';
                    else if ($request->getPost('data_source') == 2)
                        $postData['FILENAME'] = substr($document->roll_number, 5) . '_' . sprintf('%05d', $document->frame) . '.jpg';
                }
                // asd($postData);
                // die;
                $passSearchParam = array();
                $updateMainImageData = array();
                $passSearchParam['manifestImage'] = $postData['FILENAME'];
                $updateManiImageFlag = 0;
                
                $passengerManifestResult = $this->getDocumentTable()->getManifestDetails($passSearchParam);
                if (count($passengerManifestResult) > 0) {
                    $imageAutoId = $passengerManifestResult[0]['AUTOID'];
                    $postData['MANIMAGE_AUTOID'] = $imageAutoId;
                    $updateMainImageData['mainimage_autoid'] = $imageAutoId;
                    $updateManiImageFlag = 1;
                } else {
                    $postMainImageData = array();
                    $postMainImageData['file_name'] = $postData['FILENAME'];
                    $postMainImageData['roll_number'] = $document->roll_number;
                    $postMainImageData['frame'] = $document->frame;

                    $postMainImageData['arrival_date'] = $postData['arrival_date'] . " 00:00:00";
                    $postMainImageData['page_nbr'] = $document->ref_page;
                    $postMainImageData['ship_name'] = $document->ship;
                    $postMainImageData['data_source'] = $request->getPost('data_source');
                    $manimageAutoId = $this->getDocumentTable()->insertCrmPassengerMainImage($postMainImageData);
                    $postData['MANIMAGE_AUTOID'] = $manimageAutoId;
                   $updateMainImageData['mainimage_autoid'] = $manimageAutoId;
                }

                $this->getDocumentTable()->updateCrmPassenger($postData);
                
                
                $updateMainImageData['roll_number'] = $document->roll_number;
                $updateMainImageData['frame'] = $document->frame;
                $this->getDocumentTable()->updateCrmPassengerMainImage($updateMainImageData);

                // code used to update the ethnicity
                $ethnicityData = array();
                $ethnicityData['passenger_id'] = $document->passenger_id;
                $ethnicityData['ethnicity'] = $document->ethnicity;
                $ethnicityData['nationality'] = $document->nationality;
                $this->getDocumentTable()->updateCrmPassengerEthnicity($ethnicityData);

                /** insert into the change log */
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_pas_change_logs';
                $changeLogArray['activity'] = '2';/** 2 == for update */
                $changeLogArray['id'] = $document->passenger_id;
                $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                /** end insert into the change log */
                $this->flashMessenger()->addMessage($this->_config['PassengerMessages']['config']['edit_passenger']['UPDATE_CONFIRM_MSG']);

                $messages = array('status' => "success", 'message' => $this->_config['PassengerMessages']['config']['edit_passenger']['UPDATE_CONFIRM_MSG']);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel = new ViewModel();
        //$this->setValueForElement($passengerResult[0],$edit_passenger_form);

        $viewModel->setVariables(array(
            'edit_passeneger_form' => $edit_passenger_form,
            'ID' => $passengerResult[0]['ID'],
            'AGE_ARRIVE' => $passengerResult[0]['AGE_ARRIVE'],
            'PRINT_AGE' => $passengerResult[0]['PRIN_AGE_ARRIVAL'],
            'batch' => $passengerResult[0]['UDE_BATCH_NUMBER'],
            'jsLangTranslate' => $this->_config['PassengerMessages']['config']['edit_passenger'],
            'passenger_id' => $params['id'],
            'moduleName' => $this->encrypt('passenger')
        ));
        return $viewModel;
    }

    /**
     * This Action is used to to view the crm annotation search page
     * @return this will be the annotation correction form
     * @author Icreon Tech - SR
     */
    public function crmAnnotationCorrectionAction() {
        $this->checkUserAuthentication();

        $this->layout('crm');
        $this->getDocumentTable();
        $request = $this->getRequest();
        $search_annotation_list_form = new SearchAnnotationListForm();
        $save_search_annotation_form = new SaveSearchAnnotationForm();

        $getTransactionRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
        $transactionRange = array("" => "Select One");
        foreach ($getTransactionRange as $key => $val) {
            $transactionRange[$val['range_id']] = $val['range'];
        }
        $search_annotation_list_form->get('requested_on')->setAttribute('options', $transactionRange);

        $getSearchArray = array();
        $getSearchArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
        $getSearchArray['isActive'] = '1';
        $getSearchArray['searchId'] = '';

        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }

        $params = $this->params()->fromRoute();
        if (isset($params['new_annotation']) && $params['new_annotation'] != '') {
            $this->flashMessenger()->addMessage($params['new_annotation']);
        }
        $crmSearchArray = $this->getDocumentTable()->getCrmAnnotationSavedSearch($getSearchArray);

        $crmSearchList = array();
        $crmSearchList[''] = 'Select';
        foreach ($crmSearchArray as $key => $val) {
            $crmSearchList[$val['annotation_correction_search_id']] = stripslashes($val['title']);
        }
        $search_annotation_list_form->get('saved_search')->setAttribute('options', $crmSearchList);

        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $response = $this->getResponse();


        $viewModel->setVariables(array(
            'search_annotation_list_form' => $search_annotation_list_form,
            'save_search_annotation_form' => $save_search_annotation_form,
            'messages' => $messages,
            'jsLangTranslate' => array_merge($this->_config['PassengerMessages']['config']['annotations'], $this->_config['PassengerMessages']['config']['common'])
        ));
        return $viewModel;
    }

    /**
     * This Action is used to save the crm annotation search
     * @return this will save the annotation search and will reqtun the confirmation
     * @author Icreon Tech - SR
     */
    public function saveCrmAnnotationSearchAction() {
        $this->checkUserAuthentication();

        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $document = new Document($this->_adapter);
        if ($request->isPost()) {
            parse_str($request->getPost('searchString'), $searchParam);
            $searchName = $request->getPost('search_name');
            $messages = array();
            // parse_str($request->getPost('searchString'), $searchParam);
            $searchParam['search_title'] = $searchName;
            $searchParam = $document->getInputFilterCrmSearch($searchParam);
            $searchName = $document->getInputFilterCrmSearch($searchName);

            if (trim($searchName) != '') {
                $saveSearchParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $saveSearchParam['isActive'] = 1;
                $saveSearchParam['isDelete'] = 0;
                $saveSearchParam['search_name'] = $searchName;
                $saveSearchParam['search_query'] = serialize($searchParam);

                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modifiend_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['search_id'] = $request->getPost('search_id');
                    if ($this->getDocumentTable()->updateCrmAnnotationSearch($saveSearchParam) == true) {
                        $this->flashMessenger()->addMessage('Annotation & Correction search details updated successfully!');
                        $messages = array('status' => "success", 'message' => 'Search details updated successfully!');
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {
                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    if ($this->getDocumentTable()->saveCrmAnnotationSearch($saveSearchParam) == true) {
                        $this->flashMessenger()->addMessage('Annotation & Correction search details saved successfully!');
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used to generte the select option after saving the new search title
     * @return this will return the saved search listing
     * @author Icreon Tech - SR
     */
    public function getSavedCrmAnnotationSearchSelectAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam = array();
            $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $dataParam['isActive'] = '1';
            $dataParam['passengerSearchId'] = '';

            $crmSearchArray = $this->getDocumentTable()->getCrmAnnotationSavedSearch($dataParam);
            $options = '';
            $options .="<option value=''>" . $this->_config['PassengerMessages']['config']['common']['SELECT'] . "</option>";
            foreach ($crmSearchArray as $key => $val) {
                $options .="<option value='" . $val['annotation_correction_search_id'] . "'>" . $val['title'] . "</option>";
            }
            return $response->setContent($options);
        }
    }

    /**
     * This Action is used to get the saved crm annotation search
     * @param this will be the search_id
     * @return this will return the saved search details
     * @author Icreon Tech - SR
     */
    public function getCrmSearchAnnotationSavedParamAction() {
        $this->checkUserAuthentication();

        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $dataParam['isActive'] = '1';
                $dataParam['searchId'] = $request->getPost('search_id');
                $searchResult = $this->getDocumentTable()->getCrmAnnotationSavedSearch($dataParam);
                $searchResult = json_encode(unserialize($searchResult[0]['search_query']));
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This Action is used to delete the the saved crm passenger search
     * @param this will search_id
     * @return this will be a confirmation of deletion
     * @author Icreon Tech - SR
     */
    public function deleteCrmSearchAnnotationAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['search_id'] = $request->getPost('search_id');
            $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $dataParam['isDelete'] = 1;
            $dataParam['modifiend_date'] = DATE_TIME_FORMAT;
            $this->getDocumentTable()->deleteCrmAnnotationSearch($dataParam);
            $this->flashMessenger()->addMessage('Search is deleted successfully!');
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This Action is used for the detauls crm ship search
     * @return search parametars
     * @author Icreon Tech - SR
     */
    public function crmShipAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();

        $search_ship_form = new SearchShipForm();
        $save_search_ship_form = new SaveSearchShipForm();

        $getSearchArray = array();
        $engineArray = $this->getDocumentTable()->getEngine($getSearchArray);
        $engineList = array();
        $engineList[''] = 'Select';
        foreach ($engineArray as $key => $val) {
            $engineList[$val['engine_type_id']] = stripslashes($val['engine_type']);
        }
        $search_ship_form->get('engine_type')->setAttribute('options', $engineList);


        $day = array();
        $day[''] = 'All';
        for ($i = 1; $i <= 31; $i++) {
            $day[$i] = $i;
        }
        $search_ship_form->get('day_arrival')->setAttribute('options', $day);
        $arrivalYear = array();
        $arrivalYear[''] = 'Select';
        $arrivalYear['1891 - 1895'] = '1891 - 1895';
        for ($k = 1896; $k < 1930;) {
            $p = $k + 4;
            $arrivalYear[$k . ' - ' . $p] = $k . ' - ' . $p;
            $k = $k + 5;
        }
        $search_ship_form->get('arrival_year')->setAttribute('options', $arrivalYear);
        $getTransactionRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
        $transactionRange = array("" => "Select One");
        foreach ($getTransactionRange as $key => $val) {
            $transactionRange[$val['range_id']] = $val['range'];
        }
        $search_ship_form->get('build_year')->setAttribute('options', $transactionRange);
        $search_ship_form->get('scrap_year')->setAttribute('options', $transactionRange);
        $getSearchArray = array();
        $getSearchArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
        $getSearchArray['isActive'] = '1';
        $getSearchArray['searchId'] = '';
        $crmSearchArray = $this->getDocumentTable()->getCrmShipSavedSearch($getSearchArray);
        $crmSearchList = array();
        $crmSearchList[''] = 'Select';
        foreach ($crmSearchArray as $key => $val) {
            $crmSearchList[$val['ship_search_id']] = stripslashes($val['title']);
        }

        $search_ship_form->get('saved_search')->setAttribute('options', $crmSearchList);
        $viewModel = new ViewModel();

        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }

        $viewModel->setVariables(array(
            'search_ship_form' => $search_ship_form,
            'save_search_ship_form' => $save_search_ship_form,
            'messages' => $messages,
            'jsLangTranslate' => array_merge($this->_config['PassengerMessages']['config']['ship_search'], $this->_config['PassengerMessages']['config']['edit_ship'], $this->_config['PassengerMessages']['config']['common'])
        ));
        return $viewModel;
    }

    /**
     * This Action is used for the search page of crm search ship
     * @return this will be ship listing
     * @author Icreon Tech -SR
     */
    public function getCrmSearchShipAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();

        $sphinxServer = $this->_config['SphinxServer'];
        $sphinxComponentObj = new SphinxComponent($sphinxServer['serverName']);
        $sphinxComponentObj->initialize();
        $arrSphinxIndexes = array('ship');
        parse_str($request->getPost('searchString'), $searchParam);

        $dashlet = $request->getPost('crm_dashlet');
        $dashletSearchId = $request->getPost('searchId');
        $dashletId = $request->getPost('dashletId');
        $dashletSearchType = $request->getPost('searchType');
        $dashletSearchString = $request->getPost('dashletSearchString');
        if (isset($dashletSearchString) && $dashletSearchString != '') {
            $searchParam = unserialize($dashletSearchString);
        }
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $isExport = $request->getPost('export');
        if ($isExport == "" || $isExport != "excel") {
            $arrSphinxSettings = array('limit' => (int) $limit,
                'index' => $arrSphinxIndexes,
                'page' => (int) $request->getPost('page'),
                //'groupby'        =>  array(array("sphinx_group_by" , '@weight DESC')),
                'groupby' => array(array("sphinx_group_by", $request->getPost('sidx') . ' ' . $request->getPost('sord'))),
                'rankingMode' => SPH_RANK_PROXIMITY_BM25, // SPH_RANK_PROXIMITY_BM25,
                'matchMode' => SPH_MATCH_EXTENDED2, //SPH_MATCH_EXTENDED2, //SPH_MATCH_BOOLEAN, //SPH_MATCH_EXTENDED2,
                'filter' => array()
            );
        } else {
            $chunksize = $this->_config['export']['chunk_size'];
            $export_limit = $this->_config['export']['export_limit'];
            $arrSphinxSettings = array('limit' => (int) $chunksize,
                'index' => $arrSphinxIndexes,
                'page' => (int) 1,
                //'groupby'        =>  array(array("sphinx_group_by" , '@weight DESC')),
                'groupby' => array(array("sphinx_group_by", $request->getPost('sidx') . ' ' . $request->getPost('sord'))),
                'rankingMode' => SPH_RANK_PROXIMITY_BM25, // SPH_RANK_PROXIMITY_BM25,
                'matchMode' => SPH_MATCH_EXTENDED2, //SPH_MATCH_EXTENDED2, //SPH_MATCH_BOOLEAN, //SPH_MATCH_EXTENDED2,
                'filter' => array()
            );
            // asd($arrSphinxSettings);
        }
        $sphinxComponentObj->SetFieldWeights(array('field0' => 10000,
            'field6' => 5000,
            'field23' => 100,
            'field3' => 100,
            'field18' => 100,
            'field21' => 60,
            'field20' => 50,
            'field8' => 100,
            'field5' => 5000,
            'field33' => 200,
            'field3' => 100,
        ));
        $sphinxComponentObj->SetIndexWeights(array('ship' => 10000));
        $document = new Document($this->_adapter);
        if (isset($searchParam['build_year']) && $searchParam['build_year'] != '' && $searchParam['build_year'] != 1) {
            $buildYearArray = $this->getDateRange($searchParam['build_year']);
            $searchParam['build_from_date'] = substr($buildYearArray['from'], -4);
            $searchParam['build_to_date'] = substr($buildYearArray['to'], -4);
        } else if (isset($searchParam['build_year']) && $searchParam['build_year'] != '' && $searchParam['build_year'] == 1) {
            if ($searchParam['build_from_date'] != '') {
                $searchParam['build_from_date'] = substr($searchParam['build_from_date'], -4);
            } else {
                $searchParam['build_from_date'] = '1600';
            }
            if ($searchParam['build_to_date'] != '') {
                $searchParam['build_to_date'] = substr($searchParam['build_to_date'], -4);
            } else {
                $searchParam['build_to_date'] = date("Y"); // if build to year is blank the it will be till current year
            }
        } else {
            $searchParam['build_from_date'] = '';
            $searchParam['build_to_date'] = '';
        }

        if (isset($searchParam['scrap_year']) && $searchParam['scrap_year'] != '' && $searchParam['scrap_year'] != 1) {
            $scrapYearArray = $this->getDateRange($searchParam['scrap_year']);
            $searchParam['scrap_from_date'] = substr($scrapYearArray['from'], -4);
            $searchParam['scrap_to_date'] = substr($scrapYearArray['to'], -4);
        } else if (isset($searchParam['scrap_year']) && $searchParam['scrap_year'] != '' && $searchParam['scrap_year'] == 1) {
            if ($searchParam['scrap_from_date'] != '') {
                $searchParam['scrap_from_date'] = substr($searchParam['scrap_from_date'], -4);
            } else {
                $searchParam['scrap_from_date'] = '1600';
            }
            if ($searchParam['scrap_to_date'] != '') {
                $searchParam['scrap_to_date'] = substr($searchParam['scrap_to_date'], -4);
            } else {
                $searchParam['scrap_to_date'] = date("Y"); // if build to year is blank the it will be till current year
            }
        } else {
            $searchParam['scrap_from_date'] = '';
            $searchParam['scrap_to_date'] = '';
        }

        $arrival_year_array = isset($searchParam['arrival_year']) ? explode(' - ', $searchParam['arrival_year']) : '';
        if (isset($searchParam['arrival_year']) && $searchParam['arrival_year'] != '') {
            $arrival_year_from = $arrival_year_array[0];
            $arrival_year_to = $arrival_year_array[1];
        } else {
            $arrival_year_from = '';
            $arrival_year_to = '';
        }
        $arrSphinxSettingsStr = '';
        $build_from_date = $searchParam['build_from_date'];
        $build_to_date = $searchParam['build_to_date'];
        $scrap_from_date = $searchParam['scrap_from_date'];
        $scrap_to_date = $searchParam['scrap_to_date'];
        if (isset($arrival_year_from) && $arrival_year_from != '')
            $sphinxComponentObj->SetFilterRange('field19', $arrival_year_from, $arrival_year_to);

        if ($build_from_date != '' && $build_to_date != '')
            $sphinxComponentObj->SetFilterRange('field5', $build_from_date, $build_to_date);

        if ($scrap_from_date != '' && $scrap_to_date != '')
            $sphinxComponentObj->SetFilterRange('field33', $scrap_from_date, $scrap_to_date);

        $ship_id = isset($searchParam['ship_id']) ? trim($searchParam['ship_id']) : '';
        $ship_name = isset($searchParam['ship_name']) ? trim($searchParam['ship_name']) : '';
        $engine_type = isset($searchParam['engine_type']) ? $searchParam['engine_type'] : '';
        $builder = isset($searchParam['builder']) ? trim($searchParam['builder']) : '';
        $month_arrival = isset($searchParam['month_arrival']) ? $searchParam['month_arrival'] : '';
        $day_arrival = isset($searchParam['day_arrival']) ? $searchParam['day_arrival'] : '';
        $shipline_name = isset($searchParam['shipline_name']) ? $searchParam['shipline_name'] : '';

        if ($ship_id != '')
            $arrSphinxSettingsStr.= ' & (@(field0) ("^' . $ship_id . '$"))';
        if ($ship_name != '')
            $arrSphinxSettingsStr.= ' & (@(field6) ("*' . $ship_name . '*"))';
        if ($engine_type != '')
            $arrSphinxSettingsStr.= ' & (@(field23) ("*' . $engine_type . '*"))';
        if ($builder != '')
            $arrSphinxSettingsStr.= ' & (@(field3) ("*' . $builder . '*"))';
        if ($month_arrival != '')
            $sphinxComponentObj->setFilter('field20', array($month_arrival));
        if ($day_arrival != '')
            $sphinxComponentObj->setFilter('field21', array($day_arrival));
        if ($shipline_name != '')
            $arrSphinxSettingsStr.= ' & (@(field8) ("*' . $shipline_name . '*"))';

        $arrSphinxSettings['search'] = $arrSphinxSettingsStr;
        $page_counter = 1;
        do {
            $searchResult = $sphinxComponentObj->search($arrSphinxSettings);
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();

            $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
            $dashletColumnName = array();
            if (!empty($dashletResult)) {
                $dashletColumn = explode(",", $dashletResult[0]['table_column_names']);
                foreach ($dashletColumn as $val) {
                    if (strpos($val, ".")) {
                        $dashletColumnName[] = trim(strstr($val, ".", false), ".");
                    } else {
                        $dashletColumnName[] = trim($val);
                    }
                }
            }
            if ($isExport == "excel") {
                $total = $searchResult['total'];
                if ($total > $chunksize) {

                    $number_of_pages = ceil($total / $chunksize);
                    //$number_of_pages .'--'.$page_counter;
                    $page_counter++;

                    $arrSphinxSettings['page'] = $page_counter;
                } else {
                    $number_of_pages = 1;
                    $page_counter++;
                }
            } else {
                $page_counter = $page;
                $number_of_pages = ceil($searchResult['total'] / $limit);
                $jsonResult['page'] = $page;
                $jsonResult['records'] = $searchResult['total'];
                $jsonResult['total'] = ceil($searchResult['total'] / $limit);
            }
            $valueArray = array();
            if ($searchResult['total'] > 0) {
                $arrExport = array();
                foreach ($searchResult['matches'] as $key => $val) {
                    $dashletCell = array();
                    $valueArray = $val['attrs'];
                    $isEdited = 'No';
                    if ($valueArray['field29'] == 0 || $valueArray['field29'] == '')
                        $isEdited = 'No';
                    else if ($valueArray['field29'] == 1)
                        $isEdited = 'Yes';
                    if (false !== $dashletColumnKey = array_search('field0', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = '<a class="txt-decoration-underline" href="/view-crm-ship/' . $this->encrypt($valueArray['field0']) . '/' . $this->encrypt('view') . '">' . $valueArray['field0'] . '</a>';
                    if (false !== $dashletColumnKey = array_search('field2', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = stripslashes($valueArray['field2']);
                    if (false !== $dashletColumnKey = array_search('field5', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = stripslashes($valueArray['field5']);
                    if (false !== $dashletColumnKey = array_search('field33', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $valueArray['field33'];
                    if (false !== $dashletColumnKey = array_search('field3', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $valueArray['field3'];
                    if (false !== $dashletColumnKey = array_search('field6', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $valueArray['field6'];
                    if (false !== $dashletColumnKey = array_search('field29', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $isEdited;

                    $arrCell['id'] = $valueArray['field0'];
                    $viewLabel = $this->_config['PassengerMessages']['config']['common']['VIEW'];
                    $editLabel = $this->_config['PassengerMessages']['config']['common']['EDIT'];
                    $view = "<a class='view-icon' href='/view-crm-ship/" . $this->encrypt($valueArray['field0']) . "/" . $this->encrypt('view') . "/" . $this->encrypt($valueArray['field6']) . "'><div class='tooltip'>" . $viewLabel . "<span></span></div></a>";
                    $edit = "<a class='edit-icon' href='/edit-crm-ship/" . $this->encrypt($valueArray['field0']) . "/" . $this->encrypt('edit') . "'><div class='tooltip'>" . $editLabel . "<span></span></div></a>";
                    if (isset($dashlet) && $dashlet == 'dashlet') {
                        $arrCell['cell'] = $dashletCell;
                    } else if ($isExport == "excel") {
                        $arrExport[] = array('Ship Id' => $valueArray['field0'], 'Name When Built' => stripslashes($valueArray['field2']), 'Build Year' => stripslashes($valueArray['field5']), 'Scrapped Year' => $valueArray['field33'], 'Builder' => stripslashes($valueArray['field3']), 'Ship Name' => stripslashes($valueArray['field6']), 'Is Image Edited' => $isEdited);
                    } else {
                        $arrCell['cell'] = array($valueArray['field0'], stripslashes($valueArray['field2']), stripslashes($valueArray['field5']), $valueArray['field33'], stripslashes($valueArray['field3']), stripslashes($valueArray['field6']), $isEdited, $view . $edit);
                    }
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
                if ($isExport == "excel") {
                    $filename = "ship_export_" . date("Y-m-d") . ".csv";
                    $this->arrayToCsv($arrExport, $filename, $page_counter);
                }
            }
        } while ($page_counter <= $number_of_pages && $isExport == "excel");
        if ($isExport == "excel") {
            $this->downloadSendHeaders("ship_export_" . date("Y-m-d") . ".csv");

            die();
        } else {
            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
        }
        // return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This Action is used to save the crm annotation search
     * @param search id to edit the saved saerch
     * @return this will return the confirmation of edit the saved search
     * @author Icreon Tech - SR
     */
    public function saveCrmShipSearchAction() {
        $this->checkUserAuthentication();

        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $shipMessage = $this->_config['PassengerMessages']['config']['ship_search'];
        $messages = array();
        $document = new Document($this->_adapter);
        if ($request->isPost()) {
            parse_str($request->getPost('searchString'), $searchParam);
            $searchName = $request->getPost('search_name');
            $messages = array();
            // parse_str($request->getPost('searchString'), $searchParam);
            $searchParam['search_title'] = $searchName;
            $searchParam = $document->getInputFilterCrmSearch($searchParam);
            $searchName = $document->getInputFilterCrmSearch($searchName);

            if (trim($searchName) != '') {
                $saveSearchParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $saveSearchParam['isActive'] = 1;
                $saveSearchParam['isDelete'] = 0;
                $saveSearchParam['search_name'] = $searchName;
                $saveSearchParam['search_query'] = serialize($searchParam);

                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modifiend_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['search_id'] = $request->getPost('search_id');
                    if ($this->getDocumentTable()->updateCrmShipSearch($saveSearchParam) == true) {

                        $this->flashMessenger()->addMessage($shipMessage['SHIP_UPDATE_SEARCH_SUCCESS_MSG']);
                        $messages = array('status' => "success", 'message' => $shipMessage['SHIP_UPDATE_SEARCH_SUCCESS_MSG']);
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {
                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    if ($this->getDocumentTable()->saveCrmShipSearch($saveSearchParam) == true) {
                        $this->flashMessenger()->addMessage($shipMessage['SHIP_SAVE_SEARCH_SUCCESS_MSG']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used to generte the select option after saving the new search title
     * @return this will return all the saved search listing
     * @author Icreon Tech - SR
     */
    public function getSavedCrmShipSearchSelectAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam = array();
            $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $dataParam['isActive'] = '1';
            $dataParam['shipSearchId'] = '';

            $crmSearchArray = $this->getDocumentTable()->getCrmShipSavedSearch($dataParam);
            $options = '';
            $options .="<option value=''>" . $this->_config['PassengerMessages']['config']['common']['SELECT'] . "</option>";
            foreach ($crmSearchArray as $key => $val) {
                $options .="<option value='" . $val['ship_search_id'] . "'>" . $val['title'] . "</option>";
            }
            return $response->setContent($options);
        }
    }

    /**
     * This Action is used to get the saved crm ship search
     * @param search_id to get the saved search details
     * @return this will be the saved search values
     * @author Icreon Tech - SR
     */
    public function getCrmSearchShipSavedParamAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $dataParam['isActive'] = '1';
                $dataParam['searchId'] = $request->getPost('search_id');
                $searchResult = $this->getDocumentTable()->getCrmShipSavedSearch($dataParam);
                $searchResult = json_encode(unserialize($searchResult[0]['search_query']));
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This Action is used to get the view crm ship
     * @param id to get the ship details
     * @return this will be the ship details
     * @author Icreon Tech - SR
     */
    public function viewCrmShipAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getDocumentTable();
        $params = $this->params()->fromRoute();

        $searchParam['ship_id'] = $this->decrypt($params['id']);
        // if (isset($params['ship_name']) && $params['ship_name'] != '')
        //     $searchParam['ship_name'] = $this->decrypt($params['ship_name']);
        $document = new Document($this->_adapter);
        //$searchParam = $document->assignData($searchParam);
        //  asd($searchParam,2);//aaaaaaaaaaaaaaa
        $searchResult = $this->getDocumentTable()->searchCrmShip($searchParam);
        $imageCount = 0;
        $imageSearchParam = array();
        $imageSearchParam['ship_id'] = $this->decrypt($params['id']);
        $imageSearchParam['asset_for'] = '1';
        $searchShipImageResult = $this->getDocumentTable()->searchCrmShipImage($imageSearchParam);
        $imageCount = count($searchShipImageResult);
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'ship_id' => $params['id'],
            'mode' => $this->decrypt($params['mode']),
            'id' => $params['id'],
            'shipname' => $this->encrypt($searchResult[0]['SHIPNAME']),
            // 'shiplineId' => $params['shiplineId'],
            'moduleName' => $this->encrypt('ship'),
            'searchResult' => $searchResult[0],
            'imageCount' => $imageCount,
            'jsLangTranslate' => array_merge($this->_config['PassengerMessages']['config']['ship_search'], $this->_config['PassengerMessages']['config']['edit_ship'], $this->_config['PassengerMessages']['config']['common'])
        ));
        return $viewModel;
    }

    /**
     * This Action is used to get crm ship trip list
     * @param ship_id to get his trips listing
     * @return this will return the ships trip listing
     * @author Icreon Tech - SR
     */
    public function getCrmShipTripListAction() {

        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $searchParam['ship_id'] = $this->decrypt($request->getPost('ship_id'));
        $searchParam['shiplineId'] = $this->decrypt($request->getPost('shiplineId'));
        $searchParam['shipName'] = addslashes($this->decrypt($request->getPost('ship_name')));
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $searchResult = $this->getDocumentTable()->getShipTripDetails($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();

        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);

        if (count($searchResult) > 0) {
            foreach ($searchResult as $val) {
                $arrCell['id'] = $val['ID'];
                $searchShipDetailParam = array();
                $searchShipDetailParam['ship_name'] = $val['SHIP_NAME'];
                $searchShipDetailParam['DATE_ARRIVE'] = substr($val['DATE_ARRIVE'], 0, 10);
                $passCountResult = $this->getDocumentTable()->getShipPassengerListCount($searchShipDetailParam);
                $viewShip = "<a onClick=showLineShips('" . trim($this->encrypt($val['LINEID'])) . "');><img src='" . $this->_config['img_file_path']['path'] . "/img/shipline-icon.jpg' style='cursor:pointer;'></a>&nbsp;";
                $viewPassenger = "<a onClick=showShipPassenger('" . trim($this->encrypt($val['SHIP_NAME'])) . "','" . $this->encrypt($val['DATE_ARRIVE']) . "');><img src='" . $this->_config['img_file_path']['path'] . "/img/passenger-icon.jpg' style='cursor:pointer;'></a>&nbsp;";
                $viewFile = "<a href='javascript:void(0);' onclick=showManifestImage('" . $this->encrypt($val['FILENAME']) . "','" . $this->encrypt($val['DATE_ARRIVE']) . "','" . $this->encrypt($val['SHIP_NAME']) . "')><img src='" . $this->_config['img_file_path']['path'] . "/img/manifest-icon.jpg' style='cursor:pointer;'></a>";
                $departPortName = array();
                $departPortNameArray = array();
                $departPortName = explode(',', $val['DEP_PORT_NAME']);
                foreach ($departPortName as $portKey => $portVal) {
                    $departPortNameArray[] = $portVal;
                }
                $departPortNameArray = array_unique($departPortNameArray);
                $departPortNameStr = implode(',', $departPortNameArray);
                $arrCell['cell'] = array($val['ID'], $val['UDE_BATCH_NUMBER'], $this->DateFormat($val['SHIP_ARRIVAL_DATE'], 'displayformat'), $departPortNameStr, $val['SHIP_ARRIVAL_PORT'], $passCountResult[0]['cnt'], $viewShip, $viewPassenger, $viewFile);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This Action is used to list the all ships of the ship line
     * @param id to get the line ships
     * @return this will return the the line ships
     * @author Icreon Tech - SR
     */
    public function listLineshipsAction() {
        $this->checkUserAuthentication();
        $this->layout('popup');
        $params = $this->params()->fromRoute();
        $viewModel = new ViewModel();

        $searchParam['LINEID'] = $this->decrypt($params['id']);
        $searchParam['startIndex'] = 0;
        $searchParam['recordLimit'] = 1;
        $searchResult = $this->getDocumentTable()->getShipLineShipList($searchParam);

        $viewModel->setVariables(array(
            'id' => $params['id'],
            'searchResult' => $searchResult
        ));
        return $viewModel;
    }

    /**
     * This Action is used to list the all ships of the ship line
     * @param this will id
     * @return this will return the listing of the line ships
     * @author Icreon Tech - SR
     */
    public function getListLineshipsAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $searchParam['LINEID'] = $this->decrypt($request->getPost('id'));
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');

        $searchResult = $this->getDocumentTable()->getShipLineShipList($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);

        if (count($searchResult) > 0) {
            foreach ($searchResult as $val) {
                $arrCell['cell'] = array($val['SHIPNAME']);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This Action is used to list the all ships of the ship line
     * @return this will be the ship name
     * @author Icreon Tech - SR
     */
    public function shipPassengerAction() {
        $this->checkUserAuthentication();
        $this->layout('popup');
        $params = $this->params()->fromRoute();
        $viewModel = new ViewModel();

        $viewModel->setVariables(array(
            'ship_name' => $params['ship_name'],
            'DATE_ARRIVE' => $params['DATE_ARRIVE']
        ));
        return $viewModel;
    }

    /**
     * This Action is used to list the all ships of the ship line
     * @param this will ship_name, DATE_ARRIVE
     * @return this will show the listing of ships passengers
     * @author Icreon Tech - SR
     */
    public function getshipPassengerListAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();

        $searchParam['ship_name'] = $this->decrypt($request->getPost('ship_name'));
        $searchParam['DATE_ARRIVE'] = substr($this->decrypt($request->getPost('DATE_ARRIVE')), 0, 10);
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');

        $searchResult = $this->getDocumentTable()->getShipPassengerList($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();

        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
        $maritalStatusArray = $this->maritalStatusArray();
        $genderArray = $this->genderArray();
        if (count($searchResult) > 0) {
            foreach ($searchResult as $val) {
                $gender = '';
                $maritalStatus = '';
                if ($val['PRIN_GENDER_CODE'] != '')
                    $gender = $genderArray[$val['PRIN_GENDER_CODE']];
                if (empty($gender))
                    $gender = 'Unknown';
                if ($val['PRIN_MARITAL_STAT'] != '')
                    $maritalStatus = $maritalStatusArray[$val['PRIN_MARITAL_STAT']];

                if (empty($maritalStatus))
                    $maritalStatus = 'Unknown';
                $arrCell['cell'] = array($val['REF_PAGE_LINE_NBR'], $val['ID'], $val['PRIN_FIRST_NAME'] . ' ' . $val['PRIN_LAST_NAME'], $gender, $val['PRIN_PLACE_RESI'], $maritalStatus, $val['PRIN_AGE_ARRIVAL'], $val['ethnicity']);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This Action is used to to edit the ship details
     * @param this will pass passenger_id
     * @return this will return confirmation of deletion
     * @author Icreon Tech - SR
     */
    public function editShipAction() {
        $this->checkUserAuthentication();

        $this->layout('crm');
        $this->getDocumentTable();
        $edit_passenger_form = new EditPassengerForm();
        $request = $this->getRequest();
        $document = new Document($this->_adapter);
        $params = $this->params()->fromRoute();
        $response = $this->getResponse();
        $edit_ship_form = new EditShipForm();

        $day = array();
        $day[''] = 'Select';
        for ($i = 1; $i <= 31; $i++) {
            $day[$i] = $i;
        }
        $edit_ship_form->get('scrapped_day')->setAttribute('options', $day);
        $month = $this->monthArray();
        $edit_ship_form->get('scrapped_month')->setAttribute('options', $month);

        $year = $this->yearArray();
        $edit_ship_form->get('build_year')->setAttribute('options', $year);
        $edit_ship_form->get('scrapped_year')->setAttribute('options', $year);

        $getSearchArray = array();
        $engineArray = $this->getDocumentTable()->getEngine($getSearchArray);
        $engineList = array();
        $engineList[''] = 'Select';
        foreach ($engineArray as $key => $val) {
            $engineList[$val['engine_type_id']] = stripslashes($val['engine_type']);
        }
        $edit_ship_form->get('engine_type')->setAttribute('options', $engineList);

        if ($request->isPost()) {

            $edit_ship_form->setInputFilter($document->getShipInputFilter());
            $edit_ship_form->setData($request->getPost());

            if (!$edit_ship_form->isValid()) {

                $msg = array();
                $errors = $edit_ship_form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['PassengerMessages']['config']['edit_ship'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {

                $document->exchangeShipArray($edit_ship_form->getData());
                $postData = array();
                $postData['ship_id'] = $this->decrypt($document->ship_id);
                $postData['shipline_id'] = $this->decrypt($document->shipline_id);
                $postData['name_when_built'] = $document->name_when_built;
                $postData['gross_weight'] = $document->gross_weight;
                $postData['length'] = $document->length;
                $postData['beam'] = $document->beam;
                $postData['engine_type'] = $document->engine_type;
                $postData['num_screws'] = $document->num_screws;
                $postData['scrapped_country'] = $document->scrapped_country;
                $postData['builder'] = $document->builder;
                $postData['service_speed_knots'] = $document->service_speed_knots;
                $postData['scrapped_month'] = $document->scrapped_month;
                $postData['scrapped_day'] = $document->scrapped_day;
                $postData['build_year'] = substr($document->build_year, -4);
                $postData['scrapped_year'] = substr($document->scrapped_year, -4);
                $postData['build_country'] = $document->build_country;
                $postData['ship_name_iss'] = $document->ship_name_iss;
                $postData['updated_on'] = DATE_TIME_FORMAT;

                $flag = $this->getDocumentTable()->updateCrmShip($postData);
                if ($flag) {
                    if ($request->getPost('oldShipName') != '' && $request->getPost('ship_name') != '' && $request->getPost('oldShipName') != $request->getPost('ship_name')) {
                        $postShipData = array();
                        $postShipData['oldShipName'] = $request->getPost('oldShipName');
                        $postShipData['newShipName'] = $request->getPost('ship_name');
                        $this->getDocumentTable()->updateCrmPassengerShip($postShipData);
                    }

                    $shipLineArray['ship_name'] = $request->getPost('ship_name');
                    $shipLineArray['ship_id'] = $this->decrypt($document->ship_id);
                    $shipLineArray['LINEID'] = $request->getPost('LINEID');
                    $shipLineArray['updated_on'] = DATE_TIME_FORMAT;
                    $flag = $this->getDocumentTable()->updateCrmShipInfoShipLine($shipLineArray);

                    $imagePostedData = array();
                    $imagePostedData['is_edited_image'] = $document->is_edited_image;
                    $imagePostedData['ship_id'] = $this->decrypt($document->ship_id);
                    $imagePostedData['assetfor'] = '1';
                    $this->getDocumentTable()->updateCrmShipImage($imagePostedData);
                }
                /** insert into the change log */
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_shp_ship_change_logs';
                $changeLogArray['activity'] = '2';
                $changeLogArray['id'] = $postData['ship_id'];
                $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);

                /** end insert into the change log */
                if ($flag == true) {
                    $this->flashMessenger()->addMessage('Ship details updated successfully!');
                    $messages = array('status' => "success", 'message' => 'Ship details updated successfully!');
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                } else {
                    $this->flashMessenger()->addMessage($this->_config['PassengerMessages']['config']['edit_passenger']['UPDATE_ERROR_MSG']);
                    $messages = array('status' => "error", 'message' => $this->_config['PassengerMessages']['config']['edit_passenger']['UPDATE_ERROR_MSG']);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        } else {
            /*
              $searchParam['ship_id'] = $this->decrypt($params['id']);
              // $searchParam['shiplineId'] = $this->decrypt($params['shiplineId']);
              $shipResult = $this->getDocumentTable()->searchCrmShip($searchParam);

              $edit_ship_form->get('ship_id')->setAttribute('value', $this->encrypt($shipResult[0]['SHIPID']));
              $edit_ship_form->get('shipline_id')->setAttribute('value', $this->encrypt($shipResult[0]['LINEID']));
              $edit_ship_form->get('shipline')->setAttribute('value', $shipResult[0]['NAME']);
              $edit_ship_form->get('name_when_built')->setAttribute('value', $shipResult[0]['NAMEWHENBUILT']);
              $edit_ship_form->get('gross_weight')->setAttribute('value', $shipResult[0]['GROSSTONS']);
              $edit_ship_form->get('length')->setAttribute('value', $shipResult[0]['FEETLONG']);
              $edit_ship_form->get('beam')->setAttribute('value', $shipResult[0]['BEAM']);
              $edit_ship_form->get('engine_type')->setAttribute('value', $shipResult[0]['ENGINETYPE']);
              $edit_ship_form->get('num_screws')->setAttribute('value', $shipResult[0]['NUMSCREWS']);
              $edit_ship_form->get('ship_name')->setAttribute('value', $shipResult[0]['SHIPNAME']);
              $edit_ship_form->get('build_year')->setAttribute('value', $shipResult[0]['BUILDYEAR']);
              $edit_ship_form->get('scrapped_year')->setAttribute('value', $shipResult[0]['SCRAPPEDYEAR']);
              $edit_ship_form->get('scrapped_country')->setAttribute('value', $shipResult[0]['SCRAPPEDINCOUNTRY']);
              $edit_ship_form->get('builder')->setAttribute('value', $shipResult[0]['BUILDER']);
              $edit_ship_form->get('build_city')->setAttribute('value', $shipResult[0]['BUILDCITY']);
              $edit_ship_form->get('build_country')->setAttribute('value', $shipResult[0]['BUILDCOUNTRY']);
              $edit_ship_form->get('service_speed_knots')->setAttribute('value', $shipResult[0]['SERVICESPEEDKNOTS']);
              $edit_ship_form->get('scrapped_month')->setAttribute('value', $shipResult[0]['SCRAPPEDMONTH']);
              $edit_ship_form->get('scrapped_day')->setAttribute('value', $shipResult[0]['SCRAPPEDDAY']);
             */
        }
        $viewModel = new ViewModel();


        $viewModel->setVariables(array(
            'ship_id' => $shipResult[0]['SHIPID'],
            'shipline_name' => $shipResult[0]['NAME'],
            'edit_ship_form' => $edit_ship_form,
            'moduleName' => $this->encrypt('ship'),
            'jsLangTranslate' => $this->_config['PassengerMessages']['config']['edit_ship']
        ));

        return $viewModel;
    }

    /**
     * This Action is used to get the view crm ship details
     * @param this will id,
     * @return this will be be the ship details
     * @author Icreon Tech - SR
     */
    public function viewCrmShipDetailsAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $params = $this->params()->fromRoute();
        $searchParam['ship_id'] = $this->decrypt($params['id']);
        $searchParam['ship_name'] = addslashes($this->decrypt($params['ship_name']));
        $document = new Document($this->_adapter);
        $searchParam = $document->assignData($searchParam);
        $searchParam['sortField'] = 'LINESHIPID';
        $searchParam['sortOrder'] = 'DESC';
        $searchResult = $this->getDocumentTable()->searchCrmShip($searchParam);
        $isImageEditedFlag = 0;
        $searchImageParam['ship_id'] = $searchParam['ship_id'];
        $searchImageParam['asset_for'] = '1';
        $searchShipImageResult = $this->getDocumentTable()->searchCrmShipImage($searchImageParam);
        if (count($searchShipImageResult) > 0)
            $isImageEditedFlag = $searchShipImageResult[0]['is_edited_image'];

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'ship_id' => $params['id'],
            //'shiplineId' => $params['shiplineId'],
            'moduleName' => $this->encrypt('ship'),
            'searchResult' => $searchResult[0],
            'isImageEditedFlag' => $isImageEditedFlag,
            'jsLangTranslate' => array_merge($this->_config['PassengerMessages']['config']['ship_search'], $this->_config['PassengerMessages']['config']['edit_ship'])
        ));
        return $viewModel;
    }

    /**
     * This Action is used to edit the trip information
     * @param this will ID
     * @return this will be confirmation
     * @author Icreon Tech - SR
     */
    public function editTripInfoAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $postData = array();
            $postData['ID'] = $request->getPost('ID');
            $dateArray = explode('-', $request->getPost('ArrivalDate'));

            $monthNum = $dateArray[0];
            $monthName = date("M", mktime(0, 0, 0, $monthNum, 10));

            $postData['ArrivalDate'] = $dateArray[1] . ' ' . $monthName . ' ' . $dateArray[2];
            $postData['DateArrive'] = $dateArray[2] . '-' . $dateArray[0] . '-' . $dateArray[1];
            $postData['PortofDeparture'] = $request->getPost('PortofDeparture');
            $this->getDocumentTable()->updateTripInformation($postData);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This Action is used to get the edit crm ship details
     * @param this will id
     * @return this will details of ship
     * @author Icreon Tech - SR
     */
    public function editCrmShipDetailsAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $edit_passenger_form = new EditPassengerForm();
        $request = $this->getRequest();
        $document = new Document($this->_adapter);
        $params = $this->params()->fromRoute();
        $response = $this->getResponse();

        $edit_ship_form = new EditShipForm();
        $day = array();
        $day[''] = 'Select';
        for ($i = 1; $i <= 31; $i++) {
            $day[$i] = $i;
        }
        $edit_ship_form->get('scrapped_day')->setAttribute('options', $day);
        $month = $this->monthArray();
        $edit_ship_form->get('scrapped_month')->setAttribute('options', $month);
        $year = $this->yearArray();
        $edit_ship_form->get('build_year')->setAttribute('options', $year);
        $edit_ship_form->get('scrapped_year')->setAttribute('options', $year);

        $getSearchArray = array();
        $engineArray = $this->getDocumentTable()->getEngine($getSearchArray);
        $engineList = array();
        $engineList[''] = 'Select';
        foreach ($engineArray as $key => $val) {
            $engineList[$val['engine_type_id']] = stripslashes($val['engine_type']);
        }
        $edit_ship_form->get('engine_type')->setAttribute('options', $engineList);

        $searchParam['ship_id'] = $this->decrypt($params['id']);
        //asd($params);
        if (isset($params['ship_name']) && $params['ship_name'] != '')
            $searchParam['ship_name'] = addslashes($this->decrypt($params['ship_name']));
        $searchParam['sortField'] = 'LINESHIPID';
        $searchParam['sortOrder'] = 'DESC';
        // asd($searchParam); //bbbbbbbbbbbbbbbb
        //asd($searchParam);

        $shipResult = $this->getDocumentTable()->searchCrmShip($searchParam);

        $isImageEditedFlag = 0;
        $searchImageParam['ship_id'] = $searchParam['ship_id'];
        $searchImageParam['asset_for'] = '1';
        $searchShipImageResult = $this->getDocumentTable()->searchCrmShipImage($searchImageParam);
        if (count($searchShipImageResult) > 0)
            $isImageEditedFlag = $searchShipImageResult[0]['is_edited_image'];

        //   asd($shipResult);
        $edit_ship_form->get('ship_id')->setAttribute('value', $this->encrypt($shipResult[0]['SHIPID']));
        $edit_ship_form->get('shipline_id')->setAttribute('value', $this->encrypt($shipResult[0]['LINEID']));
        $edit_ship_form->get('shipline')->setAttribute('value', $shipResult[0]['NAME']);
        $edit_ship_form->get('name_when_built')->setAttribute('value', $shipResult[0]['NAMEWHENBUILT']);
        $edit_ship_form->get('gross_weight')->setAttribute('value', $shipResult[0]['GROSSTONS']);
        $edit_ship_form->get('length')->setAttribute('value', $shipResult[0]['FEETLONG']);
        $edit_ship_form->get('beam')->setAttribute('value', $shipResult[0]['BEAM']);
        $edit_ship_form->get('engine_type')->setAttribute('value', $shipResult[0]['ENGINETYPE']);
        $edit_ship_form->get('num_screws')->setAttribute('value', $shipResult[0]['NUMSCREWS']);
        $edit_ship_form->get('ship_name')->setAttribute('value', $shipResult[0]['SHIPNAME']);
        $edit_ship_form->get('build_year')->setAttribute('value', $shipResult[0]['BUILDYEAR']);
        $edit_ship_form->get('scrapped_year')->setAttribute('value', $shipResult[0]['SCRAPPEDYEAR']);
        $edit_ship_form->get('scrapped_country')->setAttribute('value', $shipResult[0]['SCRAPPEDINCOUNTRY']);
        $edit_ship_form->get('builder')->setAttribute('value', $shipResult[0]['BUILDER']);
        $edit_ship_form->get('build_city')->setAttribute('value', $shipResult[0]['BUILDCITY']);
        $edit_ship_form->get('build_country')->setAttribute('value', $shipResult[0]['BUILDCOUNTRY']);
        $edit_ship_form->get('service_speed_knots')->setAttribute('value', $shipResult[0]['SERVICESPEEDKNOTS']);
        $edit_ship_form->get('scrapped_month')->setAttribute('value', $shipResult[0]['SCRAPPEDMONTH']);
        $edit_ship_form->get('scrapped_day')->setAttribute('value', $shipResult[0]['SCRAPPEDDAY']);
        $edit_ship_form->get('is_edited_image')->setAttribute('value', $isImageEditedFlag);
        $edit_ship_form->get('oldShipName')->setAttribute('value', $shipResult[0]['SHIPNAME']);

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'ship_id' => $shipResult[0]['SHIPID'],
            'LINEID' => $shipResult[0]['LINEID'],
            'shipline_name' => $shipResult[0]['NAME'],
            'edit_ship_form' => $edit_ship_form,
            'moduleName' => $this->encrypt('ship'),
            'jsLangTranslate' => $this->_config['PassengerMessages']['config']['edit_ship']
        ));

        return $viewModel;
    }

    /**
     * This Action is used to delete the the saved crm ship search
     * @param this will search_id
     * @return this will be a confirmation of deletion
     * @author Icreon Tech - SR
     */
    public function deleteCrmSearchShipAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['search_id'] = $request->getPost('search_id');
            $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $dataParam['isDelete'] = 1;
            $dataParam['modifiend_date'] = DATE_TIME_FORMAT;

            $this->getDocumentTable()->deleteCrmShipSearch($dataParam);
            $this->flashMessenger()->addMessage('Ship search details deleted successfully!');
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This Action is used to get ship line details
     * @param this will pass $params
     * @return this will be $viewModel
     * @author Icreon Tech - AS
     */
    public function getCrmShiplineAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getDocumentTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $search_shipline_form = new SearchShiplineForm();
        $save_search_shipline_form = new SaveSearchShiplineForm();


        $getTransactionRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
        $transactionRange = array("" => "Select One");
        foreach ($getTransactionRange as $key => $val) {
            $transactionRange[$val['range_id']] = $val['range'];
        }
        $search_shipline_form->get('start_year')->setAttribute('options', $transactionRange);
        $search_shipline_form->get('end_year')->setAttribute('options', $transactionRange);
        $search_shipline_form->get('start_year')->setAttribute('value', '1');
        $search_shipline_form->get('end_year')->setAttribute('value', '1');
        $getSearchArray = array();
        $getSearchArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
        $getSearchArray['isActive'] = '1';
        $getSearchArray['searchId'] = '';
        $crmSearchArray = $this->getDocumentTable()->getCrmShiplineSavedSearch($getSearchArray);
        $crmSearchList = array();
        $crmSearchList[''] = 'Select';
        foreach ($crmSearchArray as $key => $val) {
            $crmSearchList[$val['shipline_search_id']] = stripslashes($val['title']);
        }
        $search_shipline_form->get('saved_searches')->setAttribute('options', $crmSearchList);
        $response = $this->getResponse();
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $viewModel->setVariables(array(
            'search_shipline_form' => $search_shipline_form,
            'save_search_shipline_form' => $save_search_shipline_form,
            'messages' => $messages,
            'jsLangTranslate' => array_merge($this->_config['PassengerMessages']['config']['edit_shipline'], $this->_config['campaign_messages']['config']['common'])
        ));
        return $viewModel;
    }

    /**
     * This Action is used to get ship line details
     * @param this will pass $params
     * @return this will be $viewModel
     * @author Icreon Tech - AS
     */
    public function getCrmSearchShiplineAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        parse_str($request->getPost('searchString'), $searchParam);

        $dashlet = $request->getPost('crm_dashlet');
        $dashletSearchId = $request->getPost('searchId');
        $dashletId = $request->getPost('dashletId');
        $dashletSearchType = $request->getPost('searchType');
        $dashletSearchString = $request->getPost('dashletSearchString');
        if (isset($dashletSearchString) && $dashletSearchString != '') {
            $searchParam = unserialize($dashletSearchString);
        }

        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $document = new Document($this->_adapter);
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $isExport = $request->getPost('export');
        if ($isExport != "" && $isExport == "excel") {
            $chunksize = $this->_config['export']['chunk_size'];
            $export_limit = $this->_config['export']['export_limit'];

            $searchParam['recordLimit'] = $chunksize;
            //asd($searchParam);
        }

        if (isset($searchParam['start_year']) && $searchParam['start_year'] != '' && $searchParam['start_year'] != 1) {
            $buildYearArray = $this->getDateRange($searchParam['start_year']);
            $searchParam['start_year_date_from'] = substr($buildYearArray['from'], -4);
            $searchParam['start_year_date_to'] = substr($buildYearArray['to'], -4);
        } else if (isset($searchParam['start_year']) && $searchParam['start_year'] != '' && $searchParam['start_year'] == 1) {
            if ($searchParam['start_year_date_from'] != '') {
                $searchParam['start_year_date_from'] = substr($searchParam['start_year_date_from'], -4);
            }
            if ($searchParam['start_year_date_to'] != '') {
                $searchParam['start_year_date_to'] = substr($searchParam['start_year_date_to'], -4);
            }
        } else {
            $searchParam['start_year_date_from'] = '';
            $searchParam['start_year_date_to'] = '';
        }

        if (isset($searchParam['end_year']) && $searchParam['end_year'] != '' && $searchParam['end_year'] != 1) {
            $scrapYearArray = $this->getDateRange($searchParam['end_year']);
            $searchParam['end_year_date_from'] = substr($scrapYearArray['from'], -4);
            $searchParam['end_year_date_to'] = substr($scrapYearArray['to'], -4);
        } else if (isset($searchParam['end_year']) && $searchParam['end_year'] != '' && $searchParam['end_year'] == 1) {
            if ($searchParam['end_year_date_from'] != '') {
                $searchParam['end_year_date_from'] = substr($searchParam['end_year_date_from'], -4);
            }
            if ($searchParam['end_year_date_to'] != '') {
                $searchParam['end_year_date_to'] = substr($searchParam['end_year_date_to'], -4);
            }
        } else {
            $searchParam['end_year_date_from'] = '';
            $searchParam['end_year_date_to'] = '';
        }
        $page_counter = 1;
        do {

            $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
            $dashletColumnName = array();
            if (!empty($dashletResult)) {
                $dashletColumn = explode(",", $dashletResult[0]['table_column_names']);
                foreach ($dashletColumn as $val) {
                    if (strpos($val, ".")) {
                        $dashletColumnName[] = trim(strstr($val, ".", false), ".");
                    } else {
                        $dashletColumnName[] = trim($val);
                    }
                }
            }

            $searchResult = $this->getDocumentTable()->searchShipline($searchParam);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $total = $countResult[0]->RecordCount;
            if ($isExport == "excel") {
                if ($total > $chunksize) {
                    $number_of_pages = ceil($total / $chunksize);
                    $page_counter++;
                    $start = $chunksize * $page_counter - $chunksize;
                    $searchParam['startIndex'] = $start;
                    $searchParam['page'] = $page_counter;
                } else {
                    $number_of_pages = 1;
                    $page_counter++;
                }
            } else {
                $page_counter = $page;
                $number_of_pages = ceil($total / $limit);
                $jsonResult['records'] = $countResult[0]->RecordCount;
                $jsonResult['page'] = $page;
                $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            }
            $arrExport = array();
            if (count($searchResult) > 0) {

                foreach ($searchResult as $val) {
                    $dashletCell = array();

                    if (false !== $dashletColumnKey = array_search('LINESHIPID', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = '<a href="/shipline/' . $this->encrypt($val['LINESHIPID']) . '/' . $this->encrypt('view') . '" class="txt-decoration-underline">' . $val['LINESHIPID'] . '</a>';
                    if (false !== $dashletColumnKey = array_search('STARTYEAR', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $val['STARTYEAR'];
                    if (false !== $dashletColumnKey = array_search('ENDYEAR', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $val['ENDYEAR'];
                    if (false !== $dashletColumnKey = array_search('NAME', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = stripslashes($val['NAME']);
                    if (false !== $dashletColumnKey = array_search('HOMECITY', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = stripslashes($val['HOMECITY']);
                    if (false !== $dashletColumnKey = array_search('HOMECOUNTRY', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = stripslashes($val['HOMECOUNTRY']);

                    $arrCell['id'] = $val['LINESHIPID'];
                    $view = "<a class='view-icon' href='/shipline/" . $this->encrypt($val['LINESHIPID']) . "/" . $this->encrypt('view') . "'><div class='tooltip'>View<span></span></div></a>";
                    $edit = "<a class='edit-icon' href='/shipline/" . $this->encrypt($val['LINESHIPID']) . "/" . $this->encrypt('edit') . "'><div class='tooltip'>Edit<span></span></div></a>";
                    if (isset($dashlet) && $dashlet == 'dashlet') {
                        $arrCell['cell'] = $dashletCell;
                    } else if ($isExport == "excel") {
                        $arrExport[] = array('Line Name Id' => $val['LINESHIPID'], 'Start Year' => stripslashes($val['STARTYEAR']), 'End Year' => stripslashes($val['ENDYEAR']), 'Name' => stripslashes($val['NAME']), 'Home City' => $val['HOMECITY'], 'Home Country' => $val['HOMECOUNTRY']);
                    } else {
                        $arrCell['cell'] = array($val['LINESHIPID'], stripslashes($val['STARTYEAR']), stripslashes($val['ENDYEAR']), stripslashes($val['NAME']), $val['HOMECITY'], stripslashes($val['HOMECOUNTRY']), $view . $edit);
                    }

                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
                if ($isExport == "excel") {

                    $filename = "shipline_export" . date("Y-m-d") . ".csv";
                    $this->arrayToCsv($arrExport, $filename, $page_counter);
                }
            }
        } while ($page_counter <= $number_of_pages && $isExport == "excel");
        if ($isExport == "excel") {
            //exit;
            $this->downloadSendHeaders("shipline_export" . date("Y-m-d") . ".csv");
            die;
        } else {
            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
        }

        //return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This Action is used to get crm search annotation correction
     * @return this will be the search parameters
     * @author Icreon Tech - SR
     */
    function getCrmSearchAnnotationCorrectionAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        parse_str($request->getPost('searchString'), $searchParam);

        $dashlet = $request->getPost('crm_dashlet');
        $dashletSearchId = $request->getPost('searchId');
        $dashletId = $request->getPost('dashletId');
        $dashletSearchType = $request->getPost('searchType');
        $dashletSearchString = $request->getPost('dashletSearchString');
        if (isset($dashletSearchString) && $dashletSearchString != '') {
            $searchParam = unserialize($dashletSearchString);
        }

        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $document = new Document($this->_adapter);
        if (isset($searchParam['requested_on']) && $searchParam['requested_on'] != '' && $searchParam['requested_on'] != 1) {
            $buildYearArray = $this->getDateRange($searchParam['requested_on']);
            $searchParam['from_date'] = $this->DateFormat($buildYearArray['from'], 'db_date_format_from');
            $searchParam['to_date'] = $this->DateFormat($buildYearArray['to'], 'db_date_format_to');
        } else if (isset($searchParam['requested_on']) && $searchParam['requested_on'] != '' && $searchParam['requested_on'] == 1) {
            if ($searchParam['from_date'] != '') {
                $searchParam['from_date'] = $this->DateFormat($searchParam['from_date'], 'db_date_format_from');
            }
            if ($searchParam['to_date'] != '') {
                $searchParam['to_date'] = $this->DateFormat($searchParam['to_date'], 'db_date_format_to');
            }
        } else {
            $searchParam['from_date'] = '';
            $searchParam['to_date'] = '';
        }
        $isExport = $request->getPost('export');
        if ($isExport != "" && $isExport == "excel") {
            $chunksize = $this->_config['export']['chunk_size'];
            $export_limit = $this->_config['export']['export_limit'];

            $searchParam['recordLimit'] = $chunksize;
        }

        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        if ($this->_flashMessage->hasMessages()) {
            $searchParam['sortField'] = 'added_date';
            $searchParam['sortOrder'] = 'desc';
        }
        $page_counter = 1;
        $number_of_pages = 1;
        do {
            $searchResult = $this->getDocumentTable()->searchCrmAnnotationCorrection($searchParam);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
            $dashletColumnName = array();
            if (!empty($dashletResult)) {
                $dashletColumn = explode(",", $dashletResult[0]['table_column_names']);
                foreach ($dashletColumn as $val) {
                    if (strpos($val, ".")) {
                        $dashletColumnName[] = trim(strstr($val, ".", false), ".");
                    } else {
                        $dashletColumnName[] = trim($val);
                    }
                }
            }

            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $total = $countResult[0]->RecordCount;

            if ($isExport == "excel") {
                $filename = "Annotation_and_Corrections_Export_" . EXPORT_DATE_TIME_FORMAT . ".csv";
                if ($total > $chunksize) {
                    $number_of_pages = ceil($total / $chunksize);
                    $page_counter++;
                    $start = $chunksize * $page_counter - $chunksize;
                    $searchParam['startIndex'] = $start;
                    $searchParam['page'] = $page_counter;
                } else {
                    $number_of_pages = 1;
                    $page_counter++;
                }
            } else {
                $page_counter = $page;
                $jsonResult['page'] = $page;
                $jsonResult['records'] = $countResult[0]->RecordCount;
                $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            }
        } while ($page_counter <= $number_of_pages && $isExport == "excel");

        if (count($searchResult) > 0) {
            $arrExport = array();
            //asd($searchResult);
            foreach ($searchResult as $val) {
                $dashletCell = array();
                $arrCell['id'] = $val['passenger_id'];
                if (false !== $dashletColumnKey = array_search('passenger_id', $dashletColumnName))
                    $dashletCell[$dashletColumnKey] = '<a href="/view-crm-passenger/' . $this->encrypt($val['passenger_id']) . '/' . $this->encrypt('view') . '" class="txt-decoration-underline">' . $val['passenger_id'] . '</a>';
                if (false !== $dashletColumnKey = array_search('passenger_name', $dashletColumnName))
                    $dashletCell[$dashletColumnKey] = stripslashes($val['passenger_name']);


                $viewLabel = $this->_config['PassengerMessages']['config']['common']['VIEW'];
                $editLabel = $this->_config['PassengerMessages']['config']['common']['EDIT'];
                $deleteLabel = $this->_config['PassengerMessages']['config']['common']['DELETE'];

                $delete = "<a class='delete_annotation delete-icon' href='#delete_annotation' onClick=deleteAnnotation('" . $this->encrypt($val['annotation_correction_id']) . "')><div class='tooltip'>" . $deleteLabel . "<span></span></div></a>";
                $edit = "<a class='edit-icon' href='/edit-crm-annotation-correction/" . $this->encrypt($val['annotation_correction_id']) . "'><div class='tooltip'>" . $editLabel . "<span></span></div></a>";

                $activityType = '';
                if ($val['activity_type'] == 1)
                    $activityType = 'Annotations';
                if ($val['activity_type'] == 2)
                    $activityType = 'Corrections';

                $status = '';
                if ($val['status'] == 0)
                    $status = ($val['activity_type'] == 1) ? 'Hidden' : 'Pending';
                if ($val['status'] == 1)
                    $status = ($val['activity_type'] == 1) ? 'Shown' : 'Completed';

                $approvalRequired = '';

                if ($val['activity_type'] == 1) {
                    if ($val['is_reauthorization'] == 0)
                        $approvalRequired = 'No';
                    else
                        $approvalRequired = 'Yes';
                }
                else {
                    if ($val['status'] == 1)
                        $approvalRequired = 'No';
                    else if ($val['status'] == 0)
                        $approvalRequired = 'Yes';
                }
                $approvedBy = 'N/A';
                if (isset($val['crm_first_name']) && !empty($val['crm_first_name'])) {
                    $approvedBy = $val['crm_first_name'] . ' ' . $val['crm_last_name'];
                }
                $contactNameArray = array();
                $contactName = '';


                $contactNameArray[] = $val['contact_name'];
                if (!empty($val['contact_middle_name']))
                    $contactNameArray[] = $val['contact_middle_name'];

                if (!empty($val['contact_last_name']))
                    $contactNameArray[] = substr($val['contact_last_name'], 0, 1) . ".";

                $contactName = implode(' ', $contactNameArray);

                if ($val['user_type'] == 2 || $val['user_type'] == 3) {
                    $contactName = $val['company_name'];
                }

                if (false !== $dashletColumnKey = array_search('contact_name', $dashletColumnName))
                    $dashletCell[$dashletColumnKey] = stripslashes($contactName);
                if (false !== $dashletColumnKey = array_search('added_date', $dashletColumnName))
                    $dashletCell[$dashletColumnKey] = $this->DateFormat($val['added_date'], 'displayformat');
                if (false !== $dashletColumnKey = array_search('activity_type', $dashletColumnName))
                    $dashletCell[$dashletColumnKey] = $activityType;

                if (false !== $dashletColumnKey = array_search('modified_date', $dashletColumnName))
                    $dashletCell[$dashletColumnKey] = $this->OutputDateFormat($val['modified_date'], 'dateformatampm');

                if (false !== $dashletColumnKey = array_search('is_active', $dashletColumnName))
                    $dashletCell[$dashletColumnKey] = $status;

                if (false !== $dashletColumnKey = array_search('is_reauthorization', $dashletColumnName))
                    $dashletCell[$dashletColumnKey] = $approvalRequired;

                if (false !== $dashletColumnKey = array_search('crm_first_name', $dashletColumnName))
                    $dashletCell[$dashletColumnKey] = $approvedBy;


                if (isset($dashlet) && $dashlet == 'dashlet') {
                    $arrCell['cell'] = $dashletCell;
                } else if ($isExport == "excel") {
                    $arrExport[] = array("Passenger Id" => $val['passenger_id'], "Passenger Name" => stripslashes($val['passenger_name']), "Contact Name" => stripslashes($contactName), "Requested On" => $this->OutputDateFormat($val['added_date'], 'dateformatampm'), "Modified On" => $this->OutputDateFormat($val['modified_date'], 'dateformatampm'), "Type" => $activityType, "Status" => $status, "Approval Required" => $approvalRequired, "Approved By" => $val['crm_first_name'] . ' ' . $val['crm_last_name']);
                } else {
                    $arrCell['cell'] = array($val['passenger_id'], stripslashes($val['passenger_name']), stripslashes($contactName), $this->OutputDateFormat($val['added_date'], 'dateformatampm'), $this->OutputDateFormat($val['modified_date'], 'dateformatampm'), $activityType, $status, $approvalRequired, $val['crm_first_name'] . ' ' . $val['crm_last_name'], $edit . $delete);
                }

                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
            if ($isExport == "excel") {
                $this->arrayToCsv($arrExport, $filename, $page_counter);
            }
        } else {
            $jsonResult['rows'] = array();
        }
        if ($isExport == "excel") {
            $this->downloadSendHeaders($filename);
            die;
        } else {
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        }
    }

    /**
     * This Action is used to edit ship line details
     * @param this will pass $params
     * @return this will be $viewModel
     * @author Icreon Tech - AS
     */
    public function editCrmShiplineAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $edit_shipline_form = new EditShiplineForm();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response = $this->getResponse();
        $document = new Document($this->_adapter);
        $params = $this->params()->fromRoute();
        if ($request->isPost()) {
            $edit_shipline_form->setData($request->getPost());
            if (!$edit_shipline_form->isValid()) {

                $msg = array();
                $errors = $edit_shipline_form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['PassengerMessages']['config']['edit_shipline'][$rower];
                        }
                    }
                }

                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                parse_str($request->getPost('searchString'), $search_param);
                $document->exchangeShipArray($edit_shipline_form->getData());
                $postData = array();
                $postData['shipline_id'] = $search_param['linename_id'];
                $postData['line_id'] = $search_param['line_id'];
                $postData['name'] = $search_param['name'];
                $postData['start_year'] = $search_param['start_year'];
                $postData['end_year'] = $search_param['end_year'];
                $postData['total_pass'] = $search_param['total_pass'];
                //$postData['asset_id'] = $search_param['asset_id'];
                $postData['flag'] = $search_param['flag'];
                $postData['home_city'] = $search_param['home_city'];
                $postData['home_country'] = $search_param['home_country'];
                $postData['updated_on'] = DATE_TIME_FORMAT;
                $flag = $this->getDocumentTable()->updateCrmShipLine($postData);
                //$flag = $this->getDocumentTable()->updateCrmShipLine($shipLineArray);
                // $LINEID = $request->getPost('');
                /** insert into the change log */
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_shp_shipline_change_logs';
                $changeLogArray['activity'] = '2';
                $changeLogArray['id'] = $postData['shipline_id'];
                $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);

                /** end insert into the change log */
                if ($flag == true) {
                    $this->flashMessenger()->addMessage($this->_config['PassengerMessages']['config']['edit_shipline']['UPDATE_CONFIRM_MSG']);
                    $messages = array('status' => "success", 'message' => $this->_config['PassengerMessages']['config']['edit_shipline']['UPDATE_CONFIRM_MSG']);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                } else {
                    $messages = array('status' => "error", 'message' => $this->_config['PassengerMessages']['config']['edit_shipline']['UPDATE_ERROR_MSG']);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        } else {
            $searchParam['shipline_id'] = $this->decrypt($params['id']);
            $searchResult = $this->getDocumentTable()->searchCrmShipline($searchParam);
            $edit_shipline_form->get('linename_id')->setAttribute('value', $searchResult[0]['LINESHIPID']);
            $edit_shipline_form->get('ship_id')->setAttribute('value', $searchResult[0]['SHIPID']);
            $edit_shipline_form->get('line_id')->setAttribute('value', $searchResult[0]['LINEID']);
            $edit_shipline_form->get('name')->setAttribute('value', $searchResult[0]['NAME']);
            $edit_shipline_form->get('start_year')->setAttribute('value', $searchResult[0]['STARTYEAR']);
            $edit_shipline_form->get('end_year')->setAttribute('value', $searchResult[0]['ENDYEAR']);
            $edit_shipline_form->get('total_pass')->setAttribute('value', $searchResult[0]['TOTALPASS']);
            $edit_shipline_form->get('flag')->setAttribute('value', $searchResult[0]['FLAG']);
            $edit_shipline_form->get('home_city')->setAttribute('value', $searchResult[0]['HOMECITY']);
            $edit_shipline_form->get('home_country')->setAttribute('value', $searchResult[0]['HOMECOUNTRY']);
            $edit_shipline_form->get('assets_id')->setAttribute('value', $searchResult[0]['ASSETID']);
        }
        $viewModel->setVariables(array(
            'lineship_id' => $searchParam['shipline_id'],
            'encrypted_lineship_id' => $searchParam['shipline_id'],
            'edit_shipline_form' => $edit_shipline_form,
            'module_name' => $this->encrypt('shipline'),
            'mode' => 'edit',
            'jsLangTranslate' => $this->_config['PassengerMessages']['config']['edit_shipline']
        ));
        return $viewModel;
    }

    /**
     * This Action is used to view ship line details
     * @param this will pass $params
     * @return this will be $viewModel
     * @author Icreon Tech - AS
     */
    public function getCrmShiplineInfoAction() {
        $this->checkUserAuthentication();
        //$this->layout('crm');
        $this->getDocumentTable();
        $params = $this->params()->fromRoute();
        $searchParam['shipline_id'] = $this->decrypt($params['id']);
        $searchResult = $this->getDocumentTable()->searchCrmShipline($searchParam);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'shipline_id' => $params['id'],
            'encrypted_shipline_id' => $searchParam['shipline_id'],
            'searchResult' => $searchResult[0],
            'module_name' => $this->encrypt('shipline'),
            'mode' => 'edit',
            'jsLangTranslate' => $this->_config['PassengerMessages']['config']['edit_shipline']
        ));
        return $viewModel;
    }

    /**
     * This Action is used to save the ship line search
     * @param this will pass $searchParam
     * @return this will be confirmation
     * @author Icreon Tech - AS
     */
    public function saveShiplineSearchAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $document = new Document($this->_adapter);
        if ($request->isPost()) {
            $searchName = $request->getPost('search_name');
            parse_str($request->getPost('searchString'), $searchParam);
            $searchParam['search_title'] = $searchName;
            $searchParam = $document->getInputFilterCrmSearch($searchParam);
            $searchName = $document->getInputFilterCrmSearch($searchName);

            if (trim($searchName) != '') {
                $saveSearchParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $saveSearchParam['isActive'] = 1;
                $saveSearchParam['isDelete'] = 0;
                $saveSearchParam['search_name'] = $searchName;
                $saveSearchParam['search_query'] = serialize($searchParam);
                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modifiend_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['search_id'] = $request->getPost('search_id');
                    if ($this->getDocumentTable()->updateCrmShiplineSearch($saveSearchParam) == true) {
                        $this->flashMessenger()->addMessage('Shipline search is updated successfully');
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {

                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    if ($this->getDocumentTable()->saveCrmShiplineSearch($saveSearchParam) == true) {
                        $this->flashMessenger()->addMessage('Shipline search is saved successfully!');
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used to delete the the saved crm shipline search
     * @param this will pass an array $dataParam
     * @return this will be a confirmation message in json
     * @author Icreon Tech - AS
     */
    public function deleteCrmSearchShiplineAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['search_id'] = $request->getPost('search_id');
            $dataParam['isDelete'] = 1;
            $dataParam['modifiend_date'] = DATE_TIME_FORMAT;
            $this->getDocumentTable()->deleteCrmShiplineSearch($dataParam);
            $this->flashMessenger()->addMessage('Shipline search is deleted successfully!');
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This Action is used to get the saved crm shipline search
     * @param this will pass an array $dataParam
     * @return this will be json format 
     * @author Icreon Tech - AS
     */
    public function getCrmSearchShiplineSavedParamAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {

                $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $dataParam['isActive'] = '1';
                $dataParam['campaignSearchId'] = $request->getPost('search_id');
                $searchResult = $this->getDocumentTable()->getCrmShiplineSavedSearch($dataParam);
                $searchResult = json_encode(unserialize($searchResult[0]['search_query']));
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This Action is used to generte the select option after saving the new search title
     * @param this will pass an array $dataParam
     * @return this will be a string having the all options
     * @author Icreon Tech - AS
     */
    public function getSavedShiplineSearchSelectAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam = array();
            $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $dataParam['isActive'] = '1';
            $dataParam['shiplineSearchId'] = '';
            $crmSearchArray = $this->getDocumentTable()->getCrmShiplineSavedSearch($dataParam);
            $options = '';
            $options .="<option value=''>" . $this->_config['PassengerMessages']['config']['common']['SELECT'] . "</option>";
            foreach ($crmSearchArray as $key => $val) {
                $options .="<option value='" . $val['shipline_search_id'] . "'>" . $val['title'] . "</option>";
            }
            return $response->setContent($options);
        }
    }

    /**
     * This Action is used to add annotation and corrections
     * @return this will be search annotation search parameters
     * @author Icreon Tech - SR
     */
    public function addCrmAnnotationCorrectionAction() {
        $this->checkUserAuthentication();

        $this->layout('crm');
        $this->getDocumentTable();
        $search_annotation_form = new SearchAnnotationForm();
        $annotation_form = new AnnotationForm();

        $params = $this->params()->fromRoute();

        $mode = '';
        $passenger_id = '';
        if (isset($params['mode']) && $params['mode'] != '') {
            $mode = $params['mode'];
            $passenger_id = $params['passenger_id'];
        }

        $annotationResult = array();
        $pageAction = '';
        if (isset($params['id']) && $params['id'] != '') /* Edit case */ {
            $searchParam = array();
            $pageAction = $this->encrypt('edit');
            $searchParam['annotation_correction_id'] = $this->decrypt($params['id']);
            $annotationResult = $this->getDocumentTable()->searchUserAddedAnnotationCorrectionPassenger($searchParam);

            $search_annotation_form->get('passenger_id')->setAttribute('value', $annotationResult[0]['passenger_id']);
            $search_annotation_form->get('type')->setAttribute('value', $annotationResult[0]['activity_type']);
            $search_annotation_form->get('contact_name_id')->setAttribute('value', $annotationResult[0]['user_id']);
            $search_annotation_form->get('contact_name')->setAttribute('value', $annotationResult[0]['user_first_name'] . ' ' . $annotationResult[0]['user_last_name']);
        }

        $getSearchArray = array();
        $portArray = $this->getDocumentTable()->getArrivalPort($getSearchArray);
        $portList = array();
        $portList[''] = 'Select';
        foreach ($portArray as $key => $val) {
            $portList[$val['ship_arrival_port']] = $val['ship_arrival_port'];
        }
        $search_annotation_form->get('arrival_port')->setAttribute('options', $portList);

        $day = array();
        $day[''] = 'All';
        for ($i = 1; $i <= 31; $i++) {
            $day[$i] = $i;
        }
        $search_annotation_form->get('arrival_day')->setAttribute('options', $day);

        $arrivalYear = array();
        $arrivalYear[''] = 'Select';
        $arrivalYear['1891 - 1895'] = '1891 - 1895';
        for ($k = 1896; $k < 1930;) {
            $p = $k + 4;
            $arrivalYear[$k . ' - ' . $p] = $k . ' - ' . $p;
            $k = $k + 5;
        }
        $search_annotation_form->get('arrival_year')->setAttribute('options', $arrivalYear);

        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $viewModel->setVariables(array(
            'search_annotation_form' => $search_annotation_form,
            'annotation_form' => $annotation_form,
            'annotationResult' => $annotationResult,
            'annotation_correction_id' => isset($params['id']) ? $params['id'] : '',
            'pageAction' => $pageAction,
            'mode' => $this->decrypt($mode),
            'passenger_id' => $passenger_id,
            'passenger_id_encrypt' => $this->decrypt($passenger_id),
            'jsLangTranslate' => $this->_config['PassengerMessages']['config']['annotations'],
        ));
        return $viewModel;
    }

    /**
     * This Action is used to search the passenger for annotation and corrections
     * @return this will the annotation / correction listing
     * @author Icreon Tech - SR
     */
    public function getCrmAnnotationCorrectionPassengerAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        parse_str($request->getPost('searchString'), $searchParam);

        /** Sphinx Setting section */
        $sphinxServer = $this->_config['SphinxServer'];
        $sphinxComponentObj = new SphinxComponent($sphinxServer['serverName']);
        $sphinxComponentObj->initialize();
        $arrSphinxIndexes = $this->_config['SphinxServer']['passengerIndexes'];
        parse_str($request->getPost('searchString'), $searchParam);
        $limit = ($request->getPost('rows') != 0) ? $request->getPost('rows') : '1';
        $page = $request->getPost('page');
        $arrSphinxSettings = array('limit' => (int) $limit,
            'index' => $arrSphinxIndexes,
            'page' => (int) $request->getPost('page'),
            // 'groupby'        =>  array(array("sphinx_group_by" , '@weight DESC')),
            'rankingMode' => SPH_RANK_PROXIMITY_BM25, // SPH_RANK_PROXIMITY_BM25,
            'matchMode' => SPH_MATCH_EXTENDED2, //SPH_MATCH_EXTENDED2, //SPH_MATCH_BOOLEAN, //SPH_MATCH_EXTENDED2,
            'filter' => array()
        );
        $sphinxComponentObj->SetFieldWeights(array('field0' => 10000,
            'field3' => 5000,
            'field2' => 100,
            'field18' => 100,
            'field7' => 100,
            'field9' => 60,
            'field11' => 50,
            'field13' => 100,
            'field16' => 5000,
            'field25' => 200,
            'field26' => 100,
            'field27' => 1110,
            'field28' => 50,
            'field29' => 50,
            'field30' => 50,
            'field31' => 50,
            'field32' => 50,
            'field21' => 50,
            'field20' => 50
        ));
        $sphinxComponentObj->SetIndexWeights($this->_config['SphinxServer']['passengerIndexesIndexWeights']);
        $passenger_id = (isset($searchParam['passenger_id']) && $searchParam['passenger_id'] != '') ? trim($searchParam['passenger_id']) : '';
        $ship = (isset($searchParam['ship']) && $searchParam['ship'] != '') ? trim($searchParam['ship']) : '';
        $first_name = (isset($searchParam['first_name']) && $searchParam['first_name'] != '') ? trim($searchParam['first_name']) : '';
        $last_name = (isset($searchParam['last_name']) && $searchParam['last_name'] != '') ? trim($searchParam['last_name']) : '';
        $series = (isset($searchParam['series']) && $searchParam['series'] != '') ? trim($searchParam['series']) : '';
        $frame = (isset($searchParam['frame']) && $searchParam['frame'] != '') ? trim($searchParam['frame']) : '';
        $roll_number = (isset($searchParam['roll_number']) && $searchParam['roll_number'] != '') ? trim($searchParam['roll_number']) : '';
        $placeOfBirth = (isset($searchParam['place_of_birth']) && $searchParam['place_of_birth'] != '') ? $searchParam['place_of_birth'] : '';

        if (isset($searchParam['arrival_year']) && $searchParam['arrival_year'] != '') {
            $arr_year_arr = explode(' - ', $searchParam['arrival_year']);
            $arrival_year_from = $arr_year_arr[0];
            $arrival_year_to = $arr_year_arr[1];
        } else {
            $arrival_year_from = '';
            $arrival_year_to = '';
        }

        $day_arrival = (isset($searchParam['arrival_day']) && $searchParam['arrival_day'] != '') ? trim($searchParam['arrival_day']) : '';
        $month_arrival = (isset($searchParam['arrival_month']) && $searchParam['arrival_month'] != '') ? trim($searchParam['arrival_month']) : '';


        if (isset($searchParam['birth_year']) && $searchParam['birth_year'] != '') {
            $birth_year_arr = explode(' - ', $searchParam['birth_year']);
            $birth_year_from = $birth_year_arr[0];
            $birth_year_to = $birth_year_arr[1];
        } else {
            $birth_year_from = '';
            $birth_year_to = '';
        }

        if (isset($searchParam['current_age_range']) && $searchParam['current_age_range'] != '') {
            $age_arr = explode(' - ', $searchParam['current_age_range']);
            $current_age_from = $age_arr[0];
            $current_age_to = $age_arr[1];
        } else {
            $current_age_from = '';
            $current_age_to = '';
        }

        $gender = (isset($searchParam['gender']) && $searchParam['gender'] != '') ? trim($searchParam['gender']) : '';
        $maritial_status = (isset($searchParam['maritial_status']) && $searchParam['maritial_status'] != '') ? trim($searchParam['maritial_status']) : '';
        $dept_port = (isset($searchParam['dept_port']) && $searchParam['dept_port'] != '') ? trim($searchParam['dept_port']) : '';
        $arrival_port = (isset($searchParam['arrival_port']) && $searchParam['arrival_port'] != '') ? trim($searchParam['arrival_port']) : '';
        $ethnicity = (isset($searchParam['ethnicity']) && $searchParam['ethnicity'] != '') ? trim($searchParam['ethnicity']) : '';
        $last_residence = (isset($searchParam['last_residence']) && $searchParam['last_residence'] != '') ? trim($searchParam['last_residence']) : '';
        $search_type = (isset($searchParam['search_type']) && $searchParam['search_type'] != '') ? trim($searchParam['search_type']) : '';
        // $passenger_id = '100001010001';

        $arrSphinxSettingsStr = '';

        if (isset($arrival_year_from) && $arrival_year_from != '')
            $sphinxComponentObj->SetFilterRange('field19', $arrival_year_from, $arrival_year_to);

        if (isset($passenger_id) && $passenger_id != '')
            $arrSphinxSettingsStr.= ' & (@(field0) ("*' . $passenger_id . '*"))';
        if (isset($ship) && $ship != '')
            $arrSphinxSettingsStr.= ' & (@(field7) ("*' . $ship . '*"))';
        if (isset($first_name) && $first_name != '')
            $arrSphinxSettingsStr.= ' & (@(field2) ("*' . $first_name . '*"))';
        if (isset($last_name) && $last_name != '') {
            if ($search_type == 'exact') { //Exact Match
                $arrSphinxSettingsStr.= ' & (@(field1) ("_START_' . $last_name . '_END_"))';
            } else if ($search_type == 'startwith') { //Start with
                $arrSphinxSettingsStr.= ' & (@(field3) ("^' . $last_name . '*"))';
            } else if ($search_type == 'soundlike') { // sound like
                $arrSphinxSettingsStr.= ' & (@(field35) ("*' . soundex($last_name) . '*"))';
            }
            else
                $arrSphinxSettingsStr.= ' & (@(field3) ("*' . $last_name . '*"))';
        }
        if (isset($series) && $series != '')
            $arrSphinxSettingsStr.= ' & (@(field29) ("*' . $series . '*"))';
        if (isset($frame) && $frame != '')
            $arrSphinxSettingsStr.= ' & (@(field30) ("*' . $frame . '*"))';
        if (isset($roll_number) && $roll_number != '')
            $arrSphinxSettingsStr.= ' & (@(field31) ("*' . $roll_number . '*"))';

        if (!empty($placeOfBirth))
            $arrSphinxSettingsStr.= ' & (@(field26) ("*' . trim($placeOfBirth) . '*"))';

        if (isset($month_arrival) && $month_arrival != '')
            $sphinxComponentObj->setFilter('field20', array($month_arrival));
        // $arrSphinxSettingsStr.= ' & (@(field20) (*"' . $month_arrival . '"*))';
        if (isset($day_arrival) && $day_arrival != '')
            $sphinxComponentObj->setFilter('field21', array($day_arrival));
        //$arrSphinxSettingsStr.= ' & (@(field21) (*"' . $day_arrival . '"*))';
        if (isset($birth_year_from) && $birth_year_from != '')
            $sphinxComponentObj->SetFilterRange('field12', $birth_year_from, $birth_year_to);
        if (isset($current_age_from) && $current_age_from != '')
            $sphinxComponentObj->SetFilterRange('field33', $current_age_from, $current_age_to);

        if (isset($gender) && $gender != '')
            $arrSphinxSettingsStr.= ' & (@(field10) ("*' . $gender . '*"))';
        if (isset($maritial_status) && $maritial_status != '')
            $arrSphinxSettingsStr.= ' & (@(field11) ("*' . $maritial_status . '*"))';
        if (isset($dept_port) && $dept_port != '')
            $arrSphinxSettingsStr.= ' & (@(field24) ("*' . $dept_port . '*"))';
        if (isset($arrival_port) && $arrival_port != '')
            $arrSphinxSettingsStr.= ' & (@(field25) ("*' . $arrival_port . '*"))';
        if (isset($ethnicity) && $ethnicity != '') {
            $arrSphinxSettingsStr.= ' & (@(field36) ("*' . $ethnicity . '*"))';
        }
        if (isset($last_residence) && $last_residence != '')
            $arrSphinxSettingsStr.= ' & (@(field4) ("*' . $last_residence . '*"))';
        
        $arrSphinxSettings['search'] = $arrSphinxSettingsStr;
        $sphinxComponentObj->setSortMode(($request->getPost('sord') == 'desc') ? SPH_SORT_ATTR_DESC : SPH_SORT_ATTR_ASC, $request->getPost('sidx'));
        // $sphinxComponentObj->setSortMode(SPH_SORT_ATTR_DESC, $request->getPost('sidx'));
        $searchResult = $sphinxComponentObj->search($arrSphinxSettings);
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $searchResult['total'];
        $jsonResult['total'] = ceil($searchResult['total'] / $limit);
        $valueArray = array();
        $gender = '';
        if ($searchResult['total'] > 0) {
            foreach ($searchResult['matches'] as $key => $val) {
                $valueArray = $val['attrs'];
                $arrCell['id'] = $valueArray['field0'];
                $action = "<span class='plus-sign'><a href='javascript:void(0);' onClick=showPassengerDetails('" . trim($this->encrypt($valueArray['field0'])) . "','" . $this->encrypt('add') . "','" . $valueArray['field0'] . "');><div class='tooltip'>Add Annotation/Correction</div></span></a>";
                $gender = '';
                if ($valueArray['field10'] == 'M')
                    $gender = 'Male';
                else if ($valueArray['field10'] == 'F')
                    $gender = 'Female';
                else if ($valueArray['field10'] == 'U' || empty($valueArray['field10']))
                    $gender = 'Unknown';

                $manifestImage = "<a href='javascript:void(0)' style='text-decoration:underline' onClick=showPassengerMoreManifestImage('" . trim($this->encrypt($valueArray['field31'])) . "','" . trim($this->encrypt($valueArray['field30'])) . "','" . trim($this->encrypt($valueArray['field0'])) . "');>" . strtoupper($valueArray['field32']) . "</a>";

                $arrCell['cell'] = array($valueArray['field0'], $valueArray['field3'], $valueArray['field2'], $manifestImage, $gender, $valueArray['field14'], $action);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This Action is used to search the passenger annotation and corrections information
     * @param id of the annotation/ an/correction
     * @return this will be annotation/correction information
     * @author Icreon Tech - SR
     */
    public function getCrmAnnotationCorrectionInfoAction() {
        $this->checkUserAuthentication();
        $annotation_form = new AnnotationForm(); // zzzzzzzzzzzzzzzzzzzzzzzzzz
        $viewModel = new ViewModel();
        $this->getDocumentTable();
        $params = $this->params()->fromRoute();

        $request = $this->getRequest();
        $response = $this->getResponse();
        $searchParam = array();
        $firstTimeInUsArray = array('1' => 'Yes', '0' => 'No');
        $annotation_form->get('first_time_in_us')->setAttribute('options', $firstTimeInUsArray);

        $genderArray = $this->genderArray();
        $genderArr[''] = 'Select';
        foreach ($genderArray as $key => $val) {
            $genderArr[$key] = $val;
        }
        unset($genderArr['U']);
        $annotation_form->get('gender')->setAttribute('options', $genderArr);

        $maritalStatusArray = $this->maritalStatusArray();
        $maritalStatusArr[''] = 'Select';
        foreach ($maritalStatusArray as $key => $val) {
            $maritalStatusArr[$key] = $val;
        }
        unset($maritalStatusArr['U']);
        $annotation_form->get('marital_status')->setAttribute('options', $maritalStatusArr);


        $document = new Document($this->_adapter);
        if (isset($params['act']) && $this->decrypt($params['act']) == 'edit') {
            $searchParam['annotation_correction_id'] = $this->decrypt($params['id']);
            $searchParam = $document->exchangeAnnotationParam($searchParam);
            $searchResult = $this->getDocumentTable()->searchUserAddedAnnotationCorrectionPassenger($searchParam);
            $searchPassengerParam['passenger_id'] = $searchResult[0]['passenger_id'];

            $passengerSearchResult = $this->getDocumentTable()->searchCrmPassenger($searchPassengerParam);
            $pageAction = '';
            if (count($searchResult) > 0) {
                $pageAction = $this->encrypt('edit');

                $annotation_form->get('first_name')->setAttribute('value', $searchResult[0]['first_name']);
                $annotation_form->get('last_name')->setAttribute('value', $searchResult[0]['last_name']);
                $annotation_form->get('nationality')->setAttribute('value', $searchResult[0]['nationality']);
                $annotation_form->get('last_residence')->setAttribute('value', $searchResult[0]['last_residence']);
                $annotation_form->get('arrival_age')->setAttribute('value', $searchResult[0]['age_at_arrival']);
                $annotation_form->get('gender')->setAttribute('value', $searchResult[0]['gender']);
                $annotation_form->get('marital_status')->setAttribute('value', $searchResult[0]['maritial_status']);
                $annotation_form->get('arrival_date')->setAttribute('value', $this->DateFormat($searchResult[0]['arrival_date'], 'calender'));
                $annotation_form->get('ship')->setAttribute('value', $searchResult[0]['ship_name']);
                $annotation_form->get('port_departure')->setAttribute('value', $searchResult[0]['departure_port_name']);
                $annotation_form->get('line')->setAttribute('value', $searchResult[0]['line_no']);
                $annotation_form->get('birth_date')->setAttribute('value', $this->DateFormat($searchResult[0]['date_of_birth'], 'calender'));
                $annotation_form->get('death_date')->setAttribute('value', $this->DateFormat($searchResult[0]['date_of_death'], 'calender'));
                $annotation_form->get('occupation')->setAttribute('value', $searchResult[0]['occupation']);
                $annotation_form->get('spouse')->setAttribute('value', $searchResult[0]['spouse']);
                $annotation_form->get('children')->setAttribute('value', $searchResult[0]['children']);
                $annotation_form->get('us_relatives')->setAttribute('value', $searchResult[0]['relatives_in_us']);
                $annotation_form->get('us_residence')->setAttribute('value', $searchResult[0]['setteled_in_us']);
                $annotation_form->get('military_experience')->setAttribute('value', $searchResult[0]['military_experience']);
                $annotation_form->get('religious_community')->setAttribute('value', $searchResult[0]['religious_community']);
                $annotation_form->get('note')->setAttribute('value', $searchResult[0]['note']);
                $annotation_form->get('user_id')->setAttribute('value', $searchResult[0]['user_id']);
                $annotation_form->get('status')->setAttribute('value', $searchResult[0]['status']);
                $annotation_form->get('is_reauthorization')->setAttribute('value', $searchResult[0]['is_reauthorization']);

                if ($searchResult[0]['user_type'] == 2 || $searchResult[0]['user_type'] == 3) {
                    $annotation_form->get('contact_name')->setAttribute('value', $searchResult[0]['company_name']);
                } else {
                    $annotation_form->get('contact_name')->setAttribute('value', $searchResult[0]['user_first_name'] . ' ' . $searchResult[0]['user_middle_name'] . ' ' . $searchResult[0]['user_last_name']);
                }

                $annotation_form->get('contact_name_id')->setAttribute('value', $searchResult[0]['user_id']);
                $annotation_form->get('type')->setAttribute('value', $searchResult[0]['activity_type']);
                $annotation_form->get('birth_place')->setAttribute('value', $searchResult[0]['birth_place']);

                if (!empty($searchResult[0]['date_of_marriage']) && $searchResult[0]['date_of_marriage'] != '0000-00-00')
                    $annotation_form->get('date_of_marriage')->setAttribute('value', $this->DateFormat($searchResult[0]['date_of_marriage'], 'calender'));
                $annotation_form->get('parent_names')->setAttribute('value', $searchResult[0]['parent_names']);
                $annotation_form->get('first_time_in_us')->setAttribute('value', $searchResult[0]['first_time_in_us']);

                $emailSendChecked = false;
                if ($searchResult[0]['email_send'] == 1)
                    $emailSendChecked = true;
                else
                    $emailSendChecked = false;
                $annotation_form->get('email_send')->setAttribute('checked', $emailSendChecked);
                $annotation_form->get('religious_community')->setAttribute('value', $searchResult[0]['religious_community']);

                if ($searchResult[0]['activity_type'] == 2) { // for correction
                    $statusArray = array('0' => 'Pending', '1' => 'Completed');
                    $annotation_form->get('status')->setAttribute('options', $statusArray);
                } else if ($searchResult[0]['activity_type'] == 1) { // for annotations
                    $statusArray = array('0' => 'Hidden', '1' => 'Shown');
                    $annotation_form->get('status')->setAttribute('options', $statusArray);
                }
            } else {
                $pageAction = $this->encrypt('add');
            }
        } else {
            $pageAction = $this->encrypt('add');
            $searchParam = array();
            $searchParam['passenger_id'] = $this->decrypt($params['id']);
            $passengerSearchResult = $this->getDocumentTable()->searchCrmPassenger($searchParam);
            //asd($passengerSearchResult);
        }

        if ($request->isPost()) {
            $messages = array();
            $annotation_form->setInputFilter($document->getAnnotationInputFilter());
            $annotation_form->setData($request->getPost());

            if (!$annotation_form->isValid()) {
                $msg = array();
                $messages = array();
                if (!empty($msg))
                    $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {

                $postData = $annotation_form->getData();

//                asd($postData);
                $validityFlag = 1;
                foreach ($postData as $key => $val) {
                    if ($key != 'activity_type' && $key != 'save' && $key != 'email_send' && $key != 'status' && $key != 'terms' && $key != 'user_id' && $key != 'type' && $key != 'contact_name_id' && $key != 'contact_name') {
                        if ($val != '')
                            $validityFlag = 0;
                    }
                }

                $insertData = array();
                if (isset($params['act']) && $this->decrypt($params['act']) == 'edit') {
                    if ($validityFlag == 0) {
                        $insertData['annotation_correction_id'] = $searchParam['annotation_correction_id'];
                        $insertData['modified_date'] = DATE_TIME_FORMAT;
                        $insertData['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                        $insertData['email_send'] = $postData['email_send'];
                        $insertData['note'] = $postData['note'];
                        $insertData['status'] = $postData['status'];
                        $insertData['first_name'] = $postData['first_name'];
                        $insertData['last_name'] = $postData['last_name'];
                        $insertData['nationality'] = $postData['nationality'];
                        $insertData['last_residence'] = isset($postData['last_residence']) ? $postData['last_residence'] : '';
                        $insertData['birth_place'] = isset($postData['birth_place']) ? $postData['birth_place'] : '';
                        $insertData['arrival_age'] = $postData['arrival_age'];
                        $insertData['gender'] = $postData['gender'];
                        $insertData['marital_status'] = $postData['marital_status'];
                        $insertData['ship'] = $postData['ship'];
                        $insertData['port_departure'] = $postData['port_departure'];
                        if (!empty($postData['line']))
                            $insertData['line'] = sprintf('%04d', $postData['line']);
                        else
                            $insertData['line'] = '';
                        $insertData['arrival_date'] = $this->DateFormat($postData['arrival_date'], 'db_datetime_format');

                        if ($postData['activity_type'] == 1) {  // for annotation
                            $insertData['birth_date'] = (isset($postData['birth_date']) && $postData['birth_date'] != '') ? $this->DateFormat($postData['birth_date'], 'db_datetime_format') : '';
                            $insertData['death_date'] = (isset($postData['death_date']) && $postData['death_date'] != '') ? $this->DateFormat($postData['death_date'], 'db_datetime_format') : '';
                            $insertData['occupation'] = $postData['occupation'];
                            $insertData['spouse'] = $postData['spouse'];
                            $insertData['children'] = $postData['children'];
                            $insertData['us_relatives'] = $postData['us_relatives'];
                            $insertData['us_residence'] = $postData['us_residence'];
                            $insertData['military_experience'] = $postData['military_experience'];
                            $insertData['religious_community'] = $postData['religious_community'];
                            $insertData['arrival_date'] = (isset($postData['arrival_date']) && $postData['arrival_date'] != '') ? $this->DateFormat($postData['arrival_date'], 'db_datetime_format') : '';
                            $insertData['activity_type'] = $postData['activity_type'];
                            $insertData['added_date'] = DATE_TIME_FORMAT;
                            $insertData['added_by'] = $this->auth->getIdentity()->crm_user_id;
                            $insertData['user_id'] = $postData['user_id'];
                            $insertData['activity_type'] = $postData['activity_type'];
                            $insertData['date_of_marriage'] = (isset($postData['date_of_marriage']) && $postData['date_of_marriage'] != '') ? $this->DateFormat($postData['date_of_marriage'], 'db_datetime_format') : '';
                            $insertData['parent_names'] = $postData['parent_names'];
                            $insertData['first_time_in_us'] = (isset($postData['first_time_in_us']) && $postData['first_time_in_us'] != '') ? $postData['first_time_in_us'] : 'NULL';
                            $insertData['is_reauthorization'] = (isset($postData['is_reauthorization']) && $postData['is_reauthorization'] != '') ? $postData['is_reauthorization'] : '0';
                        } else {
                            $insertData['user_id'] = $postData['user_id'];
                            $insertData['activity_type'] = $postData['activity_type'];
                            $insertData['first_name_status'] = isset($postData['first_name_status']) ? $postData['first_name_status'] : '';
                            $insertData['last_name_status'] = isset($postData['last_name_status']) ? $postData['last_name_status'] : '';
                            $insertData['nationality_status'] = isset($postData['nationality_status']) ? $postData['nationality_status'] : '';

                            $insertData['last_residence_status'] = isset($postData['last_residence_status']) ? $postData['last_residence_status'] : '';
                            $insertData['birth_place_status'] = isset($postData['birth_place_status']) ? $postData['birth_place_status'] : '';


                            $insertData['age_at_arrival_status'] = isset($postData['age_at_arrival_status']) ? $postData['age_at_arrival_status'] : '';
                            $insertData['arrival_date_status'] = isset($postData['arrival_date_status']) ? $postData['arrival_date_status'] : '';
                            $insertData['gender_status'] = isset($postData['gender_status']) ? $postData['gender_status'] : '';
                            $insertData['maritial_approve_status'] = isset($postData['maritial_approve_status']) ? $postData['maritial_approve_status'] : '';
                            $insertData['ship_name_status'] = isset($postData['ship_name_status']) ? $postData['ship_name_status'] : '';
                            $insertData['departure_port_name_status'] = isset($postData['departure_port_name_status']) ? $postData['departure_port_name_status'] : '';
                            $insertData['line_no_status'] = isset($postData['line_no_status']) ? $postData['line_no_status'] : '';
                        }

                        if ($insertData['status'] == 0) { // pending status
                            if ($postData['activity_type'] == 1) {  // for annotation
                                $this->getDocumentTable()->updateAnnotationPending($insertData);
                            } else {
                                $this->getDocumentTable()->updateCorrectionPending($insertData);
                            }
                        } else if ($insertData['status'] == 1) { // complete status
                            $insertData['passenger_id'] = $searchResult[0]['passenger_id'];

                            if ($postData['activity_type'] == 2) {  // for correction
                                $passengerArray = array();
                                $passengerArray['passenger_id'] = $insertData['passenger_id'];
                                $passengerArray['first_name'] = (isset($postData['first_name_status']) && $postData['first_name_status'] == 2) ? $insertData['first_name'] : '';
                                $passengerArray['last_name'] = (isset($postData['last_name_status']) && $postData['last_name_status'] == 2) ? $insertData['last_name'] : '';
                                $passengerArray['nationality'] = (isset($postData['nationality_status']) && $postData['nationality_status'] == 2) ? $insertData['nationality'] : '';
                                $passengerArray['last_residence'] = (isset($postData['last_residence_status']) && $postData['last_residence_status'] == 2) ? $insertData['last_residence'] : '';
                                $passengerArray['arrival_age'] = (isset($postData['age_at_arrival_status']) && $postData['age_at_arrival_status'] == 2) ? $insertData['arrival_age'] : '';
                                $passengerArray['arrival_date'] = (isset($postData['arrival_date_status']) && $postData['arrival_date_status'] == 2) ? $insertData['arrival_date'] : '';
                                $passengerArray['gender'] = (isset($postData['gender_status']) && $postData['gender_status'] == 2) ? $insertData['gender'] : '';
                                $passengerArray['maritial_status'] = (isset($postData['maritial_approve_status']) && $postData['maritial_approve_status'] == 2) ? $insertData['marital_status'] : '';
                                $passengerArray['ship'] = (isset($postData['ship_name_status']) && $postData['ship_name_status'] == 2) ? $insertData['ship'] : '';
                                $passengerArray['port_departure'] = (isset($postData['departure_port_name_status']) && $postData['departure_port_name_status'] == 2) ? $insertData['port_departure'] : '';
                                $passengerArray['line'] = (isset($postData['line_no_status']) && $postData['line_no_status'] == 2) ? $insertData['line'] : '';
                                $passengerArray['birth_place'] = (isset($postData['birth_place_status']) && $postData['birth_place_status'] == 2) ? $insertData['birth_place'] : '';
                                $this->getDocumentTable()->updateCorrectionComplete($passengerArray);
                                $this->getDocumentTable()->updateCorrectionPending($insertData);
                            } else {
                                $this->getDocumentTable()->updateAnnotationPending($insertData);
                                $this->getDocumentTable()->updateAnnotationComplete($insertData);
                            }

                            // Send Email Notification
                            if ($postData['email_send'] == 1) {
                                $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $postData['user_id']));
                                if ($userDetail['user_type'] != 1) {
                                    $userDetail['first_name'] = $userDetail['company_name'];
                                }
                                $userDataArr = $userDetail;
                                $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                                $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                                $userDataArr['passenger_id'] = $searchResult[0]['passenger_id'];
                                $userDataArr['note'] = nl2br($postData['note']);
                                $updateEmailTemplateId = 35;
                                $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $updateEmailTemplateId, $this->_config['email_options']);
                            }
                            // End Send Email Notification
                        }
                        $this->flashMessenger()->addMessage('Passenger details updated successfully!');
                    } else {

                        if ($postData['activity_type'] == 1)
                            $msg['save'] = $this->_config['PassengerMessages']['config']['common']['ATLEAST_ONE_VALUE_ERROR'];
                        else if ($postData['activity_type'] == 2)
                            $msg['line'] = $this->_config['PassengerMessages']['config']['common']['ATLEAST_ONE_VALUE_ERROR'];

                        $messages = array('status' => "error", 'message' => $msg);
                        $response->setContent(\Zend\Json\Json::encode($messages));
                        return $response;
                    }
                } else {
                    if ($validityFlag == 0) {
                        $insertData = array();
                        $insertData['passenger_id'] = $searchParam['passenger_id'];
                        $insertData['first_name'] = $postData['first_name'];
                        $insertData['last_name'] = $postData['last_name'];
                        $insertData['nationality'] = $postData['nationality'];
                        $insertData['last_residence'] = isset($postData['last_residence']) ? $postData['last_residence'] : '';
                        $insertData['birth_place'] = isset($postData['birth_place']) ? $postData['birth_place'] : '';
                        $insertData['arrival_age'] = $postData['arrival_age'];
                        $insertData['gender'] = $postData['gender'];
                        $insertData['marital_status'] = $postData['marital_status'];
                        $insertData['ship'] = $postData['ship'];
                        $insertData['port_departure'] = $postData['port_departure'];
                        //$insertData['line'] = sprintf('%04d',$postData['line']);

                        if (!empty($postData['line']))
                            $insertData['line'] = sprintf('%04d', $postData['line']);
                        else
                            $insertData['line'] = '';


                        $insertData['birth_date'] = (isset($postData['birth_date']) && $postData['birth_date'] != '') ? $this->DateFormat($postData['birth_date'], 'db_datetime_format') : '';
                        $insertData['death_date'] = (isset($postData['death_date']) && $postData['death_date'] != '') ? $this->DateFormat($postData['death_date'], 'db_datetime_format') : '';
                        $insertData['occupation'] = $postData['occupation'];
                        $insertData['spouse'] = $postData['spouse'];
                        $insertData['children'] = $postData['children'];
                        $insertData['us_relatives'] = $postData['us_relatives'];
                        $insertData['us_residence'] = $postData['us_residence'];
                        $insertData['military_experience'] = $postData['military_experience'];
                        $insertData['religious_community'] = $postData['religious_community'];
                        $insertData['arrival_date'] = (isset($postData['arrival_date']) && $postData['arrival_date'] != '') ? $this->DateFormat($postData['arrival_date'], 'db_datetime_format') : '';
                        $insertData['activity_type'] = $postData['activity_type'];
                        $insertData['added_date'] = DATE_TIME_FORMAT;
                        $insertData['added_by'] = $this->auth->getIdentity()->crm_user_id;
                        $insertData['user_id'] = $postData['user_id'];
                        $insertData['date_of_marriage'] = (isset($postData['date_of_marriage']) && $postData['date_of_marriage'] != '') ? $this->DateFormat($postData['date_of_marriage'], 'db_datetime_format') : '';
                        $insertData['parent_names'] = $postData['parent_names'];
                        $insertData['first_time_in_us'] = (isset($postData['first_time_in_us']) && $postData['first_time_in_us'] != '') ? $postData['first_time_in_us'] : 'NULL';
                        if ($postData['activity_type'] == 1) {  // for annotation
                            $insertData['status'] = '1';
                        } else if ($postData['activity_type'] == 2) {  // for annotation
                            $insertData['status'] = '0';
                        }

                        $this->getDocumentTable()->insertAnnotation($insertData);
                        $this->flashMessenger()->addMessage('Passenger details saved successfully!');
                    } else {
                        if ($postData['activity_type'] == 1)
                            $msg['save'] = $this->_config['PassengerMessages']['config']['common']['ATLEAST_ONE_VALUE_ERROR'];
                        else if ($postData['activity_type'] == 2)
                            $msg['line'] = $this->_config['PassengerMessages']['config']['common']['ATLEAST_ONE_VALUE_ERROR'];

                        $messages = array('status' => "error", 'message' => $msg);
                        $response->setContent(\Zend\Json\Json::encode($messages));
                        return $response;
                    }
                }
                $messages = array('status' => "success", 'message' => $this->_config['PassengerMessages']['config']['common']['UPDATE_CONFIRM_MSG']);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        if (isset($params['act']) && $this->decrypt($params['act']) == 'edit') {
            $passenger_id_orignal = $searchPassengerParam['passenger_id'];
        } else {
            $passenger_id_orignal = $searchParam['passenger_id'];
        }

        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'resultSet' => $passengerSearchResult[0],
            'annCorrResult' => isset($params['act']) && $this->decrypt($params['act']) == 'edit' ? $searchResult[0] : '',
            'passenger_id' => $this->encrypt($searchParam['passenger_id']),
            'passenger_id_orignal' => $passenger_id_orignal,
            'annotation_correction_id' => isset($params['act']) && $this->decrypt($params['act']) == 'edit' ? $this->encrypt($searchResult[0]['annotation_correction_id']) : '',
            'annotation_correction_id_orignal' => isset($params['act']) && $this->decrypt($params['act']) == 'edit' ? $searchResult[0]['annotation_correction_id'] : '',
            'jsLangTranslate' => $this->_config['PassengerMessages']['config']['annotations'],
            'annotation_form' => $annotation_form,
            'pageAction' => $pageAction,
            'addEditAction' => $this->decrypt($pageAction),
        ));
        return $viewModel;
    }

    /**
     * This Action is used to update ship name in bulk
     * @return this will be confirmation of deletion
     * @author Icreon Tech - SK
     */
    public function editCrmBulkshipAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getDocumentTable();
        $update_shipname_form = new UpdateShipnameForm();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $document = new Document($this->_adapter);
        $messages = '';
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }

        if ($request->isPost()) {

            // $update_shipname_form->setInputFilter($document->getShiplineInputFilter());
            $update_shipname_form->setData($request->getPost());

            if (!$update_shipname_form->isValid()) {
                $msg = array();
                $errors = $update_shipname_form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['PassengerMessages']['config']['update_shipname_bulk'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                parse_str($request->getPost('searchString'), $search_param);
                $formData = $update_shipname_form->getData();
                $document->exchangeShipArray($formData);
                $postData = array();
                $postData['old_ship_name'] = $formData['old_ship_name'];
                $postData['new_ship_name'] = $formData['new_ship_name'];

                $updateResult = $this->getDocumentTable()->updateCrmBulkship($postData);


                /** end insert into the change log */
                if ($updateResult[0]['Message'] == 'DONE') {

                    $messages = array('status' => "success");
                    $this->flashMessenger()->addMessage($this->_config['PassengerMessages']['config']['update_shipname_bulk']['UPDATE_CONFIRM_MSG']);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                } else {
                    $messages = array('status' => "error");
                    $this->flashMessenger()->addMessage($this->_config['PassengerMessages']['config']['update_shipname_bulk']['UPDATE_ERROR_MSG']);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        }
        $viewModel->setVariables(array(
            'update_shipname_form' => $update_shipname_form,
            'jsLangTranslate' => $this->_config['PassengerMessages']['config']['update_shipname_bulk'],
            'messages' => $messages,
        ));
        return $viewModel;
    }

    /**
     * This Action is used to search the passenger for update in bulk
     * @author Icreon Tech - SK
     */
    public function getCrmBulkSearchPassengerAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $search_passenger_form = new CrmSearchBulkPassengerForm();
        $update_passenger_form = new CrmBulkPassengerUpdateForm();

        $messageArray = array_merge($this->_config['PassengerMessages']['config']['passenger_search'], $this->_config['PassengerMessages']['config']['edit_passenger'], $this->_config['PassengerMessages']['config']['common']);

        /** Series */
        $passengerSeries = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPassengerSeries();
        $seriesList = array();
        $seriesList[''] = 'Select';
        foreach ($passengerSeries as $key => $val) {
            $seriesList[$val['series']] = $val['series'];
        }

        $search_passenger_form->get('series')->setAttribute('options', $seriesList);
        /** End Series */
        $viewModel = new ViewModel();
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $viewModel->setVariables(array(
            'search_passeneger_form' => $search_passenger_form,
            'update_passenger_form' => $update_passenger_form,
            'messages' => $messages,
            'jsLangTranslate' => $messageArray
        ));
        return $viewModel;
    }

    /**
     * This Action is used to show the Mainfest Image
     * @param filename
     * @return this will be filepath
     * @author Icreon Tech - SR
     */
    public function showManifestImageAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $mainFileName = $this->decrypt($params['filename']);
        $folderName = substr($mainFileName, 0, 9);
		$assetsPath = $config['file_upload_path']['assets_url'];
        $manifestPath = $assetsPath.'manifest/';
        $imagePath = $manifestPath . $folderName . "/" . $mainFileName;
        $largeImagePath = $manifestPath . $folderName . "/" . $mainFileName;
       // $largeImagePath = str_replace('\\', "/", $largeImagePath);
        $manifestParam = array();
        $shipManifestResult = array();
        $defaultImageIndex = 0;
        $shipManifestFilenameArray = array();
        $shipManifestArray = array();
        $passengerResult = array();
        $passengerId = '';
        $startFrame = '';
        if (isset($params['ship_name'])) {
            $defaultImageIndex = 0; // for ship default index will be 0
            if (isset($params['passenger_id']) && $params['passenger_id'] != '') { // for passenger it may change
                $passengerId = $params['passenger_id'];
                // $defaultImageIndex = array_search(strtoupper($mainFileName), $shipManifestFilenameArray);
                $searchPassengerParam['passenger_id'] = $this->decrypt($params['passenger_id']);
                $passengerResult = $this->getDocumentTable()->searchCrmPassenger($searchPassengerParam);
                $passengerResult = $passengerResult[0];
            }

            // $passengerResult['ROLL_NBR'] = 'T715-004857217'; // need to uncomment it
            //asd($passengerResult);
            $manifestParam['shipName'] = $this->decrypt($params['ship_name']);
            $manifestParam['dateArrive'] = substr($this->decrypt($params['arrival_date']), 0, 10);
            // for ship name having (1980) in name
			if (!empty($manifestParam['shipName'])) {
				$searchParam['ship_name'] = $manifestParam['shipName'];
				// for ship name having (1980) in name
				$shipNameString = explode('(', substr($searchParam['ship_name'], -6));
				$shipNameString = substr(@$shipNameString[1], 0, 4);
				if (is_numeric($shipNameString) == true)
					$manifestParam['shipName'] = trim(substr($searchParam['ship_name'], 0, -6));
				// end // for ship name having (1980) in name
			}
            if (!empty($params['ship_name'])) {    
				$shipNameString = explode('(', substr($params['ship_name'], -6));
				$shipNameString = substr(@$shipNameString[1], 0, 4);
				if (is_numeric($shipNameString) == true)
					$manifestParam['shipName'] = trim(substr($params['ship_name'], 0, -6));
            }

            if ($passengerResult['data_source'] == 1) {
                //$shipManifestResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getShipManifest($manifestParam);
                $shipManifestFrameResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getShipManifestFrame($manifestParam);
                $shipManifestArray = array();
                foreach ($shipManifestFrameResult as $mKey => $mVal) {
                    $manifestParam['frame'] = $mVal['ROLL_NUMBER'];
                    $shipManifestResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getShipManifestRecords($manifestParam);

                    //$manifestPath = $this->_config['Image_path']['manifestPath'];
                    if (count($shipManifestResult) > 0 && !isset($shipManifestResult[0]['Message'])) {
                        $startFrame = $shipManifestResult[0]['START_RANGE'];
                        $endFrame = $shipManifestResult[0]['END_RANGE'];

                        $rollNbr = $shipManifestResult[0]['ROLL_NBR'];
                        for ($k = $startFrame; $k <= $endFrame; $k++) {
                            $fileName = $rollNbr . sprintf('%04d', $k) . '.TIF';
                            $folderName = substr($fileName, 0, 9);
                            //$shipManifestArray[] = $manifestPath . $folderName . "/" . $fileName . "&S=.4"; // commented on 10 july for new image path 
                            $shipManifestArray[] = $fileName;
                            $shipManifestFilenameArray[] = strtoupper($fileName);
                        }
                    }
                }
            } else if ($passengerResult['data_source'] == 2) {
                $manifestParam['dataSource'] = 2;
                
                $dateArray = date_parse_from_format("Y-m-d", substr($this->decrypt($params['arrival_date']), 0, 10));
                $manifestParam['dateArriveFrom'] = $dateArray['year'].'-01-01';
                $manifestParam['dateArriveTo'] = $dateArray['year'].'-12-31';   

                $manifestFileNameArray = explode('_', $mainFileName);
                $manifestParam['fileNamePattern'] = $manifestFileNameArray[0];

                $shipManifestResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getFamilyShipManifest($manifestParam);
                
                //$manifestPath = $this->_config['Image_path']['manifestPath'];
                if (count($shipManifestResult) > 0 && !isset($shipManifestResult[0]['Message'])) {
                    $shipManifestArray = array();

                    $startFrame = $shipManifestResult[0]['START_RANGE'];
                    $endFrame = $shipManifestResult[0]['END_RANGE'];
                    $shipManifestArray = array();

                    $rollNbr = $shipManifestResult[0]['ROLL_NBR'];
                    for ($k = $startFrame; $k <= $endFrame; $k++) {
                        $fileName = substr($rollNbr, 5) . '_' . sprintf('%05d', $k) . '.jpg';
                        $folderName = substr($fileName, 0, 9);
                        $shipManifestArray[] = $fileName;
                        $shipManifestFilenameArray[] = strtoupper($fileName);
                    }
                    /*
                      foreach ($shipManifestResult as $key => $val) {
                      $shipManifestArray[] = $val['FILENAME'];
                      $shipManifestFilenameArray[] = strtoupper($val['FILENAME']);
                      }
                     */
                }
            }
            $manifestImageType = 'ship';
        } else {
            $manifestImageType = 'passenger';
        }

        if (count($shipManifestArray) == 1) {
            if ($passengerResult['data_source'] == 2) {
                $imagePartArray = explode('_', $shipManifestArray[0]);
                $sFileName = $imagePartArray[0] . '_' . sprintf('%05d', (substr($imagePartArray[1], 0, 5) + 1)) . '.jpg';
                $shipManifestFilenameArray[] = $sFileName;
                $shipManifestArray[] = $sFileName;
            }
        }

        if (!in_array(strtoupper($mainFileName), $shipManifestFilenameArray)) {
            $shipManifestArray[] = $mainFileName;
            $shipManifestFilenameArray[] = strtoupper($mainFileName);
        }

        $defaultImageIndex = 0; // for ship default index will be 0
        if (isset($params['passenger_id']) && $params['passenger_id'] != '') { // for passenger it may change
            $defaultImageIndex = array_search(strtoupper($mainFileName), $shipManifestFilenameArray);
        }
        $shipManifestArray = array_unique($shipManifestArray);
        $shipManifestFilenameArray = array_unique($shipManifestFilenameArray);


        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'imagePath' => $imagePath,
            'largeImagePath' => $largeImagePath,
            'shipManifestArray' => $shipManifestArray,
            'shipManifestFilenameArray' => $shipManifestFilenameArray,
            'defaultImageIndex' => $defaultImageIndex,
            'manifestImageType' => $manifestImageType,
            'passengerResult' => $passengerResult,
            'jsLangTranslate' => $this->_config['PassengerMessages']['config']['edit_passenger'],
            'passengerId' => $passengerId,
            'rollNumber' => $folderName,
            'frameNumber' => $startFrame,
            'uploadFilePath' => $this->_config['file_upload_path']['assets_upload_dir']
        ));
        return $viewModel;
    }

    /**
     * This Action is used to delete the the annotation and correction
     * @param this will id
     * @return this will be a confirmation message
     * @author Icreon Tech - SR
     */
    public function deleteAnnotationCorrectionAction() {
        $this->checkUserAuthentication();

        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['id'] = $this->decrypt($request->getPost('id'));
            $dataParam['isDelete'] = 1;
            $dataParam['modifiend_date'] = DATE_TIME_FORMAT;
            $this->getDocumentTable()->deleteCrmAnnotaionCorrection($dataParam);
            $this->flashMessenger()->addMessage('Passenger record for annotation and correction deleted successfully!');
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This Action is used to to get passenger Id auto suggest
     * @return this will be a all passenger id 
     * @author Icreon Tech - SR
     */
    public function getPassengerAction() {
        //$this->checkUserAuthentication();
        /* Contacts */
        $response = $this->getResponse();
        $request = $this->getRequest();
        $postArray = $request->getPost()->toArray();
        $postArray['startIndex'] = 0;
        $postArray['recordLimit'] = 10;
        $postArray['sortField'] = 'ID';
        $postArray['sortOrder'] = 'ASC';
        $resultarray = $this->getDocumentTable()->getPassengerRecord($postArray);
        $allRecords = array();
        $i = 0;
        if ($resultarray !== false) {
            foreach ($resultarray as $resultarray) {
                $allRecords[$i] = array();
                // $all_contacts[$i]['ID'] = $contact['ID'];
                $allRecords[$i]['ID'] = $resultarray['ID'];
                $i++;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($allRecords));
        return $response;
        /* End Contacts */
    }

    /**
     * This Action is used to edit the passenger details
     * @param this will be id
     * @return this will edit the passenger details
     * @author Icreon Tech - SR
     */
    public function editCrmPassengerDetailsAction() {
        $this->checkUserAuthentication();

        // $this->layout('crm');
        $this->getDocumentTable();
        $edit_passenger_form = new EditPassengerForm();
        //  $request = $this->getRequest();
        // $document = new Document($this->_adapter);
        $params = $this->params()->fromRoute();
        // $response = $this->getResponse();

        $searchParam['passenger_id'] = $this->decrypt($params['id']);
        $passengerResult = $this->getDocumentTable()->searchCrmPassenger($searchParam);

        $passParam = array();
        $passengerEthnicity = array();
        $passParam['passengerId'] = $this->decrypt($params['id']);
        $passengerEthnicity = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getPassengerEthnicity($passParam);
        $edit_passenger_form->get('passenger_id')->setAttribute('value', $passengerResult[0]['ID']);
        $edit_passenger_form->get('batch')->setAttribute('value', $passengerResult[0]['UDE_BATCH_NUMBER']);
        $edit_passenger_form->get('ref_line')->setAttribute('value', $passengerResult[0]['REF_PAGE_LINE_NBR']);
        $edit_passenger_form->get('ref_page')->setAttribute('value', $passengerResult[0]['REF_PAGE_NBR']);
        $edit_passenger_form->get('arrival_date')->setAttribute('value', $this->DateFormat($passengerResult[0]['DATE_ARRIVE'], 'calender'));

        $edit_passenger_form->get('last_name')->setAttribute('value', $passengerResult[0]['PRIN_LAST_NAME']);
        $edit_passenger_form->get('first_name')->setAttribute('value', $passengerResult[0]['PRIN_FIRST_NAME']);
        $edit_passenger_form->get('print_age')->setAttribute('value', $passengerResult[0]['PRIN_AGE_ARRIVAL']);
        $edit_passenger_form->get('birth_year')->setAttribute('value', $passengerResult[0]['BIRTH_YEAR']);
        $edit_passenger_form->get('gender')->setAttribute('value', $passengerResult[0]['PRIN_GENDER_CODE']);
        $edit_passenger_form->get('roll_number')->setAttribute('value', $passengerResult[0]['ROLL_NBR']);
        $edit_passenger_form->get('nationality')->setAttribute('value', $passengerResult[0]['PRIN_NATIONALITY']);
        $edit_passenger_form->get('file_name')->setAttribute('value', $passengerResult[0]['FILENAME']);
        $edit_passenger_form->get('data_source')->setAttribute('value', $passengerResult[0]['data_source']);
        $edit_passenger_form->get('port_of_departure')->setAttribute('value', $passengerResult[0]['DEP_PORT_NAME']);

        $series = '';
        if (!empty($passengerResult[0]['ROLL_NBR']))
            $series = substr($passengerResult[0]['ROLL_NBR'], 0, 4);
        else
            $series = '';

        $edit_passenger_form->get('series')->setAttribute('value', $series);
        $edit_passenger_form->get('frame')->setAttribute('value', $passengerResult[0]['FRAME']);

        $maritial_status = $passengerResult[0]['PRIN_MARITAL_STAT'];
        if (empty($maritial_status))
            $maritial_status = 'U';
        $edit_passenger_form->get('maritial_status')->setAttribute('value', $maritial_status);
        $edit_passenger_form->get('ship')->setAttribute('value', $passengerResult[0]['SHIP_NAME']);
        $edit_passenger_form->get('age_arrival')->setAttribute('value', $passengerResult[0]['AGE_ARRIVE']);
        $edit_passenger_form->get('ethnicity')->setAttribute('value', $passengerEthnicity[0]['ethnicity']);
        $edit_passenger_form->get('residence')->setAttribute('value', $passengerResult[0]['PRIN_PLACE_RESI']);
        $edit_passenger_form->get('place_of_birth')->setAttribute('value', $passengerResult[0]['PLACE_OF_BIRTH']);
        $edit_passenger_form->get('ship_name_iss')->setAttribute('value', $passengerResult[0]['SHIP_NAME_ISS']);
        //asd($passengerResult[0]['SHIP_ARRIVAL_DATE']);
        //echo sprintf("%02s", $this->DateFormat($passengerResult[0]['DATE_ARRIVE'], 'monthtodateformat'));
        //die;

        $edit_passenger_form->get('ship_arrival_date')->setAttribute('value', $this->DateFormat($passengerResult[0]['DATE_ARRIVE'], 'calender'));
        $ship_arrival_date_value = $this->DateFormat($passengerResult[0]['DATE_ARRIVE'], 'monthtodateformat');

        $citizenUs = false;
        if (!empty($passengerResult[0]['PRIN_CITIZEN_US_IND']) && strtolower($passengerResult[0]['PRIN_CITIZEN_US_IND']) == 'x') {
            $citizenUs = true;
        }
        $edit_passenger_form->get('us_citizen')->setAttribute('checked', $citizenUs);

        $crewShip = false;
        if (!empty($passengerResult[0]['PRIN_CREW_SHIP_IND']) && strtolower($passengerResult[0]['PRIN_CREW_SHIP_IND']) == 'x') {
            $crewShip = true;
        }
        $edit_passenger_form->get('crew_member')->setAttribute('checked', $crewShip);



        $viewModel = new ViewModel();
        //$this->setValueForElement($passengerResult[0],$edit_passenger_form);
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'edit_passeneger_form' => $edit_passenger_form,
            'ID' => $passengerResult[0]['ID'],
            'AGE_ARRIVE' => $passengerResult[0]['AGE_ARRIVE'],
            'PRINT_AGE' => $passengerResult[0]['PRIN_AGE_ARRIVAL'],
            'batch' => $passengerResult[0]['UDE_BATCH_NUMBER'],
            'data_source' => $passengerResult[0]['data_source'],
            'jsLangTranslate' => $this->_config['PassengerMessages']['config']['edit_passenger'],
            'passenger_id' => $params['id'],
            'moduleName' => $this->encrypt('passenger'),
            'ship_arrival_date_value' => $ship_arrival_date_value
        ));
        return $viewModel;
    }

    /**
     * This Action is used to to view the passenger annotation/correction details
     * @param this will be id
     * @return this will be the passenger id
     * @author Icreon Tech - SR
     */
    public function getCrmPassengerAnnotationAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $params = $this->params()->fromRoute();
        $searchParam['passenger_id'] = $this->decrypt($params['id']);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'passenger_id' => $params['id'],
            'jsLangTranslate' => $this->_config['PassengerMessages']['config']['edit_passenger']
        ));
        return $viewModel;
    }

    /**
     * This Action is used to view the passenger annotation/correction details in tabbing
     * @return this will be the list of annotations/corrections
     * @author Icreon Tech - SR
     */
    public function getCrmPassengerAnnotationListAction() {
        $this->checkUserAuthentication();
        //$form = new SearchDocumentForm();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();

        parse_str($request->getPost('searchString'), $searchParam);

        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;

        $document = new Document($this->_adapter);
        $params = $this->params()->fromRoute();
        $searchParam['passenger_id'] = $this->decrypt($params['id']);
        if (isset($searchParam['requested_on']) && $searchParam['requested_on'] != '' && $searchParam['requested_on'] != 1) {
            $buildYearArray = $this->getDateRange($searchParam['requested_on']);
            $searchParam['from_date'] = $this->DateFormat($buildYearArray['from'], 'db_date_format');
            $searchParam['to_date'] = $this->DateFormat($buildYearArray['to'], 'db_date_format');
        } else if (isset($searchParam['requested_on']) && $searchParam['requested_on'] != '' && $searchParam['requested_on'] == 1) {
            if ($searchParam['from_date'] != '') {
                $searchParam['from_date'] = $this->DateFormat($searchParam['from_date'], 'db_date_format');
            }
            if ($searchParam['to_date'] != '') {
                $searchParam['to_date'] = $this->DateFormat($searchParam['to_date'], 'db_date_format');
            }
        } else {
            $searchParam['from_date'] = '';
            $searchParam['to_date'] = '';
        }

        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');

        $searchResult = $this->getDocumentTable()->searchCrmAnnotationCorrection($searchParam);

        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);

        if (count($searchResult) > 0) {
            foreach ($searchResult as $val) {
                $arrCell['id'] = $val['passenger_id'];

                $viewLabel = $this->_config['PassengerMessages']['config']['common']['VIEW'];
                $editLabel = $this->_config['PassengerMessages']['config']['common']['EDIT'];
                $deleteLabel = $this->_config['PassengerMessages']['config']['common']['DELETE'];

                $delete = "<a class='delete_annotation delete-icon' href='#delete_annotation' onClick=deleteAnnotation('" . $this->encrypt($val['annotation_correction_id']) . "') title='" . $deleteLabel . "'></a>";
                $edit = "<a class='edit-icon' href='/edit-crm-annotation-correction/" . $this->encrypt($val['annotation_correction_id']) . "' title='" . $editLabel . "'></a>";

                $activityType = '';
                if ($val['activity_type'] == 1)
                    $activityType = 'Annotations';
                if ($val['activity_type'] == 2)
                    $activityType = 'Corrections';

                $status = '';
                if ($val['status'] == 0)
                    $status = 'Pending';
                if ($val['status'] == 1)
                    $status = 'Completed';


                $arrCell['cell'] = array(stripslashes($val['contact_name']), $this->OutputDateFormat($val['added_date'], 'dateformatampm'), $activityType, $status, $edit . $delete);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This Action is used to download the ship image
     * @param this will be id
     * @return this allow to download the image
     * @author Icreon Tech - SR
     */
    public function crmDownloadShipImageAction() {
        $this->checkUserAuthentication();
        $imageName = '';
        $params = $this->params()->fromRoute();
        $searchParam['ship_id'] = $this->decrypt($params['id']);
        $searchParam['asset_for'] = '1';
        $searchShipImageResult = $this->getDocumentTable()->searchCrmShipImage($searchParam);
        $imageName = $searchShipImageResult[0]['ASSETVALUE'];
        $extn = substr($imageName, strrpos($imageName, '.') + 1);
        header('Content-disposition: attachment; filename=' . $imageName);
        if (strtolower($extn) == 'jpg' || strtolower($extn) == 'jpeg')
            header('Content-type: image/jpeg');
        else if (strtolower($extn) == 'png')
            header('Content-Type: image/png');
        else if (strtolower($extn) == 'gif')
            header('Content-Type: image/gif');
        $shipPath = $this->_config['file_upload_path']['assets_url'].'shipimage/';
        readfile($shipPath . $imageName);
        die;
    }

    /**
     * This Action is used to to get ship auto suggest
     * @return this will be a list of all ships
     * @author Icreon Tech - SR
     */
    public function getShipListAction() {
        /* Contacts */
        $response = $this->getResponse();
        $request = $this->getRequest();
        $postArray = $request->getPost()->toArray();
        $postArray['ship_name'] = $postArray['ship'];

        $postArray['startIndex'] = 0;
        $postArray['recordLimit'] = 10;
        $postArray['sortField'] = 'ID';
        $postArray['sortOrder'] = 'ASC';
        $resultarray = $this->getDocumentTable()->getShip($postArray);
        $allRecords = array();
        $i = 0;
        if ($resultarray !== false) {
            foreach ($resultarray as $resultarray) {
                $allRecords[$i] = array();
                $allRecords[$i]['ship_name'] = $resultarray['ship_name'];
                $i++;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($allRecords));
        return $response;
        /* End Contacts */
    }

    /**
     * This Action is used to get auto suggest for assets
     * @author Icreon Tech-AS
     * @return listing of assets id
     * @param ship_id, assets_id
     */
    public function getAssetsAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getDocumentTable();

        if ($request->isPost()) {
            $getSearchArray['ship_id'] = $request->getPost('ship_id');
            $getSearchArray['assets_id'] = $request->getPost('assets_id');
            $assetsArray = $this->getServiceLocator()->get('Common\Model\CommonTable')->getShipAssets($getSearchArray);
            $assets_list = array();
            if (!empty($assetsArray)) {
                $i = 0;
                foreach ($assetsArray as $key => $val) {
                    $assets_list[$val['ASSETID']] = $val['ASSETID'];
                    $assets_list[$i] = array();
                    $assets_list[$i]['assets_id'] = $val['ASSETID'];
                    $i++;
                }
            }
            return $response->setContent(\Zend\Json\Json::encode($assets_list));
        }
    }

    /**
     * This Action is used to view a particular shipline
     * @return $viewModel
     * @param $searchParam
     * @author Icreon Tech-AS
     */
    public function viewCrmShiplineAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getDocumentTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $searchParam['shipline_id'] = $this->decrypt($params['id']);
        $searchResult = $this->getDocumentTable()->searchCrmShipline($searchParam);
        $mode = (isset($params['mode']) && $params['mode'] != '') ? $this->decrypt($params['mode']) : 'view';
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'shipline_id' => $params['id'],
            'encrypted_shipline_id' => $params['id'],
            'searchResult' => $searchResult[0],
            'module_name' => $this->encrypt('shipline'),
            'mode' => $mode,
            'jsLangTranslate' => $this->_config['PassengerMessages']['config']['edit_shipline']
        ));
        return $viewModel;
    }

    /**
     * This Action is used for the search of Document crm search passenger for bul updates
     * @return this will be searched passengers data
     * @author Icreon Tech -SR
     */
    public function getCrmSearchBulkPassengerListAction() {

        $this->checkUserAuthentication();
        $this->getDocumentTable();
        /** Sphinx Setting section */
        $sphinxServer = $this->_config['SphinxServer'];
        $sphinxComponentObj = new SphinxComponent($sphinxServer['serverName']);
        $sphinxComponentObj->initialize();
        $arrSphinxIndexes = $this->_config['SphinxServer']['passengerIndexes'];
        $request = $this->getRequest();
        $response = $this->getResponse();
        parse_str($request->getPost('searchString'), $searchParam);

        $limit = ($request->getPost('rows') != 0) ? $request->getPost('rows') : '1';
        $page = ($request->getPost('page') != '' && $request->getPost('page') != 0) ? $request->getPost('page') : 1;

        $isExport = $request->getPost('export');
        if ($isExport != "" && $isExport == "excel") {
            $chunksize = $this->_config['export']['chunk_size'];
            $limit = $export_limit = $this->_config['export']['export_limit'];

            $searchParam['limit'] = $chunksize;
        }

        $start = $limit * $page - $limit;
        $searchParam['index'] = $start;
        $searchParam['limit'] = $limit;

        $arrSphinxSettings = array('limit' => (int) $limit,
            'index' => $arrSphinxIndexes,
            'page' => (int) $page,
            // 'groupby'        =>  array(array("sphinx_group_by" , '@weight DESC')),
            'rankingMode' => SPH_RANK_PROXIMITY_BM25, // SPH_RANK_PROXIMITY_BM25,
            'matchMode' => SPH_MATCH_EXTENDED2, //SPH_MATCH_EXTENDED2, //SPH_MATCH_BOOLEAN, //SPH_MATCH_EXTENDED2,
            'filter' => array()
        );
        $sphinxComponentObj->SetFieldWeights(array('field0' => 10000,
            'field3' => 5000,
            'field2' => 100,
            'field18' => 100,
            'field7' => 100,
            'field9' => 60,
            'field11' => 50,
            'field13' => 100,
            'field16' => 5000,
            'field25' => 200,
            'field26' => 100,
            'field27' => 1110,
            'field28' => 50,
            'field29' => 50,
            'field30' => 50,
            'field31' => 50,
            'field32' => 50,
            'field21' => 50,
            'field20' => 50
        ));

        $sphinxComponentObj->SetIndexWeights($this->_config['SphinxServer']['passengerIndexesIndexWeights']);
        $arrSphinxSettingsStr = '';

        if ($searchParam['search_type'] == 1) { /** By Passenger Id search */

            $start_id = $searchParam['start_id'];
            $end_id = $searchParam['end_id'];
            $arr[] = $start_id;
            $arr[] = $end_id; //zzzzzzzzzzz

            if (isset($start_id) && $start_id != '' && isset($end_id) && $end_id != '') {
                $sphinxComponentObj->SetFilterRange('field39', $start_id, $end_id);
            }
        }
        if ($searchParam['search_type'] == 2) { /** By Series/Roll/Frame */
            $series = (isset($searchParam['series']) && $searchParam['series'] != '') ? trim($searchParam['series']) : '';
            $roll = (isset($searchParam['roll']) && $searchParam['roll'] != '') ? trim($searchParam['roll']) : '';
            $frame = (isset($searchParam['frame']) && $searchParam['frame'] != '') ? trim($searchParam['frame']) : '';
            if (isset($series) && $series != '')
                $arrSphinxSettingsStr .= ' & (@(field29) ("^' . trim($series) . '$"))';
            if (isset($roll) && $roll != '')
                $arrSphinxSettingsStr .= ' & (@(field31) ("^' . trim($series) . '-' . trim($roll) . '$"))';
            if (isset($frame) && $frame != '')
                $arrSphinxSettingsStr .= ' & (@(field30) ("*' . trim($frame) . '$"))';
        }
        if ($searchParam['search_type'] == 3) { /** By Trip */
            $ship_name = (isset($searchParam['ship_name']) && $searchParam['ship_name'] != '') ? trim($searchParam['ship_name']) : '';
            $arrival_date = (isset($searchParam['arrival_date']) && $searchParam['arrival_date'] != '') ? $this->DateFormat($searchParam['arrival_date'], 'db_date_format') : '';

            if (isset($ship_name) && $ship_name != '')
                $arrSphinxSettingsStr .= ' & (@(field7) ("*' . $ship_name . '*"))';

            if (isset($arrival_date) && $arrival_date != '')
                $arrSphinxSettingsStr .= ' & (@(field18) ("^' . $arrival_date . '$"))';
        }

        $arrSphinxSettings['search'] = $arrSphinxSettingsStr;
        $sphinxComponentObj->setSortMode(($request->getPost('sord') == 'desc') ? SPH_SORT_ATTR_DESC : SPH_SORT_ATTR_ASC, $request->getPost('sidx'));

        $page_counter = 1;
        $number_of_pages = 1;

        do {
            $searchResult = $sphinxComponentObj->search($arrSphinxSettings);
            $total = $searchResult['total'];
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();

            if ($isExport == "excel") {
                $filename = "passenger_bulk_update_" . EXPORT_DATE_TIME_FORMAT . ".csv";
                if ($total > $chunksize) {
                    $number_of_pages = ceil($total / $chunksize);
                    $page_counter++;
                    $start = $chunksize * $page_counter - $chunksize;
                    $searchParam['index'] = $start;
                    $searchParam['page'] = $page_counter;
                } else {
                    $number_of_pages = 1;
                    $page_counter++;
                }
            } else {
                $page_counter = $page;
                $jsonResult['page'] = $page;
                $jsonResult['records'] = $searchResult['total'];
                $jsonResult['total'] = ceil($searchResult['total'] / $limit);
            }
        } while ($page_counter <= $number_of_pages && $isExport == "excel");

        $valueArray = array();
        if ($searchResult['total'] > 0) {
            $arrExport = array();
            foreach ($searchResult['matches'] as $key => $val) {
                $valueArray = $val['attrs'];
                $arrCell['id'] = $valueArray['field0'];
                $gender = '';
                $genderArray = $this->genderArray();
                if ($valueArray['field10'] != '')
                    $gender = $genderArray[$valueArray['field10']];
                else
                    $gender = "Unkown";

                $maritalStatusArray = $this->maritalStatusArray();
                $maritalStatus = '';
                if ($valueArray['field11'] != '')
                    $maritalStatus = $maritalStatusArray[$valueArray['field11']];
                else
                    $maritalStatus = "Unkown";
                if (!empty($valueArray['field4']))
                    $residence = $valueArray['field4'];
                else
                    $residence = 'N/A';
                if (!empty($valueArray['field2']))
                    $firstName = stripslashes($valueArray['field2']);
                else
                    $firstName = 'N/A';
                if (!empty($valueArray['field3']))
                    $lastName = stripslashes($valueArray['field3']);
                else
                    $lastName = 'N/A';

                if (!empty($valueArray['field7']))
                    $shipName = stripslashes($valueArray['field7']);
                else
                    $shipName = 'N/A';
                if (!empty($valueArray['field18']) && $valueArray['field18'] != '0000-00-00')
                    $dateArrive = $this->Dateformat($valueArray['field18'], 'displayformat');
                else
                    $dateArrive = 'N/A';

                $manifestImage = "<a href='javascript:void(0)' style='text-decoration:underline' onClick=showPassengerMoreManifestImage('" . trim($this->encrypt($valueArray['field31'])) . "','" . trim($this->encrypt($valueArray['field30'])) . "','" . trim($this->encrypt($valueArray['field0'])) . "');>" . strtoupper($valueArray['field32']) . "</a>";

                if ($isExport == "excel") {
                    $arrExport[] = array("PASSENGER ID" => $valueArray['field0'], "Last Name" => $lastName, "First Name" => $firstName, "Ref Line #" => $valueArray['field16'], "Ship Name" => $shipName, "Arrival Date" => $dateArrive, "Manifest" => strip_tags($manifestImage), "Gender" => $gender, "Residence" => $residence, "Marital Status" => $maritalStatus, "Age" => $valueArray['field5'], "Ethnicily" => $valueArray['field36']);
                } else {
                    $arrCell['cell'] = array($valueArray['field0'], $valueArray['field0'], $lastName, $firstName, $valueArray['field16'], $shipName, $dateArrive, $manifestImage, $gender, $residence, $maritalStatus, $valueArray['field5'], $valueArray['field36']);
                }

                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
            if ($isExport == "excel") {
                $this->arrayToCsv($arrExport, $filename, $page_counter);
            }
        } else {
            $jsonResult['rows'] = array();
        }
        if ($isExport == "excel") {
            $this->downloadSendHeaders($filename);
            die;
        } else {
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        }
    }

    /**
     * This Action is used to view the passengers roll numbers
     * @return this will be the list of roll numbers
     * @author Icreon Tech - SR
     */
    public function getPassengerRollAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $postData = array();
        $postData['series'] = $request->getPost('series');
        $resultSet = $this->_documentTable->getRollNumbers($postData);
//        asd($resultSet);
        foreach ($resultSet as $key => $val) {
            $rollNbr = substr($val['ROLL_NBR'], 5);
            if (!empty($rollNbr))
                $rollNumberArray[]['ROLL_NBR'] = trim($rollNbr, '-');
        }
        sort($rollNumberArray);

        return $response->setContent(\Zend\Json\Json::encode($rollNumberArray));
    }

    /**
     * This Action is used to view the all frames on the bases of roll numbers
     * @return this will be the list of frames
     * @author Icreon Tech - SR
     */
    public function getRollFrameAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $postData = array();
        $limit = 6;
        if ($request->getPost('showSlider') == 'show') {
            $postData['limit'] = $limit;
            if ($request->getPost('page') == '') {
                $page = 1;
            } else {
                $page = $request->getPost('page');
            }
            $postData['startIndex'] = ($page - 1) * $limit;
        }

        if ($request->getPost('ship_name') != '') {
            $postData['shipName'] = $request->getPost('ship_name');
            if ($request->getPost('arrival_date') != '') {
                $postData['dateArrive'] = $request->getPost('arrival_date');
            }
            $resultSet = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getShipManifestRecord($postData);
        } else {
            $postData['rollNbr'] = $request->getPost('series') . '-' . $request->getPost('roll');
            if ($request->getPost('frame') != '') {
                $postData['frame'] = $request->getPost('frame');
            }
            $resultSet = $this->_documentTable->getRollNumbersFrames($postData);
        }

        $frameCountResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $frameCount = 0;
        $frameCount = $frameCountResult[0]->RecordCount;
        $totalPage = ceil($frameCount / $limit);

        if ($request->getPost('showSlider') == 'show') {
            $manifestPath = '';
            //$manifestPath = $this->_config['Image_path']['manifestPath'];
			$manifestPath = $config['file_upload_path']['assets_url'].'manifest';
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'resultSet' => $resultSet,
                'manifestPath' => $manifestPath,
                'page' => $page,
                'totalPage' => $totalPage
            ));
            return $viewModel;
        } else {
            return $response->setContent(\Zend\Json\Json::encode($resultSet));
        }
    }

    /**
     * This Action is used to get all date of arrivals
     * @return this will be list of all arrival date of ships
     * @author Icreon Tech - SR
     */
    public function getShipArrivalDatesAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $postData = array();

        $postData['ship_name'] = $request->getPost('ship');
        $resultSet = $this->_documentTable->getShipArrival($postData);

        $allRecords = array();
        $i = 1;
        $allRecords[0]['date_arrive'] = 'Select';
        foreach ($resultSet as $resultarray) {
            if (!empty($resultarray['DATE_ARRIVE'])) {
                $allRecords[$i] = array();
                $allRecords[$i]['date_arrive'] = $this->DateFormat($resultarray['DATE_ARRIVE'], 'calender');
                $i++;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($allRecords));
    }

    /**
     * This Action is used to update the bulk passengers
     * @return this will be the confirmation
     * @author Icreon Tech - SR
     */
    public function updateBulkPassengerAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $postData = array();
        $searchParam = array();
        parse_str($request->getPost('searchString'), $searchParam);
        $ids = $request->getPost();
        $postData['ids'] = $request->getPost('ids[]');
        $idNo = implode(',', $ids->ids);

        $postedData = array();
        if ($idNo != '') {
            $postedData['passenger_id'] = $idNo;
            $passengerRecordSet = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getPassengerManifest($postedData);
            // asd($passengerRecordSet);
            if (count($passengerRecordSet) > 0) {
                foreach ($passengerRecordSet as $val) {
                    $postMainImageData = array();
                    $postNewManifestData = array();

                    $postMainImageData['passenger_id'] = $val['ID'];
                    if (!empty($searchParam['p_roll_no']) && !empty($searchParam['p_frame'])) {

                        if ($val['pass_data_source'] == 1)
                            $postMainImageData['FILENAME'] = $searchParam['p_roll_no'] . sprintf('%04d', $searchParam['p_frame']) . '.TIF';
                        else if ($val['pass_data_source'] == 2)
                            $postMainImageData['FILENAME'] = sprintf('%09d', $searchParam['p_roll_no']) . '_' . sprintf('%05d', $searchParam['p_frame']) . '.jpg';

                        $passSearchParam = array();
                        // check for manifest already exists
                        $passSearchParam['manifestImage'] = $postMainImageData['FILENAME'];
                        $passengerManifestResult = $this->getDocumentTable()->getManifestDetails($passSearchParam);
                        if (count($passengerManifestResult) > 0) {
                            $imageAutoId = $passengerManifestResult[0]['AUTOID'];
                            $postMainImageData['MANIMAGE_AUTOID'] = $imageAutoId;
                            
                            $updateMainImageData['mainimage_autoid'] = $imageAutoId;    
                            
                        } else { // insert new manifest
                            if (isset($postMainImageData['FILENAME']) && !empty($postMainImageData['FILENAME']))
                                $postNewManifestData['file_name'] = $postMainImageData['FILENAME'];

                            $postNewManifestData['page_nbr'] = $val['REF_PAGE_NBR'];

                            if (isset($searchParam['p_roll_no']) && !empty($searchParam['p_roll_no']))
                                $postNewManifestData['roll_number'] = $searchParam['p_roll_no'];

                            if (isset($searchParam['p_frame']) && !empty($searchParam['p_frame']))
                                $postNewManifestData['frame'] = $searchParam['p_frame'];

                            if (!empty($searchParam['p_arrival_date']))
                                $postNewManifestData['arrival_date'] = $this->DateFormat($searchParam['p_arrival_date'], 'db_date_format') . " 00:00:00";
                            else
                                $postNewManifestData['arrival_date'] = $val['DATE_ARRIVE'];

                            if (!empty($searchParam['p_ship_name']))
                                $postNewManifestData['ship_name'] = $searchParam['p_ship_name'];
                            else
                                $postNewManifestData['ship_name'] = $val['SHIP_NAME'];
                            $postNewManifestData['data_source'] = $val['pass_data_source'];
                            $manimageAutoId = $this->getDocumentTable()->insertCrmPassengerMainImage($postNewManifestData);
                            $postMainImageData['MANIMAGE_AUTOID'] = $manimageAutoId;
                            
                            $updateMainImageData['mainimage_autoid'] = $manimageAutoId;
                        }


                        if (isset($postMainImageData['FILENAME']) && !empty($postMainImageData['FILENAME']))
                            $postMainImageData['file_name'] = $postMainImageData['FILENAME'];
                    }


                    if (!empty($searchParam['p_arrival_date']))
                        $postMainImageData['arrival_date'] = $this->DateFormat($searchParam['p_arrival_date'], 'db_date_format') . " 00:00:00";


                    if (!empty($searchParam['p_ship_name']))
                        $postMainImageData['ship_name'] = $searchParam['p_ship_name'];

                    if (!empty($searchParam['p_arrival_date']))
                        $postedData['ship_arrival_date'] = $this->DateFormat($searchParam['p_arrival_date'], 'db_date_format');

                    if (!empty($searchParam['p_last_name']))
                        $postMainImageData['last_name'] = $searchParam['p_last_name'];

                    $this->getDocumentTable()->updateCrmMultiplePassenger($postMainImageData);

                    if (isset($searchParam['p_roll_no']) && !empty($searchParam['p_roll_no'])) {
                        $updateMainImageData['roll_number'] = $searchParam['p_roll_no'];
                        $updateMainImageData['frame'] = $searchParam['p_frame'];
                        $this->getDocumentTable()->updateCrmPassengerMainImage($updateMainImageData);
                    }
                }
            }

            /*
              if (count($passengerRecordSet) > 0)
              foreach ($passengerRecordSet as $val) {

              if ($val['AUTOID'] == '') { // insert into TBLMANIMAGE
              if ($searchParam['p_roll_no'] != '') {
              $postMainImageData = array();
              if ($val['pass_data_source'] == 1)
              $postMainImageData['FILENAME'] = $searchParam['p_roll_no'] . sprintf('%04d', $searchParam['p_frame']) . '.TIF';
              else if ($val['pass_data_source'] == 2)
              $postMainImageData['FILENAME'] = substr($searchParam['p_roll_no'],5) .'_'. sprintf('%05d', $searchParam['p_frame']) . '.jpg';

              $postMainImageData['file_name'] = $postMainImageData['FILENAME'];
              $postMainImageData['roll_number'] = $searchParam['p_roll_no'];
              $postMainImageData['frame'] = $searchParam['p_frame'];

              $postMainImageData['arrival_date'] = $val['DATE_ARRIVE'];
              $postMainImageData['page_nbr'] = $val['REF_PAGE_NBR'];
              $postMainImageData['ship_name'] = $val['PASS_SHIP_NAME'];
              $postMainImageData['data_source'] = $val['pass_data_source'];
              $manimageAutoId = $this->getDocumentTable()->insertCrmPassengerMainImage($postMainImageData);
              $postData['MANIMAGE_AUTOID'] = $manimageAutoId;
              $passengerArray['filename'][] = "'" . $postMainImageData['FILENAME'] . "'";
              $passengerArray['manimage_autoid'][] = "'" . $manimageAutoId . "'";
              }
              }
              else {
              //$passengerArray[] = "'" . $val['FILENAME'] . "'";
              if ($searchParam['p_roll_no'] != '' || $searchParam['p_frame'] != '') {
              if ($val['pass_data_source'] == 1)
              $fileName = $searchParam['p_roll_no'] . sprintf('%04d', $searchParam['p_frame']) . '.TIF';
              else if ($val['pass_data_source'] == 2)
              $fileName = substr($searchParam['p_roll_no'],5) .'_'. sprintf('%05d', $searchParam['p_frame']) . '.jpg';

              $passengerArray['filename'][] = "'" . $fileName . "'";
              }
              else {
              $fileName = $val['FILENAME'];
              $passengerArray['filename'][] = "'" . $val['FILENAME'] . "'";
              }



              $passSearchParam = array();
              $passSearchParam['manifestImage'] = $fileName;
              $passengerManifestResult = $this->getDocumentTable()->getManifestDetails($passSearchParam);
              if (count($passengerManifestResult) > 0) {
              $imageAutoId = $passengerManifestResult[0]['AUTOID'];
              //$postData['MANIMAGE_AUTOID'] = $imageAutoId;
              $passengerArray['manimage_autoid'][] = "'" . $imageAutoId . "'";
              }
              }

              }

              //  asd($passengerArray);
              // $passengerArray = array_unique($passengerArray['filename']);
              $postedData['file_name'] = implode(',', $passengerArray['filename']);
              $postedData['roll_no'] = $searchParam['p_roll_no'];
              $postedData['frame'] = $searchParam['p_frame'];
              $postedData['ship_name'] = $searchParam['p_ship_name'];
              $postedData['last_name'] = $searchParam['p_last_name'];

              $postedData['manifest_file_name'] = trim($passengerArray['filename'][0],"'");
              $postedData['manifest_auto_id'] = trim($passengerArray['manimage_autoid'][0],"'");





              $postedData['arrival_date'] = $this->DateFormat($searchParam['p_arrival_date'], 'db_date_format');
              $postedData['ship_arrival_date'] = $this->DateFormat($searchParam['p_arrival_date'], 'datetoshortmonthformat');

              $this->_documentTable->updateBulkPassenger($postedData);




             */



            $messages = array('status' => "success");
            $messageArray = array_merge($this->_config['PassengerMessages']['config']['common'], $this->_config['PassengerMessages']['config']['passenger_search']);
            $this->flashMessenger()->addMessage($messageArray['RECORDS_UPDATED_SUCCESSFULLY']);

            return $response->setContent(\Zend\Json\Json::encode($messages));
        } else {
            $messages = array('status' => "error", 'message' => $messageArray['L_ATLEAST_ONE_VALUE']);
        }
    }

    /**
     * This Action is used to list passenger ids
     * @author Icreon Tech - SK
     */
    public function getPassengerIdAction() {
        $this->getDocumentTable();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $postArray = $request->getPost()->toArray();

        $sphinxServer = $this->_config['SphinxServer'];
        $sphinxComponentObj = new SphinxComponent($sphinxServer['serverName']);
        $sphinxComponentObj->initialize();
        $arrSphinxIndexes = $this->_config['SphinxServer']['passengerIndexes'];
        $arrSphinxSettings = array('limit' => (int) 10,
            'index' => $arrSphinxIndexes,
            'page' => (int) 1,
            'rankingMode' => SPH_RANK_PROXIMITY_BM25, // SPH_RANK_PROXIMITY_BM25,
            'matchMode' => SPH_MATCH_EXTENDED2, //SPH_MATCH_EXTENDED2, //SPH_MATCH_BOOLEAN, //SPH_MATCH_EXTENDED2,
            'filter' => array()
        );
        $sphinxComponentObj->SetFieldWeights(array('field0' => 10000,
            'field3' => 5000,
            'field2' => 100,
            'field18' => 100,
            'field7' => 100,
            'field9' => 60,
            'field11' => 50,
            'field13' => 100,
            'field16' => 5000,
            'field25' => 200,
            'field26' => 100,
            'field27' => 1110,
            'field28' => 50,
            'field29' => 50,
            'field30' => 50,
            'field31' => 50,
            'field32' => 50,
            'field21' => 50,
            'field20' => 50
        ));

        $sphinxComponentObj->SetIndexWeights($this->_config['SphinxServer']['passengerIndexesIndexWeights']);
        $passenger_id = (isset($postArray['passenger_id']) && $postArray['passenger_id'] != '') ? trim($postArray['passenger_id']) : '';
        $arrSphinxSettingsStr = '';
        if (isset($passenger_id) && $passenger_id != '')
            $arrSphinxSettingsStr.= ' & (@(field0) ("*' . $passenger_id . '*"))';

        $arrSphinxSettings['search'] = $arrSphinxSettingsStr;
        $sphinxComponentObj->setSortMode(($request->getPost('sord') == 'desc') ? SPH_SORT_ATTR_DESC : SPH_SORT_ATTR_ASC, 'field0');
        $searchResult = $sphinxComponentObj->search($arrSphinxSettings);
        $allRecords = array();
        $i = 0;
        if (!empty($searchResult) && isset($searchResult['matches'])) {
            foreach ($searchResult['matches'] as $key => $value) {
                foreach ($value as $k1 => $v1) {
                    if (is_array($v1)) {
                        $allRecords[$i] = array();
                        $allRecords[$i]['ID'] = $v1['field0'];
                        $i++;
                    }
                }
            }
        }

        $response->setContent(\Zend\Json\Json::encode($allRecords));
        return $response;
    }

    /**
     * This Action is used to show the Mainfest Image
     * @return this will be filepath
     * @author Icreon Tech - SR
     */
    function TrimString($String, $Length) {
        if (strlen($String) > $Length) {
            $Temp[0] = substr($String, 0, $Length);
            $Temp[1] = substr($String, $Length);
            $SpacePos = strpos($Temp[1], ' ');
            if ($SpacePos !== FALSE) {
                return $Temp[0] . substr($Temp[1], 0, $SpacePos);
            }
        }
        return $String;
    }

    /**
     * This Action is used to show the Mainfest Image in large with next and previous
     * @author Icreon Tech - SR
     */
    public function showManifestMoreImageAction() {

        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $params = $this->params()->fromRoute();

        $ship_manifest_form = new CrmShipManifestForm();
        /** Series */
        $passengerSeries = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPassengerSeries();
        $seriesList = array();
        foreach ($passengerSeries as $key => $val) {
            $seriesList[$val['series']] = $val['series'];
        }
        $ship_manifest_form->get('series_no')->setAttribute('options', $seriesList);

        $passengerId = '';
        if (isset($params['passenger_id']) && $params['passenger_id'] != '')
            $passengerId = $this->decrypt($params['passenger_id']);

        $passengerResult = array();
        if (isset($passengerId) && $passengerId != '') {
            $searchParam['passenger_id'] = $passengerId;
            $passengerResult = $this->getDocumentTable()->searchCrmPassenger($searchParam);
            $ship_manifest_form->get('passengerId')->setAttribute('value', $this->encrypt($passengerId));
        }


        $seriesArr = explode('--', $params['seriesRoll']);

        if (isset($seriesArr[1]) && $seriesArr[1] == 'showfromfile') {
            $params['seriesRoll'] = $this->encrypt($seriesArr[0]);
            $params['frame'] = $this->encrypt($params['frame']);
        }



        if (@$passengerResult[0]['data_source'] == 1 || strlen($this->decrypt($params['seriesRoll'])) == 9) {
            $seriesRoll = $this->decrypt($params['seriesRoll']);
            $seriesRollArr = explode('-', $seriesRoll);
            if (isset($seriesRollArr[1]))
                $seriesRoll = $seriesRollArr[0] . '-' . sprintf('%04d', $seriesRollArr[1]);
        } else {
            $seriesRoll = $this->decrypt($params['seriesRoll']);
            $seriesRollArr = explode('-', $seriesRoll);
            $seriesRoll = $seriesRollArr[0] . '-' . $seriesRollArr[1];
        }

        $frame = $this->decrypt($params['frame']);
        $rollNoJsonArray = array();

        foreach ($seriesList as $key2 => $val2) {
            $postData['series'] = $val2;
            $resultSet = $this->_documentTable->getRollNumbers($postData);

            $rollNumberArray = array();
            foreach ($resultSet as $key => $val) {
                $rollNbr = substr($val['ROLL_NBR'], 5);
                if (!empty($rollNbr))
                    $rollNumberArray[]['ROLL_NBR'] = trim($rollNbr, '-');
            }
            sort($rollNumberArray);
            $rollNoJsonArray[strtoupper($val2)] = ltrim($rollNumberArray[0]['ROLL_NBR'], 0) . '~' . ltrim($rollNumberArray[count($rollNumberArray) - 1]['ROLL_NBR'], 0);
        }

        $rollNoMinMaxArray = explode('~', $rollNoJsonArray[strtoupper($seriesRollArr[0])]);
        $ship_manifest_form->get('roll_no_min')->setAttribute('value', $rollNoMinMaxArray[0]);
        $ship_manifest_form->get('roll_no_max')->setAttribute('value', $rollNoMinMaxArray[1]);


        $rollNoJsonArray = json_encode($rollNoJsonArray);
        $ship_manifest_form->get('roll_no_range')->setAttribute('value', $rollNoJsonArray);
        $postData['rollNbr'] = $seriesRoll;
        $postData['frame'] = $frame;
        $resultSet = $this->_documentTable->getRollNumbersFrames($postData);
        //   asd($resultSet);

        $maxFrame = 1;
        if (count($resultSet) > 0)
            $maxFrame = $resultSet[count($resultSet) - 1]['FRAME'];

        $manifestPath = $config['file_upload_path']['assets_url'].'manifest/';


        if (@$passengerResult[0]['data_source'] == 1 || strlen($this->decrypt($params['seriesRoll'])) == 9) {
            $fileName = $seriesRoll . sprintf('%04d', $frame) . '.TIF';

            $ship_manifest_form->get('frame_no_max')->setAttribute('value', $maxFrame);
            $ship_manifest_form->get('series_no')->setAttribute('value', substr($seriesRoll, 0, 4));
            $ship_manifest_form->get('roll_no')->setAttribute('value', substr($seriesRoll, 5, 4));
            $ship_manifest_form->get('frame_no')->setAttribute('value', $frame);
        } else {
            $fileName = substr($seriesRoll, 5) . '_' . sprintf('%05d', $frame) . '.jpg';
            //$manifestImage = $fileName;

            $ship_manifest_form->get('frame_no_max')->setAttribute('value', $maxFrame);
            $ship_manifest_form->get('series_no')->setAttribute('value', substr($seriesRoll, 0, 4));
            $ship_manifest_form->get('roll_no')->setAttribute('value', $seriesRollArr[1]);
            $ship_manifest_form->get('frame_no')->setAttribute('value', $frame);
        }

        if (isset($passengerResult[0]['data_source']))
            $dataSource = $passengerResult[0]['data_source'];
        else {
            if (strlen($this->decrypt($params['seriesRoll'])) == 9)
                $dataSource = 1;
            else
                $dataSource = 2;
        }

        $manifestSession = new Container('campaign');
        if (isset($seriesArr[1]) && $seriesArr[1] == 'showfromfile') {
            
        } else {
            $manifestSession->imageName = $fileName;
            $manifestSession->seriesRoll = $seriesRoll;
            $manifestSession->frame = $frame;
        }

        // Code to convert the .TIF image to .jpg image            
        /*
          if($dataSource == 1) {
          $folderName = substr($fileName, 0, 9);
          $targetFileName = substr($fileName, 0, 13);
          $tifImageSourcePath = $this->_config['file_upload_path']['assets_upload_dir'].'manifest/'.$folderName.'/'.$fileName;
          $tifImageMainDestinationPath = $this->_config['file_upload_path']['assets_upload_dir'].'temp/manifest/'.$targetFileName . '.jpg';
          $tifImageThumbDestinationPath = $this->_config['file_upload_path']['assets_upload_dir'].'temp/manifest/thumb/'.$targetFileName . '.jpg';
          if(!file_exists($tifImageMainDestinationPath)) {
          $this->convertTifToJPG($tifImageSourcePath, $tifImageMainDestinationPath);
          $this->convertTifToThumbJPG($tifImageSourcePath, $tifImageThumbDestinationPath);
          }
          } */
        // End Code to convert the .TIF image to .jpg image                            


        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);

        $viewModel->setVariables(array(
            //'manifestImage' => $manifestImage,
            'jsLangTranslate' => $this->_config['PassengerMessages']['config']['passenger_details'],
            'passengerId' => $passengerId,
            'passengerResult' => $passengerResult,
            'seriesRoll' => $seriesRoll,
            'frame' => $frame,
            'imageName' => $fileName,
            'seriesList' => $seriesList,
            'ship_manifest_form' => $ship_manifest_form,
            'maxFrame' => $maxFrame,
            'manifestSessionFileName' => $manifestSession->imageName,
            'sessionSeriesRoll' => $manifestSession->seriesRoll,
            'sesionFrame' => $manifestSession->frame,
            'dataSource' => $dataSource,
            'uploadFilePath' => $this->_config['file_upload_path']['assets_upload_dir']
        ));
        return $viewModel;
    }

    public function checkValidRollnumberAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();

        $postParam = array();
        $seriesNo = $request->getPost('series_no');

        if (strlen($request->getPost('roll_no')) == 4) {
            $rollNo = sprintf('%04d', $request->getPost('roll_no')); // for passenger
        } else {
            $rollNo = sprintf('%09d', $request->getPost('roll_no')); // for family
        }


        $postParam['roll_nbr'] = $seriesNo . '-' . $rollNo;
        $postParam['frame_no'] = $request->getPost('frame_no');
        $result = $this->getDocumentTable()->checkValidRollNumber($postParam);

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($result[0]));
        return $response;
    }

    public function crmAnnotationDuplicateCheckAction() {
        $this->checkUserAuthentication();
        $this->getDocumentTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        
        $searchParam['userId'] = $params['user_id'];
        $searchParam['passengerId'] = $params['passenger_id'];
        if (isset($params['annotation_correction_id']) && !empty($params['annotation_correction_id']))
            $searchParam['annotationCorrectionId'] = $params['annotation_correction_id'];
        else
            $searchParam['annotationCorrectionId'] = '';

        $annotationResult = array();
        if(isset($params['activity_type'])) {
            if($params['activity_type'] == 1 )
            {
                $searchParam['activityType'] = 1; // annotation
                $annotationResult = $this->getDocumentTable()->checkPassengerAnnotationDuplicate($searchParam);
            }
            if($params['activity_type'] == 2)
            {
                $searchParam['activityType'] = 2; // correction
                $annotationResult = $this->getDocumentTable()->checkPassengerAnnotationDuplicate($searchParam);
            }
        }
        
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response = $this->getResponse();

        if (count($annotationResult) > 0) {
            if ($annotationResult[0]['cnt'] >= 1)
                $response->setContent(\Zend\Json\Json::encode(array('status' => 'duplicate')));
            else
                $response->setContent(\Zend\Json\Json::encode(array('status' => 'noduplicate')));
        }
        else {
            $response->setContent(\Zend\Json\Json::encode(array('status' => 'noduplicate')));
        }
        return $response;
    }

}