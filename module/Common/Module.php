<?php

/**
 * This file is used for initialize all obj
 * @package    Common
 * @author     Icreon Tech - DG
 */

namespace Common;

//namespace Zend\Mvc\Router;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\Validator\AbstractValidator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Common\Model\Country;
use Common\Model\CountryTable;
use Common\Model\Common;
use Common\Model\CommonTable;
use Common\Model\Question;
use Common\Model\QuestionTable;
use Common\Model\Survey;
use Common\Model\SurveyTable;
use Common\Model\Email;
use Common\Model\EmailTable;
use Common\Model\Ship;
use Common\Model\ShipTable;
use Common\Model\Arrivalport;
use Common\Model\ArrivalportTable;
use Zend\Authentication\AuthenticationService;
use Base\Controller\BaseController;
use Zend\Http\Request;
use Zend\Mvc\Router\Http\Literal;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Session\Container as SessionContainer;
use Zend\Http\PhpEnvironment\RemoteAddress;

/**
 * This file is used for initialize all obj
 * @package    Common
 * @author     Icreon Tech - DG
 */
class Module extends BaseController implements AutoloaderProviderInterface, ConfigProviderInterface, ViewHelperProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        //error_reporting(E_ALL);
        $translator = $e->getApplication()->getServiceManager()->get('translator');
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $translator->addTranslationFile(
                'phpArray', './module/User/languages/en/language.php', 'default', 'en_US'
        );
        $eventManager->attach(MvcEvent::EVENT_DISPATCH, array($this, 'authPreDispatch'), 1);

        $eventManager->attach(
                MvcEvent::EVENT_ROUTE, array($this, 'makeCmsRoutes'), 100
        );
        $eventManager->attach(
                MvcEvent::EVENT_ROUTE, array($this, 'getMembershipDiscount'), 100
        );        
        $e->getApplication()->getEventManager()->attach(MvcEvent::EVENT_DISPATCH_ERROR, function($e) {
                    $viewModel = $e->getViewModel();
                    $viewModel->setTemplate('error');
                });
    }

    public function authPreDispatch($event) {
        $viewModel = $event->getApplication()->getMvcEvent()->getViewModel();
        /* Get menus for frontend */
        $sm = $event->getApplication()->getServiceManager();
        $header = array();
        $auth = new AuthenticationService();
        //asd($_SERVER['REMOTE_ADDR']);
        
        $iPAddress = $this->getRemoteIP();
        $kioskId = $this->getKioskId($iPAddress,$sm);
        $kioskModules = $this->getKioskModule($kioskId,$sm);
        $kioskModuleSatausArr = $this->getModuleStatus($kioskModules['kiosk_module_status']);
        $kioskModuleConfigArr = $this->getModuleCofigVal($kioskModules['kiosk_settings_module_id_values']);
        //set value for login register module enable in header file
        $viewModel->isLogin = (!empty($kioskModuleSatausArr[1]))?true:false;
        $viewModel->isRegistration = (!empty($kioskModuleSatausArr[2]))?true:false;
        $viewModel->isPassengerSearch = (!empty($kioskModuleSatausArr[4]))?true:false;

        $this->kioskSession = new SessionContainer('kioskSession');
        if(!empty($this->kioskSession->time_end))
            $viewModel->kiosk_end_time = $this->kioskSession->time_end;
        
        
        $config = $sm->get('Config');
        $parentHeaderMenus = $sm->get('Common\Model\CommonTable')->getMenus(array('parentId' => '0', 'menuType' => '1'));

        $headerMenus = array();
        if (!empty($parentHeaderMenus)) {
            foreach ($parentHeaderMenus as $k => $value) {
                $headerMenus[$k] = $value;
                if ($headerMenus[$k]['menu_id'] == '8') {
                    $headerMenus[$k]['content_url'] = "shop";
                }
//                } else if ($headerMenus[$k]['menu_id'] == '7') {
//                    $headerMenus[$k]['content_url'] = "donate";
//                } else if ($headerMenus[$k]['menu_id'] == '6') {
//                    $headerMenus[$k]['content_url'] = "heritage-awards";
//                } else if ($headerMenus[$k]['menu_id'] == '3') {
//                    if (isset($auth->getIdentity()->user_id) && $auth->getIdentity()->user_id != '') {
//                        $headerMenus[$k]['content_url'] = "profile";
//                    } else {
//                        $headerMenus[$k]['content_url'] = "signup";
//                    }
//                }
                $childMenus = $this->getSubMenus($value['menu_id'], $sm);
                if (!empty($childMenus)) {
                    foreach ($childMenus as $key => $val) {
                        $headerMenus[$k]['submenu'][$key][] = $val;
                    }
                } else {
                    $headerMenus[$k]['submenu'] = array();
                }
            }
        }
        //asd($headerMenus);
        array_push($headerMenus[0]['submenu'], array(array('menu_id' => '', 'menu' => 'Contact Us', 'content_id' => '', 'content_url' => 'contact-us', 'sort_order' => '10')));
        array_push($headerMenus[0]['submenu'], array(array('menu_id' => '', 'menu' => 'Membership', 'content_id' => '', 'content_url' => 'member-benefit', 'sort_order' => '5')));
        // array_push($headerMenus[0]['submenu'], array(array('menu_id' => '', 'menu' => 'Blogs', 'content_id' => '', 'content_url' => 'blogs', 'sort_order' => '9'))); 
       
        if(!empty($kioskModuleSatausArr[8])){
                array_push($headerMenus[1]['submenu'][0][0]['subMenu'][0], array('menu_id' => '', 'menu' => 'Search Wall', 'content_id' => '', 'content_url' => 'search-woh', 'sort_order' => '2'));
            if(!empty($kioskModuleConfigArr[6])){
                array_push($headerMenus[1]['submenu'][0][0]['subMenu'][0], array('menu_id' => '', 'menu' => 'Add a Name', 'content_id' => '', 'content_url' => 'add-woh', 'sort_order' => '1'));
            }
        }
        if(!empty($kioskModuleSatausArr[6]))
            array_push($headerMenus[1]['submenu'][1][0]['subMenu'][0], array('menu_id' => '', 'menu' => 'Add Your Photo', 'content_id' => '', 'content_url' => 'add-fof-image', 'sort_order' => '1'));
        
        if(!empty($kioskModuleSatausArr[7]))
            array_push($headerMenus[1]['submenu'][1][0]['subMenu'][0], array('menu_id' => '', 'menu' => 'Search The Flag', 'content_id' => '', 'content_url' => 'search-fof', 'sort_order' => '2'));
        
        $headerMenus[1]['submenu'][2][0]['subMenu'][0] = array();
        if(!empty($kioskModuleSatausArr[13]))
            array_push($headerMenus[1]['submenu'][2][0]['subMenu'][0], array('menu_id' => '', 'menu' => 'View Stories', 'content_id' => '', 'content_url' => 'family-histories', 'sort_order' => '1'));
        // $headerMenus[3]['submenu'][1][0]['subMenu'][0] = array();
        array_push($headerMenus[3]['submenu'][1][0]['subMenu'][0], array('menu_id' => '', 'menu' => 'Quizzes', 'content_id' => '', 'content_url' => 'quizzes', 'sort_order' => '1'));
        //$headerMenus[3]['submenu'][2][0]['subMenu'][0] = array();
        array_push($headerMenus[3]['submenu'][0][0]['subMenu'][0], array('menu_id' => '', 'menu' => 'About The Wall', 'content_id' => '', 'content_url' => 'about-the-wall-of-honor', 'sort_order' => '10'));
        
        if(!empty($kioskModuleSatausArr[4]))
            array_push($headerMenus[3]['submenu'][2][0]['subMenu'][0], array('menu_id' => '', 'menu' => 'Passenger Search', 'content_id' => '', 'content_url' => 'passenger', 'sort_order' => '1'));

        array_push($headerMenus[3]['submenu'][2][0]['subMenu'][0], array('menu_id' => '', 'menu' => 'Ship Search', 'content_id' => '', 'content_url' => 'ship', 'sort_order' => '2'));

        array_push($headerMenus[5]['submenu'], array(array('menu_id' => '', 'menu' => 'Contact Us', 'content_id' => '', 'content_url' => 'contact-us', 'sort_order' => '2')));
        /*
          if (isset($auth->getIdentity()->user_id) && $auth->getIdentity()->user_id != '') {
          array_push($headerMenus[0]['submenu'], array(array('menu_id' => '', 'menu' => 'My File', 'content_id' => '', 'content_url' => 'profile', 'sort_order' => '0')));
          } else {
          array_push($headerMenus[0]['submenu'], array(array('menu_id' => '', 'menu' => 'Join Us', 'content_id' => '', 'content_url' => 'signup', 'sort_order' => '0')));
          array_push($headerMenus[0]['submenu'], array(array('menu_id' => '', 'menu' => 'Login', 'content_id' => '', 'content_url' => 'login', 'sort_order' => '2')));
          }
          array_push($headerMenus[3]['submenu'], array(array('menu_id' => '', 'menu' => 'Donate', 'content_id' => '', 'content_url' => 'donate', 'sort_order' => '2')));
          array_push($headerMenus[3]['submenu'], array(array('menu_id' => '', 'menu' => 'Family Histories', 'content_id' => '', 'content_url' => 'family-histories', 'sort_order' => '10')));
          array_push($headerMenus[6]['submenu'], array(array('menu_id' => '', 'menu' => 'Donate', 'content_id' => '', 'content_url' => 'donate', 'sort_order' => '1')));

          array_push($headerMenus[6]['submenu'], array(array('menu_id' => '', 'menu' => 'Gift Shop', 'content_id' => '', 'content_url' => 'shop', 'sort_order' => '1')));
          array_push($headerMenus[6]['submenu'], array(array('menu_id' => '', 'menu' => 'Cart', 'content_id' => '', 'content_url' => 'cart', 'sort_order' => '2')));
          array_push($headerMenus[6]['submenu'], array(array('menu_id' => '', 'menu' => 'Wishlist', 'content_id' => '', 'content_url' => 'wishlist', 'sort_order' => '3')));
         */

        for ($i = 0; $i < count($headerMenus); $i++) {
            $headerMenus[$i]['submenu'] = $this->sort($headerMenus[$i]['submenu']);
        }

        for ($i = 0; $i < count($headerMenus); $i++) {
            for ($j = 0; $j < count($headerMenus[$i]['submenu']); $j++) {
                if (isset($headerMenus[$i]['submenu'][$j][0]['subMenu'][0]) && !empty($headerMenus[$i]['submenu'][$j][0]['subMenu'][0])) {
                    $headerMenus[$i]['submenu'][$j][0]['subMenu'][0] = $this->sort($headerMenus[$i]['submenu'][$j][0]['subMenu'][0]);
                }
            }
        }

        $headerMenus = $this->manageMenu($headerMenus,$kioskModuleSatausArr);
        $viewModel->headerMenus = $headerMenus;
        $parentFooterMenus = $sm->get('Common\Model\CommonTable')->getMenus(array('parentId' => '0', 'menuType' => '2'));
  
        

//        $footerMenus = array();
//        if (!empty($parentFooterMenus)) {
//            foreach ($parentFooterMenus as $k => $value) {
//                $footerMenus[$k] = $value;
//                $childMenus = $sm->get('Common\Model\CommonTable')->getMenus(array('parentId' => $value['menu_id'], 'menuType' => '2'));
//                if (!empty($childMenus)) {
//                    foreach ($childMenus as $key => $val) {
//                        $footerMenus[$k]['submenu'][$key][] = $val;
//                    }
//                }
//            }
//        }
//        $viewModel->footerMenus = $footerMenus;
        $viewModel->footerMenus = $parentFooterMenus;
        $this->_config = $sm->get('Config');

        $this->getSiteAllowCondition();
        $viewModel->numPageSelect = $this->_config['grid_config']['numPageSelect'];
        $viewModel->numRecPerPage = $this->_config['grid_config']['numRecPerPage'];
        $viewModel->applicationConfigIds = $this->_config['application_config_ids'];


        //User Status 1 :  Non-logged in , 2 : Logged in Non-member, 3 : Logged in Members
        //1=>Non Logged-in, 2=>Registered, 3=>Membership
        $usrAccessLabel = '1';
        if (isset($auth->getIdentity()->user_id) && $auth->getIdentity()->user_id != '') {
            $userDetail = $sm->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $auth->getIdentity()->user_id));
            if (isset($userDetail['membership_id']) and $userDetail['membership_id'] == "1")
                $usrAccessLabel = '2';
            else if (isset($userDetail['membership_id']) and $userDetail['membership_id'] != "1" and strtotime($userDetail['membership_expire']) > strtotime(date("Y-m-d", strtotime(DATE_TIME_FORMAT))))
                $usrAccessLabel = '3';
            else
                $usrAccessLabel = '2';
        }


        $announcementsParam = array(
            'startIndex' => '',
            'recordLimit' => '',
            'sortField' => 'modified_date',
            'sortOrder' => 'DESC',
            'status' => '1',
            'type' => '6',
            'content_id' => '',
            'access_label' => $usrAccessLabel,
            'content_date' => date("Y-m-d", strtotime(DATE_TIME_FORMAT))
        );


        $viewModel->getLatestAnnoucment = $getLatestAnnoucment = $sm->get('Cms\Model\CmsTable')->getCrmContents($announcementsParam);
     
        $cartlist = array();
        $user_id = isset($auth->getIdentity()->user_id) ? $auth->getIdentity()->user_id : '';
        $cartParam['user_id'] = $user_id;
        $cartParam['source_type'] = 'frontend';
        $cartParam['user_session_id'] = session_id();
        $cartParam['membership_percent_discount'] = MEMBERSHIP_DISCOUNT;
        $cartlist = $sm->get('Transaction\Model\TransactionTable')->getCartProducts($cartParam); 
        $viewModel->cartlist = $cartlist;
        $viewModel->isBookingKiosk = 0;
		$remoteIp = $this->getRemoteIP();
		if(!empty($remoteIp)){
			if(isset($this->_config['kiosk-system-settings'][$remoteIp]))
			   $viewModel->isBookingKiosk = $this->_config['kiosk-system-settings'][$remoteIp]['isHotSystem'];
		}

        $viewModel->upload_file_path = $this->_config['file_upload_path'];

	 $viewModel->inactiveSessionTime = $this->_config['wohKisok']['inactive_session_alert_time'];

	 $viewModel->timerStartTime = $this->_config['wohKisok']['timer_start_time'];


        // added - n- start
        $viewModel->siteRouteName = "";
        if(!empty($viewModel->getChildren()[0])) {
            $viewModel->siteRouteName = $viewModel->getChildren()[0]->getTemplate();
        }
        // added - n- end

        /* Get menus for frontend */
        $config = $sm->get('Config');
        $auth = new AuthenticationService();
        if ($auth->hasIdentity() === true && isset($auth->getIdentity()->crm_user_id)) {
            $sm = $event->getApplication()->getServiceManager();
            $userArr = array('crm_user_id' => @$auth->getIdentity()->crm_user_id);
            $userDetail = $sm->get('User\Model\CrmuserTable')->getCrmUserDetails($userArr);

            $httpsUrl = (explode("/", substr($_SERVER['REQUEST_URI'], 1)));
            $assetsUrl = $config['file_upload_path'];
            $viewModel->crm_user_name = $userDetail[0]['crm_first_name'] . " " . $userDetail[0]['crm_last_name'];
            $imagePath = $assetsUrl['assets_upload_dir'] . 'crmuser/' . $this->folderStructByUserId($userDetail[0]['crm_user_id']) . 'image/thumbnail/' . $userDetail[0]['crm_user_picture'];
            $imageName = (($this->_config['https_on'] == 1 && in_array($httpsUrl[0], $this->_config['https_url'])) ? $assetsUrl['assets_url_https'] : $assetsUrl['assets_url']) . 'crmuser/' . $this->folderStructByUserId($userDetail[0]['crm_user_id']) . 'image/thumbnail/' . $userDetail[0]['crm_user_picture'];
            $userImage = (file_exists($imagePath) && !empty($userDetail[0]['crm_user_picture'])) ? $imageName : (($this->_config['https_on'] == 1) ? SITE_URL_HTTPS : SITE_URL) . '/img/user_no_image.jpg';
            $viewModel->crm_user_picture = $userImage;

            /*             * * CRM Left Menu * */

            $modules_actions = $sm->get('User\Model\CrmuserTable')->getCrmUserModuleActions();
            foreach ($modules_actions as $key => $value) {
                $modules[$value['module_id']] = $value['module_name'];
            }

            $leftMenuArray = $sm->get('Common\Model\CommonTable')->getCrmLeftMenus();

            $requestUri = $sm->get('request')->getRequestUri();
            $requestUrL = '';
            if ($requestUri != '') {
                $requestUriArray = explode('/', ltrim($requestUri, '/'));
                $requestUrL = $requestUriArray[0];
            }
            $selectedModule = '';
            $leftMenu = array();
            foreach ($leftMenuArray as $key => $value) {
                if (!isset($leftMenu[$modules[$value['parent_module_id']]]) && !is_null($value['display_order'])) {
                    $leftMenu[$modules[$value['parent_module_id']]]['routes'][$modules[$value['parent_module_id']]] = 'javascript:void(0);';
                    $leftMenu[$modules[$value['parent_module_id']]]['access'][$modules[$value['parent_module_id']]] = $value['parent_module_id'];
                }
                if (($requestUrL == ltrim($value['menu_url'], '/')) || (is_null($value['is_menu']) && is_null($value['display_order'] && isset($leftMenu[$modules[$value['parent_module_id']]]) && (!in_array($value['menu_url'], $leftMenu[$modules[$value['parent_module_id']]]['access']))))) {
                    $selectedModule = $modules[$value['parent_module_id']];
                }
                if (!is_null($value['display_order'])) {
                    $leftMenu[$modules[$value['parent_module_id']]]['routes'][$value['menu_label']] = $value['menu_url'];
                    $leftMenu[$modules[$value['parent_module_id']]]['access'][$value['menu_label']] = $value['module_id'] . "-" . $value['action_id'];
                }
            }
            // asd($leftMenu);

            $viewModel->requestUri = $requestUrL;
            $viewModel->selectedModule = $selectedModule;
            $viewModel->leftMenus = $leftMenu;

            /* End of CRM Left Menu */
        }

        /* Update Campaign Stautus of User */
        $getParamArr = $_GET;
        if (!empty($getParamArr) && isset($getParamArr['ccd']) && $getParamArr['ccd'] != '') {
            $this->campaignSession = new SessionContainer('campaign');
            $this->campaignSession->ccd = $getParamArr['ccd'];
            $this->campaignSession->ccdUpdated = true;
        }
		
		$moduleSession = new SessionContainer('moduleSession');
        if (($auth->hasIdentity() === true && isset($auth->getIdentity()->user_id)) || $moduleSession->moduleGroup == 3) {
            $this->campaignSession = new SessionContainer('campaign');
            //if ($this->campaignSession->ccd != '' && $this->campaignSession->ccdUpdated) {
                $sm = $event->getApplication()->getServiceManager();
                $channel = array_reverse(explode(".", $this->campaignSession->ccd));
                $campaignDetail = $sm->get('Campaign\Model\CampaignTable')->getCampaignId(array('is_ellis_default' => '1'));
				$channelResult = $sm->get('Common\Model\CommonTable')->getChannelType(array('channel_name' => 'Ellis'));
				/*if (!isset($channelResult) || empty($channelResult)) {
                    $channelResult[0]['channel_id'] = 9;
                }*/
                if($moduleSession->moduleGroup == 3)
					$channelResult = $sm->get('Common\Model\CommonTable')->getChannelType(array('channel_name' => 'WOHKiosks'));
				else
					$channelResult = $sm->get('Common\Model\CommonTable')->getChannelType(array('channel_name' => 'EllisKiosks'));
				if (!isset($channelResult) || empty($channelResult)) {
					if($moduleSession->moduleGroup == 3)
						$channelResult[0]['channel_id'] = 12;
					else
						$channelResult[0]['channel_id'] = 9;
                }
                if (!empty($campaignDetail)) { 
                    $campaignId = $campaignDetail['campaign_id'];
                    $updatedResponse = $sm->get('Campaign\Model\CampaignTable')->
                            userUpdateCampaign(array('user_id' => $auth->getIdentity()->user_id, 'campaign_id' => $campaignId, 'channel_id' => $channelResult[0]['channel_id']));
                    $this->campaignSession->ccdUpdated = true;
                    $this->campaignSession->campaign_code = $campaignDetail['campaign_code'];
                    $this->campaignSession->campaign_id = $campaignId;
                    $this->campaignSession->channel_id = $channelResult[0]['channel_id'];
                    $this->campaignSession->channel_name = $channelResult[0]['channel'];
                    if ($updatedResponse) {
                        $this->campaignSession->ccdUpdated = false;
                        //unset($this->campaignSession->ccd);
                    }
                }
            //}
        }
        /* Update Campaign Stautus of User */
        $this->timeZoneSession = new SessionContainer('timeZone');
        //session_destroy();
        
        if ($this->timeZoneSession->timeZone == '')
        {
            if ($config['get_time_zone_url']['get_time_zone_url'] == 2)
            {
                if (isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
                    $ip = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
                else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                {
                    $ipList = explode(", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
                    $ip = trim($ipList[0]);
                }
                else
                {
                    $ip = $_SERVER['REMOTE_ADDR'];
                }
                $timeZoneUrl = 'http://api.ipinfodb.com/v3/ip-city/?key=d7f1bf52b9bb1823c478c88dad797456dc1cb92e8c7846bea8ab8301a5f41883&ip=' . $ip . '&format=json';
                $timeZoneData = file_get_contents($timeZoneUrl);
                $timeZoneData = json_decode($timeZoneData);
                if (isset($timeZoneData->timeZone) and trim($timeZoneData->timeZone) != '')
                {
                    $timeZone0 = $timeZoneData->timeZone;
                    $timeZone = substr($timeZone0, 1);
                    $timeZone1 = explode(":", $timeZone);
                    $this->timeZoneSession->timeZone = $timeZone0[0] . ($timeZone1 [0] * 60 + $timeZone1 [1]);
                }
            }
        }
		
    }

    public function makeCmsRoutes(MvcEvent $e) {
        $router = $e->getRouter();
        $sm = $e->getApplication()->getServiceManager();
        $requestUri = $sm->get('request')->getRequestUri();
        $tmpArr = explode('?', $requestUri, 2);
        if(isset($tmpArr[0]))
        {
            $requestUri=$tmpArr[0];
        }
        if ($requestUri != '' && $requestUri != '/') {
            $routeName = explode('/', $requestUri);
            if (isset($routeName[2]) && !empty($routeName[2]) && $this->decrypt($routeName[2]) == $routeName[1]) {
                $routeName[2] = '1';
            } else {
                $routeName[2] = '';
            }
            $cmsUrl = $sm->get('Cms\Model\CmsTable')->getCrmContentUrl($routeName);
            if (!empty($cmsUrl[0])) {
                $route = Literal::factory(array(
                            'route' => $requestUri,
                            'defaults' => array(
                                'module' => 'Cms',
                                'controller' => 'Cms\Controller\Cms',
                                'action' => 'viewCmsPage'
                            )
                        ));
                $router->addRoute('asdsad', $route);
            }
        }
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Get Autoloader Configuration
     * @return array
     */
    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * Get Service Configuration
     * @return array
     */
    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Common\Model\CountryTable' => function($sm) {
                    $tableGateway = $sm->get('CountryTableGateway');
                    $table = new CountryTable($tableGateway);
                    return $table;
                },
                'CountryTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Country($dbAdapter));
                    return new TableGateway('tbl_mst_countries', $dbAdapter, null, $resultSetPrototype);
                },
                'Common\Model\CommonTable' => function($sm) {
                    $tableGateway = $sm->get('CommonTableGateway');
                    $table = new CommonTable($tableGateway);
                    return $table;
                },
                'CommonTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Common($dbAdapter));
                    return new TableGateway('tbl_mst_countries', $dbAdapter, null, $resultSetPrototype);
                },
                'Common\Model\QuestionTable' => function($sm) {
                    $tableGateway = $sm->get('QuestionTableGateway');
                    $table = new QuestionTable($tableGateway);
                    return $table;
                },
                'QuestionTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Question($dbAdapter));
                    return new TableGateway('tbl_mst_security_questions', $dbAdapter, null, $resultSetPrototype);
                },
                'Common\Model\EmailTable' => function($sm) {
                    $tableGateway = $sm->get('EmailTableGateway');
                    $table = new EmailTable($tableGateway);
                    return $table;
                },
                'EmailTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Email($dbAdapter));
                    return new TableGateway('tbl_mst_emails', $dbAdapter, null, $resultSetPrototype);
                },
                'Common\Model\SurveyTable' => function($sm) {
                    $tableGateway = $sm->get('SurveyTableGateway');
                    $table = new SurveyTable($tableGateway);
                    return $table;
                },
                'SurveyTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Question($dbAdapter));
                    return new TableGateway('tbl_mst_survey_questions', $dbAdapter, null, $resultSetPrototype);
                },
                'Common\Model\ContactTable' => function($sm) {
                    $tableGateway = $sm->get('ContactTableGateway');
                    $table = new ContactTable($tableGateway);
                    return $table;
                },
                'dbAdapter' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    return $dbAdapter;
                },
            ),
        );
    }

    /**
     * Get View Helper Configuration
     * @return array
     */
    public function getViewHelperConfig() {
        return array(
            'invokables' => array(
                'becomeAdult' => 'Common\View\Helper\BecomeAdult',
                'getAuth' => 'Common\View\Helper\Auth',
            ),
            'factories' => array(
                // the array key here is the name you will call the view helper by in your view scripts                
                'getAuth' => function($sm) {
                    $getAuth = $sm->getServiceLocator()->get(); // $sm is the view helper manager, so we need to fetch the main service manager
                    return new Common\View\Helper\Auth($getAuth);
                },
            ),
        );
    }

    /**
     * Get Controller Configuration
     * @return array
     */
    public function getControllerConfig() {
        return array();
    }

    public function loadConfiguration(MvcEvent $e) {
        $application = $e->getApplication();
        $serviceManager = $application->getServiceManager();
        $sharedManager = $application->getEventManager()->getSharedManager();
        $router = $serviceManager->get('router');
        $request = $serviceManager->get('request');

        $matchedRoute = $router->match($request);
        if (null !== $matchedRoute) {
            $sharedManager->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) use ($serviceManager) {
                        $serviceManager->get('ControllerPluginManager')->get('AclPlugin')
                                ->doAuthorization($e);
                    }, 2
            );
        }
    }

    public function loadCommonViewVars(MvcEvent $e) {
        $e->getViewModel()->setVariables(array(
            'auth' => $e->getApplication()->getServiceManager()->get('AuthService')
        ));
    }

    public function buildMenus(MvcEvent $e) {
        
    }

    function sort($params) {
        for ($i = 0; $i < count($params) - 1; $i++) {
            for ($j = $i + 1; $j < count($params); $j++) {
                if (isset($params[$i][0]['sort_order']) && !empty($params[$i][0]['sort_order'])) {
                    if ($params[$i][0]['sort_order'] > $params[$j][0]['sort_order']) {
                        $temp = $params[$i];
                        $params[$i] = $params[$j];
                        $params[$j] = $temp;
                    }
                } else {
                    if ($params[$i]['sort_order'] > $params[$j]['sort_order']) {
                        $temp = $params[$i];
                        $params[$i] = $params[$j];
                        $params[$j] = $temp;
                    }
                }
            }
        }
        return $params;
    }

    function getSubMenus($params, $sm) {
        $this->_commonTable = $sm->get('Common\Model\CommonTable');
        $menu = $this->_commonTable->getMenus(array('parentId' => $params, 'menuType' => '1'));
        if (isset($menu) && !empty($menu)) {
            $flag = 0;
            foreach ($menu as $key => $val) {
                $subMenu = $this->_commonTable->getMenus(array('parentId' => $val['menu_id'], 'menuType' => '1'));
                if (isset($subMenu) && !empty($subMenu)) {
                    $menu[$key]['subMenu'] = array();
                    array_push($menu[$key]['subMenu'], $subMenu);
                }
            }
            return $menu;
        } else {
            return $menu;
        }
    }

    function manageMenu($params,$kioskModuleSatausArr) {
        $new_array = array();
        //asd($params);
        foreach ($params as $key => $val) {
            $temp = array();
            $temp1 = array();
            if (isset($val['submenu']) && !empty($val['submenu'])) {
                for ($i = 0; $i < count($val['submenu']); $i++) {
                   $returnVal = $this->isModuleActive($val['submenu'][$i][0]['menu_id'],$kioskModuleSatausArr) ; 
                   if($returnVal == "continue"){
                       continue;
                   }
                    if (isset($val['submenu'][$i][0]['subMenu']) && !empty($val['submenu'][$i][0]['subMenu'])) {
                        //echo "here<br>";
                        array_push($temp, $val['submenu'][$i]);
                    } else {
                        // echo "there<br>";
                        array_push($temp1, $val['submenu'][$i]);
                    }
                }
                $temp = array_merge($temp, $temp1);
                $val['submenu'] = $temp;
            }
            array_push($new_array, $val);
        }
        return $new_array;
    }

    /*
      function is used to redirect the url if opened in kiosk, then it will be redirect to /crm
     */

    public function getSiteAllowCondition() {

        if ($this->_config['by_pass_source'] != '0') {
			if ($this->_config['application_source_id'] == '2' && $_SERVER['REQUEST_URI'] == '/') { // for kiosk
				header("Location: /crm");
			     exit(0);
			}
			if (($this->_config['application_source_id'] == '1' || $this->_config['application_source_id'] == '3') && $_SERVER['REQUEST_URI'] == '/crm') { // for kiosk
				header("Location: /");
			    exit(0);
			}
		}
    }
    
    public function getKioskModule($kioskId,$sm){
        //error_reporting(1);
        $searchParam['kiosk_name'] = "";
        $searchParam['current_status'] = "";
        $searchParam['is_wheelchair'] = "";
        $searchParam['history'] = "";
        $searchParam['contact_name'] = "";
        $searchParam['grid_main_view'] = '1';
        $searchParam['recordLimit'] = "";
        $searchParam['startIndex'] = "";
        $searchParam['recordLimit'] = "";
        $searchParam['sortField'] = "";
        $searchParam['sortOrder'] = "";
        $searchParam['currentTime'] = DATE_TIME_FORMAT;
        $searchParam['kiosk_id'] = $kioskId;
        $searchParam['curr_date'] = date("Y-m-d",strtotime(DATE_TIME_FORMAT));
        $searchParam['curr_time'] = date("H:i:s",strtotime(DATE_TIME_FORMAT));
        $searchResult = $sm->get('Kiosk\Model\KioskReservationTable')->searchKioskReservation($searchParam);
        return $searchResult[0];
        
    }
    
    public function getModuleStatus($data)
    { //asd($data);
        $result = array();
        $module = @explode(',',$data);
        if(empty($module))
            return false;
        
        foreach($module as $val){
             $module = explode('=>',$val);
             $result[$module[0]] = $module[1];
        }
        return $result;
        
    }
    public function getModuleCofigVal($data)
    { //asd($data);
        $result = array();
        $module = @explode(',',$data);
        if(empty($module))
            return false;
        
        foreach($module as $val){
             $module = explode('=>',$val);
             $result[$module[0]] = $module[1];
        }
        return $result;
        
    }
    public function isModuleActive($id,$kioskModuleSatausArr){
        $mapArr = array('1158'=>'8','1468'=>'13');
       if(array_key_exists($id, $mapArr)){
           $modId = $mapArr[$id];
           if(empty($kioskModuleSatausArr[$modId])){
               return "continue";
           }else{
               return "";
           }
       }
    }
    /*Function to get Requesting Machine IP
     */
    public function getRemoteIP(){
        
        if (isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
            $ip = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipList = explode(", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip = trim($ipList[0]);
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
       return $ip;
    }
    /*Function to get Kiosk ID of Requesting Machine
     */
    public function getKioskId($ip,$sm){
       $this->_config = $sm->get('Config');
       $kioskIps = $this->_config['kiosk_reservation_system'];
       $kioskId = null;
       if(is_array($ip)){
           foreach($ip as $key => $val){
               if(array_key_exists($val, $kioskIps)){
                   $kioskId = $kioskIps[$val];
               }
           }
       }else{
           if(array_key_exists($ip, $kioskIps)){
                   $kioskId = $kioskIps[$ip];
           }
       }
       return $kioskId;
    }
	
	function getMembershipDiscount(MvcEvent $e) {
            $sm = $e->getApplication()->getServiceManager();
            $auth = new AuthenticationService();
            // start membership discount code;
                    if(!empty($auth->getIdentity()->user_id) || !empty($auth->getIdentity()->crm_user_id)) {
                            if(isset($auth->getIdentity()->user_id) && !empty($auth->getIdentity()->user_id)) {
                                $shopCartUserId = $auth->getIdentity()->user_id;
                                $cartSourceType = 'frontend';
                            }
                            else {
                                if (is_numeric($e->getRequest()->getPost('user_id'))) {
                                    $shopCartUserId = $e->getRequest()->getPost('user_id');
                                }
                                else {
                                    $shopCartUserId = $this->decrypt($e->getRequest()->getPost('user_id'));
                                }
                                $cartSourceType = 'backend';
                            }
                       // die;
                        if(!empty($shopCartUserId)){
                                $auth = new AuthenticationService();            
                                $date_range = $this->getDateRange(4);
                                $userPurchasedProductArray['from_date'] = $this->DateFormat($date_range['from'], 'db_date_format_from');
                                $userPurchasedProductArray['to_date'] = $this->DateFormat($date_range['to'], 'db_date_format_to');
                                $userPurchasedProductArray['user_id'] = $shopCartUserId;

                                $totalAmountPurchased = $sm->get('Transaction\Model\TransactionTable')->getTotalUserPurchased($userPurchasedProductArray);

                                $userCartParamArray['user_id'] = $shopCartUserId;
                                $userCartParamArray['source_type'] = $cartSourceType;
                                $cartlistArray =  $sm->get('Transaction\Model\TransactionTable')->getCartProducts($userCartParamArray);                            

                                $cartElementDonationAmount = 0;
                                if(count($cartlistArray)>0) {
                                    foreach($cartlistArray as $cartKey=>$cartVal){
                                        if(($cartVal['product_transaction_type']==1) || empty($cartVal['product_id'])) { // for donation type product only
                                            $cartElementDonationAmount = $cartElementDonationAmount + $cartVal['product_subtotal'];
                                        }
                                    }
                                }
                                $minimunDonationAmount = isset($totalAmountPurchased) ? $totalAmountPurchased : '0';
                                $minimunDonationAmount = $minimunDonationAmount + $cartElementDonationAmount;
                                $userMembershipUpdateArr['0'] = $minimunDonationAmount;
                                $matchingMembership = $sm->get('Transaction\Model\TransactionTable')->getMatchingMembership($userMembershipUpdateArr);

                                if($matchingMembership['discount']!=0 && !empty($matchingMembership['discount'])){
                                    define('MEMBERSHIP_DISCOUNT', $matchingMembership['discount']);
                                }
                            }   
                    }
            // End of the membership discount code;   
    } 

}

