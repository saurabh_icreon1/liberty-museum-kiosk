<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
            'Dateformat' => 'Common\View\Helper\Dateformat',
            'Dateformatwithtime' => 'Common\View\Helper\Dateformatwithtime',
            'Timeformat' => 'Common\View\Helper\Timeformat',
            'Dateformatalpha' => 'Common\View\Helper\Dateformatalpha',
            'Currencyformat' => 'Common\View\Helper\Currencyformat',
            'Encrypt' => 'Common\View\Helper\Encrypt',
            'GetManifestPath' => 'Common\View\Helper\GetManifestPath',
            'Roundamount' => 'Common\View\Helper\Roundamount',
            'ConvertTifToJpg'=> 'Common\View\Helper\ConvertTifToJpg'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Common\Controller\Common' => 'Common\Controller\CommonController',
            'Common\Controller\Do' => 'Common\Controller\DoController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'getContacts' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-contacts',
                    'defaults' => array(
                        'controller' => 'Common\Controller\Common',
                        'action' => 'getContacts',
                    ),
                ),
            ),
            'getCompanies' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-company',
                    'defaults' => array(
                        'controller' => 'Common\Controller\Common',
                        'action' => 'getCompany',
                    ),
                ),
            ),
            'getCrmContacts' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-contacts[/:ids]',
                    'defaults' => array(
                        'controller' => 'Common\Controller\Common',
                        'action' => 'getCrmContacts',
                    ),
                ),
            ),
            'setUserTimezone' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/set-user-timezone[/:timeDiff]',
                    'defaults' => array(
                        'controller' => 'Common\Controller\Common',
                        'action' => 'setUserTimezone',
                    ),
                ),
            ),
            'getCampaigns' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-campaigns',
                    'defaults' => array(
                        'controller' => 'Common\Controller\Common',
                        'action' => 'getCampaigns',
                    ),
                ),
            ),
            'getMembershipAfihc' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-membership-afihc',
                    'defaults' => array(
                        'controller' => 'Common\Controller\Common',
                        'action' => 'getMembershipAfihc',
                    ),
                ),
            ),
            'getPhoneLookup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-phone-lookup',
                    'defaults' => array(
                        'controller' => 'Common\Controller\Common',
                        'action' => 'getPhoneLookup',
                    ),
                ),
            ),
            'getPostalLookup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-postal-lookup',
                    'defaults' => array(
                        'controller' => 'Common\Controller\Common',
                        'action' => 'getPostalLookup',
                    ),
                ),
            ),
            'getStateLookup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-state-lookup',
                    'defaults' => array(
                        'controller' => 'Common\Controller\Common',
                        'action' => 'getStateLookup',
                    ),
                ),
            ),
            'getCountryLookup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-country-lookup',
                    'defaults' => array(
                        'controller' => 'Common\Controller\Common',
                        'action' => 'getCountryLookup',
                    ),
                ),
            ),
            'unauthorizeUser' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/unauthorize-user',
                    'defaults' => array(
                        'controller' => 'Common\Controller\Common',
                        'action' => 'unauthorizeUser',
                    ),
                ),
            ),
            'getuserfamilyperson' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-user-family-person',
                    'defaults' => array(
                        'controller' => 'Common\Controller\Common',
                        'action' => 'getUserFamilyPerson',
                    ),
                ),
            ),
            'authenticateuser' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/authenticate-user',
                    'defaults' => array(
                        'controller' => 'Common\Controller\Common',
                        'action' => 'authenticateUser',
                    ),
                ),
            ),
            'donow' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/donow[/:cronParam]',
                    'defaults' => array(
                        'controller' => 'Common\Controller\Do',
                        'action' => 'donow',
                    ),
                ),
            ),
            //inscribe key for previous transaction
            'inscribekey' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/inscribe-key',
                    'defaults' => array(
                        'controller' => 'Common\Controller\Do',
                        'action' => 'inscribekey',
                    ),
                ),
            ),
            'getTraCampaigns' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-tra-campaigns',
                    'defaults' => array(
                        'controller' => 'Common\Controller\Common',
                        'action' => 'getTraCampaigns',
                    ),
                ),
            ),
            'search' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/search[/:pID]',
                    'defaults' => array(
                        'controller' => 'Common\Controller\Common',
                        'action' => 'searchManifest',
                    ),
                ),
            ),
			'getContactAutoSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-contact-auto-search',
                    'defaults' => array(
                        'controller' => 'Common\Controller\Common',
                        'action' => 'getContactAutoSearch',
                    ),
                ),
            ),
        )
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'phparray',
                'base_dir' => __DIR__ . '/../languages',
                'pattern' => '%s.php',
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'common' => __DIR__ . '/../layout',
        ),
    ),
    'leftMenus' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
);
