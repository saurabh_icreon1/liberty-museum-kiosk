<?php

/**
 * This model is used for common function used in all over projects.
 * @package    Common/Common
 * @author     Icreon Tech - DG
 */

namespace Common\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for common function used in all over projects.
 * @package    Common/Common
 * @author     Icreon Tech - DG
 */
class CommonTable {

    protected $tableGateway;
    protected $connection;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

    /**
     * Function for to get Source
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getSource($dataArr) {
        $sourceId = (isset($dataArr['source_id']) && $dataArr['source_id'] != '') ? $dataArr['source_id'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getSource(?)");
        $stmt->getResource()->bindParam(1, $sourceId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get User Type
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getType() {
        $result = $this->connection->execute("CALL usp_com_getUserType()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get User Interest
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getInterest() {
        $result = $this->connection->execute("CALL usp_com_getInterests()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get User Group
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getGroups() {
        //ticket 73 private group
        if(ALLOW_PRIVATE_GROUP == 1)
        {
            $allow_private = 1;
        }
        else
        {
            $allow_private = 0;
        }

        $result = $this->connection->execute("CALL usp_com_getGroups('$allow_private')");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get total Count
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getTotalCount() {
        $result = $this->connection->execute("CALL usp_com_getRecordCount()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_OBJ);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get all titles
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getTitle() {
        $result = $this->connection->execute("CALL usp_com_getTitle()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get all suffix
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getSuffix() {
        $result = $this->connection->execute("CALL usp_com_getSuffix()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to getLocationType
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getLocationType($dataArr) {
        $locationIds = (isset($dataArr['location_ids']) && $dataArr['location_ids'] != '') ? $dataArr['location_ids'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getLocationType(?)");
        $stmt->getResource()->bindParam(1, $locationIds);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get Age Range
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getAgeRange() {
        $result = $this->connection->execute("CALL usp_com_getAgeRange()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get Income  Range
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getIncomeRange() {
        $result = $this->connection->execute("CALL usp_com_getIncomeRange()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get Address Greeting
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getAddressGreeting($dataArr) {
        $contactType = (isset($dataArr['contact_type']) && $dataArr['contact_type'] != '') ? $dataArr['contact_type'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_com_getAddressGreeting(?)');
        $stmt->getResource()->bindParam(1, $contactType);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get Postal Greeting
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getPostalGreeting($dataArr) {
        $contactType = (isset($dataArr['contact_type']) && $dataArr['contact_type'] != '') ? $dataArr['contact_type'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_com_getPostalGreeting(?)');
        $stmt->getResource()->bindParam(1, $contactType);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get Email Greeting
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getEmailGreeting($dataArr) {
        $contactType = (isset($dataArr['contact_type']) && $dataArr['contact_type'] != '') ? $dataArr['contact_type'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_com_getEmailGreeting(?)');
        $stmt->getResource()->bindParam(1, $contactType);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get master company names
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getCompanies() {
        $result = $this->connection->execute("CALL usp_com_getCompanies()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get master transaction range
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getTransactionRange() {
        $result = $this->connection->execute("CALL usp_com_getTransactionRange()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get master education 
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getEducationDegreeList($dataArr) {
        $educationIds = (isset($dataArr['education_ids']) && $dataArr['education_ids'] != '') ? $dataArr['education_ids'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getEducations(?)");
        $stmt->getResource()->bindParam(1, $educationIds);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get Tages
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getTags() {
        $result = $this->connection->execute("CALL usp_com_getTags()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get Tages
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getRelations($dataArr = array()) {
        $relationId = (isset($dataArr['relation_id']) && $dataArr['relation_id'] != '') ? $dataArr['relation_id'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getRelations(?)");
        $stmt->getResource()->bindParam(1, $relationId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get Campaign Types
     * @author Icreon Tech - AS
     * @return Array
     * @param Array
     */
    public function getCampaignType($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getCampaignType(?)");
        $stmt->getResource()->bindParam(1, $params);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * Function for to get Case Mode Types
     * @author Icreon Tech - DT
     * @return Array
     * @param void
     */
    public function getCaseModeType() {
        $result = $this->connection->execute("CALL usp_com_getCaseModeTypes()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get Case Types
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getCaseTypes($params) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getCaseTypes(?)");
        $stmt->getResource()->bindParam(1, $params[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get Case Status
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getCaseStatus($params = array('')) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getCaseStatus(?)");
        $stmt->getResource()->bindParam(1, $params[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get Pledge Status
     * @author Icreon Tech - AG
     * @return Array
     * @param Array
     */
    public function getPledgeStatus() {
        $result = $this->connection->execute("CALL usp_com_getPledgeStatus()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get Channel Types
     * @author Icreon Tech - AS
     * @return Array
     * @param Array
     */
    public function getChannelType($params = array()) {
        $channelIds = (isset($params['channel_ids']) && $params['channel_ids'] != '') ? $params['channel_ids'] : '';
        $channelName = (isset($params['channel_name']) && $params['channel_name'] != '') ? $params['channel_name'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getChannelType(?,?)");
        $stmt->getResource()->bindParam(1, $channelIds);
        $stmt->getResource()->bindParam(2, $channelName);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * Function for to get Campaign Status
     * @author Icreon Tech - AS
     * @return Array
     * @param Array
     */
    public function getCampaignStatus($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getCampaignStatus(?)");
        $stmt->getResource()->bindParam(1, $params);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * Function for to get master org
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getOrganizations() {
        $result = $this->connection->execute("CALL usp_com_getOrganizations()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $resultSet;
    }

    /**
     * Function for to get master Industry
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getIndustry() {
        $result = $this->connection->execute("CALL usp_com_getIndustry()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $resultSet;
    }

    /**
     * Function for to get user common detail
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getUserCommonInfo($dataArr) {
        $user_id = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_usr_getUserInfo(?)");
        $stmt->getResource()->bindParam(1, $user_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0];
        } else {
            return false;
        }
    }

    /**
     * Function for to create company
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function createCompany($dataArr) {
        $name = (isset($dataArr['name'])) ? $dataArr['name'] : '';
        $comp_email = (isset($dataArr['comp_email'])) ? $dataArr['comp_email'] : '';
        $street = (isset($dataArr['street'])) ? $dataArr['street'] : '';
        $state = (isset($dataArr['state'])) ? $dataArr['state'] : '';
        $country = (isset($dataArr['country'])) ? $dataArr['country'] : '';
        $phone = (isset($dataArr['phone'])) ? $dataArr['phone'] : '';
        $description = (isset($dataArr['description'])) ? $dataArr['description'] : '';
        $city = (isset($dataArr['city'])) ? $dataArr['city'] : '';
        $postal_code = (isset($dataArr['postal_code'])) ? $dataArr['postal_code'] : '';
        $add_date = (isset($dataArr['add_date'])) ? $dataArr['add_date'] : '';
        $modified_date = (isset($dataArr['modified_date'])) ? $dataArr['modified_date'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_createCompany(?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $name);
        $stmt->getResource()->bindParam(2, $comp_email);
        $stmt->getResource()->bindParam(3, $state);
        $stmt->getResource()->bindParam(4, $country);
        $stmt->getResource()->bindParam(5, $description);
        $stmt->getResource()->bindParam(6, $city);
        $stmt->getResource()->bindParam(7, $postal_code);
        $stmt->getResource()->bindParam(8, $street);
        $stmt->getResource()->bindParam(9, $phone);
        $stmt->getResource()->bindParam(10, $add_date);
        $stmt->getResource()->bindParam(11, $modified_date);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            return false;
        }
    }

    /**
     * Function for to get all lead type
     * @author Icreon Tech - SK
     * @return Array
     * @param Array
     */
    public function getLeadType($params) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getLeadType(?)");
        $stmt->getResource()->bindParam(1, $params);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get all lead sources
     * @author Icreon Tech - SK
     * @return Array
     * @param Array
     */
    public function getLeadSource($params) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getLeadSource(?)");
        $stmt->getResource()->bindParam(1, $params);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get all lead type
     * @author Icreon Tech - SK
     * @return Array
     * @param Array
     */
    public function getLeadReason() {
        $result = $this->connection->execute("CALL usp_com_getLeadReason()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get all lead sources
     * @author Icreon Tech - SK
     * @return Array
     * @param Array
     */
    public function getMembership() {
        $result = $this->connection->execute("CALL usp_com_getMembership()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get Activity type
     * @author Icreon Tech - DT
     * @return Array
     * @param void
     */
    public function getActivityType() {
        $result = $this->connection->execute("CALL usp_com_getActivityTypes()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get Activity priorities
     * @author Icreon Tech - DT
     * @return Array
     * @param void
     */
    public function getActivityPriorities() {
        $result = $this->connection->execute("CALL usp_com_getActivityPriorities()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get Activity source type
     * @author Icreon Tech - DT
     * @return Array
     * @param void
     */
    public function getActivitySourceTypes() {
        $result = $this->connection->execute("CALL usp_com_getActivitySourceTypes()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get Activity status
     * @author Icreon Tech - DT
     * @return Array
     * @param void
     */
    public function getActivityStatus() {
        $result = $this->connection->execute("CALL usp_com_getActivityStatus()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get all currencies
     * @author Icreon Tech - AG
     * @return Array
     * @param Array
     */
    public function getCurrencies() {
        $result = $this->connection->execute("CALL usp_com_getCurrency()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get all contribution types
     * @author Icreon Tech - AG
     * @return Array
     * @param Array
     */
    public function getContributionTypes() {
        $result = $this->connection->execute("CALL usp_com_getContributionTypes()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get all contribution types
     * @author Icreon Tech - AG
     * @return Array
     * @param Array
     */
    public function getInstallmentTypes() {
        $result = $this->connection->execute("CALL usp_com_getInstallmentTypes()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

     /**
     * Function for get compaign detail
     * @author Icreon Tech - NS
     * @return Int
     * @param Array
     */
    public function getCampaignAutoSuggest($params) {
        $params['campaign_title'] = (isset($params['campaign_title']) and trim($params['campaign_title']) != "") ? trim($params['campaign_title']) : "";
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getCampaignsAutoSuggest(?)");
        $stmt->getResource()->bindParam(1, $params['campaign_title']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll();
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }
    
    /**
     * Function for get compaign detail
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function getCampaign($params) {
        $procquesmarkapp = $this->appendQuestionMars(count($params));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getCampaigns(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $params[0]);
        $stmt->getResource()->bindParam(2, $params[1]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll();
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get conatct list - Auto Suggest
     * @author Icreon Tech - NS
     * @return Arr
     * @param Array
     */
    public function getContactAutoSuggest($data_arr = null) { 
        $procquesmarkapp = $this->appendQuestionMars(count($data_arr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_usr_getContactsLookUpAutoSuggest(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $data_arr[0]);
        $stmt->getResource()->bindParam(2, $data_arr[1]);
        $stmt->getResource()->bindParam(3, $data_arr[2]);
        $stmt->getResource()->bindParam(4, $data_arr[3]);
        $stmt->getResource()->bindParam(5, $data_arr[4]);
		$stmt->getResource()->bindParam(6, $data_arr[5]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }
    
    /**
     * Function for to get conatct list
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function getContact($data_arr = null) {
        $procquesmarkapp = $this->appendQuestionMars(count($data_arr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_usr_getContactsLookUp(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $data_arr[0]);
        $stmt->getResource()->bindParam(2, $data_arr[1]);
        $stmt->getResource()->bindParam(3, $data_arr[2]);
        $stmt->getResource()->bindParam(4, $data_arr[3]);
        $stmt->getResource()->bindParam(5, $data_arr[4]);
        $stmt->getResource()->bindParam(6, $data_arr[5]);
        $stmt->getResource()->bindParam(7, $data_arr[6]);
        $stmt->getResource()->bindParam(8, $data_arr[7]);
        $stmt->getResource()->bindParam(9, $data_arr[8]);
        $stmt->getResource()->bindParam(10, $data_arr[9]);
        $stmt->getResource()->bindParam(11, $data_arr[10]);
        $stmt->getResource()->bindParam(12, $data_arr[11]);
        $stmt->getResource()->bindParam(13, $data_arr[12]);
        $stmt->getResource()->bindParam(14, $data_arr[13]);
        $stmt->getResource()->bindParam(15, $data_arr[14]);
        $stmt->getResource()->bindParam(16, $data_arr[15]);
        $stmt->getResource()->bindParam(17, $data_arr[16]);
        $stmt->getResource()->bindParam(18, $data_arr[17]);
        $stmt->getResource()->bindParam(19, $data_arr[18]);
        $stmt->getResource()->bindParam(20, $data_arr[19]);
        $stmt->getResource()->bindParam(21, $data_arr[20]);
        $stmt->getResource()->bindParam(22, $data_arr[21]);
        $stmt->getResource()->bindParam(23, $data_arr[22]);
        $stmt->getResource()->bindParam(24, $data_arr[23]);
        $stmt->getResource()->bindParam(25, $data_arr[24]);
        $stmt->getResource()->bindParam(26, $data_arr[25]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get company list
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getCompany($data_arr = null) {
        $procquesmarkapp = $this->appendQuestionMars(count($data_arr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getCompany(?)");
        $stmt->getResource()->bindParam(1, $data_arr[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for get crm user detail
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function getUserCrm($user_arr) {
        $procquesmarkapp = $this->appendQuestionMars(count($user_arr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_usr_getCrmUser(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $user_arr[0]);
        $stmt->getResource()->bindParam(2, $user_arr[1]);
        $stmt->getResource()->bindParam(3, $user_arr[2]);
        $stmt->getResource()->bindParam(4, $user_arr[3]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for append number of questions marks for stored procedure
     * @author Icreon Tech - DT
     * @return String
     * @param Array
     */
    public function appendQuestionMars($count) {
        $returnstr = '';
        for ($i = 1; $i <= $count; $i++) {
            $returnstr.='?,';
        }
        return rtrim($returnstr, ',');
    }

    /**
     * Function to get Assets Id
     * @author Icreon Tech - AS
     * @return Array
     * @param Array
     */
    public function getShipAssets($param) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getShipAssets(?,?)");
        $stmt->getResource()->bindParam(1, $param['ship_id']);
        $stmt->getResource()->bindParam(2, $param['assets_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $resultSet;
    }

    /**
     * Function for get crm user detail
     * @author Icreon Tech - DT
     * @return Array
     * @param void
     */
    public function getProducts() {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getProducts()");
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for get product type
     * @author Icreon Tech - SR
     * @return Array
     * @param void
     */
    public function getProductsType() {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getProductType()");
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function to get all crm  product name and id
     * @author Icreon Tech - AP
     * @return Array
     * @param void
     */
    public function getCrmProducts($param = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_pro_getReletedProducts(?)");
        $stmt->getResource()->bindParam(1, $param['product_name']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for get user reason
     * @author Icreon Tech - DG
     * @return Array
     * @param void
     */
    public function getUserReason() {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getUserReason()");
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for get promotion Applicable for
     * @author Icreon Tech - DT
     * @return Array
     * @param void
     */
    public function getPromotionApplicable() {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getPromotionApplicable()");
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /* Function for get product units
     * @author Icreon Tech- AP
     * param void
     * return array 
     */

    public function getProductsUnit() {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_pro_getProductUnits()");
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /* Function for get product units
     * @author Icreon Tech- AP
     * param void
     * return array
     */

    public function getSearchSaveTypes() {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getSearchSaveTypes()");
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function to get menus for frontend
     * @author Icreon Tech - DG
     * @return Array
     * @param void
     */
    public function getMenus($param = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getMenus(?,?)");
        $stmt->getResource()->bindParam(1, $param['parentId']);
        $stmt->getResource()->bindParam(2, $param['menuType']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function to get blog categories
     * @author Icreon Tech - DG
     * @return Array
     * @param void
     */
    public function getBlogCategories($param = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getBlogCategories()");
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for get membership afihc autosuggest
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function getMembershipAfihc($afihcArr) {
        $procquesmarkapp = $this->appendQuestionMars(count($afihcArr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getMembershipAFIHC(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $afihcArr[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for get phone autosuggest
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function getPhoneUser($phoneArr) {
        $procquesmarkapp = $this->appendQuestionMars(count($phoneArr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getContactPhone(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $phoneArr[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for get postal autosuggest
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function getZipCodeUser($zipArr) {
        $procquesmarkapp = $this->appendQuestionMars(count($zipArr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getContactPostal(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $zipArr[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for get state autosuggest
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function getStateUser($stateArr) {
        $procquesmarkapp = $this->appendQuestionMars(count($stateArr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getContactState(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $stateArr[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for get country autosuggest
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function getCountryUser($countryArr) {
        $procquesmarkapp = $this->appendQuestionMars(count($countryArr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getContactCountry(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $countryArr[0]);
        $stmt->getResource()->bindParam(2, $countryArr[1]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function to get transaction source
     * @author Icreon Tech - DT
     * @return Array
     * @param void
     */
    public function getTransactionSource($param = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getTransactionSource()");
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function to get transaction status
     * @author Icreon Tech - DT
     * @return Array
     * @param void
     */
    public function getTransactionStatus($param = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getTransactionStatus()");
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function to get payment mode
     * @author Icreon Tech - DT
     * @return Array
     * @param void
     */
    public function getPaymentMode($param = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getPaymentMode()");
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    public function getCrmFamilyAssociatedPerson($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getCrmFamilyAssociatedPerson(?,?)');
            $first_name = isset($param['first_name']) ? $param['first_name'] : '';
            $last_name = isset($param['last_name']) ? $param['last_name'] : '';
            $stmt->getResource()->bindParam(1, $first_name);
            $stmt->getResource()->bindParam(2, $last_name);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for get product mapping types
     * @author Icreon Tech - DT
     * @return Array
     * @param void
     */
    public function getProductsMappingType($prodMappArr = array()) {
        $procquesmarkapp = $this->appendQuestionMars(count($prodMappArr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getProductMappingType($procquesmarkapp)");
        $stmt->getResource()->bindParam(1, $prodMappArr[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for get family relationships
     * @author Icreon Tech - DT
     * @return Array
     * @param void
     */
    public function getFamilyRelationships() {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getFamilyRelationships()");
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get Nationality
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getNationality($dataArr) {
        $nationalityId = (isset($dataArr['nationality_id']) && $dataArr['nationality_id'] != '') ? $dataArr['nationality_id'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getNationality(?)");
        $stmt->getResource()->bindParam(1, $nationalityId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get Ethinicity
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getEthinicity($dataArr) {
        $ethnicityId = (isset($dataArr['ethnicity_id']) && $dataArr['ethnicity_id'] != '') ? $dataArr['ethnicity_id'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getEthinicity(?)");
        $stmt->getResource()->bindParam(1, $ethnicityId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get State
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getStates() {
        $result = $this->connection->execute("CALL usp_com_getStates()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get product status
     * @author Icreon Tech - AS
     * @return Array
     * @param Array
     */
    public function getProductStatus() {
        $result = $this->connection->execute("CALL usp_com_getProductStatus()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get check type
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getCheckTypes() {
        $result = $this->connection->execute("CALL usp_com_getCheckTypes()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get check type
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getCheckIDTypes() {
        $result = $this->connection->execute("CALL usp_com_getCheckIDTypes()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get CRM Left Menus
     * @author Icreon Tech - SK
     * @return Array
     */
    public function getCrmLeftMenus() {
        $result = $this->connection->execute("CALL usp_com_getCrmLeftMenus()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to get First Pre Member Ship Alert
     * @author Icreon Tech - NS
     * @return Array
     */
    public function getFirstPreMembershipAlert($params) {
        try {
            $currentDate = $params['currDate'];
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_usr_getFirstPreMembershipAlert(?)");
            $stmt->getResource()->bindParam(1, $currentDate);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();

            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get Repeat Pre Member Ship Alert
     * @author Icreon Tech - NS
     * @return Array
     */
    public function getRepeatPreMembershipAlert($params) {
        try {
            $currentDate = $params['currDate'];
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_usr_getRepeatPreMembershipAlert(?)");
            $stmt->getResource()->bindParam(1, $currentDate);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();

            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get Come Back Post Member Ship Alert
     * @author Icreon Tech - NS
     * @return Array
     */
    public function getRenewPostMembershipAlert($params) {
        try {
            $currentDate = $params['currDate'];
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_usr_getRenewPostMembershipAlert(?)");
            $stmt->getResource()->bindParam(1, $currentDate);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();

            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get Come Back Post Member Ship Alert
     * @author Icreon Tech - NS
     * @return Array
     */
    public function getComeBackPostMembershipAlert($params) {
        try {
            $currentDate = $params['currDate'];
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_usr_getComeBackPostMembershipAlert(?)");
            $stmt->getResource()->bindParam(1, $currentDate);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();

            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get First Pledge Due Date Alert
     * @author Icreon Tech - NS
     * @return Array
     */
    public function getFirstPledgeDueDateAlert($params) {
        try {
            $currentDate = $params['currDate'];
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_ple_getFirstPledgeDueDateAlert(?)");
            $stmt->getResource()->bindParam(1, $currentDate);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();

            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get Repeat Pledge Due Date Alert
     * @author Icreon Tech - NS
     * @return Array
     */
    public function getRepeatPledgeDueDateAlert($params) {
        try {
            $currentDate = $params['currDate'];
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_ple_getRepeatPledgeDueDateAlert(?)");
            $stmt->getResource()->bindParam(1, $currentDate);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();

            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert Pledge Email Log Alert
     * @author Icreon Tech - NS
     * @return boolean
     */
    public function insertPleEmailLogAlert($params) {
        try {
            $params['pledge_transaction_id'] = isset($params['pledge_transaction_id']) ? $params['pledge_transaction_id'] : '';
            $params['alert_type'] = isset($params['alert_type']) ? $params['alert_type'] : '';
            $params['log_date'] = isset($params['log_date']) ? $params['log_date'] : '';

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_ple_insertPleEmailLog(?,?,?)");
            $stmt->getResource()->bindParam(1, $params['pledge_transaction_id']);
            $stmt->getResource()->bindParam(2, $params['alert_type']);
            $stmt->getResource()->bindParam(3, $params['log_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert User Email Log Alert
     * @author Icreon Tech - NS
     * @return boolean
     */
    public function insertUserEmailLogAlert($params) {
        try {
            $params['user_id'] = isset($params['user_id']) ? $params['user_id'] : '';
            $params['user_membership_id'] = isset($params['user_membership_id']) ? $params['user_membership_id'] : '';
            $params['alert_type'] = isset($params['alert_type']) ? $params['alert_type'] : '';
            $params['log_date'] = isset($params['log_date']) ? $params['log_date'] : '';

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_usr_insertUserEmailLog(?,?,?,?)");
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['user_membership_id']);
            $stmt->getResource()->bindParam(3, $params['alert_type']);
            $stmt->getResource()->bindParam(4, $params['log_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update tracking code and status of all product associated with the shipment id .
     * @param this will be an array having  shipment id and tracking number 
     * @return this will return a confirmation
     * @author Icreon Tech - NS
     */
     public function updateShipmentTrackingCodeAndProductStatus($param) {
        try {
            if (isset($param['ShipmentID']) and trim($param['ShipmentID']) != "") {
                $ShipmentID = trim($param['ShipmentID']);
            } else {
                $ShipmentID = '';
            }
            if (isset($param['TrackingNumbers']) and trim($param['TrackingNumbers']) != "") {
                $TrackingNumbers = trim($param['TrackingNumbers']);
            } else {
                $TrackingNumbers = '';
            }
 	        if (isset($param['ShipDate']) and trim($param['ShipDate']) != "") {
                $shipmantDate = trim($param['ShipDate']);
            } else {
                $shipmantDate= '';
            }

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_updateShipmentStatus(?,?,?)');
            $stmt->getResource()->bindParam(1, $ShipmentID);
            $stmt->getResource()->bindParam(2, $TrackingNumbers);
            $stmt->getResource()->bindParam(3, $shipmantDate);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get PB Setting
     * @author Icreon Tech - DT
     * @return Array
     */
    public function getPBSetting() {
        $result = $this->connection->execute("CALL usp_com_getPBSetting()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }
    /**
     * Function to get series
     * @author Icreon Tech - SR
     * @return Array
     * @param Array
     */
    public function getPassengerSeries() {
        $result = $this->connection->execute("CALL usp_pas_getSeries()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }    
    /**
     * Function for get compaign detail
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function getTraCampaign($params) {
        $procquesmarkapp = $this->appendQuestionMars(count($params));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getTraCampaigns(?)");
        $stmt->getResource()->bindParam(1, $params['campaign_name']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll();
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }
    
    
     /**
     * Function to current terms version
     * @author Icreon Tech - NS
     * @return Array
     * @param Array
     */
    public function getCurrentTermsVersion() {
        $result = $this->connection->execute("CALL usp_cms_getCurrentTermsVersion()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return (isset($resultSet[0]['currVersionId']) and trim($resultSet[0]['currVersionId']) != "") ? $resultSet[0]['currVersionId'] : ""; 
        else
            return false;
    }  
    
        /**
     * Function for get compaign detail
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function updateUserTermsVersion($params) {
       try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_usr_updateUserTermsVersion(?,?,?)");
            $stmt->getResource()->bindParam(1, $params['terms_version_id']);
            $stmt->getResource()->bindParam(2, $params['terms_accept_date']);
            $stmt->getResource()->bindParam(3, $params['user_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        }
        catch(Exception $e) {
             return false;
        }
    }

	/**
     * Function for to get master company list (exact match)
     * @author Icreon Tech - SK
     * @return Arr
     * @param Array
     */
    public function getMasterCompany($data_arr = null) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getMasterCompany(?)");
        $stmt->getResource()->bindParam(1, $data_arr['company_name']);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

	/**
     * Function for to create company
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function createMasterCompany($dataArr = array()) {
		$procquesmarkapp = $this->appendQuestionMars(count($dataArr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_createCompany(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $dataArr[0]);
        $stmt->getResource()->bindParam(2, $dataArr[1]);
        $stmt->getResource()->bindParam(3, $dataArr[2]);
        $stmt->getResource()->bindParam(4, $dataArr[3]);
        $stmt->getResource()->bindParam(5, $dataArr[4]);
        $stmt->getResource()->bindParam(6, $dataArr[5]);
        $stmt->getResource()->bindParam(7, $dataArr[6]);
        $stmt->getResource()->bindParam(8, $dataArr[7]);
        $stmt->getResource()->bindParam(9, $dataArr[8]);
        $stmt->getResource()->bindParam(10, $dataArr[9]);
        $stmt->getResource()->bindParam(11, $dataArr[10]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            return false;
        }
    }
    
    /**
     * Function for to get Channel promo code
     * @author Icreon Tech - AS
     * @return Array
     * @param Array
     */
    public function getChannelPromo($params = array()) {
        $campaignId = (isset($params['campaign_id']) && $params['campaign_id'] != '') ? $params['campaign_id'] : '';
        $channelId = (isset($params['channel_id']) && $params['channel_id'] != '') ? $params['channel_id'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getChannelPromo(?,?)");
        $stmt->getResource()->bindParam(1, $campaignId);
        $stmt->getResource()->bindParam(2, $channelId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }
	
	public function getContactAutoSearch($data_arr = null) { 
        $procquesmarkapp = $this->appendQuestionMars(count($data_arr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_usr_getContactAutoSearch(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $data_arr[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }
     /**
     * this function update user id ont the basis of current session id into cart table
     * @param type $params
     * @return boolean
     */
    public function updateUserCartsData($params = array())
    {
        if(isset($params['user_id']) && $params['user_id'] != "" && isset($params['user_session_id']) && $params['user_session_id'] != "")
        {
            try
            {
                $stmt = $this->dbAdapter->createStatement();
                $stmt->prepare("CALL usp_tra_updateWOHAddToCart(?,?)");
                $stmt->getResource()->bindParam(1, $params['user_id']);
                $stmt->getResource()->bindParam(2, $params['user_session_id']);
                $result = $stmt->execute();
                $statement = $result->getResource();
                $statement->closeCursor();
                return true;
            } catch (Exception $e) {
                return false;
            }
       }
    }
    /**
     * this function update user id ont the basis of current session id into cart table
     * @param type $params
     * @return boolean
     */
    public function deleteSessionCartsData($params = array())
    {
        if(isset($params['user_id']) && $params['user_id'] != "" && isset($params['user_session_id']) && $params['user_session_id'] != "")
        {
            try
            {
                $stmt = $this->dbAdapter->createStatement();
                $stmt->prepare("CALL usp_com_deleteSessionCartsData(?,?,?)");
                $stmt->getResource()->bindParam(1, $params['user_id']);
                $stmt->getResource()->bindParam(2, $params['user_session_id']);
                $stmt->getResource()->bindParam(3, $params['product_id']);
                $result = $stmt->execute();
                $statement = $result->getResource();
                $statement->closeCursor();
                return true;
            } catch (Exception $e) {
                return false;
            }
       }
    }

}