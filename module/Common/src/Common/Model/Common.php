<?php

/**
 * This model is used for common function used in all over projects.
 * @package    User_Common
 * @author     Icreon Tech - DG
 */

namespace Common\Model;

use Common\Module;
// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This model is used for common function used in all over projects.
 * @package    User_Common
 * @author     Icreon Tech - DG
 */
class Common implements InputFilterAwareInterface {

    protected $inputFilter;
    protected $adapter;
    public $security_question;
    public $security_question_id;

    public function __construct(Adapter $adapter) {

        $this->adapter = $adapter;
    }

    public function exchangeArray($data) {
        $this->security_question = (isset($data['security_question'])) ? $data['security_question'] : null;
        $this->security_question_id = (isset($data['security_question_id'])) ? $data['security_question_id'] : null;
    }

    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        
    }

    // Add the following method:
    public function getArrayCopy() {
        return get_object_vars($this);
    }

    /**
     * This method is used to convert form post array of create case to Object of class type Cases
     * @param code
     * @return Void
     * @author Icreon Tech - AG
     */
    public function getCompaignDataArr($data = array()) {
        $returnArr = array();
        $returnArr[] = (isset($data['campaign_name']) && !empty($data['campaign_name'])) ? $data['campaign_name'] : '';
	$returnArr[] = (isset($data['campaign_code']) && !empty($data['campaign_code'])) ? $data['campaign_code'] : '';
        return $returnArr;
    }

      /**
     * This method is used to get all contacts for autosuggest
     * @param code
     * @return Void
     * @author Icreon Tech - Ns
     */
    public function getContactArrAutoSuggest($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['user_email']) and trim($dataArr['user_email']) != '') ? $dataArr['user_email'] : ''; //Not match with email id
        $returnArr[] = (isset($dataArr['user_email_with_name']) and trim($dataArr['user_email_with_name']) != '') ? $dataArr['user_email_with_name'] : ''; //also match with email id
        $returnArr[] = (isset($dataArr['sort_field']) and trim($dataArr['sort_field']) != '') ? $dataArr['sort_field'] : 'users.first_name';
        $returnArr[] = (isset($dataArr['sort_order']) and trim($dataArr['sort_order']) != '') ? $dataArr['sort_order'] : 'asc';
        $returnArr[] = (isset($dataArr['name_email']) and trim($dataArr['name_email']) != '') ? $dataArr['name_email'] : '';
		$returnArr[] = (isset($dataArr['contact_id']) and trim($dataArr['contact_id']) != '') ? $dataArr['contact_id'] : '';
        return $returnArr;
    }
    
    /**
     * This method is used to get all contacts for autosuggest
     * @param code
     * @return Void
     * @author Icreon Tech - DT
     */
    public function getContactArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['user_email']) && $dataArr['user_email'] != '') ? $dataArr['user_email'] : ''; //Not match with email id
        $returnArr[] = (isset($dataArr['user_email_with_name']) && $dataArr['user_email_with_name'] != '') ? $dataArr['user_email_with_name'] : ''; //also match with email id
        $returnArr[] = (isset($dataArr['transaction']) && $dataArr['transaction'] != '') ? $dataArr['transaction'] : '';
        $returnArr[] = (isset($dataArr['phone']) && $dataArr['phone'] != '') ? $dataArr['phone'] : '';
        $returnArr[] = (isset($dataArr['association']) && $dataArr['association'] != '') ? $dataArr['association'] : '';
        $returnArr[] = (isset($dataArr['last_transaction']) && $dataArr['last_transaction'] != '') ? $dataArr['last_transaction'] : '';
        $returnArr[] = (isset($dataArr['afihc']) && $dataArr['afihc'] != '') ? $dataArr['afihc'] : '';
        $returnArr[] = (isset($dataArr['source']) && $dataArr['source'] != '') ? $dataArr['source'] : '';
        $returnArr[] = (isset($dataArr['membership_package_shipped']) && $dataArr['membership_package_shipped'] != '') ? $dataArr['membership_package_shipped'] : '';
        $returnArr[] = (isset($dataArr['groups']) && $dataArr['groups'] != '') ? $dataArr['groups'] : '';
        $returnArr[] = (isset($dataArr['gender']) && $dataArr['gender'] != '') ? $dataArr['gender'] : '';
        $returnArr[] = (isset($dataArr['state']) && $dataArr['state'] != '') ? $dataArr['state'] : '';
        $returnArr[] = (isset($dataArr['from_date']) && $dataArr['from_date'] != '') ? $dataArr['from_date'] : '';
        $returnArr[] = (isset($dataArr['to_date']) && $dataArr['to_date'] != '') ? $dataArr['to_date'] : '';
        $returnArr[] = (isset($dataArr['membership_id']) && $dataArr['membership_id'] != '') ? $dataArr['membership_id'] : '';
        $returnArr[] = (isset($dataArr['type']) && $dataArr['type'] != '') ? $dataArr['type'] : '';
        $returnArr[] = (isset($dataArr['company_id']) && $dataArr['company_id'] != '') ? $dataArr['company_id'] : '';
        $returnArr[] = (isset($dataArr['interests']) && $dataArr['interests'] != '') ? $dataArr['interests'] : '';
        $returnArr[] = (isset($dataArr['zip']) && $dataArr['zip'] != '') ? $dataArr['zip'] : '';
        $returnArr[] = (isset($dataArr['country_id']) && $dataArr['country_id'] != '') ? $dataArr['country_id'] : '';
        $returnArr[] = (isset($dataArr['membership_year']) && $dataArr['membership_year'] != '') ? $dataArr['membership_year'] : '';
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : 0;
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : 10;
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : 'first_name';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : 'asc';
        return $returnArr;
    }

    /**
     * This method is used to get all company  for autosuggest
     * @param code
     * @return Void
     * @author Icreon Tech - DG
     */
    public function getCompanyArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['company_name']) && $dataArr['company_name'] != '') ? $dataArr['company_name'] : '';
		$returnArr[] = (isset($dataArr['company_email']) && $dataArr['company_email'] != '') ? $dataArr['company_email'] : '';
		$returnArr[] = (isset($dataArr['company_phone']) && $dataArr['company_phone'] != '') ? $dataArr['company_phone'] : '';
		$returnArr[] = (isset($dataArr['company_description']) && $dataArr['company_description'] != '') ? $dataArr['company_description'] : '';
		$returnArr[] = (isset($dataArr['company_street']) && $dataArr['company_street'] != '') ? $dataArr['company_street'] : '';
		$returnArr[] = (isset($dataArr['company_city']) && $dataArr['company_city'] != '') ? $dataArr['company_city'] : '';
		$returnArr[] = (isset($dataArr['company_state']) && $dataArr['company_state'] != '') ? $dataArr['company_state'] : '';
		$returnArr[] = (isset($dataArr['company_zipcode']) && $dataArr['company_zipcode'] != '') ? $dataArr['company_zipcode'] : '';
		$returnArr[] = (isset($dataArr['company_country_id']) && $dataArr['company_country_id'] != '') ? $dataArr['company_country_id'] : '';		
		$returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : DATE_TIME_FORMAT;			
		$returnArr[] = (isset($dataArr['modified_date']) && $dataArr['modified_date'] != '') ? $dataArr['modified_date'] : DATE_TIME_FORMAT;
        return $returnArr;
    }
    
    /**
     * This method is used to get all membership afihc  for autosuggest
     * @param code
     * @return Void
     * @author Icreon Tech - DT
     */
    public function getMembershipAfihcArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['membership_afihc']) && $dataArr['membership_afihc'] != '') ? $dataArr['membership_afihc'] : '';
        return $returnArr;
    }
    
    /**
     * This method is used to get all phone lookup array
     * @param code
     * @return Void
     * @author Icreon Tech - DT
     */
    public function getPhoneArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['phone']) && $dataArr['phone'] != '') ? $dataArr['phone'] : '';
        return $returnArr;
    }

    /**
     * This method is used to get all zip code lookup array
     * @param code
     * @return Void
     * @author Icreon Tech - DT
     */
    public function getPostalArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['zip_code']) && $dataArr['zip_code'] != '') ? $dataArr['zip_code'] : '';
        return $returnArr;
    }
     /**
     * This method is used to get all state lookup array
     * @param code
     * @return Void
     * @author Icreon Tech - DT
     */
    public function getStateArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['state']) && $dataArr['state'] != '') ? $dataArr['state'] : '';
        return $returnArr;
    }

    /**
     * This method is used to get all country lookup array
     * @param code
     * @return Void
     * @author Icreon Tech - DT
     */
    public function getCountryArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['country']) && $dataArr['country'] != '') ? $dataArr['country'] : '';
        $returnArr[] = (isset($dataArr['country_id']) && $dataArr['country_id'] != '') ? $dataArr['country_id'] : '';
        return $returnArr;
    }


    /**
     * This method is used to get all crm user  for autosuggest
     * @param code
     * @return Void
     * @author Icreon Tech - DG
     */
    public function getCrmUserDataArr($data = array()) {
        $returnArr = array();
        $returnArr[] = (isset($data['crm_full_name']) && !empty($data['crm_full_name'])) ? $data['crm_full_name'] : '';
        $returnArr[] = (isset($data['crm_email_id']) && !empty($data['crm_email_id'])) ? $data['crm_email_id'] : '';
        $returnArr[] = (isset($data['crm_user_id']) && !empty($data['crm_user_id'])) ? $data['crm_user_id'] : '';
        $returnArr[] = (isset($data['crm_user_ids']) && !empty($data['crm_user_ids'])) ? $data['crm_user_ids'] : '';
        
        return $returnArr;
    }
	
	public function getContactAutoSearchArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['contact_id']) and trim($dataArr['contact_id']) != '') ? $dataArr['contact_id'] : '';
        return $returnArr;
    }

}