<?php

/**
 * This model is used for survey
 * @package    Common
 * @author     Icreon Tech - DG
 */

namespace Common\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for survey
 * @package    Common
 * @author     Icreon Tech - DG
 */
class SurveyTable {

    protected $tableGateway;
    protected $connection;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
    }

    /**
     * Function for get Security Question
     * @author Icreon Tech - DG
     * @return array
     */
    public function getSurveyQuestions() {
        $result = $this->connection->execute('CALL usp_com_getSurveyQuestion');
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();

        if (!$resultSet) {
            throw new \Exception("Could not find record.");
        }
        return $resultSet;
    }

}