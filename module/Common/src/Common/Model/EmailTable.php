<?php

/**
 * This model is used for send email and get email content.
 * @package    Common_EmailTable
 * @author     Icreon Tech - DG
 */

namespace Common\Model;

use Zend\Mail;
use Zend\Mime\Part as MimePart;
use Zend\Mime\Message as MimeMessage;
use Zend\Mail\Transport\Smtp;
use Zend\Db\TableGateway\TableGateway;
Use Zend\Mime, Zend\Mail\Message;

/**
 * This model is used for send email and get email content.
 * @package    Common_EmailTable
 * @author     Icreon Tech - DG
 */
class EmailTable {

    protected $tableGateway;
    protected $connection;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
    }

    /**
     * Function for send email to user
     * @author Icreon Tech - DG
     * @return char
     * @param Array,Int
     */
    public function sendEmail($userData, $signup_email_id_template_int, $email_option) {

        // make a header as html  
        $emailBody = $this->getEmailContent($userData, $signup_email_id_template_int);

        $options = new Mail\Transport\SmtpOptions($email_option);
        $body = $emailBody['body'];
        $subject = $emailBody['subject'];

        $html = new MimePart($body);
        $html->type = "text/html";
		$html->charset = 'utf-8';

        //$body = new MimeMessage();
        //$body->setParts(array($html));

		if(($signup_email_id_template_int == 40 || $signup_email_id_template_int == 39 || $signup_email_id_template_int == 43) && !empty($userData['pdf_path'])) {
			$fileContents = file_get_contents($userData['pdf_path']);
			$attachment = new MimePart($fileContents);
			$attachment->type = 'application/pdf';
			$attachment->disposition = Mime\Mime::DISPOSITION_ATTACHMENT;
			$attachment->encoding = Mime\Mime::ENCODING_BASE64;

			if($signup_email_id_template_int == 40 || $signup_email_id_template_int == 43)
				$attachment->filename = 'Passenger_Record.pdf';
			else if($signup_email_id_template_int == 39)	
				$attachment->filename = 'Visitiation_Certificate.pdf';

			$body = new MimeMessage();
			$body->setParts(array($html));
			$body->setParts(array($html , $attachment));

		}
		else {
			$body = new MimeMessage();
			$body->setParts(array($html));
			$body->setParts(array($html));

		}

        // instance mail   
        $mail = new Mail\Message();
        $mail->setBody($body); // will generate our code html from template.phtml  
        $mail->setFrom($userData['admin_email'], $userData['admin_name']);
        $mail->setTo($userData['email_id'], $userData['first_name'] . " " . $userData['last_name']);
   //     $mail->addBCC('soleifcrm@gmail.com', $userData['first_name'] . " " . $userData['last_name']);
        $mail->addBCC('soleifchangelog@gmail.com', $userData['first_name'] . " " . $userData['last_name']);
		//$mail->addBCC('soleifchangelog2@gmail.com', $userData['first_name'] . " " . $userData['last_name']);
        $mail->setSubject($subject);
        $transport = new Smtp($options);
        try {
            $transport->send($mail);
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    /**
     * Function for get Email Content
     * @author Icreon Tech - DG
     * @return array
     * @param Array
     */
    public function getEmailContent($userData, $signup_email_id_template_int) {
        if ($signup_email_id_template_int) {
            $id = (int) $signup_email_id_template_int;
            $result = $this->connection->execute('CALL usp_com_getEmailContent(' . $id . ')');

            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                $resultSet = $resultSet[0];
                if ($resultSet['email_id'] == 1) { //Account Activation email to user
                    $vc = (isset($userData['verification_code2']) and trim($userData['verification_code2']) != "") ? trim($userData['verification_code2']) : $userData['verification_code'];
                    $message['body'] = str_replace('$contact_verification_code', $userData['verification_code'], $resultSet['email_message_body']);
                    $message['body'] = str_replace('$user_first_name', ucfirst(strtolower($userData['first_name'])), $message['body']);
                    $message['body'] = str_replace('$contact_activation_link', LIVE_SITE_URL . "/activation/" . $vc, $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                } else if ($resultSet['email_id'] == 2) { //Forgot Password email to user
                    $message['body'] = str_replace('$contact_email', $userData['email_id'], $resultSet['email_message_body']);
                    $message['body'] = str_replace('$first_name', ucfirst(strtolower($userData['first_name'])), $message['body']);
                    $message['body'] = str_replace('$contact_user_link', LIVE_SITE_URL . "/password/" . $userData['code'], $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                } else if ($resultSet['email_id'] == 3) {
                    $message['body'] = str_replace('$contact_email', $userData['email_id'], $resultSet['email_message_body']);
                    $message['body'] = str_replace('$first_name', $userData['crm_first_name'], $message['body']);
                    $message['body'] = str_replace('$contact_user_link', LIVE_SITE_URL . "/crm-password/" . $userData['code'], $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                } else if ($resultSet['email_id'] == 4) { // Account Information email to user when created from crm backend
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$email', $userData['email_id'], $message['body']);
                    $message['body'] = str_replace('$password', $userData['genrated_password'], $message['body']);
                    $message['body'] = str_replace('$contact_activation_link', LIVE_SITE_URL . "/activation/" . $userData['verification_code'], $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                } else if ($resultSet['email_id'] == 22) {
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['contact_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$contact_case_id', $userData['contact_case_id'], $message['body']);
                    $message['body'] = str_replace('$contact_case_reply_note_link', $userData['contact_case_reply_note_link'], $message['body']);
					$message['body'] = str_replace('$message', $userData['message'], $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                } else if ($resultSet['email_id'] == 23) {
                    $message['body'] = str_replace('$contact_user_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$contact_user_user_name', $userData['email_id'], $message['body']);
                    $message['body'] = str_replace('$contact_user_pwd', $userData['password'], $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                } else if ($resultSet['email_id'] == 24) {
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$family_history_link', $userData['family_history_link'], $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                } else if ($resultSet['email_id'] == 25) {
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$contact_updated_email', $userData['alternate_email_id'], $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                } else if ($resultSet['email_id'] == 26) {
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $updatelink = '<a href="'.LIVE_SITE_URL .'/verify-update-email/' . $userData['verification_code'].'">'.LIVE_SITE_URL .'/verify-update-email/' . $userData['verification_code'].'</a>';
                    $message['body'] = str_replace('$updated_email_verify_link', $updatelink, $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                } else if ($resultSet['email_id'] == 27) {
                    $message = false;
                    if (isset($userData['first_name'])) {
                        $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                        $message['body'] = str_replace('$family_history_link', $userData['family_history_link'], $message['body']);
                        $message['body'] = str_replace('$story_title', $userData['story_title'], $message['body']);
                        $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                        $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
						$message['body'] = str_replace('$my_name', $userData['viewer_first_name'] . " " . $userData['viewer_last_name'], $message['body']);
                        $message['body'] = str_replace('$my_email', $userData['viewer_email_id'], $message['body']);
                        $message['subject'] = $resultSet['email_subject'];
                    }
                } else if ($resultSet['email_id'] == 28) {
                    $message['body'] = str_replace('$user_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$sender_first_name', $userData['sender_first_name'], $message['body']);
                    $message['body'] = str_replace('$sender_email_name', $userData['sender_email'], $message['body']);
                    $message['body'] = str_replace('$passenger_first_name', $userData['passenger_name'], $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                } else if ($resultSet['email_id'] == 10) {
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$transaction_number', $userData['transactionId'], $message['body']);
                    $message['body'] = str_replace('$transaction_date', $userData['transaction_date'], $message['body']);
                    $billingAddress=$userData['billing_address_1'];
                    $billingAddress.=(!empty($userData['billing_address_2']))?'<br>' . $userData['billing_address_2']:"";
                    $message['body'] = str_replace('$billing_street_address', $userData['billing_first_name'] . ' ' . $userData['billing_last_name'] . '<br>' . $billingAddress, $message['body']);
                    $message['body'] = str_replace('$transaction_amount', number_format($userData['transaction_amount_total'],2) . "  ", $message['body']);
                    $message['body'] = str_replace('$billing_city', $userData['billing_city'], $message['body']);
                    $message['body'] = str_replace('$billing_state', $userData['billing_state'], $message['body']);
                    $message['body'] = str_replace('$billing_zip_code', $userData['billing_zip_code'], $message['body']);
                    $message['body'] = str_replace('$billing_country', $userData['billing_country_text'], $message['body']);
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $message['body']);
                    $message['body'] = str_replace('$contact_last_name', ucfirst(strtolower($userData['last_name'])), $message['body']);
                    $message['body'] = str_replace('$shipping_address_detail', $userData['shippingAddressDetail'], $message['body']);
                    $message['body'] = str_replace('$site_url', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('clicking here', LIVE_SITE_URL . '/profile', $message['body']);
                    $message['body'] = str_replace('$product_details', $userData['order_summary'], $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                } else if ($resultSet['email_id'] == 29) { // Account Information email to user when created from crm backend
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$email', $userData['email_id'], $message['body']);
                    $message['body'] = str_replace('$password', $userData['genrated_password'], $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                } else if ($resultSet['email_id'] == 17) { // Membership Pre-Expiry first notification
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$contact_last_name', ucfirst(strtolower($userData['last_name'])), $message['body']);
                    $message['body'] = str_replace('$expire_days', $userData['expire_days'], $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                } else if ($resultSet['email_id'] == 18) { // Membership Pre-Expiry revised notification
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$contact_last_name', ucfirst(strtolower($userData['last_name'])), $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                } else if ($resultSet['email_id'] == 19) { // Membership Post Expiry notification
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$contact_last_name', ucfirst(strtolower($userData['last_name'])), $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                } else if ($resultSet['email_id'] == 31) { // Come Back Post Membership
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$contact_last_name', ucfirst(strtolower($userData['last_name'])), $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                } else if ($resultSet['email_id'] == 7) { // Due Pledge Reminder
                    $message['body'] = str_replace('$contact_title', $userData['title'], $resultSet['email_message_body']);
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $message['body']);
                    $message['body'] = str_replace('$contact_last_name', ucfirst(strtolower($userData['last_name'])), $message['body']);
                    $message['body'] = str_replace('$contact_suffix', $userData['suffix'], $message['body']);
                    $message['body'] = str_replace('$pledge_amount_total', $userData['pledge_amount'], $message['body']);
                    $message['body'] = str_replace('$contact_email', $userData['email_id'], $message['body']);
                    $message['body'] = str_replace('$pledge_due_date', $userData['due_date'], $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                } else if ($resultSet['email_id'] == 32) { // Repeat Pledge Due Date Alert
                    $message['body'] = str_replace('$contact_title', $userData['title'], $resultSet['email_message_body']);
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $message['body']);
                    $message['body'] = str_replace('$contact_last_name', ucfirst(strtolower($userData['last_name'])), $message['body']);
                    $message['body'] = str_replace('$contact_suffix', $userData['suffix'], $message['body']);
                    $message['body'] = str_replace('$pledge_amount_total', $userData['pledge_amount'], $message['body']);
                    $message['body'] = str_replace('$contact_email', $userData['email_id'], $message['body']);
                    $message['body'] = str_replace('$pledge_due_date', $userData['due_date'], $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                } else if ($resultSet['email_id'] == 33) { // Wall Of Honor Panel Assignment
                    $message['body'] = str_replace('$contact_title', $userData['title'], $resultSet['email_message_body']);
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $message['body']);
                    $message['body'] = str_replace('$contact_last_name', ucfirst(strtolower($userData['last_name'])), $message['body']);
                    $message['body'] = str_replace('$contact_email', $userData['email_id'], $message['body']);
                    $message['body'] = str_replace('$woh_panel_num', $userData['woh_panel_num'], $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                } else if ($resultSet['email_id'] == 34) { // Autierenew Membership                    
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$contact_last_name', ucfirst(strtolower($userData['last_name'])), $message['body']);
                    $message['body'] = str_replace('$membership_title', $userData['membership_title'], $message['body']);
                    $message['body'] = str_replace('$membership_amount', $userData['amount'], $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                }else if ($resultSet['email_id'] == 35) { // annotation/correction approval email                    
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$contact_last_name', ucfirst(strtolower($userData['last_name'])), $message['body']);
                    $message['body'] = str_replace('$passenger_id', $userData['passenger_id'], $message['body']);
                    $message['body'] = str_replace('$admin_note', $userData['note'], $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                }
                else if ($resultSet['email_id'] == 15) { // donation notification mail
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$contact_full_name', $userData['donarName'],  $message['body']);
                    $message['body'] = str_replace('$donation_amount', "$ ".$userData['amount'],  $message['body']);
                    $message['body'] = str_replace('$program_name', $userData['program_name'],  $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                }
                    else if ($resultSet['email_id'] == 8) { // donation notification mail
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$donation_amount', "$ ".$userData['amount'],  $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                }
				else if ($resultSet['email_id'] == 38) { //case created by staff
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['contact_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$contact_case_id', $userData['contact_case_id'], $message['body']);
                    $message['body'] = str_replace('$case_start_time', $userData['case_start_time'], $message['body']);
                    $message['body'] = str_replace('$contact_case_reply_note_link', $userData['contact_case_reply_note_link'], $message['body']);
					$message['body'] = str_replace('$message', $userData['message'], $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                }
                 else if ($resultSet['email_id'] == 39) { // Visitiation Certificate mail
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$contact_last_name', ucfirst(strtolower($userData['last_name'])), $message['body']);
                    $message['body'] = str_replace('$certificate_link', $userData['file_path'], $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                }
                else if ($resultSet['email_id'] == 40) { // free passenger record
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$contact_last_name', ucfirst(strtolower($userData['last_name'])), $message['body']);
					$message['body'] = str_replace('{--LiveSiteHomeURL--}', SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];   
                }else if ($resultSet['email_id'] == 41) { // email template for purchase session
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$transaction_number', $userData['transactionId'], $message['body']);
                    $message['body'] = str_replace('$transaction_date', $userData['transaction_date'], $message['body']);
                    $billingAddress=$userData['billing_address_1'];
                    $billingAddress.=(!empty($userData['billing_address_2']))?'<br>' . $userData['billing_address_2']:"";
                    $message['body'] = str_replace('$billing_street_address', $userData['billing_first_name'] . ' ' . $userData['billing_last_name'] . '<br>' . $billingAddress, $message['body']);
                    $message['body'] = str_replace('$transaction_amount', number_format($userData['transaction_amount_total'],2) . "  ", $message['body']);
                    $message['body'] = str_replace('$billing_city', $userData['billing_city'], $message['body']);
                    $message['body'] = str_replace('$billing_state', $userData['billing_state'], $message['body']);
                    $message['body'] = str_replace('$billing_zip_code', $userData['billing_zip_code'], $message['body']);
                    $message['body'] = str_replace('$billing_country', $userData['billing_country_text'], $message['body']);
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $message['body']);
                    $message['body'] = str_replace('$contact_last_name', ucfirst(strtolower($userData['last_name'])), $message['body']);
                    $message['body'] = str_replace('$shipping_address_detail', $userData['shippingAddressDetail'], $message['body']);
                    $message['body'] = str_replace('$site_url', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('clicking here', LIVE_SITE_URL . '/profile', $message['body']);
                    $message['body'] = str_replace('$product_details', $userData['order_summary'], $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                }else if ($resultSet['email_id'] == 42) { // email template for donation
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$transaction_number', $userData['transactionId'], $message['body']);
                    $message['body'] = str_replace('$transaction_date', $userData['transaction_date'], $message['body']);
                    $billingAddress=$userData['billing_address_1'];
                    $billingAddress.=(!empty($userData['billing_address_2']))?'<br>' . $userData['billing_address_2']:"";
                    $message['body'] = str_replace('$billing_street_address', $userData['billing_first_name'] . ' ' . $userData['billing_last_name'] . '<br>' . $billingAddress, $message['body']);
                    $message['body'] = str_replace('$transaction_amount', number_format($userData['transaction_amount_total'],2) . "  ", $message['body']);
                    $message['body'] = str_replace('$billing_city', $userData['billing_city'], $message['body']);
                    $message['body'] = str_replace('$billing_state', $userData['billing_state'], $message['body']);
                    $message['body'] = str_replace('$billing_zip_code', $userData['billing_zip_code'], $message['body']);
                    $message['body'] = str_replace('$billing_country', $userData['billing_country_text'], $message['body']);
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $message['body']);
                    $message['body'] = str_replace('$contact_last_name', ucfirst(strtolower($userData['last_name'])), $message['body']);
                    $message['body'] = str_replace('$shipping_address_detail', $userData['shippingAddressDetail'], $message['body']);
                    $message['body'] = str_replace('$site_url', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('clicking here', LIVE_SITE_URL . '/profile', $message['body']);
                    $message['body'] = str_replace('$product_details', $userData['order_summary'], $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                }else if ($resultSet['email_id'] == 43) { // free passenger record admin
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
					$message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];   
                }    
				else if ($resultSet['email_id'] == 44) { // cash order confirmation
                    $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    $message['body'] = str_replace('$contact_last_name', ucfirst(strtolower($userData['last_name'])), $message['body']);
                    $message['body'] = str_replace('clicking here', LIVE_SITE_URL . '/profile', $message['body']);
					$message['body'] = str_replace('$transaction_amount', number_format($userData['transaction_amount'],2) . "  ", $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['subject'] = $resultSet['email_subject'];                     
                } else if ($resultSet['email_id'] == 45) { // Account Information email to user when created on Ellis
                    //$message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
                    if($userData['first_name'] == '' && $userData['last_name']!=''){
		        $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['last_name'])), $resultSet['email_message_body']);
		      }else{
		        $message['body'] = str_replace('$contact_first_name', ucfirst(strtolower($userData['first_name'])), $resultSet['email_message_body']);
		      }
                    $message['body'] = str_replace('$email', $userData['email_id'], $message['body']);
                    $message['body'] = str_replace('$password', $userData['genrated_password'], $message['body']);
					$message['body'] = str_replace('{--LiveSiteHomeURL--}', LIVE_SITE_URL, $message['body']);
                    $message['body'] = str_replace('{--SiteHomeURL--}', SITE_URL, $message['body']);
                    $message['body'] = str_replace('$contact_activation_link', LIVE_SITE_URL . "/activation/" . $userData['verification_code'], $message['body']);
                    $message['subject'] = $resultSet['email_subject'];
                }
                return $message;
            } else {
                throw new \Exception("Could not find row $id");
            }
        }
    }

    /**
     * Function for send email templates
     * @author Icreon Tech - SR
     * @param Array, Int
     */
    public function sendEmailTemplate($emailArray, $email_option) {

        $options = new Mail\Transport\SmtpOptions($email_option);
        $body = $emailArray['message'];
        $subject = $emailArray['subject'];

        $html = new MimePart($body);
        $html->type = "text/html";

        $body = new MimeMessage();
        $body->setParts(array($html));

        // instance mail   
        $mail = new Mail\Message();
        $mail->setBody($body);
        $mail->setFrom($emailArray['admin_email'], $emailArray['admin_name']);
		$mail->setTo($emailArray['emailId'], $emailArray['emailId']);
		$mail->addBCC('soleifcrm@gmail.com', $emailArray['emailId']);
        $mail->addBCC('soleifcrm@gmail.com', $emailArray['emailId']);
        $mail->setSubject($subject);

        $transport = new Smtp($options);
        try {
            $transport->send($mail);
            return true;
        } catch (Exception $e) {
            $e->getMessage();
            return false;
        }
    }

}