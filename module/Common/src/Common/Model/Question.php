<?php

/**
 * This model is used for survey
 * @package    User_User
 * @author     Icreon Tech - DG
 */

namespace Common\Model;

use Common\Module;
// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This model is used for survey
 * @package    User_User
 * @author     Icreon Tech - DG
 */
class Question implements InputFilterAwareInterface {

    protected $inputFilter;
    protected $adapter;
    public $security_question;
    public $security_question_id;

    public function __construct(Adapter $adapter) {

        $this->adapter = $adapter;
    }

    public function exchangeArray($data) {
        $this->security_question = (isset($data['security_question'])) ? $data['security_question'] : null;
        $this->security_question_id = (isset($data['security_question_id'])) ? $data['security_question_id'] : null;
    }

    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        
    }

    // Add the following method:
    public function getArrayCopy() {
        return get_object_vars($this);
    }

}