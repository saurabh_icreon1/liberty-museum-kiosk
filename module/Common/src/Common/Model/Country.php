<?php

/**
 * This model is used for country
 * @package    Common/Country
 * @author     Icreon Tech - DG
 */

namespace Common\Model;

use Common\Module;
// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This model is used for country
 * @package    Common/Country
 * @author     Icreon Tech - DG
 */
class Country implements InputFilterAwareInterface {

    protected $inputFilter;
    protected $adapter;
    public $name;
    public $country_id;
    public $is_active;
    public $added_date;

    public function __construct(Adapter $adapter) {

        $this->adapter = $adapter;
    }

    public function exchangeArray($data) {
        $this->name = (isset($data['name'])) ? $data['name'] : null;
        $this->country_id = (isset($data['country_id'])) ? $data['country_id'] : null;
        $this->is_active = (isset($data['is_active'])) ? $data['is_active'] : null;
        $this->added_date = (isset($data['added_date'])) ? $data['added_date'] : null;
    }

    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        
    }

    // Add the following method:
    public function getArrayCopy() {
        return get_object_vars($this);
    }

}