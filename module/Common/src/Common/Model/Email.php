<?php

/**
 * This model is used for email.
 * @package    Common_Email
 * @author     Icreon Tech - DG
 */

namespace Common\Model;

use Common\Module;
// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This model is used for email.
 * @package    Common_Email
 * @author     Icreon Tech - DG
 */
class Email implements InputFilterAwareInterface {

    protected $inputFilter;
    protected $adapter;
    public $email_id;
    public $email_title;
    public $email_subject;
    public $email_message_body;

    public function __construct(Adapter $adapter) {

        $this->adapter = $adapter;
    }

    public function exchangeArray($data) {
        $this->email_id = (isset($data['email_id'])) ? $data['email_id'] : null;
        $this->email_title = (isset($data['email_title'])) ? $data['email_title'] : null;
        $this->email_subject = (isset($data['email_subject'])) ? $data['email_subject'] : null;
        $this->email_message_body = (isset($data['email_message_body'])) ? $data['email_message_body'] : null;
    }

    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        
    }

    // Add the following method:
    public function getArrayCopy() {
        return get_object_vars($this);
    }

}