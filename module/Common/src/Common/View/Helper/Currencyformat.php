<?php
namespace common\View\Helper;
use Zend\View\Helper\AbstractHelper,
    Zend\ServiceManager\ServiceLocatorAwareInterface,
    Zend\ServiceManager\ServiceLocatorInterface,
    Zend\View\Exception;
 
class Currencyformat extends AbstractHelper
{
    public function __invoke($number)
    {   
        return  number_format($number, 2, '.','');
    }
}