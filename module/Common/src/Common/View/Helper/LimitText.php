<?php
/**
 * This page is used for display time on view pages
 * @package    Common_View_Helper_CommonController
 * @author     Icreon Tech - DT
 */
namespace common\View\Helper;

use Zend\View\Helper\AbstractHelper,
 Zend\ServiceManager\ServiceLocatorAwareInterface,
 Zend\ServiceManager\ServiceLocatorInterface,
 Zend\View\Exception;

/**
 * This class is used for display date format with time on view pages
 * @package    Common_View_Helper_CommonController
 * @author     Icreon Tech - DT
 */
class LimitText extends AbstractHelper {

   public function __invoke($string , $length , $append = '...')
    {
        if (strlen($string) <= $length) return $string;

        $append = ( strlen($append) ? sprintf(' %s ' , $append) : ' ... ' );

        $subString = substr($string , 0,$length+1);
      //echo strrpos($subString , ' ',-1);
        if(substr($subString , -1) ==' ')
        {
            $finalString =  substr($string ,0, $length);
        }
        else
        {
           $finalString = substr($string , 0 , ( strrpos($subString , ' ',-1)));
        }
        $finalString.=$append;
        return $finalString;
    }

}