<?php
namespace common\View\Helper;
use Zend\View\Helper\AbstractHelper,
    Zend\ServiceManager\ServiceLocatorAwareInterface,
    Zend\ServiceManager\ServiceLocatorInterface,
    Zend\View\Exception;
 
class Roundamount extends AbstractHelper
{
    public function __invoke($number)
    {   
        return  round($number, 2);
    }
}