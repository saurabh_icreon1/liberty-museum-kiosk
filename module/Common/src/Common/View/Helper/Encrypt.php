<?php

/**
 * This helper is used for encrypt the string
 * @category   Zend
 * @package    Helper_EncryptHelper
 * @version    2.2
 * @author     Icreon Tech - KK
 */

namespace Common\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Authentication\AuthenticationService;

class Encrypt extends AbstractHelper {

    public function __invoke($id) {
        return $encryptedData = base64_encode(serialize($id));
       
    }

}