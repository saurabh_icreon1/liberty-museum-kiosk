<?php

/**
 * This page is used for display date format with time on view pages
 * @package    Common_View_Helper_CommonController
 * @author     Icreon Tech - DT
 */

namespace common\View\Helper;

use Zend\View\Helper\AbstractHelper,
    Zend\ServiceManager\ServiceLocatorAwareInterface,
    Zend\ServiceManager\ServiceLocatorInterface,
    Zend\View\Exception;
use Zend\Session\Container as SessionContainer;

/**
 * This class is used for display date format with time on view pages
 * @package    Common_View_Helper_CommonController
 * @author     Icreon Tech - DT
 */
class Dateformatwithtime extends AbstractHelper {

    /**
     * @var Default timezone
     */
    protected $timezone;

    /**
     * @var Default datetime format
     */
    protected $format;

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function getTimezone() {
        if ($this->timezone) {
            return $this->timezone;
        }

        /* $config = $this->serviceLocator->getServiceLocator()->get('Configuration');
          if(isset($config['datetime']['timezone'])){
          return $this->timezone = $config['datetime']['timezone'];
          } */
        return $this->timezone = 0;
    }

    public function setTimezone($timezone) {
        $this->timezone = (int) $timezone;
    }

    public function setFormat($format) {
        $this->format = $format;
        return $this;
    }

    public function getFormat() {
        if ($this->format) {
            return $this->format;
        }
        /*
          $config = $this->serviceLocator->getServiceLocator()->get('Configuration');
          if(isset($config['datetime']['format'])){
          return $this->format = $config['datetime']['format'];
          } */
        return $this->timezone = 'm-d-Y H:i';
    }

    /**
     * Set the service locator.
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return AbstractHelper
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    /**
     * Get the service locator.
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function __construct() {
        date_default_timezone_set('UTC');
    }

    public function __invoke($time = '', $timezone = null, $format = '') {

        $this->timeZoneSession = new SessionContainer('timeZone');
        if ($this->timeZoneSession->timeZone != '') {
            $time = date("Y-m-d H:i:s", strtotime($time) + $this->timeZoneSession->timeZone * 60);
        }

        if (0 == func_num_args()) {
            return $this;
        }

        $timezone = $timezone !== null ? $timezone : $this->getTimezone();
        $format = $format ? $format : $this->getFormat();

        $time = $time ? $time : time();
        $time = is_numeric($time) ? $time : strtotime($time);
        $time = $time + $timezone * 3600;

        return date($format, $time);
    }

    /**
     * Tranform input time to timeStamp
     *
     * @param string $time
     *
     * @access public
     *
     * @return Int  timeStamp
     */
    public function timeStamp($time = '') {
        return $time ? strtotime($time) : time();
    }

    /**
     * Tranform input time to time string which can be parsed by javascript
     *
     * @param string $time
     *
     * @access public
     *
     * @return string javascript parse-able string
     */
    public function jsTime($time = '', $timezone = null) {
        if (!$time) {
            return;
        }

        $timezone = $timezone ? $timezone : $this->getTimezone();

        $time = $time ? strtotime($time) : time();
        $time = $time + $timezone * 3600;
        $prefix = $timezone < 0 ? '-' : '+';

        $zone = str_pad(str_pad(abs($timezone), 2, 0, STR_PAD_LEFT), 4, 0);
        return date('D M j H:i:s', $time) . ' UTC' . $prefix . $zone . ' ' . date('Y', $time);
    }

    /**
     * Tranform input time to iso time
     *
     * @param string $time
     * @param int $timezone
     *
     * @access public
     *
     * @return string time string
     */
    public function isoTime($time = null, $timezone = null) {
        $timezone = $timezone ? $timezone : $this->getTimezone();
        $time = $time ? strtotime($time) : time();
        return $time = date('c', $time);
    }

}