<?php

/**
 * This page is used for display manifest image path on view pages
 * @package    Common_View_Helper_CommonController
 * @author     Icreon Tech - SR
 */

namespace common\View\Helper;

use Zend\View\Helper\AbstractHelper,
    Zend\ServiceManager\ServiceLocatorAwareInterface,
    Zend\ServiceManager\ServiceLocatorInterface,
    Zend\View\Exception;

/**
 * This class is used for display manifest image path on view pages
 * @package    Common_View_Helper_CommonController
 * @author     Icreon Tech - DT
 */
class ConvertTifToJpg extends AbstractHelper implements ServiceLocatorAwareInterface {

    protected $message = null;

    /**
     * Set the service locator. 
     * 
     * @param ServiceLocatorInterface $serviceLocator 
     * @return CustomHelper 
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    /**
     * Get the service locator. 
     * 
     * @return \Zend\ServiceManager\ServiceLocatorInterface 
     */
    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function __invoke($fileName) {
                    $fileArray = explode('.', $fileName);
                    $fileExt = $fileArray[1];
                    if (strtolower($fileExt) == 'tif') {
                        $fileName = substr($fileName,0,-4);
                        $fileName = $fileName.'.jpg';    
                    }
                    else {
                        $fileName = $fileName;
                    }
				return $fileName;
               
    }

}