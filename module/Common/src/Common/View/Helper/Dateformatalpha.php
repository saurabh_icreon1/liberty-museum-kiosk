<?php

/**
 * get date format
 * @param date, and date format
 * @return string date format
 * @author Icreon Tech - SR
 */

namespace Common\View\Helper;

use Zend\View\Helper\AbstractHelper,
    Zend\ServiceManager\ServiceLocatorAwareInterface,
    Zend\ServiceManager\ServiceLocatorInterface,
    Zend\View\Exception;
use Zend\Session\Container as SessionContainer;

class Dateformatalpha extends AbstractHelper {
    protected $timezone;

    public function __invoke($date, $format = '') {
        if ($date == '' || $date == '0000:00:00' || $date == '0000:00:00 00:00:00') {
            return '';
        } else {
//            $this->timeZoneSession = new SessionContainer('timeZone');
//            if($this->timeZoneSession->timeZone!=''){
//                $date = date("Y-m-d H:i:s",strtotime($date)+$this->timeZoneSession->timeZone*60);
//            }

            if (strtolower($format) == 'db_datetime_format') {
                $dateArr = array();
                if ($date != '') {
                    $dateArr = explode('/', $date);
                    $date = $dateArr[2] . '-' . $dateArr[0] . '-' . $dateArr[1]; // "Y-m-d H:i:s", strtotime($date));
                }
            } else if (strtolower($format) == 'db_date_format') {
                $dateArr = array();
                if ($date != '') {
                    $dateArr = explode('/', $date);
                    $date = $dateArr[2] . '-' . $dateArr[0] . '-' . $dateArr[1]; // "Y-m-d H:i:s", strtotime($date));
                }
            } else if (strtolower($format) == 'calender') {
                $dateArr = array();
                if ($date != '') {
                    $dateArr = explode(' ', $date);
                    $dateArr = explode('-', $dateArr[0]);
					if(isset($dateArr[1]))
                        $date = $dateArr[1] . '/' . $dateArr[2] . '/' . $dateArr[0];
                }
            } else if (strtolower($format) == 'displaymonthformat') {
                if (!empty($date)) {
                    if (strpos($date, '-') != false) {
                        $dateDataArray = explode('-', substr($date, 0, 10));
                        $date = date("F jS", mktime(0, 0, 0, $dateDataArray[1],$dateDataArray[2])) .  ', ' . $dateDataArray[0];
                    }
                }
            } else if (strtolower($format) == 'displaymonthformat2') {
                if (!empty($date)) {
                    if (strpos($date, '-') != false) {
                        $dateDataArray = explode('-', substr($date, 0, 10));
                        $date = date("F j", mktime(0, 0, 0, $dateDataArray[1],$dateDataArray[2])) .  ', ' . $dateDataArray[0];
                    }
                }
            }else if (strtolower($format) == 'displayformattime') {
                if (!empty($date))
                    $date = date("m-d-Y H:i", strtotime($date));
            }
            else if (strtolower($format) == 'displayformatwithtime') {
                $this->timeZoneSession = new SessionContainer('timeZone');
                if ($this->timeZoneSession->timeZone != '') {
                    $date = date("m-d-Y H:i", strtotime($date) + $this->timeZoneSession->timeZone * 60);
                } else {
                    $date = date("m-d-Y H:i", strtotime($date));
                }
            } else if (strtolower($format) == 'displaymothdateyear') {
                $this->timeZoneSession = new SessionContainer('timeZone');
                if ($this->timeZoneSession->timeZone != '') {
                    $date = date("m-d-Y", strtotime($date) + $this->timeZoneSession->timeZone * 60);
                } else {
                    $date = date("m-d-Y", strtotime($date));
                }
            } else if (strtolower($format) == 'dateformatampm') {
                $dateArr = array();
                $this->timeZoneSession = new SessionContainer('timeZone');
                if ($date != '') {
                    if ($this->timeZoneSession->timeZone != '') {
                        $date = date("Y-m-d h:i A", strtotime($date) + ($this->timeZoneSession->timeZone * 60));
                    }
                }
            } else if(strtolower($format) == 'displaymonthformattime'){
                
                if (!empty($date)) {
                    if (strpos($date, '-') != false) {
                        $dateDataArray = explode('-', substr($date, 0, 10));
                        $dateDataTime = explode(' ', $date);
                        $date = date("F jS", mktime(0, 0, 0, $dateDataArray[1],$dateDataArray[2])) .  ', ' . $dateDataArray[0]." ".date("h:i A",strtotime($date) + $this->timeZoneSession->timeZone * 60);
                    }
                }
            }
            else if (strtolower($format) == 'slashdateformatampm') {
                $dateArr = array();
                $this->timeZoneSession = new SessionContainer('timeZone');
                if ($date != '') {
                    if ($this->timeZoneSession->timeZone != '') {
                        $date = date("m/d/y", strtotime($date) + ($this->timeZoneSession->timeZone * 60));
                    }
                }
            }
            else if (strtolower($format) == 'slashdateformatampmtime') {
                $dateArr = array();
                $this->timeZoneSession = new SessionContainer('timeZone');
                if ($date != '') {
                    if ($this->timeZoneSession->timeZone != '') {
                        $date = date("h:i A", strtotime($date) + ($this->timeZoneSession->timeZone * 60));
                    }
                }
            }
            else {
                $this->timeZoneSession = new SessionContainer('timeZone');
                if ($this->timeZoneSession->timeZone != '') {
                    $date = date("Y-m-d", strtotime($date) + $this->timeZoneSession->timeZone * 60);
                } else {
                    $date = date("Y-m-d", strtotime($date));
                }
            }

            return $date;
        }
    }

}

