<?php

/**
 * This helper is used for check authentication of user
 * @category   Zend
 * @package    Helper_AuthHelper
 * @version    2.2
 * @author     Icreon Tech - DG
 */

namespace Common\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Authentication\AuthenticationService;

class Auth extends AbstractHelper {

    public $auth;
    
    public function __invoke() {
        $auth = new AuthenticationService();
        $this->auth = $auth;
        if ($this->auth->hasIdentity()) {
            return $this->auth->getIdentity();
        }else{
            return false;
        }
    }

}