<?php

/**
 * This page is used for display manifest image path on view pages
 * @package    Common_View_Helper_CommonController
 * @author     Icreon Tech - SR
 */

namespace common\View\Helper;

use Zend\View\Helper\AbstractHelper,
    Zend\ServiceManager\ServiceLocatorAwareInterface,
    Zend\ServiceManager\ServiceLocatorInterface,
    Zend\View\Exception;

/**
 * This class is used for display manifest image path on view pages
 * @package    Common_View_Helper_CommonController
 * @author     Icreon Tech - DT
 */
class GetManifestPath extends AbstractHelper implements ServiceLocatorAwareInterface {

    protected $message = null;

    /**
     * Set the service locator. 
     * 
     * @param ServiceLocatorInterface $serviceLocator 
     * @return CustomHelper 
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    /**
     * Get the service locator. 
     * 
     * @return \Zend\ServiceManager\ServiceLocatorInterface 
     */
    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function __invoke($param = Array()) {
        $sm = $this->getServiceLocator()->getServiceLocator();
        $config = $sm->get('application')->getConfig();
        $assetsPath = $config['file_upload_path']['assets_url'];
        $applicationSourceId = $config['application_source_id'];

        if (!empty($param['fileName'])) {
            if ($param['dataSource'] == 1 || empty($param['dataSource'])) { // 1 == passenger records
                /*if ($applicationSourceId == 1) { // on Web
                    $fileName = str_replace(array('tif', 'TIF'), array('jpg', 'jpg'), $param['fileName']); // direct replace
                } else {
                    $fileArray = explode('.', $param['fileName']);
                    $fileExt = $fileArray[1];
                    if (strtolower($fileExt) == 'tif') {
                        // will convert .tif to .jpg file
                    }
                }*/
				$fileArray = explode('.', $param['fileName']);
				$fileExt = $fileArray[1];
				if (strtolower($fileExt) == 'tif') {
					$fileName = substr($param['fileName'],0,-4);
					$fileName = $fileName.'.jpg';    
				}
				else {
					$fileName = $param['fileName'];
				}
                $folderName = substr($fileName, 0, 9);
                
				if(isset($param['imageSource']) && $param['imageSource'] == 'frontEnd')
					$assetsPath = $assetsPath.'manifest-low/'.$folderName; 
				else
					$assetsPath = $assetsPath.'manifest-low/'.$folderName; 
                    
                return $filePath = $assetsPath . '/' . strtolower($fileName);
            }
            else if ($param['dataSource'] == 2) { // 2 == Family records
                $folderName = substr($param['fileName'], 0, 9);
				$fileName = $param['fileName']; 
				if(isset($param['imageSource']) && $param['imageSource'] == 'frontEnd')
					$assetsPath = $assetsPath.'manifest-low/'.$folderName; 
				else
					$assetsPath = $assetsPath.'manifest-low/'.$folderName;                     
                
                return $filePath = $assetsPath . '/' . $fileName;
                /*if ($param['isThumb'] == 1)
                    $assetsPath = $assetsPath . 'manifest/' . $folderName;
                else
                    $assetsPath = $assetsPath . 'manifest/' . $folderName;
                
                return $filePath = $assetsPath . '/' . $param['fileName'];*/
            }
        }
    }

}