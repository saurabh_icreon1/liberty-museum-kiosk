<?php

/**
 * This controller is used create groups
 * @package    Common_CommonController
 * @author     Icreon Tech - DT
 */

namespace Common\Controller;

use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Common\Model\Common;
use Common\Model\Campaign;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use Zend\Session\Container;
use Zend\Session\Container as SessionContainer;

/**
 * This controller is used create groups
 * @package    Common_CommonController
 * @author     Icreon Tech - DT
 */
class CommonController extends BaseController {

    protected $_commonTable = null;
    protected $_contactTable = null;
    protected $_contactCrmTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;

    function __construct() {
        parent::__construct();
    }

    /**
     * This function is used to get table and config file object
     * @return     array
     * @param array $form
     * @author Icreon Tech - DT
     */
    public function getCommonTable() {
        //$this->checkUserAuthentication();
        if (!$this->_commonTable) {
            $sm = $this->getServiceLocator();
            $this->_commonTable = $sm->get('Common\Model\CommonTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
            $this->_auth = new AuthenticationService();
        }
        return $this->_commonTable;
    }

    /**
     * This function is used to get contacts lookup 
     * @return     array
     * @param array $form
     * @author Icreon Tech - DT
     */
    public function getContactsAction() {
        $this->checkUserAuthentication();
        $this->getCommonTable();
        $common = new Common($this->_adapter);
        $response = $this->getResponse();
        /* Contacts */
        $request = $this->getRequest();
        $post_arr = $request->getPost()->toArray();
        //$contact_array = $common->getContactArr($post_arr);
        //$contacts = $this->_commonTable->getContact($contact_array);

        $contact_array = $common->getContactArrAutoSuggest($post_arr);
        $contacts = $this->_commonTable->getContactAutoSuggest($contact_array);

        $all_contacts = array();
        $i = 0;
        if ($contacts !== false) {
            foreach ($contacts as $contact) {

                if ($contact['full_name'] != '' && $contact['full_name'] != NULL) {
                    $contact['full_name'] = $contact['full_name'];
                } else if ($contact['company_name'] != '' && $contact['company_name'] != NULL) {
                    $contact['full_name'] = $contact['company_name'];
                }
                if ($contact['full_name'] != '' && $contact['full_name'] != NULL) {
                    $all_contacts[$i] = array();
                    $all_contacts[$i]['user_id'] = $contact['user_id'];
                    $all_contacts[$i]['contact_id'] = $contact['contact_id'];
                    $all_contacts[$i]['full_name'] = $contact['full_name'];
                    $all_contacts[$i]['email_id'] = (!is_null($contact['email_id']) && $contact['email_id'] != '') ? $contact['email_id'] : '';
                    $all_contacts[$i]['omx_customer_number'] = (!is_null($contact['omx_customer_number']) && $contact['omx_customer_number'] != '') ? $contact['omx_customer_number'] : '';
                    $all_contacts[$i]['street_address_one'] = $contact['street_address_one'];
                    $all_contacts[$i]['street_address_two'] = $contact['street_address_two'];
                    $all_contacts[$i]['city'] = $contact['city'];
                    $all_contacts[$i]['state'] = $contact['state'];
                    $all_contacts[$i]['zip_code'] = $contact['zip_code'];
                    $all_contacts[$i]['country_id'] = $contact['country_id'];
                    $all_contacts[$i]['country_name'] = $contact['country_name'];
                    $i++;
                }
            }
        }
        $response->setContent(\Zend\Json\Json::encode($all_contacts));
        return $response;
        /* End Contacts */
    }

    /**
     * This function is used to get all master comapny
     * @return     array
     * @param array $form
     * @author Icreon Tech - DG
     */
    public function getCompanyAction() {
        //$this->checkUserAuthentication();
        $this->getCommonTable();

        $common = new Common($this->_adapter);
        $response = $this->getResponse();
        /* Contacts */
        $request = $this->getRequest();
        $post_arr = $request->getPost()->toArray();
        $company_array = $common->getCompanyArr($post_arr);
        $companies = $this->_commonTable->getCompany($company_array);
        $all_company = array();
        $i = 0;
        if ($companies !== false) {
            foreach ($companies as $contact) {
                $all_company[$i] = array();
                $all_company[$i]['company_id'] = $contact['company_id'];
                $all_company[$i]['company_name'] = $contact['company_name'];
                $i++;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($all_company));
        return $response;
        /* End Contacts */
    }

    /**
     * This function is used to get crm users lookup
     * @return     array
     * @param array $form
     * @author Icreon Tech - DT
     */
    public function getCrmContactsAction() {
        $this->checkUserAuthentication();
        $this->getCommonTable();
        $common = new Common($this->_adapter);
        $response = $this->getResponse();
        /* Crm User */
        $param = $this->params()->fromRoute();
        $request = $this->getRequest();
        $post_arr = $request->getPost()->toArray();
        $post_arr['crm_user_ids'] = !empty($param['ids']) ? $param['ids'] : null;
        $crm_user_array = $common->getCrmUserDataArr($post_arr);
        $crm_users = $this->_commonTable->getUserCrm($crm_user_array);
        $all_crm_users = array();
        $i = 0;
        if ($crm_users !== false) {
            foreach ($crm_users as $user) {
                $all_crm_users[$i] = array();
                $all_crm_users[$i]['crm_user_id'] = $user['crm_user_id'];
                $all_crm_users[$i]['crm_full_name'] = $user['crm_first_name'] . " " . $user['crm_last_name'];
                $all_crm_users[$i]['crm_email_id'] = $user['crm_email_id'];
                $i++;
            }
        }
        /* End Crm User */
        $response->setContent(\Zend\Json\Json::encode($all_crm_users));
        return $response;
    }

    /**
     * This function is used to get campaign lookup
     * @return     array
     * @param array $form
     * @author Icreon Tech - DT
     */
    public function getCampaignsAction() {
        $this->checkUserAuthentication();
        $this->getCommonTable();
        $common = new Common($this->_adapter);
        $response = $this->getResponse();
        $request = $this->getRequest();
        $postData = $request->getPost()->toArray();
        $compaignSearchDataArray = $common->getCompaignDataArr($postData);
        $compaignDataArray = $this->_commonTable->getCampaign($compaignSearchDataArray);
        $compaigns = array();
        $i = 0;
        if ($compaignDataArray !== false) {
            foreach ($compaignDataArray as $compaign) {
                $compaigns[$i] = array();
                $compaigns[$i]['campaign_id'] = $compaign['campaign_id'];
                $compaigns[$i]['campaign_title'] = $compaign['campaign_title'];
                $compaigns[$i]['campaign_code'] = $compaign['campaign_code'];
                $i++;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($compaigns));
        return $response;
    }

    /**
     * This function is used to get all membership afihc code
     * @return     array
     * @param array $form
     * @author Icreon Tech - DT
     */
    public function getMembershipAfihcAction() {
        $this->checkUserAuthentication();
        $this->getCommonTable();
        $common = new Common($this->_adapter);
        $response = $this->getResponse();
        /* membershipafihc */
        $request = $this->getRequest();
        $post_arr = $request->getPost()->toArray();
        $memberAfihcArr = $common->getMembershipAfihcArr($post_arr);
        $memberAfihc = $this->_commonTable->getMembershipAfihc($memberAfihcArr);
        $allAfihcMem = array();
        $i = 0;
        if ($memberAfihc !== false) {
            foreach ($memberAfihc as $afihc) {
                $allAfihcMem[$i] = array();
                $allAfihcMem[$i]['id'] = $afihc['afihc_member_id'];
                $allAfihcMem[$i]['value'] = $afihc['afihc_member_val'];
                $allAfihcMem[$i]['type'] = $afihc['type'];
                $i++;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($allAfihcMem));
        return $response;
        /* End membershipafihc */
    }

    /**
     * This function is used to get all phone lookup
     * @return     array
     * @param array $form
     * @author Icreon Tech - DT
     */
    public function getPhoneLookupAction() {
        $this->checkUserAuthentication();
        $this->getCommonTable();
        $common = new Common($this->_adapter);
        $response = $this->getResponse();
        /* phones */
        $request = $this->getRequest();
        $post_arr = $request->getPost()->toArray();
        $phoneArr = $common->getPhoneArr($post_arr);
        $phoneData = $this->_commonTable->getPhoneUser($phoneArr);

        $phones = array();
        $i = 0;
        if ($phoneData !== false) {
            foreach ($phoneData as $phone) {
                $phones[$i] = array();
                $phones[$i]['user_id'] = $phone['user_id'];
                $phones[$i]['phone'] = $phone['phone_information'];
                $i++;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($phones));
        return $response;
        /* End phones */
    }

    /**
     * This function is used to get all postal lookup
     * @return     array
     * @param array $form
     * @author Icreon Tech - DT
     */
    public function getPostalLookupAction() {
        $this->checkUserAuthentication();
        $this->getCommonTable();
        $common = new Common($this->_adapter);
        $response = $this->getResponse();
        /* Postals */
        $request = $this->getRequest();
        $post_arr = $request->getPost()->toArray();
        $postalArr = $common->getPostalArr($post_arr);
        $postalData = $this->_commonTable->getZipCodeUser($postalArr);

        $postals = array();
        $i = 0;
        if ($postalData !== false) {
            foreach ($postalData as $postal) {
                $postals[$i] = array();
                $postals[$i]['user_id'] = $postal['user_id'];
                $postals[$i]['zip_code'] = $postal['zip_code'];
                $i++;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($postals));
        return $response;
        /* End Postals */
    }

    /**
     * This function is used to get all state lookup
     * @return     array
     * @param array $form
     * @author Icreon Tech - DT
     */
    public function getStateLookupAction() {
        $this->checkUserAuthentication();
        $this->getCommonTable();
        $common = new Common($this->_adapter);
        $response = $this->getResponse();
        /* States */
        $request = $this->getRequest();
        $post_arr = $request->getPost()->toArray();
        $stateArr = $common->getStateArr($post_arr);
        $stateData = $this->_commonTable->getStateUser($stateArr);

        $states = array();
        $i = 0;
        if ($stateData !== false) {
            foreach ($stateData as $state) {
                $states[$i] = array();
                $states[$i]['user_id'] = $state['user_id'];
                $states[$i]['state'] = $state['state'];
                $i++;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($states));
        return $response;
        /* End States */
    }

    /**
     * This function is used to get all country lookup
     * @return     array
     * @param array $form
     * @author Icreon Tech - DT
     */
    public function getCountryLookupAction() {
        $this->checkUserAuthentication();
        $this->getCommonTable();
        $common = new Common($this->_adapter);
        $response = $this->getResponse();
        /* Postals */
        $request = $this->getRequest();
        $post_arr = $request->getPost()->toArray();
        $countryArr = $common->getCountryArr($post_arr);
        $countryData = $this->_commonTable->getCountryUser($countryArr);

        $countries = array();
        $i = 0;
        if ($countryData !== false) {
            foreach ($countryData as $country) {
                $countries[$i] = array();
                $countries[$i]['country_id'] = $country['country_id'];
                $countries[$i]['name'] = $country['name'];
                $i++;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($countries));
        return $response;
        /* End Postals */
    }

    public function getUserFamilyPersonAction() {

        $this->checkUserAuthentication();
        $this->getCommonTable();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $post_arr = $request->getPost()->toArray();
        $userFamilyPerson = array();
        $userFamilyPerson = $this->_commonTable->getCrmFamilyAssociatedPerson($post_arr);
        $name_list = array();
        if (!empty($userFamilyPerson)) {
            $i = 0;
            foreach ($userFamilyPerson as $subject) {
                $name_list[$i] = array();
                $name_list[$i]['first_name'] = $subject['name'];
                $i++;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($name_list));
    }

    public function authenticateUserAction() {
        $auth = new AuthenticationService();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('web-popup');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        if ($auth->hasIdentity() === true) {
            $user = $auth->getIdentity();
            if (isset($user->crm_email_id)) {
                $message = array('status' => 'admin');
                if (!$request->isXmlHttpRequest()) {
                    return $this->redirect()->toRoute('dashboard');
                }
            } else {
                $message = array('status' => 'error');
            }
            $response->setContent(\Zend\Json\Json::encode($message));
            return $response;
        }
        else
        {
            if ($this->_config['application_source_id'] == 2)
            {
                $message = array('status' => 'redirect_to_crm', 'encryptPath' => $this->encrypt("redirect to crm"));
                $response->setContent(\Zend\Json\Json::encode($message));
                return $response;
            }
            else
            {
                if (isset($_SERVER['HTTP_REFERER']))
                {
                    $refererUrl = $_SERVER['HTTP_REFERER'];
                }
                else
                {
                    if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == '/login')
                    {
                        $refererUrl = SITE_URL . '/profile';
                    }
                    else
                    {
                        $refererUrl = SITE_URL . $_SERVER['REQUEST_URI'];
                    }
                }
                $this->referPathSession = new SessionContainer('referPathSession');
                $this->referPathSession->path = $refererUrl;
                $message = array('status' => 'success', 'encryptPath' => $this->encrypt($refererUrl));
                $response->setContent(\Zend\Json\Json::encode($message));
                return $response;
            }
        }
    }

    /**
     * This function is used to get campaign lookup
     * @return     array
     * @param array $form
     * @author Icreon Tech - AS
     */
    public function getTraCampaignsAction() {
        $this->checkUserAuthentication();
        $this->getCommonTable();
        $common = new Common($this->_adapter);
        $response = $this->getResponse();
        $request = $this->getRequest();
        $postData = $request->getPost()->toArray();
        $compaignDataArray = $this->_commonTable->getTraCampaign($postData);
        $compaigns = array();
        $i = 0;
        if ($compaignDataArray !== false) {
            foreach ($compaignDataArray as $compaign) {
                $compaigns[$i] = array();
                $compaigns[$i]['campaign_id'] = $compaign['campaign_id'];
                $compaigns[$i]['campaign_title'] = $compaign['campaign_title'] . " (" . $compaign['campaign_code'] . ")";
                $compaigns[$i]['campaign_code'] = $compaign['campaign_code'];
                $i++;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($compaigns));
        return $response;
    }

    /*
     * This function is used redirect the page from third party url
     * @return  void   
     * @param passenger ID,
     * @author Icreon Tech - SR
     */

    public function searchManifestAction() {
        if(isset($_REQUEST['pID']) && !empty($_REQUEST['pID'])) {
            header('Location: ' . SITE_URL . '/passenger-details/' . $this->encrypt($_REQUEST['pID']) . '/' . $this->encrypt('manifest'));
        }
        else{
            header('Location: ' . SITE_URL . '/passenger');
        }
        die;
    }
    public function setUserTimezoneAction()
    {
        $request = $this->getRequest();
        $post_arr = $request->getPost()->toArray();
        if (isset($post_arr['visitortimezone']))
        {
            $this->timeZoneSession = new SessionContainer('timeZone');
            if (!isset($this->timeZoneSession->timeZone))
            {
                $this->timeZoneSession->timeZone = $post_arr['visitortimezone'];
            }
        }
        die;//return false;
    }
	
	public function getContactAutoSearchAction() {
        $this->checkUserAuthentication();
        $this->getCommonTable();
        $common = new Common($this->_adapter);
        $response = $this->getResponse();
        /* Contacts */
        $request = $this->getRequest();
        $post_arr = $request->getPost()->toArray();
        //$contact_array = $common->getContactArr($post_arr);
        //$contacts = $this->_commonTable->getContact($contact_array);

        $contact_array = $common->getContactAutoSearchArr($post_arr);
        $contacts = $this->_commonTable->getContactAutoSearch($contact_array);

        $all_contacts = array();
        $i = 0;
        if ($contacts !== false) {
            foreach ($contacts as $contact) {

                if ($contact['full_name'] != '' && $contact['full_name'] != NULL) {
                    $contact['full_name'] = $contact['full_name'];
                } else if ($contact['company_name'] != '' && $contact['company_name'] != NULL) {
                    $contact['full_name'] = $contact['company_name'];
                }
                if ($contact['full_name'] != '' && $contact['full_name'] != NULL) {
                    $all_contacts[$i] = array();
                    $all_contacts[$i]['user_id'] = $contact['user_id'];
                    $all_contacts[$i]['contact_id'] = $contact['contact_id'];
                    $all_contacts[$i]['full_name'] = $contact['full_name'];
                    $all_contacts[$i]['email_id'] = (!is_null($contact['email_id']) && $contact['email_id'] != '') ? $contact['email_id'] : '';
                    $all_contacts[$i]['omx_customer_number'] = (!is_null($contact['omx_customer_number']) && $contact['omx_customer_number'] != '') ? $contact['omx_customer_number'] : '';
                    $all_contacts[$i]['street_address_one'] = $contact['street_address_one'];
                    $all_contacts[$i]['street_address_two'] = $contact['street_address_two'];
                    $all_contacts[$i]['city'] = $contact['city'];
                    $all_contacts[$i]['state'] = $contact['state'];
                    $all_contacts[$i]['zip_code'] = $contact['zip_code'];
                    $all_contacts[$i]['country_id'] = $contact['country_id'];
                    $all_contacts[$i]['country_name'] = $contact['country_name'];
                    $i++;
                }
            }
        }
        $response->setContent(\Zend\Json\Json::encode($all_contacts));
        return $response;
        /* End Contacts */
    }

}
