<?php

/**
 * This controller is used create crons
 * @package    Common_DoController
 * @author     Icreon Tech - NS
 */

namespace Common\Controller;

use Base\Controller\BaseController;
use Common\Model\Common;
use Zend\Http\Request;
use Zend\Console\Request as ConsoleRequest;
use Zend\Authentication\AuthenticationService;
use SimpleXMLElement;
use Transaction\Model\Transaction;
use Authorize\lib\AuthnetXML;
//inscribe key for previous transaction
use User\Model\Woh;

/**
 * This controller is used to execute cron
 * @package    Common_DoController
 * @author     Icreon Tech - NS
 */
class DoController extends BaseController {

    protected $_commonTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;

    function __construct() {
        parent::__construct();
    }

    /**
     * This function is used to get table and config file object
     * @return     array
     * @param array $form
     * @author Icreon Tech - NS
     */
    public function getCommonTable() {
        if (!$this->_commonTable) {
            $sm = $this->getServiceLocator();
            $this->_commonTable = $sm->get('Common\Model\CommonTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
            $this->_auth = new AuthenticationService();
        }
        return $this->_commonTable;
    }

    /**
     * This function is used to call donow function
     * @return     array
     * @param array $form
     * @author Icreon Tech - NS
     */
    public function donowAction() {
	    ini_set('max_execution_time', 0);
        $request = $this->getRequest();
        $param = $this->params()->fromRoute();

        if (isset($param['cronParam']) and trim($param['cronParam']) != "") {
            $CronParam = trim($param['cronParam']);
        } else {
            $CronParam = "";
        }
        // Make sure that we are running in a console and the user has not tricked our
        // application into running this action from a public web server.
        if (!$request instanceof ConsoleRequest) {
            //throw new \RuntimeException('You can only use this action from a console!');
            //die();
        }

        if ($CronParam == "")
            $CronParam = "all";

        if ($CronParam == "all" || $CronParam == "FirstPreMembershipAlert") {  // 1 time
            $this->FirstPreMembershipAlert();
        }
        if ($CronParam == "all" || $CronParam == "RepeatPreMembershipAlert") {
            $this->RepeatPreMembershipAlert();
        }
        if ($CronParam == "all" || $CronParam == "RenewPreMembershipAlert") {
            $this->RenewPreMembershipAlert();
        }
        if ($CronParam == "all" || $CronParam == "ComeBackPostMembershipAlert") {  // 1 time
            $this->ComeBackPostMembershipAlert();
        }

        if ($CronParam == "all" || $CronParam == "FirstPledgeDueDateAlert") {  // 1 time
            $this->FirstPledgeDueDateAlert();
        }

        if ($CronParam == "all" || $CronParam == "RepeatPledgeDueDateAlert") {  // 1 time
            $this->RepeatPledgeDueDateAlert();
        }

        if ($CronParam == "all" || $CronParam == "UpdateShipmentStatus") {  // 1 time
            $this->UpdateShipmentStatus();
        }

        if ($CronParam == "all" || $CronParam == "AutoRenewalMembership") {  // 1 time
            $this->autoRenewalMembership();
        }
        if ($CronParam == "all" || $CronParam == "UpdateTransactionBatch") {  // 1 time
            $this->updateTransactionBatch();
        }
		if ($CronParam == "all" || $CronParam == "UpdateTransactionStatus") {  // 1 time
            $this->updateTransactionStatus();
        }
		/*if ($CronParam == "all" || $CronParam == "card") {  // 1 time
            $this->updateCardNo();
        }*/

        exit();
    }

    /**
     * This function is used to call FirstPreMembershipAlert function
     * @return     array
     * @param array $form
     * @author Icreon Tech - NS
     */
    private function FirstPreMembershipAlert() {
        $this->getCommonTable();
        $params1 = array();
        $params1['currDate'] = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
        $memberRecord = $this->_commonTable->getFirstPreMembershipAlert($params1);

        if (isset($memberRecord) and is_array($memberRecord)) {
            foreach ($memberRecord as $valMember) {
                $userDataArr = array();
                $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                $userDataArr['email_id'] = $valMember['email_id'];
                $userDataArr['first_name'] = $valMember['first_name'];
                $userDataArr['last_name'] = $valMember['last_name'];
                $userDataArr['expire_days'] = $valMember['first_alert_before_in_days'];
                $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, 17, $this->_config['email_options']);

                $params2 = array();
                $params2['user_id'] = $valMember['user_id'];
                $params2['user_membership_id'] = $valMember['user_membership_id'];
                $params2['alert_type'] = "1";
                $params2['log_date'] = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
                $insertUserLog = $this->_commonTable->insertUserEmailLogAlert($params2);
            }
        }
    }

    /**
     * This function is used to call RepeatPreMembershipAlert function
     * @return     array
     * @param array $form
     * @author Icreon Tech - NS
     */
    private function RepeatPreMembershipAlert() {
        $this->getCommonTable();
        $params1 = array();
        $currDate = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
        $params1['currDate'] = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
        $memberRecord = $this->_commonTable->getRepeatPreMembershipAlert($params1);

        if (isset($memberRecord) and is_array($memberRecord)) {
            foreach ($memberRecord as $valMember) {

                if (isset($valMember['FirstAlertDate']) and trim($valMember['FirstAlertDate']) != "") {
                    if (isset($valMember['LastRepeatAlertDate']) and trim($valMember['LastRepeatAlertDate']) != "") {
                        $baseDate = trim($valMember['LastRepeatAlertDate']);
                        $flag = "2";
                    } else {
                        $baseDate = trim($valMember['FirstAlertDate']);
                        $flag = "1";
                    }

                    if ($flag == "1") {
                        $baseDate = date('Y-m-d', strtotime($baseDate . ' + ' . trim($valMember['repeat_alerts_in_days']) . ' days'));
                    } elseif ($flag == "2" and strtotime($currDate) == strtotime($baseDate)) {
                        $baseDate = $baseDate;
                    } elseif ($flag == "2" and strtotime($currDate) > strtotime($baseDate)) {
                        $baseDate = date('Y-m-d', strtotime($baseDate . ' + ' . trim($valMember['repeat_alerts_in_days']) . ' days'));
                    }


                    if ($currDate == $baseDate and isset($valMember['CountRepeatAlertDate']) and isset($valMember['repeat_alerts_in_times']) and trim($valMember['CountRepeatAlertDate']) < trim($valMember['repeat_alerts_in_times']) and isset($valMember['repeat_alerts_in_days']) and trim($valMember['repeat_alerts_in_days']) > 0) {
                        $userDataArr = array();
                        $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                        $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                        $userDataArr['email_id'] = $valMember['email_id'];
                        $userDataArr['first_name'] = $valMember['first_name'];
                        $userDataArr['last_name'] = $valMember['last_name'];
                        $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, 18, $this->_config['email_options']);

                        $params2 = array();
                        $params2['user_id'] = $valMember['user_id'];
                        $params2['user_membership_id'] = $valMember['user_membership_id'];
                        $params2['alert_type'] = "2";
                        $params2['log_date'] = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
                        $insertUserLog = $this->_commonTable->insertUserEmailLogAlert($params2);
                    }
                }
            }
        }
    }

    /**
     * This function is used to call RenewPreMembershipAlert function
     * @return     array
     * @param array $form
     * @author Icreon Tech - NS
     */
    private function RenewPreMembershipAlert() {
        $this->getCommonTable();
        $params1 = array();
        $currDate = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
        $params1['currDate'] = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
        $memberRecord = $this->_commonTable->getRenewPostMembershipAlert($params1);

        if (isset($memberRecord) and is_array($memberRecord)) {
            foreach ($memberRecord as $valMember) {

                if (isset($valMember['ComeBackAlertDate']) and trim($valMember['ComeBackAlertDate']) == "") {
                    if (isset($valMember['LastRenewAlertDate']) and trim($valMember['LastRenewAlertDate']) != "") {
                        $baseDate = trim($valMember['LastRenewAlertDate']);
                        $flag = "2";
                    } else {
                        $baseDate = trim($valMember['membership_date_to']);
                        $flag = "1";
                    }

                    if ($flag == "1") {
                        $baseDate = date('Y-m-d', strtotime($baseDate . ' + 1 days'));
                    } elseif ($flag == "2" and strtotime($currDate) == strtotime($baseDate)) {
                        $baseDate = $baseDate;
                    } elseif ($flag == "2" and strtotime($currDate) > strtotime($baseDate)) {
                        $baseDate = date('Y-m-d', strtotime($baseDate . ' + 1 days'));
                    }


                    if ($currDate == $baseDate and isset($valMember['CountRenewAlertDate']) and isset($valMember['renew_alerts_in_times']) and trim($valMember['CountRenewAlertDate']) < trim($valMember['renew_alerts_in_times'])) {
                        $userDataArr = array();
                        $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                        $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                        $userDataArr['email_id'] = $valMember['email_id'];
                        $userDataArr['first_name'] = $valMember['first_name'];
                        $userDataArr['last_name'] = $valMember['last_name'];
                        $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, 19, $this->_config['email_options']);

                        $params2 = array();
                        $params2['user_id'] = $valMember['user_id'];
                        $params2['user_membership_id'] = $valMember['user_membership_id'];
                        $params2['alert_type'] = "3";
                        $params2['log_date'] = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
                        $insertUserLog = $this->_commonTable->insertUserEmailLogAlert($params2);
                    }
                }
            }
        }
    }

    /**
     * This function is used to call ComeBackPostMembershipAlert function
     * @return     array
     * @param array $form
     * @author Icreon Tech - NS
     */
    private function ComeBackPostMembershipAlert() {
        $this->getCommonTable();
        $params1 = array();
        $params1['currDate'] = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
        $memberRecord = $this->_commonTable->getComeBackPostMembershipAlert($params1);

        $currDate = date("Y-m-d", strtotime(DATE_TIME_FORMAT));

        if (isset($memberRecord) and is_array($memberRecord)) {
            foreach ($memberRecord as $valMember) {

                if (isset($valMember['LastRenewAlertDate']) and trim($valMember['LastRenewAlertDate']) != "" and isset($valMember['ComeBackAlertDate']) and trim($valMember['ComeBackAlertDate']) == "") {
                    $baseDate = date('Y-m-d', strtotime(trim($valMember['LastRenewAlertDate']) . ' + ' . trim($valMember['come_back_request_in_days']) . ' days'));
                    if (strtotime($currDate) == strtotime($baseDate)) {
                        $userDataArr = array();
                        $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                        $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                        $userDataArr['email_id'] = $valMember['email_id'];
                        $userDataArr['first_name'] = $valMember['first_name'];
                        $userDataArr['last_name'] = $valMember['last_name'];
                        $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, 31, $this->_config['email_options']);

                        $params2 = array();
                        $params2['user_id'] = $valMember['user_id'];
                        $params2['user_membership_id'] = $valMember['user_membership_id'];
                        $params2['alert_type'] = "4";
                        $params2['log_date'] = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
                        $insertUserLog = $this->_commonTable->insertUserEmailLogAlert($params2);
                    }
                }
            }
        }
    }

    /**
     * This function is used to call FirstPledgeDueDateAlert function
     * @return     array
     * @param array $form
     * @author Icreon Tech - NS
     */
    private function FirstPledgeDueDateAlert() {
        $this->getCommonTable();
        $params1 = array();
        $params1['currDate'] = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
        $memberRecord = $this->_commonTable->getFirstPledgeDueDateAlert($params1);

        if (isset($memberRecord) and is_array($memberRecord)) {
            foreach ($memberRecord as $valMember) {
                $userDataArr = array();
                $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                $userDataArr['email_id'] = $valMember['email_id'];
                $userDataArr['title'] = $valMember['title'];
                $userDataArr['first_name'] = $valMember['first_name'];
                $userDataArr['last_name'] = $valMember['last_name'];
                $userDataArr['suffix'] = $valMember['suffix'];
                $userDataArr['due_date'] = $valMember['due_date'];
                $userDataArr['pledge_amount'] = $valMember['pledge_amount'];
                $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, 7, $this->_config['email_options']);

                $params2 = array();
                $params2['pledge_transaction_id'] = $valMember['pledge_transaction_id'];
                $params2['alert_type'] = "1";
                $params2['log_date'] = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
                $insertPleLog = $this->_commonTable->insertPleEmailLogAlert($params2);
            }
        }
    }

    /**
     * This function is used to call RepeatPledgeDueDateAlert function
     * @return     array
     * @param array $form
     * @author Icreon Tech - NS
     */
    private function RepeatPledgeDueDateAlert() {
        $this->getCommonTable();
        $params1 = array();
        $currDate = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
        $params1['currDate'] = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
        $memberRecord = $this->_commonTable->getRepeatPledgeDueDateAlert($params1);

        if (isset($memberRecord) and is_array($memberRecord)) {
            foreach ($memberRecord as $valMember) {

                if (isset($valMember['FirstAlertDate']) and trim($valMember['FirstAlertDate']) != "") {
                    if (isset($valMember['LastRepeatAlertDate']) and trim($valMember['LastRepeatAlertDate']) != "") {
                        $baseDate = trim($valMember['LastRepeatAlertDate']);
                    } else {
                        $baseDate = trim($valMember['FirstAlertDate']);
                    }

                    $baseDate = date('Y-m-d', strtotime($baseDate . ' + ' . trim($valMember['additional_reminder_days']) . ' days'));

                    if ($currDate == $baseDate and isset($valMember['CountAlert']) and isset($valMember['num_reminder']) and trim($valMember['CountAlert']) < trim($valMember['num_reminder']) and isset($valMember['additional_reminder_days']) and $valMember['additional_reminder_days'] > 0) {

                        $userDataArr = array();
                        $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                        $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                        $userDataArr['email_id'] = $valMember['email_id'];
                        $userDataArr['title'] = $valMember['title'];
                        $userDataArr['first_name'] = $valMember['first_name'];
                        $userDataArr['last_name'] = $valMember['last_name'];
                        $userDataArr['suffix'] = $valMember['suffix'];
                        $userDataArr['due_date'] = $valMember['due_date'];
                        $userDataArr['pledge_amount'] = $valMember['pledge_amount'];


                        $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, 32, $this->_config['email_options']);

                        $params2 = array();
                        $params2['pledge_transaction_id'] = $valMember['pledge_transaction_id'];
                        $params2['alert_type'] = "2";
                        $params2['log_date'] = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
                        $insertPleLog = $this->_commonTable->insertPleEmailLogAlert($params2);
                    }
                }
            }
        }
    }

    public function getTransactionPaymentReceiveArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['transaction_payment_id']) && $dataArr['transaction_payment_id'] != '') ? $dataArr['transaction_payment_id'] : '';
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['amount']) && $dataArr['amount'] != '') ? $dataArr['amount'] : 0;
        $returnArr[] = (isset($dataArr['id']) && $dataArr['id'] != '') ? $dataArr['id'] : '';
        $returnArr[] = (isset($dataArr['authorize_transaction_id']) && $dataArr['authorize_transaction_id'] != '') ? $dataArr['authorize_transaction_id'] : '';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : DATE_TIME_FORMAT;
        return $returnArr;
    }

    /**
     * This function is used to insert the record in payment receive table.
     * @param array
     * @author Icreon Tech - SR
     */
    public function insertIntoPaymentReceive($dataArray = array()) {
        $paymentTransactionData = array();
        $paymentTransactionData['amount'] = $dataArray['amount'];
        $paymentTransactionData['transaction_id'] = $dataArray['transaction_id'];
        $paymentTransactionData['authorize_transaction_id'] = $dataArray['authorize_transaction_id'];
        $paymentTransactionData['transaction_payment_id'] = $dataArray['transaction_payment_id'];
        $paymentTransactionData['added_by'] = '';
        $paymentTransactionData['added_date'] = DATE_TIME_FORMAT;
        $paymentTransactionData['modify_by'] = '';
        $paymentTransactionData['modify_date'] = DATE_TIME_FORMAT;
        $paymentTransactionDataArr['0'] = $dataArray['transaction_id'];
        $creditTransactionPayments = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getCreditTransactionPayment($paymentTransactionDataArr);
        $paymentTransactionData = $this->getTransactionPaymentReceiveArr($paymentTransactionData);
        $paymentTransactionData['10'] = $dataArray['UnshipProductType'];
        $paymentTransactionData['11'] = @$dataArray['table_auto_id'];
        $paymentTransactionData['12'] = @$dataArray['table_type'];
        if ($dataArray['payment_source'] == 'credit') {
            $profileParam = array();
            $profileParam['transaction_id'] = $dataArray['transaction_id'];
            $profileParam['authorize_transaction_id'] = $dataArray['authorize_transaction_id'];
            $transactionProfileDetails = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getTransactionProfileCreditCartId($profileParam);
            foreach ($transactionProfileDetails as $profile) {
                if (!empty($profile['profile_id']))
                    $paymentTransactionData['9'] = $profile['credit_card_type_id'];
            }
            if (!empty($creditTransactionPayments)) {
                echo $creditTransactionPayments[0]['authorize_transaction_id'];
                $paymentCapture = requestForPayment($transactionId = $creditTransactionPayments[0]['authorize_transaction_id'], $amount = $dataArray['amount']);
                asd($paymentCapture);
                //if ($paymentCapture->isSuccessful()) { // we need to uncomment it.
                return $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->saveTransactionPaymentReceive($paymentTransactionData);
                //}
            }
        } else {
            $paymentTransactionData['9'] = ''; // credit_card_type_id
            return $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->saveTransactionPaymentReceive($paymentTransactionData);
        }
    }

    /**
     * This action is used to cron update shipment status
     * @return
     * @author Icreon Tech - NS
     */

    private function UpdateShipmentStatus() {
        set_time_limit(0);
        $this->getCommonTable();
        $dateTime = DATE_TIME_FORMAT;
        $startdateTime = date("m/d/Y", strtotime('-2 day',strtotime($dateTime)));
	    $enddateTime = date("m/d/Y", strtotime($dateTime));
        $pbSettingInfo = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPBSetting();
        $shipApiUrl = $pbSettingInfo['pb_api_url'];
        $shipApiUsername = $pbSettingInfo['pb_api_username'];
        $shipApiRequestUsername = $pbSettingInfo['request_username'];
        $shipApiPassword = $pbSettingInfo['pb_api_password'];
        $packageType = $pbSettingInfo['package_type'];
        /* $shipApiUrl = $this->_config['transaction_config']['SHIP_API_URL'];
          $shipApiUsername = $this->_config['transaction_config']['SHIP_API_USERNAME'];
          $shipApiPassword = $this->_config['transaction_config']['SHIP_API_PASSWORD']; */
        $xmlShipmentQueryRequest = '<?xml version="1.0" encoding="utf-8"?>
								<PierbridgeShipmentQueryRequest>
								<TransactionIdentifier></TransactionIdentifier>
							     <UserName>'.$shipApiRequestUsername.'</UserName>
								<QueryMode>Advanced</QueryMode>
								<FilterByUser>false</FilterByUser>
								<FilterByOrganization>false</FilterByOrganization>
								<FilterBySenderLocationOrganization>false</FilterBySenderLocationOrganization>
								<RecordCount>1000</RecordCount>
								<SortField>ShipmentID</SortField>
								<SortDirection>Ascending</SortDirection>
								<ShipDateFrom>'.$startdateTime.'</ShipDateFrom>
								<ShipDateTo>'.$enddateTime.'</ShipDateTo>
								<AttentionTo></AttentionTo>
								<CompanyName></CompanyName>
								<City></City>
								<PostalCode></PostalCode>
								<Country></Country>
								<BoLNumber></BoLNumber>
								<TrackingNumber></TrackingNumber>
								<Status></Status>
								<CarrierName></CarrierName>
								<BasicQuery></BasicQuery>
								<ShipDays></ShipDays>
								</PierbridgeShipmentQueryRequest>';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $shipApiUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 120);
        curl_setopt($curl, CURLOPT_USERPWD, "$shipApiUsername:$shipApiPassword");
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $this->removeSpecialChar($xmlShipmentQueryRequest));
        if (($result = curl_exec($curl)) === false) {
            echo 'Failed !!';
            die;
        }
        curl_close($curl);
        $xml = new SimpleXMLElement($result);
        if (isset($xml->Status->Code) and trim($xml->Status->Code) == 1) {
            if (isset($xml->Shipments) and isset($xml->Shipments->Shipment) and count($xml->Shipments->Shipment) > 0) {
                foreach ($xml->Shipments->Shipment as $Val) {
                    if (isset($Val->BoLNumber)) {
                        $ShipmentID = (string) $Val->BoLNumber;
                        if (isset($ShipmentID) and trim($ShipmentID) != "") {
                            $ShipmentID = trim($ShipmentID);
                            $TrackingNumbers = (string) $Val->TrackingNumbers->TrackingNumber;
                            if (isset($TrackingNumbers) and trim($TrackingNumbers) != "") {
                                $TrackingNumbers = trim($TrackingNumbers);
                            } else {
                                $TrackingNumbers = '';
                            }
                            $param = array();
                            $param['ShipmentID'] = $ShipmentID;
                            $param['TrackingNumbers'] = $TrackingNumbers;
                            $param['ShipDate'] = date("Y-m-d H:i:s",strtotime($Val->ProcessedDate));
                            $this->_commonTable->updateShipmentTrackingCodeAndProductStatus($param);
                            $shipmentGetRequestShippingId = $Val->ShipmentID;
                            $xmlShipmentGetRequest ='<PierbridgeShipmentGetRequest><ShipmentID>'.$shipmentGetRequestShippingId.'</ShipmentID><UserName>CRMUSER</UserName>
                                                    </PierbridgeShipmentGetRequest>';
                            $curlShipGetRequest = curl_init();
                            curl_setopt($curlShipGetRequest, CURLOPT_URL, $shipApiUrl);
                            curl_setopt($curlShipGetRequest, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($curlShipGetRequest, CURLOPT_TIMEOUT, 120);
                            curl_setopt($curlShipGetRequest, CURLOPT_USERPWD, "$shipApiUsername:$shipApiPassword");
                            curl_setopt($curlShipGetRequest, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                            curl_setopt($curlShipGetRequest, CURLOPT_POST, 1);
                            curl_setopt($curlShipGetRequest, CURLOPT_POSTFIELDS, $this->removeSpecialChar($xmlShipmentGetRequest));
                            if (($resultShipGetReq = curl_exec($curlShipGetRequest)) === false) {
                                echo 'Failed !!';
                                die;
                            }
                            curl_close($curlShipGetRequest);
                            $xmlShipGetReq= new SimpleXMLElement($resultShipGetReq);
                            if (isset($xmlShipGetReq->Status->Code) and trim((string)$xmlShipGetReq->Status->Code) == 1) {
                                $searchParam['shipmentId'] = $ShipmentID;
                                $searchResult = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getShipmentDetails($searchParam);
                                if(isset($searchResult[0]) && !empty($searchResult[0])){
                                    //get actual shipping cost
                                    $actualShippingCost = 0;
                                    if(isset($xmlShipGetReq->Shipment->ChargeGroups->ChargeGroup) && count($xmlShipGetReq->Shipment->ChargeGroups->ChargeGroup) > 0)
                                    {
                                        $indexOfCost = count($xmlShipGetReq->Shipment->ChargeGroups->ChargeGroup)-1;
                                        if(isset($xmlShipGetReq->Shipment->ChargeGroups->ChargeGroup[$indexOfCost]->ChargeGroupValue))
                                        {
                                            $actualShippingCost = floatval($xmlShipGetReq->Shipment->ChargeGroups->ChargeGroup[$indexOfCost]->ChargeGroupValue);
                                        }
                                    }
                                  
                                    //get actual shipping cost ends
                                    $searchDetails = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->searchTransactionDetails($searchResult[0]);
                                    $this->updateTransactionStatus($searchResult[0]['transactionId']);
                                    if ($searchDetails[0]['carrier_id'] != (string)$xmlShipGetReq->Carrier || $searchDetails[0]['service_type'] != (string)$xmlShipGetReq->ServiceType) {
                                        $transaction = new Transaction($this->_adapter);
                                        $newCarrierId = (string)$xmlShipGetReq->Carrier;
                                        $newServiceTypeId = (string)$xmlShipGetReq->ServiceType;
                                        $shippingMetArr = array();
                                        $shippingMetArr['carrier_id'] = $newCarrierId;
                                        $shippingMetArr['service_type'] = $newServiceTypeId;
                                        $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                                        
                                        $getShippingMethods = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getShippingMethodDoNow($shippingMethodArr);
                                        
                                        if (!empty($getShippingMethods)) {
                                            $shippingTypeId = $getShippingMethods[0]['pb_shipping_type_id'];
                                            $shippmentTypeArr = array();
                                            $shippmentTypeArr['transaction_shipment_id'] = $ShipmentID;
                                            $shippmentTypeArr['shipping_type_id'] = $shippingTypeId;
                                            $shippmentTypeArr['modified_by'] = '';
                                            $shippmentTypeArr['modified_date'] = DATE_TIME_FORMAT;
                                            $updateShippingTypeArr = $transaction->getUpdateShipmentTypeArr($shippmentTypeArr);
                                            
                                            $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->updateShippmentType($updateShippingTypeArr);
                                        }
                                    }
                                    
                                    if($searchResult[0]['actual_shipping_price'] == 0 && $actualShippingCost !=0)
                                    {
                                        //update actual shipping//
                                        $shippmentCostArr = array();
                                        $shippmentCostArr['transaction_shipment_id'] = $ShipmentID;
                                        $shippmentCostArr['actual_shipping_price'] = $actualShippingCost;
                                        $shippmentCostArr['carrier_id'] = (string)$xmlShipGetReq->Carrier;
                                        $shippmentCostArr['service_type'] = (string)$xmlShipGetReq->ServiceType;
                                        $shippmentCostArr['modified_by'] = '';
                                        $shippmentCostArr['modified_date'] = DATE_TIME_FORMAT;
                                        $transaction = new Transaction($this->_adapter);
                                        $updateShippingCostArr = $transaction->getUpdateActualShippingPriceArr($shippmentCostArr);
                                        $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->updateActualShippingPrice($updateShippingCostArr);
                                        
                                        //update actual shipping//
                                        /*update product wise actual shipping start*/
                                        $shipmentProductResult = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getShipmentProductsQtyWise(array('shipmentId'=>$ShipmentID));
                                        //echo "<pre>";print_r($shipmentProductResult);
                                        if(is_array($shipmentProductResult) && count($shipmentProductResult) > 0)
                                        {
                                            $totalProductWeight = 0;
                                            //calculate total product weight
                                            foreach ($shipmentProductResult as $key => $value)
                                            {
                                                //if ($value['is_free_shipping'] != 1 && $value['product_weight'] > 0 && $value['is_shippable'] == 1 && $value['shipping_cost'] == 1)
                                                if ($value['product_shipping_amount'] > 0 && $value['product_category'] !='2')
                                                {
                                                    $totalProductWeight+= ($value['product_weight'] * $value['qty_shipped']);
                                                }
                                            }
                                            //calculate total product weight ends

                                            foreach ($shipmentProductResult as $index => $value)
                                            {
                                                //if ($value['is_shippable'] == '1' && $value['product_weight'] > 0 && $value['is_free_shipping'] != '1' && $value['shipping_cost'] == '1')
                                                if ($value['product_shipping_amount'] > 0)
                                                { 
                                                    if (((isset($searchDetails[0]['carrier_id']) && $searchDetails[0]['carrier_id'] != (string)$xmlShipGetReq->Carrier) || (isset($searchDetails[0]['service_type']) && $searchDetails[0]['service_type'] != (string)$xmlShipGetReq->ServiceType))&& $value['product_category'] !='2') 
                                                    {
                                                        $percentShipping = ($value['product_weight'] * $value['qty_shipped'] * 100) / $totalProductWeight;
                                                        $productShippingPrice = ($percentShipping * $actualShippingCost) / 100;
                                                    }
                                                    else
                                                    {
                                                        if($value['qty_shipped'] !=$value['product_qty'])
                                                        {
                                                            $productShippingPrice = ($value['product_shipping_amount']*$value['qty_shipped'])/($value['product_qty']);
                                                        }
                                                        else
                                                        {
                                                            $productShippingPrice = $value['product_shipping_amount']; 
                                                        }

                                                    }

                                                    if ($productShippingPrice < 0)
                                                    {
                                                        $productShippingPrice = '0';
                                                    }
                                                    //update actual product shipping//
                                                    $shippmentCostArr = array();
                                                    $shippmentCostArr['product_type'] = $value['product_type'];
                                                    $shippmentCostArr['transaction_product_id'] = $value['id'];
                                                    $shippmentCostArr['actual_product_shipping'] = $productShippingPrice;
                                                    $updateProductShippingArr = array();
                                                    $updateProductShippingArr = $transaction->getUpdateProductActualShippingArr($shippmentCostArr);

                                                    $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->updateProductActualShipping($updateProductShippingArr);
                                                    //update actual product shipping//
                                                }
                                            }
                                        }
                                        /*update product wise actual shipping ends*/
                                    }
                                }
                            }
                        }
                    }
                }
                echo "Shippment Query Updated.";
                die;
            } else {
                echo "No Record Found.";
                die;
            }
        } else {
            echo "There is some error in shippment.";
            die;
        }
    }
	
    /**
     * This function is used to capture amount from transaction
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function capturePayment($creditAmountArr = array()) {

        $cashAmount = (isset($creditAmountArr['totalCashAmount'])) ? $creditAmountArr['totalCashAmount'] : 0;
        $checkAmount = (isset($creditAmountArr['totalCheckAmount'])) ? $creditAmountArr['totalCheckAmount'] : 0;
        $creditAmount = (isset($creditAmountArr['totalCreditAmount'])) ? $creditAmountArr['totalCreditAmount'] : 0;
        $k = 0;

        $remCashAmount = $cashAmount;
        $remCheckAmount = $checkAmount;
        $remCreditAmount = $creditAmount;

        foreach ($creditAmountArr['UnshipProductTotalAmount'] as $amtPro) {
            foreach ($creditAmountArr['payment_source'] as $amtKey => $amtVal) {
                $paymentTransactionData['table_auto_id'] = $creditAmountArr['table_auto_id'][$k];
                $paymentTransactionData['table_type'] = $creditAmountArr['table_type'][$k];
                $paymentTransactionData['authorize_transaction_id'] = $creditAmountArr['authorize_transaction_id'][$k];

                if ($amtVal['type'] == 'cash') {
                    if ($remCashAmount > 0 && $creditAmountArr['UnshipProductTotalAmount'][$k] > 0 && $creditAmountArr['payment_source'][$amtKey]['amount'] > 0) {
                        if ($remCashAmount >= $creditAmountArr['UnshipProductTotalAmount'][$k] && $creditAmountArr['payment_source'][$amtKey]['amount'] >= $creditAmountArr['UnshipProductTotalAmount'][$k]) {
                            $paymentTransactionData['amount'] = $creditAmountArr['UnshipProductTotalAmount'][$k];
                            $paymentTransactionData['transaction_payment_id'] = $creditAmountArr['payment_source'][$amtKey]['id'];
                            $remCashAmount = $remCashAmount - $creditAmountArr['UnshipProductTotalAmount'][$k];
                            $creditAmountArr['payment_source'][$amtKey]['amount'] = $creditAmountArr['payment_source'][$amtKey]['amount'] - $creditAmountArr['UnshipProductTotalAmount'][$k];
                            $creditAmountArr['UnshipProductTotalAmount'][$k] = 0;
                        } else if ($remCashAmount <= $creditAmountArr['UnshipProductTotalAmount'][$k] && $creditAmountArr['payment_source'][$amtKey]['amount'] < $creditAmountArr['UnshipProductTotalAmount'][$k]) {
                            $paymentTransactionData['amount'] = $amtVal['amount'];
                            $creditAmountArr['UnshipProductTotalAmount'][$k] = $creditAmountArr['UnshipProductTotalAmount'][$k] - $creditAmountArr['payment_source'][$amtKey]['amount'];
                            $remCashAmount = $remCashAmount - $creditAmountArr['payment_source'][$amtKey]['amount'];
                            $creditAmountArr['payment_source'][$amtKey]['amount'] = 0;
                        }
                        $paymentTransactionData['transaction_payment_id'] = $amtVal['id'];
                        $paymentTransactionData['transaction_id'] = $creditAmountArr['transactionId'];
                        $paymentTransactionData['UnshipProductType'] = $creditAmountArr['UnshipProductType'][0];
                        $paymentTransactionData['payment_source'] = 'cash';

                        if ($paymentTransactionData['amount'] != 0) {
                            $transactionPaymentId = $this->insertIntoPaymentReceive($paymentTransactionData);
                        }
                    }
                }
                if ($amtVal['type'] == 'check') {
                    if ($remCheckAmount > 0 && $creditAmountArr['UnshipProductTotalAmount'][$k] > 0 && $creditAmountArr['payment_source'][$amtKey]['amount'] > 0) {
                        if ($remCheckAmount >= $creditAmountArr['UnshipProductTotalAmount'][$k] && $creditAmountArr['payment_source'][$amtKey]['amount'] >= $creditAmountArr['UnshipProductTotalAmount'][$k]) {
                            $paymentTransactionData['amount'] = $creditAmountArr['UnshipProductTotalAmount'][$k];
                            $paymentTransactionData['transaction_payment_id'] = $creditAmountArr['payment_source'][$amtKey]['id'];
                            $remCheckAmount = $remCheckAmount - $creditAmountArr['UnshipProductTotalAmount'][$k];
                            $creditAmountArr['payment_source'][$amtKey]['amount'] = $creditAmountArr['payment_source'][$amtKey]['amount'] - $creditAmountArr['UnshipProductTotalAmount'][$k];
                            $creditAmountArr['UnshipProductTotalAmount'][$k] = 0;
                        } else if ($remCheckAmount <= $creditAmountArr['UnshipProductTotalAmount'][$k] && $creditAmountArr['payment_source'][$amtKey]['amount'] < $creditAmountArr['UnshipProductTotalAmount'][$k] && $remCheckAmount > 0) {
                            $paymentTransactionData['amount'] = $creditAmountArr['payment_source'][$amtKey]['amount'];
                            $creditAmountArr['UnshipProductTotalAmount'][$k] = $creditAmountArr['UnshipProductTotalAmount'][$k] - $creditAmountArr['payment_source'][$amtKey]['amount'];
                            $remCheckAmount = $remCheckAmount - $creditAmountArr['payment_source'][$amtKey]['amount'];
                            $creditAmountArr['payment_source'][$amtKey]['amount'] = 0;
                        }
                        $paymentTransactionData['transaction_payment_id'] = $amtVal['id'];
                        $paymentTransactionData['transaction_id'] = $creditAmountArr['transactionId'];
                        $paymentTransactionData['UnshipProductType'] = $creditAmountArr['UnshipProductType'][0];
                        $paymentTransactionData['payment_source'] = 'check';

                        if ($paymentTransactionData['amount'] != 0) {
                            $transactionPaymentId = $this->insertIntoPaymentReceive($paymentTransactionData);
                        }
                    }
                }

                if ($amtVal['type'] == 'credit') {

                    if ($remCreditAmount > 0 && $creditAmountArr['UnshipProductTotalAmount'][$k] > 0 && $amtVal['amount'] > 0) {
                        if ($remCreditAmount >= $creditAmountArr['UnshipProductTotalAmount'][$k] && $amtVal['amount'] >= $creditAmountArr['UnshipProductTotalAmount'][$k]) {
                            $paymentTransactionData['amount'] = $creditAmountArr['UnshipProductTotalAmount'][$k];
                            $paymentTransactionData['transaction_payment_id'] = $creditAmountArr['payment_source'][$amtKey]['id'];
                            $remCreditAmount = $remCreditAmount - $creditAmountArr['UnshipProductTotalAmount'][$k];
                            $creditAmountArr['payment_source'][$amtKey]['amount'] = $creditAmountArr['payment_source'][$amtKey]['amount'] - $creditAmountArr['UnshipProductTotalAmount'][$k];
                            $creditAmountArr['UnshipProductTotalAmount'][$k] = 0;
                        } else if ($remCreditAmount <= $creditAmountArr['UnshipProductTotalAmount'][$k] && $amtVal['amount'] < $creditAmountArr['UnshipProductTotalAmount'][$k]) {
                            $paymentTransactionData['amount'] = $creditAmountArr['payment_source'][$amtKey]['amount'];
                            $creditAmountArr['UnshipProductTotalAmount'][$k] = $creditAmountArr['UnshipProductTotalAmount'][$k] - $creditAmountArr['payment_source'][$amtKey]['amount'];
                            $remCreditAmount = $remCreditAmount - $amtVal['amount'];
                            $creditAmountArr['payment_source'][$amtKey]['amount'] = 0;
                        }
                        $paymentTransactionData['transaction_payment_id'] = $amtVal['id'];
                        $paymentTransactionData['transaction_id'] = $creditAmountArr['transactionId'];
                        $paymentTransactionData['UnshipProductType'] = $creditAmountArr['UnshipProductType'][0];
                        $paymentTransactionData['payment_source'] = 'credit';

                        if ($paymentTransactionData['amount'] != 0) {
                            $transactionPaymentId = $this->insertIntoPaymentReceive($paymentTransactionData);
                        }
                    }
                }
                //$z++;
            }
            $k++;
        }



        /*
          $cashAmount = (isset($creditAmountArr['totalCashAmount'])) ? $creditAmountArr['totalCashAmount'] : 0;
          $checkAmount = (isset($creditAmountArr['totalCheckAmount'])) ? $creditAmountArr['totalCheckAmount'] : 0;
          $creditAmount = (isset($creditAmountArr['totalCreditAmount'])) ? $creditAmountArr['totalCreditAmount'] : 0;
          $amountToPaid = (isset($creditAmountArr['amountToPaid'])) ? $creditAmountArr['amountToPaid'] : 0;
          $transactionId = (isset($creditAmountArr['transactionId'])) ? $creditAmountArr['transactionId'] : 0;
          if ($amountToPaid > ($checkAmount + $cashAmount)) {
          $amountToPaid = $amountToPaid - ($checkAmount + $cashAmount);
          }
          if ($amountToPaid > 0) {
          $transaction = new Transaction($this->_adapter);
          $paymentTransactionData = array();
          $paymentTransactionData['transaction_id'] = $transactionId;
          $paymentTransactionDataArr = $transaction->getCreditTransactionPaymentArr($paymentTransactionData);
          $creditTransactionPayments = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getCreditTransactionPayment($paymentTransactionDataArr);
          if (!empty($creditTransactionPayments)) {
          foreach ($creditTransactionPayments as $creditTransaction) {
          if ($creditTransaction['remaining_amount'] > 0) {
          if ($amountToPaid == 0) {
          break;
          }
          $payingAmount = ($creditTransaction['remaining_amount'] >= $amountToPaid) ? $amountToPaid : $creditTransaction['remaining_amount'];
          $trans = array('transaction' => array('profileTransPriorAuthCapture' => array(
          'amount' => $payingAmount,
          'transId' => $creditTransaction['authorize_transaction_id']
          )
          ),
          'extraOptions' => '<![CDATA[x_customer_ip=' . $_SERVER['SERVER_ADDR'] . ']]>'
          );
          $paymentCapture = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
          $paymentCapture->createCustomerProfileTransactionRequest($trans);
          if ($paymentCapture->isSuccessful()) {
          $paymentTransactionData = array();
          $paymentTransactionData['amount'] = $payingAmount;
          $paymentTransactionData['transaction_id'] = $creditTransaction['transaction_id'];
          $paymentTransactionData['authorize_transaction_id'] = $creditTransaction['authorize_transaction_id'];
          $paymentTransactionData['transaction_payment_id'] = $creditTransaction['transaction_payment_id'];
          $paymentTransactionData['added_by'] = $this->_auth->getIdentity()->crm_user_id;
          $paymentTransactionData['added_date'] = DATE_TIME_FORMAT;
          $paymentTransactionData['modify_by'] = $this->_auth->getIdentity()->crm_user_id;
          $paymentTransactionData['modify_date'] = DATE_TIME_FORMAT;
          $paymentTransactionData = $transaction->getTransactionPaymentReceiveArr($paymentTransactionData);
          $transactionPaymentId = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->saveTransactionPaymentReceive($paymentTransactionData);
          if ($creditTransaction['remaining_amount'] >= $amountToPaid) {
          $amountToPaid = 0;
          } else {
          $amountToPaid = $amountToPaid - $creditTransaction['remaining_amount'];
          }
          //asd($paymentCapture);
          }
          }
          }
          }
          } */
    }

    /**
     * This function is used to call autoRenewalMembership function
     * @return     array
     * @param array $form
     * @author Icreon Tech - DG
     */
    private function autoRenewalMembership() {
        $this->getCommonTable();
        $params = array();
        $params['currDate'] = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
        $memberRecord = $this->getServiceLocator()->get('User\Model\MembershipTable')->getAutoRenewedUser($params);
        //asd($memberRecord);
        $transaction = new Transaction($this->_adapter);
        if (!empty($memberRecord)) {
            foreach ($memberRecord as $val) {
                $paymentStatus = false;
                if ($val['minimun_donation_amount'] > 0) {
                    $userTokenInfo = $this->getServiceLocator()->get('User\Model\UserTable')->getUserTokenInformations(array('user_id' => $val['user_id']));
                    if (!empty($userTokenInfo)) {
                        $userTokenProfileId = $userTokenInfo[0]['profile_id'];
                    }
                    if (isset($userTokenProfileId) && $userTokenProfileId != '') {
                        $paymentStatus = $this->authorizeForPayment($userTokenProfileId, $val['minimun_donation_amount']);
                    }
                    if ($paymentStatus) {
                        $dateTime = DATE_TIME_FORMAT;
                        $finalTransaction = array();
                        $finalTransaction['user_id'] = $val['user_id'];
                        $finalTransaction['transaction_source_id'] = $this->_config['transaction_source']['transaction_source_id'];
                        $finalTransaction['num_items'] = 1;
                        $finalTransaction['sub_total_amount'] = $val['minimun_donation_amount'];
                        $finalTransaction['transaction_amount'] = $val['minimun_donation_amount'];
                        $finalTransaction['transaction_status_id'] = 10;
                        $finalTransaction['transaction_type'] = '1';
                        $finalTransaction['transaction_date'] = $dateTime;
                        $finalTransaction['is_pickup'] = '0';
                        $finalTransaction['is_free_shipping'] = '0';
                        $finalTransaction['added_date'] = $dateTime;
                        $finalTransaction['modify_date'] = $dateTime;
                        $finalTransactionArr = $transaction->getFinalTransactionArr($finalTransaction);
                        $transactionId = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->saveFinalTransaction($finalTransactionArr);
                        if ($transactionId) {
                            $startDate = date("Y-m-d");
                            $membershipDateFrom = '';
                            $membershipDateTo = '';
                            if ($val['validity_type'] == 1) {
                                $membershipDateFrom = $startDate;
                                $startDay = $this->_config['financial_year']['srart_day'];
                                $startMonth = $this->_config['financial_year']['srart_month'];
                                $startYear = date("Y");
                                $startDate = $startYear . "-" . $startMonth . "-" . $startDay;
                                $addYear = $val['validity_time'] - date("Y");
                                $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year', strtotime($startDate)));
                                $membershipDateTo = $futureDate;
                            } else if ($val['validity_type'] == 2) {
                                $membershipDateFrom = $startDate;
                                $futureDate = date('Y-m-d', strtotime('+' . $val['validity_time'] . ' year', strtotime($startDate)));
                                $membershipDateTo = $futureDate;
                            }
                            $userMembershipUpdateArr = array();
                            $userMembershipUpdateArr['user_id'] = $val['user_id'];
                            $userMembershipUpdateArr['membership_id'] = $val['membership_id'];
                            $userMembershipUpdateArr['transaction_id'] = $transactionId;
                            $userMembershipUpdateArr['transaction_date'] = $dateTime;
                            $userMembershipUpdateArr['transaction_amount'] = $val['minimun_donation_amount'];
                            $userMembershipUpdateArr['membership_date_from'] = $membershipDateFrom;
                            $userMembershipUpdateArr['membership_date_to'] = $membershipDateTo;
                            $getUpdateUserMembershipArr = $transaction->getUpdateUserMembershipArr($userMembershipUpdateArr);
                            $userMembershipId = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->updateUserMembership($getUpdateUserMembershipArr);

                            $templateId = 34; // Autierenew Membership
                            $userDataArr = $this->getServiceLocator()->get('User\Model\UserTable')->getUser(array('user_id' => $val['user_id']));
                            $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                            $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                            $userDataArr['membership_title'] = $val['membership_title'];
                            $userDataArr['amount'] = $val['minimun_donation_amount'];
                            $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $templateId, $this->_config['email_options']);
                        }
                    }
                }
            }
        }
        die;
    }

    public function authorizeForPayment($profileId, $amount) {
        $this->getCommonTable();
        $customerProfileId = $profileId;
        $createCustomerProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
        $createCustomerProfile->getCustomerProfileRequest(array('customerProfileId' => $customerProfileId));
        $paymentProfileId = $createCustomerProfile->profile->paymentProfiles->customerPaymentProfileId;
        $trans = array('transaction' => array('profileTransAuthOnly' => array(
                    'amount' => $amount,
                    'customerProfileId' => $customerProfileId,
                    'customerPaymentProfileId' => $paymentProfileId,
                    'order' => array(
                        'invoiceNumber' => 'IN' . time(),
                        'description' => 'autorenew membership',
                        'purchaseOrderNumber' => 'No' . time()),
                    'taxExempt' => 'false',
                    'recurringBilling' => 'false'
                )
            ),
            'extraOptions' => '<![CDATA[x_customer_ip=' . $_SERVER['SERVER_ADDR'] . ']]>'
        );
        $payment = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
        $payment->createCustomerProfileTransactionRequest($trans);
        if ($payment->isSuccessful()) {
            $responseArr = explode(',', $payment->directResponse);
            $transactionId = $responseArr[6];
            $payment = $this->requestForPayment($transactionId, $amount);
            return $payment;
        } else {
            return false;
        }
    }

    public function requestForPayment($transactionId, $amount) {
        $this->getCommonTable();
        $trans = array('transaction' => array('profileTransPriorAuthCapture' => array(
                    'amount' => $amount,
                    'transId' => $transactionId
                )
            ),
            'extraOptions' => '<![CDATA[x_customer_ip=' . $_SERVER['SERVER_ADDR'] . ']]>'
        );
        $paymentCapture = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
        $paymentCapture->createCustomerProfileTransactionRequest($trans);
        if ($paymentCapture->isSuccessful()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * This function is used to update the payment / payment received from authorize site.
     * @return void
     * @param void
     * @author Icreon Tech - SR
     */
    public function updateTransactionBatch() {
        $this->getCommonTable();
        $this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'];
        $createCustomerProfileBatch = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
        $dateTime= DATE_TIME_FORMAT;
        $fromDateTime = date("Y-m-d", strtotime('-2 day',strtotime($dateTime)))."T00:00:01";
	$toDateTime = date("Y-m-d", strtotime($dateTime))."T23:59:59";
        $createCustomerProfileBatch->getSettledBatchListRequest(array(
            'includeStatistics' => 'true',
            'firstSettlementDate' => $fromDateTime, //'2014-02-01T00:00:01',
            'lastSettlementDate' => $toDateTime //'2014-02-10T23:59:59',
        ));

        $postParam = array();
        //asd($createCustomerProfileBatch);
        if (count($createCustomerProfileBatch->batchList->batch) > 0) {
            foreach ($createCustomerProfileBatch->batchList->batch as $key => $val) {

                // insert the batch_id into the payment batches master table
                $batchParam = array();
                $batchParam['batch_id'] = $val->batchId;

                // check if batch Id already exists into payment batches master table
                $batchResult = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->checkBatchId($batchParam);
                if ($batchResult[0]['cnt'] == 0) { // insert case
                    $batchParam['settlement_date'] = str_replace('Z', '', (str_replace('T', ' ', (string) $val->settlementTimeUTC)));
                    $result = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->insertTransactionBatchId($batchParam);
                    $batchId = $result[0]['LAST_INSERT_ID'];
                } else { // update case
                    $result = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getBatchId($batchParam);
                    $batchId = $result[0]['payment_batch_id'];
                }

                $createCustomerProfileTransaction = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
                $createCustomerProfileTransaction->getTransactionListRequest(array(
                    'batchId' => $val->batchId
                ));
                //echo "<br>===" . $val->batchId . "===" . $val->settlementTimeLocal . "==" . $val->settlementTimeUTC . "==== $traVal->transId . "=>" . $traVal->accountType<br>";
                foreach ($createCustomerProfileTransaction->transactions->transaction as $traKey => $traVal) {

                    $postParam['payment_batch_id'] = $batchId;
                    $postParam['authorize_transaction_id'] = (string) $traVal->transId;
                    $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->updateTransactionBatchId($postParam);
                    $cardPostParam = array();
                    $cardPostDataParam = array();
                    $cardPostParam['cartType'] = $traVal->accountType;
                    $cardArray = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getCreditCardId($cardPostParam);
                    $cardType = '';
                    if (count($cardArray) > 0)
                        $cardType = $cardArray[0]['credit_card_type_id'];

                    if ($cardType != '') {

                        $cardPostDataParam['cartType'] = $cardType;
                        $cardPostDataParam['authorize_transaction_id'] = (string) $traVal->transId;
                        $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->updateTransactionCardTypeInfo($cardPostDataParam);
                    }
                }
            }
        }
        //die;
    }
    
    /**
     * This function is used to update the transaction status
     * @return void
     * @param void
     * @author Icreon Tech - AS
     */
    public function updateTransactionStatus($transactionId){
        $searchParam['transactionId'] = $transactionId;
        $searchProduct = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->searchTransactionProducts($searchParam);
        //asd($searchProduct,2);
        $count = count($searchProduct);
        $transactionStatus = 0;
        $traCount = 0;
        $cancelCount = 0;
        $holdCount = 0;
        $voidedShipCount = 0;
        $returnCount = 0;
        foreach ($searchProduct as $key => $val) {
            if ($val['product_status'] == '6') 
            {
                $cancelCount++;
            } 
            else if ($val['product_status'] == '7') 
            {
                $holdCount++;
            } 
            else if ($val['product_status'] == '12') 
            {
                $voidedShipCount++;
            } 
            else if ($val['product_status'] == '13') 
            {
                $returnCount++;
            }
            else {
                if ($transactionStatus < $val['product_status']) {
                    $traCount = 1;
                    $transactionStatus = $val['product_status'];
                } else if ($transactionStatus == $val['product_status']) {
                    $traCount++;
                }
            }
        }
        if ($cancelCount == $count) {
            $transactionStatus = '6';
        } else if ($holdCount == $count) {
            $transactionStatus = '7';
        } else if ($voidedShipCount == $count) {
            $transactionStatus = '12';
        } else if ($returnCount == $count) {
            $transactionStatus = '13';
        } else if (($count > ($traCount+$cancelCount+$voidedShipCount+$returnCount)) && ($transactionStatus == '2' || $transactionStatus == '4' || $transactionStatus == '8' || $transactionStatus == '10')) {
            $transactionStatus++;
        }
        $update['transactionStatus'] = $transactionStatus;
        $update['transactionId'] = $searchParam['transactionId'];
        $update['modified_by'] = '';
        $update['modified_date'] = DATE_TIME_FORMAT;
        $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->updateTransactionStatus($update);
        return true;
    }
	
	/**
     * This function is used to update the Card Number on database
     * @return void
     * @param void
     * @author Icreon Tech - SKR
     */
	
	public function updateCardNo() {
        $this->getCommonTable();
		
		$userTokenInfo = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getallProfiles();
		//asd($userTokenInfo);
		 
		   foreach ($userTokenInfo as $val) {
		          //echo  asd($val,2);
				  
                 $createCustomerProfile = new \AuthnetXML('5s8fDJ4n','3KMdQ8x7m96eJz38', 'AuthnetXML::USE_PRODUCTION_SERVER');
                 $createCustomerProfile->getCustomerProfileRequest(array('customerProfileId' => $val['profile_id']));
				 
				 if (count($createCustomerProfile->profile->paymentProfiles->payment) > 0) {
				 
				 asd($createCustomerProfile->profile->paymentProfiles->payment,2);
                 $cardNumbrer = $createCustomerProfile->profile->paymentProfiles->payment->creditCard->cardNumber;
                 $cardExpiryDate = $createCustomerProfile->profile->paymentProfiles->payment->creditCard->expirationDate;
				 $paymentProfileId=$val['payment_profile_id'];
				$this->getServiceLocator()->get('Transaction\Model\TransactionTable')->updateCardNumber($paymentProfileId,$cardNumbrer,$cardExpiryDate);
				}
                 
            }



        
    }
    //inscribe key for previous transaction
    /**
     * 
     */
    public function inscribekeyAction()
    {
       
       $wohArr = $this->getServiceLocator()->get('User\Model\WohTable')->getWohProductsWithoutInscribeKey();
       //echo "<pre>";print_r($wohArr);die;
        if (is_array($wohArr) && count($wohArr) > 0)
        {
            $sm = $this->getServiceLocator();
            $wohObj = new Woh($sm->get('dbAdapter'));
            //var_dump($wohObj);die;
            foreach ($wohArr as $woh)
            {
                $param = array();
                $inscribeKey = $wohObj->getInscribeKey($woh['final_name']);
                $param['woh_id'] = $woh['woh_id'];
                $param['inscribe_key'] = $inscribeKey;
                $this->getServiceLocator()->get('User\Model\WohTable')->updateWohProductInscribeKey($param);
               
            }
        }
        exit(0);
    }

}
