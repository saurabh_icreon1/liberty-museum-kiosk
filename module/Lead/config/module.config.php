<?php

/**
 * This page is used for activity module configuration details.
 * @package    Activity
 * @author     Icreon Tech - DT
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Lead\Controller\Lead' => 'Lead\Controller\LeadController'
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'createLead' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-lead[/:contact_id][/:case_contact_id][/:case_contact_id]',
                    'defaults' => array(
                        'controller' => 'Lead\Controller\Lead',
                        'action' => 'addLead',
                    ),
                ),
            ),
            'getUserContactInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-contact-info',
                    'defaults' => array(
                        'controller' => 'Lead\Controller\Lead',
                        'action' => 'getUserContactInfo',
                    ),
                ),
            ),
            'getlead' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/leads',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Lead\Controller',
                        'controller' => 'Lead',
                        'action' => 'getLead',
                    ),
                ),
            ),
            'editlead' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-lead[/:lead_id][/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Lead\Controller',
                        'controller' => 'Lead',
                        'action' => 'editLeads',
                    ),
                ),
            ),
            'searchleads' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/search-leads[/:top]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Lead\Controller',
                        'controller' => 'Lead',
                        'action' => 'searchLeadSearch',
                    ),
                ),
            ),
            'savesearchlead' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/lead-save-search[/]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Lead\Controller',
                        'controller' => 'Lead',
                        'action' => 'leadSaveSearch',
                    ),
                ),
            ),
            'savedsearchlead' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-saved-lead-search[/]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Lead\Controller',
                        'controller' => 'Lead',
                        'action' => 'getSavedLeadSearchParam',
                    ),
                ),
            ),
            'deleteLeadSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-lead-search[/]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Lead\Controller',
                        'controller' => 'Lead',
                        'action' => 'deleteLeadSearch',
                    ),
                ),
            ),
            'deletelead' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-lead',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Lead\Controller',
                        'controller' => 'Lead',
                        'action' => 'delete-lead',
                    ),
                ),
            ),
            'viewlead' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-lead[/:id][/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Lead\Controller',
                        'controller' => 'Lead',
                        'action' => 'view-lead',
                    ),
                ),
            ),
            'leaddetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/lead-detail[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Lead\Controller',
                        'controller' => 'Lead',
                        'action' => 'leadDetail',
                    ),
                ),
            ),
            'leadphoneno' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/lead-phone-no[/:counter][/:container][/:divid]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Lead\Controller',
                        'controller' => 'Lead',
                        'action' => 'lead-phone-no',
                    ),
                ),
            ),
            'leadaddressform' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/lead-address-form[/:counter][/:container]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Lead\Controller',
                        'controller' => 'Lead',
                        'action' => 'lead-address-form',
                    ),
                ),
            ),
            'getLeadContact' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-lead-contact',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Lead\Controller',
                        'controller' => 'Lead',
                        'action' => 'getLeadContact',
                    ),
                ),
            ),
            'contactleads' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/contact-leads[/:userid]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Lead\Controller',
                        'controller' => 'Lead',
                        'action' => 'contactLeads',
                    ),
                ),
            ),
            'searchcontactleads' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/search-contact-leads[/:userid]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Lead\Controller',
                        'controller' => 'Lead',
                        'action' => 'searchContactLeads',
                    ),
                ),
            ),
        )
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'navigation' => array(
        'default' => array(
            array(
                'label' => 'Leads',
                'route' => 'getlead',
                'pages' => array(
                    array(
                        'label' => 'List',
                        'route' => 'getlead',
                        'pages' => array(
                            array(
                                'label' => 'Create',
                                'route' => 'createLead',
                                'action' => 'addLead',
                            ),
                            array(
                                'label' => 'View',
                                'route' => 'leaddetail',
                                'action' => 'leadDetail',
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'editlead',
                                'action' => 'editLeads',
                            )
                        )
                    )
                )
            )
        )
    ),
    'lead_messages' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
            'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'lead' => __DIR__ . '/../view'
        ),
        'template_map' => array(
            'layout/custom' => __DIR__ . '/../../module/Common/layout/crm.phtml',
            'error/404' => __DIR__ . '/../../module/Common/error/404.phtml',
            'error/index' => __DIR__ . '/../../module/Common/error/index.phtml',
        ),
    ),
    'global_config' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
);
