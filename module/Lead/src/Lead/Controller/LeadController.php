<?php

/**
 * This controller is used lead module
 * @package    Lead_LeadController
 * @author     Icreon Tech - ST
 */

namespace Lead\Controller;

use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Mvc\Controller\Plugin\Params;
use Lead\Form\AddLeadForm;
use Lead\Form\EditLeadForm;
use Lead\Form\CaseContactForm;
use Lead\Form\ContactPhonenoForm;
use Lead\Form\ContactAddressesForm;
use Common\Model\Contact;
use Lead\Model\Lead;
use Lead\Model\LeadTable;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use Lead\Form\SearchLeadForm;
use Lead\Form\SaveSearchForm;

/**
 * This controller is used lead module
 * @package    Lead_LeadController
 * @author     Icreon Tech - ST
 */
class LeadController extends BaseController {

    protected $_leadTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;
    protected $_commonTable = null;

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->auth = $auth;
        if ($this->auth->hasIdentity() === false) {
            //return $this->redirect()->toRoute('crm');
        }
    }

    /**
     * This functikon is used to get config variables
     * @return  array
     * @param
     * @author Icreon Tech - ST
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This function is used to get table and config file object
     * @return  array
     * @param void
     * @author Icreon Tech - ST
     */
    public function getLeadTable() {
        $this->checkUserAuthentication();
        if (!$this->_leadTable) {
            $sm = $this->getServiceLocator();
            $this->_leadTable = $sm->get('Lead\Model\LeadTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
            $this->_auth = new AuthenticationService();
        }
        return $this->_leadTable;
    }

    /**
     * This action is used to list Lead
     * @return json
     * @param void
     * @author Icreon Tech - ST
     */
    public function getLeadAction() {
        if ($this->auth->hasIdentity() === false) {
            return $this->redirect()->toRoute('crm');
        }
        $this->layout('crm');
        $this->getConfig();
        $this->getLeadTable();
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $search_lead_form = new SearchLeadForm();
        $save_search_form = new SaveSearchForm();

        $param = '';
        $get_type = $this->getServiceLocator()->get('Common\Model\CommonTable')->getLeadType($param);
        $lead_type = array();
        $leadTypeDesc = array();
        $lead_type[''] = 'None';
        foreach ($get_type as $key => $val) {
            $lead_type[$val['lead_type_id']] = $val['lead_type'];
            $lead_typeDesc[$val['lead_type_id']] = $val['type_description'];
        }
        $search_lead_form->get('lead_type')->setAttribute('options', $lead_type);


        $get_source = $this->getServiceLocator()->get('Common\Model\CommonTable')->getLeadSource($param);
        $lead_source = array();
        $leadSourceDesc = array();
        $lead_source[''] = 'None';
        foreach ($get_source as $key => $val) {
            $lead_source[$val['lead_source_id']] = $val['lead_source'];
            $lead_sourceDesc[$val['lead_source_id']] = $val['source_description'];
        }
        $search_lead_form->get('lead_source')->setAttribute('options', $lead_source);

        $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
        $dataParam['is_active'] = '1';
        $dataParam['lead_search_id'] = '';
        $crmSearchArray = $this->getLeadTable()->getSavedSearch($dataParam);
        $srmSearch_list = array();
        $srmSearch_list[''] = 'Select';
        if (!empty($crmSearchArray)) {
            foreach ($crmSearchArray as $key => $val) {
                $srmSearch_list[$val['lead_search_id']] = stripslashes($val['title']);
            }
        }
        $save_search_form->get('saved_search')->setAttribute('options', $srmSearch_list);

        $request = $this->getRequest();
        $viewModel = new ViewModel();

        $viewModel->setVariables(array(
            'leadTypeDesc' => $leadTypeDesc,
            'leadSourceDesc' => $leadSourceDesc,
            'search_lead_form' => $search_lead_form,
            'search_form' => $save_search_form,
            'messages' => $messages,
            'jsLangTranslate' => $this->_config['lead_messages']['config']['lead_search'],
            'lead_typeDesc' => $lead_typeDesc,
            'lead_sourceDesc' => $lead_sourceDesc
                )
        );
        return $viewModel;
    }

    /**
     * This action is used to create Lead
     * @return json
     * @param void
     * @author Icreon Tech - ST
     */
    public function addLeadAction() {
        $this->layout('crm');
        $this->getLeadTable();
        $lead_create_messages = $this->_config['lead_messages']['config']['lead_create_message'];
        $lead = new Lead($this->_adapter);
        $create_lead_form = new AddLeadForm();
        $form_phone = new ContactPhonenoForm();
        $form_address = new ContactAddressesForm();
        $form_case = new CaseContactForm();
        $create_lead_form->add($form_phone);
        $create_lead_form->add($form_address);
        $create_lead_form->add($form_case);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $caseDetails = array();
        $contact_id = isset($params['contact_id']) ? $this->decrypt($params['contact_id']) : null;
        $case_contact_id = isset($params['case_contact_id']) ? $this->decrypt($params['case_contact_id']) : null;
        if ($case_contact_id != null) {
            $caseDetails = $this->getServiceLocator()->get('Cases\Model\CasesTable')->getCaseById(array($case_contact_id));
        }

        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $country_list = array();
        $country_list[''] = 'Select Country';
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }
        $form_address->get('addressinfo_country[]')->setAttribute('options', $country_list);

        $get_title = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTitle();
        $user_title = array();
        $user_title[''] = 'None';
        foreach ($get_title as $key => $val) {
            $user_title[$val['title']] = $val['title'];
        }
        $create_lead_form->get('title')->setAttribute('options', $user_title);

        $get_suffix = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSuffix();
        $user_suffix = array();
        $user_suffix[''] = 'None';
        foreach ($get_suffix as $key => $val) {
            $user_suffix[$val['suffix']] = $val['suffix'];
        }
        $create_lead_form->get('suffix')->setAttribute('options', $user_suffix);
        $param = '';
        $get_source = $this->getServiceLocator()->get('Common\Model\CommonTable')->getLeadSource($param);
        $lead_source = array();
        $lead_sourceDesc = array();
        $lead_source[''] = 'None';
        foreach ($get_source as $key => $val) {
            $lead_source[$val['lead_source_id']] = $val['lead_source'];
            $lead_sourceDesc[$val['lead_source_id']] = $val['source_description'];
        }
        $create_lead_form->get('lead_source')->setAttribute('options', $lead_source);

        $get_type = $this->getServiceLocator()->get('Common\Model\CommonTable')->getLeadType($param);
        $lead_type = array();
        $lead_typeDesc = array();
        $lead_type[''] = 'None';
        foreach ($get_type as $key => $val) {
            $lead_type[$val['lead_type_id']] = $val['lead_type'];
            $lead_typeDesc[$val['lead_type_id']] = $val['type_description'];
        }

        $create_lead_form->get('lead_type')->setAttribute('options', $lead_type);

        /** add private note* */
        $moduleInfoArr = array();
        $moduleInfoArr[] = 'Lead';
        $moduleInfoArr[] = 'Lead';
        $moduleInfoArr[] = 'create-lead-private-note';
        $moduleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getModuleAction($moduleInfoArr);

        $auth = new AuthenticationService();

        $userDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getCrmUserDetails(array('crm_user_id' => $auth->getIdentity()->crm_user_id));

        $roleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->checkCrmUserPermission($userDetail[0]['role_id']);

        $addPrivateNotePermission = isset($roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']]) ? $roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']] : 1;

        /** end of add private note * */
        if ($request->isXmlHttpRequest() && $request->isPost()) {

            $postedData = $request->getPost();
            $postData = $lead->exchangeArrayLead($request->getPost());
            if ((empty($postData['contact_name_id']) || $postData['contact_name_id'] == "") && @$insertedUserId) {
                $postData['contact_name_id'] = $insertedUserId;
            }

            $postData['lead_reason_type_id'] = "";
            $postData['won_type'] = "";
            $postData['transaction_id'] = (isset($postedData['transaction_id'])) ? $postedData['transaction_id'] : '';
            $postData['pledge_id'] = (isset($postedData['pledge_id'])) ? $postedData['pledge_id'] : '';
            $postData['campaign_id'] = (isset($postedData['campaign_id'])) ? $postedData['campaign_id'] : '';
            $postData['added_by'] = $this->auth->getIdentity()->crm_user_id;
            $postData['added_date'] = DATE_TIME_FORMAT;
            $postData['modified_by'] = "";
            $postData['modified_date'] = DATE_TIME_FORMAT;
            $postData['status'] = (isset($postedData['status'])) ? $postedData['status'] : '';


            unset($postData['contact_name']);
            unset($postData['assigned_to']);

            $leadId = $this->getLeadTable()->insertLead($postData);
            if ($leadId) {
                /** insert into the change log */
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_led_change_logs';
                $changeLogArray['activity'] = '1';/** 1 == for insert */
                $changeLogArray['id'] = $leadId;
                $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                /** end insert into the change log */
                //Insert lead note
                $postData2 = $lead->exchangeArrayLeadNote($request->getPost());
                if (isset($postData2['note']) && !empty($postData2['note'])) {
                    $postData2['lead_id'] = $leadId;
                    $postData2['added_by'] = $this->auth->getIdentity()->crm_user_id;
                    $postData2['added_date'] = DATE_TIME_FORMAT;
                    $flag = $this->getLeadTable()->insertLeadNote($postData2);
                }
                //Insert phone number//
                $phoneForm = array();
                for ($i = 0; $i < count($postedData['country_code']); $i++) {
                    $phoneForm[$i]['country_code'] = isset($postedData['country_code'][$i]) ? $postedData['country_code'][$i] : null;
                    $phoneForm[$i]['area_code'] = isset($postedData['area_code'][$i]) ? $postedData['area_code'][$i] : null;
                    $phoneForm[$i]['phone_no'] = isset($postedData['phone_no'][$i]) ? $postedData['phone_no'][$i] : null;
                    $phoneForm[$i]['phone_type'] = isset($postedData['phone_type'][$i]) ? $postedData['phone_type'][$i] : null;
                    $phoneForm[$i]['continfo_primary'] = isset($postedData['continfo_primary'][$i]) ? $postedData['continfo_primary'][$i] : '0';
                }
                //Insert lead address information

                $addressForm = array();
                for ($i = 0; $i < count($postedData['addressinfo_location_type']); $i++) {
                    $addressForm[$i]['addressinfo_location_type'] = isset($postedData['addressinfo_location_type'][$i]) ? $postedData['addressinfo_location_type'][$i] : null;
                    $addressForm[$i]['addressinfo_street_address_1'] = isset($postedData['addressinfo_street_address_1'][$i]) ? $postedData['addressinfo_street_address_1'][$i] : null;
                    $addressForm[$i]['addressinfo_street_address_2'] = isset($postedData['addressinfo_street_address_2'][$i]) ? $postedData['addressinfo_street_address_2'][$i] : null;
                    $addressForm[$i]['addressinfo_city'] = isset($postedData['addressinfo_city'][$i]) ? $postedData['addressinfo_city'][$i] : null;
                    $addressForm[$i]['addressinfo_zip'] = isset($postedData['addressinfo_zip'][$i]) ? $postedData['addressinfo_zip'][$i] : null;
                    $addressForm[$i]['addressinfo_state'] = isset($postedData['addressinfo_state'][$i]) ? $postedData['addressinfo_state'][$i] : null;
                    $addressForm[$i]['addressinfo_country'] = isset($postedData['addressinfo_country'][$i]) ? $postedData['addressinfo_country'][$i] : null;

                    $locationStr = "";
                    if ($postedData['address_contact_contact_type'][$i] && strlen($postedData['address_contact_contact_type'][$i]) > 0) {
                        $locationArr = explode(',', $postedData['address_contact_contact_type'][$i]);
                        $refindeArr = $this->removeEmptyArray($locationArr);
                        $locationStr = implode(',', $refindeArr);
                    }


                    $addressForm[$i]['addressinfo_location'] = $locationStr;
                }
            }




            if ($leadId) {
                $messages = array('status' => "success", 'message' => $lead_create_messages['ADD_CONFIRM_MSG']);
                $this->flashMessenger()->addMessage($lead_create_messages['ADD_CONFIRM_MSG']);
            } else {
                $messages = array('status' => "error", 'message' => $lead_create_messages['ADD_ERROR_MSG']);
                $this->flashMessenger()->addMessage($lead_create_messages['ADD_ERROR_MSG']);
            }

            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $lead_create_messages,
            'create_lead_form' => $create_lead_form,
            'contact_id' => $contact_id,
            'case_contact_id' => $case_contact_id,
            'caseDetails' => $caseDetails,
            'addPrivateNotePermission' => $addPrivateNotePermission,
            'lead_sourceDesc' => $lead_sourceDesc,
            'lead_typeDesc' => $lead_typeDesc
        ));

        return $viewModel;
    }

    /**
     * This action is used to get user contact information
     * @return json
     * @param void
     * @author Icreon Tech - ST
     */
    public function getUserContactInfoAction() {
        $this->checkUserAuthentication();
        $this->getLeadTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $post_arr = $request->getPost()->toArray();
        $returnArr = array();

        $returnArr['username'] = (isset($dataArr['username']) && $dataArr['username'] != '') ? $dataArr['username'] : '';
        $returnArr['email_id'] = (isset($dataArr['email_id']) && $dataArr['email_id'] != '') ? $dataArr['email_id'] : '';
        $returnArr['user_id'] = (isset($post_arr['user_id']) && $post_arr['user_id'] != '') ? $post_arr['user_id'] : '';
        $returnArr['verfy_code'] = (isset($dataArr['verfy_code']) && $dataArr['verfy_code'] != '') ? $dataArr['verfy_code'] : '';
        $contact = $this->_leadTable->getContact($returnArr);

        //Get Phone Conatct infor
        $searchArr['user_id'] = (isset($post_arr['user_id']) && $post_arr['user_id'] != '') ? $post_arr['user_id'] : '';
        $searchArr['null_id'] = null;
        $contactPhoneArr = $this->_leadTable->getContactPhoneInformation($searchArr);
        //Get Address information
        $addressInfoArr = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations($searchArr);
        $all_contacts = array();
        if ($contact !== false && is_array($contact) && count($contact) > 0) {
            $all_contacts[] = array();
            $all_contacts['title'] = $contact['title'];
            $all_contacts['first_name'] = $contact['first_name'];
            $all_contacts['middle_name'] = $contact['middle_name'];
            $all_contacts['last_name'] = $contact['last_name'];
            $all_contacts['user_type'] = $contact['user_type'];
            $all_contacts['company_name'] = $contact['company_name'];
            $all_contacts['suffix'] = $contact['suffix'];
            $all_contacts['email_id'] = $contact['email_id'];
            $all_contacts['phoneArr'] = $contactPhoneArr;
            $all_contacts['addressArr'] = $addressInfoArr;
        }
        $response->setContent(\Zend\Json\Json::encode($all_contacts));
        return $response;
    }

    /**
     * This Action is used to get search lead    
     * @return json
     * @param void
     * @author Icreon Tech - ST
     */
    public function searchLeadSearchAction() {
        try {
            $request = $this->getRequest();

            $response = $this->getResponse();
            $messages = array();
            $isExport = $request->getPost('export');
            $this->getConfig();
            parse_str($request->getPost('searchString'), $searchParam);

            $dashlet = $request->getPost('crm_dashlet');
            $dashletSearchId = $request->getPost('searchId');
            $dashletId = $request->getPost('dashletId');
            $top = $request->getPost('top');
            $dashletSearchType = $request->getPost('searchType');
            $dashletSearchString = $request->getPost('dashletSearchString');
            if (isset($dashletSearchString) && $dashletSearchString != '') {
                $searchParam = unserialize($dashletSearchString);
            }

            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $searchParam['startIndex'] = $start;

            //Export to excel
            if ($isExport != "" && $isExport == "excel") {
                $chunksize = $this->_config['export']['chunk_size'];
                $export_limit = $this->_config['export']['export_limit'];

                $searchParam['recordLimit'] = $chunksize;
            } else {
                $searchParam['recordLimit'] = $limit;
            }
            $searchParam['sortField'] = $request->getPost('sidx');
            $searchParam['sortOrder'] = $request->getPost('sord');

            if (isset($searchParam['last_transaction']) && $searchParam['last_transaction'] != 1 && $searchParam['last_transaction'] != '') {
                $date_range = $this->getDateRange($searchParam['last_transaction']);
                $searchParam['from_date'] = $date_range['from'];
                $searchParam['to_date'] = $date_range['to'];
            }

            if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] != 1 && $searchParam['added_date_range'] != '') {
                $date_range = $this->getDateRange($searchParam['added_date_range']);
                $searchParam['from_date'] = $this->DateFormat($date_range['from'], 'db_date_format');
                $searchParam['to_date'] = $this->DateFormat($date_range['to'], 'db_date_format');
            } else if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] == '') {
                $searchParam['from_date'] = '';
                $searchParam['to_date'] = '';
            } else {
                if (isset($searchParam['added_date_from']) && $searchParam['added_date_from'] != '') {
                    $searchParam['from_date'] = $this->DateFormat($searchParam['added_date_from'], 'db_date_format');
                }
                if (isset($searchParam['added_date_to']) && $searchParam['added_date_to'] != '') {
                    $searchParam['to_date'] = $this->DateFormat($searchParam['added_date_to'], 'db_date_format');
                }
            }



            $page_counter = 1;
            $number_of_pages = 1;
            do {
                if(isset($top) && !empty($top)){
                    $searchParam['top'] = $top;
                }
                $get_contact_arr = $this->getLeadTable()->getLead($searchParam);
                $searchParam['startIndex'] = '';
                $searchParam['recordLimit'] = '';
                $get_contact_arr2 = $this->getLeadTable()->getLead($searchParam);
                $countResult = array();
                if (!empty($get_contact_arr2)) {
                    @$countResult[0]->RecordCount = ($get_contact_arr2 == false) ? 0 : count($get_contact_arr2);
                }

                $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
                $newExplodedColumnName = array();
                if (!empty($dashletResult)) {
                    $explodedColumnName = explode(",", $dashletResult[0]['table_column_names']);
                    foreach ($explodedColumnName as $val) {
                        if (strpos($val, ".")) {
                            $newExplodedColumnName[] = trim(strstr($val, ".", false), ".");
                        } else {
                            $newExplodedColumnName[] = trim($val);
                        }
                    }
                }

                $total = $countResult[0]->RecordCount;
                $jsonResult = array();
                $arrCell = array();
                $subArrCell = array();

                if ($isExport == "excel") {

                    /* if ($total > $export_limit) {
                      $totalExportedFiles = ceil($total / $export_limit);
                      $exportzip = 1;
                      }
                     * */
                    //echo $total;
                    //exit;
                    if ($total > $chunksize) {
                        $number_of_pages = ceil($total / $chunksize);
                        $page_counter++;
                        $start = $chunksize * $page_counter - $chunksize;
                        $searchParam['startIndex'] = $start;
                        $searchParam['page'] = $page_counter;
                    } else {
                        $number_of_pages = 1;
                        $page_counter++;
                    }
                } else {
                    $jsonResult['records'] = isset($countResult[0]->RecordCount) ? $countResult[0]->RecordCount : '';
                    $jsonResult['page'] = $page;
                    $jsonResult['total'] = isset($countResult[0]->RecordCount) ? @ceil($countResult[0]->RecordCount / $limit) : '';
                }
                if (!empty($get_contact_arr)) {
                    $arrExport = array();
                    foreach ($get_contact_arr as $k => $val) {
                        $dashletCell = array();
                        $view = $this->encrypt('view');
                        $edit = $this->encrypt('edit');
                        $view_link = "<a href='/view-lead/" . $this->encrypt($val['lead_id']) . "/" . $view . "' class='view-icon'>View<div class='tooltip'>View<span></span></div></a>";
                        $edit_link = "<a href='/edit-lead/" . $this->encrypt($val['lead_id']) . "/" . $edit . "' class='edit-icon'>Edit<div class='tooltip'>Edit<span></span></div></a>";
                        $delete_link = "<a href='#delete_lead_content' onclick='deleteLead(" . $val['lead_id'] . ");' class='delete_lead delete-icon'>Delete<div class='tooltip'>Delete<span></span></div></a>";
                        $action = '<div class="action-col">' . $view_link . $edit_link . $delete_link . '</div>';
                        $arrCell['id'] = $val['user_id'];
                        if ($val['won_type'] != '') {
                            $lead_won = 'Yes';
                            $dashletLeadWon = array_search('won_type', $newExplodedColumnName);
                            if ($dashletLeadWon !== false)
                                $dashletCell[$dashletLeadWon] = $lead_won;
                        } else {
                            $lead_won = 'No';
                            $dashletLeadWon = array_search('won_type', $newExplodedColumnName);
                            if ($dashletLeadWon !== false)
                                $dashletCell[$dashletLeadWon] = $lead_won;
                        }

                        $dashletEmailId = array_search('email_id', $newExplodedColumnName);
                        if ($dashletEmailId !== false)
                            $dashletCell[$dashletEmailId] = $val['email_id'];

                        $dashletOpAmt = array_search('opportunity_amount', $newExplodedColumnName);
                        if ($dashletOpAmt !== false)
                            $dashletCell[$dashletOpAmt] = "$ " . $val['opportunity_amount'];

                        $dashletLeadType = array_search('lead_type', $newExplodedColumnName);
                        if ($dashletLeadType !== false)
                            $dashletCell[$dashletLeadType] = $val['lead_type'];

                        $dashletLeadSoucre = array_search('lead_source', $newExplodedColumnName);
                        if ($dashletLeadSoucre !== false)
                            $dashletCell[$dashletLeadSoucre] = $val['lead_source'];

                        $caseContactId = '<span class="plus-sign"><a href="/create-activity/' . $this->encrypt($val['user_id']) . '/' . $this->encrypt('2') . '/' . $this->encrypt($val['lead_id']) . '"><div class="tooltip">Add activity<span></span></div></a></span>';
                        $fullName = trim($val['full_name']);
                        if (isset($dashlet) && $dashlet == 'dashlet') {
                            $fullName = "<a href='/view-contact/" . $this->encrypt($val['user_id']) . "/" . $view . "' class='txt-decoration-underline'>$fullName</a>";
                            $dashletFullName = array_search('full_name', $newExplodedColumnName);
                            if ($dashletFullName !== false)
                                $dashletCell[$dashletFullName] = $fullName;

                            $arrCell['cell'] = $dashletCell;
                            $subArrCell[] = $arrCell;
                            $jsonResult['rows'] = $subArrCell;
                        } else {
                            //Export to excel
                            if ($isExport == "excel") {
                                $arrExport[] = array('Name' => $val['full_name'], 'Email' => $val['email_id'], 'Type' => $val['lead_type'], 'Contact Type' => $val['lead_source'], 'Amount' => $val['opportunity_amount'], 'Converted' => $lead_won);
                            } else {
                                $arrCell['cell'] = array($caseContactId . $fullName, $val['email_id'], $val['lead_type'], $val['lead_source'], $val['opportunity_amount'], $lead_won, $action);
                                $subArrCell[] = $arrCell;
                                $jsonResult['rows'] = $subArrCell;
                            }
                        }
                    }
                    if ($isExport == "excel") {
                        $filename = "lead_export_" . date("Y-m-d") . ".csv";
                        $this->arrayToCsv($arrExport, $filename, $page_counter);
                    }
                }
            } while ($page_counter <= $number_of_pages && $isExport == "excel");
            if ($isExport == "excel") {
                $this->downloadSendHeaders("lead_export_" . date("Y-m-d") . ".csv");
                die;
            } else {
                $response->setContent(\Zend\Json\Json::encode($jsonResult));
                return $response;
            }
        } catch (Exception $e) {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * This action is used to edit lead
     * @return   array
     * @param void
     * @author Icreon Tech - ST
     */
    public function editLeadsAction() {
        $this->checkUserAuthentication();
        $this->getLeadTable();
        $lead_create_messages = $this->_config['lead_messages']['config']['lead_create_message'];
        $lead = new Lead($this->_adapter);
        $edit_lead_form = new EditLeadForm();
        $form_phone = new ContactPhonenoForm();
        $form_address = new ContactAddressesForm();
        $edit_lead_form->add($form_phone);
        $edit_lead_form->add($form_address);

        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $country_list = array();
        $country_list[''] = 'Select Country';
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }
        $form_address->get('addressinfo_country[]')->setAttribute('options', $country_list);

        $get_title = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTitle();
        $user_title = array();
        $user_title[''] = 'None';
        foreach ($get_title as $key => $val) {
            $user_title[$val['title']] = $val['title'];
        }
        $edit_lead_form->get('title')->setAttribute('options', $user_title);

        $get_suffix = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSuffix();
        $user_suffix = array();
        $user_suffix[''] = 'None';
        foreach ($get_suffix as $key => $val) {
            $user_suffix[$val['suffix']] = $val['suffix'];
        }
        $edit_lead_form->get('suffix')->setAttribute('options', $user_suffix);


        $get_reason = $this->getServiceLocator()->get('Common\Model\CommonTable')->getLeadReason();
        $lead_reason = array();
        $lead_reason[''] = 'None';
        foreach ($get_reason as $key => $val) {
            $lead_reason[$val['lead_reason_type_id']] = $val['lead_reason_type'];
        }
        $edit_lead_form->get('lead_reason_type')->setAttribute('options', $lead_reason);
        /** add private note* */
        $moduleInfoArr = array();
        $moduleInfoArr[] = 'Lead';
        $moduleInfoArr[] = 'Lead';
        $moduleInfoArr[] = 'create-lead-private-note';
        $moduleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getModuleAction($moduleInfoArr);

        $auth = new AuthenticationService();

        $userDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getCrmUserDetails(array('crm_user_id' => $auth->getIdentity()->crm_user_id));

        $roleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->checkCrmUserPermission($userDetail[0]['role_id']);

        $addPrivateNotePermission = isset($roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']]) ? $roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']] : 1;
        /** end of add private note * */
        /** view private note* */
        $moduleInfoArr = array();
        $moduleInfoArr[] = 'Lead';
        $moduleInfoArr[] = 'Lead';
        $moduleInfoArr[] = 'view-lead-private-note';
        $moduleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getModuleAction($moduleInfoArr);

        $auth = new AuthenticationService();

        $userDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getCrmUserDetails(array('crm_user_id' => $auth->getIdentity()->crm_user_id));

        $roleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->checkCrmUserPermission($userDetail[0]['role_id']);

        $viewPrivateNotePermission = isset($roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']]) ? $roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']] : 1;
        /** end of view private note * */
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $postedData = $request->getPost();

            $postData = $lead->exchangeArrayLead($request->getPost());
            //If contact id is not set
            if (!isset($postData['contact_name_id']) || empty($postData['contact_name_id'])) {
                /*
                 
                $filterParam = $lead->filterParam($request->getPost());
                $insertedUserId = $this->getLeadTable()->insertContact($filterParam);
                if ($insertedUserId) {
                    //Insert phone number
                    $phoneForm = array();
                    for ($i = 0; $i < count($postedData['country_code']); $i++) {
                        $phoneForm[$i]['country_code'] = isset($postedData['country_code'][$i]) ? $postedData['country_code'][$i] : null;
                        $phoneForm[$i]['area_code'] = isset($postedData['area_code'][$i]) ? $postedData['area_code'][$i] : null;
                        $phoneForm[$i]['phone_no'] = isset($postedData['phone_no'][$i]) ? $postedData['phone_no'][$i] : null;
                        $phoneForm[$i]['phone_type'] = isset($postedData['phone_type'][$i]) ? $postedData['phone_type'][$i] : null;

                        if ($i == $postedData['phone_index']) {
                            $phoneForm[$i]['continfo_primary'] = '1';
                        } else {
                            $phoneForm[$i]['continfo_primary'] = '0';
                        }
                    }

                    //Insert lead address information

                    $addressForm = array();
                    for ($i = 0; $i < count($postedData['addressinfo_location_type']); $i++) {
                        $addressForm[$i]['addressinfo_location_type'] = isset($postedData['addressinfo_location_type'][$i]) ? $postedData['addressinfo_location_type'][$i] : null;
                        $addressForm[$i]['addressinfo_street_address_1'] = isset($postedData['addressinfo_street_address_1'][$i]) ? $postedData['addressinfo_street_address_1'][$i] : null;
                        $addressForm[$i]['addressinfo_street_address_2'] = isset($postedData['addressinfo_street_address_2'][$i]) ? $postedData['addressinfo_street_address_2'][$i] : null;
                        $addressForm[$i]['addressinfo_city'] = isset($postedData['addressinfo_city'][$i]) ? $postedData['addressinfo_city'][$i] : null;
                        $addressForm[$i]['addressinfo_zip'] = isset($postedData['addressinfo_zip'][$i]) ? $postedData['addressinfo_zip'][$i] : null;
                        $addressForm[$i]['addressinfo_state'] = isset($postedData['addressinfo_state]'][$i]) ? $postedData['addressinfo_state]'][$i] : null;
                        $addressForm[$i]['addressinfo_country'] = isset($postedData['addressinfo_country'][$i]) ? $postedData['addressinfo_country'][$i] : null;
                        if ($postedData['address_contact_contact_type'][$i] && strlen($postedData['address_contact_contact_type'][$i]) > 0) {
                            $locationArr = explode(',', $postedData['address_contact_contact_type'][$i]);
                            $refindeArr = $this->removeEmptyArray($locationArr);
                        }
                        $locationStr = implode(',', $refindeArr);

                        $addressForm[$i]['addressinfo_location'] = $locationStr;
                    }
                }
                */
            }

            if ((empty($postData['contact_name_id']) || $postData['contact_name_id'] == "") && $insertedUserId) {
                $postData['contact_name_id'] = $insertedUserId;
            }
            $lead_id = $postedData['lead_id'];
            $postData['lead_reason_type_id'] = (isset($postedData['lead_reason_type'])) ? $postedData['lead_reason_type'] : '';
            $postData['won_type'] = (isset($postedData['lead_reason'])) ? $postedData['lead_reason'] : '';
            $postData['transaction_id'] = (isset($postedData['transaction_id'])) ? $postedData['transaction_id'] : '(NULL)';
            $postData['pledge_id'] = (isset($postedData['pledge_id'])) ? $postedData['pledge_id'] : '(NULL)';
            $postData['campaign_id'] = (isset($postedData['campaign_id'])) ? $postedData['campaign_id'] : '';
            $postData['modified_by'] = $this->auth->getIdentity()->crm_user_id;
            $postData['modified_date'] = DATE_TIME_FORMAT;
            unset($postData['case_contact_id']);
            unset($postData['contact_name']);
            unset($postData['assigned_to']);
            $postData['lead_id'] = $lead_id;
            $this->getLeadTable()->updateLead($postData);
            $leadId = $lead_id;
            if ($leadId) {
                /** insert into the change log */
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_led_change_logs';
                $changeLogArray['activity'] = '2';
                $changeLogArray['id'] = $lead_id;
                $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                /** end insert into the change log */
                $this->getLeadTable()->deleteLeadDetails(array('lead_id' => $lead_id));
                //Insert lead note
                $postData2 = $lead->exchangeArrayLeadNote($request->getPost());
                if (isset($postData2['note']) && !empty($postData2['note'])) {
                    $postData2['lead_id'] = $lead_id;
                    $postData2['added_by'] = $this->auth->getIdentity()->crm_user_id;
                    ;
                    $postData2['added_date'] = DATE_TIME_FORMAT;
                    $flag = $this->getLeadTable()->insertLeadNote($postData2);
                }
                //Insert phone number//
                $phoneForm = array();
                for ($i = 0; $i < count($postedData['country_code']); $i++) {
                    $phoneForm[$i]['country_code'] = isset($postedData['country_code'][$i]) ? $postedData['country_code'][$i] : null;
                    $phoneForm[$i]['area_code'] = isset($postedData['area_code'][$i]) ? $postedData['area_code'][$i] : null;
                    $phoneForm[$i]['phone_no'] = isset($postedData['phone_no'][$i]) ? $postedData['phone_no'][$i] : null;
                    $phoneForm[$i]['phone_type'] = isset($postedData['phone_type'][$i]) ? $postedData['phone_type'][$i] : null;
                    if ($i == $postedData['phone_index']) {
                        $phoneForm[$i]['continfo_primary'] = '1';
                    } else {
                        $phoneForm[$i]['continfo_primary'] = '0';
                    }
                }
                //Insert lead address information
                $addressForm = array();

                for ($i = 0; $i < count($postedData['addressinfo_location_type']); $i++) {
                    $addressForm[$i]['addressinfo_location_type'] = isset($postedData['addressinfo_location_type'][$i]) ? $postedData['addressinfo_location_type'][$i] : null;
                    $addressForm[$i]['addressinfo_street_address_1'] = isset($postedData['addressinfo_street_address_1'][$i]) ? $postedData['addressinfo_street_address_1'][$i] : null;
                    $addressForm[$i]['addressinfo_street_address_2'] = isset($postedData['addressinfo_street_address_2'][$i]) ? $postedData['addressinfo_street_address_2'][$i] : null;
                    $addressForm[$i]['addressinfo_city'] = isset($postedData['addressinfo_city'][$i]) ? $postedData['addressinfo_city'][$i] : null;
                    $addressForm[$i]['addressinfo_zip'] = isset($postedData['addressinfo_zip'][$i]) ? $postedData['addressinfo_zip'][$i] : null;
                    $addressForm[$i]['addressinfo_state'] = isset($postedData['addressinfo_state'][$i]) ? $postedData['addressinfo_state'][$i] : null;
                    $addressForm[$i]['addressinfo_country'] = isset($postedData['addressinfo_country'][$i]) ? $postedData['addressinfo_country'][$i] : null;
                    $locationStr = "";
                    if ($postedData['address_contact_contact_type'][$i] && strlen($postedData['address_contact_contact_type'][$i]) > 0) {
                        $locationArr = explode(',', $postedData['address_contact_contact_type'][$i]);
                        $refindeArr = $this->removeEmptyArray($locationArr);
                        $locationStr = implode(',', $refindeArr);
                    }
                    $addressForm[$i]['addressinfo_location'] = $locationStr;
                }
            }
            if ($leadId) {
                $messages = array('status' => "success", 'message' => $lead_create_messages['UPDATE_CONFIRM_MSG']);
                $this->flashMessenger()->addMessage($lead_create_messages['UPDATE_CONFIRM_MSG']);
            } else {
                $messages = array('status' => "error", 'message' => $lead_create_messages['UPDATE_ERROR_MSG']);
                $this->flashMessenger()->addMessage($lead_create_messages['UPDATE_ERROR_MSG']);
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        } else if (isset($params['lead_id']) && $params['lead_id'] != '') {

            $this->layout('crm');
            $this->getLeadTable();
            $lead_id = $this->decrypt($params['lead_id']);
            $case_detail_arr = array();
            $case_detail_arr[] = $lead_id;
            $lead_data = $this->_leadTable->getLeadById($lead_id);
            $caseDetails = "";
            if (!empty($lead_data['case_contact_id'])) {
                $caseDetails = $this->getServiceLocator()->get('Cases\Model\CasesTable')->getCaseById(array($lead_data['case_contact_id']));
            }
            $lead_address_info = array();
            $lead_address_information = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('user_id' => $lead_data['user_id'], 'address_information_id' => ''));
            if (!empty($lead_address_information)) {
                foreach ($lead_address_information as $key => $value) {
                    if ($value['location_name'] != '' && $value['location_name'] == 1) {
                        $lead_address_info[] = $value;
                    }
                }
                if (empty($lead_address_info)) {
                    foreach ($lead_address_information as $key1 => $value1) {
                        if ($key1 == 0) {
                            $lead_address_info[] = $value1;
                        }
                    }
                }
            }
            $lead_phone_information = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations(array('user_id' => $lead_data['user_id'], 'phone_id' => ''));
            $lead_phone_info = array();
            if (!empty($lead_phone_information)) {
                foreach ($lead_phone_information as $k => $v) {
                    if ($v['is_primary'] == 1) {
                        $lead_phone_info[] = $v;
                    }
                }

                if (empty($lead_phone_info)) {
                    foreach ($lead_phone_information as $k1 => $v1) {
                        if ($k1 == 0) {
                            $lead_phone_info[] = $v1;
                        }
                    }
                }
            }
            $lead_notes = $this->_leadTable->getLeadNotes(array('lead_id' => $lead_id));

            $mode = (isset($params['mode']) && $params['mode'] != '') ? $params['mode'] : $this->encrypt("view");
            $full_name = "";
            if ($lead_data['contact_first_name'] != '')
                $full_name = $lead_data['contact_first_name'];
            if ($lead_data['contact_middle_name'] != '')
                $full_name.= " " . $lead_data['contact_middle_name'];
            if ($lead_data['contact_last_name'] != '')
                $full_name.= " " . $lead_data['contact_last_name'];
            if ($full_name == '' && $lead_data['contact_company_name'] != '')
                $full_name = $lead_data['contact_company_name'];
            $edit_lead_form->get('lead_id')->setValue($lead_id);
            $edit_lead_form->get('contact_name')->setValue($full_name);
            $edit_lead_form->get('contact_name_id')->setValue($lead_data['user_id']);
            $edit_lead_form->get('assigned_to')->setValue($lead_data['crm_user_full_name']);
            $edit_lead_form->get('assigned_to_id')->setValue($lead_data['crm_user_id']);
            $edit_lead_form->get('first_name')->setValue($lead_data['contact_first_name']);
            $edit_lead_form->get('middle_name')->setValue($lead_data['contact_middle_name']);
            $edit_lead_form->get('last_name')->setValue($lead_data['contact_last_name']);
            $edit_lead_form->get('email_address')->setValue($lead_data['contact_email_id']);
            $edit_lead_form->get('title')->setValue($lead_data['contact_title']);
            $edit_lead_form->get('suffix')->setValue($lead_data['contact_suffix']);
            $edit_lead_form->get('opportunity_amount')->setValue($lead_data['opportunity_amount']);
            $edit_lead_form->get('lead_type')->setValue($lead_data['lead_type_id']);
            $edit_lead_form->get('lead_source')->setValue($lead_data['lead_source_id']);
            $edit_lead_form->get('campaign')->setValue($lead_data['campaign']);
            $edit_lead_form->get('status')->setValue($lead_data['status']);
            $edit_lead_form->get('estimated_time_frame')->setValue($lead_data['estimated_time_frame']);

            $activityArr = array();
            $activityArr['sourceType'] = 2;
            $activityArr['userId'] = $lead_data['user_id'];
            $activityCount = $this->getServiceLocator()->get('Activity\Model\ActivityTable')->getActivityCount($activityArr);


            $contact_name = '';
            if (isset($lead_data['contact_first_name']) and trim($lead_data['contact_first_name']) != "") {
                $contact_name .= trim($lead_data['contact_first_name']) . ' ';
            }
            if (isset($lead_data['contact_middle_name']) and trim($lead_data['contact_middle_name']) != "") {
                $contact_name .= trim($lead_data['contact_middle_name']) . ' ';
            }
            if (isset($lead_data['contact_last_name']) and trim($lead_data['contact_last_name']) != "") {
                $contact_name .= trim($lead_data['contact_last_name']) . ' ';
            }
            if (isset($lead_data['lead_source_id']) && !empty($lead_data['lead_source_id'])) {
                $lead_source = $lead_data['lead_source_id'];
            } else {
                $lead_source = '';
            }
            $get_source = $this->getServiceLocator()->get('Common\Model\CommonTable')->getLeadSource($lead_source);
            $lead_source = array();
            $lead_sourceDesc = array();
            $lead_source[''] = 'None';
            foreach ($get_source as $key => $val) {
                $lead_source[$val['lead_source_id']] = $val['lead_source'];
                $lead_sourceDesc[$val['lead_source_id']] = $val['source_description'];
            }
            $edit_lead_form->get('lead_source')->setAttribute('options', $lead_source);
            if (isset($lead_data['lead_type_id']) && !empty($lead_data['lead_type_id'])) {
                $lead_type = $lead_data['lead_type_id'];
            } else {
                $lead_type = '';
            }
            $get_type = $this->getServiceLocator()->get('Common\Model\CommonTable')->getLeadType($lead_type);
            $lead_type = array();
            $lead_typeDesc = array();
            $lead_type[''] = 'None';
            foreach ($get_type as $key => $val) {
                $lead_type[$val['lead_type_id']] = $val['lead_type'];
                $lead_typeDesc[$val['lead_type_id']] = $val['type_description'];
            }
            $edit_lead_form->get('lead_type')->setAttribute('options', $lead_type);
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'jsLangTranslate' => $lead_create_messages,
                'edit_lead_form' => $edit_lead_form,
                'lead_phone_info' => $lead_phone_info,
                'lead_address_info' => $lead_address_info,
                'lead_notes' => $lead_notes,
                'lead_id' => $lead_id,
                'encrypt_lead_id' => $params['lead_id'],
                'lead_data' => $lead_data,
                'moduleName' => $this->encrypt('lead'),
                'mode' => $mode,
                'login_user_detail' => $this->_auth->getIdentity(),
                'caseDetails' => $caseDetails,
                'encrypted_contact_name_id' => $this->encrypt($lead_data['user_id']),
                'encrypted_source_type_id' => $this->encrypt('2'),
                'totalActivityCount' => $activityCount,
                'country_list' => $country_list,
                'contactName' => $contact_name,
                'addPrivateNotePermission' => $addPrivateNotePermission,
                'viewPrivateNotePermission' => $viewPrivateNotePermission,
                'lead_typeDesc' => $lead_typeDesc,
                'lead_sourceDesc' => $lead_sourceDesc,
                'userId' => $this->auth->getIdentity()->crm_user_id,
                'tableName' => 'tbl_led_notes',
                'tableField' => 'note_id'
            ));
            return $viewModel;
        }
    }

    /**
     * This Action is used to save search      
     * @return  json
     * @param void
     * @author Icreon Tech - ST
     */
    public function leadSaveSearchAction() {
        $this->getLeadTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $lead = new Lead($this->_adapter);

        if ($request->isPost()) {
            parse_str($request->getPost('searchString'), $searchParam);
            $search_name = $request->getPost('search_name');
            $messages = array();
            parse_str($request->getPost('searchString'), $searchParam);
            $searchParam['search_title'] = $search_name;

            $searchParam = $lead->getInputFilterLeadSearch($searchParam);
            $search_name = $lead->getInputFilterLeadSearch($search_name);
            if (trim($search_name) != '') {
                $saveSearchParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $saveSearchParam['search_name'] = $search_name;
                $saveSearchParam['search_query'] = serialize($searchParam);

                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modified_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['search_id'] = $request->getPost('search_id');
                    if ($this->getLeadTable()->updateLeadSearch($saveSearchParam) == true) {
                        $messages = array('status' => "success", 'message' => $this->_config['lead_messages']['config']['lead_search']['SAVE_UPDATED_MSG']);
                        $this->flashMessenger()->addMessage($this->_config['lead_messages']['config']['lead_search']['SAVE_UPDATED_MSG']);
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {
                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    if ($this->getLeadTable()->saveLeadSearch($saveSearchParam) == true) {
                        $messages = array('status' => "success", 'message' => $this->_config['lead_messages']['config']['lead_search']['SAVE_SUCCESS_MSG']);
                        $this->flashMessenger()->addMessage($this->_config['lead_messages']['config']['lead_search']['SAVE_SUCCESS_MSG']);
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used to getSavedSearchParam
     * @return  json
     * @param void
     * @author Icreon Tech - ST
     */
    public function getSavedLeadSearchParamAction() {
        $this->getLeadTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $dataParam['is_active'] = '1';
                $dataParam['lead_search_id'] = $request->getPost('search_id');
                $seachResult = $this->getLeadTable()->getSavedSearch($dataParam);
                $seachResult = json_encode(unserialize($seachResult[0]['search_query']));
                return $response->setContent($seachResult);
            }
        }
    }

    /**
     * This Action is used to delete search
     * @return  json
     * @param void
     * @author Icreon Tech - ST
     */
    public function deleteLeadSearchAction() {

        $this->getLeadTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['search_id'] = $request->getPost('search_id');
            $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $dataParam['is_delete'] = 1;
            $dataParam['modified_date'] = DATE_TIME_FORMAT;
            $this->getLeadTable()->deleteSearch($dataParam);
            $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['search_contact']['RECORD_DELETED']);
            $this->flashMessenger()->addMessage($this->_config['lead_messages']['config']['lead_search']['SEARCH_DELETED']);
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This Action is used to delete lead
     * @return  json
     * @param void
     * @author Icreon Tech - ST
     */
    public function deleteLeadAction() {
        $this->getLeadTable();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['lead_id'] = $request->getPost('lead_id');
            $this->getLeadTable()->deleteLead($dataParam);

            /** insert into the change log */
            $changeLogArray = array();
            $changeLogArray['table_name'] = 'tbl_led_change_logs';
            $changeLogArray['activity'] = '3';/** 1 == for insert */
            $changeLogArray['id'] = $request->getPost('lead_id');
            $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $changeLogArray['added_date'] = DATE_TIME_FORMAT;
            $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
            /** end insert into the change log */
            $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['search_contact']['RECORD_DELETED']);
            $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['search_contact']['RECORD_DELETED']);
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This Action is used to see detail page of lead
     * @return  array
     * @param void
     * @author Icreon Tech - ST
     */
    public function viewLeadAction() {
        $this->layout('crm');
        $params = $this->params()->fromRoute();
        $this->getConfig();
        $this->getLeadTable();

        $response = $this->getResponse();
        $messages = array();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        if (isset($params['id']) && $params['id'] != '') {
            $lead_id = $this->decrypt($params['id']);
            if (isset($params['mode']))
                $mode = $params['mode'];
            else
                $mode = '';
            $lead_detail = $this->getLeadTable()->getLeadById($lead_id);
            $contact_first_name = isset($lead_detail['contact_first_name']) ? $lead_detail['contact_first_name'] : '';
            $contact_middle_name = isset($lead_detail['contact_middle_name']) ? $lead_detail['contact_middle_name'] : '';
            $contact_last_name = isset($lead_detail['contact_last_name']) ? $lead_detail['contact_last_name'] : '';

            $contact_name = '';
            if (isset($contact_first_name) and trim($contact_first_name) != "") {
                $contact_name .= trim($contact_first_name) . ' ';
            }
            if (isset($contact_middle_name) and trim($contact_middle_name) != "") {
                $contact_name .= trim($contact_middle_name) . ' ';
            }
            if (isset($contact_last_name) and trim($contact_last_name) != "") {
                $contact_name .= trim($contact_last_name) . ' ';
            }

            $activityArr = array();
            $activityArr['sourceType'] = 2;
            $activityArr['userId'] = $lead_detail['user_id'];
            $activityCount = $this->getServiceLocator()->get('Activity\Model\ActivityTable')->getActivityCount($activityArr);

            $viewModel->setVariables(array(
                'jsLangTranslate' => $this->_config['lead_messages']['config']['view_lead'],
                'lead_id' => $this->encrypt($lead_id),
                'mode' => $this->decrypt($mode),
                'moduleName' => $this->encrypt('lead'),
                'encrypted_contact_name_id' => $this->encrypt($lead_detail['user_id']),
                'encrypted_source_type_id' => $this->encrypt('2'),
                'totalActivityCount' => $activityCount,
                'contactName' => $contact_name
                    )
            );
            return $viewModel;
        }
    }

    /**
     * This Action is used to see detail page of leads
     * @return  array
     * @param void
     * @author Icreon Tech - ST
     */
    public function leadDetailAction() {
        $this->layout('crm');
        $params = $this->params()->fromRoute();
        $this->getConfig();
        $this->getLeadTable();

        $response = $this->getResponse();
        $messages = array();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        if (isset($params['id']) && $params['id'] != '') {
            $lead_id = $this->decrypt($params['id']);
            $lead_detail = $this->getLeadTable()->getLeadById($lead_id);
            if (isset($lead_detail['campaign_id']) && $lead_detail['campaign_id'] != "") {
                
            }
            $lead_notes = $this->getLeadTable()->getLeadNotes(array('lead_id' => $lead_id));
            $lead_address_information = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('user_id' => $lead_detail['user_id'], 'address_information_id' => ''));
            $lead_address_info = array();
            if (!empty($lead_address_information)) {
                foreach ($lead_address_information as $key => $value) {
                    if ($value['location_name'] != '' && $value['location_name'] == 1) {
                        $lead_address_info[] = $value;
                    }
                }
                if (empty($lead_address_info)) {
                    foreach ($lead_address_information as $key1 => $value1) {
                        if ($key1 == 0) {
                            $lead_address_info[] = $value1;
                        }
                    }
                }
            }

            $lead_phone_information = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations(array('user_id' => $lead_detail['user_id'], 'phone_id' => ''));
            $lead_phone_info = array();
            if (!empty($lead_phone_information)) {
                foreach ($lead_phone_information as $k => $v) {
                    if ($v['is_primary'] == 1) {
                        $lead_phone_info[] = $v;
                    }
                }

                if (empty($lead_phone_info)) {
                    foreach ($lead_phone_information as $k1 => $v1) {
                        if ($k1 == 0) {
                            $lead_phone_info[] = $v1;
                        }
                    }
                }
            }
            $caseDetails = "";
            if (!empty($lead_detail['case_contact_id'])) {
                $caseDetails = $this->getServiceLocator()->get('Cases\Model\CasesTable')->getCaseById(array($lead_detail['case_contact_id']));
            }

            /** view private note* */
            $moduleInfoArr = array();
            $moduleInfoArr[] = 'Lead';
            $moduleInfoArr[] = 'Lead';
            $moduleInfoArr[] = 'view-lead-private-note';
            $moduleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getModuleAction($moduleInfoArr);

            $auth = new AuthenticationService();

            $userDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getCrmUserDetails(array('crm_user_id' => $auth->getIdentity()->crm_user_id));

            $roleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->checkCrmUserPermission($userDetail[0]['role_id']);

            $viewPrivateNotePermission = isset($roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']]) ? $roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']] : 1;
            /** end of view private note * */
            $viewModel->setVariables(array(
                'jsLangTranslate' => $this->_config['lead_messages']['config']['view_lead'],
                'lead_detail' => $lead_detail,
                'lead_notes' => $lead_notes,
                'caseDetails' => $caseDetails,
                'lead_address_info' => $lead_address_info,
                'lead_phone_info' => $lead_phone_info,
                'viewPrivateNotePermission' => $viewPrivateNotePermission,
                'userId' => $this->auth->getIdentity()->crm_user_id,
                'tableName' => 'tbl_led_notes',
                'tableField' => 'note_id'
                    )
            );

            return $viewModel;
        }
    }

    /**
     * This Action is used to see detail page of leads
     * @return  array
     * @param void
     * @author Icreon Tech - ST
     */
    public function leadPhoneNoAction() {
        $form_phone = new ContactPhonenoForm();
        $this->getConfig();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $data['counter'] = isset($params['counter']) ? $params['counter'] : '';
        $data['container'] = isset($params['container']) ? $params['container'] : '';
        $data['divid'] = isset($params['divid']) ? $params['divid'] : '';
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $phoneArr = array();
        if (isset($request->getPost()->phoneArr) && count($request->getPost()->phoneArr) > 0) {
            $phoneArray = $request->getPost()->phoneArr;

            foreach ($phoneArray as $key => $value) {
                if ($value['is_primary'] == 1) {
                    $phoneArr[] = $value;
                }
            }
            if (empty($phoneArr)) {
                foreach ($phoneArray as $k1 => $v1) {
                    if ($k1 == 0) {
                        $phoneArr[] = $v1;
                    }
                }
            }
        }
        $viewModel->setVariables(array(
            'phoneData' => $phoneArr,
            'form_phone' => $form_phone,
            'jsLangTranslate' => $this->_config['user_messages']['config']['create_contact'],
            'params' => $data,
                )
        );


        return $viewModel;
    }

    /**
     * This Action is used to see detail page of leads
     * @return  array
     * @param void
     * @author Icreon Tech - ST
     */
    public function leadAddressFormAction() {
        $form_address = new ContactAddressesForm();
        $this->getConfig();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $data['counter'] = isset($params['counter']) ? $params['counter'] : '';
        $data['container'] = isset($params['container']) ? $params['container'] : '';
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $country_list = array();
        $country_list[''] = 'Select Country';
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }
        $form_address->get('addressinfo_country[]')->setAttribute('options', $country_list);
        $get_location_type = $this->getServiceLocator()->get('Common\Model\CommonTable')->getLocationType(array());
        $location_type = array();
        foreach ($get_location_type as $key => $val) {
            $location_type[$val['location_id']] = $val['location'];
        }
        $form_address->get('addressinfo_location_type[]')->setAttribute('options', $location_type);

        $addressArr = array();

        if (isset($request->getPost()->addressArr) && count($request->getPost()->addressArr) > 0) {
            $addressArray = $request->getPost()->addressArr;
            if (!empty($addressArray)) {
                foreach ($addressArray as $key => $value) {
                    if ($value['location_name'] != '' && $value['location_name'] == 1) {
                        $addressArr[] = $value;
                    }
                }
                if (empty($addressArr)) {
                    foreach ($addressArray as $key1 => $value1) {
                        if ($key1 == 0) {
                            $addressArr[] = $value1;
                        }
                    }
                }
            }
        }

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'addressData' => $addressArr,
            'form_address' => $form_address,
            'jsLangTranslate' => $this->_config['lead_messages']['config']['lead_create_message'],
            'params' => $data,
            'country_list' => $country_list
                )
        );

        return $viewModel;
    }

    /**
     * This function is used to get user of  autocomplete lead
     * @return     json
     * @param void
     * @author Icreon Tech - DT
     */
    public function getLeadContactAction() {
        $this->checkUserAuthentication();
        $this->getLeadTable();
        $lead = new Lead($this->_adapter);
        $response = $this->getResponse();
        $request = $this->getRequest();
        $postArr = $request->getPost()->toArray();
        $postArr['sortField'] = 'leads.modified_date';
        $postArr['sortOrder'] = 'desc';

        $getContactArr = $this->getLeadTable()->getLeadContacts($postArr);
        $allContacts = array();
        $i = 0;
        if ($getContactArr !== false) {
            foreach ($getContactArr as $contact) {
                if (!empty($contact['full_name_with_id']) || !empty($contact['company_name'])) {
                    $fullName = trim($contact['full_name_with_id']);
                    $fullName = (!empty($fullName)) ? $contact['full_name_with_id'] : $contact['company_name'];
                    $allContacts[$i] = array();
                    $allContacts[$i]['id'] = $contact['lead_id'];
                    $allContacts[$i]['full_name_with_id'] = $fullName;
                    $i++;
                }
            }
        }
        $response->setContent(\Zend\Json\Json::encode($allContacts));
        return $response;
    }

    public function contactLeadsAction() {
        $this->getLeadTable();
        $this->getConfig();
        $this->layout('crm');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $user_id = $params['userid'];
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $this->_config['lead_messages']['config']['lead_search'],
            'user_id' => $user_id,
                )
        );
        return $viewModel;
    }

    public function searchContactLeadsAction() {
        try {
            $request = $this->getRequest();

            $response = $this->getResponse();
            $messages = array();
            $isExport = $request->getPost('export');
            $params = $this->params()->fromRoute();
            parse_str($request->getPost('searchString'), $searchParam);
            $searchParam['user_id'] = $this->decrypt($params['userid']);
            $searchParam['userid'] = $this->decrypt($params['userid']);
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $searchParam['startIndex'] = $start;

            //Export to excel
            if ($isExport == "" || $isExport != "excel") {
                $searchParam['recordLimit'] = $limit;
            }
            $searchParam['sortField'] = $request->getPost('sidx');
            $searchParam['sortOrder'] = $request->getPost('sord');

            if (isset($searchParam['last_transaction']) && $searchParam['last_transaction'] != 1 && $searchParam['last_transaction'] != '') {
                $date_range = $this->getDateRange($searchParam['last_transaction']);
                $searchParam['from_date'] = $date_range['from'];
                $searchParam['to_date'] = $date_range['to'];
            }

            $get_contact_arr = $this->getLeadTable()->getLead($searchParam);
            if (!empty($get_contact_arr)) {
                $full_name = "";
                foreach ($get_contact_arr as $index => $item) {
                    if ($item['first_name'] != '')
                        $full_name = $item['first_name'];
                    if ($item['middle_name'] != '')
                        $full_name.= " " . $item['middle_name'];
                    if ($item['last_name'] != '')
                        $full_name.= " " . $item['last_name'];
                    if ($full_name == '' && $item['company_name'] != '')
                        $full_name = $item['company_name'];
                    $get_contact_arr[$index]['full_name'] = $full_name;
                }
            }

            $searchParam['startIndex'] = '';
            $searchParam['recordLimit'] = '';
            $get_contact_arr2 = $this->getLeadTable()->getLead($searchParam);
            @$countResult[0]->RecordCount = count($get_contact_arr2);

            $jsonResult = array();
            $arrCell = array();

            $subArrCell = array();

            $jsonResult['page'] = $page;
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);

            if (!empty($get_contact_arr)) {
                foreach ($get_contact_arr as $val) {
                    $view = $this->encrypt('view');
                    $edit = $this->encrypt('edit');
                    $view_link = "<a href='/view-lead/" . $this->encrypt($val['lead_id']) . "/" . $view . "' class='view-icon'>View<div class='tooltip'>View<span></span></div></a>";
                    $action = '<div class="action-col" align="center">' . $view_link /* . $edit_link . $delete_link */ . '</div>';
                    $arrCell['id'] = $val['user_id'];
                    if ($val['won_type'] != '') {
                        $lead_won = 'Yes';
                    } else {
                        $lead_won = 'No';
                    }

                    $caseContactId = '<span class="plus-sign"><a href="/create-activity/' . $this->encrypt($val['user_id']) . '/' . $this->encrypt('2') . '/' . $this->encrypt($val['lead_id']) . '"><div class="tooltip">Add activity<span></span></div></a></span>';

                    //Export to excel
                    if ($isExport == "excel") {
                        $arrExport[] = array('Name' => $val['full_name'], 'Email' => $val['email_id'], 'Type' => $val['lead_type'], 'Contact Type' => $val['lead_source'], 'Amount' => $val['opportunity_amount'], 'Converted' => $lead_won);
                    } else {
                        $arrCell['cell'] = array($caseContactId . $val['full_name'], $val['email_id'], $val['lead_type'], $val['lead_source'], $val['opportunity_amount'], $lead_won, $action);
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                }
            }
            if ($isExport == "excel") {
                $this->downloadSendHeaders("leads_data_export_" . date("Y-m-d") . ".csv");
                echo $this->arrayToCsv($arrExport);
                die();
            } else {
                $response->setContent(\Zend\Json\Json::encode($jsonResult));
                return $response;
            }
        } catch (Exception $e) {
            throw new \Exception('There is some error.');
        }
    }

}