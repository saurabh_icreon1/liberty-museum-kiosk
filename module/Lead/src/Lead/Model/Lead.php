<?php

/**
 * This is used for Lead module.
 * @package  Lead
 * @author   Icreon Tech -ST.
 */

namespace Lead\Model;

use Lead\Module;
// Add these import statements
use Base\Model\BaseModel;
use Zend\Db\Adapter\Adapter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * This is used for Lead module.
 * @package  Lead
 * @author   Icreon Tech -ST.
 */
class Lead extends BaseModel {

    protected $inputFilter;
    protected $adapter;

    /**
     * This is used for dafault initialization of objects.
     * @package    Document
     * @author     Icreon Tech -SR.
     */
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * This method is used to add filtering and validation to the form elements of create lead
     * @param void
     * @return array
     * @author Icreon Tech - DT
     */
    public function getInputFilterCreateLead() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            asd($inputFilter);
            $inputFilter->add($factory->createInput(array(
                        'name' => 'contact_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CONTACT_NAME_EMPTY',
                                    ),
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'title',
                        'required' => false,
                        'filters' => array(
                        ),
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'first_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'FIRST_NAME_EMPTY',
                                    ),
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'middle_init',
                        'required' => false,
                        'filters' => array(
                        ),
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'LAST_NAME_EMPTY',
                                    ),
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'suffix',
                        'required' => false,
                        'filters' => array(
                        ),
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_address',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'EmailAddress',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\EmailAddress::INVALID_FORMAT => "EMAIL_VALID",
                                    ),
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'country_code',
                        'required' => false,
                        'filters' => array(
                        ),
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'phoneno',
                        'required' => false,
                        'filters' => array(
                        ),
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'phone_type',
                        'required' => false,
                        'filters' => array(
                        ),
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'area_code',
                        'required' => false,
                        'filters' => array(
                        ),
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'phone_no',
                        'required' => false,
                        'filters' => array(
                        ),
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'continfo_primary',
                        'required' => false,
                        'filters' => array(
                        ),
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'opportunity_amount',
                        'required' => false,
                        'filters' => array(
                        ),
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'lead_type',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'LEAD_TYPE_EMPTY',
                                    ),
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'lead_source',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'LEAD_SOURCE_EMPTY',
                                    ),
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'status',
                        'required' => false,
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'street_address',
                        'required' => false,
                        'filters' => array(
                        ),
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_another_contact_address',
                        'required' => false,
                        'filters' => array(
                        ),
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_street_address_1',
                        'required' => false,
                        'filters' => array(
                        ),
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_city',
                        'required' => false,
                        'filters' => array(
                        ),
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_zip',
                        'required' => false,
                        'filters' => array(
                        ),
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_state',
                        'required' => false,
                        'filters' => array(
                        ),
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_country',
                        'required' => false,
                        'filters' => array(
                        ),
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'notes',
                        'required' => false,
                        'filters' => array(
                        ),
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'notes_private',
                        'required' => false,
                        'filters' => array(
                        ),
                        'validators' => array(
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'assigned_to',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'ASSIGN_TO_EMPTY',
                                    ),
                                ),
                            ),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * This method is used to convert form post array of create case to Object of class type Cases
     * @param code
     * @return Void
     * @author Icreon Tech - DT
     */
    public function exchangeArrayLead($data) {
        $parameter = array();
        $parameter['contact_name'] = (isset($data['contact_name']) && !empty($data['contact_name'])) ? $data['contact_name'] : '';
        $parameter['contact_name_id'] = (isset($data['contact_name_id']) && !empty($data['contact_name_id'])) ? $data['contact_name_id'] : '';
        $parameter['assigned_to'] = (isset($data['assigned_to']) && !empty($data['assigned_to'])) ? $data['assigned_to'] : '';
        $parameter['assigned_to_id'] = (isset($data['assigned_to_id']) && !empty($data['assigned_to_id'])) ? $data['assigned_to_id'] : '';
        $parameter['title'] = (isset($data['title']) && !empty($data['title'])) ? $data['title'] : '';
        $parameter['first_name'] = (isset($data['first_name']) && !empty($data['first_name'])) ? $data['first_name'] : '';
        $parameter['middle_name'] = (isset($data['middle_name']) && !empty($data['middle_name'])) ? $data['middle_name'] : '';
        $parameter['last_name'] = (isset($data['last_name']) && !empty($data['last_name'])) ? $data['last_name'] : '';
        $parameter['suffix'] = (isset($data['suffix']) && !empty($data['suffix'])) ? $data['suffix'] : '';
        $parameter['email_address'] = (isset($data['email_address']) && !empty($data['email_address'])) ? $data['email_address'] : '';
        $parameter['opportunity_amount'] = (isset($data['opportunity_amount']) && !empty($data['opportunity_amount'])) ? $data['opportunity_amount'] : '';
        $parameter['lead_type'] = (isset($data['lead_type']) && !empty($data['lead_type'])) ? $data['lead_type'] : '';
        $parameter['lead_source'] = (isset($data['lead_source']) && !empty($data['lead_source'])) ? $data['lead_source'] : '';
        $parameter['case_contact_id'] = (isset($data['case_contact_id']) && !empty($data['case_contact_id'])) ? $data['case_contact_id'] : '';
        $parameter['status'] = (isset($data['status']) && $data['status'] != "") ? $data['status'] : '';
        $parameter['estimated_time_frame'] = (isset($data['estimated_time_frame']) && $data['estimated_time_frame'] != "") ? $data['estimated_time_frame'] : '';
        return $parameter;
    }

    /**
     * Function used to check variables cases
     * @author Icreon Tech - DT
     * @return boolean
     * @param Array
     */
    public function exchangeArrayLeadNote($data) {
        $parameter = array();
        $parameter['note'] = (isset($data['notes']) && !empty($data['notes'])) ? $data['notes'] : '';
        $parameter['is_private'] = (isset($data['is_private']) && !empty($data['is_private'])) ? $data['is_private'] : '0';
        return $parameter;
    }

    /**
     * Function used to check variables cases
     * @author Icreon Tech - DT
     * @return boolean
     * @param Array
     */
    public function exchangeArray($data) {
        
    }

    public function filterParam($data) {
        $parameter = array();
        $parameter['contact_type'] = (isset($data['contact_type'])) ? $data['contact_type'] : '1';
        $parameter['upload_profile_image'] = (isset($data['upload_profile_image'])) ? $data['upload_profile_image'] : null;
        $parameter['first_name'] = (isset($data['first_name'])) ? $data['first_name'] : null;
        $parameter['middle_name'] = (isset($data['middle_name'])) ? $data['middle_name'] : null;
        $parameter['last_name'] = (isset($data['last_name'])) ? $data['last_name'] : null;
        $parameter['title'] = (isset($data['title'])) ? $data['title'] : null;
        $parameter['suffix'] = (isset($data['suffix'])) ? $data['suffix'] : null;
        $parameter['email_id'] = (isset($data['email_address'])) ? $data['email_address'] : null;
        $parameter['no_email_address'] = (isset($data['no_email_address'])) ? $data['no_email_address'] : '0';
        $parameter['source'] = (isset($data['source'])) ? $data['source'] : null;
        $parameter['afihc'] = (isset($data['afihc'])) ? $data['afihc'] : null;
        $parameter['webaccesss_security_question'] = (isset($data['webaccesss_security_question'])) ? $data['webaccesss_security_question'] : null;
        $parameter['webaccesss_security_answer'] = (isset($data['webaccesss_security_answer'])) ? $data['webaccesss_security_answer'] : null;
        $parameter['webaccesss_enabled_login'] = (isset($data['webaccesss_enabled_login'])) ? $data['webaccesss_enabled_login'] : '0';
        $parameter['demographics_gender'] = (isset($data['demographics_gender'])) ? $data['demographics_gender'] : null;
        $parameter['demographics_age'] = (isset($data['demographics_age'])) ? $data['demographics_age'] : null;
        $parameter['demographics_year'] = (isset($data['demographics_year'])) ? $data['demographics_year'] : null;
        $parameter['demographics_marital_status'] = (isset($data['demographics_marital_status'])) ? $data['demographics_marital_status'] : null;
        $parameter['demographics_age_limit'] = (isset($data['demographics_age_limit'])) ? $data['demographics_age_limit'] : '0';
        $parameter['demographics_income_range'] = (isset($data['demographics_income_range'])) ? $data['demographics_income_range'] : null;
        $parameter['demographics_nationality'] = (isset($data['demographics_nationality'])) ? $data['demographics_nationality'] : null;
        $parameter['demographics_ethnicity'] = (isset($data['demographics_ethnicity'])) ? $data['demographics_ethnicity'] : null;

        $parameter['salt'] = "";
        $parameter['password'] = "";
        $parameter['genrated_password'] = "";

        $parameter['add_date'] = $data['add_date'];
        $parameter['modified_date'] = $data['modified_date'];

        $parameter['user_id'] = (isset($data['user_id'])) ? $data['user_id'] : null;
        $parameter['alt_name'] = (isset($data['alt_name'])) ? $data['alt_name'] : null;
        $parameter['alt_email'] = (isset($data['alt_email'])) ? $data['alt_email'] : null;
        $parameter['alt_phoneno'] = (isset($data['alt_phoneno'])) ? $data['alt_phoneno'] : null;
        $parameter['alt_relationship'] = (isset($data['alt_relationship'])) ? $data['alt_relationship'] : null;

        $parameter['added_by'] = (isset($data['added_by'])) ? $data['added_by'] : null;
        $parameter['notes'] = (isset($data['notes'])) ? $data['notes'] : null;
        $parameter['notes_private'] = (isset($data['notes_private'])) ? $data['notes_private'] : null;

        $parameter['communicationpref_email_greetings'] = (isset($data['communicationpref_email_greetings'])) ? $data['communicationpref_email_greetings'] : null;
        $parameter['communicationpref_postal_greetings'] = (isset($data['communicationpref_postal_greetings'])) ? $data['communicationpref_postal_greetings'] : null;
        $parameter['communicationpref_address'] = (isset($data['communicationpref_address'])) ? $data['communicationpref_address'] : null;
        $parameter['communicationpref_custome_email_greetings'] = (isset($data['communicationpref_custome_email_greetings'])) ? $data['communicationpref_custome_email_greetings'] : null;
        $parameter['communicationpref_custome_postal_greetings'] = (isset($data['communicationpref_custome_postal_greetings'])) ? $data['communicationpref_custome_postal_greetings'] : null;
        $parameter['communicationpref_custome_address_greetings'] = (isset($data['communicationpref_custome_address_greetings'])) ? $data['communicationpref_custome_address_greetings'] : null;
        if (isset($data['communicationpref_preferred_method']) && count($data['communicationpref_preferred_method']) > 0) {
            $parameter['communicationpref_preferred_method'] = implode(",", $data['communicationpref_preferred_method']);
        } else {
            $parameter['communicationpref_preferred_method'] = null;
        }
        if (isset($data['communicationpref_privacy']) && count($data['communicationpref_privacy']) > 0) {
            $parameter['communicationpref_privacy'] = implode(",", $data['communicationpref_privacy']);
        } else {
            $parameter['communicationpref_privacy'] = null;
        }

        $parameter['tag_id'] = (isset($data['tag_id'])) ? $data['tag_id'] : null;
        $parameter['tagsandgroups_company_name'] = (isset($data['tagsandgroups_company_name'])) ? $data['tagsandgroups_company_name'] : null;
        $parameter['publicdocs_description'] = (isset($data['publicdocs_description'])) ? $data['publicdocs_description'] : null;
        if (isset($data['publicdocs_upload']) && count($data['publicdocs_upload']) > 0) {
            $parameter['publicdocs_upload'] = implode(",", $data['publicdocs_upload']);
        } else {
            $parameter['publicdocs_upload'] = null;
        }
        return $parameter;
    }

    /**
     * Function used to check validation for search Lead
     * @author Icreon Tech - SK
     * @return boolean
     * @param Array
     */
    public function getInputFilterLeadSearch($filterData) {
        $inputFilterData = array();
        if (is_array($filterData)) {
            foreach ($filterData as $key => $val) {
                if (!is_array($val))
                    $inputFilterData[$key] = trim(addslashes(strip_tags($val)));
            }
        } else {
            $inputFilterData = trim(addslashes(strip_tags($filterData)));
        }
        return $this->inputFilterCrmSearch = $inputFilterData;
    }

}

