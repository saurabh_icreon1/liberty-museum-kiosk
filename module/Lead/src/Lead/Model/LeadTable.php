<?php

/**
 * This page is used for Lead module database related work.
 * @package    Leads_LeadTable
 * @author     Icreon Tech - ST
 */

namespace Lead\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This page is used for Case module database related work.
 * @package    Cases_CasesTable
 * @author     Icreon Tech - DT
 */
class LeadTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

    /**
     * Function for to get conatct list     
     * @return array
     * @param array
     * @author Icreon Tech - ST
     */
    public function getContact($dataArr = null) {
        $procquesmarkapp = $this->appendQuestionMars(count($dataArr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_usr_getUser(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $dataArr['username']);
        $stmt->getResource()->bindParam(2, $dataArr['email_id']);
        $stmt->getResource()->bindParam(3, $dataArr['user_id']);
        $stmt->getResource()->bindParam(4, $dataArr['verfy_code']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (isset($resultSet['0'])) {
            return $resultSet['0'];
        } else {
            return false;
        }
    }

    /**
     * Function for to save conatct information
     * @return array
     * @param array
     * @author Icreon Tech - ST
     */
    public function getContactPhoneInformation($dataArr = null) {
        $procquesmarkapp = $this->appendQuestionMars(count($dataArr));

        if (isset($dataArr['is_primary']) and trim($dataArr['is_primary']) != "" and is_numeric(trim($dataArr['is_primary']))) {
            $isPrimary = trim($dataArr['is_primary']);
        } else {
            $isPrimary = '';
        }
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_usr_getPhoneInformations(?,?,?)");
        $stmt->getResource()->bindParam(1, $dataArr['user_id']);
        $stmt->getResource()->bindParam(2, $dataArr['phone_id']);
        $stmt->getResource()->bindParam(3, $isPrimary);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

        $statement->closeCursor();
        if (isset($resultSet)) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get conatct address
     * @return array
     * @param array
     * @author Icreon Tech - ST
     */
    public function getContactAddressInformation($dataArr = null) {

        $procquesmarkapp = $this->appendQuestionMars(count($dataArr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_usr_getAddressInformations(?,?)");
        $stmt->getResource()->bindParam(1, $dataArr['user_id']);
        $stmt->getResource()->bindParam(2, $dataArr['null_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (isset($resultSet)) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get save lead
     * @return int
     * @param array
     * @author Icreon Tech - ST
     */
    public function insertLead($params = null) {

        try {
            $procquesmarkapp = $this->appendQuestionMars(count($params));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_led_insertLead(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $params['contact_name_id']);
            $stmt->getResource()->bindParam(2, $params['assigned_to_id']);
            $stmt->getResource()->bindParam(3, $params['opportunity_amount']);
            $stmt->getResource()->bindParam(4, $params['lead_type']);
            $stmt->getResource()->bindParam(5, $params['lead_source']);
            $stmt->getResource()->bindParam(6, $params['case_contact_id']);
            $stmt->getResource()->bindParam(7, $params['status']);
            $stmt->getResource()->bindParam(8, $params['lead_reason_type_id']);
            $stmt->getResource()->bindParam(9, $params['won_type']);
            $stmt->getResource()->bindParam(10, $params['transaction_id']);
            $stmt->getResource()->bindParam(11, $params['pledge_id']);
            $stmt->getResource()->bindParam(12, $params['campaign_id']);
            $stmt->getResource()->bindParam(13, $params['added_by']);
            $stmt->getResource()->bindParam(14, $params['added_date']);
            $stmt->getResource()->bindParam(15, $params['modified_by']);
            $stmt->getResource()->bindParam(16, $params['modified_date']);
            $stmt->getResource()->bindParam(17, $params['estimated_time_frame']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            return $resultSet[0]['LAST_INSERT_ID'];
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to save lead notes
     * @return int
     * @param array
     * @author Icreon Tech - ST
     */
    public function insertLeadNote($params = null) {

        try {
            $procquesmarkapp = $this->appendQuestionMars(count($params));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_led_insertNote(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $params['lead_id']);
            $stmt->getResource()->bindParam(2, $params['note']);
            $stmt->getResource()->bindParam(3, $params['is_private']);
            $stmt->getResource()->bindParam(4, $params['added_by']);
            $stmt->getResource()->bindParam(5, $params['added_date']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet[0]['LAST_INSERT_ID'];
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert contact address info
     * @return boolean
     * @param array
     * @author Icreon Tech - ST
     */
    public function insertAddressInfo($params = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_led_insertAddressInformation(?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['lead_id']);
            $stmt->getResource()->bindParam(2, $params['addressinfo_location_type']);
            $stmt->getResource()->bindParam(3, $params['addressinfo_location']);
            $stmt->getResource()->bindParam(4, $params['addressinfo_street_address_1']);
            $stmt->getResource()->bindParam(5, $params['addressinfo_street_address_2']);
            $stmt->getResource()->bindParam(6, $params['addressinfo_city']);
            $stmt->getResource()->bindParam(7, $params['addressinfo_state']);
            $stmt->getResource()->bindParam(8, $params['addressinfo_zip']);
            $stmt->getResource()->bindParam(9, $params['addressinfo_country']);
            $stmt->getResource()->bindParam(10, $params['add_date']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert phone information
     * @return array
     * @param array
     * @param string
     * @author Icreon Tech - ST
     */
    public function insertPhoneInformations($params = array(), $usp_name = 'usp_led_insertPhoneInformation') {
        try {

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL ' . $usp_name . '(?,?,?,?,?,?,?,?)');
            if (isset($params['user_id'])) {
                $stmt->getResource()->bindParam(1, $params['user_id']);
            } else {
                $stmt->getResource()->bindParam(1, $params['lead_id']);
            }
            $stmt->getResource()->bindParam(2, $params['phone_type']);
            $stmt->getResource()->bindParam(3, $params['country_code']);
            $stmt->getResource()->bindParam(4, $params['area_code']);
            $stmt->getResource()->bindParam(5, $params['phone_no']);
            $stmt->getResource()->bindParam(6, $params['continfo_primary']);
            $stmt->getResource()->bindParam(7, $params['added_date']);
            $stmt->getResource()->bindParam(8, $params['modified_date']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert contact 
     * @return int
     * @param array	 
     * @author Icreon Tech - ST
     */
    public function insertContact($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_insertContact(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $params['contact_type']);
        $stmt->getResource()->bindParam(2, $params['title']);
        $stmt->getResource()->bindParam(3, $params['first_name']);
        $stmt->getResource()->bindParam(4, $params['middle_name']);
        $stmt->getResource()->bindParam(5, $params['last_name']);
        $stmt->getResource()->bindParam(6, $params['suffix']);
        $stmt->getResource()->bindParam(7, $params['email_id']);
        $stmt->getResource()->bindParam(8, $params['no_email_address']);
        $stmt->getResource()->bindParam(9, $params['password']);
        $stmt->getResource()->bindParam(10, $params['salt']);
        $stmt->getResource()->bindParam(11, $params['demographics_year']);
        $stmt->getResource()->bindParam(12, $params['demographics_gender']);
        $stmt->getResource()->bindParam(13, $params['upload_profile_image']);
        $stmt->getResource()->bindParam(14, $params['demographics_marital_status']);
        $stmt->getResource()->bindParam(15, $params['demographics_age_limit']);
        $stmt->getResource()->bindParam(16, $params['demographics_age']);
        $stmt->getResource()->bindParam(17, $params['demographics_income_range']);
        $stmt->getResource()->bindParam(18, $params['demographics_ethnicity']);
        $stmt->getResource()->bindParam(19, $params['demographics_nationality']);
        $stmt->getResource()->bindParam(20, $params['source']);
        $stmt->getResource()->bindParam(21, $params['webaccesss_security_question']);
        $stmt->getResource()->bindParam(22, $params['webaccesss_security_answer']);
        $stmt->getResource()->bindParam(23, $params['webaccesss_enabled_login']);
        $stmt->getResource()->bindParam(24, $params['add_date']);
        $stmt->getResource()->bindParam(25, $params['modified_date']);
        $stmt->getResource()->bindParam(26, $params['corporate_company_name']);
        $stmt->getResource()->bindParam(27, $params['corporate_legal_name']);
        $stmt->getResource()->bindParam(28, $params['corporate_sic_code']);
        $result = $stmt->execute();

        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            return false;
        }
    }

    /**
     * Function for to insert contact address info
     * @return boolean
     * @param array
     * @param string
     * @author Icreon Tech - ST
     */
    public function insertConatctAddressInfo($params = array(), $usp_name = 'usp_usr_insertAddressInformations') {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL ' . $usp_name . '(?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['addressinfo_location_type']);
            $stmt->getResource()->bindParam(3, $params['addressinfo_location']);
            $stmt->getResource()->bindParam(4, $params['addressinfo_street_address_1']);
            $stmt->getResource()->bindParam(5, $params['addressinfo_street_address_2']);
            $stmt->getResource()->bindParam(6, $params['addressinfo_city']);
            $stmt->getResource()->bindParam(7, $params['addressinfo_state']);
            $stmt->getResource()->bindParam(8, $params['addressinfo_zip']);
            $stmt->getResource()->bindParam(9, $params['addressinfo_country']);
            $stmt->getResource()->bindParam(10, $params['add_date']);
            $stmt->getResource()->bindParam(11, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for bind field for call to stored procedure
     * @return void
     * @param array
     * @param array
     * @author Icreon Tech - ST
     */
    public function bindFieldArray($stmt, $fields_value_arr) {
        $i = 1;
        foreach ($fields_value_arr as $field_value) {
            $stmt->getResource()->bindParam($i, $field_value);
            $i++;
        }
    }

    /**
     * Function for append number of questions marks for stored procedure    
     * @return string
     * @param array
     * @author Icreon Tech - ST
     */
    public function appendQuestionMars($count) {
        $returnstr = '';
        for ($i = 1; $i <= $count; $i++) {
            $returnstr.='?,';
        }
        return rtrim($returnstr, ',');
    }

    /**
     * Function for to get lead  
     * @return array
     * @param array
     */
    public function getLead($dataArr = null) {
        if (isset($dataArr['user_email']) && $dataArr['user_email'] != '') {
            $user_email = addslashes($dataArr['user_email']);
        } else {
            $user_email = '';
        }
        if (isset($dataArr['lead_status']) && $dataArr['lead_status'] != '') {
            $lead_status = $dataArr['lead_status'];
        } else {
            $lead_status = '';
        }
        if (isset($dataArr['lead_type']) && $dataArr['lead_type'] != '') {
            $lead_type = $dataArr['lead_type'];
        } else {
            $lead_type = '';
        }
        if (isset($dataArr['lead_source']) && $dataArr['lead_source'] != '') {
            $lead_source = $dataArr['lead_source'];
        } else {
            $lead_source = '';
        }
        if (isset($dataArr['lead_converted']) && $dataArr['lead_converted'] != '') {
            $lead_converted = $dataArr['lead_converted'];
        } else {
            $lead_converted = '';
        }
        
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;

        $start_index = (isset($dataArr['startIndex']) && $dataArr['startIndex'] != '') ? $dataArr['startIndex'] : '';
        $record_limit = (isset($dataArr['recordLimit']) && $dataArr['recordLimit'] != '') ? $dataArr['recordLimit'] : '';
        $sort_field = (isset($dataArr['sortField']) && $dataArr['sortField'] != '') ? $dataArr['sortField'] : '';
        $sort_order = (isset($dataArr['sortOrder']) && $dataArr['sortOrder'] != '') ? $dataArr['sortOrder'] : '';
        $fromDate = (isset($dataArr['from_date']) && $dataArr['from_date'] != '') ? $dataArr['from_date'] : '';
        $toDate = (isset($dataArr['to_date']) && $dataArr['to_date'] != '') ? $dataArr['to_date'] : '';
        $top = (isset($dataArr['top']) && $dataArr['top'] != '') ? $dataArr['top'] : '';

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_led_getLeads(?,?,?,?,?,?,?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $user_email);
        $stmt->getResource()->bindParam(2, $lead_status);
        $stmt->getResource()->bindParam(3, $lead_type);
        $stmt->getResource()->bindParam(4, $lead_source);
        $stmt->getResource()->bindParam(5, $lead_converted);
        $stmt->getResource()->bindParam(6, $user_id);
        $stmt->getResource()->bindParam(7, $start_index);
        $stmt->getResource()->bindParam(8, $record_limit);
        $stmt->getResource()->bindParam(9, $sort_field);
        $stmt->getResource()->bindParam(10, $sort_order);
        $stmt->getResource()->bindParam(11, $fromDate);
        $stmt->getResource()->bindParam(12, $toDate);
        $stmt->getResource()->bindParam(13, $top);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get lead contacts
     * @return array
     * @param array
     */
    public function getLeadContacts($dataArr = null) {
        if (isset($dataArr['lead_user_name']) && $dataArr['lead_user_name'] != '') {
            $lead_user_name = strtolower($dataArr['lead_user_name']);
        } else {
            $lead_user_name = '';
        }
        $sort_field = (isset($dataArr['sortField']) && $dataArr['sortField'] != '') ? $dataArr['sortField'] : '';
        $sort_order = (isset($dataArr['sortOrder']) && $dataArr['sortOrder'] != '') ? $dataArr['sortOrder'] : '';

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_led_getLeadContacts(?,?,?)');
        $stmt->getResource()->bindParam(1, $lead_user_name);
        $stmt->getResource()->bindParam(2, $sort_field);
        $stmt->getResource()->bindParam(3, $sort_order);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();

        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for get lead data on the base of id
     * @return int
     * @param array
     * @author Icreon Tech - ST    
     */
    public function getLeadById($lead_data) {
        $procquesmarkapp = $this->appendQuestionMars(count($lead_data));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_led_getLeadDetails(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $lead_data);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }

    /**
     * Function for to get lead address info
     * @return int
     * @param array
     * @author Icreon Tech - ST    
     */
    public function getLeadAddressInformations($params = array()) {
        try {
            $user_id = isset($params['user_id']) ? $params['user_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_led_getAddressInformations(?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get lead PhoneInformations
     * @return array
     * @param array
     * @author Icreon Tech - ST    
     */
    public function getLeadPhoneInformations($params = array()) {
        try {
            $lead_id = isset($params['lead_id']) ? $params['lead_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_led_getPhoneInformations(?)');
            $stmt->getResource()->bindParam(1, $lead_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get notes
     * @return array
     * @param array
     * @author Icreon Tech - ST    
     */
    public function getLeadNotes($params = array()) {
        try {
            $lead_id = isset($params['lead_id']) ? $params['lead_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_led_getNotes(?)');
            $stmt->getResource()->bindParam(1, $lead_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get conatct address
     * @return boolean
     * @param array
     * @author Icreon Tech - ST    
     */
    public function updateLead($params = null) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($params));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_led_updateLead(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $params['lead_id']);
            $stmt->getResource()->bindParam(2, $params['contact_name_id']);
            $stmt->getResource()->bindParam(3, $params['assigned_to_id']);
            $stmt->getResource()->bindParam(4, $params['opportunity_amount']);
            $stmt->getResource()->bindParam(5, $params['lead_type']);
            $stmt->getResource()->bindParam(6, $params['lead_source']);
            $stmt->getResource()->bindParam(7, $params['status']);
            $stmt->getResource()->bindParam(8, $params['lead_reason_type_id']);
            $stmt->getResource()->bindParam(9, $params['won_type']);
            $stmt->getResource()->bindParam(10, $params['transaction_id']);
            $stmt->getResource()->bindParam(11, $params['pledge_id']);
            $stmt->getResource()->bindParam(12, $params['campaign_id']);
            $stmt->getResource()->bindParam(13, $params['modified_by']);
            $stmt->getResource()->bindParam(14, $params['modified_date']);
            $stmt->getResource()->bindParam(15, $params['estimated_time_frame']);
            $result = $stmt->execute();
            $statement = $result->getResource();

            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            asd($e->getTrace());
        }
    }

    /**
     * Function used to delete lead details
     * @return boolean
     * @param array
     * @author Icreon Tech - ST    
     */
    public function deleteLeadDetails($params = array()) {
        try {
            $lead_id = isset($params['lead_id']) ? $params['lead_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_led_deleteLeadDetails(?)');
            $stmt->getResource()->bindParam(1, $lead_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to update lead search
     * @return boolean
     * @param array
     * @author Icreon Tech - ST 
     */
    public function updateLeadSearch($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_led_updateLeadSearch(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['crm_user_id']);
            $stmt->getResource()->bindParam(2, $params['search_name']);
            $stmt->getResource()->bindParam(3, $params['search_query']);
            $stmt->getResource()->bindParam(4, $params['modifiend_date']);
            $stmt->getResource()->bindParam(5, $params['search_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert lead search
     * @return boolean
     * @param array
     * @author Icreon Tech - ST 
     */
    public function saveLeadSearch($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_led_saveLeadSearch(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['crm_user_id']);
            $stmt->getResource()->bindParam(2, $params['search_name']);
            $stmt->getResource()->bindParam(3, $params['search_query']);
            $stmt->getResource()->bindParam(4, $params['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to delete saved search
     * @return void
     * @param array
     * @author Icreon Tech - ST 
     */
    public function deleteSearch($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_led_deleteLeadSearch(?,?,?,?)');
        $stmt->getResource()->bindParam(1, $params['crm_user_id']);
        $stmt->getResource()->bindParam(2, $params['is_delete']);
        $stmt->getResource()->bindParam(3, $params['modified_date']);
        $stmt->getResource()->bindParam(4, $params['search_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for to get saved lead search
     * @return array
     * @param array
     * @author Icreon Tech - ST 
     */
    public function getSavedSearch($params = array()) {

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_led_getSavedSearch(?,?,?)');
        $stmt->getResource()->bindParam(1, $params['crm_user_id']);
        $stmt->getResource()->bindParam(2, $params['is_active']);
        $stmt->getResource()->bindParam(3, $params['lead_search_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to delete lead
     * @return boolean
     * @param array
     * @author Icreon Tech - ST 
     */
    public function deleteLead($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_led_deleteLead(?)');
            $stmt->getResource()->bindParam(1, $params['lead_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /* Function for get lead count
     * @author Icreon Tech- SK
     * param void
     * return array
     */

    public function getLeadCount($param) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getLeadCount(?)");
        $stmt->getResource()->bindParam(1, $param['user_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['totalRecord'];
        } else {
            return false;
        }
    }

}

