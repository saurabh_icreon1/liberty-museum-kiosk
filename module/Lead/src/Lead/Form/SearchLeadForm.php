<?php

/**
 * This form is used tof search contact for admin
 * @category   Zend
 * @package    User_SearchContactForm
 * @author     Icreon Tech - AS
 */

namespace Lead\Form;

use Zend\Form\Form;

class SearchLeadForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'user_email',
            'attributes' => array(
                'type' => 'text',
                'id' => 'user_email',
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'lead_status',
            'options' => array(
                'value_options' => array(
                    '' => 'Any',
                    '' => 'All',
                    '1' => 'Open',
                    '0' => 'Closed'
                ),
            ),
            'attributes' => array(
                'id' => 'lead_status',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'lead_type',
            'options' => array(
                'value_options' => array(
                    '' => 'Any'
                ),
            ),
            'attributes' => array(
                'id' => 'lead_type',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'lead_source',
            'options' => array(
                'value_options' => array(
                    '' => 'Any',
                    '' => 'All',
					'2' => 'Campaign',
                    '0' => 'Email',
                    '1' => 'Phone',
                    
                ),
            ),
            'attributes' => array(
                'id' => 'lead_source',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'lead_converted',
            'checked_value' => '1',
            'unchecked_value' => '',
            'attributes' => array(
                'id' => 'lead_converted',
                'class'=> 'e2'
            ),
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'submitbutton',
				'class'=>"search-btn"
            ),
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'saved_search',
            'options' => array(
                'value_options' => array(
                    
                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e1 select-w-320',
                'id' => 'saved_search',
                'onChange' =>'getSavedSearchResult();'
            ),
        ));
    }

}