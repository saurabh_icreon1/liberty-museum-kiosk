<?php

/**
 * This form is used for create address section in individual type in contact.
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace Lead\Form;

use Zend\Form\Form;

/**
 * This class is used for create address form elemts .
 * @package    User
 * @author     Icreon Tech - AP
 */
class ContactAddressesForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('street_address');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'addressinfo_location_type[]',
            'options' => array(
                'value_options' => array(
                    '0' => 'Home',
                    '1' => 'Work',
                    '2' => 'Vacation',
                    '3' => 'Web'
                ),
            ),
            'attributes' => array(
                'id' => 'addressinfo_location_type',
                'class' => 'e1',
            )
        ));
        $this->add(array(
            'type' => 'MultiCheckbox',
            'name' => 'addressinfo_location[]',
            'options' => array(
                'value_options' => array(
                    '1' => 'Primary Location',
                    '2' => 'Billing Location',
                    '3' => 'Shipping Location'
                ),
                'use_hidden_element' => false
            ),
            'attributes' => array(
                'value' => '', /* set checked to '1' */
                'class' => 'e4 addressinfo_location',
                'id' => 'addressinfo_location'
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'addressinfo_location_primary[]',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'options' => array(
                'use_hidden_element' => false,
            ),
            'attributes' => array(
                'value' => '', /* set checked to '1' */
                'class' => 'e4 addressinfo_location_primary',
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'addressinfo_location_billing[]',
            'checked_value' => '2',
            'unchecked_value' => '0',
            'options' => array(
                'use_hidden_element' => false
            ),
            'attributes' => array(
                'value' => '2', /* set checked to '1' */
                'class' => 'e4 addressinfo_location_billing',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'addressinfo_location_shipping[]',
            'checked_value' => '3',
            'unchecked_value' => '0',
            'attributes' => array(
                'value' => '3', /* set checked to '1' */
                'class' => 'e4 addressinfo_location_shipping',
            ),
            'options' => array(
                'use_hidden_element' => false
            ),
        ));

        $this->add(array(
            'name' => 'address_contact_contact_type[]',
            'attributes' => array(
                'type' => 'hidden',
            // 'class' => 'addressinfo_street_address_1'
            )
        ));



        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'addressinfo_another_contact_address[]',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'class' => 'left address-checkbox'
            ),
        ));
        $this->add(array(
            'name' => 'addressinfo_street_address_1[]',
            'attributes' => array(
                'type' => 'text',
                'class' => 'addressinfo_street_address_1',
                'id' => 'addressinfo_street_address_1'
            )
        ));
        $this->add(array(
            'name' => 'addressinfo_street_address_2[]',
            'attributes' => array(
                'type' => 'text',
                'class' => 'addressinfo_street_address_2'
            )
        ));
        $this->add(array(
            'name' => 'addressinfo_street_address_3[]',
            'attributes' => array(
                'type' => 'text',
            )
        ));
        $this->add(array(
            'name' => 'addressinfo_city[]',
            'attributes' => array(
                'type' => 'text',
                'class' => 'addressinfo_city',
                'id' => 'addressinfo_city'
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'addressinfo_state[]',
            'attributes' => array(
                'class' => 'addressinfo_state',
                'id' => 'addressinfo_state',
                'value' => '' /* set selected to 'blank' */                
            )
        ));
        $this->add(array(
            'name' => 'addressinfo_zip[]',
            'attributes' => array(
                'type' => 'text',
                'class' => 'addressinfo_zip',
                'id' => 'addressinfo_zip'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'addressinfo_country[]',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'addressinfo_country',
                'class' => 'e1 addressinfo_country',
                'value' => '' /* set selected to 'blank' */
            )
        ));

        $this->add(array(
            'name' => 'addressinfo_search_contact[]',
            'attributes' => array(
                'type' => 'text',
                'class' => "search-icon search_contact"
            )
        ));
    }

}