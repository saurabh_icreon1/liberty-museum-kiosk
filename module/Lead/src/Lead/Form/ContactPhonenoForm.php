<?php

/**
 * This form is used for create phone no in contact
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace Lead\Form;

use Zend\Form\Form;

/**
 * This class is used for create phone no from
 * @package    User
 * @author     Icreon Tech - AP
 */
class ContactPhonenoForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('phoneno');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'country_code[]',
            'attributes' => array(
                'type' => 'text',
                'class' => 'm-l-20 width-125 m-r-20',
                'id' => 'country_code'
            )
        ));

        $this->add(array(
            'name' => 'area_code[]',
            'attributes' => array(
                'type' => 'text',
                'class' => 'width-80 m-r-20',
                'id' => 'area_code'
            )
        ));
        $this->add(array(
            'name' => 'phone_no[]',
            'attributes' => array(
                'type' => 'text',
                'class' => 'phone_no',
                'id' => 'phone_no',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'phone_type[]',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    'board' => 'Board',
                    'cell' => 'Cell',
                    'home' => 'Home',
                    'work' => 'Work',
                    'fax' => 'Fax',
                    'direct' => 'Direct',
                    'other' => 'Others'
                ),
            ),
            'attributes' => array(
                'id' => 'phone_type',
                'class' => 'e1 left select-w-150 phone-type',
                'value' => '' /* set selected to 'blank' */
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'continfo_primary[]',
            'options' => array(
                'value_options' => array(
                    '0' => 'Primary',
                ),
            ),
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'value' => '1', /* set checked to '1' */
                'class' => 'e3',
            )
        ));

        $this->add(array(
            'name' => 'phone_index',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'phone_index'
            )
        ));
    }

}