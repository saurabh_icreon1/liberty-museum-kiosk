<?php

/**
 * This form is used for Edit Lead.
 * @category   Zend
 * @package    Group_SurveyForm
 * @version    2.2
 * @author     Icreon Tech - SK
 */

namespace Lead\Form;

use Zend\Form\Form;

class EditLeadForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('create-lead');
        $this->setAttribute('method', 'post');


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'lead_reason_type',
            'options' => array(
                'value_options' => array(
                    '1' => 'Come Back With Us',
                    '1' => 'Not Intrested',
                    '2' => 'Won'
                ),
            ),
            'attributes' => array(
                'id' => 'lead_reason_type',
                'class' => 'e1',
                'value' => '', //set selected to 'blank'
                'onChange' => 'changeReasonOption(this.value)'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'lead_reason',
            'options' => array(
                'value_options' => array(
                    '1' => 'Transaction',
                    '2' => 'Pledge',
                ),
            ),
            'attributes' => array(
                'value' => '0',
                'class' => 'e3 lead_reason',
                'onChange' => 'changeReasonType(this.value)'
            )
        ));
        $this->add(array(
            'name' => 'transaction',
            'attributes' => array(
                'type' => 'text',
                'id' => 'transaction'
            )
        ));
        $this->add(array(
            'name' => 'transaction_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'transaction_id'
            )
        ));
        $this->add(array(
            'name' => 'pledge',
            'attributes' => array(
                'type' => 'text',
                'id' => 'pledge'
            )
        ));
        $this->add(array(
            'name' => 'pledge_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'pledge_id'
            )
        ));
        $this->add(array(
            'name' => 'reason_continue',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'reason_continue',
                'class' => 'save-btn',
                'value' => 'CONTINUE'
            )
        ));
        $this->add(array(
            'name' => 'campaign',
            'attributes' => array(
                'type' => 'text',
                'id' => 'campaign',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'campaign_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'campaign_id'
            )
        ));




        $this->add(array(
            'name' => 'contact_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'contact_name'
            )
        ));
        $this->add(array(
            'name' => 'contact_name_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'contact_name_id'
            )
        ));

        $this->add(array(
            'name' => 'assigned_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'assigned_to',
            )
        ));
        $this->add(array(
            'name' => 'assigned_to_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'assigned_to_id'
            )
        ));


        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name'
            )
        ));

        $this->add(array(
            'name' => 'middle_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'middle_name',
            )
        ));
        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name',
            )
        ));

        $this->add(array(
            'name' => 'email_address',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_address'
            )
        ));

        $this->add(array(
            'name' => 'email_address',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_address'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'title',
            'options' => array(
                'value_options' => array(
                    '' => 'None',
                    '1' => 'Mr.',
                    '2' => 'Mrs.',
                    '3' => 'Ms.',
                ),
            ),
            'attributes' => array(
                'class' => 'e1',
                'id' => 'title',
                'value' => '' //set selected to 'blank'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'suffix',
            'options' => array(
                'value_options' => array(
                    '' => 'None',
                    '0' => 'Jr.',
                    '1' => 'Sr.',
                    '2' => 'II',
                ),
            ),
            'attributes' => array(
                'class' => 'e1',
                'id' => 'suffix',
                'value' => '' //set selected to 'blank'
            )
        ));
        $this->add(array(
            'name' => 'opportunity_amount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'opportunity_amount',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'lead_type',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '0' => 'Cold',
                    '1' => 'Hot',
                    '2' => 'Warm'
                ),
            ),
            'attributes' => array(
                'class' => 'e1',
                'id' => 'lead_type',
                'value' => '' //set selected to 'blank'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'lead_source',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '0' => 'Email',
                    '1' => 'Phone',
                    '2' => 'Campaign',
                ),
            ),
            'attributes' => array(
                'class' => 'e1',
                'id' => 'lead_source',
                'value' => '', //set selected to 'blank'
                'onChange' => 'changeLeadSource(this.value)'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'status',
            'options' => array(
                'value_options' => array(
                    '1' => 'Open',
                    '0' => 'Closed',
                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e3 status m-r-10',
                'onClick' => 'changeReason(this.value)'
            )
        ));

        $this->add(array(
            'name' => 'notes',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'notes',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_private',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'value' => '', //set checked to '1'
                'class' => 'e2',
                'id' => 'is_private'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'addressinfo_another_contact_address1[]',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'addressinfo_another_contact_address',
                'class' => 'e4'
            ),
        ));
        $this->add(array(
            'name' => 'continue',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'continue',
                'class' => 'save-btn',
                'value' => 'UPDATE'
            )
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-20',
                'value' => 'CANCEL'
            )
        ));
        $this->add(array(
            'name' => 'addressinfo_search_contact[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'addressinfo_search_contact',
                'class' => "search-icon "
            )
        ));
        $this->add(array(
            'name' => 'lead_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'lead_id',
            ),
        ));
        $this->add(array(
            'name' => 'estimated_time_frame',
            'attributes' => array(
                'type' => 'text',
                'id' => 'estimated_time_frame'
            )
        ));
    }

}
