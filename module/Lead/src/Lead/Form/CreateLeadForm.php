<?php

/**
 * This form is used for Create Lead.
 * @package    Lead_CreateLeadForm
 * @author     Icreon Tech - AP
 */
 
namespace Lead\Form;

use Zend\Form\Form;

class CreateLeadForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('create-lead');
        $this->setAttribute('method', 'post');
	 $this->add(array(
            'name' => 'contact_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'contact_name'
            )
        ));
        $this->add(array(
            'name' => 'contact_name_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'contact_name_id'
            )
        ));

        $this->add(array(
            'name' => 'assigned_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'assigned_to',
            )
        ));
       $this->add(array(
            'name' => 'assigned_to_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'assigned_to_id'
            )
        ));


        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'firstt_name'
            )
        ));

        $this->add(array(
            'name' => 'middle_init',
            'attributes' => array(
                'type' => 'text',
                'id' => 'middle_init',
            )
        ));
        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name',
            )
        ));

        $this->add(array(
            'name' => 'email_address',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_address'
            )
        ));

//        $this->add(array(
//            'name' => 'country_code',
//            'attributes' => array(
//                'type' => 'text',
//                'id' => 'country_code',
//            )
//        ));
//        $this->add(array(
//            'name' => 'area_code',
//            'attributes' => array(
//                'type' => 'text',
//                'id' => 'area_code',
//            )
//        ));
//        $this->add(array(
//            'name' => 'phone_no',
//            'attributes' => array(
//                'type' => 'text',
//                'id' => 'phone_no',
//            )
//        ));
//        $this->add(array(
//            'type' => 'Zend\Form\Element\Radio',
//            'name' => 'primary',
//            'options' => array(
//                'value_options' => array(
//                    '1' => 'Primary',
//                  ),
//            ),
//            'attributes' => array(
//                'value' => ''
//            )
//        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'title',	
            'options' => array(				
                'value_options'=> array(
                        ''=>'None',
                        '0'=>'Mr.',
                        '1'=>'Mrs.',
                        '2'=>'Ms.',
               ),
            ),
            'attributes' => array(
                'id' => 'title',            
           )
        ));
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'suffix',	
            'options' => array(				
                'value_options'=> array(
                        ''=>'None',
                        '0'=>'Jr.',
                        '1'=>'Sr.',
                        '2'=>'II',
               ),
            ),
            'attributes' => array(
                'class' => 'e1',
                'id' => 'suffix',
                'value' => '' //set selected to 'blank'
            )
         ));
//        $this->add(array(
//           'type' => 'Zend\Form\Element\Select',
//            'name' => 'phone_type',	
//            'options' => array(				
//                'value_options'=> array(
//                        '0'=>'Home',
//                        '1'=>'Work',
//                        '2'=>'Mobile',
//                        '3'=>'Fax',
//                        '4'=>'Other'
//               ),
//            ),
//          'attributes' => array(
//            'id' => 'phone_type',            
//           )
//     ));
      $this->add(array(
        'name' => 'opportunity_amount',
        'attributes' => array(
            'type' => 'text',
            'id' => 'opportunity_amount',
        )
     ));
     $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'lead_type',	
            'options' => array(				
                'value_options'=> array(
                        ''=>'Select',
                        '0'=>'Cold',
                        '1'=>'Hot',
                        '2'=>'Warm'
               ),
            ),
          'attributes' => array(
            'id' => 'lead_type',
            
           )
     ));
     $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'lead_source',	
            'options' => array(				
                'value_options'=> array(
                        ''=>'Select',
                        '0'=>'Email',
                        '1'=>'Phone',
                        '2'=>'Campaign',
                  ),
            ),
          'attributes' => array(
            'id' => 'lead_source',
            
           )
     ));

     $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'status',
            'options' => array(
                'value_options' => array(
                    '1' => 'Open',
                    '2' => 'Closed',
                ),
            ),
            'attributes' => array(
                'value' => ''
            )
     ));
//   
//    $this->add(array(
//       'type' => 'Zend\Form\Element\Select',
//        'name' => 'addressinfo_location_type',	
//        'options' => array(				
//            'value_options'=> array(
//
//           ),
//        ),
//      'attributes' => array(
//        'id' => 'addressinfo_location_type',
//       )
//    ));
//    $this->add(array(
//                'type' => 'MultiCheckbox',
//                'name' => 'addressinfo_location',	
//                'options' => array(				
//                    'value_options'=> array(
//                            '0'=>'Primary Location',
//                            '1'=>'Billing Location',
//                            '2'=>'Shipping Location',
//                    ),
//                ),
//      ));
//      $this->add(array(
//            'type' => 'Zend\Form\Element\Checkbox',
//            'name' => 'use_another_contact_address',
//            'checked_value' => '1',
//            'unchecked_value' => '0',
//            'class'=>'checkbox'        
//        ));
//      $this->add(array(
//        'name' => 'addressinfo_address',
//        'attributes' => array(
//            'type' => 'text',
//            'id' => 'addressinfo_address',
//        )
//     ));
//     $this->add(array(
//        'name' => 'addressinfo_state',
//        'attributes' => array(
//            'type' => 'text',
//            'id' => 'addressinfo_state',
//        )
//     ));
//     $this->add(array(
//        'name' => 'addressinfo_city',
//        'attributes' => array(
//            'type' => 'text',
//            'id' => 'addressinfo_city',
//        )
//     ));
//     $this->add(array(
//        'name' => 'addressinfo_zip',
//        'attributes' => array(
//            'type' => 'text',
//            'id' => 'addressinfo_zip',
//        )
//     ));
//     $this->add(array(
//       'type' => 'Zend\Form\Element\Select',
//        'name' => 'addressinfo_country',	
//        'options' => array(				
//            'value_options'=> array(
//
//           ),
//        ),
//      'attributes' => array(
//        'id' => 'addressinfo_country',
//       )
//    ));
    $this->add(array(
        'name' => 'notes',
        'attributes' => array(
            'type' => 'textarea',
            'id' => 'notes',
        )
     ));
     $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'notes_private',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'class'=>'e2',
            'id'=>'is_private',
			
     ));
   
    $this->add(array(
        'name' => 'continue',
        'attributes' => array(
            'type' => 'submit',
            'id' => 'continue',
            'class' => 'save-btn',
            'value' => 'SAVE'
        )
    ));
    $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-20',
                'value' => 'CANCEL'
            )
   ));
}
}
