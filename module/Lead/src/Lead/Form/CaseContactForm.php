<?php

/**
 * This form is used to save cash converted information to lead
 * @category   Zend
 * @package    Lead_CaseContactForm
 * @author     Icreon Tech - SK
 */

namespace Lead\Form;

use Zend\Form\Form;

class CaseContactForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('case_contact');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'case_contact',
            'attributes' => array(
                'type' => 'text',
                'id' => 'case_contact',
                'onclick' => 'alert("ok")'
            )
        ));
        $this->add(array(
            'name' => 'case_contact_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'case_contact_id'
            )
        ));
        $this->add(array(
            'name' => 'case_subject',
            'attributes' => array(
                'type' => 'text',
                'id' => 'case_subject'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'case_type',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e1 select-w-320',
                'id' => 'case_type',
            ),
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'case_status',
            'options' => array(
                'value_options'=> array(

               ),
            ),
            'attributes' => array(
                'id' => 'case_status',
                'class' => 'e1'
           )
        ));

    }

}