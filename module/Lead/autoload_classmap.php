<?php 
return array(
    'Lead\Module'                       => __DIR__ . '/Module.php',
    'Lead\Controller\LeadController'   => __DIR__ . '/src/Lead/Controller/LeadController.php',
    'Lead\Model\Lead'            => __DIR__ . '/src/Lead/Model/Lead.php',
    'Lead\Model\LeadTable'                   => __DIR__ . '/src/Lead/Model/LeadTable.php'
);
?>