<?php

/**
 * This page is used for promotion module configuration details.
 * @package    Promotion
 * @author     Icreon Tech - DT
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Promotions\Controller\Promotion' => 'Promotions\Controller\PromotionController'
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'addCrmPromotion' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-promotion',
                    'defaults' => array(
                        'controller' => 'Promotions\Controller\Promotion',
                        'action' => 'addCrmPromotion',
                    ),
                ),
            ),
            'getCrmPromotion' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-promotion',
                    'defaults' => array(
                        'controller' => 'Promotions\Controller\Promotion',
                        'action' => 'getCrmPromotion',
                    ),
                ),
            ),
            'savePromotionSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-promotion-search',
                    'defaults' => array(
                        'controller' => 'Promotions\Controller\Promotion',
                        'action' => 'savePromotionSearch',
                    ),
                ),
            ),
            'getPromotionSearchSavedParam' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-promotion-search-saved-param',
                    'defaults' => array(
                        'controller' => 'Promotions\Controller\Promotion',
                        'action' => 'getPromotionSearchSavedParam',
                    ),
                ),
            ),
            'deleteCrmSearchPromotion' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-search-promotion',
                    'defaults' => array(
                        'controller' => 'Promotions\Controller\Promotion',
                        'action' => 'deleteCrmSearchPromotion',
                    ),
                ),
            ),
            'deleteCrmPromotion' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-promotion',
                    'defaults' => array(
                        'controller' => 'Promotions\Controller\Promotion',
                        'action' => 'deleteCrmPromotion',
                    ),
                ),
            ),
            'getCrmPromotionInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-promotion-info/:promotionId[/][:mode]',
                    'defaults' => array(
                        'controller' => 'Promotions\Controller\Promotion',
                        'action' => 'getCrmPromotionInfo',
                    ),
                ),
            ),
            'editCrmPromotionDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-promotion-detail/:promotionId',
                    'defaults' => array(
                        'controller' => 'Promotions\Controller\Promotion',
                        'action' => 'editCrmPromotionDetail',
                    ),
                ),
            ),
            'viewCrmPromotionInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-crm-promotion-info/:promotionId',
                    'defaults' => array(
                        'controller' => 'Promotions\Controller\Promotion',
                        'action' => 'viewCrmPromotionInfo',
                    ),
                )
            ),
            'editCrmPromotion' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-promotion',
                    'defaults' => array(
                        'controller' => 'Promotions\Controller\Promotion',
                        'action' => 'editCrmPromotion',
                    ),
                ),
            ),
            'getCouponCode' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-coupon-code',
                    'defaults' => array(
                        'controller' => 'Promotions\Controller\Promotion',
                        'action' => 'getCouponCode',
                    ),
                ),
            ),
		'createCrmPromotion' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-crm-promotion',
                    'defaults' => array(
                        'controller' => 'Promotions\Controller\Promotion',
                        'action' => 'createCrmPromotion',
                    ),
                ),
            ),
			'getPromotionalProducts' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-promotional-products',
                    'defaults' => array(
                        'controller' => 'Promotions\Controller\Promotion',
                        'action' => 'getPromotionalProducts',
                    ),
                ),
            ),
            'getCrmProductPromotion' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-promotions',
                    'defaults' => array(
                        'controller' => 'Promotions\Controller\Promotion',
                        'action' => 'getCrmProductPromotion',
                    ),
                ),
            ),
			'editCrmProductPromotion' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-product-promotion',
                    'defaults' => array(
                        'controller' => 'Promotions\Controller\Promotion',
                        'action' => 'editCrmProductPromotion',
                    ),
                ),
            ),
			'editCrmProductPromotionDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-product-promotion-detail/:promotionId',
                    'defaults' => array(
                        'controller' => 'Promotions\Controller\Promotion',
                        'action' => 'editCrmProductPromotionDetail',
                    ),
                ),
            ),
			'deleteCrmProductPromotion' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-product-promotion',
                    'defaults' => array(
                        'controller' => 'Promotions\Controller\Promotion',
                        'action' => 'deleteCrmProductPromotion',
                    ),
                ),
            ),
			'viewCrmProductPromotionInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-crm-product-promotion-info/:promotionId',
                    'defaults' => array(
                        'controller' => 'Promotions\Controller\Promotion',
                        'action' => 'viewCrmProductPromotionInfo',
                    ),
                )
            ),
			'getCrmProductPromotionInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-product-promotion-info/:promotionId[/][:mode]',
                    'defaults' => array(
                        'controller' => 'Promotions\Controller\Promotion',
                        'action' => 'getCrmProductPromotionInfo',
                    ),
                ),
            ),
        )
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'navigation' => array(
        'default' => array(
            array(
                'label' => 'Promotion',
                'route' => 'getCrmPromotion',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'getCrmPromotion',
                        'pages' => array(
                            array(
                                'label' => 'Create',
                                'route' => 'addCrmPromotion',
                                'action' => 'addCrmPromotion',
                            ),
                            array(
                                'label' => 'View',
                                'route' => 'viewCrmPromotionInfo',
                                'action' => 'viewCrmPromotionInfo',
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'editCrmPromotionDetail',
                                'action' => 'editCrmPromotionDetail',
                            ),
                            array(
                                'label' => 'Change Log',
                                'route' => 'crmpromotionchangelog',
                                'action' => 'change-log'
                            ),
                        ),
                    ),
                )
            ),
			array(
                'label' => 'Promotion',
                'route' => 'getCrmProductPromotion',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'getCrmProductPromotion',
                        'pages' => array(
                            array(
                                'label' => 'Create',
                                'route' => 'createCrmPromotion',
                                'action' => 'createCrmPromotion',
                            ),
                            array(
                                'label' => 'View',
                                'route' => 'viewCrmProductPromotionInfo',
                                'action' => 'viewCrmProductPromotionInfo',
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'editCrmProductPromotionDetail',
                                'action' => 'editCrmProductPromotionDetail',
                            ),
                            array(
                                'label' => 'Change Log',
                                'route' => 'crmpromotionchangelog',
                                'action' => 'change-log'
                            ),
                        ),
                    ),
                )
            )
        ),
    ),
    'promotion_messages' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
            'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'promotions' => __DIR__ . '/../view'
        ),
        'template_map' => array(
            'layout/custom' => __DIR__ . '/../../module/Common/layout/crm.phtml',
            'error/404' => __DIR__ . '/../../module/Common/error/404.phtml',
            'error/index' => __DIR__ . '/../../module/Common/error/index.phtml',
        ),
    ),
    'global_config' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
);
