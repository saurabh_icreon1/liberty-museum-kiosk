<?php

/**
 * This model is used for promotion module validation.
 * @package    Promotions
 * @author     Icreon Tech - DT
 */

namespace Promotions\Model;

use Promotions\Module;
// Add these import statements
use Base\Model\BaseModel;
use Zend\Db\Adapter\Adapter;
// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * This class is used for promotion module validation.
 * @package    Promotions_Promotion
 * @author     Icreon Tech - DT
 */
class Promotion extends BaseModel {

    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * This method is used to call add promotion procedure parameter
     * @param array
     * @return array
     * @author Icreon Tech - DT
     */
    public function getCreatePromotionArr($data) {
        $returnArr = array();
        $returnArr[] = (isset($data['promotion_name']) && !empty($data['promotion_name'])) ? $data['promotion_name'] : '';
        $returnArr[] = (isset($data['promotion_description']) && !empty($data['promotion_description'])) ? $data['promotion_description'] : '';
        $returnArr[] = (isset($data['coupon_code']) && !empty($data['coupon_code'])) ? $data['coupon_code'] : '';
        $returnArr[] = (isset($data['applicable_id']) && !empty($data['applicable_id'])) ? $data['applicable_id'] : '';
        $returnArr[] = (isset($data['product_category']) && !empty($data['product_category'])) ? $data['product_category'] : '';
        $returnArr[] = (isset($data['discount']) && !empty($data['discount'])) ? $data['discount'] : '';
        if ((isset($data['start_date']) && !empty($data['start_date']))) {
            $returnArr[] = date('Y-m-d H:i:s', strtotime($data['start_date']));
        } else {
            $returnArr[] = '';
        }
        if ((isset($data['expiry_date']) && !empty($data['expiry_date']))) {
            $returnArr[] = date('Y-m-d H:i:s', strtotime($data['expiry_date']));
        } else {
            $returnArr[] = '';
        }
        $returnArr[] = (isset($data['usage_limit']) && !empty($data['usage_limit'])) ? $data['usage_limit'] : '';
        $returnArr[] = (isset($data['is_excluded_applied']) && !empty($data['is_excluded_applied'])) ? $data['is_excluded_applied'] : '0';
        $returnArr[] = (isset($data['is_excluded_product_category']) && !empty($data['is_excluded_product_category'])) ? $data['is_excluded_product_category'] : '';
        $returnArr[] = (isset($data['added_by']) && !empty($data['added_by'])) ? $data['added_by'] : '';
        $returnArr[] = (isset($data['added_date']) && !empty($data['added_date'])) ? $data['added_date'] : '';
        $returnArr[] = (isset($data['modified_by']) && !empty($data['modified_by'])) ? $data['modified_by'] : '';
        $returnArr[] = (isset($data['modified_date']) && !empty($data['modified_date'])) ? $data['modified_date'] : '';
        $returnArr[] = (isset($data['is_free_shipping']) && !empty($data['is_free_shipping'])) ? $data['is_free_shipping'] : '0';
        $returnArr[] = (isset($data['free_shipping_amount']) && !empty($data['free_shipping_amount'])) ? $data['free_shipping_amount'] : NULL;
        if(isset($data['is_domestic']) && !empty($data['is_domestic']) && isset($data['is_international']) && !empty($data['is_international'])){
            $returnArr[] = 3;
        }
        else if(isset($data['is_international']) && !empty($data['is_international'])){
            $returnArr[] = 2;
        }
        else if(isset($data['is_domestic']) && !empty($data['is_domestic'])){
            $returnArr[] = 1;
        }        
        else {
            $returnArr[] = 0;
        }
        return $returnArr;
    }

    /**
     * This method is used to call add promotion applied procedure parameter
     * @param array
     * @return array
     * @author Icreon Tech - DT
     */
    public function getAppliedPromotionArr($data) {
        $returnArr = array();
        $returnArr[] = (isset($data['promotion_id']) && !empty($data['promotion_id'])) ? $data['promotion_id'] : '';
        $returnArr[] = (isset($data['applicable_id']) && !empty($data['applicable_id'])) ? $data['applicable_id'] : '';
        $returnArr[] = (isset($data['applied_id']) && !empty($data['applied_id'])) ? $data['applied_id'] : '';
        $returnArr[] = (isset($data['added_by']) && !empty($data['added_by'])) ? $data['added_by'] : '';
        $returnArr[] = (isset($data['added_date']) && !empty($data['added_date'])) ? $data['added_date'] : '';
        $returnArr[] = (isset($data['modified_by']) && !empty($data['modified_by'])) ? $data['modified_by'] : '';
        $returnArr[] = (isset($data['modified_date']) && !empty($data['modified_date'])) ? $data['modified_date'] : '';
        return $returnArr;
    }

    /**
     * This method is used to call remove promotion applied procedure parameter
     * @param array
     * @return array
     * @author Icreon Tech - DT
     */
    public function getAppliedPromotionRemoveArr($data) {
        $returnArr = array();
        $returnArr[] = (isset($data['promotion_id']) && !empty($data['promotion_id'])) ? $data['promotion_id'] : '';
        $returnArr[] = (isset($data['type']) && !empty($data['type'])) ? $data['type'] : '';
        $returnArr[] = (isset($data['applicable_id']) && !empty($data['applicable_id'])) ? $data['applicable_id'] : '';
        $returnArr[] = (isset($data['applied_ids']) && !empty($data['applied_ids'])) ? $data['applied_ids'] : '';
        return $returnArr;
    }

    /**
     * This method is used to call add promotion category product procedure parameter
     * @param array
     * @return array
     * @author Icreon Tech - DT
     */
    public function getProductCategoryPromotionArr($data) {
        $returnArr = array();
        $returnArr[] = (isset($data['promotion_id']) && !empty($data['promotion_id'])) ? $data['promotion_id'] : '';
        $returnArr[] = (isset($data['product_category']) && !empty($data['product_category'])) ? $data['product_category'] : '';
        $returnArr[] = (isset($data['applied_id']) && !empty($data['applied_id'])) ? $data['applied_id'] : '';
        $returnArr[] = (isset($data['added_by']) && !empty($data['added_by'])) ? $data['added_by'] : '';
        $returnArr[] = (isset($data['added_date']) && !empty($data['added_date'])) ? $data['added_date'] : '';
        $returnArr[] = (isset($data['modified_by']) && !empty($data['modified_by'])) ? $data['modified_by'] : '';
        $returnArr[] = (isset($data['modified_date']) && !empty($data['modified_date'])) ? $data['modified_date'] : '';
        return $returnArr;
    }

    /**
     * This method is used to call edit promotion procedure parameter
     * @param array
     * @return array
     * @author Icreon Tech - DT
     */
    public function getEditPromotionArr($data) {
        $returnArr = array();
        $returnArr[] = (isset($data['promotion_id']) && !empty($data['promotion_id'])) ? $data['promotion_id'] : '';
        $returnArr[] = (isset($data['promotion_name']) && !empty($data['promotion_name'])) ? $data['promotion_name'] : '';
        $returnArr[] = (isset($data['promotion_description']) && !empty($data['promotion_description'])) ? $data['promotion_description'] : '';
        $returnArr[] = (isset($data['applicable_id']) && !empty($data['applicable_id'])) ? $data['applicable_id'] : '';
        $returnArr[] = (isset($data['product_category']) && !empty($data['product_category'])) ? $data['product_category'] : '';
        $returnArr[] = (isset($data['discount']) && !empty($data['discount'])) ? $data['discount'] : '';
        if ((isset($data['start_date']) && !empty($data['start_date']))) {
            $returnArr[] = date('Y-m-d H:i:s', strtotime($data['start_date']));
        } else {
            $returnArr[] = '';
        }
        if ((isset($data['expiry_date']) && !empty($data['expiry_date']))) {
            $returnArr[] = date('Y-m-d H:i:s', strtotime($data['expiry_date']));
        } else {
            $returnArr[] = '';
        }
        $returnArr[] = (isset($data['usage_limit']) && !empty($data['usage_limit'])) ? $data['usage_limit'] : '';
        $returnArr[] = (isset($data['is_excluded_applied']) && !empty($data['is_excluded_applied'])) ? $data['is_excluded_applied'] : '0';
        $returnArr[] = (isset($data['is_excluded_product_category']) && !empty($data['is_excluded_product_category'])) ? $data['is_excluded_product_category'] : '';
        $returnArr[] = (isset($data['modified_by']) && !empty($data['modified_by'])) ? $data['modified_by'] : '';
        $returnArr[] = (isset($data['modified_date']) && !empty($data['modified_date'])) ? $data['modified_date'] : '';
        $returnArr[] = (isset($data['is_free_shipping']) && !empty($data['is_free_shipping'])) ? $data['is_free_shipping'] : '0';
        $returnArr[] = (isset($data['free_shipping_amount']) && !empty($data['free_shipping_amount'])) ? $data['free_shipping_amount'] : NULL;
        if(isset($data['is_domestic']) && !empty($data['is_domestic']) && isset($data['is_international']) && !empty($data['is_international'])){
            $returnArr[] = 3;
        }
        else if(isset($data['is_international']) && !empty($data['is_international'])){
            $returnArr[] = 2;
        }
        else if(isset($data['is_domestic']) && !empty($data['is_domestic'])){
            $returnArr[] = 1;
        }        
        else {
            $returnArr[] = 0;
        }
        return $returnArr;
    }

    /**
     * This method is used to add filtering and validation to the form elements of create promotion
     * @param void
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getInputFilterCreatePromotion() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'promotion_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'expiry_date',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'start_date',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'user',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'user_ids',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'group_name',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'group_name_ids',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_excluded_applied',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'category_name',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'category_name_ids',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'product_name',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'product_name_ids',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_excluded_product_category',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_free_shipping',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'free_shipping_amount',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'promotion_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PROMOTION_NAME_EMPTY',
                                    ),
                                )
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[A-Za-z0-9 %,.-]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'ALPHA_NUMERIC_SPECIALSYMPOL'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Db\NoRecordExists',
                                'options' => array(
                                    'table' => 'tbl_prm_promotions',
                                    'field' => 'promotion_name',
                                    'adapter' => $this->adapter,
                                    'exclude' => array(
                                        'field' => 'is_deleted',
                                        'value' => '1'
                                    ),
                                    'messages' => array(
                                        'recordFound' => 'PROMOTION_NAME_EXIST',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'applicable_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'APPLICABLE_BY_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'product_category',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PRODUCT_CATEGORY_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'discount',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^-?\d*(\.\d+)?$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'usage_limit',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'USAGE_LIMIT_DIGIT'
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_free_shipping',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'promotion_description',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )                        
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * This method is used to add filtering and validation to the form elements of edit promotion
     * @param void
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getInputFilterEditPromotion($promotionId='') {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'promotion_id',
                        'required' => true,
                    )));
             $inputFilter->add($factory->createInput(array(
                        'name' => 'start_date',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'expiry_date',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'user',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'user_ids',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'group_name',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'group_name_ids',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_excluded_applied',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'category_name',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'category_name_ids',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'product_name',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'product_name_ids',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_excluded_product_category',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_free_shipping',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'free_shipping_amount',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'promotion_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PROMOTION_NAME_EMPTY',
                                    ),
                                )
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[A-Za-z0-9 %,.-]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'ALPHA_NUMERIC_SPECIALSYMPOL'
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'applicable_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'APPLICABLE_BY_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'product_category',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PRODUCT_CATEGORY_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'discount',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^-?\d*(\.\d+)?$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'usage_limit',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'USAGE_LIMIT_DIGIT'
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_free_shipping',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'promotion_description',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )                        
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * This method is used to return the stored procedure array for search promotion
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getPromotionSearchArr($dataArr=array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['coupon_code']) && $dataArr['coupon_code'] != '') ? $dataArr['coupon_code'] : '';
        $returnArr[] = (isset($dataArr['promotion_name']) && $dataArr['promotion_name'] != '') ? $dataArr['promotion_name'] : '';
        $returnArr[] = (isset($dataArr['user']) && $dataArr['user'] != '') ? $dataArr['user'] : '';
        $returnArr[] = (isset($dataArr['group_name']) && $dataArr['group_name'] != '') ? $dataArr['group_name'] : '';
        $returnArr[] = (isset($dataArr['discount']) && $dataArr['discount'] != '') ? $dataArr['discount'] : '';
        $returnArr[] = (isset($dataArr['applicable_id']) && $dataArr['applicable_id'] != '') ? $dataArr['applicable_id'] : '';
        $returnArr[] = (isset($dataArr['expiry_date_from']) && $dataArr['expiry_date_from'] != '') ? $dataArr['expiry_date_from'] : '';
        $returnArr[] = (isset($dataArr['expiry_date_to']) && $dataArr['expiry_date_to'] != '') ? $dataArr['expiry_date_to'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for create search result
     * @param code
     * @return Void
     * @author Icreon Tech - DT

     */
    public function getAddPromotionSearchArr($dataArr=array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['crm_user_id']) && $dataArr['crm_user_id'] != '') ? $dataArr['crm_user_id'] : '';
        $returnArr[] = (isset($dataArr['title']) && $dataArr['title'] != '') ? $dataArr['title'] : '';
        $returnArr[] = (isset($dataArr['search_query']) && $dataArr['search_query'] != '') ? $dataArr['search_query'] : '';
        $returnArr[] = (isset($dataArr['is_active']) && $dataArr['is_active'] != '') ? $dataArr['is_active'] : '';
        $returnArr[] = (isset($dataArr['is_delete']) && $dataArr['is_delete'] != '') ? $dataArr['is_delete'] : '';
        //$returnArr[] = (isset($dataArr['added_date'])) ? $dataArr['added_date'] : (isset($dataArr['modified_date']))?$dataArr['modified_date']:'';
        if (isset($dataArr['added_date'])) {
            $returnArr[] = $dataArr['added_date'];
        } else {
            $returnArr[] = $dataArr['modified_date'];
        }
        if (isset($dataArr['promotion_search_id']) && $dataArr['promotion_search_id'] != '') {
            $returnArr[] = $dataArr['promotion_search_id'];
        }
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for saved search
     * @param Array
     * @return Array
     * @author Icreon Tech - DT

     */
    public function getPromotionSavedSearchArr($dataArr=array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['crm_user_id']) && $dataArr['crm_user_id'] != '') ? $dataArr['crm_user_id'] : '';
        $returnArr[] = (isset($dataArr['promotion_search_id']) && $dataArr['promotion_search_id'] != '') ? $dataArr['promotion_search_id'] : '';
        $returnArr[] = (isset($dataArr['is_active'])) ? $dataArr['is_active'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for delete saved search
     * @param Array
     * @return Array
     * @author Icreon Tech - DT

     */
    public function getDeletePromotionSavedSearchArr($dataArr=array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['is_deleted']) && $dataArr['is_deleted'] != '') ? $dataArr['is_deleted'] : '';
        $returnArr[] = (isset($dataArr['modified_date']) && $dataArr['modified_date'] != '') ? $dataArr['modified_date'] : '';
        $returnArr[] = (isset($dataArr['promotion_search_id']) && $dataArr['promotion_search_id'] != '') ? $dataArr['promotion_search_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for activity search
     * @param code
     * @return Void
     * @author Icreon Tech - DT

     */
    public function getInputFilterPromotionSearch($filterData) {
        $inputFilterData = array();
        if (is_array($filterData)) {
            foreach ($filterData as $key => $val) {
                $inputFilterData[$key] = trim(addslashes(strip_tags($val)));
            }
        } else {
            $inputFilterData = trim(addslashes(strip_tags($filterData)));
        }


        return $inputFilterData;
    }

    /**
     * Function used to get array of elements need to call remove promotion
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getArrayForRemovePromotion($dataArr) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['promotion_id']) && $dataArr['promotion_id'] != '') ? $dataArr['promotion_id'] : '';
        return $returnArr;
    }

    /**
     * Function used to get array of elements need to call remove promotion
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getCouponArr($dataArr) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['coupon_code']) && $dataArr['coupon_code'] != '') ? $dataArr['coupon_code'] : '';
        return $returnArr;
    }


	public function getCreatePromotionArray($data) {
        $returnArr = array();
        $returnArr[] = (isset($data['promotion_name']) && !empty($data['promotion_name'])) ? $data['promotion_name'] : '';
        $returnArr[] = (isset($data['promotion_description']) && !empty($data['promotion_description'])) ? $data['promotion_description'] : '';
        $returnArr[] = (isset($data['coupon_code']) && !empty($data['coupon_code'])) ? $data['coupon_code'] : '';
        
		$returnArr[] = $data['applicable_id'];
		$returnArr[] = ((isset($data['shipping_discount_type'])) && ($data['shipping_discount_type']=='1'))?'1':'';

		$returnArr[] = ((isset($data['shipping_discount_amount'])) && (!is_null($data['shipping_discount_amount']) && $data['shipping_discount_amount']!=''))?$data['shipping_discount_amount']:'';

		$returnArr[] = (!is_null($data['start_date']) && $data['start_date']!='')?$data['start_date']:'';
		$returnArr[] = (!is_null($data['expiry_date']) && $data['expiry_date']!='')?$data['expiry_date']:'';

		$returnArr[] = ((isset($data['usage_limit'])) && (!is_null($data['usage_limit']) && $data['usage_limit']!=''))?$data['usage_limit']:'';

		$returnArr[] = ((isset($data['added_by'])) && (!is_null($data['added_by']) && $data['added_by']!=''))?$data['added_by']:'';
		$returnArr[] = ((isset($data['added_date'])) && (!is_null($data['added_date']) && $data['added_date']!=''))?$data['added_date']:'';
		$returnArr[] = ((isset($data['modified_by'])) && (!is_null($data['modified_by']) && $data['modified_by']!=''))?$data['modified_by']:'';
		$returnArr[] = ((isset($data['modified_date'])) && (!is_null($data['modified_date']) && $data['modified_date']!=''))?$data['modified_date']:'';

		$returnArr[] = ((isset($data['applicable_to'])) && (!empty($data['applicable_to'])))?$data['applicable_to']:'';
		$returnArr[] = ((isset($data['promotion_display'])) && (!is_null($data['promotion_display']) && $data['promotion_display']!=''))?$data['promotion_display']:'';

		$returnArr[] = ((isset($data['usage_limit_type'])) && (!is_null($data['usage_limit_type']) && $data['usage_limit_type']!=''))?$data['usage_limit_type']:'';
		$returnArr[] = ((isset($data['usage_limit_per_contact'])) && (!is_null($data['usage_limit_per_contact']) && $data['usage_limit_per_contact']!=''))?$data['usage_limit_per_contact']:'';
		$returnArr[] = ((isset($data['promotion_qualified_by'])) && (!is_null($data['promotion_qualified_by']) && $data['promotion_qualified_by']!=''))?$data['promotion_qualified_by']:'';
		$returnArr[] = ((isset($data['promotion_qualified_min_amount'])) && (!is_null($data['promotion_qualified_min_amount']) && $data['promotion_qualified_min_amount']!=''))?$data['promotion_qualified_min_amount']:'';

		$returnArr[] = ((isset($data['minimum_amount_products'])) && (!is_null($data['minimum_amount_products']) && $data['minimum_amount_products']!=''))?$data['minimum_amount_products']:'';
		$returnArr[] = ((isset($data['minimum_amount_product_type'])) && (!is_null($data['minimum_amount_product_type']) && $data['minimum_amount_product_type']!=''))?$data['minimum_amount_product_type']:'';

		$returnArr[] = ((isset($data['excluded_product_type'])) && (!is_null($data['excluded_product_type']) && $data['excluded_product_type']!=''))?$data['excluded_product_type']:'';
		$returnArr[] = ((isset($data['excluded_products'])) && (!is_null($data['excluded_products']) && $data['excluded_products']!=''))?$data['excluded_products']:'';
		$returnArr[] = ((isset($data['bundle_products'])) && (!is_null($data['bundle_products']) && $data['bundle_products']!=''))?$data['bundle_products']:'';
		$returnArr[] = ((isset($data['bundle_product_type'])) && (!is_null($data['bundle_product_type']) && $data['bundle_product_type']!=''))?$data['bundle_product_type']:'';
		$returnArr[] = ((isset($data['promotion_any_products'])) && (!is_null($data['promotion_any_products']) && $data['promotion_any_products']!=''))?$data['promotion_any_products']:'';
		$returnArr[] = ((isset($data['promotion_any_product_type'])) && (!is_null($data['promotion_any_product_type']) && $data['promotion_any_product_type']!=''))?$data['promotion_any_product_type']:'';
		$returnArr[] = ((isset($data['is_shipping_discount'])) && (!is_null($data['is_shipping_discount']) && $data['is_shipping_discount']!=''))?$data['is_shipping_discount']:'';
		$returnArr[] = ((isset($data['shipping_discount_type'])) && (!is_null($data['shipping_discount_type']) && $data['shipping_discount_type']!=''))?$data['shipping_discount_type']:'';
		$returnArr[] = ((isset($data['pb_shipping_type_id'])) && (!empty($data['pb_shipping_type_id'])))?implode(",",$data['pb_shipping_type_id']):'';

		$returnArr[] = ((isset($data['is_cart_discount'])) && (!is_null($data['is_cart_discount']) && $data['is_cart_discount']!=''))?$data['is_cart_discount']:'';				
		$returnArr[] = ((isset($data['cart_discount_type'])) && (!is_null($data['cart_discount_type']) && $data['cart_discount_type']!=''))?$data['cart_discount_type']:'';				
		$returnArr[] = ((isset($data['cart_discount_percent'])) && (!is_null($data['cart_discount_percent']) && $data['cart_discount_percent']!=''))?$data['cart_discount_percent']:'';
		$returnArr[] = ((isset($data['cart_discount_amount'])) && (!is_null($data['cart_discount_amount']) && $data['cart_discount_amount']!=''))?$data['cart_discount_amount']:'';
		$returnArr[] = ((isset($data['is_free_product'])) && (!is_null($data['is_free_product']) && $data['is_free_product']!=''))?$data['is_free_product']:'';				
		$returnArr[] = ((isset($data['free_products'])) && (!is_null($data['free_products']) && $data['free_products']!=''))?$data['free_products']:'';

		

        return $returnArr;
    }

    /**
     * Function used to check variables promotion
     * @author Icreon Tech - DT
     * @return boolean
     * @param Array
     */
    public function exchangeArray($data) {
        
    }

	public function getPromotionSearchArray($dataArr=array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['coupon_code']) && $dataArr['coupon_code'] != '') ? $dataArr['coupon_code'] : '';
        $returnArr[] = (isset($dataArr['promotion_name']) && $dataArr['promotion_name'] != '') ? $dataArr['promotion_name'] : '';
        $returnArr[] = (isset($dataArr['user']) && $dataArr['user'] != '') ? $dataArr['user'] : '';
        $returnArr[] = (isset($dataArr['group_name']) && $dataArr['group_name'] != '') ? $dataArr['group_name'] : '';
        $returnArr[] = (isset($dataArr['applicable_id']) && $dataArr['applicable_id'] != '') ? $dataArr['applicable_id'] : '';
        $returnArr[] = (isset($dataArr['expiry_date_from']) && $dataArr['expiry_date_from'] != '') ? $dataArr['expiry_date_from'] : '';
        $returnArr[] = (isset($dataArr['expiry_date_to']) && $dataArr['expiry_date_to'] != '') ? $dataArr['expiry_date_to'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        return $returnArr;
    }

	public function getEditProductPromotionArr($data) {
        $returnArr = array();
		$returnArr[] = (isset($data['promotion_id']) && !empty($data['promotion_id'])) ? $data['promotion_id'] : '';
        $returnArr[] = (isset($data['promotion_name']) && !empty($data['promotion_name'])) ? $data['promotion_name'] : '';
        $returnArr[] = (isset($data['promotion_description']) && !empty($data['promotion_description'])) ? $data['promotion_description'] : '';
        
		$returnArr[] = $data['applicable_id'];
		$returnArr[] = ((isset($data['shipping_discount_type'])) && ($data['shipping_discount_type']=='1'))?'1':'';

		$returnArr[] = ((isset($data['shipping_discount_amount'])) && (!is_null($data['shipping_discount_amount']) && $data['shipping_discount_amount']!=''))?$data['shipping_discount_amount']:'';

		$returnArr[] = (!is_null($data['start_date']) && $data['start_date']!='')?$data['start_date']:'';
		$returnArr[] = (!is_null($data['expiry_date']) && $data['expiry_date']!='')?$data['expiry_date']:'';

		$returnArr[] = ((isset($data['usage_limit'])) && (!is_null($data['usage_limit']) && $data['usage_limit']!=''))?$data['usage_limit']:'';

		$returnArr[] = ((isset($data['modified_by'])) && (!is_null($data['modified_by']) && $data['modified_by']!=''))?$data['modified_by']:'';
		$returnArr[] = ((isset($data['modified_date'])) && (!is_null($data['modified_date']) && $data['modified_date']!=''))?$data['modified_date']:'';

		$returnArr[] = ((isset($data['applicable_to'])) && (!empty($data['applicable_to'])))?$data['applicable_to']:'';
		$returnArr[] = ((isset($data['promotion_display'])) && (!is_null($data['promotion_display']) && $data['promotion_display']!=''))?$data['promotion_display']:'';

		$returnArr[] = ((isset($data['usage_limit_type'])) && (!is_null($data['usage_limit_type']) && $data['usage_limit_type']!=''))?$data['usage_limit_type']:'';
		$returnArr[] = ((isset($data['usage_limit_per_contact'])) && (!is_null($data['usage_limit_per_contact']) && $data['usage_limit_per_contact']!=''))?$data['usage_limit_per_contact']:'';
		$returnArr[] = ((isset($data['promotion_qualified_by'])) && (!is_null($data['promotion_qualified_by']) && $data['promotion_qualified_by']!=''))?$data['promotion_qualified_by']:'';
		$returnArr[] = ((isset($data['promotion_qualified_min_amount'])) && (!is_null($data['promotion_qualified_min_amount']) && $data['promotion_qualified_min_amount']!=''))?$data['promotion_qualified_min_amount']:'';

		$returnArr[] = ((isset($data['minimum_amount_products'])) && (!is_null($data['minimum_amount_products']) && $data['minimum_amount_products']!=''))?$data['minimum_amount_products']:'';
		$returnArr[] = ((isset($data['minimum_amount_product_type'])) && (!is_null($data['minimum_amount_product_type']) && $data['minimum_amount_product_type']!=''))?$data['minimum_amount_product_type']:'';

		$returnArr[] = ((isset($data['excluded_product_type'])) && (!is_null($data['excluded_product_type']) && $data['excluded_product_type']!=''))?$data['excluded_product_type']:'';
		$returnArr[] = ((isset($data['excluded_products'])) && (!is_null($data['excluded_products']) && $data['excluded_products']!=''))?$data['excluded_products']:'';
		$returnArr[] = ((isset($data['bundle_products'])) && (!is_null($data['bundle_products']) && $data['bundle_products']!=''))?$data['bundle_products']:'';
		$returnArr[] = ((isset($data['bundle_product_type'])) && (!is_null($data['bundle_product_type']) && $data['bundle_product_type']!=''))?$data['bundle_product_type']:'';
		$returnArr[] = ((isset($data['promotion_any_products'])) && (!is_null($data['promotion_any_products']) && $data['promotion_any_products']!=''))?$data['promotion_any_products']:'';
		$returnArr[] = ((isset($data['promotion_any_product_type'])) && (!is_null($data['promotion_any_product_type']) && $data['promotion_any_product_type']!=''))?$data['promotion_any_product_type']:'';
		$returnArr[] = ((isset($data['is_shipping_discount'])) && (!is_null($data['is_shipping_discount']) && $data['is_shipping_discount']!=''))?$data['is_shipping_discount']:'';
		$returnArr[] = ((isset($data['shipping_discount_type'])) && (!is_null($data['shipping_discount_type']) && $data['shipping_discount_type']!=''))?$data['shipping_discount_type']:'';
		$returnArr[] = ((isset($data['pb_shipping_type_id'])) && (!empty($data['pb_shipping_type_id'])))?implode(",",$data['pb_shipping_type_id']):'';

		$returnArr[] = ((isset($data['is_cart_discount'])) && (!is_null($data['is_cart_discount']) && $data['is_cart_discount']!=''))?$data['is_cart_discount']:'';				
		$returnArr[] = ((isset($data['cart_discount_type'])) && (!is_null($data['cart_discount_type']) && $data['cart_discount_type']!=''))?$data['cart_discount_type']:'';				
		$returnArr[] = ((isset($data['cart_discount_percent'])) && (!is_null($data['cart_discount_percent']) && $data['cart_discount_percent']!=''))?$data['cart_discount_percent']:'';
		$returnArr[] = ((isset($data['cart_discount_amount'])) && (!is_null($data['cart_discount_amount']) && $data['cart_discount_amount']!=''))?$data['cart_discount_amount']:'';
		$returnArr[] = ((isset($data['is_free_product'])) && (!is_null($data['is_free_product']) && $data['is_free_product']!=''))?$data['is_free_product']:'';				
		$returnArr[] = ((isset($data['free_products'])) && (!is_null($data['free_products']) && $data['free_products']!=''))?$data['free_products']:'';

        return $returnArr;
    }

}