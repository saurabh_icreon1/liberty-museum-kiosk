<?php

/**
 * This model is used for Promotion module database related work.
 * @package    Promotions
 * @author     Icreon Tech - DT
 */

namespace Promotions\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for Promotion module database related work.
 * @package    Promotions_Promotion
 * @author     Icreon Tech - DT
 */
class PromotionTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

    /**
     * Function for insert promotion data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function savePromotion($promotionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_insertCrmPromotion(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionData[0]);
        $stmt->getResource()->bindParam(2, $promotionData[1]);
        $stmt->getResource()->bindParam(3, $promotionData[2]);
        $stmt->getResource()->bindParam(4, $promotionData[3]);
        $stmt->getResource()->bindParam(5, $promotionData[4]);
        $stmt->getResource()->bindParam(6, $promotionData[5]);
        $stmt->getResource()->bindParam(7, $promotionData[6]);
        $stmt->getResource()->bindParam(8, $promotionData[7]);
        $stmt->getResource()->bindParam(9, $promotionData[8]);
        $stmt->getResource()->bindParam(10, $promotionData[9]);
        $stmt->getResource()->bindParam(11, $promotionData[10]);
        $stmt->getResource()->bindParam(12, $promotionData[11]);
        $stmt->getResource()->bindParam(13, $promotionData[12]);
        $stmt->getResource()->bindParam(14, $promotionData[13]);
        $stmt->getResource()->bindParam(15, $promotionData[14]);
        $stmt->getResource()->bindParam(16, $promotionData[15]);
        $stmt->getResource()->bindParam(17, $promotionData[16]);
        $stmt->getResource()->bindParam(18, $promotionData[17]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for insert applied promotion data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function saveAppliedPromotion($promotionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_insertCrmPromotionApplied(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionData[0]);
        $stmt->getResource()->bindParam(2, $promotionData[1]);
        $stmt->getResource()->bindParam(3, $promotionData[2]);
        $stmt->getResource()->bindParam(4, $promotionData[3]);
        $stmt->getResource()->bindParam(5, $promotionData[4]);
        $stmt->getResource()->bindParam(6, $promotionData[5]);
        $stmt->getResource()->bindParam(7, $promotionData[6]);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for remove applied promotion data
     * @author Icreon Tech - DT
     * @return void
     * @param Array
     */
    public function removeAppliedPromotion($promotionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_deleteCrmPromotionAppliedCategory(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionData[0]);
        $stmt->getResource()->bindParam(2, $promotionData[1]);
        $stmt->getResource()->bindParam(3, $promotionData[2]);
        $stmt->getResource()->bindParam(4, $promotionData[3]);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for insert applied promotion data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function saveProductCategoryPromotion($promotionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_insertCrmProductCategory(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionData[0]);
        $stmt->getResource()->bindParam(2, $promotionData[1]);
        $stmt->getResource()->bindParam(3, $promotionData[2]);
        $stmt->getResource()->bindParam(4, $promotionData[3]);
        $stmt->getResource()->bindParam(5, $promotionData[4]);
        $stmt->getResource()->bindParam(6, $promotionData[5]);
        $stmt->getResource()->bindParam(7, $promotionData[6]);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for edit promotion data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function editPromotion($promotionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_updateCrmPromotion(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionData[0]);
        $stmt->getResource()->bindParam(2, $promotionData[1]);
        $stmt->getResource()->bindParam(3, $promotionData[2]);
        $stmt->getResource()->bindParam(4, $promotionData[3]);
        $stmt->getResource()->bindParam(5, $promotionData[4]);
        $stmt->getResource()->bindParam(6, $promotionData[5]);
        $stmt->getResource()->bindParam(7, $promotionData[6]);
        $stmt->getResource()->bindParam(8, $promotionData[7]);
        $stmt->getResource()->bindParam(9, $promotionData[8]);
        $stmt->getResource()->bindParam(10, $promotionData[9]);
        $stmt->getResource()->bindParam(11, $promotionData[10]);
        $stmt->getResource()->bindParam(12, $promotionData[11]);
        $stmt->getResource()->bindParam(13, $promotionData[12]);
        $stmt->getResource()->bindParam(14, $promotionData[13]);
        $stmt->getResource()->bindParam(15, $promotionData[14]);
        $stmt->getResource()->bindParam(16, $promotionData[15]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for get promotions data
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getAllPromotions($promotionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_getCrmPromotions(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionData[0]);
        $stmt->getResource()->bindParam(2, $promotionData[1]);
        $stmt->getResource()->bindParam(3, $promotionData[2]);
        $stmt->getResource()->bindParam(4, $promotionData[3]);
        $stmt->getResource()->bindParam(5, $promotionData[4]);
        $stmt->getResource()->bindParam(6, $promotionData[5]);
        $stmt->getResource()->bindParam(7, $promotionData[6]);
        $stmt->getResource()->bindParam(8, $promotionData[7]);
        $stmt->getResource()->bindParam(9, $promotionData[8]);
        $stmt->getResource()->bindParam(10, $promotionData[9]);
        $stmt->getResource()->bindParam(11, $promotionData[10]);
        $stmt->getResource()->bindParam(12, $promotionData[11]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * This function is used to get the saved search listing for promotion search
     * @params this will be an array.
     * @return this will return a title of the saved search
     * @author Icreon Tech -DT
     */
    public function getPromotionSavedSearch($promotionData = array()) {
        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_getCrmPromotionSearch(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionData[0]);
        $stmt->getResource()->bindParam(2, $promotionData[1]);
        $stmt->getResource()->bindParam(3, $promotionData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * This function will save the search title into the table for promotion search
     * @params this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -DT
     */
    public function savePromotionSearch($promotionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_prm_insertCrmPromotionSearch(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $promotionData[0]);
            $stmt->getResource()->bindParam(2, $promotionData[1]);
            $stmt->getResource()->bindParam(3, $promotionData[2]);
            $stmt->getResource()->bindParam(4, $promotionData[3]);
            $stmt->getResource()->bindParam(5, $promotionData[4]);
            $stmt->getResource()->bindParam(6, $promotionData[5]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the search title into the table for promotion search
     * @params this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -DT
     */
    public function updatePromotionSearch($promotionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_prm_updateCrmPromotionSearch(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $promotionData[0]);
            $stmt->getResource()->bindParam(2, $promotionData[1]);
            $stmt->getResource()->bindParam(3, $promotionData[2]);
            $stmt->getResource()->bindParam(4, $promotionData[3]);
            $stmt->getResource()->bindParam(5, $promotionData[4]);
            $stmt->getResource()->bindParam(6, $promotionData[5]);
            $stmt->getResource()->bindParam(7, $promotionData[6]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete the saved promotion search titles
     * @params this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech -DT
     */
    public function deleteSavedSearch($promotionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_prm_deleteCrmPromotionSearch(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $promotionData[0]);
            $stmt->getResource()->bindParam(2, $promotionData[1]);
            $stmt->getResource()->bindParam(3, $promotionData[2]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for remove promotion
     * @author Icreon Tech - DT
     * @return Void
     * @param Array
     */
    public function removePromotionById($promotionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_deleteCrmPromotion(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for get last promotion id
     * @author Icreon Tech - DT
     * @return Int
     * @param void
     */
    public function getLastPromotionId() {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_getLastPromotion()");
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetch(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * Function for get promotion data on the base of id
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getPromotionByPromotionId($promotionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_getCrmPromotionDetails(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }

    /**
     * Function for get promotion data on the base of id
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getPromotionById($promotionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_getCrmPromotionInfo(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }

    /**
     * Function for get promotion applied data on the base of promotion id
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getPromotionAppliedByPromotionId($promotionData) {

        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_getCrmPromotionAppliedCategory(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionData[0]);
        $stmt->getResource()->bindParam(2, $promotionData[1]);
        $stmt->getResource()->bindParam(3, $promotionData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get promotion applied data on the base of promotion id
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getCoupons($promotionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_getCoupons(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    public function getCouponsCode($promotionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_getCouponsCode(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * Function for get Promotion Detail By coupon code 
     * @author Icreon Tech - DT
     * @return Int
     * @param void
     */
    public function getPromotionDetailByCode($code, $option = 'code') {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_getPromotionByCode(?,?)");
        $stmt->getResource()->bindParam(1, $code);
        $stmt->getResource()->bindParam(2, $option);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetch(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }
	
	/**
     * Function for get Promotion Detail By coupon code 
     * @author Icreon Tech - DT
     * @return Int
     * @param void
     */
    public function getPromotionBundledProducts($productTypeIds, $productIds) {

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_getPromotionBundledProducts(?,?)");
        $stmt->getResource()->bindParam(1, $productTypeIds);
        $stmt->getResource()->bindParam(2, $productIds);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
		$statement->closeCursor();
        return $resultSet;
    }

    /**
     * Function for get user in promotion 
     * @author Icreon Tech - SR
     * @return Int
     * @param void
     */
    public function getUsersInPromotion($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_getCrmPromotionUsers(?,?,?)");
        $stmt->getResource()->bindParam(1, $params['promotion_id']);
        $stmt->getResource()->bindParam(2, $params['user_id']);
        $stmt->getResource()->bindParam(3, $params['applicable_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * Function for get users in Group
     * @author Icreon Tech - SR
     * @return Int
     * @param void
     */
    public function getUserInGroup($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_getUserGroup(?)");
        $stmt->getResource()->bindParam(1, $params['user_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * Function for check user already used coupon or not
     * @author Icreon Tech - DT
     * @return Int
     * @param void
     */
    public function checkUserUsedCoupon($promotionData = array()) {
        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $promotionId = (isset($promotionData['promotionId'])) ? $promotionData['promotionId'] : '';
        $userId = (isset($promotionData['userId'])) ? $promotionData['userId'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_checkUserUsedCoupon(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionId);
        $stmt->getResource()->bindParam(2, $userId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * Function for append number of questions marks for stored procedure
     * @author Icreon Tech - DT
     * @return String
     * @param Array
     */
    public function appendQuestionMars($count) {
        $returnstr = '';
        for ($i = 1; $i <= $count; $i++) {
            $returnstr.='?,';
        }
        return rtrim($returnstr, ',');
    }

    /**
     * This function will get all valid promotion
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function getAllValidPromotions($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_prm_getAllValidPromotions()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getPromotionProducts($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_prm_getCrmPromotionalProducts(?)');
            $stmt->getResource()->bindParam(1, $params['product_name']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    public function saveProductPromotion($promotionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_insertCrmProductPromotion(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionData[0]);
        $stmt->getResource()->bindParam(2, $promotionData[1]);
        $stmt->getResource()->bindParam(3, $promotionData[2]);
        $stmt->getResource()->bindParam(4, $promotionData[3]);
        $stmt->getResource()->bindParam(5, $promotionData[4]);
        $stmt->getResource()->bindParam(6, $promotionData[5]);
        $stmt->getResource()->bindParam(7, $promotionData[6]);
        $stmt->getResource()->bindParam(8, $promotionData[7]);
        $stmt->getResource()->bindParam(9, $promotionData[8]);
        $stmt->getResource()->bindParam(10, $promotionData[9]);
        $stmt->getResource()->bindParam(11, $promotionData[10]);
        $stmt->getResource()->bindParam(12, $promotionData[11]);
        $stmt->getResource()->bindParam(13, $promotionData[12]);
        $stmt->getResource()->bindParam(14, $promotionData[13]);
        $stmt->getResource()->bindParam(15, $promotionData[14]);
        $stmt->getResource()->bindParam(16, $promotionData[15]);
        $stmt->getResource()->bindParam(17, $promotionData[16]);
        $stmt->getResource()->bindParam(18, $promotionData[17]);

        $stmt->getResource()->bindParam(19, $promotionData[18]);
        $stmt->getResource()->bindParam(20, $promotionData[19]);
        $stmt->getResource()->bindParam(21, $promotionData[20]);
        $stmt->getResource()->bindParam(22, $promotionData[21]);
        $stmt->getResource()->bindParam(23, $promotionData[22]);
        $stmt->getResource()->bindParam(24, $promotionData[23]);
        $stmt->getResource()->bindParam(25, $promotionData[24]);
        $stmt->getResource()->bindParam(26, $promotionData[25]);
        $stmt->getResource()->bindParam(27, $promotionData[26]);
        $stmt->getResource()->bindParam(28, $promotionData[27]);
        $stmt->getResource()->bindParam(29, $promotionData[28]);
        $stmt->getResource()->bindParam(30, $promotionData[29]);
        $stmt->getResource()->bindParam(31, $promotionData[30]);
        $stmt->getResource()->bindParam(32, $promotionData[31]);
        $stmt->getResource()->bindParam(33, $promotionData[32]);
        $stmt->getResource()->bindParam(34, $promotionData[33]);

		$stmt->getResource()->bindParam(35, $promotionData[34]);
        $stmt->getResource()->bindParam(36, $promotionData[35]);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    public function getAllProductPromotions($promotionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_getCrmProductPromotions(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionData[0]);
        $stmt->getResource()->bindParam(2, $promotionData[1]);
        $stmt->getResource()->bindParam(3, $promotionData[2]);
        $stmt->getResource()->bindParam(4, $promotionData[3]);
        $stmt->getResource()->bindParam(5, $promotionData[4]);
        $stmt->getResource()->bindParam(6, $promotionData[5]);
        $stmt->getResource()->bindParam(7, $promotionData[6]);
        $stmt->getResource()->bindParam(8, $promotionData[7]);
        $stmt->getResource()->bindParam(9, $promotionData[8]);
        $stmt->getResource()->bindParam(10, $promotionData[9]);
        $stmt->getResource()->bindParam(11, $promotionData[10]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    public function getLastProductPromotionId() {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_getLastProductPromotion()");
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetch(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    public function getProductPromotionById($promotionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_getCrmProductPromotionInfo(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }

    public function getProductPromotionAppliedByPromotionId($promotionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_getCrmProductPromotionApplied(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionData[0]);
        $stmt->getResource()->bindParam(2, $promotionData[1]);
        $stmt->getResource()->bindParam(3, $promotionData[2]);
        $result1 = $stmt->execute();
        $statement1 = $result1->getResource();
        $resultSet1 = $statement1->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet1);
        $statement1->closeCursor();
        if (!empty($resultSet1))
            return $resultSet1;
        else
            return false;
    }

    public function editProductPromotion($promotionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_updateCrmProductPromotion(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionData[0]);
        $stmt->getResource()->bindParam(2, $promotionData[1]);
        $stmt->getResource()->bindParam(3, $promotionData[2]);
        $stmt->getResource()->bindParam(4, $promotionData[3]);
        $stmt->getResource()->bindParam(5, $promotionData[4]);
        $stmt->getResource()->bindParam(6, $promotionData[5]);
        $stmt->getResource()->bindParam(7, $promotionData[6]);
        $stmt->getResource()->bindParam(8, $promotionData[7]);
        $stmt->getResource()->bindParam(9, $promotionData[8]);
        $stmt->getResource()->bindParam(10, $promotionData[9]);
        $stmt->getResource()->bindParam(11, $promotionData[10]);
        $stmt->getResource()->bindParam(12, $promotionData[11]);
        $stmt->getResource()->bindParam(13, $promotionData[12]);
        $stmt->getResource()->bindParam(14, $promotionData[13]);
        $stmt->getResource()->bindParam(15, $promotionData[14]);
        $stmt->getResource()->bindParam(16, $promotionData[15]);

        $stmt->getResource()->bindParam(17, $promotionData[16]);
        $stmt->getResource()->bindParam(18, $promotionData[17]);
        $stmt->getResource()->bindParam(19, $promotionData[18]);
        $stmt->getResource()->bindParam(20, $promotionData[19]);
        $stmt->getResource()->bindParam(21, $promotionData[20]);
        $stmt->getResource()->bindParam(22, $promotionData[21]);
        $stmt->getResource()->bindParam(23, $promotionData[22]);
        $stmt->getResource()->bindParam(24, $promotionData[23]);
        $stmt->getResource()->bindParam(25, $promotionData[24]);
        $stmt->getResource()->bindParam(26, $promotionData[25]);
        $stmt->getResource()->bindParam(27, $promotionData[26]);
        $stmt->getResource()->bindParam(28, $promotionData[27]);
        $stmt->getResource()->bindParam(29, $promotionData[28]);
        $stmt->getResource()->bindParam(30, $promotionData[29]);
        $stmt->getResource()->bindParam(31, $promotionData[30]);
        $stmt->getResource()->bindParam(32, $promotionData[31]);

		$stmt->getResource()->bindParam(33, $promotionData[32]);
        $stmt->getResource()->bindParam(34, $promotionData[33]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    public function removeAppliedProductPromotion($promotionData) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_deleteCrmProductPromotionApplied(?)");
        $stmt->getResource()->bindParam(1, $promotionData[0]);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    public function removeProductPromotionById($promotionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($promotionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_deleteCrmProductPromotion(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $promotionData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

	public function getPromotionMinimumAmountProducts($productTypeIds, $productIds) {

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_prm_getPromotionMinimumAmountProducts(?,?)");
        $stmt->getResource()->bindParam(1, $productTypeIds);
        $stmt->getResource()->bindParam(2, $productIds);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
		$statement->closeCursor();
        return $resultSet;
    }

}