<?php

/**
 * This page is used to save search Promotion form
 * @package    Promotion_SaveSearchForm
 * @author     Icreon Tech - DT
 */

namespace Promotions\Form;

use Zend\Form\Form;

/**
 * This form is used to save search Promotion
 * @package    SaveSearchForm
 * @author     Icreon Tech - DT
 */
class SaveSearchForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('save_search');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'search_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'search_id'
            )
        ));
        $this->add(array(
            'name' => 'search_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'search_name'
            )
        ));

        $this->add(array(
            'name' => 'savebutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Save',
                'id' => 'savebutton',
                'class' => 'save-btn m-l-15',
            ),
        ));
        $this->add(array(
            'name' => 'deletebutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Delete',
                'id' => 'deletebutton',
                'style' =>'display:none;',
                'href' => '#delete_promotion_saved_search',
                'class' => 'save-btn m-l-5 delete_saved_search',
                'onclick' => 'deleteSaveSearch()'
            ),
        ));
        
    }

}