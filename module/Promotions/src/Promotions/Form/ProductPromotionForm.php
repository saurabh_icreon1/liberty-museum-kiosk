<?php

/**
 * This page is used for Create and edit Promotion form.
 * @package    Activity_ActivityForm
 * @author     Icreon Tech - DT
 */

namespace Promotions\Form;

use Zend\Form\Form;

/**
 * This class is used for Create and edit Promotion form.
 * @package    PromotionForm
 * @author     Icreon Tech - DT
 */
class ProductPromotionForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('create_promotion');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'name' => 'promotion_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'promotion_id'
            )
        ));
        $this->add(array(
            'name' => 'promotion_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'promotion_name',
            )
        ));
        $this->add(array(
            'name' => 'promotion_description',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'promotion_description',
                'class' => 'width-60'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'applicable_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'applicable_id',
                'class' => 'e1',
                'onchange' => 'onChangeApplicable(this.value)'
            )
        ));

        $this->add(array(
            'name' => 'user',
            'attributes' => array(
                'type' => 'text',
                'id' => 'user',
                'class' => 'search-icon'
            )
        ));

        $this->add(array(
            'name' => 'user_ids',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_ids'
            )
        ));
        $this->add(array(
            'name' => 'group_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'group_name',
                'class' => 'search-icon'
            )
        ));

        $this->add(array(
            'name' => 'group_name_ids',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'group_name_ids'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'product_category',
            'options' => array(
                'value_options' => array(
                    1 => 'Global', 2 => 'Product Type', 3 => 'Product'
                ),
            ),
            'attributes' => array(
                'id' => 'product_category',
                'class' => 'e1',
                'onchange' => 'onChangeProductCategory(this.value)'
            )
        ));
        $this->add(array(
            'name' => 'category_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'category_name',
                'class' => 'search-icon'
            )
        ));

        $this->add(array(
            'name' => 'category_name_ids',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'category_name_ids'
            )
        ));
        $this->add(array(
            'name' => 'product_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'product_name',
                'class' => 'search-icon'
            )
        ));

        $this->add(array(
            'name' => 'product_name_ids',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'product_name_ids'
            )
        ));


        $this->add(array(
            'name' => 'discount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'discount'
            )
        ));
        $this->add(array(
            'name' => 'start_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'start_date',
                'class' => 'width-108 cal-icon',
            )
        ));
        $this->add(array(
            'name' => 'expiry_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'expiry_date',
                'class' => 'width-108 cal-icon',
            )
        ));
        $this->add(array(
            'name' => 'usage_limit',
            'attributes' => array(
                'type' => 'text',
                'id' => 'usage_limit',
				'class' => 'width-91 m-r-10',
				'disabled' => 'disabled'
            )
        ));


        $this->add(array(
            'name' => 'is_free_shipping',
            'type' => 'Checkbox',
            'attributes' => array(
                'value' => '0',
                'id' => 'is_free_shipping',
                'class' => 'e2',
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'Checkbox',
                    '1' => 'Checkbox',
                ),
            ),
        ));
        $this->add(array(
            'name' => 'free_shipping_amount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'free_shipping_amount',
                'class' => 'width-155'
            )
        ));
        $this->add(array(
            'name' => 'is_excluded_applied',
            'type' => 'Checkbox',
            'attributes' => array(
                'value' => '0',
                'id' => 'is_excluded_applied',
                'class' => 'e2',
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'Checkbox',
                    '1' => 'Checkbox',
                ),
            ),
        ));

        $this->add(array(
            'name' => 'is_excluded_product_category',
            'type' => 'Checkbox',
            'attributes' => array(
                'value' => '0',
                'id' => 'is_excluded_product_category',
                'class' => 'e2',
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'Checkbox',
                    '1' => 'Checkbox',
                ),
            ),
        ));


        $this->add(array(
            'name' => 'savepromotion',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'savepromotion',
                'class' => 'save-btn',
                'value' => 'Save'
            )
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-20',
                'value' => 'CANCEL',
                'onclick' => 'window.location.href="/crm-promotions"'
            )
        ));
        $this->add(array(
            'name' => 'is_domestic',
            'type' => 'Checkbox',
            'attributes' => array(
                'value' => '0',
                'id' => 'is_domestic',
                'class' => 'e2',
            )
        ));
        $this->add(array(
            'name' => 'is_international',
            'type' => 'Checkbox',
            'attributes' => array(
                'value' => '0',
                'id' => 'is_international',
                'class' => 'e2',
            )
        ));

		$this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'promotion_display',
            'options' => array(
                'value_options' => array(
                    '1' => 'Global',
                    '2' => 'Members Only',
                    '3' => 'Backend Only',
                ),
            ),
            'attributes' => array(
                'id' => 'promotion_display',
                'value' => '',
                'class' => 'e2 address-checkbox'
            )
        ));

		$this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'usage_limit_type',
            'options' => array(
                'value_options' => array(
                    '1' => 'No Limit',
                    '2' => 'One Time Per Contact',
                    '3' => 'Number Available:',
                ),
            ),
            'attributes' => array(
                'id' => 'usage_limit_type',
                'value' => '',
                'class' => 'e2 address-checkbox',
				'onClick' => 'javascript:enableUsageLimit(this.value);',
            )
        ));

		$this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'usage_limit_per_contact',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'options' => array(
                'use_hidden_element' => false
            ),
            'attributes' => array(
                'value' => '0', /* set checked to '1' */
                'class' => 'e2 address-checkbox',
                'id' => 'usage_limit_per_contact',
				'disabled' => 'disabled'
            )
        ));

		$this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_shipping_discount',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'options' => array(
                'use_hidden_element' => false
            ),
            'attributes' => array(
                'value' => '0', /* set checked to '1' */
                'class' => 'e2 address-checkbox',
                'id' => 'is_shipping_discount',
				'onChange' => 'javascript:enableShippingDiscount();'
            )
        ));

		$this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'shipping_discount_type',
            'options' => array(
                'value_options' => array(
                    '1' => 'Free',
                    '2' => 'Amount Off:',
                ),
            ),
            'attributes' => array(
                'id' => 'shipping_discount_type',
                'value' => '',
                'class' => 'e2 address-checkbox',
				'onClick' => 'javascript:enableShippingLocationDiscount(this.value);',
            )
        ));

		$this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'shipping_address_location[]',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'options' => array(
                'use_hidden_element' => false
            ),
            'attributes' => array(
                'value' => '', /* set checked to '1' */
                'class' => 'e2 address-checkbox',
                'id' => 'shipping_address_location'
            )
        ));

		$this->add(array(
            'name' => 'shipping_discount_amount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_discount_amount',
				'class' => 'width-91',
				'disabled' => 'disabled'
            )
        ));

		$this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_cart_discount',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'options' => array(
                'use_hidden_element' => false
            ),
            'attributes' => array(
                'value' => '0', /* set checked to '1' */
                'class' => 'e2 address-checkbox',
                'id' => 'is_cart_discount',
				'onChange' => 'javascript:enableCartDiscount();'
            )
        ));

		$this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'cart_discount_type',
            'options' => array(
                'value_options' => array(
                    '1' => 'Percentage:',
                    '2' => 'Amount Off:',
                ),
            ),
            'attributes' => array(
                'id' => 'cart_discount_type',
                'value' => '',
                'class' => 'e2 address-checkbox',
				'onClick' => 'javascript:enableCartDiscountAmount(this.value);',
            )
        ));

		$this->add(array(
            'name' => 'cart_discount_percent',
            'attributes' => array(
                'type' => 'text',
                'id' => 'cart_discount_percent',
				'class' => 'width-91',
				'disabled' => 'disabled'
            )
        ));

		$this->add(array(
            'name' => 'cart_discount_amount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'cart_discount_amount',
				'class' => 'width-91',
				'disabled' => 'disabled'
            )
        ));

		$this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_free_product',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'options' => array(
                'use_hidden_element' => false
            ),
            'attributes' => array(
                'value' => '0', /* set checked to '1' */
                'class' => 'e2 address-checkbox',
                'id' => 'is_free_product',
				'onChange' => 'javascript:enableFreeProducts();'
            )
        ));

		$this->add(array(
            'name' => 'free_products',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'free_products'
            )
        ));

		$this->add(array(
            'name' => 'free_product_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'free_product_name',
                'class' => 'search-icon'
            )
        ));

		$this->add(array(
            'name' => 'excluded_products',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'excluded_products'
            )
        ));

		$this->add(array(
            'name' => 'excluded_product_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'excluded_product_name',
                'class' => 'search-icon'
            )
        ));
		
		$this->add(array(
            'name' => 'excluded_product_type',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'excluded_product_type'
            )
        ));

		$this->add(array(
            'name' => 'excluded_product_type_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'excluded_product_type_name',
                'class' => 'search-icon'
            )
        ));

		$this->add(array(
            'name' => 'bundle_products',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'bundle_products'
            )
        ));

		$this->add(array(
            'name' => 'bundle_product_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'bundle_product_name',
                'class' => 'search-icon'
            )
        ));
		
		$this->add(array(
            'name' => 'bundle_product_type',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'bundle_product_type'
            )
        ));

		$this->add(array(
            'name' => 'bundle_product_type_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'bundle_product_type_name',
                'class' => 'search-icon'
            )
        ));

		
		$this->add(array(
            'name' => 'promotion_any_products',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'promotion_any_products'
            )
        ));

		$this->add(array(
            'name' => 'promotion_any_product',
            'attributes' => array(
                'type' => 'text',
                'id' => 'promotion_any_product',
                'class' => 'search-icon'
            )
        ));
		
		$this->add(array(
            'name' => 'promotion_any_product_type',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'promotion_any_product_type'
            )
        ));

		$this->add(array(
            'name' => 'promotion_any_product_type_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'promotion_any_product_type_name',
                'class' => 'search-icon'
            )
        ));

		
		$this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'promotion_qualified_by',
            'options' => array(
                'value_options' => array(
                    '1' => 'Min. Product Cart Amount:',
                    '2' => 'All Bundled Products',
                    '3' => 'Any Product in List',
                ),
            ),
            'attributes' => array(
                'id' => 'promotion_qualified_by',
                'value' => '',
                'class' => 'e2 address-checkbox',
				'onClick' => 'javascript:enablePromotionQualifies(this.value);',
            )
        ));

		$this->add(array(
            'name' => 'promotion_qualified_min_amount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'promotion_qualified_min_amount',
				'class' => 'width-91 rel-auto-error',
				'disabled' => 'disabled'
            )
        ));
		
		$this->add(array(
            'name' => 'minimum_amount_products',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'minimum_amount_products'
            )
        ));

		$this->add(array(
            'name' => 'minimum_amount_product_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'minimum_amount_product_name',
                'class' => 'search-icon',
				'disabled' => 'disabled'
            )
        ));
		
		$this->add(array(
            'name' => 'minimum_amount_product_type',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'minimum_amount_product_type'
            )
        ));

		$this->add(array(
            'name' => 'minimum_amount_product_type_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'minimum_amount_product_type_name',
                'class' => 'search-icon',
				'disabled' => 'disabled'
            )
        ));
    }

}
