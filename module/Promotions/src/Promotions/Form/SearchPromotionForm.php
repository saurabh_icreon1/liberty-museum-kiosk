<?php

/**
 * This page is used for search Promotion form.
 * @package    Promotion_SearchPromotionForm
 * @author     Icreon Tech - DT
 */

namespace Promotions\Form;

use Zend\Form\Form;
use Promotions\Form\SearchPromotionForm;

/**
 * This form is used for search Promotion.
 * @package    SearchPromotionForm
 * @author     Icreon Tech - DT
 */
class SearchPromotionForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search_promotion');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'coupon_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'coupon_code',
            )
        ));
        $this->add(array(
            'name' => 'promotion_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'promotion_name',
            )
        ));

        $this->add(array(
            'name' => 'user',
            'attributes' => array(
                'type' => 'text',
                'id' => 'user'
            )
        ));
        $this->add(array(
            'name' => 'group_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'group_name'
            )
        ));
        $this->add(array(
            'name' => 'discount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'discount'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'applicable_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'applicable_id',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'expiry_date_range',
            'attributes' => array(
                'id' => 'expiry_date_range',
                'class' => 'e1 select-w-320',
                'onChange' => 'showDate(this.id,"date_range_div")'
            )
        ));
        $this->add(array(
            'name' => 'expiry_date_from',
            'attributes' => array(
                'type' => 'text',
                'class' => 'calendar-input width-130 cal-icon',
                'id' => 'expiry_date_from',
            )
        ));
        $this->add(array(
            'name' => 'expiry_date_to',
            'attributes' => array(
                'type' => 'text',
                'class' => 'calendar-input width-130 cal-icon',
                'id' => 'expiry_date_to',
            )
        ));

        $this->add(array(
            'name' => 'searchpromotionbutton',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'searchpromotionbutton',
                'class' => 'search-btn',
                'value' => 'SEARCH'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'saved_search',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e1 select-w-320',
                'id' => 'saved_search',
                'onChange' => 'getSavedPromotionSearchResult();'
            ),
        ));
    }

}

