<?php

/**
 * This page is used for Create and edit Promotion form.
 * @package    Activity_ActivityForm
 * @author     Icreon Tech - DT
 */

namespace Promotions\Form;

use Zend\Form\Form;

/**
 * This class is used for Create and edit Promotion form.
 * @package    PromotionForm
 * @author     Icreon Tech - DT
 */
class PromotionForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('create_promotion');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'name' => 'promotion_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'promotion_id'
            )
        ));
        $this->add(array(
            'name' => 'promotion_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'promotion_name',
            )
        ));
        $this->add(array(
            'name' => 'promotion_description',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'promotion_description',
                'class' => 'width-60'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'applicable_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'applicable_id',
                'class' => 'e1',
                'onchange' => 'onChangeApplicable(this.value)'
            )
        ));

        $this->add(array(
            'name' => 'user',
            'attributes' => array(
                'type' => 'text',
                'id' => 'user',
                'class' => 'search-icon'
            )
        ));

        $this->add(array(
            'name' => 'user_ids',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_ids'
            )
        ));
        $this->add(array(
            'name' => 'group_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'group_name',
                'class' => 'search-icon'
            )
        ));

        $this->add(array(
            'name' => 'group_name_ids',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'group_name_ids'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'product_category',
            'options' => array(
                'value_options' => array(
                    1 => 'Global', 2 => 'Product Type', 3 => 'Product'
                ),
            ),
            'attributes' => array(
                'id' => 'product_category',
                'class' => 'e1',
                'onchange' => 'onChangeProductCategory(this.value)'
            )
        ));
        $this->add(array(
            'name' => 'category_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'category_name',
                'class' => 'search-icon'
            )
        ));

        $this->add(array(
            'name' => 'category_name_ids',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'category_name_ids'
            )
        ));
        $this->add(array(
            'name' => 'product_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'product_name',
                'class' => 'search-icon'
            )
        ));

        $this->add(array(
            'name' => 'product_name_ids',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'product_name_ids'
            )
        ));


        $this->add(array(
            'name' => 'discount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'discount'
            )
        ));
        $this->add(array(
            'name' => 'start_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'start_date',
                'class' => 'width-108 cal-icon',
            )
        ));
        $this->add(array(
            'name' => 'expiry_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'expiry_date',
                'class' => 'width-108 cal-icon',
            )
        ));
        $this->add(array(
            'name' => 'usage_limit',
            'attributes' => array(
                'type' => 'text',
                'id' => 'usage_limit'
            )
        ));


        $this->add(array(
            'name' => 'is_free_shipping',
            'type' => 'Checkbox',
            'attributes' => array(
                'value' => '0',
                'id' => 'is_free_shipping',
                'class' => 'e2',
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'Checkbox',
                    '1' => 'Checkbox',
                ),
            ),
        ));
        $this->add(array(
            'name' => 'free_shipping_amount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'free_shipping_amount',
                'class' => 'width-155'
            )
        ));
        $this->add(array(
            'name' => 'is_excluded_applied',
            'type' => 'Checkbox',
            'attributes' => array(
                'value' => '0',
                'id' => 'is_excluded_applied',
                'class' => 'e2',
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'Checkbox',
                    '1' => 'Checkbox',
                ),
            ),
        ));

        $this->add(array(
            'name' => 'is_excluded_product_category',
            'type' => 'Checkbox',
            'attributes' => array(
                'value' => '0',
                'id' => 'is_excluded_product_category',
                'class' => 'e2',
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'Checkbox',
                    '1' => 'Checkbox',
                ),
            ),
        ));


        $this->add(array(
            'name' => 'savepromotion',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'savepromotion',
                'class' => 'save-btn',
                'value' => 'Save'
            )
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-20',
                'value' => 'CANCEL',
                'onclick' => 'window.location.href="/crm-promotion"'
            )
        ));
        $this->add(array(
            'name' => 'is_domestic',
            'type' => 'Checkbox',
            'attributes' => array(
                'value' => '0',
                'id' => 'is_domestic',
                'class' => 'e2',
            )
        ));
        $this->add(array(
            'name' => 'is_international',
            'type' => 'Checkbox',
            'attributes' => array(
                'value' => '0',
                'id' => 'is_international',
                'class' => 'e2',
            )
        ));
    }

}
