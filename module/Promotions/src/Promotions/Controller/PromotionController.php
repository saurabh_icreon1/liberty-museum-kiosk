<?php

/**
 * This controller is used for promotion module
 * @package    Promotion_PromotionController
 * @author     Icreon Tech - DT
 */

namespace Promotions\Controller;

use Base\Controller\BaseController;
use Base\Model\UploadHandler;
use Zend\View\Model\ViewModel;
use Promotions\Form\PromotionForm;
use Promotions\Form\SearchPromotionForm;
use Promotions\Form\SaveSearchForm;
use Promotions\Model\Promotion;
use Transaction\Model\Transaction;
use Common\Model\Common;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use Promotions\Form\ProductPromotionForm;

/**
 * This class is used to perform all promotion related task
 *
 * Here we do create,edit,update,save search ,update save search , listing of promotion
 *
 * @category Zend
 * @package Promotion_PromotionController
 * @link http://SITE_URL/promotions
 */
class PromotionController extends BaseController {

    protected $_promotionTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
    }

    /**
     * This function is used to get table and config file object
     * @return     array
     * @param void
     * @author Icreon Tech - DT
     */
    public function getPromotionTable() {
        $this->checkUserAuthentication();
        if (!$this->_promotionTable) {
            $serviceManager = $this->getServiceLocator();
            $this->_promotionTable = $serviceManager->get('Promotions\Model\PromotionTable');
            $this->_translator = $serviceManager->get('translator');
            $this->_config = $serviceManager->get('Config');
            $this->_adapter = $serviceManager->get('dbAdapter');
            $this->_auth = new AuthenticationService();
        }
        return $this->_promotionTable;
    }

    /**
     * This action is used to search promotions
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function getCrmPromotionAction() {
        $this->checkUserAuthentication();
        $this->getPromotionTable();
        $params = $this->params()->fromRoute();
        $promotionSearchMessage = $this->_config['promotion_messages']['config']['promotion_search_message'];
        $commonMessage = $this->_config['promotion_messages']['config']['common_message'];

        $promotion = new Promotion($this->_adapter);
        $searchPromotionForm = new SearchPromotionForm();
        $saveSearchForm = new SaveSearchForm();
        $request = $this->getRequest();
        /* Applicable for */
        $getApplicableFor = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPromotionApplicable();
        $applicableFor = array('' => 'Select');
        if ($getApplicableFor !== false) {
            foreach ($getApplicableFor as $key => $val) {
                $applicableFor[$val['applicable_id']] = $val['applicable'];
            }
        }
        $applicableFor['2,3'] = 'Contact and Group Both';
        $searchPromotionForm->get('applicable_id')->setAttributes(array('options' => $applicableFor));
        /* End  Applicable for */
        /* Get date range */
        $promotionDateRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
        $dateRange = array("" => "Select One");
        foreach ($promotionDateRange as $key => $val) {
            $dateRange[$val['range_id']] = $val['range'];
        }
        $searchPromotionForm->get('expiry_date_range')->setAttribute('options', $dateRange);
        /* end Get date range */

        /* Saved search data */
        $getSearchArray = array();
        $getSearchArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
        $getSearchArray['promotion_search_id'] = '';
        $getSearchArray['is_active'] = '1';
        $promotionSavedSearchArray = $promotion->getPromotionSavedSearchArr($getSearchArray);
        $promotionSearchArray = $this->_promotionTable->getPromotionSavedSearch($promotionSavedSearchArray);
        $promotionSearchList = array();
        $promotionSearchList[''] = 'Select';
        foreach ($promotionSearchArray as $key => $val) {
            $promotionSearchList[$val['promotion_search_id']] = stripslashes($val['title']);
        }
        $searchPromotionForm->get('saved_search')->setAttribute('options', $promotionSearchList);

        /* end  Saved search data */
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $response = $this->getResponse();
            $postArray = $request->getPost()->toArray();
            parse_str($request->getPost('searchString'), $searchParam);
            $dashlet = $request->getPost('crm_dashlet');
            $dashletSearchId = $request->getPost('searchId');
            $dashletId = $request->getPost('dashletId');
            $dashletSearchType = $request->getPost('searchType');
            $dashletSearchString = $request->getPost('dashletSearchString');
            if (isset($dashletSearchString) && $dashletSearchString != '') {
                $searchParam = unserialize($dashletSearchString);
            }
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit; // do not put $limit*($page - 1)
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            if (isset($searchParam['expiry_date_range']) && $searchParam['expiry_date_range'] != 1 && $searchParam['expiry_date_range'] != '') {
                $dateRange = $this->getDateRange($searchParam['expiry_date_range']);
                $searchParam['expiry_date_from'] = $this->DateFormat($dateRange['from'], 'db_date_format_from');
                $searchParam['expiry_date_to'] = $this->DateFormat($dateRange['to'], 'db_date_format_to');
            } else if (isset($searchParam['expiry_date_range']) && $searchParam['expiry_date_range'] == '') {
                $searchParam['expiry_date_from'] = '';
                $searchParam['expiry_date_to'] = '';
            } else {
                if (isset($searchParam['expiry_date_from']) && $searchParam['expiry_date_from'] != '') {
                    $searchParam['expiry_date_from'] = $this->DateFormat($searchParam['expiry_date_from'], 'db_date_format_from');
                }
                if (isset($searchParam['expiry_date_to']) && $searchParam['expiry_date_to'] != '') {
                    $searchParam['expiry_date_to'] = $this->DateFormat($searchParam['expiry_date_to'], 'db_date_format_to');
                }
            }
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'tbl_promotion.modified_date' : $searchParam['sort_field'];
            $searchPromotionArr = $promotion->getPromotionSearchArr($searchParam);

            $seachResult = $this->_promotionTable->getAllPromotions($searchPromotionArr);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->_auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
            $dashletColumnName = array();
            if (!empty($dashletResult)) {
                $dashletColumn = explode(",", $dashletResult[0]['table_column_names']);
                foreach ($dashletColumn as $val) {
                    if (strpos($val, ".")) {
                        $dashletColumnName[] = trim(strstr($val, ".", false), ".");
                    } else {
                        $dashletColumnName[] = trim($val);
                    }
                }
            }
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            if (!empty($seachResult)) {
                foreach ($seachResult as $val) {
                    $dashletCell = array();

                    $arrCell['id'] = $val['promotion_id'];
                    $encryptId = $this->encrypt($val['promotion_id']);
                    $viewLink = '<a href="/get-crm-promotion-info/' . $encryptId . '/' . $this->encrypt('view') . '" class="view-icon" alt="view promotion"><div class="tooltip">View<span></span></div></a>';
                    $editLink = '<a href="/get-crm-promotion-info/' . $encryptId . '/' . $this->encrypt('edit') . '" class="edit-icon" alt="edit promotion"><div class="tooltip">Edit<span></span></div></a>';
                    $deleteLink = '<a onclick="deletePromotion(\'' . $encryptId . '\')" href="#delete_promotion_content" class="delete_promotion delete-icon" alt="delete promotion"><div class="tooltip">Delete<span></span></div></a>';
                    $actions = '<div class="action-col">' . $viewLink . $editLink . $deleteLink . '</div>';
                    $expiryDate = ($val['expiry_date'] == 0) ? 'N/A' : substr($val['expiry_date'], 0, 10);
                    $startDate = ($val['start_date'] == 0) ? 'N/A' : substr($val['start_date'], 0, 10);
                    if (false !== $dashletColumnKey = array_search('coupon_code', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = '<a class="txt-decoration-underline" href="get-crm-promotion-info/' . $encryptId . '/' . $this->encrypt('view') . '">' . $val['coupon_code'] . '</a>';
                    if (false !== $dashletColumnKey = array_search('promotion_name', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $this->chunkData($val['promotion_name'], 20);
                    if (false !== $dashletColumnKey = array_search('applicable', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $val['applicable'];
                    if (false !== $dashletColumnKey = array_search('expiry_date', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $expiryDate;
                    if (false !== $dashletColumnKey = array_search('discount', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $val['discount'];
                    if (false !== $dashletColumnKey = array_search('start_date', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $startDate;
                    if (isset($dashlet) && $dashlet == 'dashlet') {
                        $arrCell['cell'] = $dashletCell;
                    } else {
                        $arrCell['cell'] = array($val['coupon_code'], $this->chunkData($val['promotion_name'], 20), $val['applicable'], $startDate, $expiryDate, $val['discount'], $actions);
                    }

                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
            } else {
                $jsonResult['rows'] = array();
            }
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        } else {
            $this->layout('crm');
            $messages = array();
            if ($this->_flashMessage->hasMessages()) {
                $messages = $this->_flashMessage->getMessages();
            }

            $viewModel = new ViewModel();
            $response = $this->getResponse();
            $viewModel->setVariables(array(
                'form' => $searchPromotionForm,
                'saveSearchForm' => $saveSearchForm,
                'messages' => $messages,
                'jsLangTranslate' => array_merge($promotionSearchMessage, $commonMessage),
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to create promotion
     * @return json 
     * @param void
     * @author Icreon Tech - DT
     */
    public function addCrmPromotionAction() {
        $this->checkUserAuthentication();
        $this->getPromotionTable();
        $params = $this->params()->fromRoute();
        $promotionCreateMessage = $this->_config['promotion_messages']['config']['promotion_create_message'];
        $commonMessage = $this->_config['promotion_messages']['config']['common_message'];
        $promotion = new Promotion($this->_adapter);
        $createPromotionForm = new PromotionForm();
        /* Applicable for */
        $getApplicableFor = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPromotionApplicable();
        $applicableFor = array();
        if ($getApplicableFor !== false) {
            foreach ($getApplicableFor as $key => $val) {
                $applicableFor[$val['applicable_id']] = $val['applicable'];
            }
        }
        $createPromotionForm->get('applicable_id')->setAttribute('options', $applicableFor);
        /* End  Applicable for */
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $createPromotionForm->setInputFilter($promotion->getInputFilterCreatePromotion());
            $createPromotionForm->setData($request->getPost());
            if (!$createPromotionForm->isValid()) {
                $errors = $createPromotionForm->getMessages();
                $msg = array();

                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $type_error => $rower) {
                            $msg [$key] = $promotionCreateMessage[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            $createPromotionFormArr = $createPromotionForm->getData();
            if ($createPromotionFormArr['applicable_id'] == 2) {
                if (empty($createPromotionFormArr['user_ids'])) {
                    $msg = array();
                    $msg ['user'] = $promotionCreateMessage['EMPTY_CONTACT_LIST'];
                    $messages = array('status' => "error", 'message' => $msg);
                }
            } else if ($createPromotionFormArr['applicable_id'] == 3) {
                if (empty($createPromotionFormArr['group_name_ids'])) {
                    $msg = array();
                    $msg ['group_name'] = $promotionCreateMessage['EMPTY_GROUP_LIST'];
                    $messages = array('status' => "error", 'message' => $msg);
                }
            }
            if ($createPromotionFormArr['product_category'] == 2) {
                if (empty($createPromotionFormArr['category_name_ids'])) {
                    $msg = array();
                    $msg ['category_name'] = $promotionCreateMessage['EMPTY_CATEGORY_LIST'];
                    $messages = array('status' => "error", 'message' => $msg);
                }
            } else if ($createPromotionFormArr['product_category'] == 3) {
                if (empty($createPromotionFormArr['product_name_ids'])) {
                    $msg = array();
                    $msg ['product_name'] = $promotionCreateMessage['EMPTY_PRODUCT_LIST'];
                    $messages = array('status' => "error", 'message' => $msg);
                }
//                else {
//                    $proShipArr = array();
//                    $proShipArr['product_ids'] = $createPromotionFormArr['product_name_ids'];
//                    $allNotShippableElemens = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductNotShippable($proShipArr);
//                    if (!empty($allNotShippableElemens)) {
//                        $msg = array();
//                        $msg ['select_product'] = $promotionCreateMessage['SOME_PRODUCT_NOT_SHIPPABLE'];
//                        $messages = array('status' => "error", 'message' => $msg);
//                    }
//                }
            }



            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $createPromotionFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                $createPromotionFormArr['added_date'] = DATE_TIME_FORMAT;
                $createPromotionFormArr['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                $createPromotionFormArr['modified_date'] = DATE_TIME_FORMAT;
                $couponCode = $this->getCouponCode();
                $createPromotionFormArr['coupon_code'] = $couponCode;
                $createPromotionArr = $promotion->getCreatePromotionArr($createPromotionFormArr);
                $promotionId = $this->_promotionTable->savePromotion($createPromotionArr);
                /* For insert record for contact and group */
                if ($createPromotionFormArr['applicable_id'] != 1) {
                    $applicableIds = ($createPromotionFormArr['applicable_id'] == 2) ? $createPromotionFormArr['user_ids'] : $createPromotionFormArr['group_name_ids'];
                    $appliedFormPromotionFormArr = array();
                    $appliedFormPromotionFormArr['promotion_id'] = $promotionId;
                    $appliedFormPromotionFormArr['applicable_id'] = $createPromotionFormArr['applicable_id'];
                    $appliedFormPromotionFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $appliedFormPromotionFormArr['added_date'] = DATE_TIME_FORMAT;
                    $appliedFormPromotionFormArr['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $appliedFormPromotionFormArr['modified_date'] = DATE_TIME_FORMAT;
                    $applicableIdsArr = explode(',', $applicableIds);
                    foreach ($applicableIdsArr as $appliedId) {
                        $appliedFormPromotionFormArr['applied_id'] = $appliedId;
                        $appliedPromotionArr = $promotion->getAppliedPromotionArr($appliedFormPromotionFormArr);
                        $appliedPromotionId = $this->_promotionTable->saveAppliedPromotion($appliedPromotionArr);
                    }
                }
                /* End for insert record for contact and group */
                /* For insert record for product and category */
                if ($createPromotionFormArr['product_category'] != 1) {
                    $proCatIds = ($createPromotionFormArr['product_category'] == 2) ? $createPromotionFormArr['category_name_ids'] : $createPromotionFormArr['product_name_ids'];
                    $catProFormPromotionFormArr = array();
                    $catProFormPromotionFormArr['promotion_id'] = $promotionId;
                    $catProFormPromotionFormArr['product_category'] = $createPromotionFormArr['product_category'];
                    $catProFormPromotionFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $catProFormPromotionFormArr['added_date'] = DATE_TIME_FORMAT;
                    $catProFormPromotionFormArr['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $catProFormPromotionFormArr['modified_date'] = DATE_TIME_FORMAT;
                    $proCatIdsArr = explode(',', $proCatIds);
                    foreach ($proCatIdsArr as $appliedId) {
                        $catProFormPromotionFormArr['applied_id'] = $appliedId;
                        $proCatPromotionArr = $promotion->getProductCategoryPromotionArr($catProFormPromotionFormArr);
                        $proCatPromotionId = $this->_promotionTable->saveProductCategoryPromotion($proCatPromotionArr);
                    }
                }
                /* End for insert record for contact and group */
                /** insert into the change log */
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_prm_change_logs';
                $changeLogArray['activity'] = '1';
                $changeLogArray['id'] = $promotionId;
                $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                /** end insert into the change log */
                $this->flashMessenger()->addMessage($createPromotionFormArr['promotion_name'] . " successfully created !");
                $messages = array('status' => "success", 'message' => $promotionCreateMessage['PROMOTION_CREATED_SUCCESSFULLY']);
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        } else {
            $this->layout('crm');
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($promotionCreateMessage, $commonMessage),
                'createPromotionForm' => $createPromotionForm
            ));

            return $viewModel;
        }
    }

    /**
     * This function is used to get coupon code
     * @return string
     * @param void
     * @author Icreon Tech - DT
     */
    public function getCouponCode() {
        $this->getPromotionTable();
        $lastPromotionId = $this->_promotionTable->getLastPromotionId();
        $coupon = '100';
        if (empty($lastPromotionId)) {
            $coupon.='01';
        } else {
            $lastPromotionId = $lastPromotionId['lastPromotionId'];
            $nextPromotionId = ($lastPromotionId + 1);
            $coupon.=$nextPromotionId;
        }
        /*$coupon = 'PROMO';
        if (empty($lastPromotionId)) {
            $coupon.='00001';
        } else {
            $lastPromotionId = $lastPromotionId['lastPromotionId'];
            $nextPromotionId = ($lastPromotionId + 1);
            $coupon.= @ str_repeat('0', 5 - strlen($nextPromotionId)) . $nextPromotionId;
        }*/
        return $coupon;
    }

    /**
     * This action is used to edit promotion
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function editCrmPromotionAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $this->getPromotionTable();
            if ($request->getPost('promotion_id') != '') {
                $createPromotionForm = new PromotionForm ();
                /* Applicable for */
                $getApplicableFor = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPromotionApplicable();
                $applicableFor = array();
                if ($getApplicableFor !== false) {
                    foreach ($getApplicableFor as $key => $val) {
                        $applicableFor[$val['applicable_id']] = $val['applicable'];
                    }
                }
                $createPromotionForm->get('applicable_id')->setAttribute('options', $applicableFor);
                /* End  Applicable for */
                $postArray = $request->getPost()->toArray();
                $promotionCreateMessage = $this->_config['promotion_messages']['config']['promotion_create_message'];
                $commonMessage = $this->_config['promotion_messages']['config']['common_message'];
                $promotion = new Promotion($this->_adapter);
                $response = $this->getResponse();
                $createPromotionForm->setInputFilter($promotion->getInputFilterEditPromotion($request->getPost('promotion_id')));
                $createPromotionForm->setData($request->getPost());
                if (!$createPromotionForm->isValid()) {
                    $errors = $createPromotionForm->getMessages();
                    $msg = array();
                    foreach ($errors as $key => $row) {
                        if (!empty($row) && $key != 'continue') {
                            foreach ($row as $rower) {
                                $msg [$key] = $promotionCreateMessage[$rower];
                            }
                        }
                    }
                    $messages = array('status' => "error", 'message' => $msg);
                }

                $editPromotionFormArr = $createPromotionForm->getData();
                if ($editPromotionFormArr['applicable_id'] == 2) {
                    if (empty($editPromotionFormArr['user_ids'])) {
                        $msg = array();
                        $msg ['user'] = $promotionCreateMessage['EMPTY_CONTACT_LIST'];
                        $messages = array('status' => "error", 'message' => $msg);
                    }
                } else if ($editPromotionFormArr['applicable_id'] == 3) {
                    if (empty($editPromotionFormArr['group_name_ids'])) {
                        $msg = array();
                        $msg ['group_name'] = $promotionCreateMessage['EMPTY_GROUP_LIST'];
                        $messages = array('status' => "error", 'message' => $msg);
                    }
                }
                if ($editPromotionFormArr['product_category'] == 2) {
                    if (empty($editPromotionFormArr['category_name_ids'])) {
                        $msg = array();
                        $msg ['category_name'] = $promotionCreateMessage['EMPTY_CATEGORY_LIST'];
                        $messages = array('status' => "error", 'message' => $msg);
                    }
                } else if ($editPromotionFormArr['product_category'] == 3) {
                    if (empty($editPromotionFormArr['product_name_ids'])) {
                        $msg = array();
                        $msg ['product_name'] = $promotionCreateMessage['EMPTY_PRODUCT_LIST'];
                        $messages = array('status' => "error", 'message' => $msg);
                    }
//                    } else {
//                        $proShipArr = array();
//                        $proShipArr['product_ids'] = $editPromotionFormArr['product_name_ids'];
//                        $allNotShippableElemens = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductNotShippable($proShipArr);
//                        if (!empty($allNotShippableElemens)) {
//                            $msg = array();
//                            $msg ['select_product'] = $promotionCreateMessage['SOME_PRODUCT_NOT_SHIPPABLE'];
//                            $messages = array('status' => "error", 'message' => $msg);
//                        }
//                    }
                }
                if (!empty($messages)) {
                    $response->setContent(\Zend\Json\Json::encode($messages));
                } else {
                    if (isset($editPromotionFormArr['promotion_id']) && $editPromotionFormArr['promotion_id'] != '' && $editPromotionFormArr['promotion_id'] != '0') {
                        $promotionDetailArr = array();
                        $promotionDetailArr[] = $editPromotionFormArr['promotion_id'];
                        $promotionDetailArr[] = 'applied';
                        $promotionDetailArr[] = 'Group';
                        $getGroupPromotionAppliedDetail = $this->_promotionTable->getPromotionAppliedByPromotionId($promotionDetailArr);

                        $promotionGroupAppliedArr = array();
                        if (!empty($getGroupPromotionAppliedDetail)) {
                            foreach ($getGroupPromotionAppliedDetail as $groupApplied) {
                                $promotionGroupAppliedArr[] = $groupApplied['applied_id'];
                            }
                        }

                        $promotionDetailArr = array();
                        $promotionDetailArr[] = $editPromotionFormArr['promotion_id'];
                        $promotionDetailArr[] = 'applied';
                        $promotionDetailArr[] = 'Contact';
                        $getContactPromotionAppliedDetail = $this->_promotionTable->getPromotionAppliedByPromotionId($promotionDetailArr);
                        $promotionContactAppliedArr = array();
                        if (!empty($getContactPromotionAppliedDetail)) {
                            foreach ($getContactPromotionAppliedDetail as $contactApplied) {
                                $promotionContactAppliedArr[] = $contactApplied['applied_id'];
                            }
                        }

                        $promotionDetailArr = array();
                        $promotionDetailArr[] = $editPromotionFormArr['promotion_id'];
                        $promotionDetailArr[] = 'category';
                        $promotionDetailArr[] = '2';
                        $getCategoryPromotionAppliedDetail = $this->_promotionTable->getPromotionAppliedByPromotionId($promotionDetailArr);
                        $promotionCategoryAppliedArr = array();
                        if (!empty($getCategoryPromotionAppliedDetail)) {
                            foreach ($getCategoryPromotionAppliedDetail as $categoryApplied) {
                                $promotionCategoryAppliedArr[] = $categoryApplied['applied_id'];
                            }
                        }


                        $promotionDetailArr = array();
                        $promotionDetailArr[] = $editPromotionFormArr['promotion_id'];
                        $promotionDetailArr[] = 'category';
                        $promotionDetailArr[] = '3';
                        $getProductPromotionAppliedDetail = $this->_promotionTable->getPromotionAppliedByPromotionId($promotionDetailArr);
                        $promotionProductAppliedArr = array();
                        if (!empty($getProductPromotionAppliedDetail)) {
                            foreach ($getProductPromotionAppliedDetail as $productApplied) {
                                $promotionProductAppliedArr[] = $productApplied['applied_id'];
                            }
                        }

                        $removePromotionContactAppliedArr = array();
                        $addPromotionContactAppliedArr = array();
                        if ($editPromotionFormArr['applicable_id'] == 2) {
                            if ($editPromotionFormArr['user_ids'] != '') {
                                $userIdsArr = explode(',', $editPromotionFormArr['user_ids']);
                                $removePromotionContactAppliedArr = array_diff($promotionContactAppliedArr, $userIdsArr);
                                $addPromotionContactAppliedArr = array_diff($userIdsArr, $promotionContactAppliedArr);
                            }
                        } else {
                            $removePromotionContactAppliedArr = $promotionContactAppliedArr;
                        }

                        $removePromotionGroupAppliedArr = array();
                        $addPromotionGroupAppliedArr = array();
                        if ($editPromotionFormArr['applicable_id'] == 3) {
                            if ($editPromotionFormArr['group_name_ids'] != '') {
                                $groupNameIdsArr = explode(',', $editPromotionFormArr['group_name_ids']);
                                $removePromotionGroupAppliedArr = array_diff($promotionGroupAppliedArr, $groupNameIdsArr);
                                $addPromotionGroupAppliedArr = array_diff($groupNameIdsArr, $promotionGroupAppliedArr);
                            }
                        } else {
                            $removePromotionGroupAppliedArr = $promotionGroupAppliedArr;
                        }

                        $removePromotionCategoryAppliedArr = array();
                        $addPromotionCategoryAppliedArr = array();
                        if ($editPromotionFormArr['product_category'] == 2) {
                            if ($editPromotionFormArr['category_name_ids'] != '') {
                                $categoryNameIdsArr = explode(',', $editPromotionFormArr['category_name_ids']);
                                $removePromotionCategoryAppliedArr = array_diff($promotionCategoryAppliedArr, $categoryNameIdsArr);
                                $addPromotionCategoryAppliedArr = array_diff($categoryNameIdsArr, $promotionCategoryAppliedArr);
                            }
                        } else {
                            $removePromotionCategoryAppliedArr = $promotionCategoryAppliedArr;
                        }

                        $removePromotionProductAppliedArr = array();
                        $addPromotionProductAppliedArr = array();
                        if ($editPromotionFormArr['product_category'] == 3) {
                            if ($editPromotionFormArr['product_name_ids'] != '') {
                                $productNameIdsArr = explode(',', $editPromotionFormArr['product_name_ids']);
                                $removePromotionProductAppliedArr = array_diff($promotionProductAppliedArr, $productNameIdsArr);
                                $addPromotionProductAppliedArr = array_diff($productNameIdsArr, $promotionProductAppliedArr);
                            }
                        } else {
                            $removePromotionProductAppliedArr = $promotionProductAppliedArr;
                        }

                        if ($editPromotionFormArr['applicable_id'] != 1) {
                            $appliedFormPromotionFormArr = array();
                            $appliedFormPromotionFormArr['promotion_id'] = $editPromotionFormArr['promotion_id'];
                            $appliedFormPromotionFormArr['applicable_id'] = $editPromotionFormArr['applicable_id'];
                            $appliedFormPromotionFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                            $appliedFormPromotionFormArr['added_date'] = DATE_TIME_FORMAT;
                            $appliedFormPromotionFormArr['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                            $appliedFormPromotionFormArr['modified_date'] = DATE_TIME_FORMAT;
                            $applicableIdsArr = array_merge($addPromotionContactAppliedArr, $addPromotionGroupAppliedArr);
                            if (!empty($applicableIdsArr)) {
                                foreach ($applicableIdsArr as $appliedId) {
                                    $appliedFormPromotionFormArr['applied_id'] = $appliedId;
                                    $appliedPromotionArr = $promotion->getAppliedPromotionArr($appliedFormPromotionFormArr);
                                    $appliedPromotionId = $this->_promotionTable->saveAppliedPromotion($appliedPromotionArr);
                                }
                            }
                        }
                        /* End for insert record for contact and group */
                        /* For insert record for product and category */
                        if ($editPromotionFormArr['product_category'] != 1) {
                            $catProFormPromotionFormArr = array();
                            $catProFormPromotionFormArr['promotion_id'] = $editPromotionFormArr['promotion_id'];
                            $catProFormPromotionFormArr['product_category'] = $editPromotionFormArr['product_category'];
                            $catProFormPromotionFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                            $catProFormPromotionFormArr['added_date'] = DATE_TIME_FORMAT;
                            $catProFormPromotionFormArr['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                            $catProFormPromotionFormArr['modified_date'] = DATE_TIME_FORMAT;
                            $proCatIdsArr = array_merge($addPromotionCategoryAppliedArr, $addPromotionProductAppliedArr);
                            if (!empty($proCatIdsArr)) {
                                foreach ($proCatIdsArr as $appliedId) {
                                    $catProFormPromotionFormArr['applied_id'] = $appliedId;
                                    $proCatPromotionArr = $promotion->getProductCategoryPromotionArr($catProFormPromotionFormArr);
                                    $proCatPromotionId = $this->_promotionTable->saveProductCategoryPromotion($proCatPromotionArr);
                                }
                            }
                        }
                        /* End for insert record for contact and group */
                        /* For remove record for contact */
                        if (!empty($removePromotionContactAppliedArr)) {
                            $appliedFormRemovePromotionArr = array();
                            $appliedFormRemovePromotionArr['promotion_id'] = $editPromotionFormArr['promotion_id'];
                            $appliedFormRemovePromotionArr['type'] = 'applied';
                            $appliedFormRemovePromotionArr['applicable_id'] = 2;
                            $appliedFormRemovePromotionArr['applied_ids'] = implode(',', $removePromotionContactAppliedArr);
                            $appliedPromotionRemoveArr = $promotion->getAppliedPromotionRemoveArr($appliedFormRemovePromotionArr);
                            $this->_promotionTable->removeAppliedPromotion($appliedPromotionRemoveArr);
                        }
                        /* End for remove record for contact */
                        /* For remove record for group */
                        if (!empty($removePromotionGroupAppliedArr)) {
                            $appliedFormRemovePromotionArr = array();
                            $appliedFormRemovePromotionArr['promotion_id'] = $editPromotionFormArr['promotion_id'];
                            $appliedFormRemovePromotionArr['type'] = 'applied';
                            $appliedFormRemovePromotionArr['applicable_id'] = 3;
                            $appliedFormRemovePromotionArr['applied_ids'] = implode(',', $removePromotionGroupAppliedArr);
                            $appliedPromotionRemoveArr = $promotion->getAppliedPromotionRemoveArr($appliedFormRemovePromotionArr);
                            $this->_promotionTable->removeAppliedPromotion($appliedPromotionRemoveArr);
                        }
                        /* End for remove record for group */
                        /* For remove record for category */
                        if (!empty($removePromotionCategoryAppliedArr)) {
                            $appliedFormRemovePromotionArr = array();
                            $appliedFormRemovePromotionArr['promotion_id'] = $editPromotionFormArr['promotion_id'];
                            $appliedFormRemovePromotionArr['type'] = 'category';
                            $appliedFormRemovePromotionArr['applicable_id'] = 2;
                            $appliedFormRemovePromotionArr['applied_ids'] = implode(',', $removePromotionCategoryAppliedArr);
                            $appliedPromotionRemoveArr = $promotion->getAppliedPromotionRemoveArr($appliedFormRemovePromotionArr);
                            $this->_promotionTable->removeAppliedPromotion($appliedPromotionRemoveArr);
                        }
                        /* End for remove record for category */
                        /* For remove record for product */
                        if (!empty($removePromotionProductAppliedArr)) {
                            $appliedFormRemovePromotionArr = array();
                            $appliedFormRemovePromotionArr['promotion_id'] = $editPromotionFormArr['promotion_id'];
                            $appliedFormRemovePromotionArr['type'] = 'category';
                            $appliedFormRemovePromotionArr['applicable_id'] = 3;
                            $appliedFormRemovePromotionArr['applied_ids'] = implode(',', $removePromotionProductAppliedArr);
                            $appliedPromotionRemoveArr = $promotion->getAppliedPromotionRemoveArr($appliedFormRemovePromotionArr);
                            $this->_promotionTable->removeAppliedPromotion($appliedPromotionRemoveArr);
                        }
                        /* End for remove record for product */

                        $editPromotionFormArr['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                        $editPromotionFormArr['modified_date'] = DATE_TIME_FORMAT;
                        $editPromotionArr = $promotion->geteditPromotionArr($editPromotionFormArr);
                        $promotionId = $this->_promotionTable->editPromotion($editPromotionArr);

                        /** insert into the change log */
                        $changeLogArray = array();
                        $changeLogArray['table_name'] = 'tbl_prm_change_logs';
                        $changeLogArray['activity'] = '2';
                        $changeLogArray['id'] = $promotionId;
                        $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                        $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                        $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                        $this->flashMessenger()->addMessage(ucfirst($editPromotionFormArr['promotion_name']) . " successfully updated !");
                        $messages = array('status' => "success", 'message' => $promotionCreateMessage['PROMOTION_UPDATED_SUCCESSFULLY']);
                    } else {
                        $this->flashMessenger()->addMessage($promotionCreateMessage['PROMOTION_UPDATED_FAIL']);
                        $messages = array('status' => "fail", 'message' => $promotionCreateMessage['PROMOTION_UPDATED_FAIL']);
                    }
                    $response->setContent(\Zend\Json\Json::encode($messages));
                }
                return $response;
            } else {
                return $this->redirect()->toRoute('getPromotions');
            }
        } else {
            return $this->redirect()->toRoute('getPromotions');
        }
    }

    /**
     * This Action is used to save the promotion search
     * @param this will pass an array $saveSearchParam
     * @return this will be json format with comfirmation message
     * @author Icreon Tech -DT
     */
    public function savePromotionSearchAction() {
        $this->checkUserAuthentication();
        $this->getPromotionTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $promotion = new Promotion($this->_adapter);
        $promotionSearchMessages = $this->_config['promotion_messages']['config']['promotion_search_message'];
        if ($request->isPost()) {
            parse_str($request->getPost('searchString'), $searchParam);
            $searchName = $request->getPost('search_name');
            $messages = array();
            parse_str($request->getPost('searchString'), $searchParam);
            $searchParam['search_title'] = $searchName;
            $searchParam = $promotion->getInputFilterPromotionSearch($searchParam);

            $searchName = $promotion->getInputFilterPromotionSearch($searchName);

            if (trim($searchName) != '') {
                $saveSearchParam['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $saveSearchParam['title'] = $searchName;
                $saveSearchParam['search_query'] = serialize($searchParam);
                $saveSearchParam['is_active'] = 1;
                $saveSearchParam['is_delete'] = '0';

                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modified_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['promotion_search_id'] = $request->getPost('search_id');
                    $promotionUpdateArray = $promotion->getAddPromotionSearchArr($saveSearchParam);
                    if ($this->_promotionTable->updatePromotionSearch($promotionUpdateArray) == true) {
                        $this->flashMessenger()->addMessage($promotionSearchMessages['SAVE_UPDATED_MSG']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {
                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    $activityAddArray = $promotion->getAddPromotionSearchArr($saveSearchParam);
                    if ($this->_promotionTable->savePromotionSearch($activityAddArray) == true) {
                        $this->flashMessenger()->addMessage($promotionSearchMessages['SAVE_SUCCESS_MSG']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used to get the saved promotion search
     * @param this will pass an array $dataParam
     * @return this will be json format
     * @author Icreon Tech - DT
     */
    public function getPromotionSearchSavedParamAction() {
        $this->checkUserAuthentication();
        $this->getPromotionTable();
        $promotion = new Promotion($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {

                $searchArray = array();
                $searchArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $searchArray['promotion_search_id'] = $request->getPost('search_id');
                $searchArray['is_active'] = '1';
                $promotionSavedSearchArray = $promotion->getPromotionSavedSearchArr($searchArray);
                $promotionSearchArray = $this->_promotionTable->getPromotionSavedSearch($promotionSavedSearchArray);
                $searchResult = json_encode(unserialize($promotionSearchArray[0]['search_query']));
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This Action is used to delete the the saved promotion search
     * @param this will take an array $dataParam
     * @return this will be a confirmation message in json
     * @author Icreon Tech - DT
     */
    public function deleteCrmSearchPromotionAction() {
        $this->checkUserAuthentication();
        $this->getPromotionTable();
        $promotion = new Promotion($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $promotionSearchMessages = $this->_config['promotion_messages']['config']['promotion_search_message'];
        if ($request->isPost()) {
            $deleteArray = array();
            $deleteArray['is_deleted'] = 1;
            $deleteArray['modified_date'] = DATE_TIME_FORMAT;
            $deleteArray['promotion_search_id'] = $request->getPost('search_id');
            $promotionDeleteSavedSearchArray = $promotion->getDeletePromotionSavedSearchArr($deleteArray);
            $this->_promotionTable->deleteSavedSearch($promotionDeleteSavedSearchArray);
            $messages = array('status' => "success");
            $this->flashMessenger()->addMessage($promotionSearchMessages['SEARCH_DELETE_CONFIRM']);
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This action is used to delete promotion
     * @param  void
     * @return json format string
     * @author Icreon Tech - DT
     */
    public function deleteCrmPromotionAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $promotionId = $request->getPost('promotionId');
            if ($promotionId != '') {
                $promotionId = $this->decrypt($promotionId);
                $postArr = $request->getPost();
                $postArr['promotion_id'] = $promotionId;
                $this->getPromotionTable();
                $promotionSearchMessages = $this->_config['promotion_messages']['config']['promotion_search_message'];
                $promotion = new Promotion($this->_adapter);

                $response = $this->getResponse();

                $promotionArr = $promotion->getArrayForRemovePromotion($postArr);
                $this->_promotionTable->removePromotionById($promotionArr);

                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_prm_change_logs';
                $changeLogArray['activity'] = '3';
                $changeLogArray['id'] = $promotionId;
                $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                /** end insert into the change log */
                $promotionDetailArr = array();
                $promotionDetailArr[] = $promotionId;
                $promotionData = $this->_promotionTable->getPromotionById($promotionDetailArr);
                $this->flashMessenger()->addMessage($promotionData['promotion_name'] . " successfully deleted !");
                $messages = array('status' => "success", 'message' => $promotionSearchMessages['PROMOTION_DELETED_SUCCESSFULLY']);

                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                return $this->redirect()->toRoute('getPromotions');
            }
        } else {
            return $this->redirect()->toRoute('getPromotions');
        }
    }

    /**
     * This Action is used to set post array elements value to form
     * @param this will pass an array $searchParam
     * @return void
     * @author Icreon Tech -DT
     */
    public function setValueForElement($postArr = array(), $form = array()) {
        if (!is_array($postArr)) {
            $postArr = $postArr->toArray();
        }
        $elements = $form->getElements();
        foreach ($elements as $elementName => $elementObject) {

            if (array_key_exists($elementName, $postArr)) {
                if (is_array($postArr[$elementName])) {
                    $value = implode(',', $postArr[$elementName]);
                } else {

                    $value = $postArr[$elementName];
                }
                $elementObject->setValue($value);
            }
        }
    }   

    /**
     * This action is used to show all promotion detail
     * @param void
     * @return array
     * @author Icreon Tech - DT
     */
    public function getCrmPromotionInfoAction() {
        $this->checkUserAuthentication();
        $this->getPromotionTable();
        $promotionCreateMessages = $this->_config['promotion_messages']['config']['promotion_create_message'];
        $commonMessage = $this->_config['promotion_messages']['config']['common_message'];
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        if (isset($params['promotionId']) && $params['promotionId'] != '') {
            $this->layout('crm');
            $this->getPromotionTable();
            $promotionId = $this->decrypt($params['promotionId']);
            $promotionDetailArr = array();
            $promotionDetailArr[] = $promotionId;
            $promotionData = $this->_promotionTable->getPromotionById($promotionDetailArr);
            $mode = (isset($params['mode']) && $params['mode'] != '') ? $this->decrypt($params['mode']) : 'view';
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($promotionCreateMessages, $commonMessage),
                'promotionId' => $promotionId,
                'encryptPromotionId' => $params['promotionId'],
                'promotionData' => $promotionData,
                'moduleName' => $this->encrypt('promotion'),
                'mode' => $mode
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to show view promotion detail on ajax call
     * @param void
     * @return     array
     * @author Icreon Tech - DT
     */
    public function viewCrmPromotionInfoAction() {
        $this->checkUserAuthentication();
        $this->getPromotionTable();
        $promotionSearchMessage = $this->_config['promotion_messages']['config']['promotion_create_message'];
        $commonMessage = $this->_config['promotion_messages']['config']['common_message'];
        $promotion = new Promotion($this->_adapter);
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        if (isset($params['promotionId']) && $params['promotionId'] != '') {
            $promotionId = $this->decrypt($params['promotionId']);
            $promotionDetailArr = array();
            $promotionDetailArr[] = $promotionId;
            $promotionData = $this->_promotionTable->getPromotionById($promotionDetailArr);
            $promotionDetailArr[] = '';
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($promotionSearchMessage, $commonMessage),
                'promotionId' => $promotionId,
                'encryptPromotionId' => $params['promotionId'],
                'promotionData' => $promotionData
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to get edit promotion detail page
     * @return string
     * @param void
     * @author Icreon Tech - DT
     */
    public function editCrmPromotionDetailAction() {
        $this->checkUserAuthentication();
        $this->getPromotionTable();
        $promotionCreateMessage = $this->_config['promotion_messages']['config']['promotion_create_message'];
        $commonMessage = $this->_config['promotion_messages']['config']['common_message'];
        $promotion = new Promotion($this->_adapter);
        $createPromotionForm = new PromotionForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $createPromotionForm->get('savepromotion')->setValue('UPDATE');

        /* Applicable for */
        $getApplicableFor = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPromotionApplicable();
        if ($getApplicableFor !== false) {
            foreach ($getApplicableFor as $key => $val) {
                $applicableFor[$val['applicable_id']] = $val['applicable'];
            }
        }
        $createPromotionForm->get('applicable_id')->setAttributes(array('options' => $applicableFor));
        /* End  Applicable for */

        $promotionId = '';
        $params = $this->params()->fromRoute();

        if (isset($params['promotionId']) && $params['promotionId'] != '') {
            $promotionId = $this->decrypt($params['promotionId']);
            $promotionDetailArr = array();
            $promotionDetailArr[] = $promotionId;
            $getPromotionDetail = $this->_promotionTable->getPromotionById($promotionDetailArr);
            $getPromotionDetail['expiry_date'] = ($getPromotionDetail['expiry_date'] == 0) ? '' : $this->DateFormat($getPromotionDetail['expiry_date'], 'calender');
            $getPromotionDetail['start_date'] = ($getPromotionDetail['start_date'] == 0) ? '' : $this->DateFormat($getPromotionDetail['start_date'], 'calender');
            $getPromotionDetail['is_excluded_applied'] = isset($getPromotionDetail['is_excluded_applicable']) ? $getPromotionDetail['is_excluded_applicable'] : '';
            $getPromotionDetail['is_excluded_product_category'] = isset($getPromotionDetail['is_excluded_product_type']) ? $getPromotionDetail['is_excluded_product_type'] : '';
            $getPromotionDetail['product_category'] = isset($getPromotionDetail['product_type']) ? $getPromotionDetail['product_type'] : '';
            $getPromotionAppliedDetail = array();
            if ($getPromotionDetail['applicable_id'] != 1) {
                $promotionDetailArr[] = 'applied';
                $promotionDetailArr[] = $getPromotionDetail['applicable'];
                $getPromotionAppliedDetail = $this->_promotionTable->getPromotionAppliedByPromotionId($promotionDetailArr);
            }
            $getPromotionCatProDetail = array();
            if ($getPromotionDetail['product_type'] != 1) {
                $promotionDetailArr[1] = 'category';
                $promotionDetailArr[2] = $getPromotionDetail['product_type'];
                $getPromotionCatProDetail = $this->_promotionTable->getPromotionAppliedByPromotionId($promotionDetailArr);
            }
            $createPromotionForm->get('promotion_id')->setValue($promotionId);
            $this->setValueForElement($getPromotionDetail, $createPromotionForm);
            switch ($getPromotionDetail['applicable_to']) {
                case '1' :
                    $createPromotionForm->get('is_domestic')->setValue('1');
                    break;
                case '2' :
                    $createPromotionForm->get('is_international')->setValue('1');
                    break;
                case '3' :
                    $createPromotionForm->get('is_domestic')->setValue('1');
                    $createPromotionForm->get('is_international')->setValue('1');
                    break;
            }
        }
        $encryptedPromotionId = (isset($params['promotionId']) && $params['promotionId'] != '') ? $params['promotionId'] : '';
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => array_merge($promotionCreateMessage, $commonMessage),
            'createPromotionForm' => $createPromotionForm,
            'promotionDetail' => $getPromotionDetail,
            'promotionId' => $promotionId,
            'promotionAppliedDetail' => $getPromotionAppliedDetail,
            'PromotionCatProDetail' => $getPromotionCatProDetail,
            'encryptedPromotionId' => $encryptedPromotionId
        ));

        return $viewModel;
    }

    /**
     * This function is used to get coupon codes
     * @return     array
     * @param array $form
     * @author Icreon Tech - DT
     */
    public function getCouponCodeAction() {
        $this->checkUserAuthentication();
        $this->getPromotionTable();
        $promotion = new Promotion($this->_adapter);
        $response = $this->getResponse();
        /* Contacts */
        $request = $this->getRequest();
        $postArr = $request->getPost()->toArray();
        $postArr['coupon_code'] = trim($postArr['coupon_code']);
        $couponArr = $promotion->getCouponArr($postArr);
        $coupons = $this->_promotionTable->getCoupons($couponArr);
        $allCoupons = array();
        $i = 0;
        if ($coupons !== false) {
            foreach ($coupons as $coupon) {
                $allCoupons[$i] = array();
                $allCoupons[$i]['promotion_id'] = $coupon['promotion_id'];
                $allCoupons[$i]['coupon_code'] = $coupon['coupon_code'];
                $i++;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($allCoupons));
        return $response;
        /* End Contacts */
    }

    public function createCrmPromotionAction() {
        $this->checkUserAuthentication();
        $this->getPromotionTable();
        $params = $this->params()->fromRoute();
        $promotionCreateMessage = $this->_config['promotion_messages']['config']['promotion_create_message'];
        $commonMessage = $this->_config['promotion_messages']['config']['common_message'];
        $promotion = new Promotion($this->_adapter);
        $createPromotionForm = new ProductPromotionForm();
        /* Applicable for */
        $getApplicableFor = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPromotionApplicable();
        $applicableFor = array();
        if ($getApplicableFor !== false) {
            foreach ($getApplicableFor as $key => $val) {
                $applicableFor[$val['applicable_id']] = $val['applicable'];
            }
        }
        $createPromotionForm->get('applicable_id')->setAttribute('options', $applicableFor);
        /* End  Applicable for */

		$applicationConfigIds = $this->_config['application_config_ids'];

		$transaction = new Transaction($this->_adapter);

		$shippingMetArr = array();
		$shippingMetArr['pb_shipping_id'] = $applicationConfigIds['domestic_pb_id'];
		$shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
		$getDomesticShippingMethods = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getShippingMethod($shippingMethodArr);

		$shippingMetArr = array();
		$shippingMetArr['pb_shipping_id'] = $applicationConfigIds['internation_pb_id'];
		$shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
		$getInternationalShippingMethods = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getShippingMethod($shippingMethodArr);
        
		$request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $postedData = $request->getPost()->toArray();
            
            if ($postedData['applicable_id'] == 2) {
                if (empty($postedData['user_ids'])) {
                    $msg = array();
                    $msg ['user'] = $promotionCreateMessage['EMPTY_CONTACT_LIST'];
                    $messages = array('status' => "error", 'message' => $msg);
                }
            } else if ($postedData['applicable_id'] == 3) {
                if (empty($postedData['group_name_ids'])) {
                    $msg = array();
                    $msg ['group_name'] = $promotionCreateMessage['EMPTY_GROUP_LIST'];
                    $messages = array('status' => "error", 'message' => $msg);
                }
            }
			if(isset($postedData['is_free_product']) && $postedData['is_free_product'] == 1){
				if(empty($postedData['free_products'])){
					$msg = array();
                    $msg ['free_product_name'] = $promotionCreateMessage['EMPTY_FREE_PRODUCT_LIST'];
                    $messages = array('status' => "error", 'message' => $msg);
				}
			}
            if(isset($postedData['promotion_qualified_by'])){
				
				if($postedData['promotion_qualified_by'] == 2){
					if(empty($postedData['bundle_product_type']) && empty($postedData['bundle_products'])){
						$msg = array();
						$msg ['bundle_product_type_name'] = $promotionCreateMessage['EMPTY_ANY_BUNDLE_PRODUCT_LIST'];
						$messages = array('status' => "error", 'message' => $msg);
					}
				}elseif($postedData['promotion_qualified_by'] == 3){
					if(empty($postedData['promotion_any_product_type']) && empty($postedData['promotion_any_products'])){
						$msg = array();
						$msg ['promotion_any_product_type_name'] = $promotionCreateMessage['EMPTY_ANY_PRODUCT_TYPE_LIST'];
						$messages = array('status' => "error", 'message' => $msg);
					}
				}
				
			}



            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $promotionSaveData = array();

                $promotionSaveData = $postedData;

				$couponCode = $this->getCouponCode();
                $promotionSaveData['coupon_code'] = $couponCode;
                $promotionSaveData['start_date'] = (!is_null($postedData['start_date']) && $postedData['start_date'] != '') ? $this->InputDateFormat($postedData['start_date'], 'db_date_format') : '';
                $promotionSaveData['expiry_date'] = (!is_null($postedData['expiry_date']) && $postedData['expiry_date'] != '') ? $this->InputDateFormat($postedData['expiry_date'], 'db_date_format') : '';
				
				
				if(isset($postedData['shipping_address_location']) && (!empty($postedData['shipping_address_location']))){ 
					if(in_array(1,$postedData['shipping_address_location']) && in_array(3,$postedData['shipping_address_location'])){ 
						$promotionSaveData['applicable_to'] = '3';
					}elseif(implode(",",$postedData['shipping_address_location']) == 1){
						$promotionSaveData['applicable_to'] = '1';
					}elseif(implode(",",$postedData['shipping_address_location']) == 3){
						$promotionSaveData['applicable_to'] = '2';
					}
				}
				
                $promotionSaveData['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                $promotionSaveData['added_date'] = DATE_TIME_FORMAT;
                $promotionSaveData['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                $promotionSaveData['modified_date'] = DATE_TIME_FORMAT;

                $createPromotionArr = $promotion->getCreatePromotionArray($promotionSaveData);
				$promotionId = $this->_promotionTable->saveProductPromotion($createPromotionArr);

                /* For insert record for contact and group */
                if ($postedData['applicable_id'] != 1) {
                    $applicableIds = ($postedData['applicable_id'] == 2) ? $postedData['user_ids'] : $postedData['group_name_ids'];
                    $appliedFormPromotionFormArr = array();
                    $appliedFormPromotionFormArr['promotion_id'] = $promotionId;
                    $appliedFormPromotionFormArr['applicable_id'] = $postedData['applicable_id'];
                    $appliedFormPromotionFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $appliedFormPromotionFormArr['added_date'] = DATE_TIME_FORMAT;
                    $appliedFormPromotionFormArr['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $appliedFormPromotionFormArr['modified_date'] = DATE_TIME_FORMAT;
                    $applicableIdsArr = explode(',', $applicableIds);
                    foreach ($applicableIdsArr as $appliedId) {
                        $appliedFormPromotionFormArr['applied_id'] = $appliedId;
                        $appliedPromotionArr = $promotion->getAppliedPromotionArr($appliedFormPromotionFormArr);
                        $appliedPromotionId = $this->_promotionTable->saveAppliedPromotion($appliedPromotionArr);
                    }
                }
                /* End for insert record for contact and group */

                /** insert into the change log */
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_prm_change_logs';
                $changeLogArray['activity'] = '1';
                $changeLogArray['id'] = $promotionId;
                $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                /** end insert into the change log */
                $this->flashMessenger()->addMessage($createPromotionFormArr['promotion_name'] . " successfully created !");
                $messages = array('status' => "success", 'message' => $promotionCreateMessage['PROMOTION_CREATED_SUCCESSFULLY']);
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        } else {
            $this->layout('crm');
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($promotionCreateMessage, $commonMessage),
                'createPromotionForm' => $createPromotionForm,
				'applicationConfigIds' => $applicationConfigIds,
				'getDomesticShippingMethods' => $getDomesticShippingMethods,
				'getInternationalShippingMethods' => $getInternationalShippingMethods
            ));

            return $viewModel;
        }
    }

    public function getPromotionalProductsAction() {
        $this->checkUserAuthentication();
        $this->getPromotionTable();
        $response = $this->getResponse();

        $request = $this->getRequest();
        $data = $request->getPost()->toArray();

        $productList = $this->getPromotionTable()->getPromotionProducts($data);
        $promotional_products = array();

        $i = 0;
        if ($productList !== false) {
            foreach ($productList as $key => $value) {
                $promotional_products[$i] = array();
                $promotional_products[$i]['product_id'] = $value['product_id'];
                $promotional_products[$i]['product_name'] = $value['product_name'];

                $i++;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($promotional_products));
        return $response;
    }

    public function getCrmProductPromotionAction() {
        $this->checkUserAuthentication();
        $this->getPromotionTable();
        $params = $this->params()->fromRoute();
        $promotionSearchMessage = $this->_config['promotion_messages']['config']['promotion_search_message'];
        $commonMessage = $this->_config['promotion_messages']['config']['common_message'];

        $promotion = new Promotion($this->_adapter);
        $searchPromotionForm = new SearchPromotionForm();
        $saveSearchForm = new SaveSearchForm();
        $request = $this->getRequest();
        /* Applicable for */
        $getApplicableFor = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPromotionApplicable();
        $applicableFor = array('' => 'Select');
        if ($getApplicableFor !== false) {
            foreach ($getApplicableFor as $key => $val) {
                $applicableFor[$val['applicable_id']] = $val['applicable'];
            }
        }
        $applicableFor['2,3'] = 'Contact and Group Both';
        $searchPromotionForm->get('applicable_id')->setAttributes(array('options' => $applicableFor));
        /* End  Applicable for */

		/* Get date range */
        $promotionDateRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
        $dateRange = array("" => "Select One");
        foreach ($promotionDateRange as $key => $val) {
            $dateRange[$val['range_id']] = $val['range'];
        }
        $searchPromotionForm->get('expiry_date_range')->setAttribute('options', $dateRange);
        /* end Get date range */

        /* Saved search data */
        $getSearchArray = array();
        $getSearchArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
        $getSearchArray['promotion_search_id'] = '';
        $getSearchArray['is_active'] = '1';
        $promotionSavedSearchArray = $promotion->getPromotionSavedSearchArr($getSearchArray);
        $promotionSearchArray = $this->_promotionTable->getPromotionSavedSearch($promotionSavedSearchArray);
        $promotionSearchList = array();
        $promotionSearchList[''] = 'Select';
        foreach ($promotionSearchArray as $key => $val) {
            $promotionSearchList[$val['promotion_search_id']] = stripslashes($val['title']);
        }
        $searchPromotionForm->get('saved_search')->setAttribute('options', $promotionSearchList);

        /* end  Saved search data */
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $response = $this->getResponse();
            $postArray = $request->getPost()->toArray();
            parse_str($request->getPost('searchString'), $searchParam);
            $dashlet = $request->getPost('crm_dashlet');
            $dashletSearchId = $request->getPost('searchId');
            $dashletId = $request->getPost('dashletId');
            $dashletSearchType = $request->getPost('searchType');
            $dashletSearchString = $request->getPost('dashletSearchString');
            if (isset($dashletSearchString) && $dashletSearchString != '') {
                $searchParam = unserialize($dashletSearchString);
            }
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit; // do not put $limit*($page - 1)
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            if (isset($searchParam['expiry_date_range']) && $searchParam['expiry_date_range'] != 1 && $searchParam['expiry_date_range'] != '') {
                $dateRange = $this->getDateRange($searchParam['expiry_date_range']);
                $searchParam['expiry_date_from'] = $this->DateFormat($dateRange['from'], 'db_date_format_from');
                $searchParam['expiry_date_to'] = $this->DateFormat($dateRange['to'], 'db_date_format_to');
            } else if (isset($searchParam['expiry_date_range']) && $searchParam['expiry_date_range'] == '') {
                $searchParam['expiry_date_from'] = '';
                $searchParam['expiry_date_to'] = '';
            } else {
                if (isset($searchParam['expiry_date_from']) && $searchParam['expiry_date_from'] != '') {
                    $searchParam['expiry_date_from'] = $this->DateFormat($searchParam['expiry_date_from'], 'db_date_format_from');
                }
                if (isset($searchParam['expiry_date_to']) && $searchParam['expiry_date_to'] != '') {
                    $searchParam['expiry_date_to'] = $this->DateFormat($searchParam['expiry_date_to'], 'db_date_format_to');
                }
            }
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'tbl_promotion.modified_date' : $searchParam['sort_field'];
			//asd($searchParam);
            $searchPromotionArr = $promotion->getPromotionSearchArray($searchParam);

            $seachResult = $this->_promotionTable->getAllProductPromotions($searchPromotionArr);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->_auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
            $dashletColumnName = array();
            if (!empty($dashletResult)) {
                $dashletColumn = explode(",", $dashletResult[0]['table_column_names']);
                foreach ($dashletColumn as $val) {
                    if (strpos($val, ".")) {
                        $dashletColumnName[] = trim(strstr($val, ".", false), ".");
                    } else {
                        $dashletColumnName[] = trim($val);
                    }
                }
            }
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            if (!empty($seachResult)) {
                foreach ($seachResult as $val) {
                    $dashletCell = array();

                    $arrCell['id'] = $val['promotion_id'];
                    $encryptId = $this->encrypt($val['promotion_id']);
                    $viewLink = '<a href="/get-crm-product-promotion-info/' . $encryptId . '/' . $this->encrypt('view') . '" class="view-icon" alt="view promotion"><div class="tooltip">View<span></span></div></a>';
                    $editLink = '<a href="/get-crm-product-promotion-info/' . $encryptId . '/' . $this->encrypt('edit') . '" class="edit-icon" alt="edit promotion"><div class="tooltip">Edit<span></span></div></a>';
                    $deleteLink = '<a onclick="deleteProductPromotion(\'' . $encryptId . '\')" href="#delete_promotion_content" class="delete_promotion delete-icon" alt="delete promotion"><div class="tooltip">Delete<span></span></div></a>';
                    $actions = '<div class="action-col">' . $viewLink . $editLink . $deleteLink . '</div>';
                    $expiryDate = ($val['expiry_date'] == 0) ? 'N/A' : substr($val['expiry_date'], 0, 10);
                    $startDate = ($val['start_date'] == 0) ? 'N/A' : substr($val['start_date'], 0, 10);
                    if (false !== $dashletColumnKey = array_search('coupon_code', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = '<a class="txt-decoration-underline" href="get-crm-promotion-info/' . $encryptId . '/' . $this->encrypt('view') . '">' . $val['coupon_code'] . '</a>';
                    if (false !== $dashletColumnKey = array_search('promotion_name', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $this->chunkData($val['promotion_name'], 20);
                    if (false !== $dashletColumnKey = array_search('applicable', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $val['applicable'];
                    if (false !== $dashletColumnKey = array_search('expiry_date', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $expiryDate;
                    if (false !== $dashletColumnKey = array_search('start_date', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $startDate;
                    if (isset($dashlet) && $dashlet == 'dashlet') {
                        $arrCell['cell'] = $dashletCell;
                    } else {
                        $arrCell['cell'] = array($val['coupon_code'], $val['promotion_name'], $val['applicable'], $startDate, $expiryDate, $actions);
                    }

                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
            } else {
                $jsonResult['rows'] = array();
            }
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        } else {
            $this->layout('crm');
            $messages = array();
            if ($this->_flashMessage->hasMessages()) {
                $messages = $this->_flashMessage->getMessages();
            }

            $viewModel = new ViewModel();
            $response = $this->getResponse();
            $viewModel->setVariables(array(
                'form' => $searchPromotionForm,
                'saveSearchForm' => $saveSearchForm,
                'messages' => $messages,
                'jsLangTranslate' => array_merge($promotionSearchMessage, $commonMessage),
				'getDomesticShippingMethods' => $getDomesticShippingMethods,
				'getInternationalShippingMethods' => $getInternationalShippingMethods,
            ));
            return $viewModel;
        }
    }

    public function getProductCouponCode() {
        $this->getPromotionTable();
        $lastPromotionId = $this->_promotionTable->getLastProductPromotionId();
        $coupon = 'PROMO';
        if (empty($lastPromotionId)) {
            $coupon.='00001';
        } else {
            $lastPromotionId = $lastPromotionId['lastPromotionId'];
            $nextPromotionId = ($lastPromotionId + 1);
            $coupon.= @ str_repeat('0', 5 - strlen($nextPromotionId)) . $nextPromotionId;
        }
        return $coupon;
    }

    public function editCrmProductPromotionAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $this->getPromotionTable();
            if ($request->getPost('promotion_id') != '') {
                $createPromotionForm = new ProductPromotionForm ();
                /* Applicable for */
                $getApplicableFor = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPromotionApplicable();
                $applicableFor = array();
                if ($getApplicableFor !== false) {
                    foreach ($getApplicableFor as $key => $val) {
                        $applicableFor[$val['applicable_id']] = $val['applicable'];
                    }
                }
                $createPromotionForm->get('applicable_id')->setAttribute('options', $applicableFor);
                /* End  Applicable for */

				$postedData = $request->getPost()->toArray();
                
                $promotionCreateMessage = $this->_config['promotion_messages']['config']['promotion_create_message'];
                $commonMessage = $this->_config['promotion_messages']['config']['common_message'];
                $promotion = new Promotion($this->_adapter);
                $response = $this->getResponse();
                
                if ($postedData['applicable_id'] == 2) {
                    if (empty($postedData['user_ids'])) {
                        $msg = array();
                        $msg ['user'] = $promotionCreateMessage['EMPTY_CONTACT_LIST'];
                        $messages = array('status' => "error", 'message' => $msg);
                    }
                } else if ($postedData['applicable_id'] == 3) {
                    if (empty($postedData['group_name_ids'])) {
                        $msg = array();
                        $msg ['group_name'] = $promotionCreateMessage['EMPTY_GROUP_LIST'];
                        $messages = array('status' => "error", 'message' => $msg);
                    }
                }

				if(isset($postedData['is_free_product']) && $postedData['is_free_product'] == 1){
					if(empty($postedData['free_products'])){
						$msg = array();
						$msg ['free_product_name'] = $promotionCreateMessage['EMPTY_FREE_PRODUCT_LIST'];
						$messages = array('status' => "error", 'message' => $msg);
					}
				}
				if(isset($postedData['promotion_qualified_by'])){
					if($postedData['promotion_qualified_by'] == 2){
						if(empty($postedData['bundle_product_type']) && empty($postedData['bundle_products'])){
							$msg = array();
							$msg ['bundle_product_type_name'] = $promotionCreateMessage['EMPTY_ANY_BUNDLE_PRODUCT_LIST'];
							$messages = array('status' => "error", 'message' => $msg);
						}
					}elseif($postedData['promotion_qualified_by'] == 3){
						if(empty($postedData['promotion_any_product_type']) && empty($postedData['promotion_any_products'])){
							$msg = array();
							$msg ['promotion_any_product_type_name'] = $promotionCreateMessage['EMPTY_ANY_PRODUCT_TYPE_LIST'];
							$messages = array('status' => "error", 'message' => $msg);
						}
					}
					
				}
                
                if (!empty($messages)) {
                    $response->setContent(\Zend\Json\Json::encode($messages));
                } else {
                    if (isset($postedData['promotion_id']) && $postedData['promotion_id'] != '' && $postedData['promotion_id'] != '0') {
                        
                        $appliedFormRemovePromotionArr = array();
                        $appliedFormRemovePromotionArr[] = $postedData['promotion_id'];
                        $this->_promotionTable->removeAppliedProductPromotion($appliedFormRemovePromotionArr);
                        
                        if ($postedData['applicable_id'] != 1) {
                            $appliedFormPromotionFormArr = array();
                            $appliedFormPromotionFormArr['promotion_id'] = $postedData['promotion_id'];
                            $appliedFormPromotionFormArr['applicable_id'] = $postedData['applicable_id'];
                            $appliedFormPromotionFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                            $appliedFormPromotionFormArr['added_date'] = DATE_TIME_FORMAT;
                            $appliedFormPromotionFormArr['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                            $appliedFormPromotionFormArr['modified_date'] = DATE_TIME_FORMAT;
                            
                            $applicableIds = ($postedData['applicable_id'] == 2) ? $postedData['user_ids'] : $postedData['group_name_ids'];
                            $applicableIdsArr = explode(',', $applicableIds);
                            if (!empty($applicableIdsArr)) {
                                foreach ($applicableIdsArr as $appliedId) {
                                    $appliedFormPromotionFormArr['applied_id'] = $appliedId;
                                    $appliedPromotionArr = $promotion->getAppliedPromotionArr($appliedFormPromotionFormArr);
                                    $appliedPromotionId = $this->_promotionTable->saveAppliedPromotion($appliedPromotionArr);
                                }
                            }
                        }
                        /* End for insert record for contact and group */
                        
                        $postedData['start_date'] = (!is_null($postedData['start_date']) && $postedData['start_date'] != '') ? $this->InputDateFormat($postedData['start_date'], 'db_date_format') : '';
                        $postedData['expiry_date'] = (!is_null($postedData['expiry_date']) && $postedData['expiry_date'] != '') ? $this->InputDateFormat($postedData['expiry_date'], 'db_date_format') : '';
                        $postedData['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                        $postedData['modified_date'] = DATE_TIME_FORMAT;
						
						if(isset($postedData['shipping_address_location']) && (!empty($postedData['shipping_address_location']))){ 
							if(in_array(1,$postedData['shipping_address_location']) && in_array(3,$postedData['shipping_address_location'])){ 
								$postedData['applicable_to'] = '3';
							}elseif(implode(",",$postedData['shipping_address_location']) == 1){
								$postedData['applicable_to'] = '1';
							}elseif(implode(",",$postedData['shipping_address_location']) == 3){
								$postedData['applicable_to'] = '2';
							}
						}
						$editPromotionArr = $promotion->getEditProductPromotionArr($postedData);
                        
                        $promotionId = $this->_promotionTable->editProductPromotion($editPromotionArr);

                        /** insert into the change log */
                        $changeLogArray = array();
                        $changeLogArray['table_name'] = 'tbl_prm_change_logs';
                        $changeLogArray['activity'] = '2';
                        $changeLogArray['id'] = $promotionId;
                        $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                        $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                        $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                        $this->flashMessenger()->addMessage(ucfirst($postedData['promotion_name']) . " successfully updated !");
                        $messages = array('status' => "success", 'message' => $promotionCreateMessage['PROMOTION_UPDATED_SUCCESSFULLY']);
                    } else {
                        $this->flashMessenger()->addMessage($promotionCreateMessage['PROMOTION_UPDATED_FAIL']);
                        $messages = array('status' => "fail", 'message' => $promotionCreateMessage['PROMOTION_UPDATED_FAIL']);
                    }
                    $response->setContent(\Zend\Json\Json::encode($messages));
                }
                return $response;
            } else {
                return $this->redirect()->toRoute('getPromotions');
            }
        } else {
            return $this->redirect()->toRoute('getPromotions');
        }
    }

    public function editCrmProductPromotionDetailAction() {
        $this->checkUserAuthentication();
        $this->getPromotionTable();
        $promotionCreateMessage = $this->_config['promotion_messages']['config']['promotion_create_message'];
        $commonMessage = $this->_config['promotion_messages']['config']['common_message'];
        $promotion = new Promotion($this->_adapter);
        $createPromotionForm = new ProductPromotionForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $createPromotionForm->get('savepromotion')->setValue('UPDATE');

        /* Applicable for */
        $getApplicableFor = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPromotionApplicable();
        if ($getApplicableFor !== false) {
            foreach ($getApplicableFor as $key => $val) {
                $applicableFor[$val['applicable_id']] = $val['applicable'];
            }
        }
        $createPromotionForm->get('applicable_id')->setAttributes(array('options' => $applicableFor));
        /* End  Applicable for */

		$applicationConfigIds = $this->_config['application_config_ids'];

		$transaction = new Transaction($this->_adapter);

		$shippingMetArr = array();
		$shippingMetArr['pb_shipping_id'] = $applicationConfigIds['domestic_pb_id'];
		$shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
		$getDomesticShippingMethods = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getShippingMethod($shippingMethodArr);

		

		$shippingMetArr = array();
		$shippingMetArr['pb_shipping_id'] = $applicationConfigIds['internation_pb_id'];
		$shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
		$getInternationalShippingMethods = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getShippingMethod($shippingMethodArr);

        $promotionId = '';
        $params = $this->params()->fromRoute();

        if (isset($params['promotionId']) && $params['promotionId'] != '') {
            $promotionId = $this->decrypt($params['promotionId']);
            $promotionDetailArr = array();
            $promotionDetailArr[] = $promotionId;
            $getPromotionDetail = $this->_promotionTable->getProductPromotionById($promotionDetailArr);

            $getPromotionDetail['expiry_date'] = ($getPromotionDetail['expiry_date'] == 0) ? '' : $this->DateFormat($getPromotionDetail['expiry_date'], 'calender');
            $getPromotionDetail['start_date'] = ($getPromotionDetail['start_date'] == 0) ? '' : $this->DateFormat($getPromotionDetail['start_date'], 'calender');

            $createPromotionForm->get('promotion_id')->setValue($promotionId);

            $getPromotionAppliedDetail = $promotionDetailArr = array();
            if ($getPromotionDetail['applicable_id'] != 1) {
                $promotionDetailArr[] = $promotionId;
                $promotionDetailArr[] = 'applied';
                $promotionDetailArr[] = $getPromotionDetail['applicable'];
                $getPromotionAppliedDetail = $this->_promotionTable->getProductPromotionAppliedByPromotionId($promotionDetailArr);
            }

            ############ free products #########################
            $freeProducts = $promotionDetailArr = array();
            if ($getPromotionDetail['free_products'] != '' && !is_null($getPromotionDetail['free_products'])) {
                $promotionDetailArr[] = $promotionId;
                $promotionDetailArr[] = 'products';
                $promotionDetailArr[] = 'free_products';
                $freeProducts = $this->_promotionTable->getProductPromotionAppliedByPromotionId($promotionDetailArr);
            }

            ################### end of free product ###########################
            ############ excluded products/productTypes #########################

            $excludedProducts = $excludedProductsName = $promotionDetailArr = array();
            if ($getPromotionDetail['excluded_products'] != '' && !is_null($getPromotionDetail['excluded_products'])) {
                $promotionDetailArr[] = $promotionId;
                $promotionDetailArr[] = 'products';
                $promotionDetailArr[] = 'excluded_products';
                $excludedProducts = $this->_promotionTable->getProductPromotionAppliedByPromotionId($promotionDetailArr);
            }

            $excludedProductTypes = $promotionDetailArr = array();
            if ($getPromotionDetail['excluded_product_type'] != '' && !is_null($getPromotionDetail['excluded_product_type'])) {
                $promotionDetailArr[] = $promotionId;
                $promotionDetailArr[] = 'product_type';
                $promotionDetailArr[] = 'excluded_product_type';
                $excludedProductTypes = $this->_promotionTable->getProductPromotionAppliedByPromotionId($promotionDetailArr);
            }

            ################### end of excluded product ###########################

			############ minimum amount products/productTypes #########################

            $minimumAmountProducts = $minimumAmountProductsName = $promotionDetailArr = array();
            if ($getPromotionDetail['promotion_qualified_by'] != '' && !is_null($getPromotionDetail['promotion_qualified_by']) && $getPromotionDetail['promotion_qualified_by'] == '1') {
                $promotionDetailArr[] = $promotionId;
                $promotionDetailArr[] = 'products';
                $promotionDetailArr[] = 'minimum_amount_products';
                $minimumAmountProducts = $this->_promotionTable->getProductPromotionAppliedByPromotionId($promotionDetailArr);
            }

            $minimumAmountProductTypes = $promotionDetailArr = array();
            if ($getPromotionDetail['promotion_qualified_by'] != '' && !is_null($getPromotionDetail['promotion_qualified_by']) && $getPromotionDetail['promotion_qualified_by'] == '1') {
                $promotionDetailArr[] = $promotionId;
                $promotionDetailArr[] = 'product_type';
                $promotionDetailArr[] = 'minimum_amount_product_type';
                $minimumAmountProductTypes = $this->_promotionTable->getProductPromotionAppliedByPromotionId($promotionDetailArr);
            }

            ################### end of minimum amount product ###########################

            ############ bundle products/productTypes #########################

            $bundleProducts = $bundleProductsName = $promotionDetailArr = array();
            if ($getPromotionDetail['promotion_qualified_by'] != '' && !is_null($getPromotionDetail['promotion_qualified_by']) && $getPromotionDetail['promotion_qualified_by'] == '2') {
                $promotionDetailArr[] = $promotionId;
                $promotionDetailArr[] = 'products';
                $promotionDetailArr[] = 'bundle_products';
                $bundleProducts = $this->_promotionTable->getProductPromotionAppliedByPromotionId($promotionDetailArr);
            }

            $bundleProductTypes = $promotionDetailArr = array();
            if ($getPromotionDetail['promotion_qualified_by'] != '' && !is_null($getPromotionDetail['promotion_qualified_by']) && $getPromotionDetail['promotion_qualified_by'] == '2') {
                $promotionDetailArr[] = $promotionId;
                $promotionDetailArr[] = 'product_type';
                $promotionDetailArr[] = 'bundle_product_type';
                $bundleProductTypes = $this->_promotionTable->getProductPromotionAppliedByPromotionId($promotionDetailArr);
            }

            ################### end of bundle product ###########################
            ############ any products #########################

            $anyProducts = $anyProductsName = $promotionDetailArr = array();
            if ($getPromotionDetail['promotion_qualified_by'] != '' && !is_null($getPromotionDetail['promotion_qualified_by']) && $getPromotionDetail['promotion_qualified_by'] == '3') {
                $promotionDetailArr[] = $promotionId;
                $promotionDetailArr[] = 'products';
                $promotionDetailArr[] = 'promotion_any_products';
                $anyProducts = $this->_promotionTable->getProductPromotionAppliedByPromotionId($promotionDetailArr);
            }

            $anyProductTypes = $promotionDetailArr = array();
            if ($getPromotionDetail['promotion_qualified_by'] != '' && !is_null($getPromotionDetail['promotion_qualified_by']) && $getPromotionDetail['promotion_qualified_by'] == '3') {
                $promotionDetailArr[] = $promotionId;
                $promotionDetailArr[] = 'product_type';
                $promotionDetailArr[] = 'promotion_any_product_type';
                $anyProductTypes = $this->_promotionTable->getProductPromotionAppliedByPromotionId($promotionDetailArr);
            }

            ################### end of bundle product ###########################

            $this->setValueForPromotionElement($getPromotionDetail, $createPromotionForm);
        }
        $encryptedPromotionId = (isset($params['promotionId']) && $params['promotionId'] != '') ? $params['promotionId'] : '';
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => array_merge($promotionCreateMessage, $commonMessage),
            'createPromotionForm' => $createPromotionForm,
            'promotionDetail' => $getPromotionDetail,
            'promotionId' => $promotionId,
            'promotionAppliedDetail' => $getPromotionAppliedDetail,
            'freeProducts' => $freeProducts,
            'excludedProducts' => $excludedProducts,
            'bundleProducts' => $bundleProducts,
			'minimumAmountProducts' => $minimumAmountProducts,
            'anyProducts' => $anyProducts,
            'excludedProductTypes' => $excludedProductTypes,
            'bundleProductTypes' => $bundleProductTypes,
			'minimumAmountProductTypes' => $minimumAmountProductTypes,
            'anyProductTypes' => $anyProductTypes,
            'encryptedPromotionId' => $encryptedPromotionId,
			'getDomesticShippingMethods' => $getDomesticShippingMethods,
			'getInternationalShippingMethods' => $getInternationalShippingMethods
        ));

        return $viewModel;
    }


	public function deleteCrmProductPromotionAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $promotionId = $request->getPost('promotionId');
            if ($promotionId != '') {
                $promotionId = $this->decrypt($promotionId);
                $postArr = $request->getPost();
                $postArr['promotion_id'] = $promotionId;
                $this->getPromotionTable();
                $promotionSearchMessages = $this->_config['promotion_messages']['config']['promotion_search_message'];
                $promotion = new Promotion($this->_adapter);

                $response = $this->getResponse();

                $promotionArr = $promotion->getArrayForRemovePromotion($postArr);
                $this->_promotionTable->removeProductPromotionById($promotionArr);

                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_prm_change_logs';
                $changeLogArray['activity'] = '3';
                $changeLogArray['id'] = $promotionId;
                $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                /** end insert into the change log */
                $promotionDetailArr = array();
                $promotionDetailArr[] = $promotionId;
                $promotionData = $this->_promotionTable->getProductPromotionById($promotionDetailArr);
                $this->flashMessenger()->addMessage($promotionData['promotion_name'] . " successfully deleted !");
                $messages = array('status' => "success", 'message' => $promotionSearchMessages['PROMOTION_DELETED_SUCCESSFULLY']);

                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                return $this->redirect()->toRoute('getPromotions');
            }
        } else {
            return $this->redirect()->toRoute('getPromotions');
        }
    }

	public function viewCrmProductPromotionInfoAction() {
        $this->checkUserAuthentication();
        $this->getPromotionTable();
        $promotionSearchMessage = $this->_config['promotion_messages']['config']['promotion_create_message'];
        $commonMessage = $this->_config['promotion_messages']['config']['common_message'];
        $promotion = new Promotion($this->_adapter);
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        if (isset($params['promotionId']) && $params['promotionId'] != '') {
            $promotionId = $this->decrypt($params['promotionId']);
            $promotionDetailArr = array();
            $promotionDetailArr[] = $promotionId;
            $promotionData = $this->_promotionTable->getProductPromotionById($promotionDetailArr);
            $promotionDetailArr[] = '';
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($promotionSearchMessage, $commonMessage),
                'promotionId' => $promotionId,
                'encryptPromotionId' => $params['promotionId'],
                'promotionData' => $promotionData
            ));
            return $viewModel;
        }
    }

	public function getCrmProductPromotionInfoAction() {
        $this->checkUserAuthentication();
        $this->getPromotionTable();
        $promotionCreateMessages = $this->_config['promotion_messages']['config']['promotion_create_message'];
        $commonMessage = $this->_config['promotion_messages']['config']['common_message'];
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        if (isset($params['promotionId']) && $params['promotionId'] != '') {
            $this->layout('crm');
            $this->getPromotionTable();
            $promotionId = $this->decrypt($params['promotionId']);
            $promotionDetailArr = array();
            $promotionDetailArr[] = $promotionId;
            $promotionData = $this->_promotionTable->getPromotionById($promotionDetailArr);
            $mode = (isset($params['mode']) && $params['mode'] != '') ? $this->decrypt($params['mode']) : 'view';
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($promotionCreateMessages, $commonMessage),
                'promotionId' => $promotionId,
                'encryptPromotionId' => $params['promotionId'],
                'promotionData' => $promotionData,
                'moduleName' => $this->encrypt('promotion'),
                'mode' => $mode
            ));
            return $viewModel;
        }
    }
    
    public function setValueForPromotionElement($postArr = array(), $form = array()) {
        if (!is_array($postArr)) {
            $postArr = $postArr->toArray();
        }
        $elements = $form->getElements();

        foreach ($elements as $elementName => $elementObject) {

            if (array_key_exists($elementName, $postArr)) {
                if (is_array($postArr[$elementName])) {
                    $value = implode(',', $postArr[$elementName]);
                    $elementObject->setValue($value);
                } else {
                    if ($postArr[$elementName] != '' && !is_null($postArr[$elementName]) && $postArr[$elementName] != 'N/A') {
                        $value = $postArr[$elementName];
                        $elementObject->setValue($value);
                    }
                }
            }
        }
    }
}
