<?php
/**
 * This page is used for Promotions module autoloading classes before module intilize.
 * @package    Promotions
 * @author     Icreon Tech - DT
 */
return array(
    'Promotions\Module'                       => __DIR__ . '/Module.php',
    'Promotions\Controller\PromotionController'   => __DIR__ . '/src/Promotions/Controller/PromotionController.php',
    'Promotions\Model\Promotion'            => __DIR__ . '/src/Promotions/Model/Promotion.php',
    'Promotions\Model\PromotionTable'                   => __DIR__ . '/src/Promotions/Model/PromotionTable.php'
);
?>