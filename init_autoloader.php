<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
/**
 * This autoloading setup is really more complicated than it needs to be for most
 * applications. The added complexity is simply to reduce the time it takes for
 * new developers to be productive with a fresh skeleton. It allows autoloading
 * to be correctly configured, regardless of the installation method and keeps
 * the use of composer completely optional. This setup should work fine for
 * most users, however, feel free to configure autoloading however you'd like.
 */
// Composer autoloading
if (file_exists('library/autoload.php')) {
    $loader = include 'library/autoload.php';
}

$zf2Path = false;

if (is_dir('library')) {
    $zf2Path = 'library/ZF2';
    $basePath = 'library/Base';
    $authorizePath = 'library/Authorize';
    $phpExcelPath = 'library/PHPExcel';
    $phpPdfPath = 'library/Html2pdf';
} elseif (getenv('ZF2_PATH')) {      // Support for ZF2_PATH environment variable or git submodule
    $zf2Path = getenv('ZF2_PATH');
} elseif (get_cfg_var('zf2_path')) { // Support for zf2_path directive value
    $zf2Path = get_cfg_var('zf2_path');
}

if ($zf2Path) {
    if (isset($loader)) {
        $loader->add('Zend', $zf2Path)
                ->add('Base', $basePath);
    } else {
        include $zf2Path . '/Zend/Loader/AutoloaderFactory.php';
        Zend\Loader\AutoloaderFactory::factory(array(
            'Zend\Loader\StandardAutoloader' => array(
                'autoregister_zf' => true
            )
        ));
        include $basePath . '/Controller/BaseController.php';
        include $basePath . '/Model/BaseModel.php';
        include $basePath . '/Model/UploadHandler.php';
        include $basePath . '/Model/SphinxComponent.php';
        include $basePath . '/Model/SphinxClient.php';
        include $basePath . '/Model/SpreadsheetExcelReader.php';
     // include $basePath . '/Model/BaseFacebook.php';
     // include $basePath . '/Model/Facebook.php';
      // - start
       spl_autoload_register(function ($class) {
          $prefix = 'Facebook\\';
          $base_dir = defined('FACEBOOK_SDK_V4_SRC_DIR') ? FACEBOOK_SDK_V4_SRC_DIR : __DIR__ . '/library/Facebook/';
          $len = strlen($prefix);
          if (strncmp($prefix, $class, $len) !== 0) { return; }
          $relative_class = substr($class, $len);
          $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
          if (file_exists($file)) { require $file; }
        });        
       // - end

        include $basePath . '/Model/ZipStream.php';
        include $authorizePath . '/lib/AuthnetXML.class.php';
        include $authorizePath . '/lib/shared/AuthorizeNetResponse.php';
        include $authorizePath . '/lib/shared/AuthorizeNetRequest.php';
        include $authorizePath . '/lib/AuthorizeNetAIM.php';
        include $authorizePath . '/lib/AuthorizeNetCP.php';
        include $phpExcelPath . '/Classes/PHPExcel.php';
        include $phpExcelPath . '/Classes/PHPExcel/IOFactory.php';
        include $phpPdfPath . '/html2pdf.class.php';
        /* include $phpPdfPath.'/_class/exception.class.php';
          include $phpPdfPath.'/_class/locale.class.php';
          include $phpPdfPath.'/_class/myPdf.class.php';
          include $phpPdfPath.'/_class/parsingHtml.class.php';
          include $phpPdfPath.'/_class/parsingCss.class.php';
          include $basePath . '/Model/html2fpdf.php';
          include $basePath . '/Model/html2pdf.php';
          include $basePath . '/Model/fpdf.php'; */
    }
}



if (!class_exists('Zend\Loader\AutoloaderFactory')) {
    throw new RuntimeException('Unable to load ZF2. Run `php composer.phar install` or define a ZF2_PATH environment variable.');
}
